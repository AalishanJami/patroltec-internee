@extends('backend.layouts.backend')
@section('content')
    @php
       $data=localization();
    @endphp
        {{ Breadcrumbs::render('clocks') }}
        <!-- <span class="table-add float-right mb-3 mr-2">
            <a class="text-success"  data-toggle="modal" data-target="#modalRegisterCompany">
                <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
            </a>
        </span> -->


         <div class="card">

            <div class="card-body">
                <div id="table" class="table-editable">
        <button class="btn btn-danger btn-sm">
            Delete Selected Clocks
        </button>
        <button class="btn btn-primary btn-sm">
          Restore Deleted Clocks
        </button>

        <button  style="display: none;">
            Show Active Clocks
        </button>
        <button class="btn btn-warning btn-sm">
           Excel Export
        </button>
        <button class="btn btn-success btn-sm">
            Word Export
        </button>
    <table  class="company_table_static table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th> ID
            </th>
            <th> Name
            </th>
            <th> Date of Birth
            </th>
            <th> Month
            </th>
            <th> Year
            </th>
            <th> Registration Number
            </th>
            <th> National Insurance Number
            </th>
            <th> Unique Learning Number
            </th>
            <th> Result
            </th>
        </tr>
        </thead>
            <tr>
                <td class="pt-3-half">1</td>
                <td class="pt-3-half">clock Test</td>
                <td class="pt-3-half">15 jan 1994</td>
                <td class="pt-3-half">Jan</td>
                <td class="pt-3-half">1994</td>
                <td class="pt-3-half">lgf77 777</td>
                <td class="pt-3-half">Zhf77 999</td>
                <td class="pt-3-half">OOllf77 999</td>
                <td class="pt-3-half">9999</td>
            </tr>
        <tbody>
        </tbody>
    </table>
   </div>
</div>
</div>

@endsection
