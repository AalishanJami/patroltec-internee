<footer class=" page-footer rgba-stylish-light"  >
    <div class="footer-copyright py-3 text-center">
        <div class="container-fluid">
        <span class="assign_class label{{getKeyid('copyright',$data)}}" data-id="{{getKeyid('copyright',$data) }}" data-value="{{checkKey('copyright',$data) }}" >
          {!! checkKey('copyright',$data) !!}
        </span>
            <a href="http://xweb4u.com/" target="_blank">
          <span>
            Xweb4u
          </span>
            </a>
        </div>
    </div>
</footer>
<div class="modal" id="myModal">
    <div class="modal-dialog" style="max-width: 724px!important;">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Change Text</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="modal_text_id">
                <input type="hidden" id="modal_text_value">
                <div class="summernote" id="body-text"></div><br>
            </div>
            <div class="modal-footer cms-modal-footer">
                <div class="row">
                    <div class="col-6">
                        <div id="placeholders"></div>
                    </div>
                    <div class="col-6">
                        <button type="button" id="cms_model" class="btn btn-success save-button float-right"   data-dismiss="modal">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="image_upload" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold"><span>Image Upload </span></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="card">
                <div class="card-body">
                    <form class="text-center" action="{{url('company/logo')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="md-form mb-5">
                            <input type="file" class="form-control" name="image"  required>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" id="close_model" class="btn btn-secondary btn-sm">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('.datepicker').pickadate({
        selectYears: 250,
    });
</script>
<!-- Footer -->
<!--Custom scripts-->
<script>
    function capital_letter(str)
    {
        str = str.split(" ");

        for (let i = 0, x = str.length; i < x; i++) {
            str[i] = str[i][0].toUpperCase() + str[i].substr(1);
        }

        return str.join(" ");
    }
    $(document).ready(function() {
        var action_text=$("table>thead>tr>th:last-child").html();
        var action_text=$(".all_action_btn").html();
        if (action_text==""){
            var result = '<span class="assign_class text-left label{{getKeyid('action',$data)}}" data-id="{{getKeyid('action',$data) }}" data-value="{{checkKey('action',$data) }}" style="display:block;">{!! checkKey('action',$data) !!}</span>';
            $(".all_action_btn").html(result);
            $(".all_action_btn").addClass('text-left');
            // $("table>thead>tr>th:last-child").html(result);
            // $("table>thead>tr>th:last-child").addClass('align-top');
        }

        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-center",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
        $('.body_notes').summernote('code', '');
        $('.summernote_inner').summernote({
            height: 500,
            tabsize: 2,
            toolbar: [
            ],
        });

        $('#datatable').DataTable().clear().destroy();
        $('#datatable').dataTable({
            processing: true,
            language: {

                'lengthMenu': '<span class="assign_class label'+$('#show_label').attr('data-id')+'" data-id="'+$('#show_label').attr('data-id')+'" data-value="'+$('#show_label').val()+'" >'+$('#show_label').val()+'</span>  _MENU_ <span class="assign_class label'+$('#entries_label').attr('data-id')+'" data-id="'+$('#entries_label').attr('data-id')+'" data-value="'+$('#entries_label').val()+'" >'+$('#entries_label').val()+'</span>',
                'search':'<span class="assign_class label'+$('#search_label').attr('data-id')+'" data-id="'+$('#search_label').attr('data-id')+'" data-value="'+$('#search_label').val()+'" >'+$('#search_label').val()+'</span>',
                'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                'info': '<span class="assign_class label'+$('#showing_page_label').attr('data-id')+'" data-id="'+$('#showing_page_label').attr('data-id')+'" data-value="'+$('#showing_page_label').val()+'" >'+$('#showing_page_label').val()+'</span> _PAGE_ <span class="assign_class label'+$('#of_label').attr('data-id')+'" data-id="'+$('#of_label').attr('data-id')+'" data-value="'+$('#of_label').val()+'" >'+$('#of_label').val()+'</span> _PAGES_',
                'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                'paginate': {
                    'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                    'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                },
                'infoFiltered': "(filtered from _MAX_ total records)"
            },
        });

    });
    // SideNav Initialization
    $(".button-collapse").sideNav();
    var container = document.querySelector('.custom-scrollbar');
    var ps = new PerfectScrollbar(container, {
        wheelSpeed: 2,
        wheelPropagation: true,
        minScrollbarLength: 20
    });


    $(document).ready(function() {
        let isOpen = false;
        let $windowWidth = $( window ).width();
        const $btnCollapse = $(".button-collapse");
        const $content = $('#content');

        $( window ).resize(function() {

            $windowWidth = $( window ).width();
            if($windowWidth > 1440) {
                $content.css('padding-left', '250px');
                if(isOpen) {
                    $btnCollapse.css('left', '0');
                    isOpen = false;
                }
            } else if($windowWidth < 530 && isOpen) {
                $btnCollapse.css('left', '0');
                $content.css('padding-left', '0');
                $('#sidenav-overlay').css('display', 'block');
                $btnCollapse.trigger('click');
            } else {
                if(!isOpen) {
                    $content.css('padding-left', '0');
                }
            }
        });

        // SideNav Button Initialization
        $btnCollapse.sideNav();

        $btnCollapse.on('click', () => {
            isOpen = !isOpen;
            if($windowWidth > 530) {
                const elPadding = isOpen ? '250px' : '0';
                $btnCollapse.css('left', elPadding);
                $content.css('padding-left', elPadding);
                $('#sidenav-overlay').css('display', 'none');
            } else {
                $('#sidenav-overlay').on('click', () => {
                    isOpen = !isOpen;
                });
            }
        });
        $('#sidenav-overlay').on('click', () => {
            isOpen = !isOpen;
        });

        $(".modal").attr({'data-keyboard':false ,'data-backdrop':'static'});
        var footer_height = window.innerHeight;
        $("#my_footer").attr('style','margin-bottom:'+footer_height+'px !important;');

    });

    $(function () {
        $('[data-toggle="popover"]').popover()
    })

    $('.company_table_static').dataTable({
        processing: true,
        order: [[ 1, "asc" ]],
        ordering: true,
        bInfo:false,
        language: {
            'lengthMenu':  '_MENU_',
            'search':'<span class="assign_class label'+$('#search_label').attr('data-id')+'" data-id="'+$('#search_label').attr('data-id')+'" data-value="'+$('#search_label').val()+'" >'+$('#search_label').val()+'</span>',
            'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
            'info': '<span class="assign_class label'+$('#showing_page_label').attr('data-id')+'" data-id="'+$('#showing_page_label').attr('data-id')+'" data-value="'+$('#showing_page_label').val()+'" >'+$('#showing_page_label').val()+'</span> _PAGE_ <span class="assign_class label'+$('#of_label').attr('data-id')+'" data-id="'+$('#of_label').attr('data-id')+'" data-value="'+$('#of_label').val()+'" >'+$('#of_label').val()+'</span> _PAGES_',
            'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
            'paginate': {
                'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
            },
            'infoFiltered': "(filtered from _MAX_ total records)"
        },
    });

</script>
<style type="text/css">
    .unset_capital
    {
        text-transform: unset !important;
    }
    .modal {
        overflow-y:auto;
    }
    .dropdown_menu_width
    {
        width: 215px !important;left:-2px !important;top: -27px !important;
    }
    footer.page-footer
    {
        bottom: 0;
        color: #fff;
        position: fixed;
        width: 100%;
        text-align: left;
        z-index: 100000;
    }
    .md-toast-error
    {
        opacity:0.9 !important;
    }
    .disable_href
    {
        pointer-events: none;
        cursor: default;
    }
    .question_span
    {
        font-size: 1rem;
        color: #757575;
    }
    .active_custom > a
    {
        color: black;
    }
    .z-index
    {
        overflow: hidden;
    }
    .mt-10
    {
        margin-top: 4rem !important;
    }
    footer
    {
        padding-left: unset !important;
    }
    main {
        height: 100%;
    }
    .model_width
    {
        width: 50%;
    }
    .loader_top
    {
        top: 53%;
    }
    .loader{
        position: absolute;
        z-index: 1000;
        left: 47%;
        width: 40px;
        height: 40px;
        display: none;
    }
    .back {
        background-color: #f1f1f1;
        color: black;
        margin-right: 19px;
    }
    .traning_font{
        font-size: 28px;
    }
    .mt-4-5 {
        margin-top: 2.5rem !important;
    }
    .white_color
    {
        color: #fff !important;
    }
    .red_color_m
    {
        color: red !important;
    }
    .green_color
    {
        color: green !important;
    }
    .orange_color
    {
        color: orange !important;
    }
    .form_group_background
    {
        background-color:#4285F4 !important;
    }
    #footer
    {
        position: fixed;
        width: 100%;
        /* left: 0; */
        bottom: 0;
    }
    .page-footer {
        margin-top: 15rem !important;
    }
    .badge {
        font-size: 100% !important;
    }
    table.dataTable tbody>tr.selected, table.dataTable tbody>tr>.selected {
        background-color: #B0BED9 !important;
    }
    input[type="search"]::-ms-clear {  display: none; width : 0; height: 0; }
    input[type="search"]::-ms-reveal {  display: none; width : 0; height: 0; }
    /* Disable browser close icon for Chrome */
    input[type="search"]::-webkit-search-decoration,
    input[type="search"]::-webkit-search-cancel-button,
    input[type="search"]::-webkit-search-results-button,
    input[type="search"]::-webkit-search-results-decoration { display: none; }
</style>
<script src="{{ asset('js/Backend/mdb.min.js') }}"></script>
<script src="{{ asset('js/Backend/datatables.min.js') }}"></script>
<script src="{{ asset('summernote/summernote-bs4.js') }}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.js"></script>
<script src="{{ asset('custom/general.js') }}"></script>
</body>
</html>
