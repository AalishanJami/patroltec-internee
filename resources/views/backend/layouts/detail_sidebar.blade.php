<div class="side-nav side-nav-custom fixed "style="transform: translateX(0px);">
    <ul class="custom-scrollbar">
        <li class="logo-sn waves-effect py-3">
            <div class="text-center">
                <a href="{{url('dashboard')}}" class="pl-0">
                    <img src="https://patroltec.net/backend/web/uploads/_180705101111.png" style="width:100px" />
                </a>
            </div>
        </li>
        <li>
            <ul class="collapsible collapsible-accordion">
                @if(Auth()->user()->hasPermissionTo('view_project') || Auth::user()->all_companies == 1 )
                    <li>
                        <a class="collapsible-header waves-effect arrow-r"  href="{{url('/project')}}" >
                            <i class="sv-slim-icon fas fa-user-alt"></i>
                            <span data-id="{{getKeyid('project',$data) }}" data-value="{{checkKey('project',$data) }}" class="assign_class label{{getKeyid('project',$data)}}">
                                {!! checkKey('project',$data) !!}
                            </span>
                        </a>
                    </li>
                @endif
                @if(Auth()->user()->hasPermissionTo('view_project') || Auth::user()->all_companies == 1 )
                    <li>
                        <a href="{{url('project/detail')}}/{{Session::get('project_id')}}"  class=" {{ request()->is('project/detail/'.Session::get('project_id')) ? 'active' : ''  }} collapsible-header waves-effect arrow-r">
                            <i class="fab fa-flipboard"></i>
                            <span data-id="{{getKeyid('main_detail',$data) }}" data-value="{{checkKey('main_detail',$data) }}" class="assign_class label{{getKeyid('main_detail',$data)}}">
                                {!! checkKey('main_detail',$data) !!}
                            </span>
                        </a>
                    </li>
                @endif
                @if(Auth()->user()->hasPermissionTo('view_bonusScore') || Auth::user()->all_companies == 1 )
                    <li>
                        <a href="{{url('bonus/score')}}"  class="collapsible-header waves-effect arrow-r {{ request()->is('bonus/score') ? 'active' : ''  }}">
                            <i class="fas fa-award"></i>
                            <span data-id="{{getKeyid('bonus_score',$data) }}" data-value="{{checkKey('bonus_score',$data) }}" class="assign_class label{{getKeyid('bonus_score',$data)}}">
                                {!! checkKey('bonus_score',$data) !!}
                            </span>
                            <span class="badge badge-default ml-2">
                                {{sidebarCount('bonus_scores','location_id',Session::get('project_id'))}}
                            </span>
                        </a>
                    </li>
                @endif
                @if(Auth()->user()->hasPermissionTo('view_address') || Auth::user()->all_companies == 1 )
                    <li>
                        <a class="collapsible-header waves-effect arrow-r {{ request()->is('address') ? 'active' : ''  }}" href="{{url('address')}}">
                            <i class="far fa-address-book"></i>
                            <span data-id="{{getKeyid('address',$data) }}" data-value="{{checkKey('address',$data) }}" class="assign_class label{{getKeyid('address',$data)}}">
                                {!! checkKey('address',$data) !!}
                            </span>
                            <span class="badge badge-default ml-2">
                                {{sidebarCount('location_addresses','location_id',Session::get('project_id'))}}
                            </span>
                        </a>
                    </li>
                @endif
                @if(Auth()->user()->hasPermissionTo('view_contacts') || Auth::user()->all_companies == 1 )
                    <li>
                        <a class="collapsible-header waves-effect arrow-r {{ request()->is('contact') ? 'active' : ''  }}" href="{{url('contact')}}">
                            <i class="fas fa-phone-square"></i>
                            <span data-id="{{getKeyid('contact',$data) }}" data-value="{{checkKey('contact',$data) }}" class="assign_class label{{getKeyid('contact',$data)}}">
                                {!! checkKey('contact',$data) !!}
                            </span>
                            <span class="badge badge-default ml-2">
                                {{sidebarCount('site_contacts','location_id',Session::get('project_id'))}}
                            </span>
                        </a>
                    </li>
                @endif
                @if(Auth()->user()->hasPermissionTo('view_equipmentGroup') || Auth::user()->all_companies == 1 )
                    <li>
                        <a class="collapsible-header waves-effect arrow-r {{ request()->is('plant_and_equipment') ? 'active' : ''  }}" href="{{url('plant_and_equipment')}}">
                            <i class="fas fa-tools"></i>
                            <span data-id="{{getKeyid('plant_and_equipment',$data) }}" data-value="{{checkKey('plant_and_equipment',$data) }}" class="sv-slim assign_class label{{getKeyid('plant_and_equipment',$data)}}">
                                {!! checkKey('plant_and_equipment',$data) !!}
                            </span>   
                            <span class="badge badge-default ml-2">
                                {{sidebarCount('location_breakdowns','location_id',Session::get('project_id'),'type','e')}}
                            </span>
                        </a>
                    </li>
                @endif
                @if(Auth()->user()->hasPermissionTo('view_projectqr') || Auth::user()->all_companies == 1 )
                    <li>
                        <a href="{{url('qr/show')}}"  class="collapsible-header waves-effect arrow-r {{ request()->is('qr/show') ? 'active' : ''  }}">
                            <i class="fas fa-qrcode"></i>
                            <span data-id="{{getKeyid('qr',$data) }}" data-value="{{checkKey('qr',$data) }}" class="sv-slim assign_class label{{getKeyid('qr',$data)}}">
                                {!! checkKey('qr',$data) !!}
                            </span>
                            <span class="badge badge-default ml-2">
                                {{sidebarCount('location_breakdowns','location_id',Session::get('project_id'),'type','t')}}
                            </span>
                        </a>
                    </li>
                @endif
                <li>
                    <a href="{{url('project/completed_forms')}}"  class="collapsible-header waves-effect arrow-r {{ request()->is('project/completed_forms') ? 'active' : ''  }}">
                        <i class="fas fa-qrcode"></i>
                        <span data-id="{{getKeyid('completed_forms',$data) }}" data-value="{{checkKey('completed_forms',$data) }}" class="sv-slim assign_class label{{getKeyid('completed_forms',$data)}}">
                            {!! checkKey('completed_forms',$data) !!}
                        </span>
                        <span class="badge badge-default ml-2">{{sidebarCount('job_groups','completed',2,null,null,'=','projects')}}</span>
                    </a>
                </li>
            </ul>
        </li>
    </ul>
    <div class="sidenav-bg mask-strong"></div>
</div>
