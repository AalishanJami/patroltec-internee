<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=yes">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>PatrolTec - @yield('title')</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
        <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="{{asset('css/Backend/bootstrap.min.css')}}">
    <!-- Material Design Bootstrap -->
    <link rel="stylesheet" href="{{asset('/css/Backend/mdb.min.css')}}">
    <link rel="stylesheet" href="{{asset('/css/Backend/mdb.css')}}">
    <link rel="stylesheet" href="{{asset('/css/Backend/app.css')}}">
    <link rel="stylesheet" href="{{asset('/css/Backend/style.css')}}">
    <link rel="stylesheet" href="{{asset('/css/Backend/style.min.css')}}">
    <link rel="stylesheet" href="{{asset('/css/Backend/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('/css/Backend/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('/css/Backend/datatables.min.css')}}">
    <link rel="stylesheet" href="{{asset('/css/Backend/datatables-select.min.css')}}">
    <link rel="stylesheet" href="{{asset('/css/Backend/multi-select.css')}}">
    <link rel="stylesheet" href="{{asset('/css/Backend/addons/mdb-file-upload.css')}}">

    <link rel="stylesheet" href="{{asset('/css/Backend/addons/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('/css/Backend/addons/datatables.min.css')}}">
    <link rel="stylesheet" href="{{asset('/css/Backend/addons/datatables-select.min.css')}}">
    <link rel="stylesheet" href="{{asset('/css/Backend/addons/datatables.min.css')}}">
    <link rel="stylesheet" href="{{asset('/css/Backend/addons/directives.min.css')}}">
    <link rel="stylesheet" href="{{asset('/css/Backend/addons/rating.min.css')}}">
    <link rel="stylesheet" href="{{asset('/css/Backend/addons/zmd.hierarchical-display.min.css')}}">
    <link rel="stylesheet" href="{{asset('/css/Backend/addons/directives.css')}}">

    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="{{ asset('summernote/summernote-bs4.css') }}">

    <link rel="stylesheet" href="{{asset('editor/css/quill.css')}}">
    <link rel="stylesheet" href="{{ asset('editor/css/quill.snow.css') }}">
    <link rel="stylesheet" href="{{asset('editor/css/quill.bubble.css')}}">
    <link rel="stylesheet" href="{{asset('css/easyresponsivetabs.css')}}">
    @toastr_css
    <style type="text/css">
        .active_plus_btn_link
        {
            background-color: lightgreen!important;
        }
        .hover_btn span {
            max-width: 0;
            -webkit-transition: max-width 1s;
            transition: max-width 1s;
            display: inline-block;
            vertical-align: top;
            white-space: nowrap;
            overflow: hidden;
        }
        .hover_btn:hover span {
            max-width: 7rem;
        }
        .active_plus_btn_text{
            font-size: 16px !important;
            font-family: Roboto, Sans-Serif;
            font-weight: 400;
            line-height: 25px !important;
            margin-top: -4px;
        }
        .note-editable
        {
            height:300px !important;
        }
        .anchor_underline
        {
            text-decoration: underline;
        }

        }
        .ml-4-5 {
            margin-left: 2.5rem !important;
        }
        .card-title
        {
            /*margin-left: 10px;*/
        }
       body
       {
        font-family: Roboto,sans-serif
       }
        .wider
        {
            margin-top: 2%;
        }
        .form_picker_set
        {
            margin: auto;
        }
        .form_height
        {
            height: 300px;
        }
        #companyCreateModel,#select_edit
        {
            padding-top: 8%;
        }
        .list-group-flush
        {
            padding-left: 0px;
        }
        .role_font
        {
            font-size: 11px;
        }
          .padding-left-custom
          {
            padding-left: 15%;
          }
          .side-nav-custom {
          position: fixed;
          top: 9%;
          left: 0;
          z-index: unset;
        }
        th{
            padding-bottom: 40px
        }
        .contact-form{
            background: #fff;
            margin-top: 4%;
            margin-bottom: 5%;
            width: 100%;
        }
        .contact-form .form-control{
            border-radius:1rem;
        }
        .contact-image{
            text-align: center;
        }
        .contact-image img{
            border-radius: 6rem;
            width: 11%;
            margin-top: -3%;
            transform: rotate(29deg);
        }
        .contact-form form{
            padding: 14%;
        }
        .contact-form form .row{
            margin-bottom: -7%;
        }
        .contact-form h3{
            margin-bottom: 8%;
            margin-top: -10%;
            text-align: center;
            color: #0062cc;
        }
        .contact-form .btnContact {
            width: 50%;
            border: none;
            border-radius: 1rem;
            padding: 1.5%;
            background: #dc3545;
            font-weight: 600;
            color: #fff;
            cursor: pointer;
        }
        .btnContactSubmit
        {
            width: 50%;
            border-radius: 1rem;
            padding: 1.5%;
            color: #fff;
            background-color: #0062cc;
            border: none;
            cursor: pointer;
        }

        .card-title-custom
        {
            font-size: 12px;
        }
        .custom_dynamic_height

        {
            /*height: 450px;*/
            overflow: overlay;
        }
        .custom-height
        {
            height: 40px;
            border-bottom: 1px solid #aaa;
        }
        .ms-container {

            width:100%;
        }
        .note-insert,.note-table,.note-fontname,.note-para {
            display: none !important;
        }
        .previous
        {
            margin-right: 2%;
            color: #868e96;
        }
        .next
        {
            margin-left: 2%;
            color: #868e96;
        }
        #myModal
        {
            z-index:100000;
        }
        #cms_btn
        {
            z-index:100000;
        }
        .form-style {
            display: inline-block; //Or display: inline;
        }
        .modal-sm-custom{
            width: 40%;
        }


    </style>

    <!-- SCRIPTS -->
    <!-- JQuery -->

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" rel="stylesheet"/> <link href="https://cdn.jsdelivr.net/sweetalert2/6.4.3/sweetalert2.min.css" rel="stylesheet"/>

    <script src="https://cdn.jsdelivr.net/sweetalert2/latest/sweetalert2.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <script src="{{ asset('/js/Backend/jquery-3.4.1.min.js') }}"></script>
    <script src="{{asset('/js/Backend/jquery.multi-select.js')}}"></script>
    <!-- Bootstrap tooltips -->
    <script src="{{ asset('js/Backend/popper.min.js') }}"></script>
    <!-- Bootstrap core JavaScript -->
    <script src="{{ asset('js/Backend/app.js') }}"></script>


    <!-- MDB custom -->
  <!--   <script src="{{ asset('js/new/main/bootstrap.js') }}"></script>
    <script src="{{ asset('js/new/main/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/new/main/jquery.js') }}"></script>
    <script src="{{ asset('js/new/main/jquery.min.js') }}"></script>
    <script src="{{ asset('js/new/main/mdb.js') }}"></script>
    <script src="{{ asset('js/new/main/mdb.lite.js') }}"></script>
    <script src="{{ asset('js/new/main/mdb.lite.min.js') }}"></script>
    <script src="{{ asset('js/new/main/mdb.min.js') }}"></script>
    <script src="{{ asset('js/new/main/mdb.min.js') }}"></script>
    <script src="{{ asset('js/new/main/modules/material-select/material-select.min.js') }}"></script>
    <script src="{{ asset('js/new/main/modules/material-select/material-select-view.min.js') }}"></script>
    <script src="{{ asset('js/new/main/modules/material-select/material-select-view-renderer.min.js') }}"></script>
    <script src="{{ asset('js/new/main/modules/dropdown/dropdown.min.js') }}"></script>
    <script src="{{ asset('js/new/main/modules/dropdown/dropdown-searchable.min.js') }}"></script> -->





    <!-- MDB core JavaScript -->
    <script src="{{ asset('js/Backend/bootstrap.js') }}"></script>
    <script src="{{ asset('js/Backend/mdb.min.js') }}"></script>
    <script src="{{ asset('js/Backend/mdb.js') }}"></script>
    <script src="{{ asset('js/Backend/datatables.min.js') }}"></script>
    <script src="{{ asset('js/Backend/datatables-select.min.js') }}"></script>

    <script src="{{ asset('summernote/summernote-bs4.js') }}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.js"></script>
    <script src="{{ asset('custom/general.js') }}"></script>
    <script src="{{ asset('custom/js/user.js') }}"></script>
    <script src="{{ asset('custom/js/company.js') }}"></script>
    <script src="{{ asset('custom/js/role.js') }}"></script>
    <script src="{{ asset('custom/js/custom_frontend.js') }}"></script>
    <script src="{{ asset('custom/js/project.js') }}"></script>
    <script src="{{ asset('js/datatables-edit.js') }}"></script>
    <script src="{{asset('editor/bootstrap-quill.js')}}"></script>
    <script src="{{asset('js/easyResponsiveTabs.js')}}"></script>
    <script src="{{ asset('custom/js/login_edit.js') }}"></script>
      <!-- JQuery -->
    <script src="{{ asset('js/Backend/addons/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('js/Backend/addons/jquery-ui-touch-punch.min.js') }}"></script>
    <script src="{{ asset('js/Backend/addons/mdb-file-upload.min.js') }}"></script>
    <script src="{{ asset('js/Backend/addons/datatables.min.js') }}"></script>
    <script src="{{ asset('js/Backend/addons/datatables-select.min.js') }}"></script>
    <script src="{{ asset('js/Backend/addons/imagesloaded.pkgd.min.js') }}"></script>
    <script src="{{ asset('js/Backend/addons/jquery.zmd.hierarchical-display.min.js') }}"></script>
    <script src="{{ asset('js/Backend/addons/masonry.pkgd.min.js') }}"></script>
    <script src="{{ asset('js/Backend/addons/progressBar.min.js') }}"></script>
    <script src="{{ asset('js/Backend/addons/rating.min.js') }}"></script>



{{--    <script src="{{ asset('custom/js/equipment.js') }}"></script>--}}
{{--    <script src="{{ asset('custom/js/plant_equipment.js') }}"></script>--}}
{{--    <script src="{{ asset('custom/js/bous_score.js') }}"></script>--}}
{{--    <script src="{{ asset('custom/js/contractor.js') }}"></script>--}}
{{--    <script src="{{ asset('custom/js/devision.js') }}"></script>--}}
{{--    <script src="{{ asset('custom/js/customer.js') }}"></script>--}}
{{--    <script src="{{ asset('custom/js/site_group.js') }}"></script>--}}
{{--    <script src="{{ asset('custom/js/manager.js') }}"></script>--}}
{{--    <script src="{{ asset('custom/js/contractor_manager.js') }}"></script>--}}
{{--    <script src="{{ asset('custom/js/qr.js') }}"></script>--}}
{{--    <script src="{{ asset('custom/js/addresstype.js') }}"></script>--}}
{{--    <script src="{{ asset('custom/js/contacttype.js') }}"></script>--}}

    @yield('headscript')


    <script type="text/javascript">
        window.onload = function() {
            document.getElementById("action").classList.remove('sorting_asc');
        };
    </script>
    <script type="text/javascript">
        $.fn.serializeObject=function(){"use strict";var a={},b=function(b,c){var d=a[c.name];"undefined"!=typeof d&&d!==null?$.isArray(d)?d.push(c.value):a[c.name]=[d,c.value]:a[c.name]=c.value};return $.each(this.serializeArray(),b),a};
    </script>
</head>
<body class="fixed-sn white-skin">
<!--Main Navigation-->
<div id="content" class="color-block content">
<header>
    @php
        $data=localization();
    @endphp
