<!-- Navbar -->
<nav class="navbar fixed-top navbar-expand-lg scrolling-navbar double-nav">
    <div class="float-left">
        <a href="#" data-activates="slide-out" class="sidebarbtn button-collapse black-text"><i class="fas fa-bars"></i></a>
    </div>
    <div class="breadcrumb-dn mr-auto">
    </div>
    <ul class="nav navbar-nav nav-flex-icons ml-auto">
         <li class="nav-item dropdown notifications-nav">
            <select searchable="Search here.."  id="companies_selected_nav" class="mdb-select companies_selected"  >
                <option disabled value="" selected>Company</option>
                @if(isset($companies))
                    @foreach($companies as $key => $company)
                        <option {{(Auth::user()->default_company ==$company->id ) ? 'selected' :''}} value="{{$company->id}}">
                            {{$company->name}}
                        </option>
                    @endforeach
                @endif
            </select>
        </li>
        <input type="hidden" class="current_path" value="">
        <li class="nav-item dropdown notifications-nav">
            <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
            </div>
        </li>
        @if(Auth()->user()->hasPermissionTo('view_cms') || Auth::user()->all_companies == 1 )
        <li class="nav-item"  >
            <button data-class="edit-disable" class="edit_cms_enable edit_cms_switch btn btn-primary btn-sm waves-effect waves-light" style="display: none;" >
                Edit cms
            </button>
            <button  data-class="edit-enable" class="edit_cms_disable edit_cms_switch btn btn-danger btn-sm waves-effect waves-light">
                Edit cms
            </button>
        </li>
        @endif
        <li class="nav-item">
            <a class="nav-link waves-effect">
                <span class="clearfix d-none d-sm-inline-block">
                    {{\Illuminate\Support\Facades\Auth::user()->name}}
                </span>
            </a>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle waves-effect" href="#" id="userDropdown" data-toggle="dropdown"
               aria-haspopup="true" aria-expanded="false">
               <i class="fas fa-book-reader"></i>
                <span class="clearfix d-none d-sm-inline-block">
                    <span>{{language()}}</span>
                </span>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                @foreach(getLanguage() as $key =>$value)
                    <a class="dropdown-item" href="{{URL::to('/language/change/to/'.$value->short_lan)}}">{{$value->language}}</a>
                @endforeach()
            </div>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle waves-effect" href="#" id="userDropdown" data-toggle="dropdown"
               aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-user"></i> <span class="clearfix d-none d-sm-inline-block">Profile</span></a>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                <a class="dropdown-item" data-toggle="modal" data-target="#image_upload">
                    Logo Upload
                </a>
                <a class="dropdown-item" href="{{ route('logout') }}"
                    onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </li>

    </ul>
</nav>
<!-- /.Navbar -->
