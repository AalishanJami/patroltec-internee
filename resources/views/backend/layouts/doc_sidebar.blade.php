<div class="side-nav side-nav-custom fixed "style="transform: translateX(0px);">
  <ul class="custom-scrollbar">
    <li class="logo-sn waves-effect py-3">
      <div class="text-center">
        <a href="{{url('dashboard')}}" class="pl-0">
          <img src="https://patroltec.net/backend/web/uploads/_180705101111.png" style="width:100px" />
        </a>
      </div>
    </li>
    <li>
      <ul class="collapsible collapsible-accordion">
        <li>
            <a href="{{(Session::get('folder_id') == null ) ? url('form_group') :url('form_group/folder').'/'.Session::get('folder_id')}}" class="collapsible-header waves-effect arrow-r">
                <i class="fas fa-arrow-left"></i>
                back
            </a>
        </li>
        <li>
          <a href="{{url('/form_group')}}" class="collapsible-header waves-effect arrow-r">
            <i class="sv-slim-icon fas fa-book"></i>
            <span class="assign_class label{{getKeyid('documents',$data)}}" data-id="{{getKeyid('documents',$data) }}" data-value="{{checkKey('documents',$data) }}" >
              {!! checkKey('documents',$data) !!}
            </span>
          </a>
        </li>
        <li>
          <a href="{{url('document/detail')}}/{{Session::get('form_id')}}" class=" collapsible-header waves-effect arrow-r  {{ request()->is('document/detail/'.Session::get('form_id')) ? 'active' : ''  }} ">
            <i class="fab fa-flipboard"></i> 
            <span class="assign_class label{{getKeyid('main_detail',$data)}}" data-id="{{getKeyid('main_detail',$data) }}" data-value="{{checkKey('main_detail',$data) }}" >
              {!! checkKey('main_detail',$data) !!}
            </span>
          </a>
        </li>
        <li>
          <a href="{{url('/document/forms')}}"  class="collapsible-header waves-effect arrow-r {{ request()->is('document/forms') ? 'active' : ''  }}">
            <i class="fab fa-wpforms"></i> 
            <span class="assign_class label{{getKeyid('dynamic_forms',$data)}}" data-id="{{getKeyid('dynamic_forms',$data) }}" data-value="{{checkKey('dynamic_forms',$data) }}" >
              {!! checkKey('dynamic_forms',$data) !!}
            </span>  
            <span class="badge badge-default ml-2">1</span>
          </a>
        </li>
        <li>
          <a href="{{url('document/bonus/score')}}"  class="collapsible-header waves-effect arrow-r {{ request()->is('document/bonus/score') ? 'active' : ''  }}">
            <i class="fas fa-chart-line"></i>
            <span class="assign_class label{{getKeyid('scoring',$data)}}" data-id="{{getKeyid('scoring',$data) }}" data-value="{{checkKey('scoring',$data) }}" >
              {!! checkKey('scoring',$data) !!}
            </span> 
            <span class="badge badge-default ml-2">
              {{sidebarCount('scorings','form_id',Session::get('form_id'))}}
            </span>
         </a>
        </li>
        <li>
          <a href="{{url('schedule')}}" class="collapsible-header waves-effect arrow-r {{ request()->is('schedule') ? 'active' : ''  }}">
            <i class="far fa-calendar-alt"></i>
            <span class="assign_class label{{getKeyid('schedule',$data)}}" data-id="{{getKeyid('schedule',$data) }}" data-value="{{checkKey('schedule',$data) }}" >
              {!! checkKey('schedule',$data) !!}
            </span>
            <span class="badge badge-default ml-2">
              {{sidebarCount('schedule_tasks','form_id',Session::get('form_id'))}}
            </span>
          </a>
        </li> 
        <li>
          <a href="{{url('version/log')}}"  class="collapsible-header waves-effect arrow-r {{ request()->is('version/log') ? 'active' : ''  }}">
            <i class="fas fa-code-branch"></i>
            <span class="assign_class label{{getKeyid('version_log',$data)}}" data-id="{{getKeyid('version_log',$data) }}" data-value="{{checkKey('version_log',$data) }}" >
              {!! checkKey('version_log',$data) !!}
            </span> 
            <span class="badge badge-default ml-2">
              {{sidebarCount('version_logs','form_id',Session::get('form_id'))}}
            </span>
          </a>
        </li>
        <li>
          <a href="{{url('document/notes')}}"  class="collapsible-header waves-effect arrow-r {{ request()->is('document/notes') ? 'active' : ''  }}">
            <i class="fas fa-sticky-note"></i>
            <span class="assign_class label{{getKeyid('notes',$data)}}" data-id="{{getKeyid('notes',$data) }}" data-value="{{checkKey('notes',$data) }}" >
              {!! checkKey('notes',$data) !!}
            </span>
            <span class="badge badge-default ml-2">
              {{sidebarCount('notes','link_id',2)}}
            </span>
          </a>
        </li>
        @if(Session::get('is_import'))
        <li>
          <a href="{{url('document/bulk/import')}}"  class="collapsible-header waves-effect arrow-r {{ request()->is('document/bulk/import') ? 'active' : ''  }}">
            <i class="fas fa-file-import"></i>
            <span class="assign_class label{{getKeyid('bulk_import',$data)}}" data-id="{{getKeyid('bulk_import',$data) }}" data-value="{{checkKey('bulk_import',$data) }}" >
              {!! checkKey('bulk_import',$data) !!}
            </span> 
            <span class="badge badge-default ml-2">
              {{ sidebarCount('uploads','link_id',2) }}
            </span>
          </a>
        </li>
        @endif
        <li>
          <a href="{{url('view_register')}}"  class="collapsible-header waves-effect arrow-r {{ request()->is('view_register') ? 'active' : ''  }}">
            <i class="fas fa-registered"></i>
            <span class="assign_class label{{getKeyid('view_register',$data)}}" data-id="{{getKeyid('view_register',$data) }}" data-value="{{checkKey('view_register',$data) }}" >
              {!! checkKey('view_register',$data) !!}
            </span>  
            <span class="badge badge-default ml-2">
              1
            </span>
          </a>
        </li>
        <li>
          <a href="{{url('document/completed_forms')}}" class="waves-effect {{ request()->is('document/completed_forms') ? 'active' : ''  }}">
            <i class="fab fa-wpforms"></i>
            <span data-id="{{getKeyid('completed_forms',$data ?? '') }}" data-value="{{checkKey('completed_forms',$data ?? '') }}" class="sv-slim assign_class label{{getKeyid('completed_forms',$data ?? '')}}">
              {!! checkKey('completed_forms',$data) !!}
            </span>
            <span class="badge badge-default ml-2">{{sidebarCount('job_groups','completed',2,null,null,'=','forms')}}</span>
          </a>
        </li>
      </ul>
    </li>
  </ul>
  <div class="sidenav-bg mask-strong"></div>
</div>