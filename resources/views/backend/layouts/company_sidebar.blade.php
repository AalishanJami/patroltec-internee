<div class="side-nav side-nav-custom fixed "style="transform: translateX(0px);">
  <ul class="custom-scrollbar">
    <li class="logo-sn waves-effect py-3">
      <div class="text-center">
        <a href="{{url('dashboard')}}" class="pl-0">
          <img src="https://patroltec.net/backend/web/uploads/_180705101111.png" style="width:100px" />
        </a>
      </div>
    </li>
    <li>
      <ul class="collapsible collapsible-accordion">
        <li>
           <a  href="{{url('/company')}}" class="collapsible-header waves-effect arrow-r">
              <i class="sv-slim-icon fas fa-tags"></i> 
              <span>
                Company
              </span>
           </a>
        </li>
        <li>
          <a class="collapsible-header waves-effect arrow-r {{ request()->is('cms/app') ? 'active' : ''  }}" href="{{url('cms/app')}}">
            <i class="sv-slim-icon fas fa-window-restore"></i>
              <span >
                App CMS<span class="badge badge-default ml-2">3</span>
              </span>
          </a>
        </li>
        <li>
          <a class="collapsible-header waves-effect arrow-r {{ request()->is('cms/web') ? 'active' : ''  }}" href="{{url('cms/web')}}">
            <i class="sv-slim-icon fas fa-tablet-alt"></i>
              <span>
                Web CMS
                <span class="badge badge-default ml-2">
                  {{sidebarCount('company_labels')}}
                </span>
              </span>
          </a>
        </li>    
      </ul>
    </li>
  </ul>
  <div class="sidenav-bg mask-strong"></div>
</div>