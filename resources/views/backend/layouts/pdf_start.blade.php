<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <title>{{$reportname}}</title>
    <style>
        .pdf_table_layout{
            vertical-align: text-top;
            width: 9%;
        }
        .col_text_style{
            width: 100%;
            word-break: break-word;
            text-align:left;
            display:inline-block;
            overflow-wrap: break-word;
            word-wrap: break-word;
            line-break: strict;
            hyphens: none;
            -webkit-hyphens: none;
            -moz-hyphens: none;
        }
        @page { width:100%;  }
        @page { size: a4 landscape; }
    </style>
</head>
<body style="width:100%;">
    <!-- <table style="width:100%;margin-top:-30px;">
        <tr style="width:100%;">
            <td style="width: 10%;">
                <img src="https://patroltec.net/backend/web/uploads/_180705101111.png" height="100" width="100"/><br>
            </td>
            <td style="width: 30%;text-align:left;">
                @if(!empty($reportname))
                    <span style="margin-top:7%;text-align:left;float:left;display: block;font-size: 12px;">{{$reportname}}</span>
                @endif
            </td>
            <td style="width: 60%;text-align: right;">
                <span class="user text-bottom" style="margin-top:7%;text-align:right;float:right;display: block;font-size: 12px;">
                    {{Auth()->user()->name}} :  {{$date}}
                </span>
            </td>
        </tr>
    </table> -->
    <table style="width:100%;">
                <tr style="width:100%;">
                    <td style="width: 10%;">
                        <img src="https://patroltec.net/backend/web/uploads/_180705101111.png" height="100" width="100"/><br>
                    </td>
                    <td style="width: 70%;text-align: left;padding-right: 30px;">
                        @if(!empty($reportname))
                            <span style="font-weight:bold;display: block;font-size: 24px;">{{$reportname}}</span>
                        @endif
                        <span class="user text-bottom" style="display: block;font-size: 12px;">
                            {{Auth()->user()->name}} :  {{$date}}
                        </span>
                    </td>
                </tr>
            </table>
    <table style="width:100%;table-layout:fixed;" class="table table-bordered">
