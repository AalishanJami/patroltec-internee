@include('backend.layouts.head')
@include('backend.layouts.header')
@include('backend.layouts.sidebar')
</header>
<!--Main Navigation-->
<!-- Main layout -->
<main>
    <div class="container-fluid">
        @toastr_js
        @toastr_render
        @yield('content')
    </div>
</main>
@include('backend.layouts.footer')
