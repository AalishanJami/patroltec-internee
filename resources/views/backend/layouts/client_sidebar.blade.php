
<div class="side-nav side-nav-custom fixed "style="transform: translateX(0px);">
  <ul class="custom-scrollbar">
      <li class="logo-sn waves-effect py-3">
        <div class="text-center">
            <a href="{{url('dashboard')}}" class="pl-0">
              <img src="https://patroltec.net/backend/web/uploads/_180705101111.png" style="width:100px" />
            </a>
        </div>
    </li>
      <li>
        <ul class="collapsible collapsible-accordion">
          @if(Auth()->user()->hasPermissionTo('view_client') || Auth::user()->all_companies == 1 )
            <li>
              <a href="{{url('client')}}" class="collapsible-header waves-effect arrow-r">
                <i class="fas fa-arrow-circle-left"></i>
                  <span class="assign_class label{{getKeyid('client',$data)}}" data-id="{{getKeyid('client',$data) }}" data-value="{{checkKey('client',$data) }}" >
                    {!! checkKey('client',$data) !!}
                  </span>
              </a>
            </li>
          @endif
          @if(Auth()->user()->hasPermissionTo('view_client') || Auth::user()->all_companies == 1 )
              <li>
                  <a href="{{url('client/detail')}}/{{Session::get('client_id')}}" class=" collapsible-header waves-effect arrow-r  {{ request()->is('client/detail/'.Session::get('client_id')) ? 'active' : ''  }} ">
                      <i class="fab fa-flipboard"></i>
                      <span class="assign_class label{{getKeyid('main_detail',$data)}}" data-id="{{getKeyid('main_detail',$data) }}" data-value="{{checkKey('main_detail',$data) }}" >
                        {!! checkKey('main_detail',$data) !!}
                      </span>
                  </a>
              </li>
          @endif
          @if(Auth()->user()->hasPermissionTo('view_clientNotes') || Auth::user()->all_companies == 1 )
                <li>
                    <a class="collapsible-header waves-effect arrow-r {{ request()->is('client/notes') ? 'active' : ''  }}" href="{{url('client/notes')}}">
                        <i class="far fa-sticky-note"></i>
                        <span class="assign_class label{{getKeyid('notes',$data)}}" data-id="{{getKeyid('notes',$data) }}" data-value="{{checkKey('notes',$data) }}" >
                          {!! checkKey('notes',$data) !!}
                        </span>
                        <span class="badge badge-default ml-2">{{sidebarCount('notes','link_id',1)}}</span>
                        
                    </a>
                </li>
            @endif
          @if(Auth()->user()->hasPermissionTo('view_clientUpload') || Auth::user()->all_companies == 1 )
              <li>
                  <a class="collapsible-header waves-effect arrow-r {{ request()->is('client/upload') ? 'active' : ''  }}" href="{{url('client/upload')}}">
                      <i class="fas fa-cloud-upload-alt"></i>
                      <span class="assign_class label{{getKeyid('upload',$data)}}" data-id="{{getKeyid('upload',$data) }}" data-value="{{checkKey('upload',$data) }}" >
                        {!! checkKey('upload',$data) !!}
                      </span>
                      <span class="badge badge-default ml-2">{{sidebarCount('uploads','link_id',1)}}</span>
                      
                  </a>
              </li>
          @endif
      </ul>
    </li>
  </ul>
    <div class="sidenav-bg mask-strong"></div>
</div>

