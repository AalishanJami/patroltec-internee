<div class="side-nav side-nav-custom fixed "style="transform: translateX(0px);">
    <ul class="custom-scrollbar">
        <li class="logo-sn waves-effect py-3">
            <div class="text-center">
                <a href="{{url('dashboard')}}" class="pl-0">
                    <img src="https://patroltec.net/backend/web/uploads/_180705101111.png" style="width:100px" />
                </a>
            </div>
        </li>
        <li>
            <ul class="collapsible collapsible-accordion">
                <li>
                    <a class="collapsible-header waves-effect arrow-r"  href="{{url('/employee')}}" >
                        <i class="sv-slim-icon fas fa-user-alt"></i>
                        <span class="assign_class label{{getKeyid('employees',$data)}}" data-id="{{getKeyid('employees',$data) }}" data-value="{{checkKey('employees',$data) }}" >
                            {!! checkKey('employees',$data) !!}
                        </span>
                    </a>
                </li>
                <li>
                    <a class="collapsible-header waves-effect arrow-r {{ request()->is('employee/detail/'.Session::get('employee_id')) ? 'active' : ''  }}"  href="{{url('employee/detail')}}/{{Session::get('employee_id')}}" >
                        <i class="fab fa-flipboard"></i>
                        <span class="assign_class label{{getKeyid('main_detail',$data)}}" data-id="{{getKeyid('main_detail',$data) }}" data-value="{{checkKey('main_detail',$data) }}" >
                          {!! checkKey('main_detail',$data) !!}
                        </span>
                    </a>
                </li>
                @if(Auth()->user()->hasPermissionTo('view_employeeStaffPermision') || Auth::user()->all_companies == 1 )
                    <li>
                        <a class="collapsible-header waves-effect arrow-r {{ request()->is('staff') ? 'active' : ''  }}" href="{{url('staff')}}" >
                            <i class="fas fa-user-circle "></i>
                            <span class="assign_class label{{getKeyid('staff',$data)}}" data-id="{{getKeyid('staff',$data) }}" data-value="{{checkKey('staff',$data) }}" >
                              {!! checkKey('staff',$data) !!}
                            </span>
                        </a>
                    </li>
                @endif
                @if(Auth()->user()->hasPermissionTo('view_employeeFilePermision') || Auth::user()->all_companies == 1 )
                    <li>
                        <a class="collapsible-header waves-effect arrow-r {{ request()->is('employee/documents') ? 'active' : ''  }}" href="{{url('employee/documents')}}">
                            <i class="far fa-address-book"></i>
                            <span class="assign_class label{{getKeyid('files',$data)}}" data-id="{{getKeyid('files',$data) }}" data-value="{{checkKey('files',$data) }}" >
                              {!! checkKey('files',$data) !!}
                            </span>
                        </a>
                    </li>
                @endif
                @if(Auth()->user()->hasPermissionTo('view_employeeLocationPermision') || Auth::user()->all_companies == 1 )
                    <li>
                        <a class="collapsible-header waves-effect arrow-r {{ request()->is('location') ? 'active' : ''  }}"  href="{{url('location')}}">
                            <i class="fas fa-location-arrow"></i>
                            <span class="assign_class label{{getKeyid('locations',$data)}}" data-id="{{getKeyid('locations',$data) }}" data-value="{{checkKey('locations',$data) }}" >
                                {!! checkKey('locations',$data) !!}
                            </span>
                        </a>
                    </li>
                @endif
                @if(Auth()->user()->hasPermissionTo('view_employeeContact') || Auth::user()->all_companies == 1 )
                    <li>
                        <a class="collapsible-header waves-effect arrow-r {{ request()->is('contact_infomation') ? 'active' : ''  }}" href="{{url('contact_infomation')}}">
                            <i class="fas fa-blender-phone"></i>
                            <span class="assign_class label{{getKeyid('contact_information',$data)}}" data-id="{{getKeyid('contact_information',$data) }}" data-value="{{checkKey('contact_information',$data) }}" >
                                {!! checkKey('contact_information',$data) !!}
                            </span>
                        </a>
                    </li>
                @endif
                <li>
                    <a class="collapsible-header waves-effect arrow-r {{ request()->is('next/kin') ? 'active' : ''  }}" href="{{url('next/kin')}}">
                        <i class="fas fa-users"></i>
                        <span class="assign_class label{{getKeyid('next_of_kin',$data)}}" data-id="{{getKeyid('next_of_kin',$data) }}" data-value="{{checkKey('next_of_kin',$data) }}" >
                            {!! checkKey('next_of_kin',$data) !!}
                        </span>
                        <span class="badge badge-default ml-2">
                            {{sidebarCount('user_next_kin','user_id',Session::get('employee_id'))}}
                        </span>
                    </a>
                </li>
                @if(Auth()->user()->hasPermissionTo('view_employeeAccount') || Auth::user()->all_companies == 1 )
                    <li>
                        <a class="collapsible-header waves-effect arrow-r {{ request()->is('account') ? 'active' : ''  }}" href="{{url('account')}}">
                            <i class="far fa-user-circle"></i>
                            <span class="assign_class label{{getKeyid('account',$data)}}" data-id="{{getKeyid('account',$data) }}" data-value="{{checkKey('account',$data) }}" >
                              {!! checkKey('account',$data) !!}
                            </span>
                        </a>
                    </li>
                @endif
                <!-- <li>
                    <a class="collapsible-header waves-effect arrow-r {{ request()->is('licence') ? 'active' : ''  }}" href="{{url('licence')}}">
                        <i class="far fa-id-card"></i>
                        <span class="assign_class label{{getKeyid('licence',$data)}}" data-id="{{getKeyid('licence',$data) }}" data-value="{{checkKey('licence',$data) }}" >
                            {!! checkKey('licence',$data) !!}
                        </span>
                        <span class="badge badge-default ml-2">
                            {{sidebarCount('sia_licences','user_id',Session::get('employee_id'))}}
                        </span>
                    </a>
                </li> -->
                <li>
                    <a class="collapsible-header waves-effect arrow-r {{ request()->is('employee/notes') ? 'active' : ''  }}" href="{{url('employee/notes')}}">
                        <i class="far fa-id-card"></i>
                        <span class="assign_class label{{getKeyid('notes',$data)}}" data-id="{{getKeyid('notes',$data) }}" data-value="{{checkKey('notes',$data) }}" >
                            {!! checkKey('notes',$data) !!}
                        </span>
                        <span class="badge badge-default ml-2">
                            {{sidebarCount('notes','link_id',3)}}
                        </span>
                    </a>
                </li>
                @if(Auth()->user()->hasPermissionTo('view_employee_document') || Auth::user()->all_companies == 1 )
                    <li>
                        <a class="collapsible-header waves-effect arrow-r {{ request()->is('user/document') ? 'active' : ''  }}" href="{{url('user/document')}}" >
                            <i class="fas fa-notes-medical"></i>
                            <span class="assign_class label{{getKeyid('documents',$data)}}" data-id="{{getKeyid('documents',$data) }}" data-value="{{checkKey('documents',$data) }}" >
                              {!! checkKey('documents',$data) !!}
                            </span>
                            <span class="badge badge-default ml-2">
                                {{sidebarCount('documents','link_id',3)}}
                            </span>
                        </a>
                    </li>
                @endif
                <li>
                    <a href="{{url('completed/forms')}}" class="waves-effect {{ request()->is('completed/forms') ? 'active' : ''  }} ">
                        <i class="fab fa-wpforms"></i>
                        <span data-id="{{getKeyid('completed_forms',$data) }}" data-value="{{checkKey('completed_forms',$data) }}" class="sv-slim assign_class label{{getKeyid('completed_forms',$data)}}">
                            {!! checkKey('completed_forms',$data) !!}
                        </span>
                        <span class="badge badge-default ml-2">{{sidebarCount('job_groups','completed',2,null,null,'=','employees')}}</span>
                    </a>
                </li>
            </ul>
        </li>
    </ul>
    <div class="sidenav-bg mask-strong"></div>
</div>
