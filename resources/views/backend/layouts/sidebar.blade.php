@php
    $data=localization();
@endphp
<div id="slide-out" class="side-nav fixed sn-bg-4 " style="background-image:url('https://cdn.pixabay.com/photo/2013/11/27/16/01/sunset-219354_960_720.jpg')">
    <ul class="custom-scrollbar">
        <li class="logo-sn waves-effect py-3">
            <div class="text-center">
                <a href="{{url('dashboard')}}" class="pl-0">
                    @if(companyImage(Auth::user()))
                        <img src="{{url('logo/'.companyImage(Auth::user()))}}" style="width:100px" />
                    @else
                        <img src="https://patroltec.net/backend/web/uploads/_180705101111.png" style="width:100px" />
                    @endif
                </a>
            </div>
        </li>
        <li>
            <ul class="collapsible collapsible-accordion">
                <li>
                    <a  href="{{route('dashboard')}}" class="collapsible-header waves-effect arrow-r {{ request()->is('dashboard') ? 'active' : ''  }}  ">
                        <i class="sv-slim-icon fas fa-tachometer-alt"></i>
                        <span data-id="{{getKeyid('dashboard',$data) }}" data-value="{{checkKey('dashboard',$data) }}" class="assign_class label{{getKeyid('dashboard',$data)}}">
                           {!! checkKey('dashboard',$data) !!}
                        </span>
                    </a>
                </li>
                <li>
                    <a class="collapsible-header waves-effect arrow-r {{ request()->is('project') ? 'active' : ''  }} {{ request()->is('client') ? 'active' : ''  }}">
                        <i class="sv-slim-icon fas fa-project-diagram"></i>
                        <span data-id="{{getKeyid('project',$data) }}" data-value="{{checkKey('project',$data) }}" class="assign_class label{{getKeyid('project',$data)}}">
                           {!! checkKey('project',$data) !!}
                        </span>
                        <i class="fas fa-angle-down rotate-icon"></i></a>
                    <div class="collapsible-body">
                        <ul>
                            @if(Auth()->user()->hasPermissionTo('view_project') || Auth::user()->all_companies == 1 )
                                <li>
                                    <a href="{{url('project')}}" class="waves-effect {{ request()->is('project') ? 'active' : ''  }}">
                                        <i class="fas fa-tasks"></i>
                                        <span data-id="{{getKeyid('project',$data) }}" data-value="{{checkKey('project',$data) }}" class="sv-slim assign_class label{{getKeyid('project',$data)}}">
                                            {!! checkKey('project',$data) !!}
                                        </span>
                                        <span class="badge badge-default ml-2">
                                            {{sidebarCount('locations')}}
                                        </span>
                                    </a>
                                </li>
                            @endif
                            @if(Auth()->user()->hasPermissionTo('view_client') || Auth::user()->all_companies == 1 )
                                <li>
                                    <a href="{{url('client')}}" class="collapsible-header waves-effect arrow-r {{ request()->is('client') ? 'active' : ''  }}">
                                        <i class="sv-slim-icon fas fa-user-tie"></i>
                                        <span data-id="{{getKeyid('client',$data) }}" data-value="{{checkKey('client',$data) }}" class="sv-slim assign_class label{{getKeyid('client',$data)}}">
                                            {!! checkKey('client',$data) !!}
                                        </span>
                                        <span class="badge badge-default ml-2">{{sidebarCount('clients')}}</span>
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </div>
                </li>
                <li>
                    <a  href="{{url('form_group')}}" class="{{ request()->is('form_group') ? 'active' : ''  }} collapsible-header waves-effect arrow-r">
                        <i class="sv-slim-icon fas fa-book"></i>
                        <span data-id="{{getKeyid('document_library',$data) }}" data-value="{{checkKey('document_library',$data) }}" class="assign_class label{{getKeyid('document_library',$data)}}">
                            {!! checkKey('document_library',$data) !!}
                        </span>
                        <span class="badge badge-default ml-2">
                            {{sidebarCount('forms','form_group_id',0)}}
                        </span>
                    </a>
                </li>
                <li>
                    <a class="collapsible-header waves-effect arrow-r  {{ request()->is('employee') ? 'active' : ''  }} {{ request()->is('training_matrix') ? 'active' : ''  }}  {{ request()->is('required_documents') ? 'active' : ''  }} {{ request()->is('completed/forms') ? 'active' : ''  }} ">
                        <i class="sv-slim-icon fas fa-user-alt"></i>
                        <span data-id="{{getKeyid('employees',$data) }}" data-value="{{checkKey('employees',$data) }}" class="assign_class label{{getKeyid('employees',$data)}}">
                              {!! checkKey('employees',$data) !!}
                        </span>
                        <i class="fas fa-angle-down rotate-icon"></i>
                    </a>
                    <div class="collapsible-body">
                        <ul>
                            @if(Auth()->user()->hasPermissionTo('view_employee') || Auth::user()->all_companies == 1 )
                            <li>
                                <a class="collapsible-header waves-effect arrow-r {{ request()->is('employee') ? 'active' : ''  }}" href="{{url('employee')}}">
                                    <i class="sv-slim-icon fas fa-user-alt"></i>
                                    <span data-id="{{getKeyid('employees',$data) }}" data-value="{{checkKey('employees',$data) }}" class="assign_class label{{getKeyid('employees',$data)}}">
                                        {!! checkKey('employees',$data) !!}
                                    </span>
                                    <span class="badge badge-default ml-2">
                                        {{sidebarCount('users')}}
                                    </span>
                                </a>
                            </li>
                            @endif
                            @if(Auth()->user()->hasPermissionTo('view_users') || Auth::user()->all_companies == 1 )
                                <li>
                                    <a class="collapsible-header waves-effect arrow-r {{ request()->is('required_documents') ? 'active' : ''  }}"  href="{{url('required_documents')}}">
                                        <i class="sv-slim-icon fas fa-book-open"></i>
                                        <span data-id="{{getKeyid('required_documents',$data) }}" data-value="{{checkKey('required_documents',$data) }}" class="assign_class label{{getKeyid('required_documents',$data)}}">
                                            {!! checkKey('required_documents',$data) !!}
                                        </span>
                                        <span class="badge badge-default ml-2">
                                            {{sidebarCount('user_doc_types')}}
                                        </span>
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </div>
                </li>
                <li>
                    <a href="{{url('job')}}" class="collapsible-header waves-effect arrow-r {{ request()->is('job') ? 'active' : ''  }}">
                        <i class="sv-slim-icon fas fa-window-maximize"></i>
                        <span data-id="{{getKeyid('jobs',$data) }}" data-value="{{checkKey('jobs',$data) }}" class="assign_class label{{getKeyid('jobs',$data)}}">
                            {!! checkKey('jobs',$data) !!}
                        </span>
                        <span class="badge badge-default ml-2"> {{sidebarCount('job_groups','completed',2,null,null,'!=')}}</span>
                    </a>
                </li>
                @if(Auth()->user()->hasPermissionTo('view_emailSupport') || Auth::user()->all_companies == 1 )
                <li>
                    <a  href="{{url('support')}}" class="  {{ request()->is('support') ? 'active' : ''  }} collapsible-header waves-effect arrow-r">
                        <i class="sv-slim-icon fas fa-headset"></i>
                        <span data-id="{{getKeyid('support',$data) }}" data-value="{{checkKey('support',$data) }}" class="assign_class label{{getKeyid('support',$data)}}">
                            {!! checkKey('support',$data) !!}
                        </span>
                        <span class="badge badge-default ml-2">
                            {{sidebarCount('supports')}}
                        </span>
                    </a>
                </li>
                @endif
                <li>
                    <a class="collapsible-header waves-effect arrow-r {{ request()->is('company') ? 'active' : ''  }} {{ request()->is('answer') ? 'active' : ''  }} {{ request()->is('question') ? 'active' : ''  }} {{ request()->is('audit') ? 'active' : ''  }} {{ request()->is('permissions') ? 'active' : ''  }} {{ request()->is('users') ? 'active' : ''  }} {{ request()->is('roles') ? 'active' : ''  }} {{ request()->is('login/cms') ? 'active' : ''  }} ">
                        <i class="sv-slim-icon fas fa-wrench"></i>
                        <span data-id="{{getKeyid('administration',$data) }}" data-value="{{checkKey('administration',$data) }}" class="assign_class label{{getKeyid('administration',$data)}}">
                            {!! checkKey('administration',$data) !!}
                        </span>
                        <span class="badge badge-default ml-2">4</span>
                        <i class="fas fa-angle-down rotate-icon"></i>
                    </a>
                    <div class="collapsible-body">
                        <ul>
                            {{--                           @if(Auth()->user()->hasPermissionTo('view_users') || Auth::user()->all_companies == 1 )--}}
                            {{--                                <li>--}}
                            {{--                                    <a href="{{url('users')}}" class="waves-effect {{ request()->is('users') ? 'active' : ''  }} ">--}}
                            {{--                                        <i class="fas fa-user"></i>--}}
                            {{--                                        <span data-id="{{getKeyid('users',$data) }}" data-value="{{checkKey('users',$data) }}" class="sv-slim assign_class label{{getKeyid('users',$data)}}">--}}
                            {{--                                            {!! checkKey('users',$data) !!}<span class="badge badge-default ml-2">2</span>--}}

                            {{--                                        </span>--}}
                            {{--                                    </a>--}}
                            {{--                                </li>--}}
                            {{--                            @endif--}}
                            @if(Auth()->user()->hasPermissionTo('view_roles') || Auth::user()->all_companies == 1 )
                                <li>
                                    <a href="{{url('roles')}}" class="{{ request()->is('roles') ? 'active' : ''  }} waves-effect">
                                        <i class="fas fa-user-cog"></i>
                                        <span data-id="{{getKeyid('roles',$data) }}" data-value="{{checkKey('roles',$data) }}" class="sv-slim assign_class label{{getKeyid('roles',$data)}}">
                                            {!! checkKey('roles',$data) !!}
                                        </span>
                                        <span class="badge badge-default ml-2">
                                            {{sidebarCount('roles')}}
                                        </span>
                                    </a>
                                </li>
                            @endif
                            @if(Auth()->user()->hasPermissionTo('view_permissions') || Auth::user()->all_companies == 1 )
                                <li>
                                    <a href="{{url('permissions')}}" class="{{ request()->is('permissions') ? 'active' : ''  }} waves-effect">
                                        <i class="fas fa-key"></i>
                                        <span data-id="{{getKeyid('permissions',$data) }}" data-value="{{checkKey('permissions',$data) }}" class="sv-slim assign_class label{{getKeyid('',$data)}}">
                                            {!! checkKey('permissions',$data) !!}
                                        </span>
                                        <span class="badge badge-default ml-2">
                                            {--sidebarCount('permissions')--}
                                        </span>
                                    </a>
                                </li>
                            @endif
{{--                            @if(Auth()->user()->hasPermissionTo('view_app_roles'))--}}
                                <li>
                                    <a href="{{url('app/roles')}}" class="{{ request()->is('app_roles') ? 'active' : ''  }} waves-effect">
                                        <i class="fas fa-key"></i>
                                        <span data-id="{{getKeyid('app_roles',$data) }}" data-value="{{checkKey('app_roles',$data) }}" class="sv-slim assign_class label{{getKeyid('',$data)}}">
                                            {!! checkKey('app_roles',$data) !!}
                                        </span>
                                        <span class="badge badge-default ml-2">
                                            {--sidebarCount('app_roles')--}
                                        </span>
                                    </a>
                                </li>
{{--                            @endif--}}
                            <li>
                                <a class="collapsible-header waves-effect arrow-r {{ request()->is('audit') ? 'active' : ''  }}  " href="{{url('audit')}}">
                                    <i class="fas fa-table"></i>
                                    <span data-id="{{getKeyid('audit_log',$data) }}" data-value="{{checkKey('audit_log',$data) }}" class="assign_class label{{getKeyid('audit_log',$data)}}">
                                        {!! checkKey('audit_log',$data) !!}
                                    </span>
                                    <span class="badge badge-default ml-2">
                                        {{sidebarCount('audits')}}
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('company')}}" class=" {{ request()->is('company') ? 'active' : ''  }} waves-effect">
                                    <i class="fas fa-building"></i>
                                    <span data-id="{{getKeyid('company',$data) }}" data-value="{{checkKey('company',$data) }}" class="sv-slim assign_class label{{getKeyid('company',$data)}}">
                                        {!! checkKey('company',$data) !!}
                                    </span>
                                    <span class="badge badge-default ml-2">
                                        {{sidebarCount('companies')}}
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a class="collapsible-header waves-effect arrow-r {{ request()->is('qr/list') ? 'active' : ''  }} {{ request()->is('plant_and_equipment/list') ? 'active' : ''  }} {{ request()->is('completed/forms') ? 'active' : ''  }} {{ request()->is('fors') ? 'active' : ''  }}  {{ request()->is('clocks') ? 'active' : ''  }} {{ request()->is('clocks') ? 'active' : ''  }} {{ request()->is('sia') ? 'active' : ''  }} {{ request()->is('app/location') ? 'active' : ''  }} {{ request()->is('panic/alaram') ? 'active' : ''  }} ">
                        <i class="sv-slim-icon far fa-chart-bar"></i>
                        <span data-id="{{getKeyid('reports',$data) }}" data-value="{{checkKey('reports',$data) }}" class="assign_class label{{getKeyid('reports',$data)}}">
                            {!! checkKey('reports',$data) !!}
                        </span>
                        <span class="badge badge-default ml-2">3</span>
                        <i class="fas fa-angle-down rotate-icon"></i>
                    </a>
                    <div class="collapsible-body">
                        <ul>
                            @if(Auth()->user()->hasPermissionTo('view_qrReport') || Auth::user()->all_companies == 1 )
                                <li>
                                    <a href="{{url('/report/qr/list')}}" class="{{ request()->is('qr/list') ? 'active' : ''  }} waves-effect">
                                        <i class="fas fa-qrcode"></i>
                                        <span data-id="{{getKeyid('qr',$data) }}" data-value="{{checkKey('qr',$data) }}" class="sv-slim assign_class label{{getKeyid('qr',$data)}}">
                                        {!! checkKey('qr',$data) !!}
                                    </span>
                                        <span class="badge badge-default ml-2">
                                            {{sidebarCount('location_breakdowns','type','t')}}
                                        </span>

                                    </a>
                                </li>
                            @endif
                            @if(Auth()->user()->hasPermissionTo('view_plantandequipmentReport') || Auth::user()->all_companies == 1 )
                                <li>
                                    <a class="collapsible-header waves-effect arrow-r {{ request()->is('plant_and_equipment/list') ? 'active' : ''  }} " href="{{url('report/plant_and_equipment/list')}}">
                                        <i class="fas fa-tools"></i>
                                        <span data-id="{{getKeyid('plant_and_equipment',$data) }}" data-value="{{checkKey('plant_and_equipment',$data) }}" class="sv-slim assign_class label{{getKeyid('plant_and_equipment',$data)}}">
                                            {!! checkKey('plant_and_equipment',$data) !!}
                                        </span>
                                        <span class="badge badge-default ml-2">
                                            {{sidebarCount('location_breakdowns','type','e')}}
                                        </span>
                                    </a>
                                </li>
                            @endif
                            <li>
                                <a href="{{url('completed_forms/report')}}" class="{{ request()->is('completed_forms/report') ? 'active' : ''  }} waves-effect">
                                    <i class="fab fa-wpforms"></i>
                                    <span data-id="{{getKeyid('completed_forms',$data) }}" data-value="{{checkKey('completed_forms',$data) }}" class="sv-slim assign_class label{{getKeyid('completed_forms',$data)}}">
                                        {!! checkKey('completed_forms',$data) !!}
                                    </span>
                                    <span class="badge badge-default ml-2">1</span>
                                </a>
                            </li>
                            <!-- <li>
                                <a href="{{url('fors')}}" class="{{ request()->is('fors') ? 'active' : ''  }} waves-effect">
                                    <i class="fas fa-database"></i>
                                    <span data-id="{{getKeyid('fors',$data) }}" data-value="{{checkKey('fors',$data) }}" class="sv-slim assign_class label{{getKeyid('fors',$data)}}">
                                        {!! checkKey('fors',$data) !!}
                                    </span>
                                    <span class="badge badge-default ml-2">2</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('clocks')}}" class="waves-effect {{ request()->is('clocks') ? 'active' : ''  }}">
                                    <i class="far fa-clock"></i>
                                    <span data-id="{{getKeyid('cloc',$data) }}" data-value="{{checkKey('cloc',$data) }}" class="sv-slim assign_class label{{getKeyid('cloc',$data)}}">
                                        {!! checkKey('cloc',$data) !!}
                                    </span>
                                    <span class="badge badge-default ml-2">4</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('sia')}}" class="waves-effect {{ request()->is('sia') ? 'active' : ''  }}">
                                    <i class="fab fa-trello"></i>
                                    <span data-id="{{getKeyid('sia',$data) }}" data-value="{{checkKey('sia',$data) }}" class="sv-slim assign_class label{{getKeyid('sia',$data)}}">
                                        {!! checkKey('sia',$data) !!}
                                    </span>
                                    <span class="badge badge-default ml-2">6</span>
                                </a>
                            </li> -->
                            <li>
                                <a href="{{url('app/location')}}" class="{{ request()->is('app/location') ? 'active' : ''  }} collapsible-header waves-effect arrow-r">
                                    <i class="sv-slim-icon fas fa-map-marker"></i>
                                    <span data-id="{{getKeyid('app_location',$data) }}" data-value="{{checkKey('app_location',$data) }}" class="sv-slim assign_class label{{getKeyid('app_location',$data)}}">
                                        {!! checkKey('app_location',$data) !!}
                                    </span>
                                    <span class="badge badge-default ml-2">0</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('panic/alaram')}}" class="{{ request()->is('panic/alaram') ? 'active' : ''  }} collapsible-header waves-effect arrow-r">
                                    <i class="sv-slim-icon fas fa-exclamation"></i>
                                    <span data-id="{{getKeyid('panic_alarms',$data) }}" data-value="{{checkKey('panic_alarms',$data) }}" class="sv-slim assign_class label{{getKeyid('panic_alarms',$data)}}">
                                        {!! checkKey('panic_alarms',$data) !!}
                                    </span>
                                    <span class="badge badge-default ml-2">1</span>
                                </a>
                            </li>
                            @if(Auth()->user()->hasPermissionTo('view_emailReport') || Auth::user()->all_companies == 1 )
                                <li>
                                    <a href="{{url('emails')}}" class="{{ request()->is('panic/alaram') ? 'active' : ''  }} collapsible-header waves-effect arrow-r">
                                        <i class="sv-slim-icon far fa-envelope-open"></i>
                                        <span data-id="{{getKeyid('email',$data) }}" data-value="{{checkKey('email',$data) }}" class="sv-slim assign_class label{{getKeyid('email',$data)}}">
                                            {!! checkKey('email',$data) !!}
                                        </span>
                                        <span class="badge badge-default ml-2">
                                            {{sidebarCount('supports')}}
                                        </span>
                                    </a>
                                </li>
                            @endif
                        </ul>
                </li>
            </ul>
            <div class="sidenav-bg mask-strong"></div>
        </li>
    </ul>
</div>
