<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>PatrolTec</title>
    <!-- Font Awesome -->
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="{{asset('css/Frontend/bootstrap.min.css')}}">
    <!-- Material Design Bootstrap -->
    <link rel="stylesheet" href="{{asset('css/Frontend/mdb.min.css')}}">
    <!-- Custom styles -->
    <style>
        html,
        body,
        header,
        .view {
            height: 100%;
        }


        @media (max-width: 740px) {
            html,
            body,
            header,
            .view {
                height: 1040px;
            }
        }
        @media (min-width: 800px) and (max-width: 850px) {
            html,
            body,
            header,
            .view {
                height: 600px;
            }
        }
    </style>
</head>

<body class="landing-page">
@php
    $data=localization();
@endphp
<header>
    <!--<nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar top-nav-collapse">-->
    <!--   <div class="container">-->
    <!--      <a class="navbar-brand" href="#"><strong>MDB</strong></a>-->
    <!--      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-7" aria-controls="navbarSupportedContent-7" aria-expanded="false" aria-label="Toggle navigation">-->
    <!--      <span class="navbar-toggler-icon"></span>-->
    <!--      </button>-->
    <!--      <div class="collapse navbar-collapse" id="navbarSupportedContent-7">-->
    <!--         <ul class="navbar-nav mr-auto">-->
    <!--            <li class="nav-item active">-->
    <!--               <a class="nav-link waves-effect waves-light" href="#">Home <span class="sr-only">(current)</span></a>-->
    <!--            </li>-->
    <!--            <li class="nav-item">-->
    <!--               <a class="nav-link waves-effect waves-light" href="#">Link</a>-->
    <!--            </li>-->
    <!--            <li class="nav-item">-->
    <!--               <a class="nav-link waves-effect waves-light" href="#">Profile</a>-->
    <!--            </li>-->
    <!--         </ul>-->
    <!--         <form class="form-inline">-->
    <!--            <div class="md-form my-0">-->
    <!--               <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">-->
    <!--            </div>-->
    <!--         </form>-->
    <!--      </div>-->
    <!--   </div>-->
    <!--</nav>-->
    <!-- Intro Section -->

    <section class="view intro-2" style="background-image:url('https://cdn.pixabay.com/photo/2016/10/12/23/23/mining-excavator-1736293_960_720.jpg');">
        <div class="mask rgba-gradient">
            <div class="container h-100 d-flex justify-content-center align-items-center">
                <div class="align-items-center content-height" style="width: 100% !important;">
                    <div class="row flex-center pt-5 mt-3">
                        <div class="col-md-6 text-center text-md-left mb-5">
                            <div class="white-text">
                                <h1 class="h1-responsive font-weight-bold wow fadeInLeft" data-wow-delay="0.3s" style="visibility: visible; animation-name: fadeInLeft; animation-delay: 0.3s;">
                                    <span data-id="{{getKeyid('content.login.header',$data) }}" data-value="{{checkKey('content.login.header',$data) }}" class=" assign_class  label{{getKeyid('content.login.header',$data)}}">
                                        {!! checkKey('content.login.header',$data) !!}
                                    </span>
                                </h1>
                                <hr class="hr-light wow fadeInLeft" data-wow-delay="0.3s" style="visibility: visible; animation-name: fadeInLeft; animation-delay: 0.3s;">
                                <h6 class="wow fadeInLeft" data-wow-delay="0.3s" style="visibility: visible; animation-name: fadeInLeft; animation-delay: 0.3s;">
                                    <span data-id="{{getKeyid('content.login.paragraph',$data) }}" data-value="{{checkKey('content.login.paragraph',$data) }}" class=" assign_class  label{{getKeyid('content.login.paragraph',$data)}}">
                                        {!! checkKey('content.login.paragraph',$data) !!}
                                    </span>
                                </h6>
                                <br>
                                <!--<a class="btn btn-outline-white btn-rounded wow fadeInLeft waves-effect waves-light" data-wow-delay="0.3s" style="visibility: visible; animation-name: fadeInLeft; animation-delay: 0.3s;">-->
                            <!--  <span data-id="{{getKeyid('content.login.button',$data) }}" data-value="{{checkKey('content.login.button',$data) }}" class=" assign_class  label{{getKeyid('content.login.button',$data)}}">-->
                            <!--  {!! checkKey('content.login.button',$data) !!}-->
                                <!--  </span></a>-->
                            </div>
                        </div>
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
    </section>
</header>
<!-- Main Navigation -->

<!-- Main layout -->

<!-- Main layout -->

<!-- Footer -->
<!-- Footer -->

<!-- SCRIPTS -->

<script src="{{ asset('js/Frontend/jquery-3.4.1.min.js') }}"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" rel="stylesheet"/> <link href="https://cdn.jsdelivr.net/sweetalert2/6.4.3/sweetalert2.min.css" rel="stylesheet"/>
<script src="https://cdn.jsdelivr.net/sweetalert2/latest/sweetalert2.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<!-- JQuery -->
<script src="{{ asset('js/Frontend/jquery-3.4.1.min.js') }}"></script>

<!-- Bootstrap tooltips -->
<script src="{{ asset('js/Frontend/popper.min.js') }}"></script>

<!-- Bootstrap core JavaScript -->
<script src="{{ asset('js/Frontend/bootstrap.js') }}"></script>

<!-- MDB core JavaScript -->
<script src="{{ asset('js/Frontend/mdb.min.js') }}"></script>
<script>
    new WOW().init();

</script>
</body>

</html>
