<section class="view intro-2">
    <div class="mask rgba-gradient">
        <div class="container h-100 d-flex justify-content-center align-items-center">
            <div class="align-items-center content-height">
                <div class="row flex-center pt-5 mt-3">
                    <div class="col-md-6 text-center text-md-left mb-5">
                        <div class="white-text">
                            <h1 class="h1-responsive font-weight-bold wow fadeInLeft" data-wow-delay="0.3s">WELCOME
                            </h1>
                            <hr class="hr-light wow fadeInLeft" data-wow-delay="0.3s">
                            <h6 class="wow fadeInLeft" data-wow-delay="0.3s">To Alandale web portal</h6>
                            <br>
                            <a class="btn btn-outline-white btn-rounded wow fadeInLeft" data-wow-delay="0.3s">Learn more</a>
                        </div>
                    </div>

                    <div class="col-md-6 col-xl-5 offset-xl-1">
                        <!-- Form -->
                        <div class="card wow fadeInRight" data-wow-delay="0.3s">
                            <div class="card-body">
                                <!-- Header -->
                                <div class="text-center">
                                    <h3 class="white-text"><i class="fas fa-user white-text"></i> Register:</h3>
                                    <hr class="hr-light">
                                </div>

                                <!-- Body -->
                                <div class="md-form">
                                    <i class="fas fa-user prefix white-text"></i>
                                    <input type="text" id="form3" class="form-control">
                                    <label for="form3" class="white-text">Your name</label>
                                </div>
                                <div class="md-form">
                                    <i class="fas fa-envelope prefix white-text"></i>
                                    <input type="text" id="form2" class="form-control">
                                    <label for="form2" class="white-text">Your email</label>
                                </div>

                                <div class="md-form">
                                    <i class="fas fa-lock prefix white-text"></i>
                                    <input type="password" id="form4" class="form-control">
                                    <label for="form4" class="white-text">Your password</label>
                                </div>

                                <div class="text-center mt-4">
                                    <button class="btn btn-light-blue btn-rounded">Login</button>
                                    <hr class="hr-light mb-3 mt-4">

                                    <div class="inline-ul text-center d-flex justify-content-center">
                                        <a class="p-2 m-2 fa-lg tw-ic"><i class="fab fa-twitter white-text"></i></a>
                                        <a class="p-2 m-2 fa-lg li-ic"><i class="fab fa-linkedin-in white-text"> </i></a>
                                        <a class="p-2 m-2 fa-lg ins-ic"><i class="fab fa-instagram white-text"> </i></a>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- /.Form -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
