@extends('backend.layouts.user')
@section('title', 'Locations')
@section('headscript')
    <script>
        $(document).ready(function() {
            $('#location_group_checkbox_all').click(function() {
                if ($(this).is(':checked')) {
                    $('.location_group_checked').attr('checked', true);
                } else {
                    $('.location_group_checked').attr('checked', false);
                }
            });

            $('#location_checkbox_all').click(function() {
                if ($(this).is(':checked')) {
                    $('.location_checked').attr('checked', true);
                } else {
                    $('.location_checked').attr('checked', false);
                }
            });
        });
    </script>
    <link rel="stylesheet" href="{{ asset('custom/css/custom_style.css') }}">
@stop

@section('content')
    @php
       $data=localization();
    @endphp
    @include('backend.layouts.user_sidebar')
    @include('backend.label.input_label')
    <div class="padding-left-custom remove-padding">
        {!! breadcrumInner('locations',Session::get('employee_id'),'users','/employee/detail/'.Session::get('employee_id'),'employees','/employee') !!}
        <div class="card">
            <div class="card-body">
                <ul  class="nav nav-tabs " id="myTab" role="tablist">
                    @if(Auth()->user()->hasPermissionTo('view_employeeLocationPermision') || Auth::user()->all_companies == 1 )
                        <li class="nav-item">
                            <a class="nav-link active" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile"
                              aria-selected="false">
                                <span class="assign_class label{{getKeyid('location_group',$data)}}" data-id="{{getKeyid('location_group',$data) }}" data-value="{{checkKey('location_group',$data) }}" >
                                    {!! checkKey('location_group',$data) !!}
                                </span>
                            </a>
                        </li>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('view_employeeLocationGroupPermision') || Auth::user()->all_companies == 1 )
                        <li class="nav-item">
                            <a class="nav-link" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">
                                <span class="assign_class label{{getKeyid('location',$data)}}" data-id="{{getKeyid('location',$data) }}" data-value="{{checkKey('location',$data) }}" >
                                    {!! checkKey('location',$data) !!}
                                </span>
                            </a>
                        </li>
                    @endif
                </ul>
                <div class="tab-content" id="myTabContentJust">
                    <div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <div class="table-responsive">
                            <form  action="{{url('location/group/update')}}" method="post">
                                @csrf
                                <table  class="company_table_static table  table-striped table-bordered " cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            @if(Auth()->user()->hasPermissionTo('employeeLocationGroupPermision_checkbox') || Auth::user()->all_companies == 1 )
                                                <th class="no-sort all_checkboxes_style">
                                                    <div class="form-check">
                                                        <input type="checkbox" class="form-check-input" id="location_group_checkbox_all">
                                                        <label class="form-check-label" for="location_group_checkbox_all">
                                                            <span class="assign_class label{{getKeyid('all',$data)}}" data-id="{{getKeyid('all',$data) }}" data-value="{{checkKey('all',$data) }}" >
                                                                {!! checkKey('all',$data) !!}
                                                            </span>
                                                        </label>
                                                    </div>
                                                </th>
                                            @endif
                                            @if(Auth()->user()->hasPermissionTo('employeeLocationGroupPermision_name') || Auth::user()->all_companies == 1 )
                                                <th>
                                                    <span class="assign_class label{{getKeyid('name',$data)}}" data-id="{{getKeyid('name',$data) }}" data-value="{{checkKey('name',$data) }}" >
                                                        {!! checkKey('name',$data) !!}
                                                    </span>
                                                </th>
                                            @endif
                                            <th>
                                                <span class="assign_class label{{getKeyid('location_count',$data)}}" data-id="{{getKeyid('location_count',$data) }}" data-value="{{checkKey('location_count',$data) }}" >
                                                    {!! checkKey('location_count',$data) !!}
                                                </span>

                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($groups as $key=>$value)
                                            <tr>
                                                @if(Auth()->user()->hasPermissionTo('employeeLocationGroupPermision_checkbox') || Auth::user()->all_companies == 1 )
                                                    @if($value->enable == 1)
                                                        <input type="hidden" name="location_group_id[]" value="{{$value->id}}">
                                                    @endif
                                                    <td>
                                                        <div class="form-check">
                                                            <input type="checkbox"
                                                            {{($value->enable == 1) ? 'checked' : '' }}
                                                             class="form-check-input location_group_checked" id="group{{$value->id}}" value="{{$value->id}}" name="group[]">
                                                            <label class="form-check-label" for="group{{$value->id}}">
                                                            </label>
                                                        </div>
                                                    </td>
                                                @endif
                                                @if(Auth()->user()->hasPermissionTo('employeeLocationGroupPermision_name') || Auth::user()->all_companies == 1 )
                                                    <td>{{$value->name}}
                                                    </td>
                                                @endif
                                                <td>
                                                    {{$value->locationcount}}
                                                </td>
                                            </tr>
                                        @endforeach()
                                    </tbody>
                                </table>
                                <button class="form_submit_check btn btn-outline-info btn-rounded btn-block my-4 waves-effect z-depth-0" type="submit">
                                    <span class="assign_class label{{getKeyid('update',$data)}}" data-id="{{getKeyid('update',$data) }}" data-value="{{checkKey('update',$data) }}" >
                                        {!! checkKey('update',$data) !!}
                                    </span>
                                </button>
                            </form>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="table-responsive">
                            <form  action="{{url('location/update')}}" method="post">
                                @csrf
                                <table  class="company_table_static table  table-striped table-bordered " cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            @if(Auth()->user()->hasPermissionTo('employeeLocationPermision_checkbox') || Auth::user()->all_companies == 1 )
                                                <th class="no-sort all_checkboxes_style">
                                                    <div class="form-check">
                                                        <input type="checkbox" class="form-check-input" id="location_checkbox_all">
                                                        <label class="form-check-label" for="location_checkbox_all">
                                                            <span class="assign_class label{{getKeyid('all',$data)}}" data-id="{{getKeyid('all',$data) }}" data-value="{{checkKey('all',$data) }}" >
                                                                {!! checkKey('all',$data) !!}
                                                            </span>
                                                        </label>
                                                    </div>
                                                </th>
                                            @endif
                                            @if(Auth()->user()->hasPermissionTo('employeeLocationPermision_name') || Auth::user()->all_companies == 1 )
                                                <th>
                                                    <span class="assign_class label{{getKeyid('name',$data)}}" data-id="{{getKeyid('name',$data) }}" data-value="{{checkKey('name',$data) }}" >
                                                        {!! checkKey('name',$data) !!}
                                                    </span>
                                                </th>
                                            @endif
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($locations as $key=>$value)
                                            <tr>
                                                @if(Auth()->user()->hasPermissionTo('employeeLocationPermision_checkbox') || Auth::user()->all_companies == 1 )
                                                    <td>
                                                    @if($value->enable == 1)
                                                        <input type="hidden" name="location_id[]" value="{{$value->id}}">
                                                    @endif
                                                        <div class="form-check">
                                                            <input type="checkbox"
                                                            {{($value->enable == 1) ? 'checked' : '' }}
                                                             class="form-check-input location_checked" id="location{{$value->id}}" value="{{$value->id}}" name="location[]">
                                                            <label class="form-check-label" for="location{{$value->id}}">
                                                            </label>
                                                        </div>
                                                    </td>
                                                @endif
                                                @if(Auth()->user()->hasPermissionTo('employeeLocationPermision_name') || Auth::user()->all_companies == 1 )
                                                    <td>{{$value->name}}
                                                    </td>
                                                @endif
                                            </tr>
                                        @endforeach()
                                    </tbody>
                                </table>
                                <button class="form_submit_check btn btn-outline-info btn-rounded btn-block my-4 waves-effect z-depth-0" type="submit">
                                    <span class="assign_class label{{getKeyid('update',$data)}}" data-id="{{getKeyid('update',$data) }}" data-value="{{checkKey('update',$data) }}" >
                                        {!! checkKey('update',$data) !!}
                                    </span>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

