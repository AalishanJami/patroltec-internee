@if(Auth()->user()->all_companies == 1)
    <input type="hidden" id="client_checkbox" value="1">
    <input type="hidden" id="client_name" value="1">
    <input type="hidden" id="client_firstname" value="1">
    <input type="hidden" id="client_surname" value="1">
    <input type="hidden" id="client_phone" value="1">
    <input type="hidden" id="client_email" value="1">
    <input type="hidden" id="client_address" value="1">
    <input type="hidden" id="edit_client" value="1">
    <input type="hidden" id="delete_client" value="1">
    <input type="hidden" id="create_client" value="1">

    <!-------create----------->
    <input type="hidden" id="client_name_create" value="1">
    <input type="hidden" id="client_firstname_create" value="1">
    <input type="hidden" id="client_surname_create" value="1">
    <input type="hidden" id="client_phone_create" value="1">
    <input type="hidden" id="client_email_create" value="1">
    <input type="hidden" id="client_address_create" value="1">

    <!-------edit----------->
    <input type="hidden" id="client_name_edit" value="1">
    <input type="hidden" id="client_firstname_edit" value="1">
    <input type="hidden" id="client_surname_edit" value="1">
    <input type="hidden" id="client_phone_edit" value="1">
    <input type="hidden" id="client_email_edit" value="1">
    <input type="hidden" id="client_address_edit" value="1">
@else
    <input type="hidden" id="client_checkbox" value="{{Auth()->user()->hasPermissionTo('client_checkbox')}}">
    <input type="hidden" id="client_name" value="{{Auth()->user()->hasPermissionTo('client_name')}}">
    <input type="hidden" id="client_firstname" value="{{Auth()->user()->hasPermissionTo('client_firstname')}}">
    <input type="hidden" id="client_surname" value="{{Auth()->user()->hasPermissionTo('client_surname')}}">
    <input type="hidden" id="client_phone" value="{{Auth()->user()->hasPermissionTo('client_phone')}}">
    <input type="hidden" id="client_email" value="{{Auth()->user()->hasPermissionTo('client_email')}}">
    <input type="hidden" id="client_address" value="{{Auth()->user()->hasPermissionTo('client_email')}}">
    <input type="hidden" id="edit_client" value="{{Auth()->user()->hasPermissionTo('edit_client')}}">
    <input type="hidden" id="delete_client" value="{{Auth()->user()->hasPermissionTo('delete_client')}}">
    <input type="hidden" id="create_client" value="{{Auth()->user()->hasPermissionTo('create_client')}}">

    <!-------create----------->
    <input type="hidden" id="client_name_create" value="{{Auth()->user()->hasPermissionTo('client_name_create')}}">
    <input type="hidden" id="client_firstname_create" value="{{Auth()->user()->hasPermissionTo('client_firstname_create')}}">
    <input type="hidden" id="client_surname_create" value="{{Auth()->user()->hasPermissionTo('client_surname_create')}}">
    <input type="hidden" id="client_phone_create" value="{{Auth()->user()->hasPermissionTo('client_phone_create')}}">
    <input type="hidden" id="client_email_create" value="{{Auth()->user()->hasPermissionTo('client_email_create')}}">
    <input type="hidden" id="client_address_create" value="{{Auth()->user()->hasPermissionTo('client_address_create')}}">

    <!-------edit----------->
    <input type="hidden" id="client_name_edit" value="{{Auth()->user()->hasPermissionTo('client_name_edit')}}">
    <input type="hidden" id="client_firstname_edit" value="{{Auth()->user()->hasPermissionTo('client_firstname_edit')}}">
    <input type="hidden" id="client_surname_edit" value="{{Auth()->user()->hasPermissionTo('client_surname_edit')}}">
    <input type="hidden" id="client_phone_edit" value="{{Auth()->user()->hasPermissionTo('client_phone_edit')}}">
    <input type="hidden" id="client_email_edit" value="{{Auth()->user()->hasPermissionTo('client_email_edit')}}">
    <input type="hidden" id="client_address_edit" value="{{Auth()->user()->hasPermissionTo('client_address_edit')}}">
@endif
