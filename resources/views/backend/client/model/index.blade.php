@php
    $data=localization();
@endphp
<div class="modal fade" id="modalClientIndex" tabindex="-1" data-keyboard="false" data-backdrop="static" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">
                    <span class="assign_class label{{getKeyid('client',$data)}}" data-id="{{getKeyid('client',$data) }}" data-value="{{checkKey('client',$data) }}" >
                        {!! checkKey('client',$data) !!}
                    </span>
                    @if(Auth()->user()->hasPermissionTo('view_cms') || Auth::user()->all_companies == 1 )
                        <button data-class="edit-disable" class="edit_cms_enable edit_cms_switch btn btn-primary btn-sm waves-effect waves-light" style="display: none;" >
                            Edit cms
                        </button>
                        <button  data-class="edit-enable" class="edit_cms_disable edit_cms_switch btn btn-danger btn-sm waves-effect waves-light">
                            Edit cms
                        </button>
                    @endif
                </h4>
                <button type="button" class="close client_listing_close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="card">
                <div class="card-body">
                    <div id="table-client" class="table-editable table-responsive">
                        @if(Auth()->user()->hasPermissionTo('create_client') || Auth::user()->all_companies == 1 )
                            <span class="add-client table-add float-right mb-3 mr-2">
                                <a class="text-success"><i class="fas fa-plus fa-2x" aria-hidden="true"></i></a>
                            </span>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_delete_client') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-danger btn-sm" id="deleteselectedclient">
                                <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                                    {!! checkKey('selected_delete',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_delete_client') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-danger btn-sm" style="display: none;" id="deleteselectedclientSoft">
                                <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                                    {!! checkKey('selected_delete',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_restore_delete_client') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-success btn-sm" style="display: none;" id="selectedactivebuttonclient">
                                <span class="assign_class label{{getKeyid('selected_active',$data)}}" data-id="{{getKeyid('selected_active',$data) }}" data-value="{{checkKey('selected_active',$data) }}" >
                                    {!! checkKey('selected_active',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('restore_delete_client') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-primary btn-sm" id="restorebuttonclient">
                                <span class="assign_class label{{getKeyid('restore',$data)}}" data-id="{{getKeyid('restore',$data) }}" data-value="{{checkKey('restore',$data) }}" >
                                    {!! checkKey('restore',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('active_client') || Auth::user()->all_companies == 1 )
                            <button  style="display: none;" class="btn btn-primary btn-sm" id="activebuttonclient">
                                <span class="assign_class label{{getKeyid('show_active',$data)}}" data-id="{{getKeyid('show_active',$data) }}" data-value="{{checkKey('show_active',$data) }}" >
                                    {!! checkKey('show_active',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('csv_client') || Auth::user()->all_companies == 1 )
                            <form class="form-style" id="export_excel_client" method="POST" action="{{ url('client/exportExcel') }}">
                                {!! csrf_field() !!}
                                <input type="hidden" class="client_export" name="excel_array" value="1">
                                <button  type="submit" class="form_submit_check btn btn-warning btn-sm">
                                    <span class="assign_class label{{getKeyid('excel_export',$data)}}" data-id="{{getKeyid('excel_export',$data) }}" data-value="{{checkKey('excel_export',$data) }}" >
                                        {!! checkKey('excel_export',$data) !!}
                                    </span>
                                </button>
                            </form>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('word_client') || Auth::user()->all_companies == 1 )
                            <form class="form-style" id="export_world_client" method="POST" action="{{ url('client/exportWord') }}">
                                {!! csrf_field() !!}
                                <input type="hidden" class="client_export" name="word_array" value="1">
                                <button  type="submit" class="form_submit_check btn btn-success btn-sm">
                                    <span class="assign_class label{{getKeyid('word_export',$data)}}" data-id="{{getKeyid('word_export',$data) }}" data-value="{{checkKey('word_export',$data) }}" >
                                        {!! checkKey('word_export',$data) !!}
                                    </span>
                                </button>
                            </form>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('pdf_client') || Auth::user()->all_companies == 1 )
                            <form  class="form-style i_am_testing_this" {{pdf_view('clients')}} id="export_pdf_client" method="POST" action="{{ url('client/exportPdf') }}">
                                {!! csrf_field() !!}
                                <input type="hidden" class="client_export" name="pdf_array" value="1">
                                <button  type="submit" class="form_submit_check btn btn-secondary btn-sm">
                                    <span class="assign_class label{{getKeyid('pdf_export',$data)}}" data-id="{{getKeyid('pdf_export',$data) }}" data-value="{{checkKey('pdf_export',$data) }}" >
                                        {!! checkKey('pdf_export',$data) !!}
                                    </span>
                                </button>
                            </form>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('view_client') || Auth::user()->all_companies == 1 )
                            <div id="table" class="table-editable table-responsive">
                                <table id="client_table" class="table table-bordered table-responsive-md table-striped">
                                    <thead>
                                    <tr>
                                        <th class="no-sort all_checkboxes_style">
                                            <div class="form-check">
                                                <input type="checkbox" class="form-check-input client_checked" id="client_checkbox_all">
                                                <label class="form-check-label" for="client_checkbox_all">
                                                    <span class="assign_class label{{getKeyid('all',$data)}}" data-id="{{getKeyid('all',$data) }}" data-value="{{checkKey('all',$data) }}" >
                                                        {!! checkKey('all',$data) !!}
                                                    </span>
                                                </label>
                                            </div>
                                        </th>
{{--                                        <th class="no-sort" id="action"></th>--}}
                                        <th>
                                            <span class="assign_class label{{getKeyid('name',$data)}}" data-id="{{getKeyid('name',$data) }}" data-value="{{checkKey('name',$data) }}" >
                                                {!! checkKey('name',$data) !!}
                                            </span>
                                        </th>
                                        <th>
                                            <span class="assign_class label{{getKeyid('surname',$data)}}" data-id="{{getKeyid('surname',$data) }}" data-value="{{checkKey('surname',$data) }}" >
                                                {!! checkKey('surname',$data) !!}
                                            </span>
                                        </th>
                                        <th>
                                            <span class="assign_class label{{getKeyid('phone',$data)}}" data-id="{{getKeyid('phone',$data) }}" data-value="{{checkKey('phone',$data) }}" >
                                                {!! checkKey('phone',$data) !!}
                                            </span>
                                        </th>
                                        <th>
                                            <span class="assign_class label{{getKeyid('email',$data)}}" data-id="{{getKeyid('email',$data) }}" data-value="{{checkKey('email',$data) }}" >
                                                {!! checkKey('email',$data) !!}
                                            </span>
                                        </th>
                                        <th>
                                            <span class="assign_class label{{getKeyid('address',$data)}}" data-id="{{getKeyid('address',$data) }}" data-value="{{checkKey('address',$data) }}" >
                                                {!! checkKey('address',$data) !!}
                                            </span>
                                        </th>
                                        <th class="no-sort all_action_btn" id="action"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        @endif
                    </div>
                    <input type="hidden" id="getAllClientMain" value="1">
                    <input type="hidden" id="main_client" value="1">
                </div>
            </div>
        </div>
    </div>
</div>
@include('backend.client.input_permission')
<script type="text/javascript">
    function client_data(url) {
        var table = $('#client_table').dataTable(
            {
                processing: true,
                language: {
                    'lengthMenu': '  _MENU_ ',
                    'search':'<span class="assign_class label'+$('#search_id').attr('data-id')+'" data-id="'+$('#search_id').attr('data-id')+'" data-value="'+$('#search_id').val()+'" >'+$('#search_id').val()+'</span>',
                    'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                    'info':'',
                    'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                    'paginate': {
                      'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                    'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                  },
                  'infoFiltered': "(filtered from _MAX_ total records)"
                },
                "ajax": {
                    "url": url,
                    "type": 'get',
                },
                "createdRow": function( row, data, dataIndex,columns )
                {
                    var checkbox_permission=$('#checkbox_permission').val();
                    var checkbox='';
                    checkbox+='<div class="form-check"><input type="checkbox" class="form-check-input selectedrowclient client_checked" id="client_checked'+data.id+'"><label class="form-check-label" for="client_checked'+data.id+'""></label></div>';
                    var submit='';
                    submit+='<a type="button" class="btn btn-primary btn-xs my-0 waves-effect all_action_btn_margin waves-light saveclientdata client_show_button_'+data['id']+' show_tick_btn'+data['id']+'" style="display: none;" id="'+data.id+'"><i class="fas fa-check"></i></a>';
                    if($("#client_checkbox").val() =='1'){
                        $(columns[0]).html(checkbox);
                    }
                    $(columns[1]).attr('id', 'first_name'+data['id']);
                    if($("#client_name_edit").val() =='1' && $("#edit_client").val() =='1'){
                        $(columns[1]).attr('Contenteditable', 'true');
                    }
                    $(columns[1]).attr('class','edit_inline_client');
                    $(columns[1]).attr('data-id', data['id']);
                    $(columns[1]).attr('onkeydown', 'editSelectedRow(client_tr'+data['id']+')');
                    $(columns[2]).attr('id','surname'+data['id']);
                    $(columns[2]).attr('class','edit_inline_client');
                    if($("#client_surname_edit").val() =='1' && $("#edit_client").val() =='1'){
                        $(columns[2]).attr('Contenteditable', 'true');
                    }
                    $(columns[2]).attr('data-id', data['id']);
                    $(columns[2]).attr('onkeydown', 'editSelectedRow(client_tr'+data['id']+')');
                    $(columns[3]).attr('onkeydown', 'editSelectedRow(client_tr'+data['id']+')');
                    $(columns[3]).attr('id', 'phone_number'+data['id']);
                    $(columns[3]).attr('class','edit_inline_client');
                    if($("#client_phone_edit").val() =='1' && $("#edit_client").val() =='1'){
                        $(columns[3]).attr('Contenteditable', 'true');
                    }
                    $(columns[3]).attr('data-id', data['id']);

                    $(columns[4]).attr('id', 'email'+data['id']);
                    $(columns[4]).attr('onkeydown', 'editSelectedRow(client_tr'+data['id']+')');
                    $(columns[4]).attr('class','edit_inline_client');
                    if($("#client_email_edit").val() =='1' && $("#edit_client").val() =='1'){
                        $(columns[4]).attr('Contenteditable', 'true');
                    }
                    $(columns[4]).attr('data-id', data['id']);
                    $(columns[5]).attr('id', 'address'+data['id']);
                    $(columns[5]).attr('onkeydown', 'editSelectedRow(client_tr'+data['id']+')');
                    $(columns[5]).attr('class','edit_inline_client');
                    if($("#client_address_edit").val() =='1' && $("#edit_client").val() =='1'){
                        $(columns[5]).attr('Contenteditable', 'true');
                    }
                    $(columns[5]).attr('data-id', data['id']);
                    if ($('#edit_client').val()){
                        $(columns[6]).append(submit);
                    }
                    $(row).attr('id', 'client_tr'+data['id']);

                   // $(row).attr('class', 'selectedrowclient');
                    $(row).attr('data-id', data['id']);
                    var temp=data['id'];
                },
                columns:
                    [
                        {data: 'checkbox', name: 'checkbox'},
                        {data: 'first_name', name: 'first_name',visible:$('#client_firstname').val()},
                        {data: 'surname', name: 'surname',visible:$('#client_surname').val()},
                        {data: 'phone_number', name: 'phone_number',visible:$('#client_phone').val()},
                        {data: 'email', name: 'email',visible:$('#client_email').val()},
                        {data: 'address', name: 'address',visible:$('#client_address').val()},
                        {data: 'actions', name: 'actions'},
                    ],
                "columnDefs": [ {
                    "targets": [0,1],
                    "orderable": false
                } ]

            }
        );
        if ($(":checkbox").prop('checked',true)){
            $(":checkbox").prop('checked',false);
        }
    }
    $(document).ready(function () {

        var url='/client/getall';
        client_data(url);
        // $("body").on('click','.edit_inline_client',function () {
        //     $(".client_show_button_"+$(this).attr('data-id')).show();
        //     $('.deleteclientdata'+$(this).attr('data-id')).hide();
        // });
        $('.client_listing_close').click(function(){
            $.ajax({
                type: "GET",
                url: '/client/getall',
                success: function(response)
                {
                    var html='';
                    var client_id=$("#client_id").children('option:selected').val();
                    for (var i = response.data.length - 1; i >= 0; i--) {
                        var selected='';
                        if (client_id==response.data[i].id){
                            selected='selected';
                        }
                        html+='<option '+selected+' value="'+response.data[i].id+'">'+response.data[i].first_name+'</option>';
                    }
                    if (client_id==''){
                        html+='<option disabled selected value="">Please Select</option>';
                    }else{
                        html+='<option disabled value="">Please Select</option>';
                    }
                    $('#client_id').empty();
                    $('#client_id').html(html);

                },
                error: function (error) {
                    console.log(error);
                }
            });

        });
    });
</script>
