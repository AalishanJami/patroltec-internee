@php
    $data=localization();
@endphp
<style type="text/css">
    .ml-4-5 {
        margin-left: 2.5rem !important;
    }
    #answer_data>tr>td:nth-child(6){
        width: 36% !important;
        text-align: center;
    }
    #answer_data>tr>td:nth-child(5){
        width: 17% !important;
    }
</style>
<div class="modal fade" id="modalAnswertablelist" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">
                  <span class="assign_class label{{getKeyid('answer',$data)}}" data-id="{{getKeyid('answer',$data) }}" data-value="{{checkKey('answer',$data) }}" >
                      {!! checkKey('answer',$data) !!}
                  </span>
                    @if(Auth()->user()->hasPermissionTo('view_cms') || Auth::user()->all_companies == 1 )
                        <button data-class="edit-disable" class="edit_cms_enable edit_cms_switch btn btn-primary btn-sm waves-effect waves-light" style="display: none;" >
                            Edit cms
                        </button>
                        <button  data-class="edit-enable" class="edit_cms_disable edit_cms_switch btn btn-danger btn-sm waves-effect waves-light">
                            Edit cms
                        </button>
                    @endif
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="card">
                <div class="card-body">
                    <div id="table-answer" class="table-editable">
                  <span class="add-answer float-right mb-3 mr-2"><a class="text-success"><i
                              class="fas fa-plus fa-2x" aria-hidden="true"></i></a></span>
                        @if(Auth()->user()->hasPermissionTo('selected_delete_document_answer') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-danger btn-sm" id="deleteselectedanswer">
                        <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                            {!! checkKey('selected_delete',$data) !!}
                        </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_delete_document_answer') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-danger btn-sm" style="display: none;" id="deleteselectedanswerSoft">
                          <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                              {!! checkKey('selected_delete',$data) !!}
                          </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_active_button_document_answer') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-success btn-sm" style="display: none;" id="selectedactivebuttonanswer">
                          <span class="assign_class label{{getKeyid('selected_active',$data)}}" data-id="{{getKeyid('selected_active',$data) }}" data-value="{{checkKey('selected_active',$data) }}" >
                              {!! checkKey('selected_active',$data) !!}
                          </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('restore_delete_document_answer') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-primary btn-sm" id="restorebuttonanswer">
                          <span class="assign_class label{{getKeyid('restore',$data)}}" data-id="{{getKeyid('restore',$data) }}" data-value="{{checkKey('restore',$data) }}" >
                              {!! checkKey('restore',$data) !!}
                          </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('active_document_answer') || Auth::user()->all_companies == 1 )
                            <button  style="display: none;" class="btn btn-primary btn-sm" id="activebuttonanswer">
                          <span class="assign_class label{{getKeyid('show_active',$data)}}" data-id="{{getKeyid('show_active',$data) }}" data-value="{{checkKey('show_active',$data) }}" >
                              {!! checkKey('show_active',$data) !!}
                          </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('csv_document_answer') || Auth::user()->all_companies == 1 )
                            <form class="form-style" method="POST" id="export_excel_answer" action="{{ url('answer/export/excel') }}">
                                {!! csrf_field() !!}
                                <input type="hidden" class="answer_export" name="excel_array" value="1">
                                <button  type="submit" class="form_submit_check btn btn-warning btn-sm">
                            <span class="assign_class label{{getKeyid('excel_export',$data)}}" data-id="{{getKeyid('excel_export',$data) }}" data-value="{{checkKey('excel_export',$data) }}" >
                              {!! checkKey('excel_export',$data) !!}
                            </span>
                                </button>
                            </form>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('word_document_answer') || Auth::user()->all_companies == 1 )
                            <form class="form-style" method="POST" id="export_world_answer" action="{{ url('answer/export/world') }}">
                                <input type="hidden" class="answer_export" name="word_array" value="1">
                                {!! csrf_field() !!}
                                <button  type="submit" class="form_submit_check btn btn-success btn-sm">
                            <span class="assign_class label{{getKeyid('word_export',$data)}}" data-id="{{getKeyid('word_export',$data) }}" data-value="{{checkKey('word_export',$data) }}" >
                              {!! checkKey('word_export',$data) !!}
                            </span>
                                </button>
                            </form>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('pdf_document_answer') || Auth::user()->all_companies == 1 )
                            <form  class="form-style" {{pdf_view('answers')}} method="POST" id="export_pdf_answer" action="{{ url('answer/export/pdf') }}">
                                <input type="hidden" class="answer_export" name="pdf_array" value="1">
                                {!! csrf_field() !!}
                                <button  type="submit" class="form_submit_check btn btn-secondary btn-sm">
                            <span class="assign_class label{{getKeyid('pdf_export',$data)}}" data-id="{{getKeyid('pdf_export',$data) }}" data-value="{{checkKey('pdf_export',$data) }}" >
                              {!! checkKey('pdf_export',$data) !!}
                            </span>
                                </button>
                            </form>
                        @endif
                        <div class="table-responsive">
                            <table id="answer_table" class="answer_table table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th class="text-left no-sort all_checkboxes_style" >
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input answer_checked" id="answer_checkbox_all">
                                            <label class="form-check-label" for="answer_checkbox_all">
                                        <span class="assign_class label{{getKeyid('all',$data)}}" data-id="{{getKeyid('all',$data) }}" data-value="{{checkKey('all',$data) }}" >
                                            {!! checkKey('all',$data) !!}
                                        </span>
                                            </label>
                                        </div>
                                    </th>
                                    <th class="text-left no-sort" ></th>
                                    <th class="text-left all_name_style" >
                                  <span class="assign_class label{{getKeyid('name',$data)}}" data-id="{{getKeyid('name',$data) }}" data-value="{{checkKey('name',$data) }}" >
                                    {!! checkKey('name',$data) !!}
                                </span>
                                    </th>
                                    <th class="text-left">
                                  <span class="assign_class label{{getKeyid('grade',$data)}}" data-id="{{getKeyid('grade',$data) }}" data-value="{{checkKey('grade',$data) }}" >
                                    {!! checkKey('grade',$data) !!}
                                  </span>
                                    </th>
                                    <th class="text-left">
                                        <span class="assign_class label{{getKeyid('score',$data)}}" data-id="{{getKeyid('score',$data) }}" data-value="{{checkKey('score',$data) }}" >{!! checkKey('score',$data) !!}</span>
                                    </th>
                                    <th class="text-left">
                                        <span class="assign_class label{{getKeyid('upload',$data)}}" data-id="{{getKeyid('upload',$data) }}" data-value="{{checkKey('upload',$data) }}" >{!! checkKey('upload',$data) !!}</span>
                                    </th>
                                    <th class="text-left no-sort"></th>
                                </tr>
                                </thead>
                                <tbody id="answer_data"></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- answer Group  -->
@if(Auth()->user()->all_companies == 1)
    <!-- listing -->
    <input type="hidden" id="document_answer_checkbox" value="1">
    <input type="hidden" id="document_answer_name" value="1">
    <input type="hidden" id="document_answer_grade" value="1">
    <input type="hidden" id="document_answer_score" value="1">
    <input type="hidden" id="document_answer_image" value="1">
    <!-- create -->
    <input type="hidden" id="document_answer_name_create" value="1">
    <input type="hidden" id="document_answer_grade_create" value="1">
    <input type="hidden" id="document_answer_score_create" value="1">
    <!-- edit -->
    <input type="hidden" id="document_answer_name_edit" value="1">
    <input type="hidden" id="document_answer_grade_edit" value="1">
    <input type="hidden" id="document_answer_score_edit" value="1">
    <!-- edit permision -->
    <input type="hidden" id="edit_permision_document_answer" value="1">
    <!-- upload permision -->
    <input type="hidden" id="upload_permision_document_answer" value="1">
@else
    <!-- listing -->
    <input type="hidden" id="document_answer_checkbox" value="{{Auth()->user()->hasPermissionTo('document_answer_checkbox')}}">
    <input type="hidden" id="document_answer_name" value="{{Auth()->user()->hasPermissionTo('document_answer_name')}}">
    <input type="hidden" id="document_answer_grade" value="{{Auth()->user()->hasPermissionTo('document_answer_grade')}}">
    <input type="hidden" id="document_answer_score" value="{{Auth()->user()->hasPermissionTo('document_answer_score')}}">
    <input type="hidden" id="document_answer_image" value="{{Auth()->user()->hasPermissionTo('document_answer_image')}}">
    <!-- create -->
    <input type="hidden" id="document_answer_name_create" value="{{Auth()->user()->hasPermissionTo('document_answer_name_create')}}">
    <input type="hidden" id="document_answer_grade_create" value="{{Auth()->user()->hasPermissionTo('document_answer_grade_create')}}">
    <input type="hidden" id="document_answer_score_create" value="{{Auth()->user()->hasPermissionTo('document_answer_score_create')}}">
    <!-- edit -->
    <input type="hidden" id="document_answer_name_edit" value="{{Auth()->user()->hasPermissionTo('document_answer_name_edit')}}">
    <input type="hidden" id="document_answer_grade_edit" value="{{Auth()->user()->hasPermissionTo('document_answer_grade_edit')}}">
    <input type="hidden" id="document_answer_score_edit" value="{{Auth()->user()->hasPermissionTo('document_answer_score_edit')}}">
    <!-- edit permision -->
    <input type="hidden" id="edit_permision_document_answer" value="{{Auth()->user()->hasPermissionTo('edit_document_answer')}}">
    <!-- upload permision -->
    <input type="hidden" id="upload_permision_document_answer" value="{{Auth()->user()->hasPermissionTo('upload_document_answer')}}">

@endif

@if(!empty(Session::get('error_code')) && Session::get('error_code') == 5)
    <script>
        $(document).ready(function(){
            $(function() {
                var i = '{{Session::get('answer_id_redirect')}}';
                sessionStorage.setItem('answer_id', i);
                $('#modalAnswertablelist').modal('show');
                var url='/answer/getall/'+i;
                answer_data(url);
            });
            $("body").on("focusin",".edit_inline_answer",function(){
                $("#answer_tr"+$(this).attr('data-id')).addClass('selected');
            });
        });
    </script>
@endif
<script type="text/javascript">
    function checkSelected(value,checkValue)
    {
        if(value == checkValue)
        {
            return 'selected';
        }
        else
        {
            return "";
        }

    }
    function uploadAnswerImage(formid,id){
        if($('#upload_permision_document_answer').val())
        {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: '{{url('answer/uploadIcon')}}',
                data:new FormData(document.getElementById(formid)),
                // dataType:'JSON',
                contentType:false,
                cache:false,
                processData:false,
                success: function(data)
                {
                    // toastr["success"]('uploaded successfully');
                    if(data.flag != 1)
                    {
                        if(data.data.icon!=null)
                        {

                            var html= '<img src="{{asset("/uploads/icons")}}/'+data.data.icon+'"  width="50" height="50" alt="No Icon">';
                        }else{
                            var html='';
                        }
                        $("#image_data"+id).html(html);

                    }
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
    }
    function answer_data(url)
    {
        var table = $('.answer_table').dataTable(
            {
                processing: true,
                language: {
                    'lengthMenu': '  _MENU_ ',
                    'search':'<span class="assign_class label'+$('#search_id').attr('data-id')+'" data-id="'+$('#search_id').attr('data-id')+'" data-value="'+$('#search_id').val()+'" >'+$('#search_id').val()+'</span>',
                    'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                    'info':'',
                    'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                    'paginate': {
                        'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                        'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                    },
                    'infoFiltered': "(filtered from _MAX_ total records)"
                },
                "ajax": {
                    "url": url,
                    "type": 'get',
                },
                "createdRow": function( row, data, dataIndex,columns )
                {
                    var checkbox='';
                    checkbox+='<div class="form-check"><input type="checkbox" class="form-check-input selectedrowanswer" id="answer'+data.id+'"><label class="form-check-label" for="answer'+data.id+'""></label></div>';
                    var upload_file='';
                    upload_file+='<div style=" display: flex;flex-direction: row; flex-wrap: nowrap; "><form  method="post"  class="mr-2" id="form_upload_icon'+data.id+'" enctype="multipart/form-data" action="/answer/uploadIcon" > <input type="hidden"  name="_token" id="csrf-token" value="{{csrf_token()}}" /> <div style=" display: flex;flex-direction: row; flex-wrap: nowrap; "><input type="file" onchange="editSelectedRow('+"answer_tr"+data.id+')" name="file" style=" border: 1px solid #ccc;display: inline-block;padding: 6px 12px;cursor: pointer;" ><input type="hidden" value="'+data.id+'" name="answer_id"></form></div>';
                    $(columns[5]).html(upload_file);
                    $(columns[0]).html(checkbox);
                    var image='';
                    if(data.icon!=null)
                    {
                        image='<img src="{{asset("/uploads/icons")}}/'+data.icon+'"  width="50" height="50" alt="No Icon">';
                    }
                    $(columns[1]).html(image);
                    $(columns[1]).attr('id', 'image_data'+data['id']);

                    if($('#edit_permision_document_answer').val() && $('#document_answer_name_edit').val())
                    {
                        $(columns[2]).attr('Contenteditable', 'true');
                        $(columns[2]).attr('style', 'text-transform: capitalize;');
                        $(columns[2]).attr('id', 'answer_name'+data['id']);
                        $(columns[2]).attr('data-id', data['id']);
                    }
                    $(columns[2]).attr('onkeydown', 'editSelectedRow(answer_tr'+data['id']+')');
                    $(columns[2]).attr('class','edit_inline_answer');

                    var grade_disable='';
                    if(! $('#edit_permision_document_answer').val() || !$('#document_answer_grade_edit').val())
                    {
                        grade_disable='disabled';
                    }
                    var grade= '<select '+grade_disable+' onchange="editSelectedRow('+"answer_tr"+data.id+')" searchable="Search here.."  id="answer_grade'+data.id+'"  style="background-color: inherit;border: 0px"><option value="1" '+checkSelected("1",data.grade)+'>Pass</option><option value="0" '+checkSelected("0",data.grade)+'>Fail</option><option value="2" '+checkSelected("2",data.grade)+'>Ignore</option></td>';
                    $(columns[3]).attr('class','edit_inline_answer');
                    $(columns[3]).html(grade);
                    $(columns[3]).attr('data-id', data['id']);

                    if($('#edit_permision_document_answer').val() && $('#document_answer_score_edit').val())
                    {
                        $(columns[4]).attr('Contenteditable', 'true');
                        $(columns[4]).attr('id', 'answer_score'+data['id']);
                    }
                    $(columns[4]).attr('onkeydown', 'editSelectedRow(answer_tr'+data['id']+')');
                    $(columns[4]).attr('data-id', data['id']);
                    $(columns[4]).attr('class','edit_inline_answer');
                    $(row).attr('id', 'answer_tr'+data['id']);
                    //$(row).attr('class', 'selectedrowanswer');
                    $(row).attr('data-id', data['id']);
                },
                columns:
                    [
                        {data: 'checkbox', name: 'checkbox',visible:$('#document_answer_checkbox').val()},
                        {data: 'image', name: 'image',visible:$('#document_answer_image').val()},
                        {data: 'answer', name: 'answer',visible:$('#document_answer_name').val()},
                        {data: 'grade', name: 'grade',visible:$('#document_answer_grade').val()},
                        {data: 'score', name: 'score',visible:$('#document_answer_score').val()},
                        {data: 'upload', name: 'upload'},
                        {data: 'actions', name: 'actions'},
                    ],
            }
        );
        if ($(":checkbox").prop('checked',true)){
            $(":checkbox").prop('checked',false)
        }
    }
    $("body").on('keypress','.edit_inline_answer_group',function(){
        $(this).attr('style','text-transform: capitalize;')
    });
</script>
