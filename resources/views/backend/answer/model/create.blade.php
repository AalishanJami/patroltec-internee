@php
        $data=localization();
@endphp
<style type="text/css">
      .ml-4-5 {
            margin-left: 2.5rem !important;
        }
</style>
<div class="modal fade" id="modalAnswerforms" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Answer Edit</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="registernewcompany" method="POST" >
                @csrf
                <div class="modal-body mx-3">

                    
                     

                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                      <!--   <input type="file" class="form-control validate @error('name') is-invalid @enderror" name="name" value="Choice" required autocomplete="name" autofocus>
                        <label data-error="wrong" data-success="right" for="name" class="active">
                            Add Icon
                         
                        </label> -->
                            <div class="md-form  ml-4-5 ">
                                <div class="file-field">
                                      <div class="btn btn-primary btn-sm float-right">
                                         <span>Choose file  </span>
                                         <input type="file">
                                      </div>
                                      <div class="file-path-wrapper">
                                         <input class="file-path validate" type="text" placeholder="Upload your file">
                                      </div>
                                </div>
                            </div>

                    </div>
                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="Choice" required autocomplete="name" autofocus>
                        <label data-error="wrong" data-success="right" for="name" class="active">
                            Answer
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                            data-content="tool tip for Answer"></i>
                        </label>
                    </div>
                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input type="number" class="form-control validate @error('name') is-invalid @enderror" name="name" value="Order" required autocomplete="name" autofocus>
                        <label data-error="wrong" data-success="right" for="name" class="active">
                            Order
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                            data-content="tool tip for Order"></i>
                        </label>
                    </div>
                    <div class="md-form mb-5">
                        <div class="md-form">
                            <i class="fas fa-user prefix grey-text"></i>
                            <div class="md-form  ml-4-5 ">
                                <select searchable="Search here.."  class="mdb-select">
                                    <option value="1" selected>Fail</option>
                                      <option value="2">Pass</option>
                                      <option value="3">Ignore</option>
                                </select>
                            </div>
                            <label data-error="wrong" data-success="right" for="name" class="active">
                                 Grade<i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                            data-content="tool tip for Score"></i>
                            </label>
                        </div>
                    </div> 


                
                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input type="number" class="form-control validate @error('name') is-invalid @enderror" name="name" value="Score" required autocomplete="name" autofocus>
                        <label data-error="wrong" data-success="right" for="name" class="active">
                            Score
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                            data-content="tool tip for Score"></i>
                        </label>
                    </div>
                    

                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                         <i class="fas fa-user prefix grey-text"></i> 
                          <textarea id="form7" class="md-textarea form-control" rows="3"></textarea>
                          <label for="form7">Additional Comments</label>
                    </div>

              

                <div class="modal-footer d-flex justify-content-center">
                    <button class="btn btn-primary btn-sm" type="submit"><span class="assign_class label{{getKeyid('save',$data)}}" data-id="{{getKeyid('save',$data) }}" data-value="{{checkKey('save',$data) }}" >{!! checkKey('save',$data) !!} </span></button>
                </div>
            </form>
        </div>
    </div>
</div>
