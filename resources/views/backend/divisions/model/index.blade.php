@php
    $data=localization();
@endphp

<div class="modal fade" id="modalDevisionsIndex" tabindex="-1" data-keyboard="false" data-backdrop="static" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">
                    <span class="assign_class label{{getKeyid('devision',$data)}}" data-id="{{getKeyid('devision',$data) }}" data-value="{{checkKey('devision',$data) }}" >
                        {!! checkKey('devision',$data) !!}
                    </span>
                    @if(Auth()->user()->hasPermissionTo('view_cms') || Auth::user()->all_companies == 1 )
                        <button data-class="edit-disable" class="edit_cms_enable edit_cms_switch btn btn-primary btn-sm waves-effect waves-light" style="display: none;" >
                            Edit cms
                        </button>
                        <button  data-class="edit-enable" class="edit_cms_disable edit_cms_switch btn btn-danger btn-sm waves-effect waves-light">
                            Edit cms
                        </button>
                    @endif
                </h4>
                <button type="button" class="close division_listing_close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="card">
                <div class="card-body">
                    <div id="table-devision" class="table-editable">
                        @if(Auth()->user()->hasPermissionTo('create_division') || Auth::user()->all_companies == 1 )
                            <span class="add-devision table-add float-right mb-3 mr-2">
                                <a class="text-success"><i class="fas fa-plus fa-2x" aria-hidden="true"></i></a>
                            </span>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_delete_division') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-danger btn-sm" id="deleteselecteddevision">
                                <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                                    {!! checkKey('selected_delete',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_delete_division') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-danger btn-sm" style="display: none;" id="deleteselecteddevisionSoft">
                                <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                                    {!! checkKey('selected_delete',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_active_division') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-success btn-sm" style="display: none;" id="selectedactivebuttondevision">
                                <span class="assign_class label{{getKeyid('selected_active',$data)}}" data-id="{{getKeyid('selected_active',$data) }}" data-value="{{checkKey('selected_active',$data) }}" >
                                    {!! checkKey('selected_active',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('restore_delete_division') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-primary btn-sm" id="restorebuttondevision">
                                <span class="assign_class label{{getKeyid('restore',$data)}}" data-id="{{getKeyid('restore',$data) }}" data-value="{{checkKey('restore',$data) }}" >
                                    {!! checkKey('restore',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('active_division') || Auth::user()->all_companies == 1 )
                            <button  style="display: none;" class="btn btn-primary btn-sm" id="activebuttondevision">
                               <span class="assign_class label{{getKeyid('show_active',$data)}}" data-id="{{getKeyid('show_active',$data) }}" data-value="{{checkKey('show_active',$data) }}" >
                                    {!! checkKey('show_active',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('csv_division') || Auth::user()->all_companies == 1 )
                        <form class="form-style" method="POST" id="export_excel_devision" action="{{ url('devision/export/excel') }}">
                            {!! csrf_field() !!}
                            <input type="hidden" class="devision_export" name="excel_array" value="1">
                            <button  type="submit" class="form_submit_check btn btn-warning btn-sm">
                                <span class="assign_class label{{getKeyid('excel_export',$data)}}" data-id="{{getKeyid('excel_export',$data) }}" data-value="{{checkKey('excel_export',$data) }}" >
                                    {!! checkKey('excel_export',$data) !!}
                                </span>
                            </button>
                        </form>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('word_division') || Auth::user()->all_companies == 1 )
                         <form class="form-style" method="POST" id="export_world_devision" action="{{ url('devision/export/world') }}">
                            {!! csrf_field() !!}
                            <input type="hidden" class="devision_export" name="word_array" value="1">
                            <button  type="submit" class="form_submit_check btn btn-success btn-sm">
                                <span class="assign_class label{{getKeyid('word_export',$data)}}" data-id="{{getKeyid('word_export',$data) }}" data-value="{{checkKey('word_export',$data) }}" >
                                    {!! checkKey('word_export',$data) !!}
                                </span>
                            </button>
                        </form>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('pdf_division') || Auth::user()->all_companies == 1 )
                        <form  class="form-style" {{pdf_view('divisions')}} method="POST" id="export_pdf_devision" action="{{ url('devision/export/pdf') }}">
                            {!! csrf_field() !!}
                            <input type="hidden" class="devision_export" name="pdf_array" value="1">
                            <button  type="submit" class="form_submit_check btn btn-secondary btn-sm">
                                <span class="assign_class label{{getKeyid('pdf_export',$data)}}" data-id="{{getKeyid('pdf_export',$data) }}" data-value="{{checkKey('pdf_export',$data) }}" >
                                    {!! checkKey('pdf_export',$data) !!}
                                </span>
                            </button>
                        </form>
                        @endif
                        <div id="table" class="table-editable">
                            <table id="devision_table" class=" table table-bordered table-responsive-md table-striped">
                                <thead>
                                <tr>
                                    @if(Auth()->user()->hasPermissionTo('division_checkbox') || Auth::user()->all_companies == 1 )
                                        <th class="no-sort all_checkboxes_style">
                                            <div class="form-check">
                                                <input type="checkbox" class="form-check-input division_checked" id="division_checkbox_all">
                                                <label class="form-check-label" for="division_checkbox_all">
                                                    <span class="assign_class label{{getKeyid('all',$data)}}" data-id="{{getKeyid('all',$data) }}" data-value="{{checkKey('all',$data) }}" >
                                                        {!! checkKey('all',$data) !!}
                                                    </span>
                                                </label>
                                            </div>
                                        </th>
                                    @endif
                                    @if(Auth()->user()->hasPermissionTo('division_name') || Auth::user()->all_companies == 1 )
                                        <th class="all_name_style">
                                            <span class="assign_class label{{getKeyid('name',$data)}}" data-id="{{getKeyid('name',$data) }}" data-value="{{checkKey('name',$data) }}" >
                                                {!! checkKey('name',$data) !!}
                                            </span>
                                        </th>
                                    @endif
                                    <th class="no-sort all_action_btn" id="action"></th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        @if(Auth()->user()->all_companies == 1)
                            <input type="hidden" id="division_checkbox" value="1">
                            <input type="hidden" id="delete_division" value="1">
                            <input type="hidden" id="hard_delete_division" value="1">
                            <input type="hidden" id="division_name" value="1">
                            <input type="hidden" id="edit_division" value="1">
                            <input type="hidden" id="division_name_edit" value="1">
                            <input type="hidden" id="division_name_create" value="1">
                        @else
                            <input type="hidden" id="division_checkbox" value="{{Auth()->user()->hasPermissionTo('division_checkbox')}}">
                            <input type="hidden" id="delete_division" value="{{Auth()->user()->hasPermissionTo('delete_division')}}">
                            <input type="hidden" id="hard_delete_division" value="{{Auth()->user()->hasPermissionTo('hard_delete_division')}}">
                            <input type="hidden" id="division_name" value="{{Auth()->user()->hasPermissionTo('division_name')}}">
                            <input type="hidden" id="edit_division" value="{{Auth()->user()->hasPermissionTo('edit_division')}}">
                            <input type="hidden" id="division_name_edit" value="{{Auth()->user()->hasPermissionTo('division_name_edit')}}">
                            <input type="hidden" id="division_name_create" value="{{Auth()->user()->hasPermissionTo('division_name_create')}}">
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function devision_data(url) {
        $.ajax({
            type: 'GET',
            url: url,
            success: function(response)
            {
                $('#devision_table').DataTable().clear().destroy();
                var table = $('#devision_table').dataTable(
                {
                    processing: true,
                    language: {
                    'search':'<span class="assign_class label'+$('#search_id').attr('data-id')+'" data-id="'+$('#search_id').attr('data-id')+'" data-value="'+$('#search_id').val()+'" >'+$('#search_id').val()+'</span>',
                    'paginate':{
                            'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                            'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                        },
                    'lengthMenu': '_MENU_',
                    'info': ' ',
                    },
                    "ajax": {
                        "url": url,
                        "type": 'get',
                    },
                    "createdRow": function( row, data,dataIndex,columns )
                    {
                        var checkbox_permission=$('#checkbox_permission').val();
                        var checkbox='';
                        checkbox+='<div class="form-check"><input type="checkbox" class="form-check-input selectedrowdevision devision_checked" id="devision'+data.id+'"><label class="form-check-label" for="devision'+data.id+'""></label></div>';
                        var submit='';
                        submit+='<a type="button" class="btn btn-primary btn-xs my-0 all_action_btn_margin waves-effect waves-light savedivisiondata devision_show_button_'+data.id+' show_tick_btn'+data.id+'" style="display: none;" id="'+data.id+'"><i class="fas fa-check"></i></a>';
                        $(columns[0]).html(checkbox);
                        $(columns[1]).attr('id', 'nameD'+data['id']);
                        $(columns[1]).attr('data-id', data['id']);
                        $(columns[1]).attr('onkeydown', 'editSelectedRow(devision_tr'+data['id']+')');
                        if ($('#division_name_edit').val()==1){
                            $(columns[1]).attr('Contenteditable', 'true');
                        }
                        if($('#edit_division').val()==1) {
                            $(columns[2]).append(submit);
                        }
                        $(columns[1]).attr('class','edit_inline_devision');
                        $(row).attr('id', 'devision_tr'+data['id']);
                        //$(row).attr('class', 'selectedrowdevision');
                        $(row).attr('data-id', data['id']);
                    },
                    columns:
                    [
                      {data: 'checkbox', name: 'checkbox',visible:$('#division_checkbox').val()},
                      {data: 'name', name: 'name',visible:$('#division_name').val()},
                      {data: 'actions', name: 'actions',},
                    ],

                  }
                );
            },
            error: function (error) {
              console.log(error);
            }
        });
        if ($(":checkbox").prop('checked',true)){
            $(":checkbox").prop('checked',false);
        }
      }
    $(document).ready(function () {
        var url='/devision/getall';
        devision_data(url);

        $('.division_listing_close').click(function(){
            $.ajax({
                type: "GET",
                url: '/devision/getall',
                success: function(response)
                {
                    var html='';
                    console.log(response);
                    var division_id=$("#division_id").children('option:selected').val();
                    for (var i = response.data.length - 1; i >= 0; i--) {
                        var selected='';
                        if(response.project.division_id == response.data[i].id)
                        {
                            selected='selected';
                        }
                        html+='<option '+selected+' value="'+response.data[i].id+'">'+response.data[i].name+'</option>';
                    }
                    if (division_id==''){
                        html+='<option selected disabled value="">---Please Select---</option>';
                    }else{
                        html+='<option disabled value="">---Please Select---</option>';
                    }
                    $('#division_id').empty();
                    $('#division_id').html(html);
                },
                error: function (error) {
                    console.log(error);
                }
            });

        });
        $('body').on('change','#division_id',function(){
            var division_id=$('#division_id').children("option:selected").val();
            $.ajax({
                type: "GET",
                url: '/customer/getall/bydivision',
                data: {
                    'division_id': division_id
                },
                success: function(response)
                {
                    var html='';
                    html+='<option selected disabled value="">---Please Select---</option>';
                    if(response.data.length<=0){
                        html+='<option value="">No Customer Found ....</option>';
                    }
                    for (var i = response.data.length - 1; i >= 0; i--) {
                        html+='<option value="'+response.data[i].id+'">'+response.data[i].name+'</option>';
                    }
                    $('#customer_id').html(html);
                },
                error: function (error) {
                    console.log(error);
                }
            });

        });
    });
</script>
