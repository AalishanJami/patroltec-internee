@extends('backend.form_export.layout')
@section('content')
    @php
        $data=localization();
    @endphp
    <style type="text/css">
        .nav-select{
            margin-left: 26rem !important;
            width: 20%;
            margin-top: 2px;
            margin-bottom: -15px;
        }
        .white-color
        {
            color:#fff;
        }
        #pdf-viewer,#pdf-viewer-static
        {
            height: 700px;
        }

        .form_alignment{
            width: 96%;
            margin: auto !important;
        }

        .radio_checkbox_label{
            width: 100px !important;
            text-align: left !important;
            /*overflow: hidden !important;*/
            /*text-overflow: ellipsis !important;*/
        }
        .row {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            margin-right: -15px;
            margin-left: -15px;
        }
        .mb-4,
        .my-4 {
            margin-bottom: 1.5rem !important;
        }
        .custom_dynamic_height>div>div>div>div{
            padding-bottom: 6px !important;
            padding-top: 6px !important;
            padding-right: 6px;
        }
        .card {
            position: relative;
            display: -ms-flexbox;
            display: flex;
            -ms-flex-direction: column;
            flex-direction: column;
            min-width: 0;
            word-wrap: break-word;
            background-color: #fff;
            background-clip: border-box;
            border: 1px solid rgba(0, 0, 0, 0.125);
            border-radius: 0.25rem;
        }

        .card > hr {
            margin-right: 0;
            margin-left: 0;
        }

        .card > .list-group:first-child .list-group-item:first-child {
            border-top-left-radius: 0.25rem;
            border-top-right-radius: 0.25rem;
        }

        .card > .list-group:last-child .list-group-item:last-child {
            border-bottom-right-radius: 0.25rem;
            border-bottom-left-radius: 0.25rem;
        }



        .card-header {
            padding: 0.75rem 1.25rem;
            margin-bottom: 0;
            background-color: rgba(0, 0, 0, 0.03);
            border-bottom: 1px solid rgba(0, 0, 0, 0.125);
        }

        .card-header:first-child {
            border-radius: calc(0.25rem - 1px) calc(0.25rem - 1px) 0 0;
        }

        .card-header + .list-group .list-group-item:first-child {
            border-top: 0;
        }

        .text-muted {
            color: #6c757d !important;
        }

        .text-left {
            text-align: left !important;
        }

        .col-md-5, .md-5, .col-md-12, .col-md {
            position: relative;
            width: 100%;
            padding-right: 15px;
            padding-left: 15px;
        }

        .card-body {
            -ms-flex: 1 1 auto;
            flex: 1 1 auto;
            padding: 1.25rem;
        }
        .mt-2 {
            margin-top: 2px
        }
        .card.card-cascade .view {
            box-shadow: 0 8px 17px 0 rgba(0, 0, 0, 0.12), 0 6px 20px 0 rgba(0, 0, 0, 0.12); }
        .card.card-cascade .view.gradient-card-header {
            padding: 1rem 1rem; }

        .card.card-cascade .view.view-cascade {
            border-radius: 0.25rem;
            box-shadow: 0 5px 11px 0 rgba(0, 0, 0, 0.18), 0 4px 15px 0 rgba(0, 0, 0, 0.15); }
        .card.card-cascade .view.view-cascade.gradient-card-header {
            padding: 1.6rem 1rem;
            color: #fff;
            text-align: center; }
        .card.card-cascade .view.view-cascade.gradient-card-header .card-header-title {
            font-weight: 500; }
        .card.card-cascade .view.view-cascade.gradient-card-header .btn-floating {
            background-color: rgba(255, 255, 255, 0.2); }

        .card.card-cascade.wider {
            background-color: transparent;
            box-shadow: none; }
        .card.card-cascade.wider .view.view-cascade {
            z-index: 2; }
        .card.card-cascade.wider .card-body.card-body-cascade {
            z-index: 1;
            margin-right: 4%;
            margin-left: 4%;
            background: #fff;
            border-radius: 0 0 0.25rem 0.25rem;
            box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12); }
        .card.card-cascade.wider .card-body.card-body-cascade .card-footer {
            margin-right: -1.25rem;
            margin-left: -1.25rem; }
        .card.card-cascade.wider.reverse .card-body.card-body-cascade {
            z-index: 3;
            margin-top: -1rem;
            border-radius: 0.25rem;
            box-shadow: 0 5px 11px 0 rgba(0, 0, 0, 0.18), 0 4px 15px 0 rgba(0, 0, 0, 0.15); }

        .card.card-cascade.narrower {
            margin-top: 1.25rem; }
        .card.card-cascade.narrower .view.view-cascade {
            margin-top: -1.25rem;
            margin-right: 4%;
            margin-left: 4%; }
        .card.card-cascade.wider {
            background-color: transparent;
            box-shadow: none; }
        .card.card-cascade.wider .view.view-cascade {
            z-index: 2; }
        .card.card-cascade.wider .card-body.card-body-cascade {
            z-index: 1;
            margin-right: 4%;
            margin-left: 4%;
            background: #fff;
            border-radius: 0 0 0.25rem 0.25rem;
            box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12); }
        .card.card-cascade.wider .card-body.card-body-cascade .card-footer {
            margin-right: -1.25rem;
            margin-left: -1.25rem; }
        .card.card-cascade.wider.reverse .card-body.card-body-cascade {
            z-index: 3;
            margin-top: -1rem;
            border-radius: 0.25rem;
            box-shadow: 0 5px 11px 0 rgba(0, 0, 0, 0.18), 0 4px 15px 0 rgba(0, 0, 0, 0.15); }
        .card.card-cascade.wider.reverse .card-body.card-body-cascade {
            z-index: 3;
            margin-top: -1rem;
            border-radius: 0.25rem;
            box-shadow: 0 5px 11px 0 rgba(0, 0, 0, 0.18), 0 4px 15px 0 rgba(0, 0, 0, 0.15); }
        .card-title {
            margin-bottom: 0.75rem;
        }
        .text-left {
            text-align: left !important;
        }
        .text-center {
            /* text-align: center !important;*/
            margin-left: 24px
        }
        select {
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif; }
        select.mdb-select {
            display: none !important; }
        select.browser-default {
            display: block !important; }
        select:disabled {
            color: rgba(0, 0, 0, 0.3); }
        .form_alignment{
            width: 96%;
            margin: auto !important;
        }
        .form-check-input[type="radio"]:not(:checked) + label,
        .form-check-input[type="radio"]:checked + label,
        label.btn input[type="radio"]:not(:checked) + label,
        label.btn input[type="radio"]:checked + label {
            position: relative;
            display: inline-block;
            height: 1.5625rem;
            padding-left: 35px;
            line-height: 1.5625rem;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            transition: 0.28s ease; }

        .form-check-input[type="radio"] + label:before,
        .form-check-input[type="radio"] + label:after,
        label.btn input[type="radio"] + label:before,
        label.btn input[type="radio"] + label:after {
            position: absolute;
            top: 0;
            left: 0;
            z-index: 0;
            width: 16px;
            height: 16px;
            margin: 4px;
            content: "";
            transition: 0.28s ease; }

        .form-check-input[type="radio"]:not(:checked) + label:before,
        .form-check-input[type="radio"]:not(:checked) + label:after,
        .form-check-input[type="radio"]:checked + label:before,
        .form-check-input[type="radio"]:checked + label:after,
        .form-check-input[type="radio"].with-gap:checked + label:before,
        .form-check-input[type="radio"].with-gap:checked + label:after,
        label.btn input[type="radio"]:not(:checked) + label:before,
        label.btn input[type="radio"]:not(:checked) + label:after,
        label.btn input[type="radio"]:checked + label:before,
        label.btn input[type="radio"]:checked + label:after,
        label.btn input[type="radio"].with-gap:checked + label:before,
        label.btn input[type="radio"].with-gap:checked + label:after {
            border-radius: 50%; }

        .form-check-input[type="radio"]:not(:checked) + label:before,
        .form-check-input[type="radio"]:not(:checked) + label:after,
        label.btn input[type="radio"]:not(:checked) + label:before,
        label.btn input[type="radio"]:not(:checked) + label:after {
            border: 2px solid #5a5a5a; }

        .form-check-input[type="radio"]:not(:checked) + label:after,
        label.btn input[type="radio"]:not(:checked) + label:after {
            transform: scale(0); }

        .form-check-input[type="radio"]:checked + label:before,
        label.btn input[type="radio"]:checked + label:before {
            border: 2px solid transparent; }

        .form-check-input[type="radio"]:checked + label:after,
        .form-check-input[type="radio"].with-gap:checked + label:before,
        .form-check-input[type="radio"].with-gap:checked + label:after,
        label.btn input[type="radio"]:checked + label:after,
        label.btn input[type="radio"].with-gap:checked + label:before,
        label.btn input[type="radio"].with-gap:checked + label:after {
            border: 2px solid #4285f4; }

        .form-check-input[type="radio"]:checked + label:after,
        .form-check-input[type="radio"].with-gap:checked + label:after,
        label.btn input[type="radio"]:checked + label:after,
        label.btn input[type="radio"].with-gap:checked + label:after {
            background-color: #4285f4; }

        .form-check-input[type="radio"]:checked + label:after,
        label.btn input[type="radio"]:checked + label:after {
            transform: scale(1.02); }

        .form-check-input[type="radio"].with-gap:checked + label:after,
        label.btn input[type="radio"].with-gap:checked + label:after {
            transform: scale(0.5); }

        .form-check-input[type="radio"].with-gap:disabled:checked + label:before,
        label.btn input[type="radio"].with-gap:disabled:checked + label:before {
            border: 2px solid rgba(0, 0, 0, 0.46); }

        .form-check-input[type="radio"].with-gap:disabled:checked + label:after,
        label.btn input[type="radio"].with-gap:disabled:checked + label:after {
            background-color: rgba(0, 0, 0, 0.46);
            border: none; }

        .form-check-input[type="radio"]:disabled:not(:checked) + label:before,
        .form-check-input[type="radio"]:disabled:checked + label:before,
        label.btn input[type="radio"]:disabled:not(:checked) + label:before,
        label.btn input[type="radio"]:disabled:checked + label:before {
            background-color: transparent;
            border-color: rgba(0, 0, 0, 0.46); }

        .form-check-input[type="radio"]:disabled + span,
        label.btn input[type="radio"]:disabled + span {
            color: rgba(0, 0, 0, 0.46); }

        .form-check-input[type="radio"]:disabled:not(:checked) + span:before,
        label.btn input[type="radio"]:disabled:not(:checked) + span:before {
            border-color: rgba(0, 0, 0, 0.46); }

        .form-check-input[type="radio"]:disabled:checked + span:after,
        label.btn input[type="radio"]:disabled:checked + span:after {
            background-color: rgba(0, 0, 0, 0.46);
            border-color: #bdbdbd; }

        .form-check-input[type="radio"]:checked + label:after .disabled-material,
        label.btn input[type="radio"]:checked + label:after .disabled-material {
            background-color: rgba(66, 133, 244, 0.2); }

        .md-disabled::after {
            background-color: rgba(66, 133, 244, 0.5) !important;
            border-color: rgba(66, 133, 244, 0.2) !important; }

        .md-disabled::before {
            border-color: rgba(66, 133, 244, 0.25) !important; }

        /* Remove default checkbox */
        [type="checkbox"]:not(:checked),
        [type="checkbox"]:checked {
            position: absolute;
            pointer-events: none;
            opacity: 0; }

        .form-check-input[type="checkbox"] + label,
        label.btn input[type="checkbox"] + label {
            position: relative;
            display: inline-block;
            height: 1.5625rem;
            padding-left: 35px;
            line-height: 1.5625rem;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none; }
        .form-check-input[type="checkbox"] + label.form-check-label-left,
        label.btn input[type="checkbox"] + label.form-check-label-left {
            padding: 0 35px 0 0 !important; }
        .form-check-input[type="checkbox"] + label.form-check-label-left:before,
        label.btn input[type="checkbox"] + label.form-check-label-left:before {
            right: 0;
            left: 100% !important;
            transform: translateX(-100%); }

        .form-check-input[type="checkbox"] + label:before,
        .form-check-input[type="checkbox"]:not(.filled-in) + label:after,
        label.btn input[type="checkbox"] + label:before,
        label.btn input[type="checkbox"]:not(.filled-in) + label:after {
            position: absolute;
            top: 0;
            left: 0;
            z-index: 0;
            width: 18px;
            height: 18px;
            margin-top: 3px;
            content: "";
            border: 2px solid #8a8a8a;
            border-radius: 1px;
            transition: 0.2s; }

        .form-check-input[type="checkbox"]:not(.filled-in) + label:after,
        label.btn input[type="checkbox"]:not(.filled-in) + label:after {
            border: 0;
            transform: scale(0); }

        .form-check-input[type="checkbox"]:not(:checked):disabled + label:before,
        label.btn input[type="checkbox"]:not(:checked):disabled + label:before {
            background-color: #bdbdbd;
            border: none; }

        .form-check-input[type="checkbox"]:checked + label:before,
        label.btn input[type="checkbox"]:checked + label:before {
            top: -4px;
            left: -5px;
            width: 12px;
            height: 1.375rem;
            border-top: 2px solid transparent;
            border-right: 2px solid #4285f4;
            border-bottom: 2px solid #4285f4;
            border-left: 2px solid transparent;
            transform: rotate(40deg);
            transform-origin: 100% 100%;
            -webkit-backface-visibility: hidden;
            backface-visibility: hidden; }

        .form-check-input[type="checkbox"]:checked + label.form-check-label-left:before,
        label.btn input[type="checkbox"]:checked + label.form-check-label-left:before {
            transform: translateX(0) rotateZ(40deg);
            transform-origin: 0 0; }

        .form-check-input[type="checkbox"]:checked:disabled + label:before,
        label.btn input[type="checkbox"]:checked:disabled + label:before {
            border-right: 2px solid #bdbdbd;
            border-bottom: 2px solid #bdbdbd; }

        .form-check-input[type="checkbox"]:indeterminate + label:before,
        label.btn input[type="checkbox"]:indeterminate + label:before {
            top: -11px;
            left: -12px;
            width: 10px;
            height: 1.375rem;
            border-top: none;
            border-right: 2px solid #4285f4;
            border-bottom: none;
            border-left: none;
            transform: rotate(90deg);
            transform-origin: 100% 100%;
            -webkit-backface-visibility: hidden;
            backface-visibility: hidden; }

        .form-check-input[type="checkbox"]:indeterminate + label.form-check-label-left:before,
        label.btn input[type="checkbox"]:indeterminate + label.form-check-label-left:before {
            top: 0;
            transform-origin: 0 0; }

        .form-check-input[type="checkbox"]:indeterminate:disabled + label:before,
        label.btn input[type="checkbox"]:indeterminate:disabled + label:before {
            background-color: transparent;
            border-right: 2px solid rgba(0, 0, 0, 0.46); }

        .form-check-input[type="checkbox"].filled-in + label:after,
        label.btn input[type="checkbox"].filled-in + label:after {
            border-radius: 0.125rem; }

        .form-check-input[type="checkbox"].filled-in + label:before,
        .form-check-input[type="checkbox"].filled-in + label:after,
        label.btn input[type="checkbox"].filled-in + label:before,
        label.btn input[type="checkbox"].filled-in + label:after {
            position: absolute;
            left: 0;
            z-index: 1;
            content: "";
            /* .1s delay is for check animation */
            transition: border 0.25s, background-color 0.25s, width 0.2s 0.1s, height 0.2s 0.1s, top 0.2s 0.1s, left 0.2s 0.1s; }

        .form-check-input[type="checkbox"].filled-in:not(:checked) + label:before,
        label.btn input[type="checkbox"].filled-in:not(:checked) + label:before {
            top: 10px;
            left: 6px;
            width: 0;
            height: 0;
            border: 3px solid transparent;
            transform: rotateZ(37deg);
            transform-origin: 100% 100%; }

        .form-check-input[type="checkbox"].filled-in:not(:checked) + label:after,
        label.btn input[type="checkbox"].filled-in:not(:checked) + label:after {
            top: 0;
            z-index: 0;
            width: 20px;
            height: 20px;
            background-color: transparent;
            border: 2px solid #5a5a5a; }

        .form-check-input[type="checkbox"].filled-in:checked + label:before,
        label.btn input[type="checkbox"].filled-in:checked + label:before {
            top: 0;
            left: 1px;
            width: 8px;
            height: 13px;
            border-top: 2px solid transparent;
            border-right: 2px solid #fff;
            border-bottom: 2px solid #fff;
            border-left: 2px solid transparent;
            transform: rotateZ(37deg);
            transform-origin: 100% 100%; }

        .form-check-input[type="checkbox"].filled-in:checked + label:after,
        label.btn input[type="checkbox"].filled-in:checked + label:after {
            top: 0;
            z-index: 0;
            width: 20px;
            height: 20px;
            background-color: #a6c;
            border: 2px solid #a6c; }

        .form-check-input[type="checkbox"].filled-in.filled-in-danger:checked + label:after,
        label.btn input[type="checkbox"].filled-in.filled-in-danger:checked + label:after {
            background-color: #f44336;
            border-color: #f44336; }

        .form-check-input[type="checkbox"]:disabled:not(:checked) + label:before,
        label.btn input[type="checkbox"]:disabled:not(:checked) + label:before {
            background-color: #bdbdbd;
            border-color: #bdbdbd; }

        .form-check-input[type="checkbox"]:disabled:not(:checked) + label:after,
        label.btn input[type="checkbox"]:disabled:not(:checked) + label:after {
            background-color: #bdbdbd;
            border-color: #bdbdbd; }

        .form-check-input[type="checkbox"]:disabled:checked + label:before,
        label.btn input[type="checkbox"]:disabled:checked + label:before {
            background-color: transparent; }

        .form-check-input[type="checkbox"]:disabled:checked + label:after,
        label.btn input[type="checkbox"]:disabled:checked + label:after {
            background-color: #bdbdbd;
            border-color: #bdbdbd; }
        .float-left {
            float: left !important;
        }
        .mt-3 {
            margin-top: : 3px;
        }

        .radio_checkbox_label{
            width: 100px !important;
            text-align: left !important;
            /*overflow: hidden !important;*/
            /*text-overflow: ellipsis !important;*/
        }
        .card-footer {
            padding: 0.75rem 1.25rem;
            background-color: rgba(0, 0, 0, 0.03);
            border-top: 1px solid rgba(0, 0, 0, 0.125);
        }

        .card-footer:last-child {
            border-radius: 0 0 calc(0.25rem - 1px) calc(0.25rem - 1px);
        }
        .card-group > .card:not(:last-child) .card-footer {
            border-bottom-right-radius: 0;
        }
        .card-group > .card:not(:first-child) .card-footer {
            border-bottom-left-radius: 0;
        }
    </style>
    <div class="row">
        <div class="col-md-12" style="text-align: center; margin:auto;">
            <a style="display: block;" href="{{url('api/form-download/'.$groupss[0]->job_group_id)}}"> <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAV1BMVEX///8AAAClpaUoKCjv7+/t7e0lJSXy8vIiIiIrKysfHx/5+fmdnZ2CgoKtra329vbo6Ohra2s9PT2NjY01NTWVlZXh4eHIyMjCwsJCQkJhYWF2dnZMTEwr+RVfAAAEDklEQVR4nO2d4VLbMBCEbWJwIUAgUFravv9zFpE0sWJZ0knrs4/u94sZzGY/LOkCY4amIYT8ZzzdP39/W7rEnFy1jvula8zHj/bAz6WLzMb70XC/dJHZaP+xdJHZoKF9aGgfGtqHhvahoX1oaB8a2oeG9qGhfWhoHxrah4b2oaF9aGgfGtqHhvahoX1oaB8a2oeG9qGhfWhoHxrah4b2oaF9aGgfGtqHhvahoX1oaB8a2meVhvetiJdtLCzLcPsie8nav7m9kr1c+7qrNNy9Cl/xStmwfag0fJC+oLphrL3oGhrScDbDL78Pv/xZusA8hBk+VgY5sgwzeYQb1gY5kIawYjQUQUMJNNQPctBQAg31gxw0lEBD/SAHDSXQUD/IQUMJNNQPctBQAg31gxw0lEBD/SAHDSXQUD/IQUMJNNQPctBQAg31gxw0lLCMYZ/4fK5h9GGAkmKYoKdfH1f9jj2KkWe4c8+bPERzZMVQQU+HyzZd5Jocw+7284rblKK+4Z/jdXeRpZph2N8dL4k9eSQqliI3aHdqv5lWTBv2t7l7Vd3w7dSsvZlcqMn23eYckzhu9Ffpudq0Ysrw+maQgiqWIjvoOUMx0b4bCq5uHzb9YIG1N+G9GDfsh4IrPEub7tugYPi4iRoODpmPE/kaVwwXlK4YM8z4BpUWwwX5CzWgGDHsvK+NvW0oKIYL8moGRv+0YT+8gzmCS/1s4SmO38BNGnoLPHT3a4vhguKLbcpQvETlxXBB0bsxYZi485hiuKDYjgobpnYvqBguKLLkgobJExhVDBc0PdlChsJBX1MMFzRZOmAoHfRVxXBBUwtvbFhyilYUwwVNHB4jQ/mgryyGCwoPgEvDgkFfWwwXFFx+F4blS7SiGC4odH98w6JBX18MFxTYY55h2aAHFMMFjRfh0LBw0COK4YJGs25gWDroIcVwQZcaZ8PiQY8phgvyl+L29OG25hRFFMMFeb8g3Ac+KhVcjaF/3IQoOGQgxXBBCcXCO7gmQ/+4uUQ86IHFcEHe6PeRD3pkMVzQ5EIt3YOoYrigLnwXSwY9thguKLgXiwY9uBguqB8v1OJTFFoMFzTai5WC6zO8VKw5ZLDFcM9EVf1EP2Mx4FNfg+OmfNDjiyGfazuN/opBjy8GfXLvuFA3tXvQsU7Dw0IFLNFmtYbN9r3d5zx5mGaths32GSO4XkMYNNQPQkND/SA0NNQPQkND/SA0NNQPQkND/SA0NNQPQkND/SA0NNQPQkND/SA0NNQPQkND/SA0NNQPQkND/SA0NNQPQkND/SA0NNQPQkND/SA0NNQPQkND/SA0NNQPQkND/SA0NNQPQkND/SA0NNQPQoM3RPwvWST4/yW7WmhIQxouDw1pSMPlSRn+BVdWKC8GMCHVAAAAAElFTkSuQmCC" style="height: 50px;" /></a>
        </div>


        @foreach ($groupss as $key => $value)
            <div id="{{$value->form_Section->order}}-{{$value->form_Section->id}}" class="custom_clear  sortable-card col-md-{{$value->form_Section->width}}">
                <div class="custom_clear mb-4 custom_dynamic_height">
                    <br>
                    <div class="custom_clear card text-center ">
                        <div class="custom_clear card-header text-muted text-left md-5">
                            <span class="custom_clear" style="flex: 1; text-align:left;padding-bottom: 40px">{{$value->form_Section->header}}</span>
                        </div>
                        <div class="custom_clear card-body mt-2">
                            @foreach($value->form_section->questions as $key2 => $question)
                                <div class="custom_clear card card-cascade wider reverse" style="padding-top: 10px;padding-bottom: 10px; border: 1px solid rgba(0, 0, 0, 0.125); margin: 24px">
                                    <p class="custom_clear card-title text-left" style="padding-left: 20px;">
                                        <strong>{{($question->question)}}</strong>
                                    </p>
                                    <div class="custom_clear row text-center">
                                        @switch ($question->answer_type->name)
                                            @case ('dropdown')
                                            <div class="custom_clear col-md-12 ">
                                                <div class="custom_clear md-form mt-5">
                                                    @if($question->response)
                                                        @if($question->response->answer_data)
                                                            <p>{{$question->response->answer_data->answer}}</p>
                                                            @if($question->response->image)
                                                             <img src="{{$question->response->image}}" alt="Signature" style="height: 300px; width: auto;"/>
                                                             @endif
                                                        @endif
                                                    @endif

                                                    
                                                </div>
                                            </div>
                                            @break
                                            @case ('radio')
                                            <div class="custom_clear col-md-12 ">
                                                <div class="custom_clear md-form mt-5">

                                                    @if($question->response)

                                                        @if($question->response->answer_data)

                                                            <p>{{$question->response->answer_data->answer}}</p>
                                                             @if($question->response->image)
                                                             <img src="{{$question->response->image}}" alt="Signature" style="height: 300px; width: auto;"/>
                                                             @endif
                                                        @endif
                                                    @endif
                                                </div>
                                            </div>
                                            @break
                                            @case ('checkbox')
                                            <div class="custom_clear col-md-12 ">
                                                <div class="custom_clear md-form mt-5">
                                                    @if($question->response)
                                                        @if($question->response->answer_data)
                                                            <p>{{$question->response->answer_data->answer}}</p>
                                                             @if($question->response->image)
                                                             <img src="{{$question->response->image}}" alt="Signature" style="height: 300px; width: auto;"/>
                                                             @endif
                                                        @endif
                                                    @endif
                                                </div>
                                            </div>
                                            @break

                                            @case ('multi')
                                            <div class="custom_clear col-md-12 ">
                                                <div class="custom_clear md-form mt-5">
                                                    @if($question->response)
                                                        @if($question->response->answer_data)
                                                            <p>{{$question->response->answer_data->answer}}</p>
                                                             @if($question->response->image)
                                                             <img src="{{$question->response->image}}" alt="Signature" style="height: 300px; width: auto;"/>
                                                             @endif
                                                        @endif
                                                    @endif
                                                </div>
                                            </div>
                                            @break
                                            @case ('date')
                                            <div class="custom_clear col-md-12 ">
                                                <div class="custom_clear md-form mt-5">
                                                    @if($question->response)
                                                        @if($question->response)
                                                            <p>{{$question->response->answer_text}}</p>
                                                             @if($question->response->image)
                                                             <img src="{{$question->response->image}}" alt="Signature" style="height: 300px; width: auto;"/>
                                                             @endif
                                                        @endif
                                                    @endif

                                                </div>
                                            </div>
                                            @break
                                            @case ('time')
                                            <div class="custom_clear col-md-12 ">
                                                <div class="custom_clear col-md-12 ">
                                                    <div class="custom_clear md-form mt-5">
                                                        @if($question->response)
                                                            @if($question->response)
                                                                <p>{{$question->response->answer_text}}</p>
                                                                 @if($question->response->image)
                                                             <img src="{{$question->response->image}}" alt="Signature" style="height: 300px; width: auto;"/>
                                                             @endif
                                                            @endif
                                                        @endif

                                                    </div>
                                                </div>
                                            </div>
                                            @case ('text')

                                            <div class="custom_clear col-md-12 ">
                                                <div class="custom_clear col-md-12 ">
                                                    <div class="custom_clear md-form mt-5">
                                                        @if($question->response)
                                                            @if($question->response)
                                                                <p>{{$question->response->answer_text}}</p>
                                                                 @if($question->response->image)
                                                             <img src="{{$question->response->image}}" alt="Signature" style="height: 300px; width: auto;"/>
                                                             @endif
                                                            @endif
                                                        @endif

                                                    </div>
                                                </div>
                                            </div>
                                            @break
                                            @case ('comment')

                                            <div class="custom_clear col-md-12 ">
                                                <div class="custom_clear md-form mt-5">
                                                    <p>{{$question->comment}}</p>
                                                     @if($question->response->image)
                                                             <img src="{{$question->response->image}}" alt="Signature" style="height: 300px; width: auto;"/>
                                                             @endif
                                                </div>
                                            </div>
                                            @break
                                            @case ('richtext')

                                            <div class="custom_clear col-md-12 ">
                                                <div class="custom_clear col-md-12 ">
                                                    <div class="custom_clear md-form mt-5">
                                                        @if($question->response)
                                                            @if($question->response)
                                                                <p>{{strip_tags($question->response->answer_text)}}</p>
                                                                 @if($question->response->image)
                                                             <img src="{{$question->response->image}}" alt="Signature" style="height: 300px; width: auto;"/>
                                                             @endif
                                                            @endif
                                                        @endif

                                                    </div>
                                                </div>
                                            </div>
                                            @break
                                            @case ('datetime')
                                            <div class="custom_clear col-md-12 ">
                                                <div class="custom_clear col-md-12 ">
                                                    <div class="custom_clear md-form mt-5">
                                                        @if($question->response)
                                                            @if($question->response)
                                                                <p>{{$question->response->answer_text}}</p>
                                                                 @if($question->response->image)
                                                             <img src="{{$question->response->image}}" alt="Signature" style="height: 300px; width: auto;"/>
                                                             @endif
                                                            @endif
                                                        @endif

                                                    </div>
                                                </div>
                                            </div>
                                            @break
                                            @case ('signature text')

                                            <div class="custom_clear col-md-12 ">
                                                <div class="custom_clear md-form mt-5">
                                                    @if($question->response)
                                                        <img src="{{$question->response->image}}" alt="Signature" style="height: 300px; width: auto;"/>
                                                    @endif
                                                </div>
                                            </div>
                                            @break
                                            @case ('signature user')

                                            <div class="custom_clear col-md-12 ">
                                                <div class="custom_clear md-form mt-5">
                                                    @if($question->response)
                                                        <img src="{{$question->response->image}}" alt="Signature" style="height: 300px; width: auto;"/>
                                                    @endif
                                                </div>
                                            </div>
                                            @break
                                            @case ('signature select')

                                            <div class="custom_clear col-md-12 ">
                                                <div class="custom_clear md-form mt-5">
                                                    @if($question->response)
                                                        <img src="{{$question->response->image}}" alt="Signature" style="height: 300px; width: auto;"/>
                                                    @endif
                                                </div>
                                            </div>
                                            @break

                                            @break
                                        @endswitch
                                    </div>
                                </div>
                            @endforeach()

                        </div>
                        <div class="custom_clear card-footer text-muted text-left">
                            {{$value->form_Section->footer}}
                        </div>
                    </div>
                </div>
            </div>
        @endforeach()
    </div>
@endsection
