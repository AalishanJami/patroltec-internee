<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=yes">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
</head>
<body class="fixed-sn white-skin">
<!--Main Navigation-->
<div id="content" class="color-block content">
<header>
    @php
        $data=localization();
    @endphp

</header>
<!--Main Navigation-->
<!-- Main layout -->
<main>
    <div class="container-fluid">
        @toastr_js
        @toastr_render
        @yield('content')
    </div>
</main>
</div>
</body>
