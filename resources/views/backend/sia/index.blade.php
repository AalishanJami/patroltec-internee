@extends('backend.layouts.backend')
@section('content')
    @php
       $data=localization();
    @endphp
        {{ Breadcrumbs::render('sia') }}
        <!-- <span class="table-add float-right mb-3 mr-2">
            <a class="text-success"  data-toggle="modal" data-target="#modalRegisterCompany">
                <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
            </a>
        </span> -->

         <div class="card">

            <div class="card-body">
                <div id="table" class="table-editable">

        <button class="btn btn-danger btn-sm">
            Delete Selected SIA
        </button>
        <button class="btn btn-primary btn-sm">
          Restore Deleted SIA
        </button>

        <button  style="display: none;">
            Show Active Sia
        </button>
        <button class="btn btn-warning btn-sm">
           Excel Export
        </button>
        <button class="btn btn-success btn-sm">
            Word Export
        </button>
    <table  class="company_table_static table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th> Name
            </th>
            <th> Licence Sector
            </th>
            <th> Service
            </th>
            <th> Status
            </th>
            <th> Licence Number
            </th>
            <th> Expiry
            </th>
            <th> Role
            </th>
        </tr>
        </thead>
            <tr>
                <td class="pt-3-half">New york</td>
                <td class="pt-3-half">Sector B</td>
                <td class="pt-3-half">Wash</td>
                <td class="pt-3-half">pending</td>
                <td class="pt-3-half">877899-998</td>
                <td class="pt-3-half">17 march 2020</td>
                <td class="pt-3-half">worker</td>
            </tr>
        <tbody>
        </tbody>
    </table>
   </div>
</div>
</div>

@endsection
