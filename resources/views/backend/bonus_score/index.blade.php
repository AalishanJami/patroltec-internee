@extends('backend.layouts.detail')
@section('title', 'Bonus Score')
@section('headscript')
    <link rel="stylesheet" href="{{ asset('custom/css/custom_style.css') }}">
    <script src="{{ asset('custom/js/bous_score.js') }}"></script>
@stop
@section('content')
    @php
        $data=localization();
    @endphp
    @include('backend.layouts.detail_sidebar')
    <div class="padding-left-custom remove-padding">
        {!! breadcrumInner('bonus_score',Session::get('project_id'),'locations','/project/detail/'.Session::get('project_id') ,'project','/project') !!}
        <div class="card">
            <div class="card-body">
                <div id="table-bonus_score" class="table-editable">
                    @if(Auth()->user()->hasPermissionTo('create_bonusScore') || Auth::user()->all_companies == 1 )
                        <span class="add-bonus table-add float-right mb-3 mr-2">
                            <a class="text-success"><i class="fas fa-plus fa-2x" aria-hidden="true"></i></a>
                        </span>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('selected_delete_bonusScore') || Auth::user()->all_companies == 1 )
                        <button class="btn btn-danger btn-sm" id="deleteselectedbonus_score">
                            <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                                {!! checkKey('selected_delete',$data) !!}
                            </span>
                        </button>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('selected_delete_bonusScore') || Auth::user()->all_companies == 1 )
                        <button class="btn btn-danger btn-sm" style="display: none;" id="deleteselectedbonus_scoreSoft">
                            <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                                {!! checkKey('selected_delete',$data) !!}
                            </span>
                        </button>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('selected_active_bonusScore') || Auth::user()->all_companies == 1 )
                        <button class="btn btn-success btn-sm" style="display: none;" id="selectedactivebuttonbonus_score">
                           <span class="assign_class label{{getKeyid('selected_active',$data)}}" data-id="{{getKeyid('selected_active',$data) }}" data-value="{{checkKey('selected_active',$data) }}" >
                                {!! checkKey('selected_active',$data) !!}
                            </span>
                        </button>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('restore_delete_bonusScore') || Auth::user()->all_companies == 1 )
                        <button class="btn btn-primary btn-sm" id="restorebuttonbonus_score">
                            <span class="assign_class label{{getKeyid('restore',$data)}}" data-id="{{getKeyid('restore',$data) }}" data-value="{{checkKey('restore',$data) }}" >
                                {!! checkKey('restore',$data) !!}
                            </span>
                        </button>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('active_bonusScore') || Auth::user()->all_companies == 1 )
                        <button  style="display: none;" class="btn btn-primary btn-sm" id="activebuttonbonus_score">
                            <span class="assign_class label{{getKeyid('show_active',$data)}}" data-id="{{getKeyid('show_active',$data) }}" data-value="{{checkKey('show_active',$data) }}" >
                                {!! checkKey('show_active',$data) !!}
                            </span>
                        </button>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('csv_bonusScore') || Auth::user()->all_companies == 1 )
                        <form class="form-style" method="POST" action="{{ url('bonus/score/export/excel') }}">
                            <input type="hidden" id="export_excel_bonus_score" name="export_excel_bonus_score" value="1">
                            <input type="hidden" class="bonus_score_export" name="excel_array" value="1">
                            {!! csrf_field() !!}
                            <button  type="submit" id="export_excel_bonus_score_btn" class="form_submit_check btn btn-warning btn-sm">
                                <span class="assign_class label{{getKeyid('excel_export',$data)}}" data-id="{{getKeyid('excel_export',$data) }}" data-value="{{checkKey('excel_export',$data) }}" >
                                    {!! checkKey('excel_export',$data) !!}
                                </span>
                            </button>
                        </form>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('word_bonusScore') || Auth::user()->all_companies == 1 )
                        <form class="form-style" method="POST" action="{{ url('bonus/score/export/world') }}">
                            <input type="hidden" id="export_world_bonus_score" name="export_world_bonus_score" value="1">
                            <input type="hidden" class="bonus_score_export" name="word_array" value="1">
                            {!! csrf_field() !!}
                            <button  type="submit" id="export_world_bonus_score_btn" class="form_submit_check btn btn-success btn-sm">
                                <span class="assign_class label{{getKeyid('word_export',$data)}}" data-id="{{getKeyid('word_export',$data) }}" data-value="{{checkKey('word_export',$data) }}" >
                                    {!! checkKey('word_export',$data) !!}
                                </span>
                            </button>
                        </form>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('pdf_bonusScore') || Auth::user()->all_companies == 1 )
                        <form  class="form-style" {{pdf_view("scorings")}}  method="POST" action="{{ url('bonus/score/export/pdf') }}">
                            <input type="hidden" id="export_pdf_bonus_score" name="export_world_pdf" value="1">
                            <input type="hidden" class="bonus_score_export" name="pdf_array" value="1">
                            {!! csrf_field() !!}
                            <button  type="submit" id="export_pdf_bonus_score_btn"  class="form_submit_check btn btn-secondary btn-sm">
                                <span class="assign_class label{{getKeyid('pdf_export',$data)}}" data-id="{{getKeyid('pdf_export',$data) }}" data-value="{{checkKey('pdf_export',$data) }}" >
                                    {!! checkKey('pdf_export',$data) !!}
                                </span>
                            </button>
                        </form>
                    @endif
                    <div id="table" class="table-editable">
                        <table id="scoring_table_static_document" class="scoring_table_static_document  table table-bordered table-responsive-md table-striped">
                            <thead>
                            <tr>
                                <th class="no-sort all_checkboxes_style">
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input bonusscore_checked" id="bonusscore_checkbox_all">
                                        <label class="form-check-label" for="bonusscore_checkbox_all">
                                            <span class="assign_class label{{getKeyid('all',$data)}}" data-id="{{getKeyid('all',$data) }}" data-value="{{checkKey('all',$data) }}" >
                                                {!! checkKey('all',$data) !!}
                                            </span>
                                        </label>
                                    </div>
                                </th>
                                <th>
                                    <span class="assign_class label{{getKeyid('month',$data)}}" data-id="{{getKeyid('month',$data) }}" data-value="{{checkKey('month',$data) }}" >
                                        {!! checkKey('month',$data) !!}
                                    </span>
                                </th>
                                <th>
                                    <span class="assign_class label{{getKeyid('year',$data)}}" data-id="{{getKeyid('year',$data) }}" data-value="{{checkKey('year',$data) }}" >
                                        {!! checkKey('year',$data) !!}
                                    </span>
                                </th>
                                <th>
                                    <span class="assign_class label{{getKeyid('score',$data)}}" data-id="{{getKeyid('score',$data) }}" data-value="{{checkKey('score',$data) }}" >
                                        {!! checkKey('score',$data) !!}
                                    </span>
                                </th>
                                <th>
                                    <span class="assign_class label{{getKeyid('notes',$data)}}" data-id="{{getKeyid('notes',$data) }}" data-value="{{checkKey('notes',$data) }}" >
                                        {!! checkKey('notes',$data) !!}
                                    </span>
                                </th>
                                <th id="action" class="no-sort all_action_btn"></th>
                            </tr>
                            </thead>
                            <tbody id="bonus_score_data"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backend.label.input_label')
    @include('backend.bonus_score.input_permission')
    <script type="text/javascript">
        function checkSelected(value,checkValue)
        {
            if(value == checkValue)
            {
                return 'selected';
            }
            else
            {
                return "";
            }
        }
        function bonus_score_data(url) {
            $.ajax({
                type: 'GET',
                url: url,
                success: function(response)
                {
                    $.ajax({
                        type: 'GET',
                        url: url,
                        success: function(response)
                        {
                            var table = $('.scoring_table_static_document').dataTable(
                                {
                                    "columnDefs": [
                                        { "orderable": false, "targets": [0,5] }
                                    ],
                                    processing: true,
                                    language: {
                                      'lengthMenu': '  _MENU_ ',
                                      'search':'<span class="assign_class label'+$('#search_id').attr('data-id')+'" data-id="'+$('#search_id').attr('data-id')+'" data-value="'+$('#search_id').val()+'" >'+$('#search_id').val()+'</span>',
                                      'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                                      'info':'',
                                      'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                                      'paginate': {
                                          'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                                          'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                                      },
                                      'infoFiltered': "(filtered from _MAX_ total records)"
                                },
                                    "ajax": {
                                        "url": url,
                                        "type": 'get',
                                    },
                                    "createdRow": function( row, data, dataIndex,columns )
                                    {
                                        var checkbox_permission=$('#checkbox_permission').val();
                                        var checkbox='';
                                        checkbox+='<div class="form-check"><input type="checkbox" class="form-check-input selectedrowbonus_score" id="bonus_score'+data.id+'"><label class="form-check-label" for="bonus_score'+data.id+'""></label></div>';
                                        var submit='';
                                        if ($('#edit_bonusScore').val()) {
                                           submit+='<a type="button" class="btn btn-primary btn-xs all_action_btn_margin waves-effect waves-light savebonus_scoredata scoredata_show_button_'+data.id+' show_tick_btn'+data.id+'" style="display: none;" id="'+data.id+'"><i class="fas fa-check"></i></a>';
                                         }
                                        var month='';
                                        var disabled="";
                                        if (!$("#bonusScore_month_edit").val() || !$('#edit_bonusScore').val()) {
                                            var disabled="disabled";
                                        }
                                        month+='<select '+disabled+' onchange="editSelectedRow('+"bonus_score_tr"+data.id+')" searchable="Search here.."  id="month'+data.id+'"  style="background-color: inherit;border: 0px"><option '+disabled+' value="1" '+checkSelected(data.month,1)+'  >Jan</option><option '+disabled+' value="2" '+checkSelected(data.month,2)+' >Feb</option><option '+disabled+' value="3" '+checkSelected(data.month,3)+'  >Mar</option><option '+disabled+' value="4" '+checkSelected(data.month,4)+'  >April</option><option '+disabled+' value="5"  '+checkSelected(data.month,5)+' >May</option><option '+disabled+' value="6" '+checkSelected(data.month,6)+'  >June</option><option '+disabled+' value="7" '+checkSelected(data.month,7)+'  >July</option><option '+disabled+' value="8" '+checkSelected(data.month,8)+'  >August</option> <option '+disabled+' value="9" '+checkSelected(data.month,9)+' >September</option><option '+disabled+' value="10" '+checkSelected(data.month,10)+'  >October</option><option '+disabled+' value="11" '+checkSelected(data.month,11)+' >November</option><option '+disabled+' value="12" '+checkSelected(data.month,12)+'  >December</option></select>';
                                        $(columns[0]).html(checkbox);
                                        if ($("#bonusScore_month_edit").val()==1 && $('#edit_bonusScore').val()) {

                                        }
                                        $(columns[1]).attr('class','edit_inline_bonus_score');
                                        $(columns[1]).attr('data-id',data['id']);
                                        $(columns[1]).html(month);
                                        $(columns[2]).attr('id', 'year'+data['id']);
                                        if ($("#bonusScore_year_edit").val()==1 && $('#edit_bonusScore').val()) {
                                            $(columns[2]).attr('Contenteditable', 'true');
                                        }
                                        $(columns[2]).attr('onkeydown', 'editSelectedRow(bonus_score_tr'+data['id']+')');
                                        $(columns[2]).attr('class','edit_inline_bonus_score');
                                        $(columns[2]).attr('data-id',data['id']);

                                        $(columns[3]).attr('id','bonus_score_filed'+data['id']);
                                        $(columns[3]).attr('class','edit_inline_bonus_score');
                                        $(columns[3]).attr('data-id',data['id']);
                                        $(columns[3]).attr('onkeydown', 'editSelectedRow(bonus_score_tr'+data['id']+')');
                                        if ($("#bonusScore_score_edit").val()==1 && $('#edit_bonusScore').val()) {
                                            $(columns[3]).attr('Contenteditable', 'true');
                                        }
                                        $(columns[4]).attr('id','notes'+data['id']);
                                        $(columns[4]).attr('class','edit_inline_bonus_score');
                                        $(columns[4]).attr('onkeydown', 'editSelectedRow(bonus_score_tr'+data['id']+')');
                                        $(columns[4]).attr('data-id',data['id']);
                                        if ($("#bonusScore_notes_edit").val()==1 && $('#edit_bonusScore').val()) {
                                            $(columns[4]).attr('Contenteditable', 'true');
                                        }
                                        $(columns[5]).append(submit);
                                        $(row).attr('id', 'bonus_score_tr'+data['id']);
                                        //$(row).attr('class', 'selectedrowbonus_score');
                                        $(row).attr('data-id', data['id']);
                                    },
                                    columns:
                                        [
                                            {data: 'checkbox', name: 'checkbox',visible:$('#bonusScore_checkbox').val()},
                                            {data: 'month', name: 'month',visible:$('#bonusScore_month').val()},
                                            {data: 'year', name: 'year',visible:$('#bonusScore_year').val()},
                                            {data: 'bonus_score', name: 'bonus_score',visible:$('#bonusScore_score').val()},
                                            {data: 'notes', name: 'notes',visible:$('#bonusScore_notes').val()},
                                            // {data: 'submit', name: 'submit'},
                                            {data: 'actions', name: 'actions',visible:$('#delete_bonusScore').val()},
                                        ],
                                }
                            );
                            if ($(":checkbox").prop('checked',true)){
                                $(":checkbox").prop('checked',false);
                            }
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
                },
                error: function (error) {
                    console.log(error);
                }
            });
            // (function ($) {
            //     var active ='<span class="assign_class label'+$('#breadcrumb_bonusScore').attr('data-id')+'" data-id="'+$('#breadcrumb_bonusScore').attr('data-id')+'" data-value="'+$('#breadcrumb_bonusScore').val()+'" >'+$('#breadcrumb_bonusScore').val()+'</span>';
            //     $('.breadcrumb-item.active').html(active);
            //     var parent ='<span class="assign_class label'+$('#dashboard_label').attr('data-id')+'" data-id="'+$('#dashboard_label').attr('data-id')+'" data-value="'+$('#dashboard_label').val()+'" >'+$('#dashboard_label').val()+'</span>';
            //     $('.breadcrumb-item a').html(parent);
            // }(jQuery));
        }

        $(document).ready(function () {
            var url='/bonus/score/getall';
            bonus_score_data(url);

        });
    </script>
@endsection
