@php
        $data=localization();
@endphp
<style type="text/css">
    .ml-4-5 {
            margin-left: 2.5rem !important;
        }
        .ml-4
        {
            margin-left: 2.5rem !important;
        }
</style>
<div class="modal fade" id="modalRegisterCompanycreate_doc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Bonus Score</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form >
                <div class="modal-body mx-3">
                    <div class="md-form mb-5">
                
                    <div class="row">

                    <div class="col">
                       
                       <i class="far fa-map prefix grey-text fa-2x"></i>
                      
                        <div class="col ml-4">
                           <label data-error="wrong" data-success="right" for="name" class="active">Location Type</label>
                           <select searchable="Search here.."  class="mdb-select">
                              <option value="" disabled>Please Select</option>
                              <option value="1">QR</option>
                              <option value="2">Plant and Equipment</option>
                           </select>
                        </div>
                     </div>

        
                    <div class="col">
                     <i class="fas fa-bullseye prefix grey-text"></i>
                        <div class="col ml-4">
                            <input type="number" class="form-control validate @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                            <label data-error="wrong" data-success="right" for="name">
                                Score
                                <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                                data-content="tool tip for Score"></i>
                            </label>
                        </div>
                    </div>
                    <div class="col">
                        <i class="fab fa-maxcdn prefix grey-text"></i>
                          <div class="col ml-4">
                        <input type="number" class="form-control validate @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                        <label data-error="wrong" data-success="right" for="name">
                            Max Visit Per Month
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                            data-content="tool tip for Score"></i>
                        </label>
                    </div>
                    </div>
                    <div class="col">
                        <i class="fas fa-chart-line prefix grey-text"></i>
                        <input type="checkbox" class="form-check-input company" id="name" value="1">
                        <label class="form-check-label" for="name">  Show dashbroad </label>
                    </div>
                     </div>
                            
                            
                           
                      
                    </div> 


                   <!--  <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                        <label data-error="wrong" data-success="right" for="name">
                            Notes
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                            data-content="tool tip for Notes"></i>
                        </label>
                    </div> -->
                    <div class=" form-row">
                        <div class="col">
                          <div class="md-form mb-5">
                             <div id="document-full-doc" class="ql-scroll-y" style="height: 300px;">
                             </div>
                          </div>
                        </div>
                        <!--  <div class="col"> -->
                    </div>

                </div>
             
                <div class="modal-footer d-flex justify-content-center">
                    <button class="btn btn-primary btn-sm" type="submit"><span class="assign_class label{{getKeyid('save',$data)}}" data-id="{{getKeyid('save',$data) }}" data-value="{{checkKey('save',$data) }}" >{!! checkKey('save',$data) !!} </span></button>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="{{asset('editor/sprite.svg.js')}}"></script>

<script type="text/javascript">
   var toolbarOptions = [
      ['bold', 'italic', 'underline'], // toggled buttons
      ['blockquote'],

     // custom button values
      [{
        'list': 'ordered'
      }, {
        'list': 'bullet'
      }],
      [{
        'indent': '-1'
      }, {
        'indent': '+1'
      }], // outdent/indent
     
      
       // dropdown with defaults from theme
     
      [{
        'align': []
      }],
     // remove formatting button
    ];

    var quillFull = new Quill('#document-full-doc', {
      modules: {
        toolbar: toolbarOptions,
        autoformat: true
      },
      theme: 'snow',
      placeholder: "Write something..."
    });

</script>
