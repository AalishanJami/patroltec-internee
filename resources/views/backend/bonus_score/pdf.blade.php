@include('backend.layouts.pdf_start')
  <thead>
     <tr>
       @if(Auth()->user()->hasPermissionTo('bonusScore_month_pdf_export') || Auth::user()->all_companies == 1 )
       <th style="vertical-align: text-top;" class="pdf_table_layout"> Month</th>
       @endif
       @if(Auth()->user()->hasPermissionTo('bonusScore_year_pdf_export') || Auth::user()->all_companies == 1 )
        <th style="vertical-align: text-top;" class="pdf_table_layout">Year</th>
       @endif
       @if(Auth()->user()->hasPermissionTo('bonusScore_score_pdf_export') || Auth::user()->all_companies == 1 )
        <th style="vertical-align: text-top;" class="pdf_table_layout">Bonus Score</th>
       @endif
       @if(Auth()->user()->hasPermissionTo('bonusScore_notes_pdf_export') || Auth::user()->all_companies == 1 )
        <th style="vertical-align: text-top;" class="pdf_table_layout">Notes</th>
       @endif
    </tr>
  </thead>
  <tbody>
    @foreach($data as $value)
      <tr>
          @if(Auth()->user()->hasPermissionTo('bonusScore_month_pdf_export') || Auth::user()->all_companies == 1 )
              <td class="pdf_table_layout"><p class="col_text_style">{{ $value->month }}</p></td>
          @endif
          @if(Auth()->user()->hasPermissionTo('bonusScore_year_pdf_export') || Auth::user()->all_companies == 1 )
          <td class="pdf_table_layout"><p class="col_text_style">{{ $value->year }}</p></td>
          @endif
          @if(Auth()->user()->hasPermissionTo('bonusScore_score_pdf_export') || Auth::user()->all_companies == 1 )
          <td class="pdf_table_layout"><p class="col_text_style">{{ $value->bonus_score }}</p></td>
          @endif
          @if(Auth()->user()->hasPermissionTo('bonusScore_notes_pdf_export') || Auth::user()->all_companies == 1 )
          <td class="pdf_table_layout"><p class="col_text_style">{{$value->notes}}</p></td>
          @endif
      </tr>
    @endforeach
  </tbody>
@include('backend.layouts.pdf_end')
