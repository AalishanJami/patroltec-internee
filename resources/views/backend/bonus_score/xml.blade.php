<table>
    <thead>
      <tr>
          @if(Auth()->user()->hasPermissionTo('bonusScore_month_excel_export') || Auth::user()->all_companies == 1 )
              <th> Month</th>
          @endif
          @if(Auth()->user()->hasPermissionTo('bonusScore_year_excel_export') || Auth::user()->all_companies == 1 )
              <th> Year</th>
          @endif
          {{--      <th> Serial Number</th>--}}
          @if(Auth()->user()->hasPermissionTo('bonusScore_score_excel_export') || Auth::user()->all_companies == 1 )
              <th> Bonus Score</th>
          @endif
          @if(Auth()->user()->hasPermissionTo('bonusScore_notes_excel_export') || Auth::user()->all_companies == 1 )
              <th> Notes</th>
          @endif
      </tr>
    </thead>
    <tbody>
    @foreach($data as $row)
      <tr>
          @if(Auth()->user()->hasPermissionTo('bonusScore_month_excel_export') || Auth::user()->all_companies == 1 )
              <td>{{ $row->month }}</td>
          @endif
          @if(Auth()->user()->hasPermissionTo('bonusScore_year_excel_export') || Auth::user()->all_companies == 1 )
              <td>{{ $row->year }}</td>
          @endif
          @if(Auth()->user()->hasPermissionTo('bonusScore_score_excel_export') || Auth::user()->all_companies == 1 )
              <td>{{ $row->bonus_score }}</td>
          @endif
          @if(Auth()->user()->hasPermissionTo('bonusScore_notes_excel_export') || Auth::user()->all_companies == 1 )
              <td>{{$row->notes}}</td>
          @endif
      </tr>
    @endforeach
    </tbody>
</table>
