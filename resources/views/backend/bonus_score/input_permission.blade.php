@if(Auth()->user()->all_companies == 1)
    <input type="hidden" id="bonusScore_checkbox" value="1">
    <input type="hidden" id="delete_bonusScore" value="1">
    <input type="hidden" id="bonusScore_month" value="1">
    <input type="hidden" id="bonusScore_year" value="1">
    <input type="hidden" id="bonusScore_score" value="1">
    <input type="hidden" id="bonusScore_notes" value="1">
    <input type="hidden" id="edit_bonusScore" value="1">
    <!---------Create---------->
    <input type="hidden" id="bonusScore_month_create" value="1">
    <input type="hidden" id="bonusScore_year_create" value="1">
    <input type="hidden" id="bonusScore_score_create" value="1">
    <input type="hidden" id="bonusScore_notes_create" value="1">

    <!---------edit---------->
    <input type="hidden" id="bonusScore_month_edit" value="1">
    <input type="hidden" id="bonusScore_year_edit" value="1">
    <input type="hidden" id="bonusScore_score_edit" value="1">
    <input type="hidden" id="bonusScore_notes_edit" value="1">
@else
    <input type="hidden" id="bonusScore_checkbox" value="{{Auth()->user()->hasPermissionTo('bonusScore_checkbox')}}">
    <input type="hidden" id="delete_bonusScore" value="{{Auth()->user()->hasPermissionTo('delete_bonusScore')}}">
    <input type="hidden" id="bonusScore_month" value="{{Auth()->user()->hasPermissionTo('bonusScore_month')}}">
    <input type="hidden" id="bonusScore_year" value="{{Auth()->user()->hasPermissionTo('bonusScore_year')}}">
    <input type="hidden" id="bonusScore_score" value="{{Auth()->user()->hasPermissionTo('bonusScore_score')}}">
    <input type="hidden" id="bonusScore_notes" value="{{Auth()->user()->hasPermissionTo('bonusScore_notes')}}">
    <input type="hidden" id="edit_bonusScore" value="{{Auth()->user()->hasPermissionTo('edit_bonusScore')}}">

    <!---------Create---------->
    <input type="hidden" id="bonusScore_month_create" value="{{Auth()->user()->hasPermissionTo('bonusScore_month_create')}}">
    <input type="hidden" id="bonusScore_year_create" value="{{Auth()->user()->hasPermissionTo('bonusScore_year_create')}}">
    <input type="hidden" id="bonusScore_score_create" value="{{Auth()->user()->hasPermissionTo('bonusScore_score_create')}}">
    <input type="hidden" id="bonusScore_notes_create" value="{{Auth()->user()->hasPermissionTo('bonusScore_notes_create')}}">

    <!---------edit---------->
    <input type="hidden" id="bonusScore_month_edit" value="{{Auth()->user()->hasPermissionTo('bonusScore_month_edit')}}">
    <input type="hidden" id="bonusScore_year_edit" value="{{Auth()->user()->hasPermissionTo('bonusScore_year_edit')}}">
    <input type="hidden" id="bonusScore_score_edit" value="{{Auth()->user()->hasPermissionTo('bonusScore_score_edit')}}">
    <input type="hidden" id="bonusScore_notes_edit" value="{{Auth()->user()->hasPermissionTo('bonusScore_notes_edit')}}">

@endif
