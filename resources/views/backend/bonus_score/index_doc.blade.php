@extends('backend.layouts.doc')
@section('content')
    @php
       $data=localization();
    @endphp
    <div class="row">
        @include('backend.layouts.doc_sidebar')
        <div class="padding-left-custom remove-padding">
            {{ Breadcrumbs::render('bonus/score') }}
             <div class="card">

            <div class="card-body">
                <div id="table" class="table-editable">
           <!--  <span class="table-add float-right mb-3 mr-2">
                <a class="text-success"  data-toggle="modal" data-target="#modalRegisterCompanycreate_doc">
                    <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
                </a>
            </span> -->
              <span class="table-add float-right mb-3 mr-2"><a href="#!" class="text-success"><i
            class="fas fa-plus fa-2x" aria-hidden="true"></i></a></span>

            <button class="btn btn-danger btn-sm">
                Delete Selected Bonus Score
            </button>
            <button class="btn btn-primary btn-sm">
              Restore Deleted Bonus Score
            </button>

            <button  style="display: none;">
                Show Active Bonus Score
            </button>
            <button class="btn btn-warning btn-sm">
               Excel Export
            </button>
            <button class="btn btn-success btn-sm">
                Word Export
            </button>
            <table  class="company_table_static table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th id="action">
                    </th>
                    <th> Location
                    </th>
                    <th> Score
                    </th>
                    <th> Max Visit
                    </th>
                    <th> Dashbroad
                    </th>
                    <th> Notes
                    </th>
                    
                </tr>
                </thead>
                    <tr>

                         <td >
                          <!--   <a  data-toggle="modal" data-target="#modalRegisterCompanycreate_doc" class="btn btn-primary btn-xs"><i class="fas fa-pencil-alt"></i></a> -->
                            <button type="button"class="btn btn-danger btn-xs my-0" onclick="deleteBonusdata()"><i class="fas fa-trash"></i></button>

                        </td>
                        <td class="pt-3-half" contenteditable="true">Lilly</td>

                        <td class="col-0">
                     <select searchable="Search here.."  style="background-color: transparent;border: 0px;">
  <option  disabled>Location</option>
  <option value="1" selected>Hamilton</option>
  <option value="2">LA</option>

</select>
                    </td>

                        <td class="pt-3-half" contenteditable="true">12</td>

                        <td class="pt-3-half" > 
                            <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="materialUnchecked">
                            <label class="form-check-label" for="materialUnchecked"></label>
</div>
                        </td>
                        <td class="pt-3-half" contenteditable="true">honest and sincere</td>
                       
                    </tr>
                <tbody>
                </tbody>
            </table>
                     </div>
            </div>
        </div>
             {{--    Edit Modal--}}
            @component('backend.bonus_score.model.edit')
            @endcomponent
            {{--    END Edit Modal--}}

            {{--    Register Modal--}}
            @component('backend.bonus_score.model.create_doc')
            @endcomponent
            {{--end modal register--}}
        </div>
    </div>

<script type="text/javascript">
     const $tableID = $('#table');
 const $BTN = $('#export-btn');
 const $EXPORT = $('#export');

 const newTr = `
<tr  role="row" class="hide odd">
                        <td><div class="form-check"><input type="checkbox" class="form-check-input" id="materialUncheckedTest"><label class="form-check-label" for="materialUncheckedTest"></label></div></td><td class="sorting_1">
                          <!--   <a  data-toggle="modal" data-target="#modalEditForm" class="btn btn-primary btn-xs"><i class="fas fa-pencil-alt"></i></a> -->
                            <a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light" onclick="deleteplant_and_equipmentdata()"><i class="fas fa-trash"></i></a>

                        </td>
                        <td class="pt-3-half" contenteditable="true">Laser Plant</td>
                        <td class="pt-3-half" contenteditable="true">Laser</td>
                        <td class="pt-3-half" contenteditable="true">007</td>
                        <td class="pt-3-half" contenteditable="true">2005</td>
                        <td class="pt-3-half" contenteditable="true">2010</td>
                        <td class="pt-3-half" contenteditable="true">28 Jan 2020</td>
                        <td class="pt-3-half" contenteditable="true">29 Feb 2020</td>
                        <td class="pt-3-half" contenteditable="true">Excellent</td>
                        <td class="pt-3-half" contenteditable="true">We use best products</td>
                        
                    </tr>`;

 $('.table-add').on('click', 'i', () => {

   const $clone = $tableID.find('tbody tr').last().clone(true).removeClass('hide table-line');

   if ($tableID.find('tbody tr').length === 0) {

     $('tbody').append(newTr);
   }

   $tableID.find('table').append($clone);
 });

 $tableID.on('click', '.table-remove', function () {

   $(this).parents('tr').detach();
 });

 $tableID.on('click', '.table-up', function () {

   const $row = $(this).parents('tr');

   if ($row.index() === 1) {
     return;
   }

   $row.prev().before($row.get(0));
 });

 $tableID.on('click', '.table-down', function () {

   const $row = $(this).parents('tr');
   $row.next().after($row.get(0));
 });

 // A few jQuery helpers for exporting only
 jQuery.fn.pop = [].pop;
 jQuery.fn.shift = [].shift;

 $BTN.on('click', () => {

   const $rows = $tableID.find('tr:not(:hidden)');
   const headers = [];
   const data = [];

   // Get the headers (add special header logic here)
   $($rows.shift()).find('th:not(:empty)').each(function () {

     headers.push($(this).text().toLowerCase());
   });

   // Turn all existing rows into a loopable array
   $rows.each(function () {
     const $td = $(this).find('td');
     const h = {};

     // Use the headers from earlier to name our hash keys
     headers.forEach((header, i) => {

       h[header] = $td.eq(i).text();
     });

     data.push(h);
   });

   // Output the result
   $EXPORT.text(JSON.stringify(data));
 });
      window.onload = function() {
    document.getElementById("action").classList.remove('sorting_asc');
};
function deleteBonusdata()
{

    swal({
        title:'Are you sure?',
        text: "Delete Record.",
        type: "error",
        showCloseButton: true,
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, Delete!",
        cancelButtonText: "Cancel!",
        closeOnConfirm: false,
        customClass: "confirm_class",
        closeOnCancel: false,
    },
    function(isConfirm){
        swal.close();
 });


}

</script>

@endsection