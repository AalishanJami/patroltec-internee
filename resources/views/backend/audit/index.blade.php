@extends('backend.layouts.backend')
@section('title', 'Audit')
@section('content')
    @php
        $data=localization();
    @endphp
    {{Breadcrumbs::render('audit')}}
    @include('backend.label.input_label')
    <div class="card">
        <div class="card-body">
            <div id="table" class="table-editable table-responsive">
                <table id="auditdatatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="th-sm assign_class label{{getKeyid('id',$data)}}" data-id="{{getKeyid('id',$data) }}" data-value="{{ checkKey('id',$data) }}" >    {!! checkKey('id',$data) !!}</th>
                            <th class="th-sm assign_class label{{getKeyid('url',$data)}}" data-id="{{getKeyid('url',$data) }}" data-value="{{ checkKey('url',$data) }}" >{!! checkKey('url',$data) !!}</th>
                            <th class="th-sm assign_class label{{getKeyid('tab_name',$data)}}" data-id="{{getKeyid('tab_name',$data) }}" data-value="{{ checkKey('tab_name',$data) }}" >{!! checkKey('tab_name',$data) !!}</th>
                             <th class="th-sm assign_class label{{getKeyid('event',$data)}}" data-id="{{getKeyid('event',$data) }}" data-value="{{ checkKey('event',$data) }}" >
                                {!! checkKey('event',$data) !!}
                            </th>
                            <th class="th-sm assign_class label{{getKeyid('name',$data)}}" data-id="{{getKeyid('name',$data) }}" data-value="{{ checkKey('name',$data) }}" >    {!! checkKey('name',$data) !!}</th>
                           <!--  <th class="th-sm assign_class label{{getKeyid('ip_address',$data)}}" data-id="{{getKeyid('ip_address',$data) }}" data-value="{{ checkKey('ip_address',$data) }}" >    {!! checkKey('ip_address',$data) !!}</th> -->
                            <th class="th-sm assign_class label{{getKeyid('new_values',$data)}}" data-id="{{getKeyid('new_values',$data) }}" data-value="{{ checkKey('new_values',$data) }}" >
                                {!! checkKey('new_values',$data) !!}
                            </th>
                            <th class="th-sm assign_class label{{getKeyid('old_values',$data)}}" data-id="{{getKeyid('old_values',$data) }}" data-value="{{ checkKey('old_values',$data) }}" >
                                {!! checkKey('old_values',$data) !!}
                            </th>
                            
                            <!-- <th class="th-sm assign_class label{{getKeyid('user_agent',$data)}}" data-id="{{getKeyid('user_agent',$data) }}" data-value="{{ checkKey('user_agent',$data) }}" >
                                {!! checkKey('user_agent',$data) !!}
                            </th> -->
                            <th class="th-sm assign_class label{{getKeyid('date',$data)}}" data-id="{{getKeyid('date',$data) }}" data-value="{{ checkKey('date',$data) }}" >
                                {!! checkKey('date',$data) !!}
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <input type="hidden" class="get_current_path" value="{{Request::path()}}">
    <script type="text/javascript">
        $(document).ready(function() {
            var current_path=$('.get_current_path').val();
            localStorage.setItem('current_path',current_path);
            $(function () {
                auditAppend();
            });
            (function ($) {
                var active ='<span class="assign_class label'+$('#audit_log_label').attr('data-id')+'" data-id="'+$('#audit_log_label').attr('data-id')+'" data-value="'+$('#audit_log_label').val()+'" >'+$('#audit_log_label').val()+'</span>';
                $('.breadcrumb-item.active').html(active);
                var parent ='<span class="assign_class label'+$('#dashboard_label').attr('data-id')+'" data-id="'+$('#dashboard_label').attr('data-id')+'" data-value="'+$('#dashboard_label').val()+'" >'+$('#dashboard_label').val()+'</span>';
                $('.breadcrumb-item a').html(parent);
            }(jQuery));

        });
        function auditAppend()
        {
            var url='/audit/getall';
            var table = $('#auditdatatable').dataTable({
                processing: true,
                serverSide: true,
                stateSave:true,
                order: [[ 0, "desc" ]],
                language: {
                    'lengthMenu': ' _MENU_ ',
                    'search':'<span class="assign_class label'+$('#search_label').attr('data-id')+'" data-id="'+$('#search_label').attr('data-id')+'" data-value="'+$('#search_label').val()+'" >'+$('#search_label').val()+'</span>',
                    'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                    'info': '<span class="assign_class label'+$('#showing_page_label').attr('data-id')+'" data-id="'+$('#showing_page_label').attr('data-id')+'" data-value="'+$('#showing_page_label').val()+'" >'+$('#showing_page_label').val()+'</span> _PAGE_ <span class="assign_class label'+$('#of_label').attr('data-id')+'" data-id="'+$('#of_label').attr('data-id')+'" data-value="'+$('#of_label').val()+'" >'+$('#of_label').val()+'</span> _PAGES_',
                    'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                    'paginate': {
                        'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                        'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                    },
                    'infoFiltered': "(filtered from _MAX_ total records)"
                },
                 "ajax": {
                    "url": url,
                    "type": 'get',
                },
                "createdRow": function( row, data, dataIndex,columns )
                {

                },
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'url', name: 'url'},
                    {data: 'tab_name', name: 'tab_name'},
                    {data: 'event', name: 'event'},
                    {data: 'name', name: 'name'},
                    // {data: 'ip_address', name: 'ip_address'},
                    {data: 'new_values', name: 'new_values'},
                    {data: 'old_values', name: 'old_values'},
                    {data: 'date', name: 'date'},
                ],

            });
        }
    </script>
@endsection
