@extends('backend.layouts.company')
@section('title', 'Company')
@section('content')
    @php
       $data=localization();
    @endphp

     {{ Breadcrumbs::render('companies') }}
    <link rel="stylesheet" href="{{ asset('custom/css/custom_style.css') }}">
    <div class="card">
        <div class="card-body">
            <div id="table" class="table-editable">
                @if(Auth()->user()->hasPermissionTo('create_company') || Auth::user()->all_companies == 1 )
                    <span class="table-add float-right mb-3 mr-2">
                        <a class="text-success" href=""  data-toggle="modal" data-target="#modalRegisterCompany">
                            <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
                        </a>
                    </span>
                @endif
                @if(Auth()->user()->hasPermissionTo('selected_delete_company') || Auth::user()->all_companies == 1 )
                    <button id="deleteSelectedCompany"  data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" class=" assign_class btn btn-danger btn-sm label{{getKeyid('selected_delete',$data)}}">
                         {!! checkKey('selected_delete',$data) !!}
                    </button>
                        <input type="hidden" id="deleteSelectedCompany_type" value="1">
                @endif
                @if(Auth()->user()->hasPermissionTo('selected_restore_delete_company') || Auth::user()->all_companies == 1 )
                    <button id="restorebutton" data-id="{{getKeyid('restore',$data) }}" data-value="{{checkKey('restore',$data) }}"   class="btn btn-primary btn-sm assign_class label{{getKeyid('restore',$data)}}">
                        {!! checkKey('restore',$data) !!}
                    </button>
                @endif
                @if(Auth()->user()->hasPermissionTo('selected_restore_delete_company') || Auth::user()->all_companies == 1 )
                    <button  style="display: none;" id="selectedactivebuttoncompant" class="btn btn-success btn-sm assign_class label{{getKeyid('selected_active',$data)}}"  data-id="{{getKeyid('selected_active',$data) }}" data-value="{{checkKey('selected_active',$data) }}" >
                        {!! checkKey('selected_active',$data) !!}
                    </button>
                @endif
                @if(Auth()->user()->hasPermissionTo('active_company') || Auth::user()->all_companies == 1 )
                    <button id="showactivebuttoncompany" data-id="{{getKeyid('show_active',$data) }}" data-value="{{checkKey('show_active',$data) }}"  class="btn btn-primary btn-sm assign_class label{{getKeyid('show_active',$data)}}" style="display: none;">
                        {!! checkKey('show_active',$data) !!}
                    </button>
                @endif
                @if(Auth()->user()->hasPermissionTo('csv_company') || Auth::user()->all_companies == 1 )
                    <form class="form-style export_company_show_hide" method="POST" action="{{ url('export_excel_company') }}">
                        <input type="hidden" id="export_excel_company" name="export_excel_company" value="1">
                        <input type="hidden" class="company_export" name="export_array" value="1">
                        {!! csrf_field() !!}
                        <button  type="submit" data-id="{{getKeyid('excel_export',$data) }}" data-value="{{checkKey('excel_export',$data) }}"  class="btn btn-warning btn-sm assign_class label{{getKeyid('excel_export',$data)}}">
                            {!! checkKey('excel_export',$data) !!}
                        </button>
                    </form>
                @endif
                @if(Auth()->user()->hasPermissionTo('word_company') || Auth::user()->all_companies == 1 )
                    <form  class="form-style export_company_show_hide"  method="POST" action="{{ url('export_word_company') }}">
                        <input type="hidden" id="export_word_company" name="export_word_company" value="1">
                        <input type="hidden" class="company_export" name="word_array" value="1">
                        {!! csrf_field() !!}
                         <button  type="submit" data-id="{{getKeyid('word_export',$data) }}" data-value="{{checkKey('word_export',$data) }}"  class="btn btn-success btn-sm assign_class label{{getKeyid('word_export',$data)}}">
                            {!! checkKey('word_export',$data) !!}
                        </button>
                    </form>
                @endif
{{--                @if(Auth()->user()->hasPermissionTo('pdf_company') || Auth::user()->all_companies == 1 )--}}
                    <form  class="form-style export_company_show_hide"  method="POST" action="{{ url('export_pdf_company') }}">
                        <input type="hidden" id="export_word_company" name="export_word_company" value="1">
                        <input type="hidden" class="company_export" name="pdf_array" value="1">
                        {!! csrf_field() !!}
                        <button  type="submit" data-id="{{getKeyid('pdf_export',$data) }}" data-value="{{checkKey('pdf_export',$data) }}"  class="btn btn-danger btn-sm assign_class label{{getKeyid('pdf_export',$data)}}">
                            {!! checkKey('pdf_export',$data) !!}
                        </button>
                    </form>
{{--                @endif--}}
                @include('backend.label.input_label')
                <table id="company_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="th-sm no-sort all_checkboxes_style">
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input company_checked" id="company_checkbox_all">
                                <label class="form-check-label" for="company_checkbox_all">
                                    <span class="assign_class label{{getKeyid('all',$data)}}" data-id="{{getKeyid('all',$data) }}" data-value="{{checkKey('all',$data) }}" >
                                        {!! checkKey('all',$data) !!}
                                    </span>
                                </label>
                            </div>
                        </th>

                        <th class="th-sm assign_class label{{getKeyid('name',$data)}}" data-id="{{getKeyid('name',$data) }}" data-value="{{ checkKey('name',$data) }}" >
                            {!! checkKey('name',$data) !!}
                        </th>
                        <th class="th-sm assign_class label{{getKeyid('notes',$data)}}" data-id="{{getKeyid('notes',$data) }}" data-value="{{ checkKey('notes',$data) }}">
                            Notes
                        </th>
                        <th id="active" class="all_action_btn_extra"></th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                 {{--    Edit Modal--}}
                @component('backend.company.model.edit')
                @endcomponent
                {{--    END Edit Modal--}}

                {{--    Register Modal--}}
                @component('backend.company.model.create')
                @endcomponent
                {{--end modal register--}}
                @if(Auth()->user()->all_companies == 1)
                <input type="hidden" id="checkbox_permission" value="1">
                <input type="hidden" id="name_permission" value="1">
                @else
                <input type="hidden" id="checkbox_permission" value="{{Auth()->user()->hasPermissionTo('company_checkbox')}}">
                <input type="hidden" id="name_permission" value="{{Auth()->user()->hasPermissionTo('company_name')}}">
                @endif
            </div>
        </div>
    </div>

<script type="text/javascript">
    $(document).ready(function() {
        setTimeout(function(){
           $(".companies_selected:nth-child(1)>input").attr('style','display:none;')
        },500);
        $(function () {
            var company_url='getallcompany';
            companyAppend(company_url);
             var active ='<span class="assign_class label'+$('#manage_companies_label').attr('data-id')+'" data-id="'+$('#manage_companies_label').attr('data-id')+'" data-value="'+$('#manage_companies_label').val()+'" >'+$('#manage_companies_label').val()+'</span>';
            $('.breadcrumb-item.active').html(active);
            var parent ='<span class="assign_class label'+$('#dashboard_label').attr('data-id')+'" data-id="'+$('#dashboard_label').attr('data-id')+'" data-value="'+$('#dashboard_label').val()+'" >'+$('#dashboard_label').val()+'</span>';
            $('.breadcrumb-item a').html(parent);
        });

            // editor = new $.fn.dataTable.Editor( {
            //         "ajaxUrl": "/getallcompany",
            //         "domTable": "#company_table",
            //         fields: [ {
            //                 label: "name:",
            //                 name: "name"
            //             }
            //         ]
            //     });
            // $('#company_table').on( 'click', 'tbody td:not(:first-child)', function (e) {
            //     editor.inline( this );
            // } );

        });
        function companyAppend(url)
        {
            var table = $('#company_table').dataTable({
                processing: true,
                language: {
                    'lengthMenu': '_MENU_ ',
                    'search':'<span class="assign_class label'+$('#search_label').attr('data-id')+'" data-id="'+$('#search_label').attr('data-id')+'" data-value="'+$('#search_label').val()+'" >'+$('#search_label').val()+'</span>',
                    'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                    'info': '<span class="assign_class label'+$('#showing_page_label').attr('data-id')+'" data-id="'+$('#showing_page_label').attr('data-id')+'" data-value="'+$('#showing_page_label').val()+'" >'+$('#showing_page_label').val()+'</span> _PAGE_ <span class="assign_class label'+$('#of_label').attr('data-id')+'" data-id="'+$('#of_label').attr('data-id')+'" data-value="'+$('#of_label').val()+'" >'+$('#of_label').val()+'</span> _PAGES_',
                    'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                    'paginate': {
                        'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                        'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                    },
                    'infoFiltered': "(filtered from _MAX_ total records)"
                },
                "ajax": {
                    "url": url,
                    "type": 'get',
                },
              "createdRow": function( row, data, dataIndex,columns ) {
                  var checkbox='<div class="form-check"><input type="checkbox" class="form-check-input selectedrowcompany company_checked" id="company_check'+data.id+'"><label class="form-check-label" for="company_check'+data.id+'"></label></div>';
                  $(columns[0]).html(checkbox);
                    $(row).attr('id', 'company_tr'+data['id']);
                    var temp=data['id'];
                    $(row).attr('data-id',data['id']);
                    //$(row).attr('onclick',"selectedrow(this.id)");
                },
                columns: [
                    {data: 'checkbox', name: 'checkbox',visible:$('#checkbox_permission').val(),},
                    {data: 'name', name: 'name',visible:$('#name_permission').val(),},
                    {data: 'field_notes', name: 'field_notes'},
                    {data: 'actions', name: 'actions'},
                ],
            });
            if ($(":checkbox").prop('checked',true)){
                $(":checkbox").prop('checked',false);
            }
        }
</script>
    <script type="text/javascript" src="{{ asset('custom/js/company.js') }}" ></script>

@endsection
