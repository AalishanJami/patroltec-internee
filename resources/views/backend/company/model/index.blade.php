 @php
        $data=localization();
@endphp
<style type="text/css">
   .form-check-input {
    position: absolute !important;
    pointer-events: none !important;
    opacity: 0!important;
}
 @media (min-width: 576px) {
      .modal-dialog {
        max-width: 75% !important;
      }
    }
</style>
<div class="modal fade" id="modalCompanyIndex" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold"><span class="assign_class label{{getKeyid('new_client',$data)}}" data-id="{{getKeyid('new_client',$data) }}" data-value="{{checkKey('new_client',$data) }}" ><!-- {!! checkKey('new_client',$data) !!} --> Company Listing </span></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <div class="col-md-12">
            <span class="table-add float-right mb-3 mr-2">
            <a class="text-success"  data-toggle="modal" data-target="#modalRegisterCompany">
                <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
            </a>
            </span>
            <button class="btn btn-danger btn-sm">
                Delete Selected Company
            </button>
            <button class="btn btn-primary btn-sm">
              Restore Deleted Company
            </button>

            <button  style="display: none;">
                Show Active Company
            </button>
            <button class="btn btn-warning btn-sm">
               Excel Export
            </button>
            <button class="btn btn-success btn-sm">
                Word Export
            </button>

        </div>

   <table id="company_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th> Title
            </th>
            <th> First Name
            </th>
            <th> Surname
            </th>
            <th> Phone
            </th>
            <th> email
            </th>
            <th>address
            </th>
            <th id="action">
            </th>
        </tr>
        </thead>
            <tr>
                <td class="pt-3-half" contenteditable="true">Worker</td>
                <td class="pt-3-half" contenteditable="true">Imran Khan</td>
                <td class="pt-3-half" contenteditable="true">Imran</td>
                <td class="pt-3-half" contenteditable="true">0900078601</td>
                <td class="pt-3-half" contenteditable="true">imranshah@gmail.com</td>
                <td class="pt-3-half" contenteditable="true">Islamabad,Pakistan</td>
                <td>
                    <a  data-toggle="modal" data-target="#modalEditFormCompany" class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i></a>
                    <button type="button"class="btn btn-danger btn-sm my-0" onclick="deleteclientdata()"><i class="fas fa-trash"></i></button>

                </td>
            </tr>
        <tbody>
        </tbody>
    </table>
</div>
    </div>
</div>

    @component('backend.company.model.create')
    @endcomponent

     {{--    Edit Modal--}}
    @component('backend.company.model.edit')
    @endcomponent
    {{--    END Edit Modal--}}
        <script type="text/javascript">
            function deleteclientdata()
            {

                swal({
                    title:'Are you sure?',
                    text: "Delete Record.",
                    type: "error",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, Delete!",
                    cancelButtonText: "Cancel!",
                    closeOnConfirm: false,
                    customClass: "confirm_class",
                    closeOnCancel: false,
                },
                function(isConfirm){
                    swal.close();
             });


            }

        </script>
