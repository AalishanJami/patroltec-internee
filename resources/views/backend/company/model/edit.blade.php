@php
    $data=localization();
@endphp
<div class="modal fade" id="modalEditFormCompany" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold text-center">
                  <span class="assign_class label{{getKeyid('title',$data)}}" data-id="{{getKeyid('title',$data) }}" data-value="{{checkKey('title',$data) }}" >
                    {!! checkKey('title',$data) !!}
                  </span>
                        @if(Auth()->user()->hasPermissionTo('view_cms') || Auth::user()->all_companies == 1 )
                            <button data-class="edit-disable" class="edit_cms_enable edit_cms_switch btn btn-primary btn-sm waves-effect waves-light" style="display: none;" >
                                Edit cms
                            </button>
                            <button  data-class="edit-enable" class="edit_cms_disable edit_cms_switch btn btn-danger btn-sm waves-effect waves-light">
                                Edit cms
                            </button>
                        @endif
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="edit_company_form_submit" class="" action="javascript:;" method="POST">
                @csrf
                <input type="hidden" id="company_id_hidden_edit" class="company_id_hidden_edit">
                <div class="modal-body mx-3">
                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        {{--                        <label data-error="wrong" data-success="right" for="name">--}}
                        <span style="margin-left: 40px;" class="assign_class label{{getKeyid('name',$data)}}" data-id="{{getKeyid('name',$data) }}" data-value="{{checkKey('name',$data) }}" >
                            {!! checkKey('name',$data) !!}
                        </span>
                        @php
                            $id=getKeyid('company.model.edit.name.msg',$data);
                            $value=checkKey('company.model.edit.name.msg',$data);
                            $title=$id.'---'.$value;
                        @endphp
                        <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                           data-content="{{$title}}"></i>
                        {{--                        </label>--}}
                        <input id="edit_user_name" type="text" class="form-control" name="name">
                    </div>
                    <div class="md-form mb-5">
                        <div class="md-form mb-5">
                            <div id="document-full-edit" class="ql-scroll-y" style="height: 300px;">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer d-flex justify-content-center">
                    <button class="btn btn-primary btn-sm edit_company_name_btn" id="edit_company_name_btn" type="submit">
                        <span class="assign_class label{{getKeyid('update',$data)}}" data-id="{{getKeyid('update',$data) }}" data-value="{{checkKey('update',$data) }}" >
                          {!! checkKey('update',$data) !!}
                        </span>
                    </button>
                </div>
            </form>
            <div class="modal-body mx-3"></div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var toolbarOptions = [
        ['bold', 'italic', 'underline'], // toggled buttons
        ['blockquote'],

        // custom button values
        [{
            'list': 'ordered'
        }, {
            'list': 'bullet'
        }],
        [{
            'indent': '-1'
        }, {
            'indent': '+1'
        }], // outdent/indent


        // dropdown with defaults from theme

        [{
            'align': []
        }],
        // remove formatting button
    ];

    var quillFull = new Quill('#document-full-edit', {
        modules: {
            toolbar: toolbarOptions,
            autoformat: true
        },
        theme: 'snow',
        placeholder: "Write something..."
    });

</script>
