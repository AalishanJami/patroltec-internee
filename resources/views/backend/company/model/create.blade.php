@php
        $data=localization();
@endphp
<div class="modal fade" id="modalRegisterCompany" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">
                  <span class="assign_class label{{getKeyid('title',$data)}}" data-id="{{getKeyid('title',$data) }}" data-value="{{checkKey('title',$data) }}" >
                    {!! checkKey('title',$data) !!}
                  </span>
                  @if(Auth()->user()->hasPermissionTo('view_cms') || Auth::user()->all_companies == 1 )
                    <button data-class="edit-disable" class="edit_cms_enable edit_cms_switch btn btn-primary btn-sm waves-effect waves-light" style="display: none;" >
                        Edit cms
                    </button>
                    <button  data-class="edit-enable" class="edit_cms_disable edit_cms_switch btn btn-danger btn-sm waves-effect waves-light">
                        Edit cms
                    </button>
                  @endif
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="register_new_company" action="javascript:;" method="POST" >
                @csrf
                <div class="modal-body mx-3">
                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input id="new_company" type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                        <label data-error="wrong" data-success="right" for="name">
                          <span class="assign_class label{{getKeyid('name',$data)}}" data-id="{{getKeyid('name',$data) }}" data-value="{{checkKey('name',$data) }}" >
                            {!! checkKey('name',$data) !!}
                          </span>
                        </label>
                    </div>
                    <div class="md-form mb-5">
                       <div class="md-form mb-5">
                         <div id="document-full-create" class="ql-scroll-y" style="height: 300px;"></div>
                      </div>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button class="form_submit_check btn btn-primary btn-sm" id="register_new_company_btn" type="submit">
                      <span class="assign_class label{{getKeyid('save',$data)}}" data-id="{{getKeyid('save',$data) }}" data-value="{{checkKey('save',$data) }}" >
                        {!! checkKey('save',$data) !!}
                      </span>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="{{asset('editor/sprite.svg.js')}}"></script>
<script type="text/javascript">
   var toolbarOptions = [
      ['bold', 'italic', 'underline'], // toggled buttons
      ['blockquote'],

     // custom button values
      [{
        'list': 'ordered'
      }, {
        'list': 'bullet'
      }],
      [{
        'indent': '-1'
      }, {
        'indent': '+1'
      }], // outdent/indent


       // dropdown with defaults from theme

      [{
        'align': []
      }],
     // remove formatting button
    ];

    var quillFull = new Quill('#document-full-create', {
      modules: {
        toolbar: toolbarOptions,
        autoformat: true
      },
      theme: 'snow',
      placeholder: "Write something..."
    });

</script>
