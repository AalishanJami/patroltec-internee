@include('backend.layouts.pdf_start')
  <thead>
    <tr>
        <th style="vertical-align: text-top;" class="pdf_table_layout"> Name</th>
        <th style="vertical-align: text-top;" class="pdf_table_layout"> Note</th>
    </tr>
  </thead>
  <tbody>
  @foreach($data as $row)
    <tr>
        <td class="pdf_table_layout"><p class="col_text_style">{{ $row->name }}</p></td>
        <td class="pdf_table_layout"><p class="col_text_style">{{ $row->field_notes }}</p></td>
    </tr>
  @endforeach
  </tbody>
@include('backend.layouts.pdf_end')
