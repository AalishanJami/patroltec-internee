@if(Auth()->user()->all_companies == 1)
    <input type="hidden" id="scoring_checkbox" value="1">
    <input type="hidden" id="delete_scoring" value="1">
    <input type="hidden" id="scoring_location" value="1">
    <input type="hidden" id="scoring_max_visit" value="1">
    <input type="hidden" id="scoring_score" value="1">
    <input type="hidden" id="scoring_notes" value="1">
    <input type="hidden" id="scoring_dashbroad" value="1">
    <input type="hidden" id="edit_scoring" value="1">
    <!---------Create---------->
    <input type="hidden" id="scoring_location_create" value="1">
    <input type="hidden" id="scoring_max_visit_create" value="1">
    <input type="hidden" id="scoring_score_create" value="1">
    <input type="hidden" id="scoring_notes_create" value="1">
    <input type="hidden" id="scoring_dashbroad_create" value="1">

    <!---------edit---------->
    <input type="hidden" id="scoring_location_edit" value="1">
    <input type="hidden" id="scoring_max_visit_edit" value="1">
    <input type="hidden" id="scoring_score_edit" value="1">
    <input type="hidden" id="scoring_notes_edit" value="1">
    <input type="hidden" id="scoring_dashbroad_edit" value="1">
@else
    <input type="hidden" id="scoring_checkbox" value="{{Auth()->user()->hasPermissionTo('scoring_checkbox')}}">
    <input type="hidden" id="delete_scoring" value="{{Auth()->user()->hasPermissionTo('delete_scoring')}}">
    <input type="hidden" id="scoring_location" value="{{Auth()->user()->hasPermissionTo('scoring_location')}}">
    <input type="hidden" id="scoring_max_visit" value="{{Auth()->user()->hasPermissionTo('scoring_max_visit')}}">
    <input type="hidden" id="scoring_score" value="{{Auth()->user()->hasPermissionTo('scoring_score')}}">
    <input type="hidden" id="scoring_notes" value="{{Auth()->user()->hasPermissionTo('scoring_notes')}}">
    <input type="hidden" id="scoring_dashbroad" value="{{Auth()->user()->hasPermissionTo('scoring_dashbroad')}}">

    <input type="hidden" id="edit_scoring" value="{{Auth()->user()->hasPermissionTo('edit_scoring')}}">

    <!---------Create---------->
    <input type="hidden" id="scoring_location_create" value="{{Auth()->user()->hasPermissionTo('scoring_location_create')}}">
    <input type="hidden" id="scoring_max_visit_create" value="{{Auth()->user()->hasPermissionTo('scoring_max_visit_create')}}">
    <input type="hidden" id="scoring_score_create" value="{{Auth()->user()->hasPermissionTo('scoring_score_create')}}">
    <input type="hidden" id="scoring_notes_create" value="{{Auth()->user()->hasPermissionTo('scoring_notes_create')}}">
    <input type="hidden" id="scoring_dashbroad_create" value="{{Auth()->user()->hasPermissionTo('scoring_dashbroad_create')}}">

    <!---------edit---------->
    <input type="hidden" id="scoring_location_edit" value="{{Auth()->user()->hasPermissionTo('scoring_location_edit')}}">
    <input type="hidden" id="scoring_max_visit_edit" value="{{Auth()->user()->hasPermissionTo('scoring_max_visit_edit')}}">
    <input type="hidden" id="scoring_score_edit" value="{{Auth()->user()->hasPermissionTo('scoring_score_edit')}}">
    <input type="hidden" id="scoring_notes_edit" value="{{Auth()->user()->hasPermissionTo('scoring_notes_edit')}}">
    <input type="hidden" id="scoring_dashbroad_edit" value="{{Auth()->user()->hasPermissionTo('scoring_dashbroad_edit')}}">

@endif


