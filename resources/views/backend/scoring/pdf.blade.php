@include('backend.layouts.pdf_start')
    <thead>
    <tr>
        @if(Auth()->user()->hasPermissionTo('scoring_location_pdf_export') || Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout"> Location
            </th>
        @endif
        @if(Auth()->user()->hasPermissionTo('scoring_score_pdf_export') || Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout"> Score
            </th>
        @endif
        @if(Auth()->user()->hasPermissionTo('scoring_max_visit_pdf_export') || Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout"> Max Visits
            </th>
        @endif
        @if(Auth()->user()->hasPermissionTo('scoring_dashbroad_pdf_export') || Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout"> Dashboard Name
            </th>
        @endif
        @if(Auth()->user()->hasPermissionTo('scoring_notes_pdf_export') || Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout"> Notes
            </th>
        @endif
    </tr>
    </thead>
    <tbody>
    @foreach($data as $value)
        <tr>
            @if(Auth()->user()->hasPermissionTo('scoring_location_pdf_export') || Auth::user()->all_companies == 1 )
               <td class="pdf_table_layout"><p class="col_text_style">
                    @if(isset($value->location->name))
                        {{ $value->location->name }}
                    @else
                        All Location
                    @endif
                </p></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('scoring_score_pdf_export') || Auth::user()->all_companies == 1 )
               <td class="pdf_table_layout"><p class="col_text_style">{{ $value->score }}</p></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('scoring_max_visit_pdf_export') || Auth::user()->all_companies == 1 )
               <td class="pdf_table_layout"><p class="col_text_style">{{ $value->max_visits }}</p></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('scoring_dashbroad_pdf_export') || Auth::user()->all_companies == 1 )
               <td class="pdf_table_layout"><p class="col_text_style">{{ $value->dashboard_name }}</p></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('scoring_notes_pdf_export') || Auth::user()->all_companies == 1 )
               <td class="pdf_table_layout"><p class="col_text_style">{{ $value->notes }}</p></td>
            @endif
        </tr>
    @endforeach
    </tbody>
@include('backend.layouts.pdf_end')