@extends('backend.layouts.doc')
@section('title', 'Scoring')
@section('content')
    @php
        $data=localization();
    @endphp

    <link rel="stylesheet" href="{{ asset('custom/css/custom_style.css') }}">
    @include('backend.layouts.doc_sidebar')
    <div class="padding-left-custom remove-padding">
        {!! breadcrumInner('scoring',Session::get('form_id'),'forms','/document/detail/'.Session::get('form_id'),'documents','/form_group') !!}
        <div class="locations-form container-fluid form_body">
            <div class="card">
                <div class="card-body">
                    <div id="table-scoring" class="table-editable">
                        @if(Auth()->user()->hasPermissionTo('create_scoring') || Auth::user()->all_companies == 1 )
                            <span class="add-scoring table-add float-right mb-3 mr-2">
                          <a class="text-success">
                            <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
                          </a>
                        </span>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_delete_scoring') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-danger btn-sm" id="deleteselectedscoring">
                              <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                                {!! checkKey('selected_delete',$data) !!}
                              </span>
                            </button>
                            <button class="btn btn-danger btn-sm" style="display: none;" id="deleteselectedscoringSoft">
                              <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                                {!! checkKey('selected_delete',$data) !!}
                              </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_active_scoring') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-success btn-sm" style="display: none;" id="selectedactivebuttonscoring">
                                <span class="assign_class label{{getKeyid('selected_active',$data)}}" data-id="{{getKeyid('selected_active',$data) }}" data-value="{{checkKey('selected_active',$data) }}" >
                                    {!! checkKey('selected_active',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('restore_delete_scoring') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-primary btn-sm" id="restorebuttonscoring">
                              <span class="assign_class label{{getKeyid('restore',$data)}}" data-id="{{getKeyid('restore',$data) }}" data-value="{{checkKey('restore',$data) }}" >
                                {!! checkKey('restore',$data) !!}
                              </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('active_scoring') || Auth::user()->all_companies == 1 )
                            <button  style="display: none;" class="btn btn-primary btn-sm" id="activebuttonscoring">
                                <span class="assign_class label{{getKeyid('show_active',$data)}}" data-id="{{getKeyid('show_active',$data) }}" data-value="{{checkKey('show_active',$data) }}" >
                                    {!! checkKey('show_active',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('csv_scoring') || Auth::user()->all_companies == 1 )
                            <form class="form-style" method="POST" id="export_excel_scoring" action="{{ url('/document/bonus/score/export/excel') }}">
                                {!! csrf_field() !!}
                                <input type="hidden" class="scoring_export" name="excel_array" value="1">
                                <button  type="submit" class="form_submit_check btn btn-warning btn-sm">
                                    <span class="assign_class label{{getKeyid('excel_export',$data)}}" data-id="{{getKeyid('excel_export',$data) }}" data-value="{{checkKey('excel_export',$data) }}" >
                                        {!! checkKey('excel_export',$data) !!}
                                    </span>
                                </button>
                            </form>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('word_scoring') || Auth::user()->all_companies == 1 )
                            <form class="form-style" method="POST" id="export_world_scoring" action="{{ url('/document/bonus/score/export/world') }}">
                                {!! csrf_field() !!}
                                <input type="hidden" class="scoring_export" name="word_array" value="1">
                                <button  type="submit" class="form_submit_check btn btn-success btn-sm">
                                    <span class="assign_class label{{getKeyid('word_export',$data)}}" data-id="{{getKeyid('word_export',$data) }}" data-value="{{checkKey('word_export',$data) }}" >
                                        {!! checkKey('word_export',$data) !!}
                                    </span>
                                </button>
                            </form>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('pdf_scoring') || Auth::user()->all_companies == 1 )
                            <form  class="form-style" {{pdf_view('scorings')}}  method="POST" id="export_pdf_scoring" action="{{ url('document/bonus/score/export/pdf') }}">
                                {!! csrf_field() !!}
                                <input type="hidden" class="scoring_export" name="pdf_array" value="1">
                                <button  type="submit" class="form_submit_check btn btn-secondary btn-sm">
                                    <span class="assign_class label{{getKeyid('pdf_export',$data)}}" data-id="{{getKeyid('pdf_export',$data) }}" data-value="{{checkKey('pdf_export',$data) }}" >
                                        {!! checkKey('pdf_export',$data) !!}
                                    </span>
                                </button>
                            </form>
                        @endif
                        <div class="table-editable">
                            <table id="scoring_data"  class="scoring_table_static table table-bordered table-responsive-md table-striped">
                                <thead>
                                <tr>
                                    <th class="no-sort all_checkboxes_style">
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input bonusscore_checked" id="bonusscore_checkbox_all">
                                            <label class="form-check-label" for="bonusscore_checkbox_all">
                                                <span class="assign_class label{{getKeyid('all',$data)}}" data-id="{{getKeyid('all',$data) }}" data-value="{{checkKey('all',$data) }}" >
                                                    {!! checkKey('all',$data) !!}
                                                </span>
                                            </label>
                                        </div>
                                    </th>
                                    <th>
                                        <span class="assign_class label{{getKeyid('location',$data)}}" data-id="{{getKeyid('location',$data) }}" data-value="{{checkKey('location',$data) }}" >
                                          {!! checkKey('location',$data) !!}
                                        </span>
                                    </th>
                                    <th>
                                        <span class="assign_class label{{getKeyid('score',$data)}}" data-id="{{getKeyid('score',$data) }}" data-value="{{checkKey('score',$data) }}" >
                                          {!! checkKey('score',$data) !!}
                                        </span>
                                    </th>
                                    <th>
                                        <span class="assign_class label{{getKeyid('max_visit',$data)}}" data-id="{{getKeyid('max_visit',$data) }}" data-value="{{checkKey('max_visit',$data) }}" >
                                          {!! checkKey('max_visit',$data) !!}
                                        </span>
                                    </th>
                                    <th>
                                        <span class="assign_class label{{getKeyid('dashboard',$data)}}" data-id="{{getKeyid('dashboard',$data) }}" data-value="{{checkKey('dashboard',$data) }}" >
                                          {!! checkKey('dashboard',$data) !!}
                                        </span>
                                    </th>
                                    <th>
                                        <span class="assign_class label{{getKeyid('notes',$data)}}" data-id="{{getKeyid('notes',$data) }}" data-value="{{checkKey('notes',$data) }}" >
                                          {!! checkKey('notes',$data) !!}
                                        </span>
                                    </th>
                                    <th class="no-sort all_action_btn" id="action"></th>

{{--                                    <th class="no-sort"></th>--}}
                                </tr>
                                </thead>
                                <tbody >
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backend.label.input_label')
    @include('backend.scoring.input_permission')
    <script type="text/javascript">
        function checkSelectedScoring(value,checkValue)
        {
            if(value == checkValue)
            {
                return 'selected';
            }
            else
            {
                return "";
            }
        }
        function scoring_data(url)
        {
            $.ajax({
                type: 'GET',
                url: url,
                success: function(response)
                {
                    $('#scoring_data').DataTable().clear().destroy();
                    var table = $('#scoring_data').dataTable(
                        {
                            processing: true,
                            serverSide: true,
                            stateSave:true,
                            language: {
                                'lengthMenu': '  _MENU_ ',
                                'search':'<span class="assign_class label'+$('#search_id').attr('data-id')+'" data-id="'+$('#search_id').attr('data-id')+'" data-value="'+$('#search_id').val()+'" >'+$('#search_id').val()+'</span>',
                                'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                                'info':'',
                                'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                                'paginate': {
                                    'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                                    'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                                },
                                'infoFiltered': "(filtered from _MAX_ total records)"
                            },
                            "ajax": {
                                "url": url,
                                "type": 'get',
                            },
                            "createdRow": function( row, data, dataIndex,columns )
                            {
                                var checkbox_permission=$('#checkbox_permission').val();
                                var checkbox='';
                                checkbox+='<div class="form-check"><input type="checkbox" class="form-check-input bonusscore_checked selectedrowscoring" data-id="'+data.id+'" id="scoring'+data.id+'"><label class="form-check-label" for="scoring'+data.id+'""></label></div>';
                                var submit='';
                                if ($('#edit_scoring').val()) {
                                    submit+='<a  type="button" class="btn btn-primary btn-xs my-0 all_action_btn_margin waves-effect waves-light savescoredata show_tick_btn'+data.id+'" id="'+data.id+'" style="display:none"><i class="fas fa-check"></i></a>';
                                }
                                var location_edit='';
                                if (!$("#scoring_location_edit").val() || !$('#edit_scoring').val()) {
                                    var location_edit='disabled';
                                }
                                var location='';
                                location+='<input type="hidden" id="old_location'+data.id+'" value="'+data.location_id+'">';
                                location+='<select '+location_edit+' onchange="editSelectedRow('+"scoring_tr"+data.id+')" class="location" searchable="Search here.."  id="location_id'+data.id+'"  style="background-color: inherit;border: 0px">';
                                location+=' <option value=" ">Select Location</option>';
                                if(!response.flag_check)
                                {
                                    location+='<option value="0">All Location</option>';
                                }
                                if(data.all_location == 1)
                                {
                                    location+='<option value="0"  '+checkSelectedScoring(0,0)+' >All Location</option>';
                                }
                                // else
                                // {
                                //     location+='<option value="0"  '+checkSelectedScoring(0,0)+' >All Location</option>';
                                // }

                                // if (data.all_location !==null){
                                //     location+='<option value="'+data.location.id+'" selected >'+data.location.name+'</option>';
                                // }else{
                                //     location+='<option value="0"  '+checkSelectedScoring(0,0)+' >All Location</option>';
                                // }
                                for (var j = response.all_location.length - 1; j >= 0; j--) {

                                        location+='<option value="'+response.all_location[j].id+'" '+checkSelectedScoring(data.location_id,response.all_location[j].id)+'  >'+response.all_location[j].name+'</option>';
                                    // if (response.all_location[j].score_location ==null ){
                                    // }
                                    // else
                                    // {
                                    //     console.log(response.all_location[j],data);
                                    //     if(response.all_location[j].id==data.location_id)
                                    //     {
                                    //         location+='<option value="'+response.all_location[j].id+'" '+checkSelectedScoring(data.location_id,response.all_location[j].id)+'  >'+response.all_location[j].name+'</option>';
                                    //     }
                                    // }
                                }
                                location+='</select>';
                                console.log(location);
                                var dashboard='';
                                var dashboard_edit='';
                                if (!$("#scoring_dashbroad_edit").val() || !$('#edit_scoring').val() ) {
                                    var dashboard_edit='disabled';
                                }
                                dashboard+='<select '+dashboard_edit+' onchange="editSelectedRow('+"scoring_tr"+data.id+')" searchable="Search here.."  id="dashboard_name'+data.id+'"  style="background-color: inherit;border: 0px">';
                                dashboard+='<option value="yes"'+checkSelectedScoring("yes",data.dashboard_name)+'>YES</option>';
                                dashboard+='<option value="no"'+checkSelectedScoring("no",data.dashboard_name)+'>NO</option>';
                                dashboard+='</select>';

                                $(columns[0]).html(checkbox);
                                if ($("#scoring_score_edit").val()  && $('#edit_scoring').val() ) {
                                    if (data.flag==1){
                                        $(columns[2]).attr('Contenteditable', 'true');
                                    }
                                }
                                $(columns[1]).attr('Contenteditable', 'false');
                                $(columns[1]).attr('class','edit_inline_scoring');
                                // $(columns[1]).html(location);
                                $(columns[1]).attr('data-id',data['id']);

                                $(columns[2]).attr('id', 'score'+data['id']);
                                $(columns[2]).attr('class','edit_inline_scoring');
                                $(columns[2]).attr('data-id',data['id']);
                                $(columns[2]).attr('onkeydown', 'editSelectedRow(scoring_tr'+data['id']+')');
                                $(columns[3]).attr('id','max_visits'+data['id']);
                                $(columns[3]).attr('class','edit_inline_scoring');
                                if ($("#scoring_max_visit_edit").val() && $('#edit_scoring').val()) {
                                    if (data.flag==1){
                                        $(columns[3]).attr('Contenteditable', 'true');
                                    }
                                }
                                $(columns[3]).attr('onkeydown', 'editSelectedRow(scoring_tr'+data['id']+')');

                                $(columns[3]).attr('data-id',data['id']);
                                $(columns[4]).attr('class','edit_inline_scoring');
                                $(columns[4]).attr('data-id',data['id']);
                                $(columns[4]).html(dashboard);

                                $(columns[5]).attr('id', 'notes'+data['id']);
                                $(columns[5]).attr('class','edit_inline_scoring');
                                $(columns[5]).attr('data-id',data['id']);
                                $(columns[5]).attr('onkeydown', 'editSelectedRow(scoring_tr'+data['id']+')');

                                if ($("#scoring_notes_edit").val() && $('#edit_scoring').val()) {
                                    if (data.flag==1){
                                        $(columns[5]).attr('Contenteditable', 'true');
                                    }
                                }
                                if (data.flag==1){
                                    $(columns[6]).append(submit);
                                }
                                $(row).attr('id', 'scoring_tr'+data['id']);
                                $(row).attr('data-id', data['id']);
                            },
                            columns:
                                [
                                    {data: 'checkbox', name: 'checkbox',visible:$('#scoring_checkbox').val()},
                                    {data: 'location_id', name: 'location_id',visible:$('#scoring_location').val()},
                                    {data: 'score', name: 'score',visible:$('#scoring_score').val()},
                                    {data: 'max_visits', name: 'max_visits',visible:$('#scoring_max_visit').val()},
                                    {data: 'dashboard_name', name: 'dashboard_name',visible:$('#scoring_dashbroad').val()},
                                    {data: 'notes', name: 'notes',visible:$('#scoring_notes').val()},
                                    // {data: 'submit', name: 'submit'},
                                    {data: 'actions', name: 'actions',visible:$('#delete_scoring').val()},
                                ],
                                columnDefs: [ {
                                    'targets': [0,6], /* column index */
                                    'orderable': false, /* true or false */
                                }],
                        }
                    );
                },
                error: function (error) {
                    console.log(error);
                }
            });
            if ($(":checkbox").prop('checked',true)){
                $(":checkbox").prop('checked',false);
            }
        }
        $('.savescoredata').hide();
        $(document).ready(function () {
            $("body").on('keyup','.edit_inline_scoring',function () {
                $(".show_tick_btn"+$(this).attr('data-id')).show();
                $('.delete-btn'+$(this).attr('data-id')).hide();
            });
            var url='/document/bonus/score/getall';
            scoring_data(url);
            // (function ($) {
            //     var active ='<span class="assign_class label'+$('#breadcrumb_scoring').attr('data-id')+'" data-id="'+$('#breadcrumb_scoring').attr('data-id')+'" data-value="'+$('#breadcrumb_scoring').val()+'" >'+$('#breadcrumb_scoring').val()+'</span>';
            //     $('.breadcrumb-item.active').html(active);
            //     var parent ='<span class="assign_class label'+$('#dashboard_label').attr('data-id')+'" data-id="'+$('#dashboard_label').attr('data-id')+'" data-value="'+$('#dashboard_label').val()+'" >'+$('#dashboard_label').val()+'</span>';
            //     $('.breadcrumb-item a').html(parent);
            // }(jQuery));
        });
    </script>
    <script src="{{ asset('custom/js/scoring.js') }}"></script>
@endsection
