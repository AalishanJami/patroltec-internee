<table>
  <thead>
    <tr>
      @if(Auth()->user()->hasPermissionTo('scoring_location_excel_export') || Auth::user()->all_companies == 1 )
        <th> Location
        </th>
      @endif
      @if(Auth()->user()->hasPermissionTo('scoring_score_excel_export') || Auth::user()->all_companies == 1 )
        <th> Score
        </th>
      @endif
      @if(Auth()->user()->hasPermissionTo('scoring_max_visit_excel_export') || Auth::user()->all_companies == 1 )
        <th> Max Visits
        </th>
      @endif
      @if(Auth()->user()->hasPermissionTo('scoring_dashbroad_excel_export') || Auth::user()->all_companies == 1 )
        <th> Dashboard Name
        </th>
      @endif
      @if(Auth()->user()->hasPermissionTo('scoring_notes_excel_export') || Auth::user()->all_companies == 1 )
        <th> Notes
        </th>
      @endif
    </tr>
  </thead>
  <tbody>
    @foreach($data as $value)
      <tr>
        @if(Auth()->user()->hasPermissionTo('scoring_location_excel_export') || Auth::user()->all_companies == 1 )
          <td>
            @if(isset($value->location->name))
              {{ $value->location->name }}
            @else
              All Location
            @endif
          </td>
        @endif
        @if(Auth()->user()->hasPermissionTo('scoring_score_excel_export') || Auth::user()->all_companies == 1 )
          <td>{{ $value->score }}</td>
        @endif
        @if(Auth()->user()->hasPermissionTo('scoring_max_visit_excel_export') || Auth::user()->all_companies == 1 )
          <td>{{ $value->max_visits }}</td>
        @endif
        @if(Auth()->user()->hasPermissionTo('scoring_dashbroad_excel_export') || Auth::user()->all_companies == 1 )
          <td>{{ $value->dashboard_name }}</td>
        @endif
        @if(Auth()->user()->hasPermissionTo('scoring_notes_excel_export') || Auth::user()->all_companies == 1 )
          <td>{{ $value->notes }}</td>
        @endif
      </tr>
    @endforeach
  </tbody>
</table>