@php
        $data=localization();
@endphp
<div class="modal fade" id="modalRegisterCompany" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Bonus Score</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form >
                <div class="modal-body mx-3">
                    <div class="md-form mb-5">
                        <div class="md-form">
                            <div class="row">
                                <div class="col-1">
                                    <i class="fas fa-user prefix grey-text" style="margin-top: 22px"></i>
                                </div>
                                <div class="col">
                                      <div class="row">
                                <div class="col-0 ml-3">
                                    
                                    <i class="fas fa-calendar prefix grey-text fa-2x" style="margin-top:32px;font-size: 28px;"></i>
                                </div>
                                <div class="col">
                                  <div class="md-form">
                                        <label data-error="wrong" data-success="right" for="name" class="active">Month</label>
                                        <select searchable="Search here.."  class="mdb-select">
                                            <option value="" disabled>Please Select</option>
                                            <option value="1">Jan</option>
                                            <option value="2">Feb</option>
                                        </select>
                                    </div>
                                   
                                </div>
                            </div>
                                </div>
                            </div>
                            
                            
                           
                        </div>
                    </div> 
                    <div class="md-form mb-5">
                    <i class="fas fa-chart-line prefix grey-text"></i>
                        <input type="number" class="form-control validate @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                        <label data-error="wrong" data-success="right" for="name">
                            Score
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                            data-content="tool tip for Score"></i>
                        </label>
                    </div>
                    <div class="md-form mb-5">
                        <i class="far fa-sticky-note prefix grey-text"></i>
                        <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                        <label data-error="wrong" data-success="right" for="name">
                            Notes
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                            data-content="tool tip for Notes"></i>
                        </label>
                    </div>

                </div>
             
                <div class="modal-footer d-flex justify-content-center">
                    <button class="btn btn-primary btn-sm" type="submit"><span class="assign_class label{{getKeyid('save',$data)}}" data-id="{{getKeyid('save',$data) }}" data-value="{{checkKey('save',$data) }}" >{!! checkKey('save',$data) !!} </span></button>
                </div>
            </form>
        </div>
    </div>
</div>
