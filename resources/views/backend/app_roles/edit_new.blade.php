@php
    $data=localization();
@endphp
{{ Form::model($role, array('url' => array('roles/update', $role->id), 'method' => 'POST')) }}
@csrf
<div class="modal-body mx-3">
    <div class="md-form mb-5">
        <i class="fas fa-user prefix grey-text"></i>
        @if(Auth()->user()->hasPermissionTo('role_name_edit') || Auth::user()->all_companies == 1 )
            <input id="new_company" type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="{{$role->name}}" required autocomplete="name" autofocus>
        @else
            <input id="new_company" type="text" class="form-control" name="name" value="{{$role->name}}" disabled >
        @endif
    </div>
    <input id="app_role_id" type="hidden"  value="{{$role->id}}">
    <div class="accordion md-accordion accordion-1" id="accordionEx23" role="tablist">
        <div class="accordion md-accordion" id="accordionEx" role="tablist" aria-multiselectable="true">
            @foreach ($permission_group_module as $key_group=>$permission_group)
                <div class="accordion md-accordion accordion-1 "  id="accordionEx23{{$permission_group->id}}" role="tablist">
                    <div class="card">
                        <div class="card-header blue lighten-3 z-depth-1" role="tab" id="heading{{$permission_group->id}}">
                            <a class="collapsed font-weight-bold white-text" data-toggle="collapse" href="#collapse{{$permission_group->id}}"
                               aria-expanded="false" aria-controls="collapse{{$permission_group->id}}">
                                <h5 class="text-uppercase mb-0 py-1">
                                      <span class="assign_class label{{getKeyid(str_slug($permission_group->name),$data)}}" data-id="{{getKeyid(str_slug($permission_group->name),$data) }}" data-value="{{checkKey(str_slug($permission_group->name),$data) }}" >
                                        {!! checkKey(str_slug($permission_group->name),$data) !!}
                                      </span>
                                </h5>
                            </a>
                        </div>
                        <div id="collapse{{$permission_group->id}}" class="collapse" role="tabpanel" aria-labelledby="heading{{$permission_group->id}}"
                             data-parent="#accordionEx23{{$permission_group->id}}">
                            <div class="card-body">
                                <div class="row my-4">
                                    <div class="col-md-12">
                                        @foreach ($permission_group->appPermissionModuleId as $key=>$permission_data)
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="panel panel-info">
                                                        <div class="panel-body">
                                                            <input  class="form-check-input appPermisionChange" {{appPermisionCheck($permission_data->id,$role->id)}} id="role_permission{{$permission_data->id}}" name="permissions[]" type="checkbox" value="{{$permission_data->id}}">
                                                            <label for="role_permission{{$permission_data->id}}" class="form-check-label">{{strtoupper(str_replace('_',' ',$permission_data->name))}}</label>
                                                        </div>
                                                    </div>
                                                    <br>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div class="modal-footer d-flex justify-content-center">
{{--        <button class="form_submit_check btn btn-primary btn-sm" type="submit">--}}
{{--              <span class="assign_class label{{getKeyid('update',$data)}}" data-id="{{getKeyid('update',$data) }}" data-value="{{checkKey('update',$data) }}" >--}}
{{--                {{checkKey('update',$data) }}--}}
{{--              </span>--}}
{{--        </button>--}}
    </div>
</div>
{{ Form::close() }}
