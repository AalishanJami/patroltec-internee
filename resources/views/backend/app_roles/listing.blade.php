@if($permissionsArray->count()>0)
    <div class="col-md-12 mt-5">
        <div class="card">
            <div class="card-body">
                <div class="row col-md-12 text-center">
                    <h4 class="col-md-12">Listing</h4>
                </div>
                <div id="table" class="table-editable table-responsive">
                    <table class=" table table-striped table-bordered" id="tablecompletedlists" cellspacing="0" width="100%">
                        <tr>
                            <th></th>
                            <th>View</th>
                            <th>Edit</th>
                        </tr>
                        @foreach ($permissionsArray as $key_permission=> $index)
                            <tr>
                                @foreach ($index as $listing_index)
                                    <td>
                                        <input class="form-check-input permisionChange" id="role{{$index->id}}" hidden="" name="permissions[]" type="checkbox" value="{{$index->id}}">
                                        <label for="role{{$index->id}}" class="form-check-label"></label>
                                    </td>
                                @endforeach()
                            </tr>
                        @endforeach()
                    </table>
                </div>
            </div>
        </div>
    </div>
@endif



