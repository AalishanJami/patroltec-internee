@extends('Frontend.layouts.app')

@section('content')



    <script src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
    <script>
        $( document ).ready(function() {


            $('#login_form').submit(function () {
                event.preventDefault();
                var email=$('#email').val();
                var password=$('#password').val();

                $.ajax({
                    type: "POST",
                    url: "{{ route('login') }}",
                    data: {'email':email,'password':password,'_token':'{{csrf_token()}}'},
                    success: function(response){
                        window.location.href = "dashboard";

                    },
                    error: function (error) {
                        swal("Credentials Doesn't match!!", "Your Email or Password is incorrect!", "error");
                        toastr["error"]("Credentials Doesn't match!");
                    }
                });

            });



            $('#registernewuser').submit(function () {
                event.preventDefault();
                var name=$('#newuser_name').val();
                var email=$('#newuser_email').val();
                var password=$('#newuser_password').val();
                var password_confirm=$('#newuser_password_confirm').val();
if(password != password_confirm)
{
    return toastr["error"]("Password and Confirm Password not match!");
}
                $.ajax({
                    type: "POST",
                    url: "{{ route('ajaxcreateuser') }}",
                    data: {'name':name,'email':email,'password':password,'confirm-password':password_confirm,'_token':'{{csrf_token()}}'},
                    success: function(response){
                        console.log(response);

                        $('#modalRegisterForm').modal('hide');
                        if (response.user_exist== true)
                        {
                            swal("Try other email!", "Email ALready Exists!", "error");
                            toastr["error"]("Operation Failed!");
                        }
                        else {
                            swal("New User Created Successfully!", "success");
                            toastr["success"]("Operation Sucess!");
                            window.location.href = "dashboard";

                        }


},
                    error: function (error) {

                    }
                });

            });


            $('#resetpassword').submit(function () {


                event.preventDefault();
                var email=$('#reset_email').val();



                $.ajax({
                    type: "POST",
                    url: "{{ route('checkemail')}}",
                    data: {'email':email,'_token':'{{csrf_token()}}'},
                    beforeSend: function(){
                        // Show image container
                        $("#loader").show();
                    },
                    success: function(response){
                        $("#loader").hide();

                        console.log(response);
if(response==1)
{
    $.ajax({
        type: "POST",
        url: "{{ route('password.email')}}",
        data: {'email':email,'_token':'{{csrf_token()}}'},
        beforeSend: function(){
            // Show image container
            $("#loader").show();
        },
        success: function(response){
            $("#loader").hide();

            console.log(response);

            swal("Reset link sent!", "Check your email to reset password!", "success");

            toastr["success"]("Email sent!");

        },
        error: function (error) {
            $("#loader").hide();


        }
    });

}//exists
else{
    swal("Credentials Doesn't match!!", "Your Email is not available!", "error");
    toastr["error"]("Email Doesn't match!");
}


                        // swal("Reset link sent!", "Check your email to reset password!", "success");
                        //
                        // toastr["success"]("Email sent!");

                    },
                    error: function (error) {
                    console.log(error);
                    }
                });

            });
        });

    </script>

    <div class="col-md-6 col-xl-5 offset-xl-1">
        <!-- Form -->
        <form id="login_form" method="POST" action="{{ route('login') }}"  style="color: #757575;">
            @csrf
        <div class="card wow fadeInRight" data-wow-delay="0.3s">
            <div class="card-body">
                <!-- Header -->
                <div class="text-center">
                    <h3 class="white-text"><i class="fas fa-user white-text"></i> Login:</h3>
                    <hr class="hr-light">
                </div>

                <!-- Body -->
                <div class="md-form">
                    <i class="fas fa-envelope prefix white-text"></i>
                    <input id="email" type="email" style="padding:5px;" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                    <label for="form2" class="white-text">Your email</label>
                </div>

                <div class="md-form">
                    <i class="fas fa-lock prefix white-text"></i>
                    <input id="password" style="padding:5px;" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                    <label for="form4" class="white-text">Your password</label>
                </div>

                <div class="text-center mt-4">
                    <button id="submit_button" class="btn btn-light-blue btn-rounded" type="submit">Sign in
                    </button>

                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="materialLoginFormRemember" name="remember" {{ old('remember') ? 'checked' : '' }}>
                        <label class="form-check-label" for="materialLoginFormRemember">Remember me</label>
                    </div>

                    <a href="" data-toggle="modal" data-target="#modalSubscriptionForm">
                        {{ __('Forgot Your Password?') }}
                    </a>
{{--                    <p>Not a member?--}}
{{--                        <a href=""  data-toggle="modal" data-target="#modalRegisterForm">Register</a>--}}
{{--                    </p>--}}
                    <hr class="hr-light mb-3 mt-4">

                    <div class="inline-ul text-center d-flex justify-content-center">
                        <a class="p-2 m-2 fa-lg tw-ic"><i class="fab fa-twitter white-text"></i></a>
                        <a class="p-2 m-2 fa-lg li-ic"><i class="fab fa-linkedin-in white-text"> </i></a>
                        <a class="p-2 m-2 fa-lg ins-ic"><i class="fab fa-instagram white-text"> </i></a>
                    </div>
                </div>

            </div>
        </div>
        </form>
        <!-- /.Form -->
    </div>































{{--    Register Modal--}}
    <div class="modal fade" id="modalRegisterForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h4 class="modal-title w-100 font-weight-bold">Sign up</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="registernewuser" method="POST" action="{{ route('register') }}">
                    @csrf
                <div class="modal-body mx-3">
                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>

                        <input id="newuser_name" type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                        @error('name')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror

                        <label data-error="wrong" data-success="right" for="name">Your name</label>
                    </div>
                    <div class="md-form mb-5">
                        <i class="fas fa-envelope prefix grey-text"></i>
                        <input id="newuser_email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                        @error('email')
                        <span class="invalid-feedback" role="alert">
                         <strong>{{ $message }}</strong>
                         </span>
                        @enderror
                        <label data-error="wrong" data-success="right" for="orangeForm-email">Your email</label>
                    </div>

                    <div class="md-form mb-4">
                        <i class="fas fa-lock prefix grey-text"></i>
                        <input id="newuser_password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                        @error('password')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                        <label data-error="wrong" data-success="right" for="orangeForm-pass">Your password</label>
                    </div>


                    <div class="md-form mb-4">
                        <i class="fas fa-lock prefix grey-text"></i>
                        <label data-error="wrong" data-success="right" for="orangeForm-pass">Confirm Password</label>


                            <input id="newuser_password_confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">

                    </div>

                </div>


                <div class="modal-footer d-flex justify-content-center">
                    <button class="btn btn-deep-orange" type="submit">Sign up</button>
                </div>
                </form>
            </div>
        </div>
    </div>

{{--end modal register--}}

{{--Forget Link modal--}}
    <div class="modal fade" id="modalSubscriptionForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">

            <form id="resetpassword" method="POST" action="{{ route('password.update')}}">
                @csrf
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h4 class="modal-title w-100 font-weight-bold">Send Password Reset Link</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body mx-3">

                    <div class="md-form mb-4">
                        <i class="fas fa-envelope prefix grey-text"></i>

                        <input id="reset_email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email">
                        <label data-error="wrong" data-success="right" for="form2">Your email</label>
                    </div>
                    <div>

                    </div>
                    <div class="justify-content-center" style="display: none" id="loader">
                        <strong>Sending Email...</strong>
                        <div class="spinner-border " role="status" aria-hidden="true"></div>
                    </div>

                </div>

                <div class="modal-footer d-flex justify-content-center">

                    <button class="btn btn-indigo" type="submit">Send <i class="fas fa-paper-plane-o ml-1"></i></button>
                </div>

            </div>

            </form>
        </div>
    </div>
    <!-- Material form login -->
@endsection