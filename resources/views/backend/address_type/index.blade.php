@php
   $data=localization();
@endphp

<div class="modal fade" id="modalAddressTypeIndex" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">

            <div class="card">
                <div class="card-body">
                    <div id="table" class="table-editable table-responsive">
                        <span class="table-add float-right mb-3 mr-2">
                            <span class="float-right mb-3 mr-2" onclick="append()"><a  href="#!" class="text-success"><i
                                        class="fas fa-plus fa-2x" aria-hidden="true"></i></a></span>
                        </span>
                        <button class="btn btn-danger btn-sm">
                            Delete Selected Site Groups
                        </button>
                        <button class="btn btn-primary btn-sm">
                            Restore Deleted Site Groups
                        </button>

                        <button  style="display: none;">
                            Show Active Site Groups
                        </button>
                        <button class="btn btn-warning btn-sm">
                            Excel Export
                        </button>
                        <button class="btn btn-success btn-sm">
                            Word Export
                        </button>
                        <table id="address_type" class=" company_table_static table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th></th>
                                <th> Name</th>
                            </tr>
                            </thead>
                            <tr>
                                <td  contenteditable="true">
                                    <a type="button"class="btn btn-danger btn-sm my-0" onclick="deletesite_groupsdata()"><i class="fas fa-trash"></i></a>
                                </td>
                                <td  contenteditable="true">Dummy</td>
                            </tr>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function append()
    {
        $("#address_type").append('<tr> <td  contenteditable="true"><a type="button"class="btn btn-danger btn-sm my-0" onclick="deletesite_groupsdata()"><i class="fas fa-trash"></i></a></td><td contenteditable="true">Dummy</td></tr>');

    }
</script>


<script type="text/javascript">
    function deletesite_groupsdata()
    {

        swal({
                title:'Are you sure?',
                text: "Delete Record.",
                type: "error",
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, Delete!",
                cancelButtonText: "Cancel!",
                closeOnConfirm: false,
                customClass: "confirm_class",
                closeOnCancel: false,
            },
            function(isConfirm){
                swal.close();
            });
    }

</script>
