@include('backend.layouts.pdf_start')
  <thead>
    <tr>
        @if(Auth()->user()->hasPermissionTo('contacts_name_pdf_export') ||  Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout">  Name</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('contacts_title_pdf_export') ||  Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout">  Title</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('contacts_firstname_pdf_export') ||  Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout">  First Name</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('contacts_email_pdf_export') ||  Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout"> Email Address</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('contacts_phonenumber_pdf_export') ||  Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout"> Phone Number</th>
        @endif
        <th></th>
    </tr>
  </thead>
  <tbody>
    @foreach($data as $value)
      </br>
      <tr>
          @if(Auth()->user()->hasPermissionTo('contacts_name_pdf_export') ||  Auth::user()->all_companies == 1 )
              <td class="pdf_table_layout"><p class="col_text_style">{{ $value->name }}</p></td>
          @endif
          @if(Auth()->user()->hasPermissionTo('contacts_title_pdf_export') ||  Auth::user()->all_companies == 1 )
              <td class="pdf_table_layout"><p class="col_text_style">{{ $value->title }}</p></td>
          @endif
          @if(Auth()->user()->hasPermissionTo('contacts_firstname_pdf_export') ||  Auth::user()->all_companies == 1 )
              <td class="pdf_table_layout"><p class="col_text_style">{{ $value->first_name }}</p></td>
          @endif
          @if(Auth()->user()->hasPermissionTo('contacts_email_pdf_export') ||  Auth::user()->all_companies == 1 )
              <td class="pdf_table_layout"><p class="col_text_style">{{ $value->email }}</p></td>
          @endif
          @if(Auth()->user()->hasPermissionTo('contacts_phonenumber_pdf_export') ||  Auth::user()->all_companies == 1 )
              <td class="pdf_table_layout"><p class="col_text_style">{{ $value->phone_number }}</p></td>
          @endif
          @if(Auth()->user()->hasPermissionTo('contacts_postcode_pdf_export') ||  Auth::user()->all_companies == 1 )
              <td class="pdf_table_layout"><p class="col_text_style">{{ $value->post_code }}</p></td>
          @endif
      </tr>
    @endforeach
  </tbody>
@include('backend.layouts.pdf_end')