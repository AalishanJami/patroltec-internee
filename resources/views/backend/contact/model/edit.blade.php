@php
$data=localization();
@endphp
<div class="modal fade" id="modalContactEdit" tabindex="-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
   aria-hidden="true">
   <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
         <div class="modal-header text-center">
            <h4 class="modal-title w-100 font-weight-bold">
              <span class="assign_class label{{getKeyid('contact',$data)}}" data-id="{{getKeyid('contact',$data) }}" data-value="{{checkKey('contact',$data) }}" >
                  {!! checkKey('contact',$data) !!}
              </span>
              @if(Auth()->user()->hasPermissionTo('view_cms') || Auth::user()->all_companies == 1 )
                <button data-class="edit-disable" class="edit_cms_enable edit_cms_switch btn btn-primary btn-sm waves-effect waves-light" style="display: none;" >
                    Edit cms
                </button>
                <button  data-class="edit-enable" class="edit_cms_disable edit_cms_switch btn btn-danger btn-sm waves-effect waves-light">
                    Edit cms
                </button>
              @endif
            </h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
          @if(Auth()->user()->hasPermissionTo('edit_contacts') ||  Auth::user()->all_companies == 1 )
              <form id="contact_form_edit">
            <input type="hidden" name="id" id="edit_id">
            <div class="modal-body">
               <div class="row">
                 <div class="col mt-2">
                    <a class="text-success modal_contacttype" >
                      <button type="button" class="btn btn-sm btn-outline-primary waves-effect form-custom-boutton" style="float: left;">
                       <span class="assign_class label{{getKeyid('contact_type',$data)}}" data-id="{{getKeyid('contact_type',$data) }}" data-value="{{checkKey('contact_type',$data) }}" >
                            {!! checkKey('contact_type',$data) !!}
                        </span>
                      </button>
                    </a>
                    <div class="md-form ml-4-5 contactsType_listing">
                       @if(Auth()->user()->hasPermissionTo('contacts_contactType_edit') ||  Auth::user()->all_companies == 1 )
                           <select searchable="Search here.." placeholder="Please Select"  class="mdb-select contacts_type_id" name="contacts_type_id" id="contactsType_edit_ajax">
                               <option value="" selected disabled>Please Select</option>
                               @foreach($contactsType as $value)
                                   <option value="{{$value->id}}">{{$value->name}}</option>
                               @endforeach
                           </select>
                       @endif
                    </div>
                 </div>
                  <div class="col">
                     <div class="md-form mb-5">
                         @if(Auth()->user()->hasPermissionTo('contacts_title_edit') ||  Auth::user()->all_companies == 1 )
                            <i class="fas fa-address-book prefix grey-text"></i>
                            <span class="ml-5 assign_class label{{getKeyid('title',$data)}}" data-id="{{getKeyid('title',$data) }}" data-value="{{checkKey('title',$data) }}" >
                                {!! checkKey('title',$data) !!}
                            </span>
                             <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="title" id="edit_title" value="" required autocomplete="name" autofocus>
                            <!--  <label class="active" data-error="wrong" data-success="right" for="name" >
                             </label> -->
                         @endif
                     </div>
                  </div>
                  <div class="col">
                     <div class="md-form mb-5">
                         @if(Auth()->user()->hasPermissionTo('contacts_firstname_edit') ||  Auth::user()->all_companies == 1 )
                            <i class="fas fa-user prefix grey-text"></i>
                            <span class="ml-5 assign_class label{{getKeyid('first_name',$data)}}" data-id="{{getKeyid('first_name',$data) }}" data-value="{{checkKey('first_name',$data) }}" >
                                {!! checkKey('first_name',$data) !!}
                            </span>
                             <input type="text" class="form-control" name="first_name" id="edit_first_name" value="" required autocomplete="name" autofocus>
                             <!-- <label class="active" for="name" >
                             </label> -->
                             <small class="text-danger" style="text-transform:initial;position:absolute;top:98%;left: 11% !important;" id="edit_first_name_message"></small>
                         @endif
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col">
                     <div class="md-form mb-5">
                         @if(Auth()->user()->hasPermissionTo('contacts_surname_edit') ||  Auth::user()->all_companies == 1 )
                            <i class="fas fa-user prefix grey-text"></i>
                            <span class="ml-5 assign_class label{{getKeyid('surname',$data)}}" data-id="{{getKeyid('surname',$data) }}" data-value="{{checkKey('surname',$data) }}" >
                              {!! checkKey('surname',$data) !!}
                            </span>
                            <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="surname" id="edit_surname" value="" required autocomplete="name" autofocus>
                            <!--  <label class="active" data-error="wrong" data-success="right" for="name" >
                             </label> -->
                         @endif
                     </div>
                  </div>
                  <div class="col">
                     <div class="md-form mb-5">
                         @if(Auth()->user()->hasPermissionTo('contacts_email_edit') ||  Auth::user()->all_companies == 1 )
                            <i class="fas fa-envelope prefix grey-text"></i>
                            <span class="ml-5 assign_class label{{getKeyid('email',$data)}}" data-id="{{getKeyid('email',$data) }}" data-value="{{checkKey('email',$data) }}" >
                                {!! checkKey('email',$data) !!}
                            </span>
                             <input type="email" class="form-control" name="email" id="edit_email" value=""  autocomplete="name" autofocus>
                            <!--  <label class="active"  for="name" >
                             </label> -->
                             <small class="text-danger" style="text-transform:initial;position:absolute;top:98%;left: 11% !important;" id="edit_email_message"></small>
                         @endif
                     </div>
                  </div>
                  <div class="col">
                     <div class="md-form mb-5">
                         @if(Auth()->user()->hasPermissionTo('contacts_phonenumber_edit') ||  Auth::user()->all_companies == 1 )
                            <i class="fas fa-blender-phone prefix grey-text"></i>
                            <span class="ml-5 assign_class label{{getKeyid('number',$data)}}" data-id="{{getKeyid('number',$data) }}" data-value="{{checkKey('number',$data) }}" >
                              {!! checkKey('number',$data) !!}
                            </span>
                             <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="phone_number" id="edit_phone_number" value="" required autocomplete="name" autofocus>
                             <!-- <label class="active" data-error="wrong" data-success="right" for="name" > -->
                             <!-- </label> -->
                         @endif
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col">
                     <div class="md-form mb-5">
                         @if(Auth()->user()->hasPermissionTo('contacts_extension_edit') ||  Auth::user()->all_companies == 1 )
                            <i class="fas fa-phone-square prefix grey-text"></i>
                            <span class="ml-5 assign_class label{{getKeyid('extension',$data)}}" data-id="{{getKeyid('extension',$data) }}" data-value="{{checkKey('extension',$data) }}" >
                              {!! checkKey('extension',$data) !!}
                            </span>
                             <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="extension" id="edit_extension" value="" required autocomplete="name" autofocus>
                             <!-- <label class="active" data-error="wrong" data-success="right" for="name" >
                             </label> -->
                         @endif
                     </div>
                  </div>
                  <div class="col">
                     <div class="md-form mb-5">
                         @if(Auth()->user()->hasPermissionTo('contacts_mobilenumber_edit') ||  Auth::user()->all_companies == 1 )
                            <i class="fas fa-mobile-alt prefix grey-text"></i>
                            <span class="ml-5 assign_class label{{getKeyid('mobile_number',$data)}}" data-id="{{getKeyid('mobile_number',$data) }}" data-value="{{checkKey('mobile_number',$data) }}" >
                              {!! checkKey('mobile_number',$data) !!}
                            </span>
                             <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="mobile_number" id="edit_mobile_number" value="" required autocomplete="name" autofocus>
                            <!--  <label class="active" data-error="wrong" data-success="right" for="name" >
                             </label> -->
                         @endif
                     </div>
                  </div>
                  <div class="col">
                     <div class="md-form mb-5">
                         @if(Auth()->user()->hasPermissionTo('contacts_othernumber_edit') ||  Auth::user()->all_companies == 1 )
                            <i class="fas fa-address-book prefix grey-text"></i>
                            <span class="ml-5 assign_class label{{getKeyid('other_number',$data)}}" data-id="{{getKeyid('other_number',$data) }}" data-value="{{checkKey('other_number',$data) }}" >
                              {!! checkKey('other_number',$data) !!}
                            </span>
                             <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="other_number" id="edit_other_number" value="" required autocomplete="name" autofocus>
                             <!-- <label class="active" data-error="wrong" data-success="right" for="name" >
                             </label> -->
                         @endif
                     </div>
                  </div>
               </div>
               <div class="row">
               </div>
               <div class="row">
                  <div class="col">
                     <div class="md-form mb-5">
                         @if(Auth()->user()->hasPermissionTo('contacts_buildingname_edit') ||  Auth::user()->all_companies == 1 )
                            <i class="fas fa-building prefix grey-text"></i>
                            <span class="ml-5 assign_class label{{getKeyid('building',$data)}}" data-id="{{getKeyid('building',$data) }}" data-value="{{checkKey('building',$data) }}" >
                              {!! checkKey('building',$data) !!}
                            </span>
                            <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" id="edit_name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                             <!-- <label class="active" data-error="wrong" data-success="right" for="name">
                             </label> -->
                         @endif
                     </div>
                  </div>
                  <div class="col">
                     <div class="md-form mb-5">
                         @if(Auth()->user()->hasPermissionTo('contacts_street_edit') ||  Auth::user()->all_companies == 1 )
                            <i class="fas fa-road prefix grey-text"></i>
                            <span class="ml-5 assign_class label{{getKeyid('street',$data)}}" data-id="{{getKeyid('street',$data) }}" data-value="{{checkKey('street',$data) }}" >
                                {!! checkKey('street',$data) !!}
                              </span>
                             <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="address_street" id="edit_address_street" value="{{ old('name') }}" required autocomplete="name" autofocus>
                             <!-- <label class="active" data-error="wrong" data-success="right" for="name">
                             </label> -->
                         @endif
                     </div>
                  </div>
                  <div class="col">
                     <div class="md-form mb-5">
                         @if(Auth()->user()->hasPermissionTo('contacts_town_edit') ||  Auth::user()->all_companies == 1 )
                            <i class="fas fa-city prefix grey-text"></i>
                            <span class="ml-5 assign_class label{{getKeyid('town',$data)}}" data-id="{{getKeyid('town',$data) }}" data-value="{{checkKey('town',$data) }}" >
                                  {!! checkKey('town',$data) !!}
                                </span>
                             <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="town" id="edit_town" value="{{ old('name') }}" required autocomplete="name" autofocus>
                             <!-- label class="active" data-error="wrong" data-success="right" for="name">
                             </label> -->
                         @endif
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col">
                     <div class="md-form mb-5">
                         @if(Auth()->user()->hasPermissionTo('contacts_state_edit') ||  Auth::user()->all_companies == 1 )
                            <i class="fas fa-vihara prefix grey-text"></i>
                            <span class="ml-5 assign_class label{{getKeyid('state',$data)}}" data-id="{{getKeyid('state',$data) }}" data-value="{{checkKey('state',$data) }}" >
                                {!! checkKey('state',$data) !!}
                              </span>
                             <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="state" id="edit_state" value="{{ old('name') }}" required autocomplete="name" autofocus>
                             <!-- <label class="active" data-error="wrong" data-success="right" for="name">
                             </label> -->
                        @endif
                     </div>
                  </div>
                  <div class="col">
                     <div class="md-form mb-5">
                         @if(Auth()->user()->hasPermissionTo('contacts_country_edit') ||  Auth::user()->all_companies == 1 )
                            <i class="fas fa-globe prefix grey-text"></i>
                            <span class="ml-5 assign_class label{{getKeyid('country',$data)}}" data-id="{{getKeyid('country',$data) }}" data-value="{{checkKey('country',$data) }}" >
                                  {!! checkKey('country',$data) !!}
                                </span>
                             <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="country" id="edit_country" value="{{ old('name') }}" required autocomplete="name" autofocus>
                             <!-- <label class="active" data-error="wrong" data-success="right" for="name">
                             </label> -->
                        @endif
                     </div>
                  </div>
                  <div class="col">
                     <div class="md-form mb-5">
                         @if(Auth()->user()->hasPermissionTo('contacts_postcode_edit') ||  Auth::user()->all_companies == 1 )
                            <i class="fas fa-user prefix grey-text"></i>
                            <span class="ml-5 assign_class label{{getKeyid('post_code',$data)}}" data-id="{{getKeyid('post_code',$data) }}" data-value="{{checkKey('post_code',$data) }}" >
                                  {!! checkKey('post_code',$data) !!}
                                </span>
                             <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="post_code" id="edit_post_code" value="{{ old('name') }}" required autocomplete="name" autofocus>
                            <!--  <label class="active" data-error="wrong" data-success="right" for="name">
                             </label> -->
                        @endif
                     </div>
                  </div>
               </div>
               <div class="row justify-content-center">
                  <button type="button" class="btn btn-primary btn-sm" onclick="update()">
                    <span class="assign_class label{{getKeyid('update',$data)}}" data-id="{{getKeyid('update',$data) }}" data-value="{{checkKey('update',$data) }}" >
                      {!! checkKey('update',$data) !!}
                    </span>
                  </button>
               </div>
            </div>
         </form>
          @endif
      </div>
   </div>
</div>
