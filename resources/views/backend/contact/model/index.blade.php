@php
    $data=localization();
@endphp
<link rel="stylesheet" href="{{ asset('custom/css/custom_style.css') }}">
<div class="modal fade" id="modalContactTypeIndexpage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">
                    <span class="assign_class label{{getKeyid('contact_type',$data)}}" data-id="{{getKeyid('contact_type',$data) }}" data-value="{{checkKey('contact_type',$data) }}" >
                        {!! checkKey('contact_type',$data) !!}
                    </span>
                    @if(Auth()->user()->hasPermissionTo('view_cms') || Auth::user()->all_companies == 1 )
                        <button data-class="edit-disable" class="edit_cms_enable edit_cms_switch btn btn-primary btn-sm waves-effect waves-light" style="display: none;" >
                            Edit cms
                        </button>
                        <button  data-class="edit-enable" class="edit_cms_disable edit_cms_switch btn btn-danger btn-sm waves-effect waves-light">
                            Edit cms
                        </button>
                    @endif
                </h4>
                <button type="button" class="close contact_listing_close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="card">
                <div class="card-body">
                    <div id="table-contacttype" class="table-responsive table-editable">
                        @if(Auth()->user()->hasPermissionTo('create_contactType')  || Auth::user()->all_companies == 1 )
                            <span class="add-contacttype table-add float-right mb-3 mr-2">
                                <a class="text-success"><i class="fas fa-plus fa-2x" aria-hidden="true"></i></a>
                            </span>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_delete_contactType')  || Auth::user()->all_companies == 1 )
                            <button class="btn btn-danger btn-sm" id="deleteselectedcontacttype">
                                <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                                    {!! checkKey('selected_delete',$data) !!}
                                </span>
                            </button>
                            <button class="btn btn-danger btn-sm" style="display: none;" id="deleteselectedcontacttypeSoft">
                                <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                                    {!! checkKey('selected_delete',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_active_contactType') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-success btn-sm" style="display: none;" id="selectedactivebuttoncontacttype">
                                <span class="assign_class label{{getKeyid('selected_active',$data)}}" data-id="{{getKeyid('selected_active',$data) }}" data-value="{{checkKey('selected_active',$data) }}" >
                                    {!! checkKey('selected_active',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_restore_delete_contactType') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-primary btn-sm" id="restorebuttoncontacttype">
                                <span class="assign_class label{{getKeyid('restore',$data)}}" data-id="{{getKeyid('restore',$data) }}" data-value="{{checkKey('restore',$data) }}" >
                                    {!! checkKey('restore',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('active_contactType') || Auth::user()->all_companies == 1 )
                            <button  style="display: none;" class="btn btn-primary btn-sm" id="activebuttoncontacttype">
                                <span class="assign_class label{{getKeyid('show_active',$data)}}" data-id="{{getKeyid('show_active',$data) }}" data-value="{{checkKey('show_active',$data) }}" >
                                    {!! checkKey('show_active',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('csv_contactType') || Auth::user()->all_companies == 1 )
                            <form class="form-style" method="POST" id="export_excel_contacttype" action="{{ url('contacttype/export/excel') }}">
                                {!! csrf_field() !!}
                                <input type="hidden" class="contacttype_export" name="excel_array" value="1">
                                <button  type="submit" class="form_submit_check btn btn-warning btn-sm">
                                    <span class="assign_class label{{getKeyid('excel_export',$data)}}" data-id="{{getKeyid('excel_export',$data) }}" data-value="{{checkKey('excel_export',$data) }}" >
                                        {!! checkKey('excel_export',$data) !!}
                                    </span>
                                </button>
                            </form>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('word_contactType') || Auth::user()->all_companies == 1 )
                            <form class="form-style" method="POST" id="export_world_contacttype" action="{{ url('contacttype/export/world') }}">
                                {!! csrf_field() !!}
                                <input type="hidden" class="contacttype_export" name="word_array" value="1">
                                <button  type="submit"class="form_submit_check btn btn-success btn-sm">
                                    <span class=" assign_class label{{getKeyid('word_export',$data)}}" data-id="{{getKeyid('word_export',$data) }}" data-value="{{checkKey('word_export',$data) }}" >
                                        {!! checkKey('word_export',$data) !!}
                                    </span>
                                </button>
                            </form>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('pdf_contactType') || Auth::user()->all_companies == 1 )
                            <form  class="form-style" {{pdf_view('contacts_types')}} id="export_pdf_contacttype" method="POST" action="{{ url('contacttype/export/pdf') }}">
                                {!! csrf_field() !!}
                                <input type="hidden" class="contacttype_export" name="pdf_array" value="1">
                                <button  type="submit" class="form_submit_check btn btn-secondary btn-sm">
                                    <span class=" assign_class label{{getKeyid('pdf_export',$data)}}" data-id="{{getKeyid('pdf_export',$data) }}" data-value="{{checkKey('pdf_export',$data) }}" >
                                        {!! checkKey('pdf_export',$data) !!}
                                    </span>
                                </button>
                            </form>
                        @endif
                        <div id="table" class="table-editable table-responsive">
                            <table class="contacttype_table table table-bordered table-responsive-md table-striped">
                                <thead>
                                <tr>
                                    <th class="all_checkboxes_style no-sort">
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input contacttype_checked" id="contacttype_checkbox_all">
                                            <label class="form-check-label" for="contacttype_checkbox_all">
                                                <span class=" assign_class label{{getKeyid('all',$data)}}" data-id="{{getKeyid('all',$data) }}" data-value="{{checkKey('all',$data) }}" >
                                                    {!! checkKey('all',$data) !!}
                                                </span>
                                            </label>
                                        </div>
                                    </th>
{{--                                    <th id="action" class="no-sort"></th>--}}
                                    <th>
                                        <span class=" assign_class label{{getKeyid('name',$data)}}" data-id="{{getKeyid('name',$data) }}" data-value="{{checkKey('name',$data) }}" >
                                            {!! checkKey('name',$data) !!}
                                        </span>
                                    </th>
                                    <th id="action" class=" all_action_btn"></th>
                                </tr>
                                </thead>
                                <tbody id="contactType_data"></tbody>
                            </table>
                        </div>
                    </div>
                    @if(Auth()->user()->all_companies == 1)
                        <input type="hidden" id="contactType_name" value="1">
                        <input type="hidden" id="contactType_checkbox" value="1">
                        <!-- edit permision -->
                        <input type="hidden" id="edit_contactType" value="1">
                        <!-- create -->
                        <input type="hidden" id="contactType_name_create" value="1">
                        <!-- edit -->
                        <input type="hidden" id="contactType_name_edit" value="1">
                    @else
                        <input type="hidden" id="contactType_name" value="{{Auth()->user()->hasPermissionTo('contactType_name')}}">
                        <input type="hidden" id="contactType_checkbox" value="{{Auth()->user()->hasPermissionTo('contactType_checkbox')}}">
                        <!-- edit permision -->
                        <input type="hidden" id="edit_contactType" value="{{Auth()->user()->hasPermissionTo('edit_contactType')}}">
                         <!-- create -->
                        <input type="hidden" id="contactType_name_create" value="{{Auth()->user()->hasPermissionTo('contactType_name_create')}}">
                        <!-- edit -->
                        <input type="hidden" id="contactType_name_edit" value="{{Auth()->user()->hasPermissionTo('contactType_name_edit')}}">
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function contacttype_data(url) {
        var table = $('.contacttype_table').dataTable(
        {
            columnDefs: [
                {"targets": [0,2], "orderable": false }
            ],
            processing: true,
            language: {
                'search':'<span class="assign_class label'+$('#search_id').attr('data-id')+'" data-id="'+$('#search_id').attr('data-id')+'" data-value="'+$('#search_id').val()+'" >'+$('#search_id').val()+'</span>',
                'paginate':{
                    'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                    'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                },
                'lengthMenu': '_MENU_',
                'info': ' ',
            },
            "ajax": {
                "url": url,
                "type": 'get',
            },
            "createdRow": function( row, data,dataIndex,columns )
            {
                var checkbox_permission=$('#checkbox_permission').val();
                var checkbox='';
                checkbox+='<div class="form-check"><input type="checkbox" class="selectedrowcontacttype form-check-input contacttype_checked" id="contacttype'+data.id+'"><label class="form-check-label" for="contacttype'+data.id+'""></label></div>';
                var submit='';
                submit+='<a type="button" class="btn btn-primary btn-xs all_action_btn_margin my-0 waves-effect waves-light savecontacttypedata show_tick_btn'+data.id+'" style="display: none;"  id="'+data.id+'"><i class="fas fa-check"></i></a>';
                $(columns[0]).html(checkbox);
                $(columns[1]).attr('id', 'nameD'+data['id']);
                $(columns[1]).attr('data-id',data['id']);
                $(columns[1]).attr('onkeydown', 'editSelectedRow(contacttype_tr'+data['id']+')');
                if($('#contactType_name_edit').val() && $('#edit_contactType').val())
                {
                    $(columns[1]).attr('Contenteditable', 'true');
                    $(columns[2]).append(submit);
                }
                $(columns[1]).attr('class','edit_inline_contacttype');
                $(row).attr('id', 'contacttype_tr'+data['id']);
                $(row).attr('data-id', data['id']);
                //$(row).attr('class', 'selectedrowcontacttype');
            },
            columns:
                [
                    {data: 'checkbox', name: 'checkbox',visible:$('#contactType_checkbox').val()},
                    {data: 'name', name: 'name',visible:$('#contactType_name').val()},
                    {data: 'actions', name: 'actions'},
                    // {data: 'submit', name: 'submit',},
                ],

        }
        );
        if ($(":checkbox").prop('checked',true)){
            $(":checkbox").prop('checked',false);
        }
    }

    $(document).ready(function () {
        var url='/contacttype/getall';
        contacttype_data(url);
        // $("body").on('click','.edit_inline_contacttype',function () {
        //     // $(".show_tick_btn"+$(this).attr('data-id')).show();
        // });
    });
</script>
