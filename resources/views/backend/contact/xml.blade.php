<table>
    <thead>
      <tr>
          @if(Auth()->user()->hasPermissionTo('contacts_title_excel_export') ||  Auth::user()->all_companies == 1 )
              <th> Title</th>
          @endif
          @if(Auth()->user()->hasPermissionTo('contacts_name_excel_export') ||  Auth::user()->all_companies == 1 )
              <th> Name</th>
          @endif
          @if(Auth()->user()->hasPermissionTo('contacts_firstname_excel_export') ||  Auth::user()->all_companies == 1 )
              <th> First Name</th>
          @endif
          @if(Auth()->user()->hasPermissionTo('contacts_surname_excel_export') ||  Auth::user()->all_companies == 1 )
              <th>Surname</th>
          @endif
          @if(Auth()->user()->hasPermissionTo('contacts_phonenumber_excel_export') ||  Auth::user()->all_companies == 1 )
              <th>Phone Number</th>
          @endif
          @if(Auth()->user()->hasPermissionTo('contacts_extension_excel_export') ||  Auth::user()->all_companies == 1 )
              <th>Extension</th>
          @endif
          @if(Auth()->user()->hasPermissionTo('contacts_mobilenumber_excel_export') ||  Auth::user()->all_companies == 1 )
              <th>Mobile Number</th>
          @endif
          @if(Auth()->user()->hasPermissionTo('contacts_othernumber_excel_export') ||  Auth::user()->all_companies == 1 )
              <th>Other Number</th>
          @endif
          @if(Auth()->user()->hasPermissionTo('contacts_street_excel_export') ||  Auth::user()->all_companies == 1 )
              <th>Address Street</th>
          @endif
          @if(Auth()->user()->hasPermissionTo('contacts_email_excel_export') ||  Auth::user()->all_companies == 1 )
              <th>Email</th>
          @endif
          @if(Auth()->user()->hasPermissionTo('contacts_town_excel_export') ||  Auth::user()->all_companies == 1 )
              <th>Town</th>
          @endif
          @if(Auth()->user()->hasPermissionTo('contacts_state_excel_export') ||  Auth::user()->all_companies == 1 )
              <th>State</th>
          @endif
          @if(Auth()->user()->hasPermissionTo('contacts_postcode_excel_export') ||  Auth::user()->all_companies == 1 )
              <th>Post Code</th>
          @endif
      </tr>
    </thead>
    <tbody>
    @foreach($data as $row)
      <tr>
          @if(Auth()->user()->hasPermissionTo('contacts_title_excel_export') ||  Auth::user()->all_companies == 1 )
              <td>{{ $row->title }}</td>
          @endif
          @if(Auth()->user()->hasPermissionTo('contacts_name_excel_export') ||  Auth::user()->all_companies == 1 )
              <td>{{ $row->name }}</td>
          @endif
          @if(Auth()->user()->hasPermissionTo('contacts_firstname_excel_export') ||  Auth::user()->all_companies == 1 )
              <td>{{ $row->first_name }}</td>
          @endif
          @if(Auth()->user()->hasPermissionTo('contacts_surname_excel_export') ||  Auth::user()->all_companies == 1 )
              <td>{{ $row->surname }}</td>
          @endif
          @if(Auth()->user()->hasPermissionTo('contacts_phonenumber_excel_export') ||  Auth::user()->all_companies == 1 )
              <td>{{ $row->phone_number }}</td>
          @endif
          @if(Auth()->user()->hasPermissionTo('contacts_extension_excel_export') ||  Auth::user()->all_companies == 1 )
              <td>{{ $row->extension }}</td>
          @endif
          @if(Auth()->user()->hasPermissionTo('contacts_mobilenumber_excel_export') ||  Auth::user()->all_companies == 1 )
              <td>{{ $row->mobile_number }}</td>
          @endif
          @if(Auth()->user()->hasPermissionTo('contacts_othernumber_excel_export') ||  Auth::user()->all_companies == 1 )
              <td>{{ $row->other_number }}</td>
          @endif
          @if(Auth()->user()->hasPermissionTo('contacts_street_excel_export') ||  Auth::user()->all_companies == 1 )
              <td>{{ $row->address_street }}</td>
          @endif
          @if(Auth()->user()->hasPermissionTo('contacts_email_excel_export') ||  Auth::user()->all_companies == 1 )
              <td>{{ $row->email }}</td>
          @endif
          @if(Auth()->user()->hasPermissionTo('contacts_town_excel_export') ||  Auth::user()->all_companies == 1 )
              <td>{{ $row->town }}</td>
          @endif
          @if(Auth()->user()->hasPermissionTo('contacts_state_excel_export') ||  Auth::user()->all_companies == 1 )
              <td>{{ $row->State }}</td>
          @endif
          @if(Auth()->user()->hasPermissionTo('contacts_postcode_excel_export') ||  Auth::user()->all_companies == 1 )
                  <td>{{ $row->post_code }}</td>
          @endif

      </tr>
    @endforeach
    </tbody>
</table>
