@extends('backend.layouts.detail')
@section('title', 'Contact')
@section('headscript')
    <script src="{{ asset('custom/js/contacttype.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('custom/css/custom_style.css') }}">
@stop
@section('content')
    @php
        $data=localization();
    @endphp
    @include('backend.layouts.detail_sidebar')
    <div class="padding-left-custom remove-padding">
        {!! breadcrumInner('contact',Session::get('project_id'),'locations','/project/detail/'.Session::get('project_id') ,'project','/project') !!}
        <div class="card">
            <div class="card-body">
                <div id="table" class="table-editable">
                        <span class="table-add float-right mb-3 mr-2">
                            @if(Auth()->user()->hasPermissionTo('create_contacts') || Auth::user()->all_companies == 1 )
                                <a class="text-success modalContactCreate"  data-toggle="modal" data-target="#modalContactCreate">
                                <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
                            </a>
                            @endif
                        </span>
                    @if(Auth()->user()->hasPermissionTo('selected_delete_contacts') || Auth::user()->all_companies == 1 )
                        <button id="deleteSelectedContact" class="btn btn-danger btn-sm">
                             <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                                {!! checkKey('selected_delete',$data) !!}
                            </span>
                        </button>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('selected_restore_delete_contacts') || Auth::user()->all_companies == 1 )
                        <button id="restore_button_contact" class="btn btn-primary btn-sm">
                           <span class="assign_class label{{getKeyid('restore',$data)}}" data-id="{{getKeyid('restore',$data) }}" data-value="{{checkKey('restore',$data) }}" >
                                {!! checkKey('restore',$data) !!}
                            </span>
                        </button>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('active_contacts') || Auth::user()->all_companies == 1 )
                        <button id="show_active_button_contact" class="btn btn-primary btn-sm" style="display: none;">
                            <span class="assign_class label{{getKeyid('show_active',$data)}}" data-id="{{getKeyid('show_active',$data) }}" data-value="{{checkKey('show_active',$data) }}" >
                                {!! checkKey('show_active',$data) !!}
                            </span>
                        </button>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('csv_contacts') || Auth::user()->all_companies == 1 )
                        <form class="form-style" method="POST" action="{{ url('/contact/exportExcel') }}">
                            <input type="hidden" id="export_excel_contact" name="export_excel_contact" value="1">
                            {!! csrf_field() !!}
                            <input type="hidden" class="contact_export" name="excel_array" value="1">
                            <button  type="submit" id="export_excel_contact_btn" data-id="{{getKeyid('excel_export',$data) }}" data-value="{{checkKey('excel_export',$data) }}"  class="form_submit_check btn btn-warning btn-sm assign_class label{{getKeyid('excel_export',$data)}}">
                                {!! checkKey('excel_export',$data) !!}
                            </button>
                        </form>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('word_contacts') || Auth::user()->all_companies == 1 )
                        <form class="form-style" method="POST" action="{{ url('/contact/exportWord') }}">
                            <input type="hidden" id="export_word_contact" name="export_word_contact" value="1">
                            <input type="hidden" class="contact_export" name="word_array" value="1">
                            {!! csrf_field() !!}
                            <button  type="submit" id="export_word_contact_btn" class="form_submit_check btn btn-success btn-sm">
                               <span class="assign_class label{{getKeyid('word_export',$data)}}" data-id="{{getKeyid('word_export',$data) }}" data-value="{{checkKey('word_export',$data) }}" >
                                    {!! checkKey('word_export',$data) !!}
                                </span>
                            </button>
                        </form>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('pdf_contacts') || Auth::user()->all_companies == 1 )
                        <form  class="form-style"  {{pdf_view('site_contacts')}}  method="POST" action="{{ url('contact/exportPdf') }}">
                            <input type="hidden" id="export_pdf_contact" name="export_pdf_contact" value="1">
                            <input type="hidden" class="contact_export" name="pdf_array" value="1">
                            {!! csrf_field() !!}
                            <button  type="submit" id="export_pdf_contact_btn"  class="form_submit_check btn btn-secondary btn-sm">
                                <span class="assign_class label{{getKeyid('pdf_export',$data)}}" data-id="{{getKeyid('pdf_export',$data) }}" data-value="{{checkKey('pdf_export',$data) }}" >
                                    {!! checkKey('pdf_export',$data) !!}
                                </span>
                            </button>
                        </form>
                    @endif
                    <input type="hidden" name="flag" id="flagContact" value="1">
                    <table id="contact_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        @include('backend.label.input_label')
                        <thead>
                        <tr>
                            @if(Auth()->user()->hasPermissionTo('contacts_checkbox') ||  Auth::user()->all_companies == 1 )
                                <th class="all_checkboxes_style no-sort">
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input contact_checked" id="contact_checkbox_all">
                                        <label class="form-check-label" for="contact_checkbox_all">
                                                <span class="assign_class label{{getKeyid('all',$data)}}" data-id="{{getKeyid('all',$data) }}" data-value="{{checkKey('all',$data) }}" >
                                                    {!! checkKey('all',$data) !!}
                                                </span>
                                        </label>
                                    </div>
                                </th>
                            @endif
                            @if(Auth()->user()->hasPermissionTo('contacts_contactType') ||  Auth::user()->all_companies == 1 )
                                <th class="th-sm">
                                        <span class="assign_class label{{getKeyid('contact_type',$data)}}" data-id="{{getKeyid('contact_type',$data) }}" data-value="{{checkKey('contact_type',$data) }}" >
                                            {!! checkKey('contact_type',$data) !!}
                                        </span>
                                </th>
                            @endif
                            @if(Auth()->user()->hasPermissionTo('contacts_firstname') ||  Auth::user()->all_companies == 1 )
                                <th class="th-sm" >
                                        <span class="assign_class label{{getKeyid('first_name',$data)}}" data-id="{{getKeyid('first_name',$data) }}" data-value="{{checkKey('first_name',$data) }}" >
                                            {!! checkKey('first_name',$data) !!}
                                        </span>
                                </th>
                            @endif
                            @if(Auth()->user()->hasPermissionTo('contacts_surname') ||  Auth::user()->all_companies == 1 )
                                <th>
                                        <span class="assign_class label{{getKeyid('surname',$data)}}" data-id="{{getKeyid('surname',$data) }}" data-value="{{checkKey('surname',$data) }}" >
                                            {!! checkKey('surname',$data) !!}
                                        </span>
                                </th>
                            @endif
                            @if(Auth()->user()->hasPermissionTo('contacts_email') ||  Auth::user()->all_companies == 1 )
                                <th>
                                        <span class="assign_class label{{getKeyid('email',$data)}}" data-id="{{getKeyid('email',$data) }}" data-value="{{checkKey('email',$data) }}" >
                                            {!! checkKey('email',$data) !!}
                                        </span>
                                </th>
                            @endif
                            @if(Auth()->user()->hasPermissionTo('contacts_phonenumber') ||  Auth::user()->all_companies == 1 )
                                <th>
                                        <span class="assign_class label{{getKeyid('number',$data)}}" data-id="{{getKeyid('number',$data) }}" data-value="{{checkKey('number',$data) }}" >
                                            {!! checkKey('number',$data) !!}
                                        </span>
                                </th>
                            @endif
                            @if(Auth()->user()->hasPermissionTo('contacts_postcode') ||  Auth::user()->all_companies == 1 )
                                <th>
                                        <span class="assign_class label{{getKeyid('post_code',$data)}}" data-id="{{getKeyid('post_code',$data) }}" data-value="{{checkKey('post_code',$data) }}" >
                                            {!! checkKey('post_code',$data) !!}
                                        </span>
                                </th>
                            @endif
                            <th class="all_action_btn" ></th>
                        </tr>
                        </thead>

                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        @if(Auth()->user()->all_companies == 1)
            <input type="hidden" id="edit_contacts" value="1">
            <input type="hidden" id="delete_contacts" value="1">
            <input type="hidden" id="contacts_checkbox" value="1">
            <input type="hidden" id="contacts_contactType" value="1">
            <input type="hidden" id="contacts_firstname" value="1">
            <input type="hidden" id="contacts_surname" value="1">
            <input type="hidden" id="contacts_email" value="1">
            <input type="hidden" id="contacts_phonenumber" value="1">
            <input type="hidden" id="contacts_postcode" value="1">
        @else
            <input type="hidden" id="edit_contacts" value="{{Auth()->user()->hasPermissionTo('edit_contacts')}}">
            <input type="hidden" id="delete_contacts" value="{{Auth()->user()->hasPermissionTo('delete_contacts')}}">
            <input type="hidden" id="contacts_checkbox" value="{{Auth()->user()->hasPermissionTo('contacts_checkbox')}}">
            <input type="hidden" id="contacts_contactType" value="{{Auth()->user()->hasPermissionTo('contacts_contactType')}}">
            <input type="hidden" id="contacts_firstname" value="{{Auth()->user()->hasPermissionTo('contacts_firstname')}}">
            <input type="hidden" id="contacts_surname" value="{{Auth()->user()->hasPermissionTo('contacts_surname')}}">
            <input type="hidden" id="contacts_email" value="{{Auth()->user()->hasPermissionTo('contacts_email')}}">
            <input type="hidden" id="contacts_phonenumber" value="{{Auth()->user()->hasPermissionTo('contacts_phonenumber')}}">
            <input type="hidden" id="contacts_postcode" value="{{Auth()->user()->hasPermissionTo('contacts_postcode')}}">
        @endif
    </div>

    @include('backend.contact.model.create')
    @include('backend.contact.model.edit')


    <script type="text/javascript" src="{{ asset('custom/js/contact.js') }}" ></script>
    <script type="text/javascript">

        $(document).ready(function() {
            $('.modal_contacttype').click(function(e) {
                if ($('.edit_cms_disable').css('display') == 'none') {
                    return 0;
                }
                $('#modalContactTypeIndexpage').modal('show');
            });
            $('.modalContactCreate').click(function() {
                document.getElementById("contactForm").reset();
                $('#contactForm').trigger('reset');
                $('#contacts_type_id option:first').val('');
                $('#contacts_type_id option:first').attr('selected',true);
                $('#contacts_type_id option:first').attr('disabled',true);
                $('#first_name_message').empty();
                $('#email').css('border-bottom','1px solid #ced4da');
                $('#first_name').css('border-bottom','1px solid #ced4da');
                $('#email_message').empty();
            });
            $(function () {
                contactAppend();
            });
            // (function ($) {
            //     var active ='<span class="assign_class label'+$('#breadcrumb_contact').attr('data-id')+'" data-id="'+$('#breadcrumb_contact').attr('data-id')+'" data-value="'+$('#breadcrumb_contact').val()+'" >'+$('#breadcrumb_contact').val()+'</span>';
            //     $('.breadcrumb-item.active').html(active);
            //     var parent ='<span class="assign_class label'+$('#dashboard_label').attr('data-id')+'" data-id="'+$('#dashboard_label').attr('data-id')+'" data-value="'+$('#dashboard_label').val()+'" >'+$('#dashboard_label').val()+'</span>';
            //     $('.breadcrumb-item a').html(parent);
            // }(jQuery));

        });
        function contactAppend()
        {
            var table = $('#contact_table').dataTable({
                columnDefs: [ {
                    "targets": [0,7],
                    "orderable": false
                } ],
                processing: true,
                bInfo:false,
                language: {
                    'lengthMenu': '  _MENU_ ',
                    'search':'<span class="assign_class label'+$('#search_label').attr('data-id')+'" data-id="'+$('#search_label').attr('data-id')+'" data-value="'+$('#search_label').val()+'" >'+$('#search_label').val()+'</span>',
                    'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                    'info': ' _PAGE_ <span class="assign_class label'+$('#of_label').attr('data-id')+'" data-id="'+$('#of_label').attr('data-id')+'" data-value="'+$('#of_label').val()+'" >'+$('#of_label').val()+'</span> _PAGES_',
                    'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                    'paginate': {
                        'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                        'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                    },
                    'infoFiltered': "(filtered from _MAX_ total records)"
                },
                "ajax": {
                    "url": '/contact/getalldata',
                    "type": 'get',
                },
                "createdRow": function( row, data, dataIndex,columns ) {
                    var checkbox='<div class="form-check"><input type="checkbox" class="form-check-input selectedRowContact" id="contact'+data.id+'"><label class="form-check-label" for="contact'+data.id+'""></label></div>';
                    $(columns[0]).html(checkbox);
                    $(row).attr('id', 'contact_tr'+data['id']);
                    $(row).attr('class', 'selectedRowContact');
                    $(row).attr('data-id', data['id']);
                },
                columns: [
                    {data: 'checkbox', name: 'checkbox',visible:$('#contacts_checkbox').val()},
                    {data: 'contacttype', name: 'contacttype',visible:$('#contacts_contactType').val()},
                    {data: 'first_name', name: 'first_name',visible:$('#contacts_firstname').val()},
                    {data: 'surname', name: 'surname',visible:$('#contacts_surname').val()},
                    {data: 'email', name: 'email',visible:$('#contacts_email').val()},
                    {data: 'phone_number', name: 'phone_number',visible:$('#contacts_phonenumber').val()},
                    {data: 'post_code', name: 'post_code',visible:$('#contacts_postcode').val()},
                    {data: 'actions', name: 'actions'},
                ],
            });
            if ($(":checkbox").prop('checked',true)){
                $(":checkbox").prop('checked',false);
            }

        }

        function contactSoftAppend(){
            var table = $('#contact_table').dataTable({
                columnDefs: [ {
                    "targets": [0,7],
                    "orderable": false
                } ],
                processing: true,
                language: {
                    'lengthMenu': '  _MENU_ ',
                    'search':'<span class="assign_class label'+$('#search_label').attr('data-id')+'" data-id="'+$('#search_label').attr('data-id')+'" data-value="'+$('#search_label').val()+'" >'+$('#search_label').val()+'</span>',
                    'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                    'info': ' _PAGE_ <span class="assign_class label'+$('#of_label').attr('data-id')+'" data-id="'+$('#of_label').attr('data-id')+'" data-value="'+$('#of_label').val()+'" >'+$('#of_label').val()+'</span> _PAGES_',
                    'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                    'paginate': {
                        'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                        'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                    },
                    'infoFiltered': "(filtered from _MAX_ total records)"
                },
                "ajax": {
                    "url": '/contact/getAllSoftData',
                    "type": 'get',
                },
                "createdRow": function( row, data, dataIndex,columns ) {
                    var checkbox='<div class="form-check"><input type="checkbox" class="form-check-input selectedRowContact" id="contact'+data.id+'"><label class="form-check-label" for="contact'+data.id+'""></label></div>';
                    $(columns[0]).html(checkbox);
                    $(row).attr('id', 'contact_tr'+data['id']);
                    //$(row).attr('class', 'selectedRowContact');
                },
                columns: [
                    {data: 'checkbox', name: 'checkbox'},
                    {data: 'title', name: 'title'},
                    {data: 'first_name', name: 'first_name'},
                    {data: 'surname', name: 'surname'},
                    {data: 'email', name: 'email'},
                    {data: 'phone_number', name: 'phone_number'},
                    {data: 'post_code', name: 'post_code'},
                    {data: 'actions', name: 'actions'},
                ],
            });
            if ($(":checkbox").prop('checked',true)){
                $(":checkbox").prop('checked',false);
            }
        }
    </script>

    <script type="text/javascript">
        window.onload = function() {
            document.getElementById("action").classList.remove('sorting_asc');
        };

    </script>

@endsection
