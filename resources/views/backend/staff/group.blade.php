@extends('backend.layouts.user')
@section('content')
    @php
       $data=localization();
    @endphp
    <div class="row">
        @include('backend.layouts.user_sidebar')
        <div class="padding-left-custom remove-padding">
            {{ Breadcrumbs::render('staff/group') }}
            <span class="table-add float-right mb-3 mr-2">
                <a class="text-success"  data-toggle="modal" data-target="#modelStaffCreate">
                    <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
                </a>
            </span>
            <button class="btn btn-danger btn-sm">
                Delete Selected Staff
            </button>
            <button class="btn btn-primary btn-sm">
              Restore Deleted Staff
            </button>
           
            <button  style="display: none;">
                Show Active Staff
            </button>
            <button class="btn btn-warning btn-sm">
               Excel Export
            </button>
            <button class="btn btn-success btn-sm">
                Word Export
            </button>
            <table id="company_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th> ID 
                    </th> 
                    <th> Name 
                    </th>      
                    <th id="action">
                    </th>
                </tr>
                </thead>
                    <tr>
                        <td>1</td>
                        <td>Manager</td>
                        <td>
                            <a  data-toggle="modal" data-target="#modelEditStaff" class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i></a>
                            <button type="button"class="btn btn-danger btn-sm my-0" onclick="deleteStaffdata()"><i class="fas fa-trash"></i></button>

                        </td>
                    </tr>
                <tbody>
                </tbody>
            </table>
            @component('backend.staff.model.create')
            @endcomponent

             {{--    Edit Modal--}}
            @component('backend.staff.model.edit')
            @endcomponent
            {{--    END Edit Modal--}}
        </div>
    </div>
   

@endsection
<script type="text/javascript">
function deletedeleteStaffdatadata()
{

    swal({
        title:'Are you sure?',
        text: "Delete Record.",
        type: "error",
        showCloseButton: true,
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, Delete!",
        cancelButtonText: "Cancel!",
        closeOnConfirm: false,
        customClass: "confirm_class",
        closeOnCancel: false,
    },
    function(isConfirm){
        swal.close();
 });

    
}

</script>