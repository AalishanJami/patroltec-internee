@extends('backend.layouts.user')
@section('title', 'Staff')
@section('headscript')
  <script>
    $(document).ready(function() {
      $('#staff_checkbox_all').click(function() {
          if ($(this).is(':checked')) {
              $('.staff_checked').attr('checked', true);
              $("tr").addClass('selected');
          } else {
              $('.staff_checked').attr('checked', false);
              $("tr").removeClass('selected');
          }
      });
      $('#staff_roles_checkbox_all').click(function() {
          if ($(this).is(':checked')) {
              $('.staff_roles_checked').attr('checked', true);
              $("tr").addClass('selected');
          } else {
              $('.staff_roles_checked').attr('checked', false);
              $("tr").removeClass('selected');
          }
      });
      $('#staff_group_permission_checkbox_all').click(function() {
          if ($(this).is(':checked')) {
              $('.staff_group_permission_checked').attr('checked', true);
              $("tr").addClass('selected');
          } else {
              $('.staff_group_permission_checked').attr('checked', false);
              $("tr").removeClass('selected');
          }
      });
    });
  </script>
  <link rel="stylesheet" href="{{ asset('custom/css/custom_style.css') }}">
@stop
@section('content')
    @php
       $data=localization();
    @endphp
    @include('backend.layouts.user_sidebar')
    <div class="padding-left-custom remove-padding">
     {!! breadcrumInner('staff',Session::get('employee_id'),'users','/employee/detail/'.Session::get('employee_id'),'employees','/employee') !!}
      <div class="card">
        <div class="card-body">
            <ul  class="nav nav-tabs" id="myTab" role="tablist">
              @if(Auth()->user()->hasPermissionTo('view_employeeStaffGroupPermision') || Auth::user()->all_companies == 1 )
                <li class="nav-item">
                    <a class="nav-link active" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">
                      <span class="assign_class label{{getKeyid('position',$data)}}" data-id="{{getKeyid('position',$data) }}" data-value="{{checkKey('position',$data) }}" >
                        {!! checkKey('position',$data) !!}
                      </span>
                    </a>
                </li>
              @endif
              @if(Auth()->user()->hasPermissionTo('view_employeeStaffPermision') || Auth::user()->all_companies == 1 )
                <li class="nav-item">
                    <a class="nav-link " id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home"aria-selected="true">
                      <span class="assign_class label{{getKeyid('staff',$data)}}" data-id="{{getKeyid('staff',$data) }}" data-value="{{checkKey('staff',$data) }}" >
                        {!! checkKey('staff',$data) !!}
                      </span>
                    </a>
                </li>
              @endif
              @if(Auth()->user()->hasPermissionTo('view_employeeStaffRolePermision') || Auth::user()->all_companies == 1 )
                <li class="nav-item">
                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact"aria-selected="false">
                      <span class="assign_class label{{getKeyid('roles',$data)}}" data-id="{{getKeyid('roles',$data) }}" data-value="{{checkKey('roles',$data) }}" >
                        {!! checkKey('roles',$data) !!}
                      </span>
                    </a>
                </li>
              @endif
            </ul>
            <br>
            <div class="tab-content" id="myTabContent">
              <div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                <div class="card-body">
                    <div class="table-responsive">
                        <form  action="{{url('staff/group/update')}}" method="post">
                            @csrf
                            <table class="company_table_static table  table-striped table-bordered " cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                      @if(Auth()->user()->hasPermissionTo('employeeStaffGroupPermision_checkbox') || Auth::user()->all_companies == 1 )
                                        <th class="no-sort all_checkboxes_style">
                                          <div class="form-check">
                                            <input type="checkbox" class="form-check-input" id="staff_group_permission_checkbox_all">
                                            <label class="form-check-label" for="staff_group_permission_checkbox_all">
                                              <span class="assign_class label{{getKeyid('all',$data)}}" data-id="{{getKeyid('all',$data) }}" data-value="{{checkKey('all',$data) }}" >
                                                {!! checkKey('all',$data) !!}
                                              </span>
                                            </label>
                                          </div>
                                        </th>
                                      @endif
                                      @if(Auth()->user()->hasPermissionTo('employeeStaffGroupPermision_name') || Auth::user()->all_companies == 1 )
                                        <th>
                                          <span class="assign_class label{{getKeyid('name',$data)}}" data-id="{{getKeyid('name',$data) }}" data-value="{{checkKey('name',$data) }}" >
                                            {!! checkKey('name',$data) !!}
                                          </span>
                                        </th>
                                      @endif
                                    </tr>
                                </thead>
                                <tbody>
                                  @foreach($positions as $key=>$value)
                                    <tr>
                                      @if(Auth()->user()->hasPermissionTo('employeeStaffGroupPermision_checkbox') || Auth::user()->all_companies == 1 )
                                        <td>
                                            @if($value->enable == 1)
                                              <input type="hidden" name="position_id[]" value="{{$value->id}}">
                                            @endif
                                            <div class="form-check">
                                              <input type="checkbox"
                                              {{($value->enable == 1) ? 'checked' : '' }}
                                               class="form-check-input staff_group_permission_checked" id="user_group{{$value->id}}" value="{{$value->id}}" name="user_group[]">
                                              <label class="form-check-label" for="user_group{{$value->id}}">
                                              </label>
                                            </div>
                                        </td>
                                      @endif
                                      @if(Auth()->user()->hasPermissionTo('employeeStaffGroupPermision_name') || Auth::user()->all_companies == 1 )
                                        <td>{!! $value->name !!}
                                        </td>
                                       @endif
                                    </tr>
                                  @endforeach()
                                </tbody>
                            </table>
                            <button class="form_submit_check btn btn-outline-info btn-rounded btn-block my-4 waves-effect z-depth-0" type="submit">
                              <span class="assign_class label{{getKeyid('update',$data)}}" data-id="{{getKeyid('update',$data) }}" data-value="{{checkKey('update',$data) }}" >
                                {!! checkKey('update',$data) !!}
                              </span>
                            </button>
                        </form>
                    </div>
                </div>
              </div>
               <div class="tab-pane fade " id="home" role="tabpanel" aria-labelledby="home-tab">
                <div class="card-body">
                    <div class="table-responsive">
                        <form  action="{{url('staff/update')}}" method="post">
                            @csrf
                            <table class="company_table_static table  table-striped table-bordered " cellspacing="0" width="100%">
                                <thead>
                                  <tr>
                                    @if(Auth()->user()->hasPermissionTo('employeeStaffPermision_checkbox') || Auth::user()->all_companies == 1 )
                                      <th class="all_checkboxes_style">
                                          <div class="form-check">
                                            <input type="checkbox" class="form-check-input" id="staff_checkbox_all">
                                            <label class="form-check-label" for="staff_checkbox_all">
                                              <span class="assign_class label{{getKeyid('all',$data)}}" data-id="{{getKeyid('all',$data) }}" data-value="{{checkKey('all',$data) }}" >
                                                {!! checkKey('all',$data) !!}
                                              </span>
                                            </label>
                                          </div>
                                      </th>
                                    @endif
                                    @if(Auth()->user()->hasPermissionTo('employeeStaffPermision_name') || Auth::user()->all_companies == 1 )
                                      <th>
                                        <span class="assign_class label{{getKeyid('name',$data)}}" data-id="{{getKeyid('name',$data) }}" data-value="{{checkKey('name',$data) }}" >
                                          {!! checkKey('name',$data) !!}
                                        </span>
                                      </th>
                                    @endif
                                  </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $key=>$value)
                                    <tr>
                                      @if(Auth()->user()->hasPermissionTo('employeeStaffPermision_checkbox') || Auth::user()->all_companies == 1 )
                                        <td>
                                          @if($value->enable == 1)
                                            <input type="hidden" name="user_id[]" value="{{$value->id}}">
                                          @endif
                                          <div class="form-check">
                                            <input type="checkbox" {{($value->enable == 1) ? 'checked' : '' }} class="form-check-input staff_checked" id="user{{$value->id}}" value="{{$value->id}}" name="user[]">
                                            <label class="form-check-label" for="user{{$value->id}}">
                                            </label>
                                          </div>
                                        </td>
                                      @endif
                                      @if(Auth()->user()->hasPermissionTo('employeeStaffPermision_name') || Auth::user()->all_companies == 1 )
                                        <td>{{$value->first_name}}
                                        </td>
                                      @endif
                                    </tr>
                                @endforeach()
                                </tbody>
                            </table>
                            <button class="form_submit_check btn btn-outline-info btn-rounded btn-block my-4 waves-effect z-depth-0" type="submit">
                              <span class="assign_class label{{getKeyid('update',$data)}}" data-id="{{getKeyid('update',$data) }}" data-value="{{checkKey('update',$data) }}" >
                                {!! checkKey('update',$data) !!}
                              </span>
                            </button>
                        </form>
                    </div>
                </div>
              </div>
              <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                  <div class="card-body">
                      <div class="table-responsive">
                          <form  action="{{url('staff/role/update')}}" method="post">
                              @csrf
                              <table class="company_table_static table  table-striped table-bordered " cellspacing="0" width="100%">
                                  <thead>
                                  <tr>
                                    @if(Auth()->user()->hasPermissionTo('employeeStaffRolePermision_checkbox') || Auth::user()->all_companies == 1 )
                                      <th class="all_checkboxes_style">
                                        <div class="form-check">
                                          <input type="checkbox" class="form-check-input" id="staff_roles_checkbox_all">
                                          <label class="form-check-label" for="staff_roles_checkbox_all">
                                          <span class="assign_class label{{getKeyid('all',$data)}}" data-id="{{getKeyid('all',$data) }}" data-value="{{checkKey('all',$data) }}" >
                                            {!! checkKey('all',$data) !!}
                                          </span>
                                          </label>
                                        </div>
                                      </th>
                                    @endif
                                    @if(Auth()->user()->hasPermissionTo('employeeStaffRolePermision_name') || Auth::user()->all_companies == 1 )
                                      <th>
                                        <span class="assign_class label{{getKeyid('name',$data)}}" data-id="{{getKeyid('name',$data) }}" data-value="{{checkKey('name',$data) }}" >
                                          {!! checkKey('name',$data) !!}
                                        </span>
                                      </th>
                                    @endif
                                  </tr>
                                  </thead>
                                  <tbody>
                                  @foreach($roles as $key=>$value)
                                    <tr>
                                      @if(Auth()->user()->hasPermissionTo('employeeStaffRolePermision_checkbox') || Auth::user()->all_companies == 1 )
                                        <td>
                                          @if($value->enable == 1)
                                            <input type="hidden" name="role_id[]" value="{{$value->id}}">
                                          @endif
                                          <div class="form-check">
                                            <input type="checkbox" {{($value->enable == 1) ? 'checked' : '' }} class="form-check-input staff_roles_checked" id="role{{$value->id}}" value="{{$value->id}}" name="role[]">
                                            <label class="form-check-label" for="role{{$value->id}}">
                                            </label>
                                          </div>
                                        </td>
                                      @endif
                                      @if(Auth()->user()->hasPermissionTo('employeeStaffRolePermision_name') || Auth::user()->all_companies == 1 )
                                        <td>{{$value->name}}
                                        </td>
                                      @endif
                                    </tr>
                                  @endforeach()
                                  </tbody>
                              </table>
                              <button class="form_submit_check btn btn-outline-info btn-rounded btn-block my-4 waves-effect z-depth-0" type="submit">
                                <span class="assign_class label{{getKeyid('update',$data)}}" data-id="{{getKeyid('update',$data) }}" data-value="{{checkKey('update',$data) }}" >
                                  {!! checkKey('update',$data) !!}
                                </span>
                              </button>
                          </form>
                      </div>
                  </div>
              </div>
            </div>
        </div>
      </div>
    </div>
@include('backend.label.input_label')
@endsection

