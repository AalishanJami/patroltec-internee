@php
        $data=localization();
@endphp
<style type="text/css">
 .form-custom-boutton{
      margin-left: unset!important;
      padding: 0.3rem!important;
   }
</style>
<div class="modal fade" id="modelEditRelationship" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Relation Ship</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="registernewcompany" method="POST" >
                <div class="modal-body">
                    <div class="row">
                        <div class="col">
                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="" required autocomplete="name" autofocus>
                        <label data-error="wrong" data-success="right" for="name" >
                            Title
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                            data-content="tool tip for Title"></i>
                        </label>
                    </div>
                </div>
                        <div class="col">

                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="" required autocomplete="name" autofocus>
                        <label data-error="wrong" data-success="right" for="name" >
                            First Name
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                            data-content="tool tip for First Name"></i>
                        </label>
                    </div>
                </div>
            </div>

            <div class="row">
                        <div class="col">
                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="" required autocomplete="name" autofocus>
                        <label data-error="wrong" data-success="right" for="name" >
                            Surname
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                            data-content="tool tip for Surname"></i>
                        </label>
                    </div>
                </div>

            <div class="col">
                   <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="" required autocomplete="name" autofocus>
                        <label data-error="wrong" data-success="right" for="name" >
                            Email
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                            data-content="tool tip for Surname"></i>
                        </label>
                    </div>
                </div>
            </div>
                <div class="form-row">
                <div class="col">
                     <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="" required autocomplete="name" autofocus>
                        <label data-error="wrong" data-success="right" for="name" >
                            Phone Number
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                            data-content="tool tip for >Phone Number"></i>
                        </label>
                    </div>
                    </div>
                </div>
                    <div class="form-row">
                       <div class="col">
                           <a class="text-success"  data-toggle="modal" data-target="#modalClientIndex" >
                              <button type="button" class="btn btn-sm btn-outline-primary waves-effect form-custom-boutton" style="float: left;">+Relation Ship</button>
                           </a>
                             
                                 <div class="md-form">
                                    
                                    <select searchable="Search here.."  class="mdb-select">
                                       <option value="">Choose option</option>
                                       <option value="755">Brother</option>
                                       <option value="748">Sister</option>
                                  
                                    </select>
                                 </div>
                           </div>
                    </div>
 
                 
           


                <div class="modal-footer d-flex justify-content-center">
                    <button class="btn btn-primary btn-sm" type="submit"><span class="assign_class label{{getKeyid('save',$data)}}" data-id="{{getKeyid('save',$data) }}" data-value="{{checkKey('save',$data) }}" >{!! checkKey('save',$data) !!} </span></button>
                </div>
            </form>
        </div>
    </div>
</div>
@component('backend.client.model.index')
@endcomponent


