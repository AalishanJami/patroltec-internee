<table class="table table-bordered">
    <thead>
    <tr>
        @if(Auth()->user()->hasPermissionTo('nextofkin_title_excel_export') || Auth::user()->all_companies == 1 )
            <th> Title</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('nextofkin_firstname_excel_export') || Auth::user()->all_companies == 1 )
            <th> First Name</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('nextofkin_surname_excel_export') || Auth::user()->all_companies == 1 )
            <th> Surname</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('nextofkin_email_excel_export') || Auth::user()->all_companies == 1 )
            <th> Email</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('nextofkin_relationship_excel_export') || Auth::user()->all_companies == 1 )
            <th> Relationship</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('nextofkin_phone_excel_export') || Auth::user()->all_companies == 1 )
            <th>Phone Number</th>
        @endif
    </tr>
    </thead>
    <tbody>
    @foreach($data as $row)
        <tr>
            @if(Auth()->user()->hasPermissionTo('nextofkin_title_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{ $row->title }}</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('nextofkin_firstname_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{ $row->first_name }}</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('nextofkin_surname_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{ $row->surname }}</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('nextofkin_email_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{ $row->email }}</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('nextofkin_relationship_excel_export') || Auth::user()->all_companies == 1 )
                <td>@if(!empty($row->relation_ship->name)){{ $row->relation_ship->name }}@endif</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('nextofkin_phone_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{ $row->phone_number }}</td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>
