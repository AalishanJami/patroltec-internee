@include('backend.layouts.pdf_start')
    <thead>
    <tr>
        @if(Auth()->user()->hasPermissionTo('nextofkin_title_pdf_export') || Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout"> Title</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('nextofkin_firstname_pdf_export') || Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout"> First Name</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('nextofkin_surname_pdf_export') || Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout"> Surname</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('nextofkin_email_pdf_export') || Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout"> Email</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('nextofkin_relationship_pdf_export') || Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout"> Relationship</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('nextofkin_phone_pdf_export') || Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout">Phone Number</th>
        @endif
    </tr>
    </thead>
    <tbody>
        @foreach($data as $row)
            @if(!empty($row))
            <tr>
                @if(Auth()->user()->hasPermissionTo('nextofkin_title_pdf_export') || Auth::user()->all_companies == 1 )
                    <td class="pdf_table_layout"><p class="col_text_style">{{ $row->title }}</p></td>
                @endif
                @if(Auth()->user()->hasPermissionTo('nextofkin_firstname_pdf_export') || Auth::user()->all_companies == 1 )
                    <td class="pdf_table_layout"><p class="col_text_style">{{ $row->first_name }}</p></td>
                @endif
                @if(Auth()->user()->hasPermissionTo('nextofkin_surname_pdf_export') || Auth::user()->all_companies == 1 )
                    <td class="pdf_table_layout"><p class="col_text_style">{{ $row->surname }}</p></td>
                @endif
                @if(Auth()->user()->hasPermissionTo('nextofkin_email_pdf_export') || Auth::user()->all_companies == 1 )
                    <td class="pdf_table_layout"><p class="col_text_style">{{ $row->email }}</p></td>
                @endif
                @if(Auth()->user()->hasPermissionTo('nextofkin_relationship_pdf_export') || Auth::user()->all_companies == 1 )
                    <td class="pdf_table_layout"><p class="col_text_style">@if(!empty($row->relation_ship->name)){{ $row->relation_ship->name }}@endif</p></td>
                @endif
                @if(Auth()->user()->hasPermissionTo('nextofkin_phone_pdf_export') || Auth::user()->all_companies == 1 )
                    <td class="pdf_table_layout"><p class="col_text_style">{{ $row->phone_number }}</p></td>
                @endif
            </tr>
            @endif
        @endforeach
    </tbody>
@include('backend.layouts.pdf_end')