@extends('backend.layouts.user')
@section('title', 'Next Of Kin')
@section('headscript')
    <link rel="stylesheet" href="{{ asset('custom/css/custom_style.css') }}">
@stop
@section('content')
    @php
        $data=localization();
    @endphp
    @include('backend.layouts.user_sidebar')
    @include('backend.label.input_label')
    <div class="padding-left-custom remove-padding">
        {!! breadcrumInner('next_of_kin',Session::get('employee_id'),'users','/employee/detail/'.Session::get('employee_id'),'employees','/employee') !!}
        <div class="locations-form container-fluid form_body">
            <div class="custom_clear locations-form container-fluid form_body">
                <div class="custom_clear card">
                    <ul  class="custom_clear nav nav-tabs" id="myTab" role="tablist">
                        @if(Auth()->user()->hasPermissionTo('view_nextofkin') || Auth::user()->all_companies == 1 )
                            <li class="custom_clear nav-item">
                                <a class="custom_clear nav-link active" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">
                                <span class="assign_class label{{getKeyid('next_of_kin',$data)}}" data-id="{{getKeyid('next_of_kin',$data) }}" data-value="{{checkKey('next_of_kin',$data) }}" >
                                    {!! checkKey('next_of_kin',$data) !!}
                                </span>
                                </a>
                            </li>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('view_employee_relationship') || Auth::user()->all_companies == 1 )
                            <li class="custom_clear nav-item">
                                <a class="custom_clear nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">
                                    <span class="assign_class label{{getKeyid('relationship',$data)}}" data-id="{{getKeyid('relationship',$data) }}" data-value="{{checkKey('relationship',$data) }}" >
                                        {!! checkKey('relationship',$data) !!}
                                    </span>
                                </a>
                            </li>
                        @endif
                    </ul>
                    <div class="custom_clear tab-content" id="myTabContent">
                        <div class="custom_clear tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <div class="card-body">
                                <div id="table_next_kin" class="table-responsive table-editable">
                                    @if(Auth()->user()->hasPermissionTo('view_nextofkin') || Auth::user()->all_companies == 1 )
                                        <button class="btn btn-danger btn-sm" id="deleteselectednext_kin">
                                        <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                                            {!! checkKey('selected_delete',$data) !!}
                                        </span>
                                        </button>
                                    @endif
                                    @if(Auth()->user()->hasPermissionTo('delete_selected_nextofkin') || Auth::user()->all_companies == 1 )
                                        <button class="btn btn-danger btn-sm" style="display: none;" id="deleteselectednext_kinSoft">
                                        <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                                            {!! checkKey('selected_delete',$data) !!}
                                        </span>
                                        </button>
                                    @endif
                                    @if(Auth()->user()->hasPermissionTo('selected_active_nextofkin') || Auth::user()->all_companies == 1 )
                                        <button class="btn btn-success btn-sm" style="display: none;" id="selectedactivebuttonnext_kin">
                                        <span class="assign_class label{{getKeyid('selected_active',$data)}}" data-id="{{getKeyid('selected_active',$data) }}" data-value="{{checkKey('selected_active',$data) }}" >
                                            {!! checkKey('selected_active',$data) !!}
                                        </span>
                                        </button>
                                    @endif
                                    @if(Auth()->user()->hasPermissionTo('selected_restore_nextofkin') || Auth::user()->all_companies == 1 )
                                        <button class="btn btn-primary btn-sm" id="restorebuttonnext_kin">
                                        <span class="assign_class label{{getKeyid('restore',$data)}}" data-id="{{getKeyid('restore',$data) }}" data-value="{{checkKey('restore',$data) }}" >
                                            {!! checkKey('restore',$data) !!}
                                        </span>
                                        </button>
                                    @endif
                                    @if(Auth()->user()->hasPermissionTo('active_nextofkin') || Auth::user()->all_companies == 1 )
                                        <button  style="display: none;" class="btn btn-primary btn-sm" id="activebuttonnext_kin">
                                        <span class="assign_class label{{getKeyid('show_active',$data)}}" data-id="{{getKeyid('show_active',$data) }}" data-value="{{checkKey('show_active',$data) }}" >
                                            {!! checkKey('show_active',$data) !!}
                                        </span>
                                        </button>
                                    @endif
                                    @if(Auth()->user()->hasPermissionTo('csv_nextofkin') || Auth::user()->all_companies == 1 )
                                        <form class="form-style" method="POST" id="export_excel_next_kin" action="{{ url('/next/kin/export/excel') }}">
                                            <input type="hidden"  name="export_excel_next_kin" value="1">
                                            <input type="hidden" class="next_kin_export" name="excel_array" value="1">
                                            {!! csrf_field() !!}
                                            <button  type="submit" id="export_excel_next_kin_btn" class="form_submit_check btn btn-warning btn-sm">
                                                <span class="assign_class label{{getKeyid('excel_export',$data)}}" data-id="{{getKeyid('excel_export',$data) }}" data-value="{{checkKey('excel_export',$data) }}" >
                                                    {!! checkKey('excel_export',$data) !!}
                                                </span>
                                            </button>
                                        </form>
                                    @endif
                                    @if(Auth()->user()->hasPermissionTo('word_nextofkin') || Auth::user()->all_companies == 1 )
                                        <form class="form-style" method="POST" id="export_world_next_kin" action="{{ url('/next/kin/export/world') }}">
                                            <input type="hidden"  name="export_world_next_kin" value="1">
                                            <input type="hidden" class="next_kin_export" name="word_array" value="1">
                                            {!! csrf_field() !!}
                                            <button  type="submit" id="export_world_next_kin_btn" class="btn btn-success btn-sm form_submit_check">
                                                <span class="assign_class label{{getKeyid('word_export',$data)}}" data-id="{{getKeyid('word_export',$data) }}" data-value="{{checkKey('word_export',$data) }}" >
                                                    {!! checkKey('word_export',$data) !!}
                                                </span>
                                            </button>
                                        </form>
                                    @endif
                                    @if(Auth()->user()->hasPermissionTo('pdf_nextofkin') || Auth::user()->all_companies == 1 )
                                        <form  class="form-style"  {{pdf_view('user_next_kin')}} id="export_pdf_next_kin" method="POST" action="{{ url('/next/kin/export/pdf') }}">
                                            <input type="hidden" name="export_world_pdf" value="1">
                                            <input type="hidden" class="next_kin_export" name="pdf_array" value="1">
                                            {!! csrf_field() !!}
                                            <button  type="submit" id="export_pdf_next_kin_btn"  class="btn btn-secondary btn-sm form_submit_check">
                                                <span class="assign_class label{{getKeyid('pdf_export',$data)}}" data-id="{{getKeyid('pdf_export',$data) }}" data-value="{{checkKey('pdf_export',$data) }}" >
                                                    {!! checkKey('pdf_export',$data) !!}
                                                </span>
                                            </button>
                                        </form>
                                    @endif
                                    @if(Auth()->user()->hasPermissionTo('create_nextofkin') || Auth::user()->all_companies == 1 )
                                        <span class="add_next_kin float-right mb-3 mr-2">
                                        <a class="text-success">
                                            <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
                                        </a>
                                    </span>
                                    @endif
                                    @if(Auth()->user()->hasPermissionTo('view_nextofkin') || Auth::user()->all_companies == 1 )
                                        <div class="table-responsive">
                                            <table id="next_kin_table" style="width:100% !important;" class="table table-bordered  table-striped text-center">
                                                <thead>
                                                <tr>
                                                    <th class="no-sort all_checkboxes_style">
                                                        <div class="form-check">
                                                            <input type="checkbox" class="form-check-input nextofkin_checked" id="nextofkin_checkbox_all">
                                                            <label class="form-check-label" for="nextofkin_checkbox_all">
                                                                <span class="assign_class label{{getKeyid('all',$data)}}" data-id="{{getKeyid('all',$data) }}" data-value="{{checkKey('all',$data) }}" >
                                                                    {!! checkKey('all',$data) !!}
                                                                </span>
                                                            </label>
                                                        </div>
                                                    </th>
                                                    <th>
                                                        <span class="assign_class label{{getKeyid('title',$data)}}" data-id="{{getKeyid('title',$data) }}" data-value="{{checkKey('title',$data) }}" >
                                                            {!! checkKey('title',$data) !!}
                                                        </span>
                                                    </th>
                                                    <th>
                                                        <span class="assign_class label{{getKeyid('first_name',$data)}}" data-id="{{getKeyid('first_name',$data) }}" data-value="{{checkKey('first_name',$data) }}" >
                                                            {!! checkKey('first_name',$data) !!}
                                                        </span>
                                                    </th>
                                                    <th>
                                                        <span class="assign_class label{{getKeyid('surname',$data)}}" data-id="{{getKeyid('surname',$data) }}" data-value="{{checkKey('surname',$data) }}" >
                                                            {!! checkKey('surname',$data) !!}
                                                        </span>
                                                    </th>
                                                    <th>
                                                        <span class="assign_class label{{getKeyid('email',$data)}}" data-id="{{getKeyid('email',$data) }}" data-value="{{checkKey('email',$data) }}" >
                                                            {!! checkKey('email',$data) !!}
                                                        </span>
                                                    </th>
                                                    <th>
                                                        <span class="assign_class label{{getKeyid('relationship',$data)}}" data-id="{{getKeyid('relationship',$data) }}" data-value="{{checkKey('relationship',$data) }}" >
                                                            {!! checkKey('relationship',$data) !!}
                                                        </span>
                                                    </th>
                                                    <th>
                                                        <span class="assign_class label{{getKeyid('number',$data)}}" data-id="{{getKeyid('number',$data) }}" data-value="{{checkKey('number',$data) }}" >
                                                            {!! checkKey('number',$data) !!}
                                                        </span>
                                                    </th>
                                                    <th class="no-sort all_action_btn" id="action"></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    @endif
                                    @include('backend.nextkin.input_nexki_permission')
                                </div>
                            </div>
                        </div>
                        <div class="custom_clear tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                            <div class="card-body">
                                <div id="table_relationship" class="table-editable table-responsive">
                                    @if(Auth()->user()->hasPermissionTo('selected_delete_employee_relationship') || Auth::user()->all_companies == 1 )
                                        <button class="btn btn-danger btn-sm" id="deleteselectedrelationship">
                                            <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                                                {!! checkKey('selected_delete',$data) !!}
                                            </span>
                                        </button>
                                        <button class="btn btn-danger btn-sm" style="display: none;" id="deleteselectedrelationshipSoft">
                                            <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                                                {!! checkKey('selected_delete',$data) !!}
                                            </span>
                                        </button>
                                    @endif
                                    @if(Auth()->user()->hasPermissionTo('selected_active_employee_relationship') || Auth::user()->all_companies == 1 )
                                        <button class="btn btn-success btn-sm" style="display: none;" id="selectedactivebuttonrelationship">
                                            <span class="assign_class label{{getKeyid('selected_active',$data)}}" data-id="{{getKeyid('selected_active',$data) }}" data-value="{{checkKey('selected_active',$data) }}" >
                                                {!! checkKey('selected_active',$data) !!}
                                            </span>
                                        </button>
                                    @endif
                                    @if(Auth()->user()->hasPermissionTo('selected_restore_delete_employee_relationship') || Auth::user()->all_companies == 1 )
                                        <button class="btn btn-primary btn-sm" id="restorebuttonrelationship">
                                            <span class="assign_class label{{getKeyid('restore',$data)}}" data-id="{{getKeyid('restore',$data) }}" data-value="{{checkKey('restore',$data) }}" >
                                                {!! checkKey('restore',$data) !!}
                                            </span>
                                        </button>
                                    @endif
                                    @if(Auth()->user()->hasPermissionTo('active_employee_relationship') || Auth::user()->all_companies == 1 )
                                        <button  style="display: none;" class="btn btn-primary btn-sm" id="activebuttonrelationship">
                                            <span class="assign_class label{{getKeyid('show_active',$data)}}" data-id="{{getKeyid('show_active',$data) }}" data-value="{{checkKey('show_active',$data) }}" >
                                                {!! checkKey('show_active',$data) !!}
                                            </span>
                                        </button>
                                    @endif
                                    @if(Auth()->user()->hasPermissionTo('csv_employee_relationship') || Auth::user()->all_companies == 1 )
                                        <form class="form-style" method="POST" id="export_excel_relationship" action="{{ url('relationship/export/excel') }}">
                                            <input type="hidden"  name="export_excel_relationship" value="1">
                                            <input type="hidden" class="relationship_export" name="excel_array" value="1">
                                            {!! csrf_field() !!}
                                            <button  type="submit" id="export_excel_relationship_btn" class="form_submit_check btn btn-warning btn-sm">
                                                <span class="assign_class label{{getKeyid('excel_export',$data)}}" data-id="{{getKeyid('excel_export',$data) }}" data-value="{{checkKey('excel_export',$data) }}" >
                                                    {!! checkKey('excel_export',$data) !!}
                                                </span>
                                            </button>
                                        </form>
                                    @endif
                                    @if(Auth()->user()->hasPermissionTo('word_employee_relationship') || Auth::user()->all_companies == 1 )
                                        <form class="form-style" method="POST" id="export_world_relationship" action="{{ url('relationship/export/world') }}">
                                            <input type="hidden"  name="export_world_relationship" value="1">
                                            <input type="hidden" class="relationship_export" name="word_array" value="1">
                                            {!! csrf_field() !!}
                                            <button  type="submit" id="export_world_relationship_btn" class="form_submit_check btn btn-success btn-sm">
                                                <span class=" assign_class label{{getKeyid('word_export',$data)}}" data-id="{{getKeyid('word_export',$data) }}" data-value="{{checkKey('word_export',$data) }}" >
                                                    {!! checkKey('word_export',$data) !!}
                                                </span>
                                            </button>
                                        </form>
                                    @endif
                                    @if(Auth()->user()->hasPermissionTo('pdf_employee_relationship') || Auth::user()->all_companies == 1 )
                                        <form  class="form-style" {{pdf_view('relation_ships')}} id="export_pdf_relationship" method="POST" action="{{ url('relationship/export/pdf') }}">
                                            <input type="hidden" name="export_world_pdf" value="1">
                                            <input type="hidden" class="relationship_export" name="pdf_array" value="1">
                                            {!! csrf_field() !!}
                                            <button  type="submit" id="export_pdf_relationship_btn"  class="form_submit_check btn btn-secondary btn-sm">
                                                <span class=" assign_class label{{getKeyid('pdf_export',$data)}}" data-id="{{getKeyid('pdf_export',$data) }}" data-value="{{checkKey('pdf_export',$data) }}" >
                                                    {!! checkKey('pdf_export',$data) !!}
                                                </span>
                                            </button>
                                        </form>
                                    @endif
                                    @if(Auth()->user()->hasPermissionTo('create_employee_relationship') || Auth::user()->all_companies == 1 )
                                        <span class="add_relation float-right mb-3 mr-2">
                                          <a class="text-success">
                                            <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
                                          </a>
                                        </span>
                                    @endif
                                    <table id="relationship_table" style="width:100% !important;" class="table table-bordered table-responsive-md table-striped text-center">
                                        <thead>
                                        <tr>
                                            <th class="no-sort all_checkboxes_style">
                                                <div class="form-check">
                                                    <input type="checkbox" class="form-check-input relationship_checked" id="relationship_checkbox_all">
                                                    <label class="form-check-label" for="relationship_checkbox_all">
                                                        <span class="assign_class label{{getKeyid('all',$data)}}" data-id="{{getKeyid('all',$data) }}" data-value="{{checkKey('all',$data) }}" >
                                                            {!! checkKey('all',$data) !!}
                                                        </span>
                                                    </label>
                                                </div>
                                            </th>
                                            <th class="text-center all_name_style">
                                                <span class=" assign_class label{{getKeyid('relationship',$data)}}" data-id="{{getKeyid('relationship',$data) }}" data-value="{{checkKey('relationship',$data) }}" >
                                                    {!! checkKey('relationship',$data) !!}
                                                </span>
                                            </th>
                                            <th class="no-sort all_action_btn"></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(Auth()->user()->all_companies == 1)
        <input type="hidden" id="relationship_checkbox" value="1">
        <input type="hidden" id="relationship_name" value="1">
        <input type="hidden" id="relationship_edit" value="1">
        <input type="hidden" id="relationship_create" value="1">
        <input type="hidden" id="edit_permision_relationship" value="1">
    @else
        <input type="hidden" id="relationship_checkbox" value="{{Auth()->user()->hasPermissionTo('employee_relationship_checkbox')}}">
        <input type="hidden" id="relationship_name" value="{{Auth()->user()->hasPermissionTo('employee_relationship_name')}}">
        <input type="hidden" id="relationship_edit" value="{{Auth()->user()->hasPermissionTo('employee_relationship_name_edit')}}">
        <input type="hidden" id="relationship_create" value="{{Auth()->user()->hasPermissionTo('employee_relationship_name_create')}}">
        <input type="hidden" id="edit_permision_relationship" value="{{Auth()->user()->hasPermissionTo('edit_employee_relationship')}}">
    @endif
    @component('backend.relationship.model.relationship')
    @endcomponent
    <script type="text/javascript" src="{{ asset('custom/js/next_kin.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('custom/js/relationship.js') }}" ></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $(".saverelationshipdata").hide();
            $("body").on('keyup','.edit_inline_relationship',function () {
                $('.deleterelationshipdata'+$(this).attr('data-id')).hide();
                $(".saverelationshipdata_show_"+$(this).attr('data-id')).show();
            });
            $("body").on('keyup','.edit_inline_next_kin',function () {
                $('.deletenext_kindata'+$(this).attr('data-id')).hide();
                $(".savenext_kindata_show_"+$(this).attr('data-id')).show();
            });

            var url='/next/kin/getall';
            next_kin_data(url);
            var url='/relationship/getall';
            relationship_data(url);
        });

        function relationship_data(url) {
            var table = $('#relationship_table').dataTable(
                {
                    processing: true,
                    columnDefs: [
                        { "orderable": false, "targets": 0 }
                    ],
                    language: {
                        'search':'<span class="assign_class label'+$('#search_id').attr('data-id')+'" data-id="'+$('#search_id').attr('data-id')+'" data-value="'+$('#search_id').val()+'" >'+$('#search_id').val()+'</span>',
                        'paginate':{
                            'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                            'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                        },
                        'lengthMenu': '_MENU_',
                        'info': ' ',
                    },
                    "ajax": {
                        "url": url,
                        "type": 'get',
                    },
                    "createdRow": function( row, data,dataIndex,columns )
                    {
                        var checkbox_permission=$('#checkbox_permission').val();
                        var checkbox='';
                        // checkbox+='<div class="form-check"><input type="checkbox" class="form-check-input relationship_checked" id="relationship'+data.id+'"><label class="form-check-label" for="relationship'+data.id+'""></label></div>';
                        checkbox+='<div class="form-check"><input type="checkbox" class="form-check-input relationship_checked selectedrowrelationship" id="relationship_checked'+data.id+'"><label class="form-check-label" for="relationship_checked'+data.id+'""></label></div>';

                        var submit='';
                        submit+='<a type="button" class="btn btn-primary btn-xs my-0 waves-effect all_action_btn_margin waves-light saverelationshipdata saverelationshipdata_show_'+data.id+'" style="display: none;" id="'+data.id+'"><i class="fas fa-check"></i></a>';
                        $(columns[0]).html(checkbox);
                        $(columns[1]).attr('id', 'name'+data['id']);
                        $(columns[1]).attr('data-id',data['id']);
                        if($('#edit_permision_relationship').val() && $('#relationship_edit').val())
                        {
                            $(columns[1]).attr('Contenteditable', 'true');
                            $(columns[2]).append(submit);
                        }
                        $(columns[1]).attr('onkeydown', 'editSelectedRow(relationship_tr'+data['id']+')');
                        $(columns[1]).attr('class','edit_inline_relationship');
                        $(row).attr('id', 'relationship_tr'+data['id']);
                        $(row).attr('data-id',data['id']);
                        $(row).attr('class', 'selectedrowrelationship'+data['id']);
                    },
                    columns:
                        [
                            {data: 'checkbox', name: 'checkbox',visible:$('#relationship_checkbox').val()},
                            {data: 'name', name: 'name',visible:$('#relationship_name').val()},
                            {data: 'actions', name: 'actions'},
                        ],

                }
            );
            if ($(":checkbox").prop('checked',true)){
                $(":checkbox").prop('checked',false);
            }
        }
        function checkSelected(value,checkValue)
        {
            if(value == checkValue)
            {
                return 'selected';
            }
            else
            {
                return "";
            }

        }

        function next_kin_data(url) {
            $.ajax({
                type: 'GET',
                url: url,
                success: function(response)
                {
                    $('#next_kin_table').DataTable().clear().destroy();
                    var table = $('#next_kin_table').dataTable(
                        {
                            processing: true,
                            columnDefs: [
                                { "orderable": false, "targets": 0 }
                            ],
                            language: {
                                'lengthMenu': '_MENU_',
                                'search':'<span class="assign_class label'+$('#search_id').attr('data-id')+'" data-id="'+$('#search_id').attr('data-id')+'" data-value="'+$('#search_id').val()+'" >'+$('#search_id').val()+'</span>',
                                'paginate':{
                                    'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                                    'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                                },
                                'info': ' ',
                            },
                            "ajax": {
                                "url": url,
                                "type": 'get',
                            },
                            "createdRow": function( row, data,dataIndex,columns )
                            {
                                var checkbox_permission=$('#checkbox_permission').val();
                                var checkbox='';
                                // checkbox+='<div class="form-check"><input type="checkbox" class="form-check-input next_kin_checked" id="next_kin'+data.id+'"><label class="form-check-label" for="next_kin'+data.id+'""></label></div>';
                                checkbox+='<div class="form-check"><input type="checkbox" class="form-check-input notes_checked selectedrownext_kin" id="next_kin'+data.id+'"><label class="form-check-label" for="next_kin'+data.id+'""></label></div>';
                                var submit='';
                                submit+='<a type="button" class="btn btn-primary btn-xs my-0 waves-effect all_action_btn_margin waves-light savenext_kindata savenext_kindata_show_'+data.id+'" style="display: none;" id="'+data.id+'"><i class="fas fa-check"></i></a>';
                                var relation_ship_id='';
                                var relationship=($('#nextofkin_relationship_edit').val()==1) ? "":"disabled";
                                relation_ship_id+='<select searchable="Search here.." onchange="editSelectedRow('+"next_kin_tr"+data.id+')"  id="relation_ship_id'+data.id+'"  style="background-color: inherit;border: 0px">';
                                for (var j = response.relationships.length - 1; j >= 0; j--) {
                                    relation_ship_id+='<option value="'+response.relationships[j].id+'" '+relationship+' '+checkSelected(data.relation_ship_id,response.relationships[j].id)+'  >'+response.relationships[j].name+'</option>';
                                }
                                relation_ship_id+='</select>';
                                if ($("#nextofkin_checkbox").val()==1){
                                    $(columns[0]).html(checkbox);
                                }
                                $(columns[1]).attr('id', 'title'+data['id']);
                                if($('#nextofkin_title_edit').val()==1 && $('#edit_nextofkin').val()==1){
                                    $(columns[1]).attr('Contenteditable', 'true');
                                }
                                $(columns[1]).attr('onkeydown', 'editSelectedRow(next_kin_tr'+data['id']+')');

                                $(columns[1]).attr('class','edit_inline_next_kin');
                                $(columns[1]).attr('data-id', data['id']);

                                $(columns[2]).attr('id', 'first_name'+data['id']);
                                $(columns[2]).attr('onkeydown', 'editSelectedRow(next_kin_tr'+data['id']+')');
                                $(columns[2]).attr('data-id', data['id']);
                                if($('#nextofkin_firstname_edit').val()==1 && $('#edit_nextofkin').val()==1){
                                    $(columns[2]).attr('Contenteditable', 'true');
                                }
                                $(columns[2]).attr('class','edit_inline_next_kin');

                                $(columns[3]).attr('onkeydown', 'editSelectedRow(next_kin_tr'+data['id']+')');
                                $(columns[3]).attr('id', 'surname'+data['id']);
                                if($('#nextofkin_surname_edit').val()==1 && $('#edit_nextofkin').val()==1){
                                    $(columns[3]).attr('Contenteditable', 'true');
                                }
                                $(columns[3]).attr('data-id', data['id']);
                                $(columns[3]).attr('class','edit_inline_next_kin');

                                $(columns[4]).attr('id', 'email'+data['id']);
                                if($('#nextofkin_email_edit').val()==1 && $('#edit_nextofkin').val()==1){
                                    $(columns[4]).attr('Contenteditable', 'true');
                                }
                                $(columns[4]).attr('onkeydown', 'editSelectedRow(next_kin_tr'+data['id']+')');
                                $(columns[4]).attr('data-id', data['id']);
                                $(columns[4]).attr('class','edit_inline_next_kin');

                                // $(columns[6).attr('id', 'title'+data['id']);
                                // $(columns[6).attr('Contenteditable', 'true');
                                // $(columns[6).attr('class','edit_inline_next_kin');
                                $(columns[5]).html(relation_ship_id);
                                $(columns[5]).attr('data-id', data['id']);
                                $(columns[6]).attr('id', 'phone_number'+data['id']);
                                if($('#nextofkin_phone_edit').val()==1 && $('#edit_nextofkin').val()==1){
                                    $(columns[6]).attr('Contenteditable', 'true');
                                }
                                $(columns[6]).attr('onkeydown', 'editSelectedRow(next_kin_tr'+data['id']+')');
                                $(columns[6]).attr('data-id', data['id']);
                                $(columns[6]).attr('class','edit_inline_next_kin');
                                if ($('#edit_nextofkin').val()==1){
                                    $(columns[7]).append(submit);
                                }
                                $(row).attr('id', 'next_kin_tr'+data['id']);
                                $(row).attr('class', 'selectedrownext_kin'+data['id']);
                                var temp=data['id'];
                                $(row).attr('data-id',data['id']);
                            },
                            columns:
                                [
                                    {data: 'checkbox', name: 'checkbox'},
                                    {data: 'title', name: 'title',visible:$('#nextofkin_title').val()},
                                    {data: 'first_name', name: 'first_name',visible:$('#nextofkin_firstname').val()},
                                    {data: 'surname', name: 'surname',visible:$('#nextofkin_surname').val()},
                                    {data: 'email', name: 'email',visible:$('#nextofkin_email').val()},
                                    {data: 'relation_ship_id', name: 'relation_ship_id',visible:$('#nextofkin_relationship').val()},
                                    {data: 'phone_number', name: 'phone_number',visible:$('#nextofkin_phone').val()},
                                    {data: 'actions', name: 'actions'},
                                ],
                        }
                    );
                },
                error: function (error) {
                    console.log(error);
                }
            });
            // (function ($) {
            //     var active ='<span class="assign_class label'+$('#breadcrumb_next_kin').attr('data-id')+'" data-id="'+$('#breadcrumb_next_kin').attr('data-id')+'" data-value="'+$('#breadcrumb_next_kin').val()+'" >'+$('#breadcrumb_next_kin').val()+'</span>';
            //     $('.breadcrumb-item.active').html(active);
            //     var parent ='<span class="assign_class label'+$('#dashboard_label').attr('data-id')+'" data-id="'+$('#dashboard_label').attr('data-id')+'" data-value="'+$('#dashboard_label').val()+'" >'+$('#dashboard_label').val()+'</span>';
            //     $('.breadcrumb-item a').html(parent);
            // }(jQuery));
            if ($(":checkbox").prop('checked',true)){
                $(":checkbox").prop('checked',false);
            }
        }
    </script>

@endsection
