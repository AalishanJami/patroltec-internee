@if(Auth()->user()->all_companies == 1)
    <input type="hidden" id="nextofkin_checkbox" value="1">
    <input type="hidden" id="nextofkin_title" value="1">
    <input type="hidden" id="nextofkin_firstname" value="1">
    <input type="hidden" id="nextofkin_surname" value="1">
    <input type="hidden" id="nextofkin_email" value="1">
    <input type="hidden" id="nextofkin_relationship" value="1">
    <input type="hidden" id="nextofkin_phone" value="1">
    <input type="hidden" id="edit_nextofkin" value="1">
    <input type="hidden" id="create_nextofkin" value="1">
    <input type="hidden" id="delete_nextofkin" value="1">
    <!--------create---->
    <input type="hidden" id="nextofkin_title_create" value="1">
    <input type="hidden" id="nextofkin_firstname_create" value="1">
    <input type="hidden" id="nextofkin_surname_create" value="1">
    <input type="hidden" id="nextofkin_email_create" value="1">
    <input type="hidden" id="nextofkin_relationship_create" value="1">
    <input type="hidden" id="nextofkin_phone_create" value="1">
    <!--------edit---->
    <input type="hidden" id="nextofkin_title_edit" value="1">
    <input type="hidden" id="nextofkin_firstname_edit" value="1">
    <input type="hidden" id="nextofkin_surname_edit" value="1">
    <input type="hidden" id="nextofkin_email_edit" value="1">
    <input type="hidden" id="nextofkin_relationship_edit" value="1">
    <input type="hidden" id="nextofkin_phone_edit" value="1">
@else
    <input type="hidden" id="nextofkin_checkbox" value="{{Auth()->user()->hasPermissionTo('nextofkin_checkbox')}}">
    <input type="hidden" id="nextofkin_title" value="{{Auth()->user()->hasPermissionTo('nextofkin_title')}}">
    <input type="hidden" id="nextofkin_firstname" value="{{Auth()->user()->hasPermissionTo('nextofkin_firstname')}}">
    <input type="hidden" id="nextofkin_surname" value="{{Auth()->user()->hasPermissionTo('nextofkin_surname')}}">
    <input type="hidden" id="nextofkin_email" value="{{Auth()->user()->hasPermissionTo('nextofkin_email')}}">
    <input type="hidden" id="nextofkin_relationship" value="{{Auth()->user()->hasPermissionTo('nextofkin_relationship')}}">
    <input type="hidden" id="nextofkin_phone" value="{{Auth()->user()->hasPermissionTo('nextofkin_phone')}}">
    <input type="hidden" id="edit_nextofkin" value="{{Auth()->user()->hasPermissionTo('edit_nextofkin')}}">
    <input type="hidden" id="create_nextofkin" value="{{Auth()->user()->hasPermissionTo('create_nextofkin')}}">
    <input type="hidden" id="delete_nextofkin" value="{{Auth()->user()->hasPermissionTo('delete_nextofkin')}}">
    <!--------create---->
    <input type="hidden" id="nextofkin_title_create" value="{{Auth()->user()->hasPermissionTo('nextofkin_title_create')}}">
    <input type="hidden" id="nextofkin_firstname_create" value="{{Auth()->user()->hasPermissionTo('nextofkin_firstname_create')}}">
    <input type="hidden" id="nextofkin_surname_create" value="{{Auth()->user()->hasPermissionTo('nextofkin_surname_create')}}">
    <input type="hidden" id="nextofkin_email_create" value="{{Auth()->user()->hasPermissionTo('nextofkin_email_create')}}">
    <input type="hidden" id="nextofkin_relationship_create" value="{{Auth()->user()->hasPermissionTo('nextofkin_relationship_create')}}">
    <input type="hidden" id="nextofkin_phone_create" value="{{Auth()->user()->hasPermissionTo('nextofkin_phone_create')}}">
    <!--------edit---->
    <input type="hidden" id="nextofkin_title_edit" value="{{Auth()->user()->hasPermissionTo('nextofkin_title_edit')}}">
    <input type="hidden" id="nextofkin_firstname_edit" value="{{Auth()->user()->hasPermissionTo('nextofkin_firstname_edit')}}">
    <input type="hidden" id="nextofkin_surname_edit" value="{{Auth()->user()->hasPermissionTo('nextofkin_surname_edit')}}">
    <input type="hidden" id="nextofkin_email_edit" value="{{Auth()->user()->hasPermissionTo('nextofkin_email_edit')}}">
    <input type="hidden" id="nextofkin_relationship_edit" value="{{Auth()->user()->hasPermissionTo('nextofkin_relationship_edit')}}">
    <input type="hidden" id="nextofkin_phone_edit" value="{{Auth()->user()->hasPermissionTo('nextofkin_phone_edit')}}">
@endif
