@extends('backend.layouts.backend')
@section('content')
    @php
       $data=localization();
    @endphp
    {{ Breadcrumbs::render('login/cms') }}
  


    <span class="table-add float-right mb-3 mr-2">
        <a class="text-success"  data-toggle="modal" data-target="#modalcmsLogin">
            <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
        </a>
    </span>
    @include('backend.label.input_label')
    <table id="editLoginDatatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th class="th-sm assign_class label{{getKeyid('cms.key',$data)}}" data-id="{{getKeyid('cms.key',$data) }}" data-value="{{ checkKey('cms.key',$data) }}" >{!!checkKey('cms.key',$data) !!}
            </th>
            <th class="th-sm assign_class label{{getKeyid('cms.value',$data)}}" data-id="{{getKeyid('cms.value',$data) }}" data-value="{{ checkKey('cms.value',$data) }}" >{!!checkKey('cms.value',$data) !!}
            </th>
         
        </tr>
        </thead>
           
        <tbody>
        </tbody>
    </table>
     @component('backend.edit_login.model.create')
    @endcomponent
     {{--    Edit Modal--}}
    @component('backend.edit_login.model.edit')
    @endcomponent
    {{--    END Edit Modal--}}
     {{--    Edit Modal--}}
    @component('backend.edit_login.model.create')
    @endcomponent
    {{--    END Edit Modal--}}
 <script type="text/javascript">
        $(document).ready(function() {
            $(function () {
                editLoginAppend();
            }); 


            (function ($) {
                var active ='<span class="assign_class label'+$('#cms_label').attr('data-id')+'" data-id="'+$('#cms_label').attr('data-id')+'" data-value="'+$('#cms_label').val()+'" >'+$('#cms_label').val()+'</span>';
                $('.breadcrumb-item.active').html(active);
                var parent ='<span class="assign_class label'+$('#dashboard_label').attr('data-id')+'" data-id="'+$('#dashboard_label').attr('data-id')+'" data-value="'+$('#dashboard_label').val()+'" >'+$('#dashboard_label').val()+'</span>';
                $('.breadcrumb-item a').html(parent);
            }(jQuery));
        
        }); 

        function editLoginAppend()
        {
            var table = $('#editLoginDatatable').dataTable({
               processing: true,
                language: {
                    'lengthMenu': '<span class="assign_class label'+$('#show_label').attr('data-id')+'" data-id="'+$('#show_label').attr('data-id')+'" data-value="'+$('#show_label').val()+'" >'+$('#show_label').val()+'</span>  _MENU_ <span class="assign_class label'+$('#entries_label').attr('data-id')+'" data-id="'+$('#entries_label').attr('data-id')+'" data-value="'+$('#entries_label').val()+'" >'+$('#entries_label').val()+'</span>',
                    'search':'<span class="assign_class label'+$('#search_label').attr('data-id')+'" data-id="'+$('#search_label').attr('data-id')+'" data-value="'+$('#search_label').val()+'" >'+$('#search_label').val()+'</span>',
                    'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                    'info': '<span class="assign_class label'+$('#showing_page_label').attr('data-id')+'" data-id="'+$('#showing_page_label').attr('data-id')+'" data-value="'+$('#showing_page_label').val()+'" >'+$('#showing_page_label').val()+'</span> _PAGE_ <span class="assign_class label'+$('#of_label').attr('data-id')+'" data-id="'+$('#of_label').attr('data-id')+'" data-value="'+$('#of_label').val()+'" >'+$('#of_label').val()+'</span> _PAGES_',
                    'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                    'paginate': {
                        'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                        'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                    },
                    'infoFiltered': "(filtered from _MAX_ total records)"
                },
                "ajax": {
                    "url": '/getalllogincms',
                    "type": 'get',
                },
              "createdRow": function( row, data, dataIndex ) {
                    // console.log(row);
                    $( row ).find('td:eq(0)') .attr('contenteditable',true);
                    $( row ).find('td:eq(1)') .attr('contenteditable',true);
                    // console.log(data);
                    // console.log(dataIndex)
                    // $(row).attr('id', data['id']);
                    // var temp=data['id'];
                    // $(row).attr('onclick',"selectedrowuser(this.id)");
                },
                columns: [
                    {data: 'key', name: 'key'},
                    {data: 'value', name: 'value'},
                    

                ],

               
            
                

            });


        }

 
    </script>
     

@endsection
                        