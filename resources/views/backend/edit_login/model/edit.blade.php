 @php
        $data=localization();
@endphp


<div class="modal fade" id="modalEditloginCms" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold"><span class="assign_class label{{getKeyid('title',$data)}}" data-id="{{getKeyid('title',$data) }}" data-value="{{checkKey('title',$data) }}" >{!! checkKey('title',$data) !!} </span></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
              <form >
                <div class="modal-body mx-3">
                    <input type="hidden" id="cms_id">
                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input type="text" class="form-control validate @error('key') is-invalid @enderror"  id="cms_key_edit"  required autocomplete="key" autofocus readonly>
                        <label data-error="wrong" data-success="right" for="key" class="active">
                            <span class="assign_class label{{getKeyid('key',$data)}}" data-id="{{getKeyid('key',$data) }}" data-value="{{checkKey('key',$data) }}" >{!! checkKey('key',$data) !!}
                            </span>
                        </label>
                    </div>
                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input type="text" class="form-control validate @error('value') is-invalid @enderror" id="cms_value_edit"  required autocomplete="value" autofocus>
                        <label data-error="wrong" data-success="right" for="value" class="active">
                            <span class="assign_class label{{getKeyid('value',$data)}}" data-id="{{getKeyid('value',$data) }}" data-value="{{checkKey('value',$data) }}" >{!! checkKey('value',$data) !!}
                            </span>
                        </label>
                    </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button class="cms_login_Edit btn btn-primary btn-sm" type="submit"><span class="assign_class label{{getKeyid('update',$data)}}" data-id="{{getKeyid('update',$data) }}" data-value="{{checkKey('update',$data) }}" >{!! checkKey('update',$data) !!} </span></button>
                </div>
            </form>

            <div class="modal-body mx-3">

           
            </div>
        </div>
    </div>
</div>
