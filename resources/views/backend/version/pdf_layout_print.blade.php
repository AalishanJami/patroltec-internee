@extends('backend.layouts.doc')
@section('title', 'Version Log View')
@section('content')
    @php
        $data=localization();
    @endphp
    <link rel="stylesheet" href="{{ asset('custom/css/versionlog_pdf_print.css') }}">
    <div class="row">
        <div class="col-md-12" style="padding:0px !important;">
            <table style="width:100%;">
                <tr style="width:100%;">
                    <td style="width: 10%;">
                        <img src="https://patroltec.net/backend/web/uploads/_180705101111.png" height="100" width="100"/><br>
                    </td>
                    <td style="width: 70%;text-align: left;padding-right: 30px;">
                        <span style="font-weight:bold;display: block;font-size: 24px;">{{$form_name}}</span>
                        <span class="user text-bottom" style="display: block;font-size: 12px;">
                             {{$date}}
                        </span>
                        <span class="user text-bottom" style="display: block;font-size: 12px;">
                             {{"Version".$version_id}}
                        </span>
                    </td>
                    <td style="width: 10%;text-align: right;">
                        <button class="mb-3 mr-2 btn btn-sm btn-success" style="float:right !important;" id="print_code" onclick="print_code()"><i class="fa fa-print" style="font-size:20px"></i></button>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12" style="padding-left: 0px;padding-right: 0px; border: 1px solid #e3e3e3 !important;padding-top: 0px !important;">
            <div class="card-deck"  style="overflow: hidden" id="div_for_dynamic_forms_display"></div>
        </div>
    </div>
    @include('backend.version.input_permission')
    @include('backend.label.input_label')
    <script src="{{asset('js/pdfview.js') }}"></script>
    <script type="text/javascript">
        var options = {
            pdfOpenParams: {toolbar: '0'}
        };
        PDFObject.embed("{{ url('static_form/view-pdf', ['id' => $id_static]) }}", "#pdf-viewer-static",options);
    </script>
    <script src="{{asset('/custom/js/version_log.js')}}"></script>
    <script src="{{asset('/custom/js/answer_group.js')}}"></script>

    <script type="text/javascript">
        // $(".form_datetime").datetimepicker({format: 'yyyy-mm-dd hh:ii'});
        var answergroupBool =false;
        var answerCount = 0;
        var answerGroupCount=0;
        function populate()
        {
            var formData = $("#dynamicForm").serializeObject();
            formData.pre_populate_id = $('#pre_populate_id').children("option:selected").val();
            $.ajax({
                type: 'POST',
                url: '/form/populate',
                data: {
                    "_token": "{{ csrf_token() }}",formData
                },
                success: function(response)
                {
                    toastr["success"](response.message);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
        var token = '@csrf';
        var upload ="{{url('answer/uploadIcon')}}";

        var url='/answer_group/getall';
        answer_group_data(url);
        function answer_group_data(url)
        {
            var table = $('.answer_group_table').dataTable(
                {
                    processing: true,
                    language: {
                        'lengthMenu': '  _MENU_ ',
                        'search':'<span class="assign_class label'+$('#answer_group_search_id').attr('data-id')+'" data-id="'+$('#answer_group_search_id').attr('data-id')+'" data-value="'+$('#answer_group_search_id').val()+'" >'+$('#answer_group_search_id').val()+'</span>',
                        'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                        'info':'',
                        'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                        'paginate': {
                            'previous': '<span class="assign_class label'+$('#answer_group_previous_label').attr('data-id')+'" data-id="'+$('#answer_group_previous_label').attr('data-id')+'" data-value="'+$('#answer_group_previous_label').val()+'" >'+$('#answer_group_previous_label').val()+'</span>',
                            'next': '<span class="assign_class label'+$('#answer_group_next_label').attr('data-id')+'" data-id="'+$('#answer_group_next_label').attr('data-id')+'" data-value="'+$('#answer_group_next_label').val()+'" >'+$('#answer_group_next_label').val()+'</span>',
                        },
                        'infoFiltered': "(filtered from _MAX_ total records)"
                    },
                    "ajax": {
                        "url": url,
                        "type": 'get',
                    },
                    "createdRow": function( row, data, dataIndex,columns )
                    {

                        var checkbox='';
                        checkbox+='<div class="form-check"><input type="checkbox" class="form-check-input" id="answer_group'+data.id+'"><label class="form-check-label" for="answer_group'+data.id+'""></label></div>';
                        $(columns[0]).html(checkbox);
                        if($('#edit_permision_document_answer_group').val() && $('#document_answer_group_edit').val())
                        {
                            $(columns[1]).attr('Contenteditable', 'true');
                            $(columns[1]).attr('class','edit_inline_answer_group');
                        }

                        $(columns[1]).attr('id', 'answer_group_name'+data['id']);

                        $(row).attr('id', 'answer_group_tr'+data['id']);
                        $(row).attr('class', 'selectedrowanswer_group');
                    },
                    columns:
                        [
                            {data: 'checkbox', name: 'checkbox',visible:$('#document_answer_group_checkbox').val()},
                            {data: 'name', name: 'name',visible:$('#document_answer_group_name').val()},
                            {data: 'actions', name: 'actions'},
                        ],
                }
            );
        }
        function get_group_answer_id($id){
            sessionStorage.setItem('answer_group_id', $id);
            $('#modalAnswertablelist').modal('show');
            var url='/answer/getall/'+$id;
            $('.answer_table').DataTable().clear().destroy();
            answer_data(url);
        }
    </script>
    <script src="{{asset('editor/sprite.svg.js')}}"></script>
    <script src="{{asset('custom/js/form_sections.js') }}"></script>
    <script type="text/javascript">

        function getAllForms(){
            $.ajax({
                type: 'GET',
                url: '/version/log/get/version',
                data:{
                    'varsion_id':'{{$version_id}}',
                },
                success: function(response)
                {
                    var form_section = response.form_section;
                    console.log(form_section);
                    var html = '';
                    for(var i = 0; i < form_section.length; i++){

                        html += ' <div id="'+form_section[i].order+'-'+form_section[i].id+'" style="padding:0px !important;margin:0px !important;" class=" sortable-card col-md-'+form_section[i].width+'">';
                        html += '<div class="mb-4  " id="form_section_height'+form_section[i].id+'" style="margin-bottom:0rem !important;">';
                        html += '<div class="card text-center">';
                        if (form_section[i].header_height!==null){
                            var header_height='height: '+form_section[i].header_height+'px;';
                        }else{
                            var header_height='';
                        }

                        if (form_section[i].show_header ==1){
                            html += '<div class="card-header" id="section_card_header'+form_section[i].id+'" style="display:flex;'+header_height+'">';
                            if ($("#versionLogDetail_form_header").val()=='1'){
                                if (form_section[i].header !==null){
                                    html += '<span  style="flex: 1; text-align:left;font-weight: 700;">'+form_section[i].header+'</span>';
                                }else{
                                    html += '<span style="flex: 1; text-align:left; ">&nbsp;&nbsp;&nbsp;&nbsp;</span>';
                                }
                            }else{
                                html +='<span  style="flex: 1; text-align:left; ">&nbsp;&nbsp;&nbsp;</span>';
                            }
                            html += '<span class="float-right mb-3 mr-2"></span>';
                            html += '</div>';
                        }
                        html += '<div class="card-body section_custom_height" id="section_custom_height'+form_section[i].id+'"> ';
                        html += '<input type="hidden" name="id" class="form_section" value="'+form_section[i].id+'">';
                        html += '<input type="hidden" id="formsectionheight_'+form_section[i].id+'">';

                        if(form_section[i].questions !=null)
                            if (form_section[i].form_section_column!==null){
                                html += '<div class="row">';
                            }
                            html+=questionSet(form_section[i].questions,form_section[i].shared,form_section[i].width,form_section[i].id,form_section[i].form_section_column);
                        if (form_section[i].form_section_column!==null){
                            html += '</div>';
                        }
                        html += '</div>';
                        if (form_section[i].footer_height!==null){
                            var footer_height='height: '+form_section[i].footer_height+'px;';
                        }else{
                            var footer_height='';
                        }

                        if (form_section[i].show_footer ==1){
                            html += '<div class="card-footer text-muted text-left" id="section_card_footer'+form_section[i].id+'" style="'+footer_height+'">';
                            if ($("#versionLogDetail_form_footer").val()=='1'){
                                if (form_section[i].footer !==null){
                                    html += form_section[i].footer;
                                }else{
                                    html +='<span>&nbsp;&nbsp;&nbsp;</span>';
                                }
                            }else{
                                html +='<span>&nbsp;&nbsp;&nbsp;</span>';
                            }
                            html += '</div>';
                        }
                        html += '</div>';
                        html += '</div>';
                        html += '</div>';
                    }

                    $('#div_for_dynamic_forms_display').html(html);
                    $('#div_for_dynamic_forms_display_pdf').html(html);
                    $('.mdb-select_forms').materialSelect();
                    $('.input_starttime').pickatime({});
                    $('.datepicker').pickadate({selectYears:250,});
                    $('select .mdb-select_forms, .initialized').hide();
                    $('.answertype_richtext').summernote();
                    setFormSectionHeight();
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }

        function setFormSectionHeight(){

        }

        function print_code(){
            $("#print_code").hide();
            window.print();
            $("#print_code").show();
        }
        function questionSet(data,flag,form_section_width=null,form_section_id,form_section_column) {
            var html='';
            for(var j = 0 ; j < data.length; j++){
                let question = data[j];
                section_question =form_section_id+'.'+question.id;
                if (form_section_column !==null){
                    var columnsize=parseFloat(12)/parseFloat(form_section_column);
                    html += '<div class="form_section_columnsize col-md-'+columnsize+'">';
                }
                html += '<div class="card card-cascade wider reverse" style="margin-top:-20px !important;">';
                if(form_section_width<=6){
                    var p_style = "margin-left:10px !important;";
                }else{
                    var p_style ="";
                }
                html += '<p class="card-title text-left" style="'+p_style+'">';
                if($("#versionLogDetail_question").val()=='1'){
                    html += '<strong>'+question.question+'</strong>';
                }
                html += '<input type="hidden" value="'+form_section_id+'.'+question.id+'" name="section_question_id"/>';
                html += '</p>';
                if(question.layout == 1)
                {
                    html += '<div style="padding: 0rem !important;" class="card-body hori_layout'+question.layout+'">';
                }
                html += '<div class="row text-center">';
                if($("#versionLogDetail_answertype").val()=='1'){
                    var answer_type = question.answer_type.name;
                    // var answer_type = question.answer_type.name;
                }else{
                    var answer_type ='';
                }
                switch (answer_type) {
                    case 'dropdown':
                        if ($("#versionLogDetail_answer").val()=='1'){
                            html += '<div class="col-md-12" style="padding-left:25px;text-align:left;">';
                            html += '<select searchable="Search here.." disabled style="width: 100% !important;"  class="mdb-select_forms select_forms_dropdown colorful-select dropdown-primary" name="dropdown.'+section_question+'" >';
                            if (question.answer_group)
                            {
                                html +=loopAnswer(question.answer_group.answer,1,section_question);
                            }
                            html +='</select>';
                            html += '</div>';
                        }
                        break;
                    case 'radio':
                        if ($("#versionLogDetail_answer").val()=='1'){
                            //var icon_size="width:"+question.icon_size+"%;height:auto;";
                            var icon_size="max-width:90%;height:auto;";
                            if (question.column_size !==null){
                                var num=parseFloat(12)/parseFloat(question.column_size);
                                var column_size =parseInt(num);
                            }else{
                                var column_size="";
                            }
                            if(question.layout == 2)
                            {
                                var layout_type ='col-md-6';
                                var style ='img-class img_style_verti';
                                var img_style= '';
                                if(form_section_width <=3){
                                    var column = 'col-md-12';
                                }else if(form_section_width <=5){
                                    var column ='col-md-9';
                                }else if(form_section_width ==6){
                                    var column ='col-md-9';
                                }else{
                                    var column ='col-md-6';
                                }
                                html += '<div class="'+column+' verti">';
                            }
                            else
                            {
                                var img_style= 'img_style';
                                var layout_type ='col-md-12';
                                var style ='';
                                html += '<div class="col-md-12 hori" style="padding-right: 40px;"><div class="row">';
                            }
                            if (question.answer_group)
                            {
                                html +=loopAnswer(question.answer_group.answer,2,style,img_style,form_section_width,section_question,layout_type,question.layout,icon_size,column_size);
                            }
                            html += '</div>';
                            if(question.layout == 1)
                            {
                                html += '</div>';
                            }
                        }
                        break;
                    case 'checkbox':
                        if ($("#versionLogDetail_answer").val()=='1'){
                            //var icon_size="width:"+question.icon_size+"%;height:auto;";
                            var icon_size="max-width:90%;height:auto;";
                            if (question.column_size !==null){
                                var num=parseFloat(12)/parseFloat(question.column_size);
                                var column_size =parseInt(num);
                            }else{
                                var column_size="";
                            }
                            if(question.layout == 2)
                            {
                                var layout_type ='col-md-6';
                                var style ='img-class img_style';
                                var img_style= 'img_style_verti_check';

                                if(form_section_width <=3){
                                    var column = 'col-md-12';
                                }else if(form_section_width <=5){
                                    var column = 'col-md-9';
                                }else if(form_section_width ==6){
                                    var column = 'col-md-9';
                                }else{
                                    var column = 'col-md-6';
                                }

                                html += '<div class="'+column+' verti">';

                            } else {
                                var img_style= 'img_style';
                                var layout_type ='col-md-12';
                                html += '<div class="col-md-12 hori" style="padding-right: 40px;"><div class="row">';
                            }
                            if (question.answer_group) {
                                html +=loopAnswer(question.answer_group.answer,6,style,img_style,form_section_width,section_question,layout_type,question.layout,icon_size,column_size);
                            }
                            // id="answer_type_id"
                            html += '</div>';
                            if(question.layout == 1)
                            {
                                html += '</div>';
                            }
                        }
                        break;
                    case 'multi':
                        if ($("#versionLogDetail_answer").val()=='1'){
                            html += '<div class="col-md-12" style="padding-left:23px !important;text-align:left;"><div class="md-form">';
                            html += '<select searchable="Search here.." disabled  class="mdb-select_forms " multiple name="multi.'+section_question+'" >';
                            if (question.answer_group)
                            {
                                html +=loopAnswer(question.answer_group.answer,1,section_question);
                            }
                            html +='</select>';
                            html += '</div></div>';
                        }
                        break;
                    case 'date':
                        if ($("#versionLogDetail_answer").val()=='1'){
                            html += '<div class="col-md-12">';
                            if (question.answer_group)
                            {
                                html +=withoutLoopAnswer(question.answer_group.answer,3,section_question,question.id);
                            }
                            html += '</div>';
                        }
                        break;
                    case 'time':
                        if ($("#versionLogDetail_answer").val()=='1'){
                            html += '<div class="col-md-12">';
                            if (question.answer_group)
                            {
                                html +=withoutLoopAnswer(question.answer_group.answer,4,section_question,question.id);

                            }
                            html += '</div>';
                        }
                        break;
                    case 'datetime':
                        // html += '<div class="col-md-12">';
                        // if (question.answer_group)
                        //       {
                        //         html +=withoutLoopAnswer(question.answer_group.answer,3,section_question);
                        //
                        //       }
                        //   html += '</div>';
                        break;
                    case 'text':
                        if ($("#versionLogDetail_answer").val()=='1'){
                            html += '<div class="col-md-12">';
                            if (question.answer_group)
                            {
                                html +=withoutLoopAnswer(question.answer_group.answer,5,section_question,question.id);

                            }
                            html += '</div>';
                        }
                        break;
                    case 'richtext':
                        if ($("#versionLogDetail_answer").val()=='1'){
                            html += '<div class="col-md-12">';
                            if (question.answer_group)
                            {
                                html +=withoutLoopAnswer(question.answer_group.answer,8,section_question,question.id);
                            }
                            html += '</div>';
                        }
                        break;
                    case 'comment':
                        if ($("#versionLogDetail_answer").val()=='1'){
                            html += '<div class="col-md-12">';
                            if (question.answer_group)
                            {

                                html +=withoutLoopAnswer(question.answer_group.answer,10,section_question,question.id,question.comment);

                            }
                            html += '</div>';
                        }
                        break;
                    case 'cloc':
                        if ($("#versionLogDetail_answer").val()=='1'){
                            html += '<div class="col-md-12">';
                            if (question.answer_group)
                            {

                                html +=withoutLoopAnswer(question.answer_group.answer,12,section_question,question.id);

                            }
                            html += '</div>';
                        }
                        break;
                    default:
                        if ($("#versionLogDetail_answer").val()=='1'){
                            html += '<div class="col-md-12">';
                            if (question.answer_group)
                            {

                                html +=withoutLoopAnswer(question.answer_group.answer,0,section_question,question.id);

                            }
                            html += '</div>';
                        }
                }
                html += '</div>';
                if(question.layout == 1)
                {
                    html += '</div>';
                }
                html += '</div>';
                if (form_section_column !==null){
                    html += '</div>';
                }
            }

            return html;
        }


        function loopAnswer(data,flag,custom_class=null,imgStyle=null,form_section_width=null,section_question,layout_type=null,layout=null,icon_size=null,column_size=null)
        {
            var html='';
            for(var k=0;k<data.length;k++)
            {
                let answer = data[k];
                if(answer.answer)
                {
                    switch(flag) {
                        case 1:
                            html += '<option value="'+answer.id+'">';
                            html += answer.answer;
                            html += '</option>';
                            break;
                        case 2:
                            if (layout==2){
                                if(form_section_width <=2){
                                    var img_style = 'margin-left:6px !important;';
                                    // var column = layout_type;
                                    var img_column = layout_type;
                                    var label_column =layout_type;
                                    var custom_sectionstart = "<div class='row'>";
                                    var custom_sectionend = "</div>";
                                }else{
                                    var img_style = 'margin-left:9px !important;';
                                    if (form_section_width <=8){
                                        var img_column = 'col-md-6';
                                        var label_column = 'col-md-6';
                                    }else{
                                        var img_column = 'col-md-4 ';
                                        var label_column = 'col-md-8';
                                    }
                                    // var column = layout_type;
                                    var custom_sectionstart = "";
                                    var custom_sectionend = "";
                                }
                                if(answer.icon!=null){
                                    var lineheight='line-height:60px;';
                                    if (form_section_width <=8){
                                        var img_column = 'col-md-6';
                                        var label_column = 'col-md-6';
                                    }else{
                                        var img_column = 'col-md-4 ';
                                        var label_column = 'col-md-8';
                                    }
                                    var verticla_abswer_label_radio='verticla_abswer_label_radio';
                                    var img='<img src="{{asset("/uploads/icons")}}/'+answer.icon+'" style="margin-left:10px !important;'+icon_size+'"   class=" '+imgStyle+'">';
                                }else{
                                    var lineheight ='';
                                    if (form_section_width <=6){
                                        var img_column = 'col-md-2';
                                        var label_column = 'col-md-10';
                                    }else{
                                        var img_column = 'col-md-1 ';
                                        var label_column = 'col-md-11';
                                    }
                                    var verticla_abswer_label_radio='';
                                    var img='';
                                }

                                html += custom_sectionstart;
                                html += '<div class="form-check form-check-form_section" style="padding-left:1rem !important;">';
                                html += '<div class="row" style="height: 100%;">';
                                html += '<div class="'+img_column+' text-left" style="'+lineheight+'">';

                                html += '<input type="radio" disabled class="form-check-input" id="'+section_question+'.'+answer.id+'" value="'+answer.id+'" name="answer_id.'+section_question+'">';
                                html += '<label style="text-align: center;height: 100%;" class="form-check-label abswer_label_radio '+verticla_abswer_label_radio+'" for="'+section_question+'.'+answer.id+'">'+img+'</label>';

                                html += '</div>';
                                html += '<div class="'+label_column+' text-left">';

                                if (form_section_width >4){
                                    html+='<span style="margin-left:10px !important;text-align: left !important;display: block;line-height:60px;">'+answer.answer+'</span>';
                                }else{
                                    html+='<span style="margin-left:25px !important;text-align: left !important;display: block;line-height:60px;">'+answer.answer+'</span>';
                                }

                                html += '</div>';
                                html += '</div>';
                                html += '</div>';
                                html += custom_sectionend;
                            }else{
                                if(form_section_width <=2){
                                    var img_style = 'margin-left:6px !important;';
                                    var column = layout_type;
                                    var custom_sectionstart = "<div class='row'>";
                                    var custom_sectionend = "</div>";
                                }else{
                                    var img_style = 'margin-left:9px !important;';
                                    var column = layout_type;
                                    var custom_sectionstart = "";
                                    var custom_sectionend = "";
                                }
                                html += custom_sectionstart;
                                html += '<div class="col-md-'+column_size+'"><div class="form-check form-check-form_section" style="padding-left:1rem !important;">';

                                if(answer.icon!=null){
                                    html += '<div class="row" style="margin-bottom:40px !important;">';
                                }else{
                                    html += '<div class="row">';
                                }
                                html += '<div class="'+column+' text-left">';

                                html += '<input type="radio" disabled class="form-check-input" id="'+section_question+'.'+answer.id+'" value="'+answer.id+'" name="answer_id.'+section_question+'">';
                                html += '<label class="form-check-label abswer_label_radio" for="'+section_question+'.'+answer.id+'">'+answer.answer+'</label>';

                                html += '</div>';
                                html += '<div class="'+column+' text-left">';
                                if(answer.icon!=null){
                                    if(k==0){
                                        var mystyle = img_style;
                                    }else{
                                        var mystyle ='';
                                    }
                                    html += '<img class="'+custom_class+' '+imgStyle+'" style="vertical-align:baseline;'+icon_size+'" src="{{asset("/uploads/icons")}}/'+answer.icon+'"  width="35" height="35"  class="ml-1 mb-1 rounded">';
                                }else{
                                    html += '<img class="'+custom_class+' '+imgStyle+'" img_style" src="{{asset("/uploads/icons/default/BlankEmailPixels-700px.png")}}" style="vertical-align:baseline;width:2%;height:auto;"  width="35" height="35"  class="ml-1 mb-1 rounded">';
                                }
                                html += '</div>';
                                html += '</div>';
                                html += '</div>';
                                html += '</div>';
                                html += custom_sectionend;
                            }
                            break;
                        case 3:
                            html += ' <div class="md-form">';
                            html+=' <input type="text" id="date-picker-form'+answer.id+'" name="start_date.'+section_question+'" class="form-control datepicker">';
                            html += ' <label for="date-picker-form'+answer.id+'">Time</label>';
                            html += '</div>';
                            break;
                        case 4:
                            html += ' <div class="md-form">';
                            html+=' <input placeholder="Selected time" type="text" id="time-picker-form'+answer.id+'"  name="time.'+section_question+'" class="input_starttime form-control timepicker">';
                            html += ' <label for="time-picker-form'+answer.id+'" >Time</label>';
                            html += '</div>';
                            break;
                        case 6:
                            if(form_section_width <=2){
                                var img_style = 'margin-left:6px !important;';
                                var column = 'col-md-4';
                                var custom_sectionstart = "<div class='row'>";
                                var custom_sectionend = "</div>";
                            }else{
                                var img_style = 'margin-left:9px !important;';
                                var column = 'col';
                                var custom_sectionstart = "";
                                var custom_sectionend = "";
                            }
                            if (form_section_width <=8){
                                var img_column = layout_type;
                                var label_column = layout_type;
                            }else{
                                var img_column = 'col-md-4';
                                var label_column = 'col-md-8';
                            }
                            if (layout==2){
                                if(answer.icon!=null){
                                    if (form_section_width <=8){
                                        var img_column = 'col-md-6';
                                        var label_column = 'col-md-6';
                                    }else{
                                        var img_column = 'col-md-4 ';
                                        var label_column = 'col-md-8';
                                    }
                                    var abswer_label_checkbox_verticle='abswer_label_checkbox_verticle';
                                    var img='<img src="{{asset("/uploads/icons")}}/'+answer.icon+'" style="margin-left:10px !important;'+icon_size+'"  width="35" height="35"  class="rounded '+imgStyle+'">';
                                }else{
                                    if (form_section_width <=6){
                                        var img_column = 'col-md-2';
                                        var label_column = 'col-md-10';
                                    }else{
                                        var img_column = 'col-md-1 ';
                                        var label_column = 'col-md-11';
                                    }
                                    var abswer_label_checkbox_verticle='';
                                    var img='';
                                }
                                html += custom_sectionstart;
                                html += '<div class="form-check checkbox_style"><div class="row" style="height:100%;"> <div class="'+img_column+' text-left" >';
                                html += '<input disabled type="checkbox" class="form-check-input" id="'+section_question+'.'+answer.id+'" value="'+answer.id+'" name="checkbox.'+section_question+'" value="'+answer.id+'">';
                                html += '<label class="form-check-label abswer_label_checkbox '+abswer_label_checkbox_verticle+'" style="padding-left: 25px !important;height: 100%;" for="'+section_question+'.'+answer.id+'">'+img+'</label></div><div class="'+label_column+' text-left" style="padding-right: 0px !important;">';
                                if (form_section_width >4){
                                    html+='<span style="margin-left:25px !important;text-align:left;display: block;line-height:60px;">'+answer.answer+'</span>';
                                }else{
                                    html+='<span style="margin-left:25px !important;text-align:left;display: block;line-height:60px;">'+answer.answer+'</span>';
                                }
                                html += '</div>';
                                html += '</div>';
                                html += '</div>';
                                html += custom_sectionend;
                            }else{
                                html += custom_sectionstart;
                                if(answer.icon!=null){
                                    html += '<div class="col-md-'+column_size+'" style="margin-bottom:40px !important;"><div class="form-check checkbox_style "><div class="row"> <div class="'+layout_type+' text-left" >';
                                }else{
                                    html += '<div class="col-md-'+column_size+'"><div class="form-check checkbox_style "><div class="row"> <div class="'+layout_type+' text-left" >';
                                }
                                html += '<input disabled type="checkbox" class="form-check-input" id="'+section_question+'.'+answer.id+'" value="'+answer.id+'" name="checkbox.'+section_question+'" value="'+answer.id+'">';
                                html += '<label class="form-check-label abswer_label_checkbox" style="padding-left: 25px !important;" for="'+section_question+'.'+answer.id+'">'+answer.answer+'</label></div><div class="'+layout_type+' text-left " style="padding-right: 0px !important;">';
                                if(answer.icon!=null){
                                    if(k==0){
                                        var mystyle = 'margin-left:12px !important;';
                                    }else{
                                        var mystyle ='';
                                    }
                                    html += '<img src="{{asset("/uploads/icons")}}/'+answer.icon+'" style="'+icon_size+'"   class="rounded '+imgStyle+'">';
                                }else{
                                    html += '<img src="{{asset("/uploads/icons/default/BlankEmailPixels-700px.png")}}" style="width:2%;height:auto;"   class="'+imgStyle+' rounded">';
                                }

                                html += '</div>';
                                html += '</div>';
                                html += '</div>';
                                html += '</div>';
                                html += custom_sectionend;
                            }
                            break;
                    }
                }
                this.answerCount++;
            }
            this.answerGroupCount++;
            return html;
        }

        function withoutLoopAnswer(data,flag,section_question,question_id,comment)
        {
            var html='';
            let answer = data[1];
            switch(flag) {
                case 3:
                    html += ' <div class="md-form" style="padding-left:9px !important;width: 97% !important;text-align: left;margin-top:-0.5rem !important;">';
                    html+=' <input  type="text" id="input-text"  name="'+section_question+'" style="display: none;" class=" form-control ">';
                    html+='<span id="input_date_result_'+question_id+'"></span>';
                    html += ' <label for="input-text" style="margin-top:-22px !important;display: none;margin-left:16px !important;">Text</label>';
                    //html += ' <label for="input-text'+answer.id+'" style="margin-top:-22px !important;margin-left:16px !important;">Text</label>';
                    html += '</div>';
                    break;
                case 4:
                    html += ' <div class="md-form" style="padding-left:9px !important;width: 97% !important;text-align: left;margin-top:-0.5rem !important;">';
                    html+=' <input  type="text" id="input-text"  name="'+section_question+'" style="display: none;" class=" form-control ">';
                    html+='<span id="input_time_result_'+question_id+'"></span>';
                    html += ' <label for="input-text" style="margin-top:-22px !important;display: none;margin-left:16px !important;">Text</label>';
                    //html += ' <label for="input-text'+answer.id+'" style="margin-top:-22px !important;margin-left:16px !important;">Text</label>';
                    html += '</div>';
                    break;
                case 5:
                    html += ' <div class="md-form" style="padding-left:9px !important;width: 97% !important;text-align: left;margin-top:-0.5rem !important;">';
                    html+=' <input  type="text" id="input-text"  name="'+section_question+'" style="display: none;" class=" form-control ">';
                    html+='<span id="input_text_result_'+question_id+'"></span>';
                    html += ' <label for="input-text" style="margin-top:-22px !important;display: none;margin-left:16px !important;">Text</label>';
                    //html += ' <label for="input-text'+answer.id+'" style="margin-top:-22px !important;margin-left:16px !important;">Text</label>';
                    html += '</div>';
                    break;
                case 8:
                    html += ' <div class="md-form" style="padding-left:9px !important;width: 97% !important;text-align: left;margin-top:-0.5rem !important;">';
                    html+=' <input  type="text" id="input-text"  name="'+section_question+'" style="display: none;" class=" form-control ">';
                    html+='<span id="input_richtext_result_'+question_id+'"></span>';
                    html += ' <label for="input-text" style="margin-top:-22px !important;display: none;margin-left:16px !important;">Text</label>';
                    //html += ' <label for="input-text'+answer.id+'" style="margin-top:-22px !important;margin-left:16px !important;">Text</label>';
                    html += '</div>';
                    break;
                case 10:
                    html +='<div class="md-form" style="">';
                    html+='<div class="text-left">'+comment+'</div>';
                    html +='</div>';
                    break;
                case 12:
                    html += ' <div class="md-form" style="padding-left:18px !important;width: 97% !important;">';
                    html+=' <input  type="text" id="input-text" class=" form-control ">';

                    html += ' <label for="input-text" style="margin-top:-22px !important;margin-left:16px !important;">Clocs</label>';

                    html += '</div>';
                    break;
                default :
                    html += ' <div class="md-form" style="padding-left:18px !important;width: 97% !important;">';
                    html+=' <img src="{{asset("/img/no_si.jpeg")}}" width="200" height="100">';

                    html += '</div>';

            }
            return html;
        }

        function getAllFormsQuestion(){
            $.ajax({
                type: 'GET',
                url: '/document/forms/get/question',
                // data: data,
                success: function(response)
                {

                    var form_section = response.form_section;
                    var html = '';
                    for(var i = 0; i < form_section.length; i++){

                        html += ' <div class="col-md-'+form_section[i].width+'">';
                        html += '<div class="mb-4 ">';
                        html += '<div class="card text-center ">';
                        html += '<div class="card-header" style="display:flex">';
                        html += '<span class="white-color" style="flex: 1; text-align:left; ">'+form_section[i].header+'</span>';
                        html += '<span class=" float-right">';

                        html += ' <a onClick="getform_sectiondata('+form_section[i].id+')"  class=" form_btn_circle">';
                        html += '<i class="white-color fas fa-pencil-alt fa-sm" style="margin-left: auto;"></i>';
                        html += '</a> ';

                        html += '<a  class="text-success open_popup form_btn_circle" id="'+form_section[i].id+'">';
                        html += '<i class="white-color fas fa-plus fa-md" aria-hidden="true"></i>';
                        html += '</a>';
                        html += '</span>';

                        html += '</div>';
                        html += '<div class="card-body"> ';
                        html += '<input type="hidden" name="id" class="form_section" value="'+form_section[i].id+'">';
                        html+=questionSet(form_section[i].questions,0);

                        html += '</div>';
                        html += '<div class="card-footer text-muted text-left">';
                        html += form_section[i].footer;
                        html += '</div>';
                        html += '</div>';
                        html += '</div>';
                        html += '</div>';
                    }
                    $('#div_for_dynamic_forms_display_question').html(html);

                },
                error: function (error) {
                    console.log(error);
                }
            });
        }

        $('.datepicker').pickadate({selectYears:250,});

        var toolbarOptions = [
            [{
                'header': [1, 2, 3, 4, 5, 6, false]
            }],
            ['bold', 'italic', 'underline', 'strike'], // toggled buttons
            ['blockquote', 'code-block'],

            [{
                'header': 1
            }, {
                'header': 2
            }], // custom button values
            [{
                'list': 'ordered'
            }, {
                'list': 'bullet'
            }],
            [{
                'script': 'sub'
            }, {
                'script': 'super'
            }], // superscript/subscript
            [{
                'indent': '-1'
            }, {
                'indent': '+1'
            }], // outdent/indent
            [{
                'direction': 'rtl'
            }], // text direction

            [{
                'size': ['small', false, 'large', 'huge']
            }], // custom dropdown

            [{
                'color': []
            }, {
                'background': []
            }], // dropdown with defaults from theme
            [{
                'font': []
            }],
            [{
                'align': []
            }],
            ['link', 'image'],

            ['clean'] // remove formatting button
        ];

        $(document).ready(function(){
            getAllFormsQuestion();

            if ($("#versionLogDetail_form_view").val()=='1'){
                getAllForms();
            }

            setTimeout(function(){
                getpre_populate_data();
            },3000);
            $("body").on('change','#pre_populate_id',function(){
                if (parseInt($(this).val())){
                    $(".pre_populate_id").val($(this).val());
                }
            });

            $('a[data-toggle="tab"]').on('click', function(e) {

                if($(e.target).attr('href')){
                    var x = document.getElementById("pop_div");
                    if($(e.target).attr('href')=="#profile")
                    {
                        x.style.display = "flex";
                        console.log("show",$(e.target).attr('href'));
                    }
                    else
                    {
                        x.style.display = "none";
                        console.log("hide",$(e.target).attr('href'));
                    }
                }
            });
            $('body').on('focusout', '#formName', function() {

                var formData = $("#addPopulate").serializeObject();


                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url:'/form/addPopulate',
                    data:formData,
                    success: function(response)
                    {
                        console.log(Object.keys(response.pre_populate).length );

                        var html='';

                        html+='<select class="mdb-select" name="pre_populate_id" id="pre_populate_id" >';

                        for (var key in response.pre_populate) {
                            if (response.pre_populate.hasOwnProperty(key))
                                html+='<option  value="'+key+'">'+response.pre_populate[key]+'</option>';
                        }
                        html+='</select> ';
                        $('#pre_populate_listing').empty();
                        console.log(html, "html");
                        document.getElementById('pre_populate_listing').innerHTML=html;
                        $('#pre_populate_id').materialSelect();
                        toastr["success"](response.message);
                    }
                });
                // console.log(data);
            });
        });

        function getpre_populate_data(){
                @if(!empty($pre_populate_id))
            var pre_populate_id = '{{$pre_populate_id}}';
                @else
            var pre_populate_id = '1';
            @endif
                url = '/version/log/getPopulate/'+pre_populate_id;
            // setTimeout(function() {
            //     $.ajax({
            //         type: "GET",
            //         url:url,
            //         success: function(response)
            //         {
            //             console.log(response);
            //             for (var i = response.length - 1; i >= 0; i--) {
            //                 if(response[i].answer_id==null)
            //                     var  id = response[i].form_section_id+'.'+response[i].question_id;
            //                 else
            //                     var id = response[i].form_section_id+'.'+response[i].question_id+'.'+response[i].answer_id;
            //                 console.log(id);
            //                 try
            //                 {
            //                     var type = document.getElementsByName(id)[0].type;
            //                 }
            //                 catch
            //                 {
            //                     try
            //                     {
            //                         var type =  document.getElementById(id).type;
            //                     }
            //                     catch
            //                     {
            //                         continue;
            //                     }
            //                 }
            //
            //                 console.log(type);
            //                 if(type=='radio' || type=='checkbox')
            //                 {
            //                     try
            //                     {
            //                         document.getElementsByName(id)[0].checked=true;
            //                     }
            //                     catch
            //                     {
            //                         document.getElementById(id).checked = true;
            //                     }
            //                 }
            //                 else if( type=='select')
            //                 {
            //                     document.getElementById(id).selected = "true";
            //                 }
            //                 else if( type=='text') {
            //                     id = response[i].form_section_id+'.'+response[i].question_id;
            //                     document.getElementsByName(id)[0].value=response[i].answer_text;
            //                     $('#input_text_result_'+response[i].question_id).html(response[i].answer_text);
            //                     $('#input_date_result_'+response[i].question_id).html(response[i].answer_text);
            //                     $('#input_time_result_'+response[i].question_id).html(response[i].answer_text);
            //                     $('#input_richtext_result_'+response[i].question_id).html(response[i].answer_text);
            //                 }else if(type=='textarea')
            //                 {
            //                     document.getElementsByName(id)[0].value=response[i].answer_text;
            //                     var id = id.split('.');
            //                     var nid = '.'+id[0]+'-'+id[1];
            //                     console.log(nid);
            //                     $(nid).summernote('code',response[i].answer_text);
            //                 }
            //                 else
            //                 {
            //                     document.getElementById(id).selected = "true";
            //                 }
            //                 var dropdown="dropdown"+id;
            //                 //alert($('select[name="' + dropdown + '"]').children("option:selected").val());
            //                 $('#form_dropdown_'+response[i].question_id).html($("select[name='"+dropdown+"']").children("option:selected").val());
            //             }
            //         }
            //     });
            // },1000);

            setTimeout(function() {
                $.ajax({
                    type: "GET",
                    url:url,

                    success: function(response)
                    {
                        console.log(response);

                        for (var i = response.length - 1; i >= 0; i--) {


                            if(response[i].answer_id==null)

                                var  id = response[i].form_section_id+'.'+response[i].question_id;
                            else
                                var id = response[i].form_section_id+'.'+response[i].question_id+'.'+response[i].answer_id;

                            console.log(id);
                            try
                            {

                                var type = document.getElementsByName(id)[0].type;
                            }
                            catch
                            {
                                try
                                {
                                    var type =  document.getElementById(id).type;
                                }
                                catch
                                {
                                    continue;
                                }
                            }

                            console.log(type);
                            if(type=='radio' || type=='checkbox')
                            {
                                try
                                {
                                    document.getElementsByName(id)[0].checked=true;
                                }
                                catch
                                {
                                    document.getElementById(id).checked = true;
                                }
                            }
                            else if( type=='select')
                            {
                                document.getElementById(id).selected = "true";
                            }
                            else if( type=='text') {
                                id = response[i].form_section_id+'.'+response[i].question_id;
                                document.getElementsByName(id)[0].value=response[i].answer_text;
                                $('#input_text_result_'+response[i].question_id).html(response[i].answer_text);
                                $('#input_date_result_'+response[i].question_id).html(response[i].answer_text);
                                $('#input_time_result_'+response[i].question_id).html(response[i].answer_text);
                                $('#input_richtext_result_'+response[i].question_id).html(response[i].answer_text);
                            }else if(type=='textarea')
                            {
                            }
                            else
                            {
                                document.getElementById(id).selected = "true";
                            }
                        }
                    }

                });
            },800);
        }
    </script>
    <script src="{{asset('/custom/js/answers.js')}}"></script>
    <script src="{{asset('/custom/js/populate.js')}}"></script>
@endsection
