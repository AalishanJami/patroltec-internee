@extends('backend.layouts.doc')
@section('content')
    @php
        $data=localization();
    @endphp
    <style type="text/css">
        .nav-select{
            margin-left: 26rem !important;
            width: 20%;
            margin-top: 2px;
            margin-bottom: -15px;
        }
        .white-color
        {
            color:#fff;
        }
        #pdf-viewer,#pdf-viewer-static
        {
            height: 700px;
        }

        .form_alignment{
            width: 96%;
            margin: auto !important;
        }

        .radio_checkbox_label{
            width: 100px !important;
            text-align: left !important;
            /*overflow: hidden !important;*/
            /*text-overflow: ellipsis !important;*/
        }
    </style>
    @include('backend.layouts.doc_sidebar')
    <div class="custom_clear padding-left-custom remove-padding">
        {{ Breadcrumbs::render('Version/log/edit') }}
        <div class="custom_clear locations-form container-fluid form_body">
            <div class="custom_clear card">
                <ul  class="custom_clear nav nav-tabs" id="myTab" role="tablist">
                    <li class="custom_clear nav-item">
                        <a class="custom_clear nav-link active" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile"
                           aria-selected="false"> Dynamic</a>
                    </li>
                    @if($id_static)
                        <li class="custom_clear nav-item">
                            <a class="custom_clear nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact"
                               aria-selected="false">Static</a>
                        </li>
                    @endif
                    {!! Form::select('pre_populate_id',$pre_populate,null, ['class' =>'mdb-select nav-select','placeholder'=>'Select Form','id'=>'pre_populate_id']) !!}
                </ul>
                <div class="custom_clear tab-content" id="myTabContent">
                    <div class="custom_clear tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <h5 class="custom_clear card-header  t py-4 white-color">
                            <strong>Dynamic Forms</strong>
                            <span class="custom_clear  float-right mb-3 mr-2">
                </span>
                        </h5>
                        <div class="row">
                            @foreach ($dynamic_data as $key => $value)
                                <div id="{{$value->form_Section->order}}-{{$value->form_Section->id}}" class="custom_clear  sortable-card col-md-{{$value->form_Section->width}}">
                                    <div class="custom_clear mb-4 custom_dynamic_height">
                                        <br>
                                        <div class="custom_clear card text-center ">
                                            <div class="custom_clear card-header text-muted text-left md-5">
                                                <span class="custom_clear white-color" style="flex: 1; text-align:left;padding-bottom: 40px">{{$value->form_Section->header}}</span>
                                            </div>
                                            <div class="custom_clear card-body mt-2">
                                                @foreach($value->form_Section->questions as $question)
                                                    <div class="custom_clear card card-cascade wider reverse" style="padding-top: 10px;padding-bottom: 10px;">
                                                        <p class="custom_clear card-title text-left" style="padding-left: 20px;">
                                                            <strong>{{($question->question)}}</strong>
                                                        </p>
                                                        <div class="custom_clear row text-center">
                                                            @switch ($question->answer_type->name)
                                                                @case ('dropdown')
                                                                <div class="custom_clear col-md-12  ">
                                                                    <select searchable="Search here.." style="width: 100%;"  class="custom_clear mdb-select form_alignment" name="{{$value->form_Section->id.'.'.$question->id}}">
                                                                        @if($question->answer_group)
                                                                            @foreach($question->answer_group->answer as $answer)
                                                                                <option id="{{$value->form_Section->id.'.'.$question->id.'.'.$answer->id}}" value="{{$answer->id}}">{{$answer->answer}}</option>
                                                                            @endforeach
                                                                        @endif
                                                                    </select>
                                                                </div>
                                                                @break
                                                                @case ('radio')
                                                                <div class="custom_clear col-md-12 ">
                                                                    @if($question->answer_group)
                                                                        @foreach($question->answer_group->answer as $answer)
                                                                            <div class="custom_clear form-check float-left mt-3">
                                                                                <input type="radio" class="custom_clear form-check-input" id="answer_group{{$answer->id}}" name="{{$value->form_Section->id.'.'.$question->id.'.'.$answer->id}}">
                                                                                <label class="custom_clear form-check-label radio_checkbox_label" for="answer_group{{$answer->id}}">{{$answer->answer}}</label>
                                                                                <img src="{{url('/uploads/icons/'.$answer->icon)}}" style="width: 35px;height: 35px;margin-left: 0px;" alt="">
                                                                            </div>
                                                                        @endforeach
                                                                    @endif
                                                                </div>
                                                                @break
                                                                @case ('checkbox')
                                                                <div class="custom_clear col-md-12 ">
                                                                    @if($question->answer_group)
                                                                        @foreach($question->answer_group->answer as $answer)
                                                                            <div class="custom_clear form-check float-left mt-3">
                                                                                <input type="checkbox" class="custom_clear form-check-input" id="answer_group_checkbox{{$answer->id}}" name="{{$value->form_Section->id.'.'.$question->id.'.'.$answer->id}}">
                                                                                <label class="custom_clear form-check-label radio_checkbox_label" for="answer_group_checkbox{{$answer->id}}">{{$answer->answer}}</label>
                                                                                <img src="{{url('/uploads/icons/'.$answer->icon)}}" style="width: 35px;height: 35px;margin-left: 0px;" alt="">
                                                                            </div>
                                                                        @endforeach
                                                                    @endif
                                                                </div>
                                                                @break
                                                                @case ('multi')
                                                                <div class="custom_clear col-md-12">
                                                                    <select searchable="Search here.." style="width: 100%;"  class="custom_clear mdb-select form_alignment" multiple  name="{{$value->form_Section->id.'.'.$question->id.'.'.$answer->id}}">
                                                                        @if($question->answer_group)
                                                                            @foreach($question->answer_group->answer as $answer)
                                                                                <option id="{{$value->form_Section->id.'.'.$question->id.'.'.$answer->id}}" value="{{$answer->id}}">{{$answer->answer}}</option>
                                                                            @endforeach
                                                                        @endif
                                                                    </select>
                                                                </div>
                                                                @break
                                                                @case ('date')
                                                                <div class="custom_clear col-md-12 ">
                                                                    @if($question->answer_group)
                                                                        <div class="custom_clear md-form mt-5">
                                                                            <input type="text" placeholder="Date" id="date-picker-form" name="{{$value->form_Section->id.'.'.$question->id}}" class="custom_clear form_alignment form-control ">
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                                @break
                                                                @case ('time')
                                                                <div class="custom_clear col-md-12 ">
                                                                    @if($question->answer_group)
                                                                        <div class="custom_clear md-form mt-5">
                                                                            <input type="text" placeholder="Time" id="date-picker-form" name="{{$value->form_Section->id.'.'.$question->id}}" class="custom_clear form-control form_alignment">
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                                @break
                                                                @case ('datetime')
                                                                <div class="custom_clear col-md-12 ">
                                                                    @if($question->answer_group)


                                                                        <div class="custom_clear md-form mt-5">
                                                                            <input type="text" placeholder="Date" id="date-picker-form" name="{{$value->form_Section->id.'.'.$question->id}}" class="custom_clear form-control form_alignment">
                                                                        </div>

                                                                    @endif
                                                                </div>
                                                                @break
                                                                @case ('text')
                                                                <div class="custom_clear col-md-12 ">
                                                                    @if($question->answer_group)

                                                                        <div class="custom_clear md-form mt-5">
                                                                            <input  type="text" placeholder="Text" id="text-form"  name="{{$value->form_Section->id.'.'.$question->id}}" class="custom_clear  form-control form_alignment">
                                                                        </div>



                                                                    @endif
                                                                </div>
                                                                @break
                                                            @endswitch
                                                        </div>
                                                    </div>
                                                @endforeach()

                                            </div>
                                            <div class="custom_clear card-footer text-muted text-left">
                                                {{$value->form_Section->footer}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach()
                        </div>
                    </div>
                    @if($id_static)
                        <div class="custom_clear tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                            <h5 class="custom_clear card-header  t py-4 white-color">
                                <strong>Static Forms View</strong>
                                <span class="custom_clear  float-right mb-3 mr-2">
                      <a href="{{url('static_form/download/pdf/'.$id_static)}}" class="custom_clear btn btn-success btn-sm ">
                              Download As Pdf
                      </a>
                      <a  href="{{url('static_form/download/orginal/'.$id_static)}}" class="custom_clear btn btn-secondary btn-sm ">
                              Download As Orginal
                      </a>
                </span>
                            </h5>
                            <div id="pdf-viewer-static"></div>
                        </div>
                    @endif

                </div>
            </div>
        </div>
        <script src="{{asset('js/pdfview.js') }}"></script>
        <script type="text/javascript">
            var options = {
                pdfOpenParams: {toolbar: '0'}
            };
            PDFObject.embed("{{ url('static_form/view-pdf', ['id' => $id_static]) }}", "#pdf-viewer-static",options);
            ('.input_starttime').pickatime({});
            $('.datepicker').pickadate({selectYears:250,});
            $('.mdb-select_forms').materialSelect();
        </script>
        <script src="{{asset('/custom/js/version_log.js')}}"></script>

@endsection
