<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    @if(!empty($reportname))
        <title style="margin-left:15%;display: block;font-size: 12px;">{{$reportname}}</title>
    @endif
    <style>
        .pdf_table_layout{
            vertical-align: text-top;width: 9%;
        }
    </style>
</head>
<body style="width:100%;">
<table style="width:100%;">
    <tr style="width:100%;">
        <td style="width: 25%;">
            <img src="https://patroltec.net/backend/web/uploads/_180705101111.png" height="100" width="100"/><br>
            <span style="margin-left:15%;font-weight:bold;display: block;font-size: 12px;">{{$formname}}</span>
        </td>
        <td style="width: 75%;text-align: right;">
            <span class="user text-bottom" style="margin-top:11%;text-align:right;float:right;display: block;font-size: 12px;">
                {{Auth()->user()->name}} :  {{$date}}
            </span>
        </td>
    </tr>
</table>
<hr>
<table style="width:100%;">
    @if(count($form_section)>0)
        @foreach($form_section as $key=>$value)
        <tr style="width:100%;">
            <th style="width:100%;"><strong style="font-size:24px;">{{$value->header}}</strong></th>
        </tr>
        @if(count($value->questions)>0)
            @foreach($value->questions as $key=>$question)
                <tr style="width:100%;">
                    <td style="width:100%;">
                        <table>
                            <tr>
                                <td style="width:100%;">
                                    <strong style="font-size:16px;">{{$question->question}} :</strong>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:100%;">
                                    <table>
                                        <tr>
                                            <td style="width:100%;">
                                                @php $populate=\App\PopulateResponse::where('pre_populate_id',$pre_populate_id)->where('question_id',$question->id)->first(); @endphp
                                                @switch ($question->answer_type->name)
                                                    @case ('dropdown')
                                                    @if($question->answer_group)
                                                        @foreach($question->answer_group->answer as $answer)
                                                            <table>
                                                                @if(!empty($populate))
                                                                    @if($answer->id==$populate->answer_id)
                                                                        <tr>
                                                                            <td>{{$answer->answer}}</td>
                                                                        </tr>
                                                                    @endif
                                                                @endif
                                                            </table>
                                                        @endforeach
                                                    @endif
                                                    @break
                                                    @case ('radio')
                                                    @if($question->answer_group)
                                                        @foreach($question->answer_group->answer as $answer)
                                                            <table>
                                                                <tr style="width:100%;padding-top: 20px;padding-bottom: 20px;">
                                                                    <td style="width:10%;padding-top: 20px;padding-bottom: 20px;">
                                                                        @if(!empty($populate))
                                                                            @if($answer->id==$populate->answer_id)
                                                                                <img src="{{public_path('/uploads/icons/selected_radio_button.png')}}" style="width:20px;height:auto;"   alt="No Icon">
                                                                            @else
                                                                                <img src="{{public_path('/uploads/icons/un_selected_radio_button.png')}}" style="width:20px;height:auto;" alt="No Icon">
                                                                            @endif
                                                                        @endif
                                                                    </td>
                                                                    <td style="padding-left:10px;width:30%;">
                                                                        @if(isset($answer->icon))
                                                                            <img src="{{public_path('/uploads/icons/'.$answer->icon)}}"  width="50" height="50" alt="No Icon">
                                                                        @endif
                                                                    </td>
                                                                    <td style="padding-left:10px;width:60%;">{{$answer->answer}}</td>
                                                                </tr>
                                                            </table>
                                                        @endforeach
                                                    @endif
                                                    @break
                                                    @case ('checkbox')
                                                    @if($question->answer_group)
                                                        @foreach($question->answer_group->answer as $answer)
                                                            <table>
                                                                <tr style="width:100%;">
                                                                    <td style="width:10%;padding-top: 20px;padding-bottom: 20px;">
                                                                        @if(!empty($populate))
                                                                            @if($answer->id==$populate->answer_id)
                                                                                <img src="{{public_path('/uploads/icons/selected_checkboxes_button.png')}}" style="width:20px;height:auto;"   alt="No Icon">
                                                                            @else
                                                                                <img src="{{public_path('/uploads/icons/un_selected_checkbox_button.png')}}" style="width:20px;height:auto;" alt="No Icon">
                                                                            @endif
                                                                        @endif
                                                                    </td>
                                                                    <td style="padding-left:10px;width:30%;">
                                                                        @if(isset($answer->icon))
                                                                            <img src="{{public_path('/uploads/icons/'.$answer->icon)}}"  width="50" height="50" alt="No Icon">
                                                                        @endif
                                                                    </td>
                                                                    <td style="padding-left:10px;width:60%;">{{$answer->answer}}</td>
                                                                </tr>
                                                            </table>
                                                        @endforeach
                                                    @endif
                                                    @break
                                                    @case ('multi')
                                                    @if($question->answer_group)
                                                        @foreach($question->answer_group->answer as $answer)
                                                            <table>
                                                                <tr>
                                                                    <td style="width:10%;">
                                                                        @if(!empty($populate))
                                                                            @if($answer->id==$populate->answer_id)
                                                                                <img src="{{public_path('/uploads/icons/240px-Green_check.png')}}" style="width:20px;height:auto;"   alt="No Icon">
                                                                            @else
                                                                                <img src="{{public_path('/uploads/icons/240px_uncheck-Green_check.png')}}" style="width:20px;height:auto;" alt="No Icon">
                                                                            @endif
                                                                        @endif
                                                                    </td>
                                                                    <td style="padding-left:10px;width:60%;">{{$answer->answer}}</td>
                                                                </tr>
                                                            </table>
                                                        @endforeach
                                                    @endif
                                                    @break
                                                @endswitch
                                                @if(!empty($populate))
                                                        {{$populate->answer_text}}
                                                @endif
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            @endforeach
        @endif
            <tr style="width:100%;">
                <td><hr></td>
            </tr>
        @endforeach
    @endif
</table>
<table style="width:100%;">
    <tr style="width:100%;">
        <td style="width: 100%;">
            Company : Patroltec
        </td>
    </tr>
</table>
</body>
</html>
