<table  class="company_table_static table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
    <tr>
        @if(Auth()->user()->hasPermissionTo('documentVersionlogList_formname_excel_export') || Auth::user()->all_companies == 1 )
            <th> Form Name</th>
        @else
            <th></th>
        @endif
        @if(Auth()->user()->hasPermissionTo('documentVersionlogList_username_excel_export') || Auth::user()->all_companies == 1 )
            <th> Name</th>
        @else
            <th></th>
        @endif
        @if(Auth()->user()->hasPermissionTo('documentVersionlogList_date_excel_export') || Auth::user()->all_companies == 1 )
            <th>  Date Added</th>
        @else
            <th></th>
        @endif
    </tr>
    </thead>
    <tbody>
    @foreach($data as $key=>$value)
        <tr>
            @if(Auth()->user()->hasPermissionTo('documentVersionlogList_formname_excel_export') || Auth::user()->all_companies == 1 )
                <td  class="pt-3-half" >{{$value->form->name}}</td>
            @else
                <td></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('documentVersionlogList_username_excel_export') || Auth::user()->all_companies == 1 )
                <td  class="pt-3-half" >{{$value->user->name}}</td>
            @else
                <td></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('documentVersionlogList_date_excel_export') || Auth::user()->all_companies == 1 )
                <td class="pt-3-half" >{{$value->date_time}}</td>
            @else
                <td></td>
            @endif
        </tr>
    @endforeach()
    </tbody>
</table>
