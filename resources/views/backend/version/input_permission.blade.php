<!---form permission---->
@if(Auth()->user()->all_companies == 1)
    <input type="hidden" id="versionLogDetail_form_view" value="1">
    <input type="hidden" id="versionLogDetail_form_name" value="1">
    <input type="hidden" id="versionLogDetail_form_header" value="1">
    <input type="hidden" id="versionLogDetail_form_footer" value="1">
@else
    <input type="hidden" id="versionLogDetail_form_view" value="{{Auth()->user()->hasPermissionTo('versionLogDetail_form_view')}}">
    <input type="hidden" id="versionLogDetail_form_name" value="{{Auth()->user()->hasPermissionTo('versionLogDetail_form_name')}}">
    <input type="hidden" id="versionLogDetail_form_header" value="{{Auth()->user()->hasPermissionTo('versionLogDetail_form_header')}}">
    <input type="hidden" id="versionLogDetail_form_footer" value="{{Auth()->user()->hasPermissionTo('versionLogDetail_form_footer')}}">
@endif

<!---form permission---->
@if(Auth()->user()->all_companies == 1)
    <input type="hidden" id="versionLogDetail_question_view" value="1">
    <input type="hidden" id="versionLogDetail_question" value="1">
    <input type="hidden" id="versionLogDetail_answertype" value="1">
    <input type="hidden" id="versionLogDetail_answer" value="1">
@else
    <input type="hidden" id="versionLogDetail_question_view" value="{{Auth()->user()->hasPermissionTo('versionLogDetail_question_view')}}">
    <input type="hidden" id="versionLogDetail_question" value="{{Auth()->user()->hasPermissionTo('versionLogDetail_question')}}">
    <input type="hidden" id="versionLogDetail_answertype" value="{{Auth()->user()->hasPermissionTo('versionLogDetail_answertype')}}">
    <input type="hidden" id="versionLogDetail_answer" value="{{Auth()->user()->hasPermissionTo('versionLogDetail_answer')}}">
@endif
