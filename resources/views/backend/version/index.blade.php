@extends('backend.layouts.doc')
@section('title', 'Version Log')
@section('content')
    @php
        $data=localization();
    @endphp
    <link rel="stylesheet" href="{{ asset('custom/css/custom_style.css') }}">
    @include('backend.layouts.doc_sidebar')
    <div class="padding-left-custom remove-padding">
        {!! breadcrumInner('version_log',Session::get('form_id'),'forms','/document/detail/'.Session::get('form_id'),'documents','/form_group') !!}
        <div class="locations-form container-fluid form_body">
            <div class="card">
                <div class="card-body">
                    @if(Auth()->user()->hasPermissionTo('selected_delete_documentVersionlogList') || Auth::user()->all_companies == 1 )
                        <button class="btn btn-danger btn-sm" id="deleteSelectedVersionlog">
                            <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                                {!! checkKey('selected_delete',$data) !!}
                            </span>
                        </button>
                    @endif
                    <input type="hidden" id="flagVersiolog" value="1">
                    @if(Auth()->user()->hasPermissionTo('selected_restore_delete_documentVersionlogList') || Auth::user()->all_companies == 1 )
                        <button class="btn btn-primary btn-sm" id="restore_versionlog">
                            <span class="assign_class label{{getKeyid('restore',$data)}}" data-id="{{getKeyid('restore',$data) }}" data-value="{{checkKey('restore',$data) }}" >
                                {!! checkKey('restore',$data) !!}
                            </span>
                        </button>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('selected_active_documentVersionlogList') || Auth::user()->all_companies == 1 )
                        <button class="btn btn-success btn-sm" id="selectedactivebuttonversionlog" style="display:none;">
                            <span class="assign_class label{{getKeyid('selected_active',$data)}}" data-id="{{getKeyid('selected_active',$data) }}" data-value="{{checkKey('selected_active',$data) }}" >
                                {!! checkKey('selected_active',$data) !!}
                            </span>
                        </button>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('show_active_documentVersionlogList') || Auth::user()->all_companies == 1 )
                        <button id="show_active_versionlog"  class="btn btn-primary btn-sm" style="display:none;">
                            <span class="assign_class label{{getKeyid('show_active',$data)}}" data-id="{{getKeyid('show_active',$data) }}" data-value="{{checkKey('show_active',$data) }}" >
                                {!! checkKey('show_active',$data) !!}
                            </span>
                        </button>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('csv_documentVersionlogList') || Auth::user()->all_companies == 1 )
                        <form class="form-style" id="export_excel_versionlog" method="POST" action="{{ url('version/log/excel/export') }}">
                            {!! csrf_field() !!}
                            <input type="hidden" class="export_versionlog" name="excel_array" value="1">
                            <button  type="submit" class="form_submit_check btn btn-warning btn-sm">
                                <span class="assign_class label{{getKeyid('excel_export',$data)}}" data-id="{{getKeyid('excel_export',$data) }}" data-value="{{checkKey('excel_export',$data) }}" >
                                    {!! checkKey('excel_export',$data) !!}
                                </span>
                            </button>
                        </form>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('word_documentVersionlogList') || Auth::user()->all_companies == 1 )
                        <form class="form-style " id="export_word_versionlog" method="POST"  action="{{ url('version/log/word/export') }}">
                            <input type="hidden"  name="word_array" class="export_versionlog" value="1">
                            {!! csrf_field() !!}
                            <button  type="submit" class="form_submit_check btn btn-success btn-sm">
                               <span class="assign_class label{{getKeyid('word_export',$data)}}" data-id="{{getKeyid('word_export',$data) }}" data-value="{{checkKey('word_export',$data) }}" >
                                    {!! checkKey('word_export',$data) !!}
                                </span>
                            </button>
                        </form>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('pdf_documentVersionlogList') || Auth::user()->all_companies == 1 )
                        <form  class="form-style" {{pdf_view('version_logs')}} id="export_pdf_versionlog"  method="POST"  action="{{ url('version/log/pdf/export') }}">
                            <input type="hidden"  name="pdf_array" class="export_versionlog" value="1">
                            {!! csrf_field() !!}
                            <button  type="submit" class="form_submit_check btn btn-secondary btn-sm">
                               <span class="assign_class label{{getKeyid('pdf_export',$data)}}" data-id="{{getKeyid('pdf_export',$data) }}" data-value="{{checkKey('pdf_export',$data) }}" >
                                    {!! checkKey('pdf_export',$data) !!}
                                </span>
                            </button>
                        </form>
                    @endif

                    <div id="table" class="table-editable">
                        <table id="version_log_table"  class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th class="no-sort all_checkboxes_style">
                                    <div class="form-check"><input type="checkbox" class="form-check-input versionlog" id="versionlog_checkbox_all"><label class="form-check-label" for="versionlog_checkbox_all"></label></div>
                                </th>
                                <th>
                                    <span class="assign_class label{{getKeyid('version_type',$data)}}" data-id="{{getKeyid('version_type',$data) }}" data-value="{{checkKey('version_type',$data) }}" >
                                        {!! checkKey('version_type',$data) !!}
                                    </span>
                                </th>

                                <th>
                                    <span class="assign_class label{{getKeyid('form_name',$data)}}" data-id="{{getKeyid('form_name',$data) }}" data-value="{{checkKey('form_name',$data) }}" >
                                        {!! checkKey('form_name',$data) !!}
                                    </span>
                                </th>
                                <th>
                                    <span class="assign_class label{{getKeyid('name',$data)}}" data-id="{{getKeyid('name',$data) }}" data-value="{{checkKey('name',$data) }}" >
                                        {!! checkKey('name',$data) !!}
                                    </span>
                                </th>
                                <th>
                                    <span class="assign_class label{{getKeyid('date',$data)}}" data-id="{{getKeyid('date',$data) }}" data-value="{{checkKey('date',$data) }}" >
                                        {!! checkKey('date',$data) !!}
                                    </span>
                                </th>
                                <th class="no-sort all_action_btn" id="action"></th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
                @if(Auth()->user()->all_companies == 1)
                    <input type="hidden" id="documentVersionlogList_checkbox" value="1">
                    <input type="hidden" id="documentVersionlogList_versiontype" value="1">
                    <input type="hidden" id="documentVersionlogList_formname" value="1">
                    <input type="hidden" id="documentVersionlogList_username" value="1">
                    <input type="hidden" id="documentVersionlogList_date" value="1">
                @else
                    <input type="hidden" id="documentVersionlogList_checkbox" value="{{Auth()->user()->hasPermissionTo('documentVersionlogList_checkbox')}}">
                    <input type="hidden" id="documentVersionlogList_versiontype" value="{{Auth()->user()->hasPermissionTo('documentVersionlogList_versiontype')}}">
                    <input type="hidden" id="documentVersionlogList_formname" value="{{Auth()->user()->hasPermissionTo('documentVersionlogList_formname')}}">
                    <input type="hidden" id="documentVersionlogList_username" value="{{Auth()->user()->hasPermissionTo('documentVersionlogList_username')}}">
                    <input type="hidden" id="documentVersionlogList_date" value="{{Auth()->user()->hasPermissionTo('documentVersionlogList_date')}}">
                @endif
            </div>
        </div>
    </div>
    @include('backend.label.input_label')
    <script type="text/javascript" src="{{ asset('custom/js/versionlog.js') }}" ></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $(function () {
                var url='/version/log/getAll';
                versionLogData(url);
                // (function ($) {
                //     var active ='<span class="assign_class label'+$('#breadcrumb_versionlog').attr('data-id')+'" data-id="'+$('#breadcrumb_versionlog').attr('data-id')+'" data-value="'+$('#breadcrumb_versionlog').val()+'" >'+$('#breadcrumb_versionlog').val()+'</span>';
                //     $('.breadcrumb-item.active').html(active);
                //     var parent ='<span class="assign_class label'+$('#dashboard_label').attr('data-id')+'" data-id="'+$('#dashboard_label').attr('data-id')+'" data-value="'+$('#dashboard_label').val()+'" >'+$('#dashboard_label').val()+'</span>';
                //     $('.breadcrumb-item a').html(parent);
                // }(jQuery));
            });
        });

        function versionLogData(url)
        {
            var table = $('#version_log_table').dataTable({
                processing: true,
                bInfo:false,
                language: {
                    'lengthMenu': '<span class="assign_class label'+$('#show_label').attr('data-id')+'" data-id="'+$('#show_label').attr('data-id')+'" data-value="'+$('#show_label').val()+'" ></span>  _MENU_ <span class="assign_class label'+$('#entries_label').attr('data-id')+'" data-id="'+$('#entries_label').attr('data-id')+'" data-value="'+$('#entries_label').val()+'" ></span>',
                    'search':'<span class="assign_class label'+$('#search_id').attr('data-id')+'" data-id="'+$('#search_id').attr('data-id')+'" data-value="'+$('#search_id').val()+'" >'+$('#search_id').val()+'</span>',
                    'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                    'info': '<span class="assign_class label'+$('#showing_page_label').attr('data-id')+'" data-id="'+$('#showing_page_label').attr('data-id')+'" data-value="'+$('#showing_page_label').val()+'" >'+$('#showing_page_label').val()+'</span> _PAGE_ <span class="assign_class label'+$('#of_label').attr('data-id')+'" data-id="'+$('#of_label').attr('data-id')+'" data-value="'+$('#of_label').val()+'" >'+$('#of_label').val()+'</span> _PAGES_',
                    'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                    'paginate': {
                        'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                        'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                    },
                    'infoFiltered': " "
                },
                "ajax": {
                    "url": url,
                    "type": 'get'
                },
                "createdRow": function( row, data, dataIndex,columns) {
                    $(row).attr('id', 'versionlog_tr'+data['id']);
                    var checkbox='<div class="form-check"><input type="checkbox" class="form-check-input selectedRowVersionlog" id="versionlog_check'+data.id+'"><label class="form-check-label" for="versionlog_check'+data.id+'"></label></div>';
                    $(columns[0]).html(checkbox);
                    $(row).attr('data-id', data['id']);
                    var temp=data['id'];
                },
                columns: [
                    {data: 'checkbox', name: 'checkbox',visible:$('#documentVersionlogList_checkbox').val()},
                    {data: 'id', name: 'id',visible:$('#documentVersionlogList_versiontype').val()},
                    {data: 'form_name', name: 'form_name',visible:$('#documentVersionlogList_formname').val()},
                    {data: 'user_name', name: 'user_name',visible:$('#documentVersionlogList_username').val()},
                    {data: 'date_time', name: 'date_time',visible:$('#documentVersionlogList_date').val()},
                    {data: 'actions', name: 'actions'},
                ],

            });
            if ($("#versionlog_checkbox_all").prop('checked',true)){
                $("#versionlog_checkbox_all").prop('checked',false)
            }
            return table;
        }
    </script>
@endsection

