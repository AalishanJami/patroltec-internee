@include('backend.layouts.pdf_start')
    <thead>
    <tr>
        <th style="vertical-align: text-top;" class="pdf_table_layout"> Form Name</th>
        <th style="vertical-align: text-top;" class="pdf_table_layout"> Name</th>
        <th style="vertical-align: text-top;" class="pdf_table_layout">  Date Added</th>
    </tr>
    </thead>
    <tbody>
    @foreach($data as $key=>$value)
        <tr>
            <td class="pdf_table_layout">
                <p class="col_text_style">{{$value->form->name}}</p>
            </td>
            <td class="pdf_table_layout">
                <p class="col_text_style">{{$value->user->name}}</p>
            </td>
            <td class="pdf_table_layout">
                <p class="col_text_style">{{$value->date_time}}</p>
            </td>
        </tr>
    @endforeach()
    </tbody>
@include('backend.layouts.pdf_end')
