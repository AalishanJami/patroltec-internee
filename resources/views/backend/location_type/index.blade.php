@extends('backend.layouts.user')
@section('content')
    @php
       $data=localization();
    @endphp
    <div class="row">
        @include('backend.layouts.user_sidebar')
        <div class="padding-left-custom remove-padding">
            {{ Breadcrumbs::render('location_group') }}
              <div class="card">

            <div class="card-body">
                <div id="table" class="table-editable">
            <span class="table-add float-right mb-3 mr-2">
                <a class="text-success"  data-toggle="modal" data-target="#modellocationCreate">
                    <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
                </a>
            </span>
            <button class="btn btn-danger btn-sm">
                Delete Selected Location Group
            </button>
            <button class="btn btn-primary btn-sm">
              Restore Deleted Location Group
            </button>

            <button  style="display: none;">
                Show Active Location Group
            </button>
            <button class="btn btn-warning btn-sm">
               Excel Export
            </button>
            <button class="btn btn-success btn-sm">
                Word Export
            </button>
            <table id="company_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th> ID
                    </th>
                    <th> Name
                    </th>
                    <th id="action">
                    </th>
                </tr>
                </thead>
                    <tr>
                        <td  class="pt-3-half" contenteditable="true">1</td>
                        <td class="pt-3-half" contenteditable="true">New York</td>
                        <td class="pt-3-half" contenteditable="true">
                            <a  data-toggle="modal" data-target="#modelEditLocationGroup" class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i></a>
                            <button type="button"class="btn btn-danger btn-sm my-0" onclick="deletelocation_groupdata()"><i class="fas fa-trash"></i></button>

                        </td>
                    </tr>
                <tbody>
                </tbody>
            </table>
                            </div>
            </div>
        </div>

            @component('backend.location_type.model.create')
            @endcomponent

             {{--    Edit Modal--}}
            @component('backend.location_type.model.edit')
            @endcomponent
            {{--    END Edit Modal--}}
        </div>
    </div>


@endsection
<script type="text/javascript">
function deletelocation_groupdata()
{

    swal({
        title:'Are you sure?',
        text: "Delete Record.",
        type: "error",
        showCloseButton: true,
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, Delete!",
        cancelButtonText: "Cancel!",
        closeOnConfirm: false,
        customClass: "confirm_class",
        closeOnCancel: false,
    },
    function(isConfirm){
        swal.close();
 });


}

</script>
