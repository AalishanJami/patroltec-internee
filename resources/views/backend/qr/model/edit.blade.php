@php
        $data=localization();
@endphp
<div class="modal fade" id="modalEditForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">QR</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
             <form >
               <div class="modal-body">
                 <div class=" form-row">
                    <div class="col">
                       <div class="md-form">
                          <i class="fas fa-user prefix grey-text"></i>
                          <input type=text id="form7" class="form-control" rows="3">
                          <label for="form7">Name</label>
                       </div>
                    </div>
                  </div>
                  <div class="row">

                    <div class="col">
                       <div class="md-form">
                          <i class="fas fa-sitemap prefix grey-text"></i> 
                          <textarea id="form7" class="md-textarea form-control" rows="3"></textarea>
                          <label for="form7">Directions</label>
                       </div>
                    </div>
                    <div class="col">
                       <div class="md-form">
                          <i class="fas fa-info prefix grey-text"></i>
                          <textarea id="form7" class="md-textarea form-control" rows="3"></textarea>
                          <label for="form7">Information</label>
                       </div>
                    </div>
                  </div>
                  <div class=" form-row">
                    <div class="col">
                      <div class="md-form mb-5">
                         <div id="document-full-create" class="ql-scroll-y" style="height: 300px;">
                         </div>
                      </div>
                    </div>
                    <!--  <div class="col"> -->
                 </div>
                 <div class="modal-footer d-flex justify-content-center">
                    <button class="btn btn-primary btn-sm" type="submit"><span class="assign_class label{{getKeyid('save',$data)}}" data-id="{{getKeyid('save',$data) }}" data-value="{{checkKey('save',$data) }}" >{!! checkKey('save',$data) !!} </span></button>
                 </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="{{asset('editor/sprite.svg.js')}}"></script>

<script type="text/javascript">
   var toolbarOptions = [
      ['bold', 'italic', 'underline'], // toggled buttons
      ['blockquote'],

     // custom button values
      [{
        'list': 'ordered'
      }, {
        'list': 'bullet'
      }],
      [{
        'indent': '-1'
      }, {
        'indent': '+1'
      }], // outdent/indent
     
      
       // dropdown with defaults from theme
     
      [{
        'align': []
      }],
     // remove formatting button
    ];

    var quillFull = new Quill('#document-full-create', {
      modules: {
        toolbar: toolbarOptions,
        autoformat: true
      },
      theme: 'snow',
      placeholder: "Write something..."
    });

</script>
