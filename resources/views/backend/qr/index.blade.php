@extends('backend.layouts.detail')
@section('title', 'QR')
@section('headscript')
    <script src="{{ asset('custom/js/qr.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('custom/css/custom_style.css') }}">
@stop
@section('content')
    @php
        $data=localization();
    @endphp
    @include('backend.layouts.detail_sidebar')
    <div class="padding-left-custom remove-padding">
        {!! breadcrumInner('qr',Session::get('project_id'),'locations','/project/detail/'.Session::get('project_id'),'project','/project') !!}
        <div class="card">
            <div class="card-body">
                @if(Auth()->user()->hasPermissionTo('create_projectqr') || Auth::user()->all_companies == 1 )
                    <span class="add-qr float-right mb-3 mr-2">
                        <a  class="text-success">
                            <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
                        </a>
                    </span>
                @endif
                @if(Auth()->user()->hasPermissionTo('selected_delete_projectqr') || Auth::user()->all_companies == 1 )
                    <button class="btn btn-danger btn-sm" id="deleteselectedqr">
                        <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                            {!! checkKey('selected_delete',$data) !!}
                        </span>
                    </button>
                @endif
                @if(Auth()->user()->hasPermissionTo('selected_delete_projectqr') || Auth::user()->all_companies == 1 )
                    <button class="btn btn-danger btn-sm" style="display: none;" id="deleteselectedqrSoft">
                        <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                            {!! checkKey('selected_delete',$data) !!}
                        </span>
                    </button>
                @endif
                @if(Auth()->user()->hasPermissionTo('selected_active_button_projectqr') || Auth::user()->all_companies == 1 )
                    <button class="btn btn-success btn-sm" style="display: none;" id="selectedactivebuttonqr">
                        <span class="assign_class label{{getKeyid('selected_active',$data)}}" data-id="{{getKeyid('selected_active',$data) }}" data-value="{{checkKey('selected_active',$data) }}" >
                            {!! checkKey('selected_active',$data) !!}
                        </span>
                    </button>
                @endif
                @if(Auth()->user()->hasPermissionTo('restore_delete_projectqr') || Auth::user()->all_companies == 1 )
                    <button class="btn btn-primary btn-sm" id="restorebuttonqr">
                        <span class="assign_class label{{getKeyid('restore',$data)}}" data-id="{{getKeyid('restore',$data) }}" data-value="{{checkKey('restore',$data) }}" >
                            {!! checkKey('restore',$data) !!}
                        </span>
                    </button>
                @endif
                @if(Auth()->user()->hasPermissionTo('active_projectqr') || Auth::user()->all_companies == 1 )
                    <button  style="display: none;" class="btn btn-primary btn-sm" id="activebuttonqr">
                        <span class="assign_class label{{getKeyid('show_active',$data)}}" data-id="{{getKeyid('show_active',$data) }}" data-value="{{checkKey('show_active',$data) }}" >
                            {!! checkKey('show_active',$data) !!}
                        </span>
                    </button>
                @endif
                @if(Auth()->user()->hasPermissionTo('csv_projectqr') || Auth::user()->all_companies == 1 )
                    <form class="form-style" method="POST" action="{{ url('qr/export/excel') }}">
                        <input type="hidden" id="export_excel_qr" name="export_excel_qr" value="1">
                        <input type="hidden" class="qr_export" name="excel_array" value="1">
                        {!! csrf_field() !!}
                        <button  type="submit" id="export_excel_qr_btn" class="form_submit_check btn btn-warning btn-sm">
                            <span class="assign_class label{{getKeyid('excel_export',$data)}}" data-id="{{getKeyid('excel_export',$data) }}" data-value="{{checkKey('excel_export',$data) }}" >
                              {!! checkKey('excel_export',$data) !!}
                            </span>
                        </button>
                    </form>
                @endif
                @if(Auth()->user()->hasPermissionTo('word_projectqr') || Auth::user()->all_companies == 1 )
                    <form class="form-style" method="POST" action="{{ url('qr/export/world') }}">
                        <input type="hidden" id="export_world_qr" name="export_world_qr" value="1">
                        <input type="hidden" class="qr_export" name="word_array" value="1">
                        {!! csrf_field() !!}
                        <button  type="submit" id="export_world_qr_btn" class="form_submit_check btn btn-success btn-sm">
                            <span class="assign_class label{{getKeyid('word_export',$data)}}" data-id="{{getKeyid('word_export',$data) }}" data-value="{{checkKey('word_export',$data) }}" >
                                {!! checkKey('word_export',$data) !!}
                            </span>
                        </button>
                    </form>
                @endif
                @if(Auth()->user()->hasPermissionTo('pdf_projectqr') || Auth::user()->all_companies == 1 )
                    <form  class="form-style" {{pdf_view('location_breakdowns')}}  method="POST" action="{{ url('qr/export/pdf') }}">
                        <input type="hidden" id="export_pdf_qr" name="export_pdf_qr" value="1">
                        <input type="hidden" class="qr_export" name="pdf_array" value="1">
                        {!! csrf_field() !!}
                        <button  type="submit" id="export_pdf_qr_btn"  class="form_submit_check btn btn-secondary btn-sm">
                            <span class="assign_class label{{getKeyid('pdf_export',$data)}}" data-id="{{getKeyid('pdf_export',$data) }}" data-value="{{checkKey('pdf_export',$data) }}" >
                                {!! checkKey('pdf_export',$data) !!}
                            </span>
                        </button>
                    </form>
                @endif
                <div id="table-qr" class="table-editable table-responsive">
                    <table id="qr_table" class=" table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th class="no-sort all_checkboxes_style">
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input qr_checkbox_all" id="qr_checkbox_all">
                                    <label class="form-check-label" for="qr_checkbox_all">
                                        <span class="assign_class label{{getKeyid('all',$data)}}" data-id="{{getKeyid('all',$data) }}" data-value="{{checkKey('all',$data) }}" >
                                            {!! checkKey('all',$data) !!}
                                        </span>
                                    </label>
                                </div>
                            </th>
                            <th class="no-sort">
                                <span class="assign_class label{{getKeyid('qr',$data)}}" data-id="{{getKeyid('qr',$data) }}" data-value="{{checkKey('qr',$data) }}" >
                                    {!! checkKey('qr',$data) !!}
                                </span>
                            </th>
                            <th>
                                <span class="assign_class label{{getKeyid('name',$data)}}" data-id="{{getKeyid('name',$data) }}" data-value="{{checkKey('name',$data) }}" >
                                    {!! checkKey('name',$data) !!}
                                </span>
                            </th>
                            <th>
                                <span class="assign_class label{{getKeyid('directions',$data)}}" data-id="{{getKeyid('directions',$data) }}" data-value="{{checkKey('directions',$data) }}" >
                                    {!! checkKey('directions',$data) !!}
                                </span>
                            </th>
                            <th>
                                <span class="assign_class label{{getKeyid('information',$data)}}" data-id="{{getKeyid('information',$data) }}" data-value="{{checkKey('information',$data) }}" >
                                    {!! checkKey('information',$data) !!}
                                </span>
                            </th>
{{--                            <th></th>--}}
                            <th class="no-sort all_action_btn" id="action"></th>
                        </tr>
                        </thead>
                        <tbody id="qr_data"></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @include('backend.label.input_label')
    @include('backend.qr.input_permission')
    <script type="text/javascript">
        function qr_data(url) {
            $.ajax({
                type: 'GET',
                url: url,
                success: function(response)
                {
                    $('#qr_table').DataTable().clear().destroy();
                    var table = $('#qr_table').dataTable(
                        {
                            processing: true,
                            language: {
                              'lengthMenu': '  _MENU_ ',
                              'search':'<span class="assign_class label'+$('#search_id').attr('data-id')+'" data-id="'+$('#search_id').attr('data-id')+'" data-value="'+$('#search_id').val()+'" >'+$('#search_id').val()+'</span>',
                              'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                              'info':'',
                              'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                              'paginate': {
                                  'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                                  'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                              },
                              'infoFiltered': "(filtered from _MAX_ total records)"
                            },
                            "ajax": {
                                "url": url,
                                "type": 'get',
                            },
                            "createdRow": function( row, data, dataIndex,columns,flag )
                            {

                                var checkbox_permission=$('#checkbox_permission').val();
                                var checkbox='';
                                checkbox+='<div class="form-check"><input type="checkbox" class="form-check-input selectedrowqr" id="qrs'+data.id+'"><label class="form-check-label" for="qrs'+data.id+'""></label></div>';
                                var submit='';
                                if ($('#edit_projectqr').val()) {
                                 submit+='<a type="button" class="btn btn-primary btn-xs all_action_btn_margin my-0 waves-effect waves-light saveqrdata equipment_show_button_'+data.id+' show_tick_btn'+data.id+'" style="display: none;" id="'+data.id+'"><i class="fas fa-check"></i></a>';
                                }
                                var qr='';
                                qr+= '<img src="{{asset("/qr")}}/'+data.name+'.png"  width="50" height="50">';
                                $(columns[0]).html(checkbox);
                                $(columns[1]).html(qr);
                                $(columns[1]).attr('class','qr_image'+data['id']);
                                $(columns[2]).attr('id', 'name'+data['id']);
                                $(columns[2]).attr('onkeydown', 'editSelectedRow(qr_tr'+data['id']+')');
                                if($("#projectqr_name_edit").val() =='1' && $('#edit_projectqr').val()) {
                                    if (data.flag==1){
                                        $(columns[2]).attr('Contenteditable', 'true');
                                    }
                                }
                                $(columns[2]).attr('class','edit_inlinepe');
                                $(columns[2]).attr('data-id',data['id']);
                                $(columns[3]).attr('id','directions'+data['id']);
                                $(columns[3]).attr('class','edit_inlinepe');
                                $(columns[3]).attr('data-id',data['id']);
                                $(columns[3]).attr('onkeydown', 'editSelectedRow(qr_tr'+data['id']+')');
                                if($("#projectqr_directions_edit").val() =='1' && $('#edit_projectqr').val()) {
                                    if (data.flag==1){
                                        $(columns[3]).attr('Contenteditable', 'true');
                                    }
                                }
                                if($("#projectqr_information_edit").val() =='1' && $('#edit_projectqr').val()) {
                                    if (data.flag==1){
                                        $(columns[4]).attr('Contenteditable', 'true');
                                    }
                                }
                                $(columns[4]).attr('class','edit_inlinepe');
                                $(columns[4]).attr('data-id',data['id']);
                                $(columns[4]).attr('id','information'+data['id']);
                                $(columns[4]).attr('onkeydown', 'editSelectedRow(qr_tr'+data['id']+')');
                                if (data.flag==1){
                                    $(columns[5]).append(submit);
                                }
                                $(row).attr('id', 'qr_tr'+data['id']);
                                //$(row).attr('class', 'selectedrowqr');
                                $(row).attr('data-id', data['id']);
                            },
                            columns:
                                [
                                    {data: 'checkbox', name: 'checkbox',visible:$('#projectqr_checkbox').val()},
                                    {data: 'qr', name: 'qr',visible:$('#projectqr_qr').val()},
                                    {data: 'name', name: 'name',visible:$('#projectqr_name').val()},
                                    {data: 'directions', name: 'directions',visible:$('#projectqr_directions').val()},
                                    {data: 'information', name: 'information',visible:$('#projectqr_information').val()},
                                    // {data: 'submit', name: 'submit'},
                                    {data: 'actions', name: 'actions',visible:$('#delete_projectqr').val()},
                                ],
                            "columnDefs": [ {
                                "targets": [0,1],
                                "orderable": false
                            } ]
                        }
                    );
                    if ($(":checkbox").prop('checked',true)){
                        $(":checkbox").prop('checked',false);
                    }
                },
                error: function (error) {
                    console.log(error);
                }
            });
            // (function ($) {
            //     var active ='<span class="assign_class label'+$('#breadcrumb_qr').attr('data-id')+'" data-id="'+$('#breadcrumb_qr').attr('data-id')+'" data-value="'+$('#breadcrumb_qr').val()+'" >'+$('#breadcrumb_qr').val()+'</span>';
            //     $('.breadcrumb-item.active').html(active);
            //     var parent ='<span class="assign_class label'+$('#dashboard_label').attr('data-id')+'" data-id="'+$('#dashboard_label').attr('data-id')+'" data-value="'+$('#dashboard_label').val()+'" >'+$('#dashboard_label').val()+'</span>';
            //     $('.breadcrumb-item a').html(parent);
            // }(jQuery));
        }

        $(document).ready(function () {
            var url='/qr/getall';
            qr_data(url);

        });

    </script>
@endsection
