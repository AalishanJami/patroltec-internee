<table>
    <thead>
    <tr>
        @if(Auth()->user()->hasPermissionTo('projectqr_name_excel_export') || Auth::user()->all_companies == 1 )
            <th>Name</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('projectqr_directions_excel_export') || Auth::user()->all_companies == 1 )
            <th>Directions</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('projectqr_information_excel_export') || Auth::user()->all_companies == 1 )
            <th>Information</th>
        @endif
        <th>Notes</th>
    </tr>
    </thead>
    <tbody>
    @foreach($data as $row)
        <tr>
            @if(Auth()->user()->hasPermissionTo('projectqr_name_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{ $row->name }}</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('projectqr_directions_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{ $row->directions }}</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('projectqr_information_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{ $row->information }}</td>
            @endif
            <td>{{ $row->tag }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
