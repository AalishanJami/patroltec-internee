@if(Auth()->user()->all_companies == 1)
    <input type="hidden" id="projectqr_checkbox" value="1">
    <input type="hidden" id="projectqr_qr" value="1">
    <input type="hidden" id="projectqr_name" value="1">
    <input type="hidden" id="projectqr_directions" value="1">
    <input type="hidden" id="projectqr_information" value="1">
    <input type="hidden" id="edit_projectqr" value="1">
    <input type="hidden" id="delete_projectqr" value="1">

    <!----edit------>
    <input type="hidden" id="projectqr_qr_edit" value="1">
    <input type="hidden" id="projectqr_name_edit" value="1">
    <input type="hidden" id="projectqr_directions_edit" value="1">
    <input type="hidden" id="projectqr_information_edit" value="1">

    <!----create------>
    <input type="hidden" id="projectqr_qr_create" value="1">
    <input type="hidden" id="projectqr_name_create" value="1">
    <input type="hidden" id="projectqr_directions_create" value="1">
    <input type="hidden" id="projectqr_information_create" value="1">
@else
    <input type="hidden" id="projectqr_checkbox" value="{{Auth()->user()->hasPermissionTo('projectqr_checkbox')}}">
    <input type="hidden" id="projectqr_qr" value="{{Auth()->user()->hasPermissionTo('projectqr_qr')}}">
    <input type="hidden" id="projectqr_name" value="{{Auth()->user()->hasPermissionTo('projectqr_name')}}">
    <input type="hidden" id="projectqr_directions" value="{{Auth()->user()->hasPermissionTo('projectqr_directions')}}">
    <input type="hidden" id="projectqr_information" value="{{Auth()->user()->hasPermissionTo('projectqr_information')}}">
    <input type="hidden" id="edit_projectqr" value="{{Auth()->user()->hasPermissionTo('edit_projectqr')}}">
    <input type="hidden" id="delete_projectqr" value="{{Auth()->user()->hasPermissionTo('edit_projectqr')}}">

    <!----edit------>
    <input type="hidden" id="projectqr_qr_edit" value="{{Auth()->user()->hasPermissionTo('projectqr_qr_edit')}}">
    <input type="hidden" id="projectqr_name_edit" value="{{Auth()->user()->hasPermissionTo('projectqr_name_edit')}}">
    <input type="hidden" id="projectqr_directions_edit" value="{{Auth()->user()->hasPermissionTo('projectqr_directions_edit')}}">
    <input type="hidden" id="projectqr_information_edit" value="{{Auth()->user()->hasPermissionTo('projectqr_information_edit')}}">

    <!----create------>
    <input type="hidden" id="projectqr_qr_create" value="{{Auth()->user()->hasPermissionTo('projectqr_qr_create')}}">
    <input type="hidden" id="projectqr_name_create" value="{{Auth()->user()->hasPermissionTo('projectqr_name_create')}}">
    <input type="hidden" id="projectqr_directions_create" value="{{Auth()->user()->hasPermissionTo('projectqr_directions_create')}}">
    <input type="hidden" id="projectqr_information_create" value="{{Auth()->user()->hasPermissionTo('projectqr_information_create')}}">
@endif
