@include('backend.layouts.pdf_start')
  <thead>
  <tr>
    @if(Auth()->user()->hasPermissionTo('projectqr_qr_pdf_export') || Auth::user()->all_companies == 1 )
      <th style="vertical-align: text-top;" class="pdf_table_layout"> QR</th>
    @endif
    @if(Auth()->user()->hasPermissionTo('projectqr_name_pdf_export') || Auth::user()->all_companies == 1 )
      <th style="vertical-align: text-top;" class="pdf_table_layout"> Name</th>
    @endif
    @if(Auth()->user()->hasPermissionTo('projectqr_directions_pdf_export') || Auth::user()->all_companies == 1 )
        <th style="vertical-align: text-top;" class="pdf_table_layout">Direction</th>
    @else
        <th style="vertical-align: text-top;" class="pdf_table_layout"></th>
    @endif
    @if(Auth()->user()->hasPermissionTo('projectqr_information_pdf_export') || Auth::user()->all_companies == 1 )
        <th style="vertical-align: text-top;" class="pdf_table_layout">Information</th>
    @else
        <th style="vertical-align: text-top;" class="pdf_table_layout"></th>
    @endif
  </tr>
  </thead>
  <tbody>
  @foreach($data as $value)
    <tr>
        @if(Auth()->user()->hasPermissionTo('projectqr_qr_pdf_export') || Auth::user()->all_companies == 1 )
            <td class="pdf_table_layout"><p class="col_text_style"><img src="{{public_path('qr/'.$value->name.'.png')}}" height="100" width="100"></p></td>
        @endif
        @if(Auth()->user()->hasPermissionTo('projectqr_name_pdf_export') || Auth::user()->all_companies == 1 )
            <td class="pdf_table_layout"><p class="col_text_style">{{ $value->name }}</p></td>
        @endif
        @if(Auth()->user()->hasPermissionTo('projectqr_directions_pdf_export') || Auth::user()->all_companies == 1 )
            <td class="pdf_table_layout"><p class="col_text_style">{{$value->directions}}</p></td>
        @else
            <td class="pdf_table_layout"><p class="col_text_style"></p></td>
        @endif
        @if(Auth()->user()->hasPermissionTo('projectqr_information_pdf_export') || Auth::user()->all_companies == 1 )
            <td class="pdf_table_layout"><p class="col_text_style">{{$value->information}}</p></td>
        @else
            <td class="pdf_table_layout"><p class="col_text_style"></p></td>
        @endif
    </tr>
  @endforeach
  </tbody>
@include('backend.layouts.pdf_end')
