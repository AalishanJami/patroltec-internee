@if(Auth()->user()->all_companies == 1)
    <input type="hidden" id="schedule_checkbox" value="1">
    <input type="hidden" id="schedule_projectname" value="1">
    <input type="hidden" id="schedule_startdate" value="1">
    <input type="hidden" id="schedule_time" value="1">

@else
    <input type="hidden" id="schedule_checkbox" value="{{Auth()->user()->hasPermissionTo('schedule_checkbox')}}">
    <input type="hidden" id="schedule_projectname" value="{{Auth()->user()->hasPermissionTo('schedule_projectname')}}">
    <input type="hidden" id="schedule_startdate" value="{{Auth()->user()->hasPermissionTo('schedule_startdate')}}">
    <input type="hidden" id="schedule_time" value="{{Auth()->user()->hasPermissionTo('schedule_time')}}">
@endif


