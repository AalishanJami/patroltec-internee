@include('backend.layouts.pdf_start')
    <thead>
    <tr>
        @if(Auth()->user()->hasPermissionTo('schedule_projectname_pdf_export') || Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout"> Location Name</th>
        @else
            <th style="vertical-align: text-top;" class="pdf_table_layout"></th>
        @endif
        @if(Auth()->user()->hasPermissionTo('schedule_startdate_pdf_export') || Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout"> Start Date</th>
        @else
            <th style="vertical-align: text-top;" class="pdf_table_layout"></th>
        @endif
        @if(Auth()->user()->hasPermissionTo('schedule_time_pdf_export') || Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout"> Time</th>
        @else
            <th style="vertical-align: text-top;" class="pdf_table_layout"></th>
        @endif
    </tr>
    </thead>
    <tbody>
    @foreach($data as $value)
        <tr>
            @if(Auth()->user()->hasPermissionTo('schedule_projectname_pdf_export') || Auth::user()->all_companies == 1 )
                <td class="pdf_table_layout"><p class="col_text_style">{{ $value->scheduleTaskLocation->location->name }}</p></td>
            @else
                <td class="pdf_table_layout"><p class="col_text_style"></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('schedule_startdate_pdf_export') || Auth::user()->all_companies == 1 )
                <td class="pdf_table_layout"><p class="col_text_style">{{ $value->start_date }}</p></td>
            @else
                <td class="pdf_table_layout"><p class="col_text_style"></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('schedule_time_pdf_export') || Auth::user()->all_companies == 1 )
                <td class="pdf_table_layout"><p class="col_text_style">{{ $value->time }}</p></td>
            @else
                <td class="pdf_table_layout"><p class="col_text_style"></td>
            @endif
        </tr>
    @endforeach
    </tbody>
@include('backend.layouts.pdf_end')