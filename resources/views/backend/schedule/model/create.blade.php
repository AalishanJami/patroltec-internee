@php $data=localization(); @endphp
<style>
    .error_message{
        position: absolute;
        top: 98%;
        text-transform: initial !important;
        color:#e3342f;
        display:block;
    }
    .drompdown_border{
        border-bottom:1px solid #e3342f !important
    }
</style>
<div class="modal fade" id="modalSchedule" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="width:100%">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">
                    <span class="assign_class label{{getKeyid('schedule',$data)}}" data-id="{{getKeyid('schedule',$data) }}" data-value="{{checkKey('schedule',$data) }}" >
                        {!! checkKey('schedule',$data) !!}
                    </span>
                    @if(Auth()->user()->hasPermissionTo('view_cms') || Auth::user()->all_companies == 1 )
                        <button data-class="edit-disable" class="edit_cms_enable edit_cms_switch btn btn-primary btn-sm waves-effect waves-light" style="display: none;" >
                            Edit cms
                        </button>
                        <button  data-class="edit-enable" class="edit_cms_disable edit_cms_switch btn btn-danger btn-sm waves-effect waves-light">
                            Edit cms
                        </button>
                    @endif
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="schedule_form">
                <div class="modal-body">
                    <div class="card-body px-lg-5 pt-0">
                        <div class="form-row">
                            <div class="col mt-2">
                                @if(Auth()->user()->hasPermissionTo('schedule_tasktype_create') || Auth::user()->all_companies == 1 )
                                    <a class="text-success task_type_btn task_type_model">
                                        <button type="button"  class="btn btn-sm btn-outline-primary waves-effect form-custom-boutton " style="float: left;">
                                            <span class="assign_class label{{getKeyid('task_type',$data)}}" data-id="{{getKeyid('task_type',$data) }}" data-value="{{checkKey('task_type',$data) }}" >
                                                {!! checkKey('task_type',$data) !!}
                                            </span>
                                        </button>
                                    </a>
                                    <div class="md-form task_type_listing">
                                        <select searchable="Search here.."   class="mdb-select" name="task_type_id" id="task_type_id">
                                            <option value="" disabled selected>Please Select</option>
                                            @foreach($task_type as $key=>$value)
                                                <option value="{{$value->id}}">{{$value->name}}</option>
                                            @endforeach
                                        </select>
                                        <small id="location_breakdown_id_message" class="text-danger error_message"></small>
                                    </div>
                                @endif
                            </div>
                            <div class="col">
                                <!-- Last name -->
                                @if(Auth()->user()->hasPermissionTo('schedule_projectname_create') || Auth::user()->all_companies == 1 )
                                    <div class="md-form">
                                        <span class="assign_class label{{getKeyid('project_name',$data)}}" data-id="{{getKeyid('project_name',$data) }}" data-value="{{checkKey('project_name',$data) }}" >
                                            {!! checkKey('project_name',$data) !!}
                                        </span>

                                        <select searchable="Search here.."   class="mdb-select location-dropdown" name="location_id" id="schdule_project" searchable="searchable">
                                            <option value="" disabled selected>Please Select</option>
                                            @foreach($locations as $key=>$value)
                                                <option value="{{$value->id}}">{{$value->name}}</option>
                                            @endforeach
                                        </select>
{{--                                        {!! Form::select('location_id',$locations->pluck('name','id'),null, ['class'=>'mdb-select location-dropdown','id'=>'schdule_project','placeholder'=>'Please Select','searchable'=>'search here']) !!}--}}
                                        <small id="schdule_project_message" class="text-danger error_message"></small>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="row">

                            <div class="col">
                                <div class="md-form">
                                @if(Auth()->user()->hasPermissionTo('schedule_user_create') || Auth::user()->all_companies == 1 )
                                    <label class="mdb-main-label" for="schedule_user_id">
                                        <span class="assign_class label{{getKeyid('user_name',$data)}}" data-id="{{getKeyid('user_name',$data) }}" data-value="{{checkKey('user_name',$data) }}" >
                                            {!! checkKey('user_name',$data) !!}
                                        </span>
                                    </label>
                                    <select searchable="Search here.."   class="mdb-select colorful-select dropdown-primary" name="user_id[]" id="schedule_user_id" searchable="searchable" multiple>
                                        <option value="" disabled selected>Please Select</option>
                                        @foreach($users as $key=>$value)
                                            <option value="{{$key}}">{{$value}}</option>
                                        @endforeach
                                    </select>
{{--                                    {!! Form::select('user_id[]',$users,null, ['class' =>' mdb-select colorful-select dropdown-primary md-form','id'=>'schedule_user_id','placeholder'=>'Please Select','searchable'=>'search here','multiple']) !!}--}}
                                </div>
                                @endif
                            </div>
                            <div class="col">
                                <input type="hidden" value="{{$form_type}}" name="form_type_create">
                                @if($form_type!="m")
                                    <div class="md-form" >
                                        @if(Auth()->user()->hasPermissionTo('schedule_location_breakdown_create') || Auth::user()->all_companies == 1 )
                                            <select searchable="Search here.." required  class="mdb-select colorful-select dropdown-primary md-form schedule_qr_id" multiple  name="location_breakdown_id[]" id="location_breakdown_id_create">
                                                <option value="" disabled selected>Please Select</option>
                                            </select>
                                            @if($form_type=="t")
                                                <label class="mdb-main-label">
                                                        <span class="assign_class label{{getKeyid('qr_list',$data)}}" data-id="{{getKeyid('qr_list',$data) }}" data-value="{{checkKey('qr_list',$data) }}" >
                                                            {!! checkKey('qr_list',$data) !!}
                                                        </span>
                                                </label>
                                            @elseif($form_type=="e")
                                                <label class="mdb-main-label">
                                                        <span class="assign_class label{{getKeyid('equipment_list',$data)}}" data-id="{{getKeyid('equipment_list',$data) }}" data-value="{{checkKey('equipment_list',$data) }}" >
                                                            {!! checkKey('equipment_list',$data) !!}
                                                        </span>
                                                </label>
                                            @endif
                                            <small id="location_breakdown_id" class="text-danger error_message"></small>
                                        @endif
                                    </div>
                                @endif
                            </div>
                        </div>
                        <br>
                        <div class="card">
                            <h5 class="card-header info-color white-text  py-1s">
                                <strong>
                                        <span class="assign_class label{{getKeyid('schedule',$data)}}" data-id="{{getKeyid('schedule',$data) }}" data-value="{{checkKey('schedule',$data) }}" >
                                            {!! checkKey('schedule',$data) !!}
                                        </span>
                                </strong>
                            </h5>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <div class="md-form">
                                            @if(Auth()->user()->hasPermissionTo('schedule_startdate_create') || Auth::user()->all_companies == 1 )
                                                <input type="text" required id="start_date_create" value="{{Carbon\Carbon::parse()->format('jS M Y')}}" name="start_date" class="form-control start_date datepicker">
                                                <label for="start_date_create" style="position: absolute;top: -76%;display: block;">
                                                        <span class="assign_class label{{getKeyid('start_date',$data)}}" data-id="{{getKeyid('start_date',$data) }}" data-value="{{checkKey('start_date',$data) }}" >
                                                            {!! checkKey('start_date',$data) !!}
                                                        </span>
                                                </label>
                                                <small id="start_date_er" class="text-danger error_message"></small>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="md-form">
                                            @if(Auth()->user()->hasPermissionTo('schedule_time_create') || Auth::user()->all_companies == 1 )
                                                <input  type="text" required id="input_starttime_create" name="time" class="form-control inputStarttime timepicker">
                                                <label for="date-picker-example">
                                                        <span class="assign_class label{{getKeyid('time',$data)}}" data-id="{{getKeyid('time',$data) }}" data-value="{{checkKey('time',$data) }}" >
                                                            {!! checkKey('time',$data) !!}
                                                        </span>
                                                </label>
                                                <small id="time_er" class="text-danger error_message input_starttime"></small>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="md-form">
                                            @if(Auth()->user()->hasPermissionTo('schedule_enddate_create') || Auth::user()->all_companies == 1 )
                                                <input type="text" required  name="end_date" value="{{Carbon\Carbon::parse()->format('jS M Y')}}" id="end_date_create" class="form-control date-picker-example2 datepicker">
                                                <label for="end_date_create" style="position: absolute;top: -76%;display: block;">
                                                        <span class="assign_class label{{getKeyid('expiry_date',$data)}}" data-id="{{getKeyid('expiry_date',$data) }}" data-value="{{checkKey('expiry_date',$data) }}" >
                                                            {!! checkKey('expiry_date',$data) !!}
                                                        </span>
                                                </label>
                                                <small id="end_date_er" class="text-danger error_message"></small>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    @if(Auth()->user()->hasPermissionTo('schedule_pretime_create') || Auth::user()->all_companies == 1 )
                                        <div class="col">
                                            <div class="md-form">
                                                    <span class="assign_class label{{getKeyid('pre',$data)}}" data-id="{{getKeyid('pre',$data) }}" data-value="{{checkKey('pre',$data) }}" >
                                                        {!! checkKey('pre',$data) !!}
                                                    </span>
                                                <input type="number" max="1" value="1" class="form-control pre_time_mins" name="pre_time_mins">
                                                <small id="pre_time_mins_er" class="text-danger error_message"></small>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="md-form">
                                                    <span class="assign_class label{{getKeyid('time',$data)}}" data-id="{{getKeyid('time',$data) }}" data-value="{{checkKey('time',$data) }}" >
                                                        {!! checkKey('time',$data) !!}
                                                    </span>
                                                {!! Form::select('pre_time_select',['1'=>'Min','2'=>'Hrs','3'=>'Days','4'=>'Week'], null, ['class' =>'mdb-select','id'=>'pre_time_select']) !!}
                                            </div>
                                        </div>
                                    @endif
                                    @if(Auth()->user()->hasPermissionTo('schedule_posttime_create') || Auth::user()->all_companies == 1 )
                                        <div class="col">
                                            <div class="md-form">
                                                    <span class="assign_class label{{getKeyid('post',$data)}}" data-id="{{getKeyid('post',$data) }}" data-value="{{checkKey('post',$data) }}" >
                                                        {!! checkKey('post',$data) !!}
                                                    </span>
                                                <input type="number" max="1" value="1" class="form-control post_time_mins" name="post_time_mins">
                                                <small id="post_time_mins_er" class="text-danger error_message"></small>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="md-form">
                                                    <span class="assign_class label{{getKeyid('time',$data)}}" data-id="{{getKeyid('time',$data) }}" data-value="{{checkKey('time',$data) }}" >
                                                        {!! checkKey('time',$data) !!}
                                                    </span>
                                                {!! Form::select('post_time_select',['1'=>'Min','2'=>'Hrs','3'=>'Days','4'=>'Week'], null, ['class' =>'mdb-select','id'=>'post_time_select']) !!}
                                            </div>
                                        </div>
                                    @endif
                                </div>
                                <div class="row">
                                    @if(Auth()->user()->hasPermissionTo('schedule_recyclealljobs_create') || Auth::user()->all_companies == 1 )
                                        <div class="col">
                                            <div class="md-form">
                                                <input type="text" id="date-picker-example" required name="recycle_jobs_after" class="form-control datepicker">
                                                <label for="date-picker-example">
                                                        <span class="assign_class label{{getKeyid('recycle_all_jobs_after',$data)}}" data-id="{{getKeyid('recycle_all_jobs_after',$data) }}" data-value="{{checkKey('recycle_all_jobs_after',$data) }}" >
                                                            {!! checkKey('recycle_all_jobs_after',$data) !!}
                                                        </span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="md-form">
                                                <div class="form-check ">
                                                    <input type="checkbox" name="do_not_schedule" required class="form-check-input company" id="Schedule1" value="1">
                                                    <label class="form-check-label" for="Schedule1">
                                                            <span class="assign_class label{{getKeyid('do_not_schedule',$data)}}" data-id="{{getKeyid('do_not_schedule',$data) }}" data-value="{{checkKey('do_not_schedule',$data) }}" >
                                                                {!! checkKey('do_not_schedule',$data) !!}
                                                            </span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <br>
                        @if(Auth()->user()->hasPermissionTo('schedule_frequency_create') || Auth::user()->all_companies == 1 )
                            <div class="card">
                                <h5 class="card-header info-color white-text  py-1s">
                                    <strong>
                                            <span class="assign_class label{{getKeyid('frequency',$data)}}" data-id="{{getKeyid('frequency',$data) }}" data-value="{{checkKey('frequency',$data) }}" >
                                                {!! checkKey('frequency',$data) !!}
                                            </span>
                                    </strong>
                                </h5>
                                <div class="card-body">

                                    <div class="row">
                                        <div class="col">
                                            <ul class="list-group list-group-flush">
                                                <li class="list-group-item">
                                                    <div class="form-check ">
                                                        <input type="checkbox"  name="mon" required class="form-check-input company" id="mon1" value="1">
                                                        <label class="form-check-label" for="mon1">
                                                                <span class="assign_class label{{getKeyid('mon',$data)}}" data-id="{{getKeyid('mon',$data) }}" data-value="{{checkKey('mon',$data) }}" >
                                                                    {!! checkKey('mon',$data) !!}
                                                                </span>
                                                        </label>
                                                    </div>
                                                </li>
                                                <li class="list-group-item">
                                                    <div class="form-check ">
                                                        <input type="checkbox" name="thu" required  class="form-check-input company" id="thu1" value="1">
                                                        <label class="form-check-label" for="thu1">
                                                                <span class="assign_class label{{getKeyid('thu',$data)}}" data-id="{{getKeyid('thu',$data) }}" data-value="{{checkKey('thu',$data) }}" >
                                                                    {!! checkKey('thu',$data) !!}
                                                                </span>
                                                        </label>
                                                    </div>
                                                </li>
                                                <li class="list-group-item">
                                                    <div class="form-check ">
                                                        <input type="checkbox" name="sun" required class="form-check-input company" id="sun1" value="1">
                                                        <label class="form-check-label" for="sun1">
                                                                <span class="assign_class label{{getKeyid('sun',$data)}}" data-id="{{getKeyid('sun',$data) }}" data-value="{{checkKey('sun',$data) }}" >
                                                                    {!! checkKey('sun',$data) !!}
                                                                </span>
                                                        </label>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col">
                                            <ul class="list-group list-group-flush">
                                                <li class="list-group-item">
                                                    <div class="form-check ">
                                                        <input type="checkbox" name="tue" required  class="form-check-input company" id="tue3" value="1">
                                                        <label class="form-check-label" for="tue3">
                                                                <span class="assign_class label{{getKeyid('tue',$data)}}" data-id="{{getKeyid('tue',$data) }}" data-value="{{checkKey('tue',$data) }}" >
                                                                    {!! checkKey('tue',$data) !!}
                                                                </span>
                                                        </label>
                                                    </div>
                                                </li>
                                                <li class="list-group-item">
                                                    <!-- Default checked -->
                                                    <div class="form-check ">
                                                        <input type="checkbox" name="fri" required class="form-check-input company" id="fri1" value="1">
                                                        <label class="form-check-label" for="fri1">
                                                                <span class="assign_class label{{getKeyid('fri',$data)}}" data-id="{{getKeyid('fri',$data) }}" data-value="{{checkKey('fri',$data) }}" >
                                                                    {!! checkKey('fri',$data) !!}
                                                                </span>
                                                        </label>
                                                    </div>
                                                </li>
                                                <li class="list-group-item">
                                                    <div class="form-check ">
                                                        <input type="checkbox" name="bank" required class="form-check-input company" id="bank1" value="1">
                                                        <label class="form-check-label" for="bank1">
                                                                <span class="assign_class label{{getKeyid('bank',$data)}}" data-id="{{getKeyid('bank',$data) }}" data-value="{{checkKey('bank',$data) }}" >
                                                                    {!! checkKey('bank',$data) !!}
                                                                </span>
                                                        </label>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col">
                                            <ul class="list-group list-group-flush">
                                                <li class="list-group-item">
                                                    <div class="form-check ">
                                                        <input type="checkbox" name="wed" required class="form-check-input company" id="wed1" value="1">
                                                        <label class="form-check-label" for="wed1">
                                                                <span class="assign_class label{{getKeyid('wed',$data)}}" data-id="{{getKeyid('wed',$data) }}" data-value="{{checkKey('wed',$data) }}" >
                                                                    {!! checkKey('wed',$data) !!}
                                                                </span>
                                                        </label>
                                                    </div>
                                                </li>
                                                <li class="list-group-item">
                                                    <div class="form-check ">
                                                        <input type="checkbox" name="sat" required class="form-check-input company" id="sat1" value="1">
                                                        <label class="form-check-label" for="sat1">
                                                                <span class="assign_class label{{getKeyid('sat',$data)}}" data-id="{{getKeyid('sat',$data) }}" data-value="{{checkKey('sat',$data) }}" >
                                                                    {!! checkKey('sat',$data) !!}
                                                                </span>
                                                        </label>
                                                    </div>
                                                </li>
                                                <li class="list-group-item">
                                                </li>
                                            </ul>
                                        </div>
                                        <small id="frequency_error_msg" class="text-danger error_message" style="top:90%;left: 5%"></small>

                                    </div>

                                </div>
                            </div>
                        @endif
                        <br>
                        @if(Auth()->user()->hasPermissionTo('schedule_frequency_limit_create') || Auth::user()->all_companies == 1 )
                            <div class="card">
                                <h5 class="card-header card-sm info-color white-text  py-1s">
                                    <strong>
                                            <span class="assign_class label{{getKeyid('frequency_limit',$data)}}" data-id="{{getKeyid('frequency_limit',$data) }}" data-value="{{checkKey('frequency_limit',$data) }}" >
                                                {!! checkKey('frequency_limit',$data) !!}
                                            </span>
                                    </strong>
                                </h5>
                                <div class="card-body">
                                    <div class="row">
                                        <!-- <div class="col">
                                            <div class="md-form">
                                                <input type="number" name="week_limit" required id="date-picker-example" class="form-control ">
                                                <label for="date-picker-example">Week</label>
                                            </div>
                                        </div>

                                        <div class="col">
                                            <div class="md-form">
                                                <input type="number" required name="month_limit"
                                                id="date-picker-example" class="form-control ">
                                                <label for="date-picker-example">Month</label>
                                            </div>
                                        </div>

                                        <div class="col">
                                            <div class="md-form">
                                                <input type="number" required name="year_limit" id="date-picker-example" class="form-control ">
                                                <label for="date-picker-example">Year</label>
                                            </div>

                                        </div> -->
                                        <div class="col">
                                            <div class="md-form">
                                                <input type="number" value="1" class="form-control" name="frequency_limit">
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="md-form">

                                                {!! Form::select('frequency_type',['4'=>'Week','5'=>'Month','6'=>'Year'], null, ['class' =>'mdb-select','id'=>'frequency_type_create','placeholder'=>'plese select']) !!}
                                            </div>
                                        </div>

                                        <div class="col">
                                            <div class="md-form">
                                                <input type="number" name="max_display" required id="date-picker-example" class="form-control">
                                                <label for="date-picker-example">
                                                        <span class="assign_class label{{getKeyid('max_display',$data)}}" data-id="{{getKeyid('max_display',$data) }}" data-value="{{checkKey('max_display',$data) }}" >
                                                            {!! checkKey('max_display',$data) !!}
                                                        </span>
                                                </label>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        @endif
                        <br>
                        @if(Auth()->user()->hasPermissionTo('schedule_frequency_repeat_create') || Auth::user()->all_companies == 1 )
                            <div class="card">
                                <h5 class="card-header card-sm info-color white-text  py-1s">
                                    <strong>
                                            <span class="assign_class label{{getKeyid('frequency_repeat',$data)}}" data-id="{{getKeyid('frequency_repeat',$data) }}" data-value="{{checkKey('frequency_repeat',$data) }}" >
                                                {!! checkKey('frequency_repeat',$data) !!}
                                            </span>
                                    </strong>
                                </h5>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col">
                                            <div class="md-form">
                                                <input type="number" name="repeat" required class="form-control ">
                                                <label for="date-picker-example">
                                                        <span class="assign_class label{{getKeyid('frequency_repeat',$data)}}" data-id="{{getKeyid('frequency_repeat',$data) }}" data-value="{{checkKey('frequency_repeat',$data) }}" >
                                                            {!! checkKey('frequency_repeat',$data) !!}
                                                        </span>
                                                </label>
                                            </div>
                                        </div>

                                        <div class="col">
                                            <div class="md-form">
                                                <input type="number" name="repeat_min_gap" required  class="form-control">
                                                <label class="active" for="date-picker-example">
                                                    <span class="assign_class label{{getKeyid('gap_mins',$data)}}" data-id="{{getKeyid('gap_mins',$data) }}" data-value="{{checkKey('gap_mins',$data) }}" >
                                                        {!! checkKey('gap_mins',$data) !!}
                                                    </span>
                                                </label>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        @endif
                        <br>

                        <div class="card">
                            <h5 class="card-header  info-color white-text  py-1">
                                <strong>
                                        <span class="assign_class label{{getKeyid('task_details',$data)}}" data-id="{{getKeyid('task_details',$data) }}" data-value="{{checkKey('task_details',$data) }}" >
                                            {!! checkKey('task_details',$data) !!}
                                        </span>
                                </strong>
                            </h5>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        @if(Auth()->user()->hasPermissionTo('schedule_tag_swipe_create') || Auth::user()->all_companies == 1 )
                                            <div class="md-form">
                                                    <span class="assign_class label{{getKeyid('tag_swipe_required',$data)}}" data-id="{{getKeyid('tag_swipe_required',$data) }}" data-value="{{checkKey('tag_swipe_required',$data) }}" >
                                                        {!! checkKey('tag_swipe_required',$data) !!}
                                                    </span>
                                                <select searchable="Search here.." required  class="mdb-select" name="tag_swipe">
                                                    <option value="" disabled>Please Select</option>
                                                    <option value="1">Yes</option>
                                                    <option value="2">NO</option>
                                                </select>
                                            </div>
                                        @endif
                                    </div>

                                    <div class="col">
                                        @if(Auth()->user()->hasPermissionTo('schedule_signature_create') || Auth::user()->all_companies == 1 )
                                            <!-- <div class="md-form mt-5">
                                                <input type="number" name="signature" required class="form-control ">
                                                <label for="date-picker-example">
                                                        <span class="assign_class label{{getKeyid('signature',$data)}}" data-id="{{getKeyid('signature',$data) }}" data-value="{{checkKey('signature',$data) }}" >
                                                            {!! checkKey('signature',$data) !!}
                                                        </span>
                                                </label>
                                            </div> -->
                                        @endif
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col" id="error_result"></div>
                        </div>
            </form>
        </div>

    </div>
    <div class="modal-footer d-flex justify-content-center">
        <button class="form_submit_check btn btn-primary btn-sm" type="button" id="create_schedule">
            <span class="assign_class label{{getKeyid('save',$data)}}" data-id="{{getKeyid('save',$data) }}" data-value="{{checkKey('save',$data) }}">
                {!! checkKey('save',$data) !!}
            </span>
        </button>
    </div>
    </form>

    <div class="modal-body mx-3">

    </div>
</div>
</div>
</div>
@component('backend.schedule.model.index')
@endcomponent
<script type="text/javascript">
    $('#input_starttime_create').pickatime({});
    $('.datepicker').pickadate({selectYears:250,});
</script>
