<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
<table>
    <thead>
    <tr>
        @if(Auth()->user()->hasPermissionTo('documentTaskType_name_excel_export') || Auth::user()->all_companies == 1 )
            <th> Name</th>
        @else
            <th></th>
        @endif
    </tr>
    </thead>
    <tbody>
    @foreach($data as $value)
        <tr>
            @if(Auth()->user()->hasPermissionTo('documentTaskType_name_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{ $value->name }}</td>
            @else
                <td></td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>
</body>
</html>
