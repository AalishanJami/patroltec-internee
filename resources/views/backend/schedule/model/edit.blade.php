@php
    $data=localization();
@endphp
<style type="text/css">
    label
    {
        /*transform: translateY(-14px) scale(0.8) !important;*/
    }
</style>
<div class="modal fade" id="modalEditSchedule" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="width:100%">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">
                    <span class="assign_class label{{getKeyid('schedule',$data)}}" data-id="{{getKeyid('schedule',$data) }}" data-value="{{checkKey('schedule',$data) }}" >
                        {!! checkKey('schedule',$data) !!}
                    </span>
                    @if(Auth()->user()->hasPermissionTo('view_cms') || Auth::user()->all_companies == 1 )
                        <button data-class="edit-disable" class="edit_cms_enable edit_cms_switch btn btn-primary btn-sm waves-effect waves-light" style="display: none;" >
                            Edit cms
                        </button>
                        <button  data-class="edit-enable" class="edit_cms_disable edit_cms_switch btn btn-danger btn-sm waves-effect waves-light">
                            Edit cms
                        </button>
                    @endif
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="schedule_form_edit">

                <div class="modal-body"> <!--Card content-->
                    <div class="card-body px-lg-5 pt-0">
                        <!-- Form -->
                        <form style="color: #757575;" action="#!">
                            <!-- Name -->
                            <div class="form-row">
                                <div class="col">
                                    <input type="hidden" name="id" id="schedule_id" >
                                    @if(Auth()->user()->hasPermissionTo('schedule_tasktype_edit') || Auth::user()->all_companies == 1 )
                                        <a class="text-success task_type_btn task_type_model">
                                            <button type="button" class="btn btn-sm btn-outline-primary waves-effect form-custom-boutton" style="float: left;">
                                                <span class="assign_class label{{getKeyid('task_type',$data)}}" data-id="{{getKeyid('task_type',$data) }}" data-value="{{checkKey('task_type',$data) }}" >
                                                    {!! checkKey('task_type',$data) !!}
                                                </span>
                                            </button>
                                        </a>
                                        <div class="md-form" id="task_type_id_edit">
                                            <select searchable="Search here.."  class="mdb-select" name="task_type_id">
                                                <option value="" disabled>Please Select</option>
                                                @foreach($task_type as $key=>$value)
                                                    <option value="{{$value->id}}">{{$value->name}}</option>
                                                @endforeach()
                                            </select>
                                        </div>
                                    @endif
                                </div>
                                <div class="col">
                                    @if(Auth()->user()->hasPermissionTo('schedule_projectname_edit') || Auth::user()->all_companies == 1 )
                                        <div class="md-form" id="location_id_edit"> 
                                            <select searchable="Search here.."  class="mdb-select location-dropdown" name="location_id" id="schdule_project_edit">
                                                <option value="" selected>Please Select</option>
                                                @foreach($locations as $key=>$value)
                                                    <option value="{{$value->id}}">{{$value->name}}</option>
                                                @endforeach()
                                            </select>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    @if(Auth()->user()->hasPermissionTo('schedule_user_create') || Auth::user()->all_companies == 1 )
                                        <div class="md-form">
{{--                                            <label class="mdb-main-label" for="schedule_user_id_edit">--}}
                                                <span class="assign_class label{{getKeyid('user_name',$data)}}" data-id="{{getKeyid('user_name',$data) }}" data-value="{{checkKey('user_name',$data) }}" >
                                                    {!! checkKey('user_name',$data) !!}
                                                </span>
{{--                                            </label>--}}
                                            {!! Form::select('user_id[]',$users,null, ['class' =>' mdb-select colorful-select dropdown-primary md-form','id'=>'schedule_user_id_edit','searchable'=>'search here','multiple']) !!}
                                        </div>
                                    @endif
                                </div>
                                <div class="col">
                                    @if(Auth()->user()->hasPermissionTo('schedule_location_breakdown_edit') || Auth::user()->all_companies == 1 )
                                        <input type="hidden" value="{{$form_type}}" name="form_type_edit">
                                    @if($form_type!="m")
                                        <div class="md-form" id="location_breakdown_id_edit">
                                            <select searchable="Search here.."  class="mdb-select colorful-select dropdown-primary md-form schedule_qr_id" multiple  name="location_breakdown_id[]" id="schedule_qr_id" >
                                                <option value="" disabled selected>Choose</option>
                                            </select>
                                            @if($form_type=="t")
                                                <label class="mdb-main-label">
                                                        <span class="assign_class label{{getKeyid('qr_list',$data)}}" data-id="{{getKeyid('qr_list',$data) }}" data-value="{{checkKey('qr_list',$data) }}" >
                                                            {!! checkKey('qr_list',$data) !!}
                                                        </span>
                                                </label>
                                            @elseif($form_type=="e")
                                                <label class="mdb-main-label">
                                                        <span class="assign_class label{{getKeyid('equipment_list',$data)}}" data-id="{{getKeyid('equipment_list',$data) }}" data-value="{{checkKey('equipment_list',$data) }}" >
                                                            {!! checkKey('equipment_list',$data) !!}
                                                        </span>
                                                </label>
                                            @endif
                                        </div>
                                        <small id="location_breakdown_id_edit_msg" class="text-danger error_message"></small>
                                        @endif
                                    @endif
                                </div>
                            </div>
                            <br>
                            <div class="card">
                                <h5 class="card-header info-color white-text  py-1s">
                                    <strong>
                                        <span class="assign_class label{{getKeyid('schedule',$data)}}" data-id="{{getKeyid('schedule',$data) }}" data-value="{{checkKey('schedule',$data) }}" >
                                            {!! checkKey('schedule',$data) !!}
                                        </span>
                                    </strong>
                                </h5>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col">
                                            @if(Auth()->user()->hasPermissionTo('schedule_startdate_edit') || Auth::user()->all_companies == 1 )
                                                <div class="md-form">
                                                    <span class="assign_class label{{getKeyid('start_date',$data)}}" data-id="{{getKeyid('start_date',$data) }}" data-value="{{checkKey('start_date',$data) }}" >
                                                        {!! checkKey('start_date',$data) !!}
                                                    </span>
                                                    <input type="text" id="date-picker-example" name="start_date" class="start_date_Edit form-control datepicker">
                                                    <!--  <label class="active" for="date-picker-example">
                                                     </label> -->
                                                    <small id="start_date_edit" class="text-danger error_message"></small>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="col">
                                            @if(Auth()->user()->hasPermissionTo('schedule_time_edit') || Auth::user()->all_companies == 1 )
                                                <div class="md-form">
                                                    <span class="assign_class label{{getKeyid('time',$data)}}" data-id="{{getKeyid('time',$data) }}" data-value="{{checkKey('time',$data) }}" >
                                                        {!! checkKey('time',$data) !!}
                                                    </span>
                                                    <input placeholder="Selected time" type="text" id="input_starttime" name="time" class="time_Edit form-control timepicker">
                                                    <!--   <label class="active" for="date-picker-example">
                                                      </label> -->
                                                    <small id="time_edit" class="text-danger error_message input_starttime"></small>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="col">
                                            @if(Auth()->user()->hasPermissionTo('schedule_enddate_edit') || Auth::user()->all_companies == 1 )
                                                <div class="md-form">
                                                    <span class="assign_class label{{getKeyid('expiry_date',$data)}}" data-id="{{getKeyid('expiry_date',$data) }}" data-value="{{checkKey('expiry_date',$data) }}" >
                                                        {!! checkKey('expiry_date',$data) !!}
                                                    </span>
                                                    <input type="text" name="end_date"  id="date-picker-example2" class="end_date_Edit form-control datepicker">
                                                    <!-- <label class="active" for="date-picker-example">
                                                    </label> -->
                                                    <small id="end_date_edit" class="text-danger error_message"></small>
                                                </div>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="row">
                                        @if(Auth()->user()->hasPermissionTo('schedule_pretime_edit') || Auth::user()->all_companies == 1 )
                                            <div class="col">
                                                <div class="md-form">
                                                    <span class="assign_class label{{getKeyid('pre',$data)}}" data-id="{{getKeyid('pre',$data) }}" data-value="{{checkKey('pre',$data) }}" >
                                                        {!! checkKey('pre',$data) !!}
                                                    </span>
                                                    <input type="number" max="1" value="1" class="form-control pre_time_mins_edit" name="pre_time_mins">
                                                    <small id="pre_time_mins_edit" class="text-danger error_message"></small>
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="md-form">
                                                    <span class="assign_class label{{getKeyid('time',$data)}}" data-id="{{getKeyid('time',$data) }}" data-value="{{checkKey('time',$data) }}" >
                                                        {!! checkKey('time',$data) !!}
                                                    </span>
                                                    {!! Form::select('pre_time_select',['1'=>'Min','2'=>'Hrs','3'=>'Days','4'=>'Week'], null, ['class' =>'mdb-select','id'=>'pre_time_select_edit']) !!}
                                                </div>
                                            </div>
                                        @endif
                                        @if(Auth()->user()->hasPermissionTo('schedule_posttime_edit') || Auth::user()->all_companies == 1 )
                                            <div class="col">
                                                <div class="md-form">
                                                    <span class="assign_class label{{getKeyid('post',$data)}}" data-id="{{getKeyid('post',$data) }}" data-value="{{checkKey('post',$data) }}" >
                                                        {!! checkKey('post',$data) !!}
                                                    </span>
                                                    <input type="number" max="1" value="1" class="form-control post_time_mins_edit" name="post_time_mins">
                                                    <small id="post_time_mins_edit" class="text-danger error_message"></small>
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="md-form">
                                                    <span class="assign_class label{{getKeyid('time',$data)}}" data-id="{{getKeyid('time',$data) }}" data-value="{{checkKey('time',$data) }}" >
                                                        {!! checkKey('time',$data) !!}
                                                    </span>
                                                    {!! Form::select('post_time_select',['1'=>'Min','2'=>'Hrs','3'=>'Days','4'=>'Week'], null, ['class' =>'mdb-select','id'=>'post_time_select_edit']) !!}
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="row">
                                        @if(Auth()->user()->hasPermissionTo('schedule_recyclealljobs_edit') || Auth::user()->all_companies == 1 )
                                            <div class="col">
                                                <div class="md-form">
                                                    <span class="assign_class label{{getKeyid('recycle_all_jobs_after',$data)}}" data-id="{{getKeyid('recycle_all_jobs_after',$data) }}" data-value="{{checkKey('recycle_all_jobs_after',$data) }}" >
                                                        {!! checkKey('recycle_all_jobs_after',$data) !!}
                                                    </span>
                                                    <input type="text" id="date-picker-example" name="recycle_jobs_after" class="recycle_jobs_after_Edit form-control datepicker">
                                                    <!-- <label class="active" for="date-picker-example">
                                                    </label> -->
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="md-form">
                                                    <div class="form-check ">
                                                        <input type="checkbox" name="do_not_schedule" class="form-check-input company" id="Scheduleedit1" value="1">
                                                        <label class="active" class="form-check-label" for="Scheduleedit1">
                                                            <span class="assign_class label{{getKeyid('do_not_schedule',$data)}}" data-id="{{getKeyid('do_not_schedule',$data) }}" data-value="{{checkKey('do_not_schedule',$data) }}" >
                                                                {!! checkKey('do_not_schedule',$data) !!}
                                                            </span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <br>
                            @if(Auth()->user()->hasPermissionTo('schedule_frequency_edit') || Auth::user()->all_companies == 1 )
                                <div class="card">
                                    <h5 class="card-header info-color white-text  py-1s">
                                        <strong>
                                            <span class="assign_class label{{getKeyid('frequency',$data)}}" data-id="{{getKeyid('frequency',$data) }}" data-value="{{checkKey('frequency',$data) }}" >
                                                {!! checkKey('frequency',$data) !!}
                                            </span>
                                        </strong>
                                    </h5>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col">
                                                <ul class="list-group list-group-flush">
                                                    <li class="list-group-item">
                                                        <!-- Default checked -->
                                                        <div class="form-check ">
                                                            <input type="checkbox"  name="mon" class="form-check-input company" id="monedit1" value="1">
                                                            <label class="active" class="form-check-label" for="monedit1">
                                                                <span class="assign_class label{{getKeyid('mon',$data)}}" data-id="{{getKeyid('mon',$data) }}" data-value="{{checkKey('mon',$data) }}" >
                                                                    {!! checkKey('mon',$data) !!}
                                                                </span>
                                                            </label>
                                                        </div>
                                                    </li>
                                                    <li class="list-group-item">
                                                        <div class="form-check ">
                                                            <input type="checkbox" name="thu"  class="form-check-input company" id="thuedit1" value="1">
                                                            <label class="active" class="form-check-label" for="thuedit1">
                                                                <span class="assign_class label{{getKeyid('thu',$data)}}" data-id="{{getKeyid('thu',$data) }}" data-value="{{checkKey('thu',$data) }}" >
                                                                    {!! checkKey('thu',$data) !!}
                                                                </span>
                                                            </label>
                                                        </div>
                                                    </li>
                                                    <li class="list-group-item">
                                                        <div class="form-check ">
                                                            <input type="checkbox" name="sun" class="form-check-input company" id="sunedit1" value="1">
                                                            <label class="active" class="form-check-label" for="sunedit1">
                                                                <span class="assign_class label{{getKeyid('sun',$data)}}" data-id="{{getKeyid('sun',$data) }}" data-value="{{checkKey('sun',$data) }}" >
                                                                    {!! checkKey('sun',$data) !!}
                                                                </span>
                                                            </label>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col">
                                                <ul class="list-group list-group-flush">
                                                    <li class="list-group-item">
                                                        <div class="form-check ">
                                                            <input type="checkbox" name="tue"  class="form-check-input company" id="tueedit3" value="1">
                                                            <label class="active" class="form-check-label" for="tueedit3">
                                                                <span class="assign_class label{{getKeyid('tue',$data)}}" data-id="{{getKeyid('tue',$data) }}" data-value="{{checkKey('tue',$data) }}" >
                                                                    {!! checkKey('tue',$data) !!}
                                                                </span>
                                                            </label>
                                                        </div>
                                                    </li>
                                                    <li class="list-group-item">
                                                        <div class="form-check ">
                                                            <input type="checkbox" name="fri" class="form-check-input company" id="friedit1" value="1">
                                                            <label class="active" class="form-check-label" for="friedit1">
                                                                <span class="assign_class label{{getKeyid('fri',$data)}}" data-id="{{getKeyid('fri',$data) }}" data-value="{{checkKey('fri',$data) }}" >
                                                                    {!! checkKey('fri',$data) !!}
                                                                </span>
                                                            </label>
                                                        </div>
                                                    </li>
                                                    <li class="list-group-item">
                                                        <!-- Default checked -->
                                                        <div class="form-check ">
                                                            <input type="checkbox" name="bank" class="form-check-input company" id="bankedit1" value="1">
                                                            <label class="active" class="form-check-label" for="bankedit1">
                                                                <span class="assign_class label{{getKeyid('bank',$data)}}" data-id="{{getKeyid('bank',$data) }}" data-value="{{checkKey('bank',$data) }}" >
                                                                    {!! checkKey('bank',$data) !!}
                                                                </span>
                                                            </label>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col">
                                                <ul class="list-group list-group-flush">
                                                    <li class="list-group-item">
                                                        <!-- Default checked -->
                                                        <div class="form-check ">
                                                            <input type="checkbox" name="wed" class="form-check-input company" id="wededit1" value="1">
                                                            <label class="active" class="form-check-label" for="wededit1">
                                                                <span class="assign_class label{{getKeyid('wed',$data)}}" data-id="{{getKeyid('wed',$data) }}" data-value="{{checkKey('wed',$data) }}" >
                                                                    {!! checkKey('wed',$data) !!}
                                                                </span>
                                                            </label>
                                                        </div>


                                                    </li>
                                                    <li class="list-group-item">
                                                        <!-- Default checked -->
                                                        <div class="form-check ">
                                                            <input type="checkbox" name="sat" class="form-check-input company" id="satedit1" value="1">
                                                            <label class="active" class="form-check-label" for="satedit1">
                                                                <span class="assign_class label{{getKeyid('sat',$data)}}" data-id="{{getKeyid('sat',$data) }}" data-value="{{checkKey('sat',$data) }}" >
                                                                    {!! checkKey('sat',$data) !!}
                                                                </span>
                                                            </label>
                                                        </div>


                                                    </li>
                                                    <li class="list-group-item">
                                                        <!-- Default checked -->
                                                        <!-- <div class="form-check ">
                                                            <input type="checkbox" name="everyweek" class="form-check-input company" id="weekedit1" value="1">
                                                            <label class="active" class="form-check-label" for="weekedit1">Every Week</label>
                                                        </div> -->
                                                    </li>

                                                </ul>
                                            </div>
                                            <small id="frequency_error_msg_edit" class="text-danger error_message" style="top:90%;left: 5%"></small>
                                        </div>
                                    </div>
                                </div>
                                <br>
                            @endif
                            @if(Auth()->user()->hasPermissionTo('schedule_frequency_limit_edit') || Auth::user()->all_companies == 1 )
                                <div class="card">
                                    <h5 class="card-header card-sm info-color white-text  py-1s">
                                        <strong>
                                            <span class="assign_class label{{getKeyid('frequency_limit',$data)}}" data-id="{{getKeyid('frequency_limit',$data) }}" data-value="{{checkKey('frequency_limit',$data) }}" >
                                                {!! checkKey('frequency_limit',$data) !!}
                                            </span>
                                        </strong>
                                    </h5>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col">
                                                <div class="md-form">
                                                    <span>&nbsp;&nbsp;&nbsp;</span>
                                                    <input type="number" value="1" class="form-control" id="frequency_limit_edit" name="frequency_limit">
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="md-form">
                                                    <span>&nbsp;&nbsp;&nbsp;</span>
                                                    {!! Form::select('frequency_type',['4'=>'Week','5'=>'Month','6'=>'Year'], null, ['class' =>'mdb-select','id'=>'frequency_type','placeholder'=>'plese select']) !!}
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="md-form">
                                                    <span class="assign_class label{{getKeyid('max_display',$data)}}" data-id="{{getKeyid('max_display',$data) }}" data-value="{{checkKey('max_display',$data) }}" >
                                                            {!! checkKey('max_display',$data) !!}
                                                    </span>
                                                    <input type="number" name="max_display" required id="date-picker-example" class="max_display_edit form-control">
                                                    {{--                                                    <label for="date-picker-example">--}}
                                                    {{--                                                        <span class="assign_class label{{getKeyid('max_display',$data)}}" data-id="{{getKeyid('max_display',$data) }}" data-value="{{checkKey('max_display',$data) }}" >--}}
                                                    {{--                                                            {!! checkKey('max_display',$data) !!}--}}
                                                    {{--                                                        </span>--}}
                                                    {{--                                                    </label>--}}
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <br>
                            @endif
                            @if(Auth()->user()->hasPermissionTo('schedule_frequency_repeat_edit') || Auth::user()->all_companies == 1 )
                                <div class="card">
                                    <h5 class="card-header card-sm info-color white-text  py-1s">
                                        <strong>
                                            <span class="assign_class label{{getKeyid('frequency_repeat',$data)}}" data-id="{{getKeyid('frequency_repeat',$data) }}" data-value="{{checkKey('frequency_repeat',$data) }}" >
                                                {!! checkKey('frequency_repeat',$data) !!}
                                            </span>
                                        </strong>
                                    </h5>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col">
                                                <div class="md-form">
                                                    <span class=" assign_class label{{getKeyid('frequency_repeat',$data)}}" data-id="{{getKeyid('frequency_repeat',$data) }}" data-value="{{checkKey('frequency_repeat',$data) }}" >
                                                        {!! checkKey('frequency_repeat',$data) !!}
                                                    </span>
                                                    <input type="number" name="repeat" id="repeat" class="form-control ">
                                                    <!-- <label class="active" for="date-picker-example">
                                                        <strong>
                                                        </strong>
                                                    </label> -->
                                                </div>
                                            </div>

                                            <div class="col">
                                                <div class="md-form ">
                                                    <span class="assign_class label{{getKeyid('gap_mins',$data)}}" data-id="{{getKeyid('gap_mins',$data) }}" data-value="{{checkKey('gap_mins',$data) }}" >
                                                        {!! checkKey('gap_mins',$data) !!}
                                                    </span>
                                                    <input type="number" name="repeat_min_gap" id="repeat_min_gap"  class="form-control">
                                                    <!-- label class="active" for="date-picker-example">
                                                    </label> -->
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                                <br>
                            @endif
                            <div class="card">
                                <h5 class="card-header  info-color white-text  py-1">
                                    <strong>
                                        <span class="assign_class label{{getKeyid('task_details',$data)}}" data-id="{{getKeyid('task_details',$data) }}" data-value="{{checkKey('task_details',$data) }}" >
                                            {!! checkKey('task_details',$data) !!}
                                        </span>
                                    </strong>
                                </h5>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col">
                                            @if(Auth()->user()->hasPermissionTo('schedule_tag_swipe_edit') || Auth::user()->all_companies == 1 )
                                                <div class="md-form">
                                                    <span class="assign_class label{{getKeyid('tag_swipe_required',$data)}}" data-id="{{getKeyid('tag_swipe_required',$data) }}" data-value="{{checkKey('tag_swipe_required',$data) }}" >
                                                        {!! checkKey('tag_swipe_required',$data) !!}
                                                    </span>
                                                    <select searchable="Search here.."  class="mdb-select" id="tag_swipe_edit" name="tag_swipe">
                                                        <option value="" disabled>Please Select</option>
                                                        <option value="1">Yes</option>
                                                        <option value="2">NO</option>
                                                    </select>
                                                </div>
                                            @endif
                                        </div>

                                        <div class="col">
                                        @if(Auth()->user()->hasPermissionTo('schedule_signature_edit') || Auth::user()->all_companies == 1 )
                                            <!-- <div class="md-form mt-5">
                                                    <input type="number" id="signature" name="signature" class="form-control ">
                                                    <label class="active" for="date-picker-example">
                                                        <span class="assign_class label{{getKeyid('signature',$data)}}" data-id="{{getKeyid('signature',$data) }}" data-value="{{checkKey('signature',$data) }}" >
                                                            {!! checkKey('signature',$data) !!}
                                                </span>
                                            </label>
                                        </div> -->
                                            @endif
                                        </div>

                                    </div>

                                </div>
                            </div>

                        </form>
                        <!-- Form -->
                    </div>

                </div>

                <div class="modal-footer d-flex justify-content-center">
                    <button class="form_submit_check btn btn-primary btn-sm" id="edit_schedule" type="submit">
                        <span class="assign_class label{{getKeyid('update',$data)}}" data-id="{{getKeyid('update',$data) }}" data-value="{{checkKey('update',$data) }}">
                            {!! checkKey('update',$data) !!}
                        </span>
                    </button>
                </div>
            </form>
            <div class="modal-body mx-3"></div>
        </div>
    </div>
</div>
@component('backend.schedule.model.index')
@endcomponent
<script type="text/javascript">
    $('#input_starttime').pickatime({});
    $('.datepicker').pickadate({selectYears:250,});
</script>
