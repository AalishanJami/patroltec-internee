@php
    $data=localization();
@endphp
<link rel="stylesheet" href="{{ asset('custom/css/custom_style.css') }}">

<div class="modal model_tasktype fade" id="modaltaskTypeIndexpage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">
                    <span class="assign_class label{{getKeyid('task_type',$data)}}" data-id="{{getKeyid('task_type',$data) }}" data-value="{{checkKey('task_type',$data) }}" >
                        {!! checkKey('task_type',$data) !!}
                    </span>
                    @if(Auth()->user()->hasPermissionTo('view_cms') || Auth::user()->all_companies == 1 )
                        <button data-class="edit-disable" class="edit_cms_enable edit_cms_switch btn btn-primary btn-sm waves-effect waves-light" style="display: none;" >
                            Edit cms
                        </button>
                        <button  data-class="edit-enable" class="edit_cms_disable edit_cms_switch btn btn-danger btn-sm waves-effect waves-light">
                            Edit cms
                        </button>
                    @endif
                </h4>
                <button type="button"  class="task_listing_close close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="card">
                <input type="hidden" id="taskTypeflag">
                <div class="card-body">
                    <div id="table-tasktype" class="table-editable">
                        @if(Auth()->user()->hasPermissionTo('create_documentTaskType') || Auth::user()->all_companies == 1 )
                            <span class="add-tasktype table-add float-right mb-3 mr-2">
                            <a class="text-success"><i class="fas fa-plus fa-2x" aria-hidden="true"></i></a>
                        </span>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('delete_selected_documentTaskType') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-danger btn-sm" id="deleteselectedtasktype">
                                <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                                    {!! checkKey('selected_delete',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_active_documentTaskType') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-success btn-sm" style="display: none;" id="selectedactivebuttontasktype">
                                <span class="assign_class label{{getKeyid('selected_active',$data)}}" data-id="{{getKeyid('selected_active',$data) }}" data-value="{{checkKey('selected_active',$data) }}" >
                                    {!! checkKey('selected_active',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_restore_documentTaskType') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-primary btn-sm" id="restorebuttontasktype">
                                <span class="assign_class label{{getKeyid('restore',$data)}}" data-id="{{getKeyid('restore',$data) }}" data-value="{{checkKey('restore',$data) }}" >
                                    {!! checkKey('restore',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('active_documentTaskType') || Auth::user()->all_companies == 1 )
                            <button  style="display: none;" class="btn btn-primary btn-sm" id="activebuttontasktype">
                                <span class="assign_class label{{getKeyid('show_active',$data)}}" data-id="{{getKeyid('show_active',$data) }}" data-value="{{checkKey('show_active',$data) }}" >
                                    {!! checkKey('show_active',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('csv_documentTaskType') || Auth::user()->all_companies == 1 )
                            <form class="form-style task_export_from" method="POST" action="{{ url('tasktype/export/excel') }}">
                                <input type="hidden" id="export_excel_tasktype" class="task_export" name="excel_array" value="1">
                                {!! csrf_field() !!}
                                <button  type="submit" class="form_submit_check btn btn-warning btn-sm">
                                    <span class="assign_class label{{getKeyid('excel_export',$data)}}" data-id="{{getKeyid('excel_export',$data) }}" data-value="{{checkKey('excel_export',$data) }}" >
                                        {!! checkKey('excel_export',$data) !!}
                                    </span>
                                </button>
                            </form>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('word_documentTaskType') || Auth::user()->all_companies == 1 )
                            <form class="form-style task_export_from" method="POST" action="{{ url('tasktype/export/word') }}">
                                <input type="hidden" id="export_world_tasktype" class="task_export" name="word_array" value="1">
                                {!! csrf_field() !!}
                                <button  type="submit" class="form_submit_check btn btn-success btn-sm">
                                    <span class="assign_class label{{getKeyid('word_export',$data)}}" data-id="{{getKeyid('word_export',$data) }}" data-value="{{checkKey('word_export',$data) }}" >
                                        {!! checkKey('word_export',$data) !!}
                                    </span>
                                </button>
                            </form>
                        @endif

                        @if(Auth()->user()->hasPermissionTo('pdf_documentTaskType') || Auth::user()->all_companies == 1 )
                            <form  class="form-style task_export_from" {{pdf_view('task_types')}} method="POST" action="{{ url('tasktype/export/pdf') }}">
                                <input type="hidden" id="export_pdf_tasktype" class="task_export" name="pdf_array" value="1">
                                {!! csrf_field() !!}
                                <button  type="submit" class="form_submit_check btn btn-secondary btn-sm">
                                    <span class="assign_class label{{getKeyid('pdf_export',$data)}}" data-id="{{getKeyid('pdf_export',$data) }}" data-value="{{checkKey('pdf_export',$data) }}" >
                                        {!! checkKey('pdf_export',$data) !!}
                                    </span>
                                </button>
                            </form>
                        @endif
                        <div id="table" class="table-editable">
                            <table id="tasktype_table_model" class=" table table-bordered table-responsive-md table-striped">
                                <thead>
                                <tr>
                                    <th class="no-sort all_checkboxes_style">
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input tasktype_checked" id="tasktype_checkbox_all">
                                            <label class="form-check-label" for="tasktype_checkbox_all">
                                                <span class="assign_class label{{getKeyid('all',$data)}}" data-id="{{getKeyid('all',$data) }}" data-value="{{checkKey('all',$data) }}" >
                                                    {!! checkKey('all',$data) !!}
                                                </span>
                                            </label>
                                        </div>
                                    </th>
                                    <th>
                                        <span class="assign_class label{{getKeyid('name',$data)}}" data-id="{{getKeyid('name',$data) }}" data-value="{{checkKey('name',$data) }}" >
                                            {!! checkKey('name',$data) !!}
                                        </span>
                                    </th>
{{--                                    <th id="action" class="no-sort all_action_btn"></th>--}}
                                    <th id="action" class="no-sort all_action_btn"></th>
                                </tr>
                                </thead>
                                <tbody id="tasktype_data"></tbody>
                            </table>
                        </div>
                        @if(Auth()->user()->all_companies == 1)
                            <input type="hidden" id="documentTaskType_checkbox" value="1">
                            <input type="hidden" id="documentTaskType_name" value="1">
                            <input type="hidden" id="documentTaskType_name_create" value="1">
                            <input type="hidden" id="delete_documentTaskType" value="1">
                            <input type="hidden" id="documentTaskType_name_edit" value="1">
                        @else
                            <input type="hidden" id="documentTaskType_checkbox" value="{{Auth()->user()->hasPermissionTo('documentTaskType_checkbox')}}">
                            <input type="hidden" id="documentTaskType_name" value="{{Auth()->user()->hasPermissionTo('documentTaskType_name')}}">
                            <input type="hidden" id="delete_documentTaskType" value="{{Auth()->user()->hasPermissionTo('delete_documentTaskType')}}">
                            <input type="hidden" id="documentTaskType_name_create" value="{{Auth()->user()->hasPermissionTo('documentTaskType_name_create')}}">
                            <input type="hidden" id="documentTaskType_name_edit" value="{{Auth()->user()->hasPermissionTo('documentTaskType_name_edit')}}">
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    function datatable_intialize_task_type() {
        // $('#tasktype_table_model').dataTable({
        //     processing: true,
        //     language: {
        //         'lengthMenu': '_MENU_',
        //         'info': ' ',
        //     },
        //     columnDefs: [ {
        //         'targets': [0,1], /* column index */
        //         'orderable': false, /* true or false */
        //     }],
        // });
    }
    $(document).ready(function () {

        var url='/tasktype/getall';
        tasktype_data(url);
    });
    function tasktype_data(url) {
        $('#tasktype_table_model').DataTable().clear().destroy();
        var table = $('#tasktype_table_model').dataTable({
            columnDefs: [ {
                'targets': [0,1], /* column index */
                'orderable': false, /* true or false */
            }],
            processing: true,
            language: {
                'lengthMenu': '  _MENU_ ',
                'search':'<span class="assign_class label'+$('#search_label').attr('data-id')+'" data-id="'+$('#search_label').attr('data-id')+'" data-value="'+$('#search_label').val()+'" >'+$('#search_label').val()+'</span>',
                'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                'info':'',
                'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                'paginate': {
                    'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                    'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                },
                'infoFiltered': "(filtered from _MAX_ total records)"
            },
            "ajax": {
                "url": url,
                "type": 'get',
            },
            "createdRow": function( row, data, dataIndex,columns ) {
                var checkbox='<div class="form-check"><input type="checkbox" class="form-check-input selectedrowtasktype" id="tasktype'+data['id']+'"><label class="form-check-label" for="tasktype'+data['id']+'""></label></div></td>';
                $(row).attr('id', 'tasktype_tr'+data['id']);
                $(row).attr('data-id',data['id']);
                $(columns[0]).html(checkbox);
                if (data['flag']=='1'){
                    if ($("#documentTaskType_name_edit").val()=='1'){
                        $(columns[1]).attr('Contenteditable', 'true');
                        $(columns[1]).attr('class','edit_inline_tasktype');
                        $(columns[1]).attr('data-id',data['id']);
                        $(columns[1]).attr('onkeydown', 'editSelectedRow(tasktype_tr'+data['id']+')');
                    }
                }
                var submit='';
                submit+='<a type="button" class="btn btn-primary btn-xs my-0 waves-effect all_action_btn_margin waves-light savetasktypedata show_tick_btn'+data['id']+'" style="display: none;" id="'+data.id+'"><i class="fas fa-check"></i></a>';
                $(columns[1]).attr('id','nameD'+data['id']);
                $(columns[2]).append(submit);
            },
            columns: [
                {data: 'checkbox', name: 'checkbox',visible:$('#documentTaskType_checkbox').val()},
                {data: 'name', name: 'name',visible:$('#documentTaskType_name').val()},
                {data: 'actions', name: 'actions'},
                // {data: 'submit', name: 'submit'},
            ],
        });
        if ($(":checkbox").prop('checked',true)){
            $(":checkbox").prop('checked',false);
        }
    }

</script>
