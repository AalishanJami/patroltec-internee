<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
<table>
    <thead>
    <tr>
        @if(Auth()->user()->hasPermissionTo('schedule_projectname_excel_export') || Auth::user()->all_companies == 1 )
            <th> Location Name</th>
        @else
            <th></th>
        @endif
        @if(Auth()->user()->hasPermissionTo('schedule_startdate_excel_export') || Auth::user()->all_companies == 1 )
            <th> Start Date</th>
        @else
            <th></th>
        @endif
        @if(Auth()->user()->hasPermissionTo('schedule_time_excel_export') || Auth::user()->all_companies == 1 )
            <th> Time</th>
        @else
            <th></th>
        @endif
    </tr>
    </thead>
    <tbody>
    @foreach($data as $value)
        <tr>
            @if(Auth()->user()->hasPermissionTo('schedule_projectname_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{ $value->scheduleTaskLocation->location->name }}</td>
            @else
                <th></th>
            @endif
            @if(Auth()->user()->hasPermissionTo('schedule_startdate_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{ $value->start_date }}</td>
            @else
                <th></th>
            @endif
            @if(Auth()->user()->hasPermissionTo('schedule_time_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{ $value->time }}</td>
            @else
                <th></th>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>
</body>
</html>
