@extends('backend.layouts.doc')
@section('title', 'Schedule')
@section('content')
    @php
        $data=localization();
    @endphp
    <link rel="stylesheet" href="{{ asset('custom/css/custom_style.css') }}">
    @include('backend.layouts.doc_sidebar')
    <div class="padding-left-custom remove-padding">
        {!! breadcrumInner('schedule',Session::get('form_id'),'forms','/document/detail/'.Session::get('form_id'),'documents','/form_group') !!}
        <div class="locations-form container-fluid form_body">
            <div class="card">
                <div class="card-body">
                    <div id="table-schedule" class="table-editable">
                        @if(Auth()->user()->hasPermissionTo('create_schedule') || Auth::user()->all_companies == 1 )
                            <span class="schedule-table table-add float-right mb-3 mr-2"><a class="text-success modalSchedule_btn" id="add_schedule_btn"  data-toggle="modal" data-target="#modalSchedule" ><i class="fas fa-plus fa-2x" aria-hidden="true"></i></a></span>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_delete_schedule') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-danger btn-sm" id="deleteselectedschedule">
                                <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                                    {!! checkKey('selected_delete',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_delete_schedule') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-danger btn-sm" style="display: none;" id="deleteselectedscheduleSoft">
                                <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                                    {!! checkKey('selected_delete',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_active_schedule') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-success btn-sm" style="display: none;" id="selectedactivebuttonschedule">
                                <span class="assign_class label{{getKeyid('selected_active',$data)}}" data-id="{{getKeyid('selected_active',$data) }}" data-value="{{checkKey('selected_active',$data) }}" >
                                    {!! checkKey('selected_active',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('restore_delete_schedule') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-primary btn-sm" id="restorebuttonschedule">
                                <span class="assign_class label{{getKeyid('restore',$data)}}" data-id="{{getKeyid('restore',$data) }}" data-value="{{checkKey('restore',$data) }}" >
                                    {!! checkKey('restore',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('active_schedule') || Auth::user()->all_companies == 1 )
                            <button  style="display: none;" class="btn btn-primary btn-sm" id="activebuttonschedule">
                                <span class="assign_class label{{getKeyid('show_active',$data)}}" data-id="{{getKeyid('show_active',$data) }}" data-value="{{checkKey('show_active',$data) }}" >
                                    {!! checkKey('show_active',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('csv_schedule') || Auth::user()->all_companies == 1 )
                            <form class="form-style" id="export_excel_schedule" method="POST" action="{{ url('schedule/export/excel') }}">
                                <input type="hidden" class="schedule_export" name="excel_array" value="1">
                                {!! csrf_field() !!}
                                <button  type="submit" class="form_submit_check btn btn-warning btn-sm">
                                    <span class="assign_class label{{getKeyid('excel_export',$data)}}" data-id="{{getKeyid('excel_export',$data) }}" data-value="{{checkKey('excel_export',$data) }}" >
                                        {!! checkKey('excel_export',$data) !!}
                                    </span>
                                </button>
                            </form>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('word_schedule') || Auth::user()->all_companies == 1 )
                            <form class="form-style" id="export_world_schedule" method="POST" action="{{ url('schedule/export/world') }}">
                                <input type="hidden" class="schedule_export" name="word_array" value="1">
                                {!! csrf_field() !!}
                                <button  type="submit" class="form_submit_check btn btn-success btn-sm">
                                   <span class="assign_class label{{getKeyid('word_export',$data)}}" data-id="{{getKeyid('word_export',$data) }}" data-value="{{checkKey('word_export',$data) }}" >
                                        {!! checkKey('word_export',$data) !!}
                                    </span>
                                </button>
                            </form>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('pdf_schedule') || Auth::user()->all_companies == 1 )
                            <form  class="form-style" {{pdf_view('schedule_tasks')}}  id="export_pdf_schedule" method="POST" action="{{ url('schedule/export/pdf') }}">
                                <input type="hidden" class="schedule_export" name="pdf_array" value="1">
                                {!! csrf_field() !!}
                                <button  type="submit" class="form_submit_check btn btn-secondary btn-sm">
                                   <span class="assign_class label{{getKeyid('pdf_export',$data)}}" data-id="{{getKeyid('pdf_export',$data) }}" data-value="{{checkKey('pdf_export',$data) }}" >
                                        {!! checkKey('pdf_export',$data) !!}
                                    </span>
                                </button>
                            </form>
                        @endif
                        <div id="table" class="table-editable">
                            <table id="schedule_table_static" class="table table-bordered table-responsive-md table-striped">
                                <thead>
                                <tr>
                                    <th class="no-sort all_checkboxes_style">
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input schedule_checked" id="schedule_checkbox_all">
                                            <label class="form-check-label" for="schedule_checkbox_all">
                                                <span class="assign_class label{{getKeyid('all',$data)}}" data-id="{{getKeyid('all',$data) }}" data-value="{{checkKey('all',$data) }}" >
                                                    {!! checkKey('all',$data) !!}
                                                </span>
                                            </label>
                                        </div>
                                    </th>
                                    <th>
                                        <span class="assign_class label{{getKeyid('location_name',$data)}}" data-id="{{getKeyid('location_name',$data) }}" data-value="{{checkKey('location_name',$data) }}" >
                                            {!! checkKey('location_name',$data) !!}
                                        </span>
                                    </th>
                                    <th>
                                        <span class="assign_class label{{getKeyid('start_date',$data)}}" data-id="{{getKeyid('start_date',$data) }}" data-value="{{checkKey('start_date',$data) }}" >
                                            {!! checkKey('start_date',$data) !!}
                                        </span>
                                    </th>
                                    <th>
                                        <span class="assign_class label{{getKeyid('time',$data)}}" data-id="{{getKeyid('time',$data) }}" data-value="{{checkKey('time',$data) }}" >
                                            {!! checkKey('time',$data) !!}
                                        </span>
                                    </th>
                                    <th>
                                        <span class="assign_class label{{getKeyid('summary',$data)}}" data-id="{{getKeyid('summary',$data) }}" data-value="{{checkKey('summary',$data) }}" >
                                            {!! checkKey('summary',$data) !!}
                                        </span>
                                    </th>
                                    <th class="no-sort all_action_btn" id="action"></th>
                                </tr>
                                </thead>
                                <tbody id="schedule_data">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('backend.label.input_label')
        @include('backend.schedule.model.edit')
        @include('backend.schedule.model.create')
        @include('backend.schedule.input_permission')
    </div>
    <script src="{{ asset('custom/js/tasktype.js') }}"></script>
    <script src="{{ asset('custom/js/schedule.js') }}"></script>
    <script type="text/javascript">

        $(document).ready(function() {

            $('.task_type_model').click(function(e) {
                if ($('.edit_cms_disable').css('display') == 'none') {
                    return 0;
                }
                $('#modaltaskTypeIndexpage').modal('show');
            });
            $('.task_type_btn').click(function(){
                var url='/tasktype/getall';
                tasktype_data(url);
            });

            $(function () {
                ScheduleAppend();
            });
            // (function ($) {
            //     var active ='<span class="assign_class label'+$('#breadcrumb_schedule').attr('data-id')+'" data-id="'+$('#breadcrumb_schedule').attr('data-id')+'" data-value="'+$('#breadcrumb_schedule').val()+'" >'+$('#breadcrumb_schedule').val()+'</span>';
            //     $('.breadcrumb-item.active').html(active);
            //     var parent ='<span class="assign_class label'+$('#dashboard_label').attr('data-id')+'" data-id="'+$('#dashboard_label').attr('data-id')+'" data-value="'+$('#dashboard_label').val()+'" >'+$('#dashboard_label').val()+'</span>';
            //     $('.breadcrumb-item a').html(parent);
            // }(jQuery));

        });
        function ScheduleAppend()
        {
            var table = $('#schedule_table_static').dataTable({
                columnDefs: [ {
                    'targets': [0,5], /* column index */
                    'orderable': false, /* true or false */
                }],
                processing: true,
                language: {
                    'lengthMenu': '  _MENU_ ',
                    'search':'<span class="assign_class label'+$('#search_label').attr('data-id')+'" data-id="'+$('#search_label').attr('data-id')+'" data-value="'+$('#search_label').val()+'" >'+$('#search_label').val()+'</span>',
                    'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                    'info':'',
                    'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                    'paginate': {
                        'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                        'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                    },
                    'infoFiltered': "(filtered from _MAX_ total records)"
                },
                "ajax": {
                    "url": '/schedule/getall',
                    "type": 'get',
                },
                "createdRow": function( row, data, dataIndex ) {

                    var checkbox_permission=$('#checkbox_permission').val();
                    $(row).attr('id', 'schedule_tr'+data['id']);
                    var temp=data['id'];
                    $(row).attr('onclick',"selectedrowschedule(this.id)");
                    $(row).attr('data-id', data['id']);
                },
                columns: [
                    {data: 'checkbox', name: 'checkbox',visible:$('#schedule_checkbox').val()},
                    {data: 'project_name', name: 'project_name',visible:$('#schedule_projectname').val()},
                    {data: 'start_date', name: 'start_date',visible:$('#schedule_startdate').val()},
                    // {data: 'location_name', name: 'location_name'},
                    {data: 'time', name: 'time',visible:$('#schedule_time').val()},
                    {data: 'summery', name: 'summery'},
                    {data: 'actions', name: 'actions'},
                ],

            });
            if ($(".selectedrowschedule").prop('checked',true)){
                $(".selectedrowschedule").prop('checked',false);
            }
            if ($("#schedule_checkbox_all").prop('checked',true)){
                $("#schedule_checkbox_all").prop('checked',false);
            }
        }


        function ScheduleSoftAppend()
        {
            var table = $('#schedule_table_static').dataTable({
                processing: true,
                language: {
                    'lengthMenu': '  _MENU_ ',
                    'search':'<span class="assign_class label'+$('#search_label').attr('data-id')+'" data-id="'+$('#search_label').attr('data-id')+'" data-value="'+$('#search_label').val()+'" >'+$('#search_label').val()+'</span>',
                    'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                    'info': '<span class="assign_class label'+$('#showing_page_label').attr('data-id')+'" data-id="'+$('#showing_page_label').attr('data-id')+'" data-value="'+$('#showing_page_label').val()+'" >'+$('#showing_page_label').val()+'</span> _PAGE_ <span class="assign_class label'+$('#of_label').attr('data-id')+'" data-id="'+$('#of_label').attr('data-id')+'" data-value="'+$('#of_label').val()+'" >'+$('#of_label').val()+'</span> _PAGES_',
                    'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                    'paginate': {
                        'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                        'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                    },
                    'infoFiltered': "(filtered from _MAX_ total records)"
                },
                "ajax": {
                    "url": '/schedule/getallSoft',
                    "type": 'get',
                },
                "createdRow": function( row, data, dataIndex ) {
                    var checkbox_permission=$('#checkbox_permission').val();
                    $(row).attr('id', 'schedule_tr'+data['id']);
                    var temp=data['id'];
                    $(row).attr('onclick',"selectedrowschedule(this.id)");
                },
                columns: [
                    {data: 'checkbox', name: 'checkbox',visible:$('#schedule_checkbox').val()},
                    {data: 'project_name', name: 'project_name',visible:$('#schedule_projectname').val()},
                    {data: 'start_date', name: 'start_date',visible:$('#schedule_startdate').val()},
                    // {data: 'location_name', name: 'location_name'},
                    {data: 'time', name: 'time',visible:$('#schedule_time').val()},
                    {data: 'summery', name: 'summery'},
                    {data: 'actions', name: 'actions'},
                ],
            });
            if ($(".selectedrowschedule").prop('checked',true)){
                $(".selectedrowschedule").prop('checked',false);
            }
            if ($("#schedule_checkbox_all").prop('checked',true)){
                $("#schedule_checkbox_all").prop('checked',false);
            }
        }
    </script>
@endsection
