<p>Dear {{$name}}<p>
<br>
<p>You have been given access to {{$company_name}} to view {{$will_user_name}} will. Please login to {{$login}} and download the documents<p>
<br>
<p>User Name: {{$user_name}}</p>
<p>Password: {{$password}}</p>
<br>
The above details give you access to the forms created by {{$will_user_name}}, if you wish to create your own documents Signup at {{$register}} for an account. 
<br>
<p>Kind Regards</p>
<p>{{$company_name}} Support</p>