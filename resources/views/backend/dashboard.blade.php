@extends('backend.layouts.backend')
@section('title', 'Dashboard')
@php
  $data=localization();
@endphp
<style type="text/css">
.h-100 {
  height: unset !important;
}
.progress, .progress .progress-bar {
  height: 6px!important;
}
main {
  height: unset !important;
}
.text-purple
{
  color:  #9400d3!important;
}
.far{
  font-family: 'Roboto'!important;
}
</style>
@section('content')
<div class="row">
  <div class="col-md-12">
    <section class="mt-md-4 pt-md-2 mb-5 pb-4">
      <div class="row">
        @if(!$formCounts->isEmpty())
          @foreach($formCounts as $form)
            @if($form[0]->formType->_show_dashboard)
              <div class="col-xl-3 col-md-6 mb-xl-0 mb-4">
                <div class="card card-cascade cascading-admin-card">
                  <div class="admin-up">

                      <p class="font-weight-bold far primary-color mr-3 z-depth-2 ">
                      {{round(($form->sum('total_completed')/$form->sum('total'))*100)}}%</p>

                    <div class="data">
                      <p class="font-weight-bold dark-grey-text">
                        {{$form[0]->formType->name}}
                      </p>
                      <h4 class="font-weight-bold dark-grey-text">
                        <a href="{{url('/completed_forms/report').'/'.$form[0]->formType->id}}">
                          {{$form->sum('total_completed')}}
                        </a>/
                        <a href="{{url('/job').'/'.$form[0]->formType->id}}">
                          {{$form->sum('total')}}
                        </a>
                      </h4>
                    </div>
                  </div>
                  <div class="card-body card-body-cascade">
                    <div class="progress mb-3">
                      <div class="progress-bar bg-primary" role="progressbar" style="width: {{($form->sum('total_completed')/$form->sum('total'))*100}}%; background-color:rgb({{255-round(($form->sum('total_completed')/$form->sum('total'))*150)}},{{0+round(($form->sum('total_completed')/$form->sum('total'))*200)}},0)!important;" aria-valuenow="25"
                              aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <p class="card-text"></p>
                  </div>
                </div>
              </div>
            @endif
          @endforeach
        @endif
      </div>
    </section>
    <section class="mb-5">
      <div class="card card-cascade narrower">
        <section>
          <div class="row">
            <div class="col-xl-5 col-lg-12 mr-0 pb-2">
              <div class="view view-cascade gradient-card-header light-blue lighten-1">
                <h2 class="h2-responsive mb-0 font-weight-500">
                  Score
                </h2>
              </div>
              <div class="card-body card-body-cascade pb-0">
                <div class="row py-3 pl-4">
                  <div class="col-md-6">
                    <form id="LocationSelect">
                      <p class="lead">
                        <span class="badge info-color p-2">Site List</span>
                      </p>
                      <div class="row">
                        <div class="col-md-12">
                          {!! Form::select('location_id',$locations,null, ['class' =>'mdb-select','id'=>'location_id','searchable'=>'search here','multiple']) !!}
                        </div>
                      </div>
                      <p class="lead pt-3 pb-4">
                        <span class="badge info-color p-2">
                          Choose Date
                        </span>
                      </p>
                      <div class="md-form">
                        <input placeholder="Selected date" type="text" name="from" id="from" class="form-control datepicker">
                        <label for="from" class="active">
                          From
                        </label>
                      </div>
                      <div class="md-form">
                        <input placeholder="Selected date" name="to" type="text" id="to" class="form-control datepicker">
                        <label for="to" class="active">To</label>
                      </div>

                      <p class="lead pt-3 pb-4">
                        <span class="badge info-color" onclick="getDataLocationMain()">
                         Submit<i class="fas fa-check-circle ml-1"></i>
                        </span>
                      </p>

                    </form>
                  </div>
                  <div class="col-md-6 text-center pl-xl-2 my-md-0 my-3">
                    <p>Top Score:
                      <strong id="topScore">{{$maxScore}}</strong>
                      <button type="button" class="btn btn-info btn-sm p-2" data-toggle="tooltip" data-placement="top"
                          title="Total scores in the given period">
                        <i class="fas fa-question"></i>
                      </button>
                    </p>
                    <p>Average Score:
                      <strong id="avgScore">{{$averageScore}}</strong>
                      <button type="button" class="btn btn-info btn-sm p-2 mr-0" data-toggle="tooltip" data-placement="top" title="Average daily scores in the given period">
                        <i class="fas fa-question"></i>
                      </button>
                    </p>
                    <span class="min-chart my-4" id="chart-sales" data-percent="0">
                      <span class="percent"></span>
                    </span>
                    <h5>
                      <span class="badge red accent-2 p-2" id="prc">Change

                      </span>
                      <button type="button" class="btn btn-info btn-sm p-2" data-toggle="tooltip" data-placement="top" title="Percentage change compared to the same period in the past">
                        <i class="fas fa-question"></i>
                      </button>
                    </h5>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xl-7 col-lg-12 mb-4 pb-2">
              <div class="view view-cascade gradient-card-header blue-gradient" style="height: 500px !important">
                {!! $chart->container() !!}
              </div>
            </div>
          </div>
        </section>
        <section>
          <div class="table-ui p-2 mb-3 mx-4 mb-5">
            <div class="row">
              <div class="col-xl-3 col-md-6">
              </div>
              <div class="col-xl-3 col-md-6">
              </div>
              <div class="col-xl-3 col-md-6">
              </div>
              <div class="col-xl-3 col-md-6">
                <form class="form-inline mt-2 ml-2">
                  <div class="form-group md-form mt-2" >
                    <input class="form-control w-100" type="text" placeholder="Search">
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div class="card card-cascade narrower z-depth-0">
            <div
              class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-left">
              <div>
                <!-- <button type="button" class="btn btn-outline-white btn-rounded btn-sm px-2"><i
                    class="fas fa-th-large mt-0"></i></button><button type="button" class="btn btn-outline-white btn-rounded btn-sm px-2"><i
                    class="fas fa-columns mt-0"></i></button> -->
              </div>
              <a href="" class="white-text mx-3">Forms Completed</a>
              <div>
                <button type="button" class="btn btn-outline-white btn-rounded btn-sm px-2">Export</button>
                <!--  <button type="button" class="btn btn-outline-white btn-rounded btn-sm px-2"><i
                    class="fas fa-eraser mt-0"></i></button><button type="button" class="btn btn-outline-white btn-rounded btn-sm px-2"><i
                    class="fas fa-info-circle mt-0"></i></button> -->
              </div>
            </div>
            <div class="px-4">
              <div class="table-responsive">
                <!--Table-->
                <table id="complete_table"  class="table table-striped table-bordered" cellspacing="0" width="100%">
                  <!-- Table head -->
                  <thead>
                    <tr>
                      <th></th>
                      <th></th>
                      <th class="th-lg">
                        <a>Form Name
                          <i class="fas "></i>
                        </a>
                      </th>
                      <th class="th-lg">
                        <a>Employee Name
                          <i class="fas "></i>
                        </a>
                      </th>
                      <th class="th-lg">
                        <a>Location Name
                          <i class="fas "></i>
                        </a>
                      </th>
                      <th class="th-lg">
                        <a>Location BreakDown
                          <i class="fas "></i>
                        </a>
                      </th>
                      <th class="th-lg">
                        <a>Date n Time
                          <i class="fas "></i>
                        </a>
                      </th>
                      <th class="th-lg">
                        <a>Answer Score
                          <i class="fas "></i>
                        </a>
                      </th>
                      <th class="th-lg">
                        <a>Form Type Score
                          <i class="fas "></i>
                        </a>
                      </th>
                      <th class="th-lg">
                        <a>Form Score
                          <i class="fas "></i>
                        </a>
                      </th>
                    </tr>
                  </thead>
                  <!-- Table head -->
                </table>
                <!-- Table -->
              </div>
              <hr class="my-0">
              </div>
            </div>
        </section>
      </div>
    </section>
    <section class="mb-5">
      <div class="row">
        <div class="col-lg-6 col-md-6">
          <div class="card mb-4">
            <div class="card-header white-text primary-color">
              <h5 class="font-weight-500 my-1">Location Wise Scoring</h5>
            </div>
            <div class="card-body">
              <table class="table cs_table" id="score_table">
                <tbody>
                  @foreach($formLocations as $form)
                    <tr onclick="detail({{$form->id}})" id="{{$form->id}}">
                      <td class="border-top-0">{{$form->location->name}}</td>
                      <td class="border-top-0">{{$mnt[($form->month)-1]}}</td>
                      <td class="border-top-0 text-primary">Bonus: {{$form->bonus_score}}</td>
                      <td class="border-top-0 hour">
                        <span class=" float-right">
                          <i class="far fa-clock-o" aria-hidden="true"></i>
                        @if($form->max_score>0)
                         {{round(($form->score/$form->max_score)*100)}}% (
                          <span class="text-primary">{{$form->score}}</span>/
                          <span class="text-purple">{{$form->max_score}}</span>)
                         @endif

                        </span>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 ">
          <div class="card mb-4">
            <div class="card-header white-text primary-color">
              <h5 class="font-weight-500 my-1" id="head_score">Details</h5>
            </div>
            <div class="card-body">
              <div class="row my-2 pt-1">
                <div class="col-4">
                  <small class="grey-text">Form Score</small>
                  <h4 id="form_score" class="text-primary"></h4>
                </div>
                <div class="col-4">
                  <small class="grey-text">Max Form Score</small>
                  <h4 id="max_form_score" class="text-purple"></h4>
                </div>
                <div class="col-4">
                  <small class="grey-text">Answer Score</small>
                  <h4 id="answer_score" class="text-primary"></h4>
                </div>
              </div>
              <div class="row mb-2">
                <div class="col-4">
                  <small class="grey-text">Max Answer Score</small>
                  <h4 id="max_anwser_score" class="text-purple"></h4>
                </div>
                <div class="col-4">
                  <small class="grey-text">Required</small>
                  <h4 id="required"></h4>
                </div>
                <div class="col-4">
                  <small class="grey-text">Completed</small>
                  <h4 id="completed"></h4>
                </div>
              </div>
              <div class="row mb-3">
                <div class="col-4">
                  <small class="grey-text">Outstandin</small>
                  <h4 id="os"></h4>
                </div>
                <div class="col-4">
                  <small class="grey-text">Late</small>
                  <h4 id="late"></h4>
                </div>
                <div class="col-4">
                  <small class="grey-text">Action Score</small>
                  <h4 id="action_score" class="text-primary"></h4>
                </div>
              </div>
              <div class="row mb-3">
                <div class="col-4">
                  <small class="grey-text">Action Max Score</small>
                  <h4 id="action_max_score" class="text-purple"></h4>
                </div>
                <div class="col-4">
                  <small class="grey-text">Outstandin</small>
                  <h4 id="os_2"></h4>
                </div>
                <div class="col-4">
                  <small class="grey-text">Resolved</small>
                  <h4 id="resolved"></h4>
                </div>
              </div>
              <div id="location_button">
                <button class="btn btn btn-flat grey lighten-3 btn-rounded waves-effect font-weight-bold dark-grey-text float-right btn-dash">
                  View full report
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    {!! Form::select('filter_id',$filter,null, ['class' =>'mdb-select colorful-select dropdown-primary md-form col-md-3','id'=>'filter_id','searchable'=>'search here']) !!}
    <section class="mb-5">
      <div class="row">
        <div class="col-lg-6 col-md-6">
          <div class="card">
            <a href="{{url('completed/forms')}}">
              <div class="card-header white-text primary-color ">
                <span class="type_name"> Accidents</span> by Current Months(week)
              </div>
            </a>
            <div class="card-body"style="height: 300px !important">
              {!! $chart_week->container() !!}
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 ">
          <div class="card">
            <a href="{{url('completed/forms')}}">
              <div class="card-header white-text primary-color">
                <span class="type_name"> Accidents</span> By Months
              </div>
            </a>
            <div class="card-body"style="height: 300px !important">
              {!! $chart_month->container() !!}
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="mb-5">
      <div class="row">
        <div class="col-lg-6 col-md-6 ">
          <div class="card">
            <a href="{{url('completed/forms')}}">
              <div class="card-header white-text primary-color">
                <span class="type_name"> Accidents</span> Per Day
              </div>
            </a>
            <div class="card-body"style="height: 300px !important">
              {!! $chart_day->container() !!}
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-md-6">
          <div class="card">
            <a href="{{url('completed/forms')}}">
              <div class="card-header white-text primary-color">
                Observations for
                <span class="type_name"></span>
                last six Months
              </div>
            </a>
            <div class="card-body"style="height: 300px !important">
              {!! $chart_lm->container() !!}
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="mb-5">
      <div class="row">
        <div class="col-lg-6 col-md-6 ">
          <div class="card">
            <a href="{{url('completed/forms')}}">
              <div class="card-header white-text primary-color">
                Completed Late jobs for <span class="type_name"></span> the last Year
              </div>
            </a>
            <div class="card-body"style="height: 300px !important">
              {!! $chart_late->container() !!}
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 ">
          <div class="card">
            <a href="{{url('completed/forms')}}">
              <div class="card-header white-text primary-color">
                Outstanding Jobs for <span class="type_name"></span> the last Years
              </div>
            </a>
            <div class="card-body"style="height: 300px !important">
              {!! $chart_out->container() !!}
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="pb-5">
      <div class="row">
        <div class="col-xl-3 col-md-6 mb-xl-0 mb-4">
          <div class="card classic-admin-card primary-color" style="background-color: green!important">
            <div class="card-body">
                <h6 class="white-text " style="color: white">GREEN OBSERVATIONS</h6>

              <h4 class="check">{{$count_positive}}</h4>
            </div>
            @if($count_positive>0 && $count_negative>0)
              <div class="progress">
                <div class="progress-bar bg grey darken-3" role="progressbar" style="width: {{round(($count_positive/($count_positive+$count_negative))*100,2)}}%;" aria-valuenow="25"
                aria-valuemin="0" aria-valuemax="100"></div>
              </div>
              <div class="card-body">
                <p>{{round(($count_positive/($count_positive+$count_negative))*100,2)}}%</p>
              </div>
            @else
              <div class="progress">
                <div class="progress-bar bg grey darken-3" role="progressbar" style="width:0%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
              <div class="card-body">
                <p>0%</p>
              </div>
            @endif
          </div>
        </div>
        <div class="col-xl-3 col-md-6 mb-xl-0 mb-4">
          <div class="card classic-admin-card warning-color"style="background-color: red!important">
            <div class="card-body">

                <h6 class="white-text" style="color: white">RED OBSERVATION</h6>

              <h4 class="check">{{$count_negative}}</h4>
            </div>
            @if($count_positive>0 || $count_negative>0)
              <div class="progress">
                <div class="progress-bar bg grey darken-3" role="progressbar" style="width: {{round(($count_negative/($count_positive+$count_negative))*100,2)}}%;" aria-valuenow="25"
                aria-valuemin="0" aria-valuemax="100"></div>
              </div>
              <div class="card-body">
                <p>{{round(($count_negative/($count_positive+$count_negative))*100,2)}}%</p>
              </div>
            @else
              <div class="progress">
                <div class="progress-bar bg grey darken-3" role="progressbar" style="width: 0%;" aria-valuenow="25"
                aria-valuemin="0" aria-valuemax="100"></div>
              </div>
              <div class="card-body">
                <p>0%</p>
              </div>
            @endif
          </div>
        </div>
        <div class="col-xl-3 col-md-6 mb-md-0 mb-4">
          <div class="card classic-admin-card light-blue lighten-1">
            <div class="card-body">

                <h6 class="white-text " style="color: white">OUTSTANDING ACTIONS</h6>

              <h4 class="check">{{$action_resolved}}</h4>
            </div>
            @if($action_created>0)
              <div class="progress">
                <div class="progress-bar bg grey darken-4" role="progressbar" style="width: {{round(($action_resolved/$action_created)*100,2)}}%;" aria-valuenow="75"
                aria-valuemin="0" aria-valuemax="100"></div>
              </div>
              <div class="card-body">
                <p>{{round(($action_resolved/$action_created)*100,2)}}%</p>
              </div>
            @else
              <div class="progress">
                <div class="progress-bar bg grey darken-4" role="progressbar" style="width: 0%;" aria-valuenow="75"
                aria-valuemin="0" aria-valuemax="100"></div>
              </div>
              <div class="card-body">
                <p>0%</p>
              </div>
            @endif
          </div>
        </div>
        <div class="col-xl-3 col-md-6 mb-0">
          <div class="card classic-admin-card red accent-2" style="background-color: purple!important">
            <div class="card-body">

                <h6 class="white-text " style="color: white">Late Completion</h6>

              <h4 class="check">{{$completed}}</h4>
            </div>
            @if($count>0)
              <div class="progress">
                <div class="progress-bar bg grey darken-4" role="progressbar" style="width: {{round(($completed/$count)*100,2)}}%;" aria-valuenow="25"
                aria-valuemin="0" aria-valuemax="100"></div>
              </div>
              <div class="card-body">
                <p>{{round(($completed/$count)*100,2)}}%</p>
              </div>
            @else
              <div class="progress">
                <div class="progress-bar bg grey darken-4" role="progressbar" style="width: 0%;" aria-valuenow="25"
                aria-valuemin="0" aria-valuemax="100"></div>
              </div>
              <div class="card-body">
                <p>0%</p>
              </div>
            @endif
          </div>
        </div>
      </div>
    </section>
  </div>
</div>
<input type="hidden" class="get_current_path" value="{{Request::path()}}">
        <!-- Section: Classic admin cards -->
<script type="text/javascript">
 $('.datepicker').pickadate({selectYears:250,});

    // Material Select Initialization
    $(document).ready(function () {
       var current_path=$('.get_current_path').val();
        localStorage.setItem('current_path',current_path);
      $('.mdb-select').formSelect();
    });

    // Tooltips Initialization
    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    })

  </script>

  <!-- Charts -->
  <script>
    // Small chart
    $(function () {
      $('.min-chart#chart-sales').easyPieChart({
        barColor: "#FF5252",
        onStep: function (from, to, percent) {
          $(this.el).find('.percent').text(Math.round(percent));
        }
      });
    });


</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment-with-locales.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment-with-locales.min.js"></script>


</script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.7.3/dist/Chart.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@0.7.0"></script>


<script type="text/javascript">
function handleSelect(elm)
{
window.location = elm.value;
}
</script>

     {!! $chart->script() !!}
     {!! $chart_week->script() !!}
     {!! $chart_month->script() !!}
     {!! $chart_late->script() !!}
     {!! $chart_out->script() !!}
     {!! $chart_lm->script() !!}
     {!! $chart_day->script() !!}

        <script type="text/javascript">

function type(val)
{
  console.log("chart-type=",val);
  var formData = $("#LocationSelect").serializeObject();
  {{ $chart_week->id }}_refresh("chart-line-ajax-week?type="+val+"&ids="+JSON.stringify(formData));
  {{ $chart_month->id }}_refresh("chart-line-ajax-month?type="+val+"&ids="+JSON.stringify(formData));
  {{ $chart_late->id }}_refresh("chart-line-ajax-late?type="+val+"&ids="+JSON.stringify(formData));
  {{ $chart_out->id }}_refresh("chart-line-ajax-out?type="+val+"&ids="+JSON.stringify(formData));
  {{ $chart_lm->id }}_refresh("chart-line-ajax-lm?type="+val+"&ids="+JSON.stringify(formData));
  {{ $chart_day->id }}_refresh("chart-line-ajax-day?type="+val+"&ids="+JSON.stringify(formData));

}



function getDataLocationMain()
 {

  var formData = $("#LocationSelect").serializeObject();
   var form =$("#LocationSelect").serializeArray();
    console.log(form);
  var url='/home/getAllForms/?ids='+formData.from+'.'+formData.to;
    $('#complete_table').DataTable().clear().destroy();
    complete_data(url);
    console.log(formData);
  {{ $chart->id }}_refresh("chart-line-ajax?ids="+JSON.stringify(formData));
  {{ $chart_week->id }}_refresh("chart-line-ajax-week?ids="+JSON.stringify(formData));
  {{ $chart_month->id }}_refresh("chart-line-ajax-month?ids="+JSON.stringify(formData));
  {{ $chart_late->id }}_refresh("chart-line-ajax-late?ids="+JSON.stringify(formData));
  {{ $chart_out->id }}_refresh("chart-line-ajax-out?ids="+JSON.stringify(formData));
  {{ $chart_lm->id }}_refresh("chart-line-ajax-lm?ids="+JSON.stringify(formData));
  {{ $chart_day->id }}_refresh("chart-line-ajax-day?ids="+JSON.stringify(formData));






  $.ajax({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          type: "POST",
          url: '/dashboard/getDataLocationMain',
          data: formData,
          success: function(data)
          {
              $("#topScore").html(data.maxScore);
             $("#avgScore").html(data.averageScore);
              var percent = parseInt(data.percent);
              $("#prc").children().remove();
             if(data.dir)
             {
               $("#prc").append('<i class="fas fa-arrow-circle-up ml-1"></i>');
             }
             else
             {
                $("#prc").append('<i class="fas fa-arrow-circle-down ml-1"></i>');
                percent = -1*percent;
             }

                $('.min-chart#chart-sales').data('easyPieChart').update(percent);


          },
          error: function (error) {
              console.log(error);
          }
      });
 }
</script>
<script type="text/javascript">

     function checkSelected(value,checkValue)
    {
      console.log(value);
      if(value == checkValue)
      {
        return 'selected';
      }
      else
      {
        return "";
      }

    }

     function complete_data(url) {

          var table = $('#complete_table').dataTable(
          {
            processing: true,
            "ajax": {
                "url": url,
                "type": 'get',
            },
            "createdRow": function( row, data,dataIndex,columns )
            {
                var checkbox_permission=$('#checkbox_permission').val();
                var checkbox='';
                checkbox+='<div class="form-check"><input type="checkbox" class="form-check-input" id="job'+data.id+'"><label class="form-check-label" for="job'+data.id+'""></label></div>';

                $(columns[0]).html(checkbox);
                $(row).attr('id', 'job_tr'+data['id']);
                $(row).attr('class', 'selectedrowjob');
                var temp=data['id'];
            },
            columns:
            [
                {data:'checkbox', name:'checkbox'},
                {data:'actions', name:'actions'},
                {data:'form_name',name:'form_name'},
                {data:'employee_name', name:'employee_name'},
                {data:'location_name', name:'location_name'},
                {data:'location_breakdown',name:'location_breakdown'},
                {data:'date_time',name:'date_time'},
                {data:'score',name:'score'},
                {data:'form_type_score',name:'form_type_score'},
                {data:'form_score',name:'form_score'},


            ],

          }
          );
  }




   function formLocation(url) {
    $.ajax({
        type: 'GET',
        url: url,
        success: function(response)
        {
          var table = $('#form_location_table').dataTable(
          {
            processing: true,
            language: {
            'lengthMenu': '_MENU_',
            'info': ' ',
            },
            "ajax": {
                "url": url,
                "type": 'get',
            },
            "createdRow": function( row, data,dataIndex,columns )
            {
                var checkbox_permission=$('#checkbox_permission').val();
                var checkbox='';
                checkbox+='<div class="form-check"><input type="checkbox" class="form-check-input" id="job'+data.id+'"><label class="form-check-label" for="job'+data.id+'""></label></div>';

                $(columns[0]).html(checkbox);

                $(row).attr('id', 'job_tr'+data['id']);
                $(row).attr('class', 'selectedrowjob');
                var temp=data['id'];
            },
            columns:
            [
                {data:'checkbox', name:'checkbox'},
                {data:'actions', name:'actions'},
                {data:'location', name:'location'},
                {data:'month',name:'month'},
                {data:'score',name:'score'},
                {data:'max_score',name:'max_score'},
                {data:'response_total',name:'response_total'},
                {data:'response_score_total',name:'response_score_total'},
                {data:'forms_completed',name:'forms_completed'},
                {data:'bonus_score',name:'bonus_score'},
                {data:'outstanding_count',name:'outstanding_count'},
                {data:'actions_resolved',name:'actions_resolved'},

            ],

          }
          );
        },
        error: function (error) {
          console.log(error);
        }
    });
  }

  var url='/home/getAllFormScoreLocation';
  formLocation(url);


</script>
<script src="{{asset('/custom/js/dashboard.js')}}"></script>
@endsection
