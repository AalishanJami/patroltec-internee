@extends('backend.layouts.backend')
@section('title', 'Required Documents')
@section('content')
    @php
        $data=localization();
    @endphp
    <link rel="stylesheet" href="{{ asset('custom/css/custom_style.css') }}">

    {{ Breadcrumbs::render('required_documents') }}
    <div class="card">
        <div class="card-body">
            <ul  class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">
                        Required Documents
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact"aria-selected="false">
                        Doc Type
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home"aria-selected="true">
                <span class="assign_class label{{getKeyid('position',$data)}}" data-id="{{getKeyid('position',$data) }}" data-value="{{checkKey('position',$data) }}" >
                    {!! checkKey('position',$data) !!}
                </span>
                    </a>
                </li>
            </ul>
            <br>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    <div id="table-user_doc_type"  class="table-editable">
            <span class="add_user_doc_type table-add float-right mb-3 mr-2">
              <a class="text-success">
                  <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
              </a>
            </span>
                        <button id="deleteselecteduser_doc_type" class="btn btn-danger btn-sm">
                            Delete Selected Doc Type
                        </button>
                        <button style="display: none;" id="deleteselecteduser_doc_typeSoft" class="btn btn-danger btn-sm">
                            Delete Selected Doc Type
                        </button>
                        <button  style="display: none;" id="selectedactivebuttonuser_doc_type" class="btn btn-success btn-sm" >
                            Active Selected Doc Type
                        </button>
                        <button id="restorebuttonuser_doc_type" class="btn btn-primary btn-sm">
                            Restore Doc Type
                        </button>
                        <button  style="display: none;" id="activebuttonuser_doc_type" class="btn btn-primary btn-sm">
                            Show Selected Doc Type
                        </button>
                        <form class="form-style" id="export_excel_user_doc_type" method="POST" action="{{ url('user/doc_type/exportExcel') }}">
                            {!! csrf_field() !!}
                            <input type="hidden" class="user_doc_type_export" name="excel_array" value="1">
                            <button  type="submit" class="form_submit_check btn btn-warning btn-sm">
                                Excel Export
                            </button>
                        </form>
                        <form class="form-style" id="export_world_user_doc_type" method="POST" action="{{ url('user/doc_type/exportWord') }}">
                            {!! csrf_field() !!}
                            <input type="hidden" class="user_doc_type_export" name="word_array" value="1">
                            <button  type="submit" class="form_submit_check btn btn-success btn-sm">
                                Word Export
                            </button>
                        </form>
                        <form  class="form-style" id="export_pdf_user_doc_type" method="POST" action="{{ url('user/doc_type/exportPdf') }}">
                            {!! csrf_field() !!}
                            <input type="hidden" class="user_doc_type_export" name="pdf_array" value="1">
                            <button  type="submit" class="form_submit_check btn btn-secondary btn-sm">
                                Pdf Export
                            </button>
                        </form>
                        <div id="table" class="table-responsive table-editable">
                            <table id="user_doc_type_table" class="table table-bordered table-responsive-md table-striped">
                                <thead>
                                <tr>
                                    <th class="no-sort all_checkboxes_style">
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input user_doc_type_table_checked" id="user_doc_type_table_checkbox_all">
                                            <label class="form-check-label" for="user_doc_type_table_checkbox_all">
                                                <span class="assign_class label{{getKeyid('all',$data)}}" data-id="{{getKeyid('all',$data) }}" data-value="{{checkKey('all',$data) }}" >
                                                    {!! checkKey('all',$data) !!}
                                                </span>
                                            </label>
                                        </div>
                                    </th>
                                    <th> Doc Type</th>
                                    <th> Position</th>
                                    <th class="no-sort all_action_btn" id="action"></th>
                                    {{--                            <th class="no-sort all_action_btn"></th>--}}
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                    <div id="table_doc_type" class="table-editable">
              <span class="adddoc_typeModal table-add float-right mb-3 mr-2">
                <a class="text-success">
                    <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
                </a>
              </span>
                        <button id="deleteselecteddoc_type" class="btn btn-danger btn-sm">
                <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                  {!! checkKey('selected_delete',$data) !!}
                </span>
                        </button>
                        <button style="display: none;" id="deleteselecteddoc_typeSoft" class="btn btn-danger btn-sm">
                <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                  {!! checkKey('selected_delete',$data) !!}
                </span>
                        </button>
                        <button  style="display: none;" id="selectedactivebuttondoc_type" class="btn btn-success btn-sm" >
                <span class="assign_class label{{getKeyid('selected_active',$data)}}" data-id="{{getKeyid('selected_active',$data) }}" data-value="{{checkKey('selected_active',$data) }}" >
                  {!! checkKey('selected_active',$data) !!}
                </span>
                        </button>
                        <button id="restorebuttondoc_type" class="btn btn-primary btn-sm">
                <span class="assign_class label{{getKeyid('restore',$data)}}" data-id="{{getKeyid('restore',$data) }}" data-value="{{checkKey('restore',$data) }}" >
                  {!! checkKey('restore',$data) !!}
                </span>
                        </button>
                        <button  style="display: none;" id="activebuttondoc_type" class="btn btn-primary btn-sm">
                <span class="assign_class label{{getKeyid('show_active',$data)}}" data-id="{{getKeyid('show_active',$data) }}" data-value="{{checkKey('show_active',$data) }}" >
                  {!! checkKey('show_active',$data) !!}
                </span>
                        </button>
                        <form class="form-style" id="export_excel_doc_type" method="POST" action="{{ url('doc_type/exportExcel') }}">
                            {!! csrf_field() !!}
                            <input type="hidden" class="doc_type_export" name="excel_array" value="1">
                            <button  type="submit" class="form_submit_check btn btn-warning btn-sm">
                  <span class="assign_class label{{getKeyid('excel_export',$data)}}" data-id="{{getKeyid('excel_export',$data) }}" data-value="{{checkKey('excel_export',$data) }}" >
                    {!! checkKey('excel_export',$data) !!}
                  </span>
                            </button>
                        </form>
                        <form class="form-style" id="export_world_doc_type" method="POST" action="{{ url('doc_type/exportWord') }}">
                            {!! csrf_field() !!}
                            <input type="hidden" class="doc_type_export" name="word_array" value="1">
                            <button  type="submit" class="form_submit_check btn btn-success btn-sm">
                  <span class="assign_class label{{getKeyid('word_export',$data)}}" data-id="{{getKeyid('word_export',$data) }}" data-value="{{checkKey('word_export',$data) }}" >
                      {!! checkKey('word_export',$data) !!}
                  </span>
                            </button>
                        </form>
                        <form  class="form-style" id="export_pdf_doc_type" method="POST" action="{{ url('doc_type/exportPdf') }}">
                            {!! csrf_field() !!}
                            <input type="hidden" class="doc_type_export" name="pdf_array" value="1">
                            <button  type="submit" class="form_submit_check btn btn-secondary btn-sm">
                  <span class="assign_class label{{getKeyid('pdf_export',$data)}}" data-id="{{getKeyid('pdf_export',$data) }}" data-value="{{checkKey('pdf_export',$data) }}" >
                      {!! checkKey('pdf_export',$data) !!}
                  </span>
                            </button>
                        </form>
                        <div id="table" class="table-responsive table-editable">
                            <table id="doc_type_table" style="width: 100%;" class="doc_type_table table table-bordered table-responsive-md table-striped">
                                <thead>
                                <tr>
                                    <th class="no-sort all_checkboxes_style">
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input doc_type_table_checked" id="doc_type_table_checkbox_all">
                                            <label class="form-check-label" for="doc_type_table_checkbox_all">
                                                <span class="assign_class label{{getKeyid('all',$data)}}" data-id="{{getKeyid('all',$data) }}" data-value="{{checkKey('all',$data) }}" >
                                                    {!! checkKey('all',$data) !!}
                                                </span>
                                            </label>
                                        </div>
                                    </th>
                                    <th>
                            <span class="assign_class label{{getKeyid('name',$data)}}" data-id="{{getKeyid('name',$data) }}" data-value="{{checkKey('name',$data) }}" >
                                {!! checkKey('name',$data) !!}
                            </span>
                                    </th>
                                    <th class="no-sort all_action_btn" id="action"></th>
                                    {{--                          <th class="no-sort"></th>--}}
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade " id="home" role="tabpanel" aria-labelledby="home-tab">
                    <div id="table_position" class="table-responsive table-editable">
                        @if(Auth()->user()->hasPermissionTo('create_employeePosition') || Auth::user()->all_companies == 1 )
                            <span class="addpositionModal table-add float-right mb-3 mr-2">
                  <a class="text-success">
                      <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
                  </a>
              </span>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_delete_employeePosition') || Auth::user()->all_companies == 1 )
                            <button id="deleteselectedposition" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" class=" assign_class btn btn-danger btn-sm label{{getKeyid('selected_delete',$data)}}">
                                {!! checkKey('selected_delete',$data) !!}
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_delete_employeePosition') || Auth::user()->all_companies == 1 )
                            <button style="display: none;" id="deleteselectedpositionSoft" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" class=" assign_class btn btn-danger btn-sm label{{getKeyid('selected_delete',$data)}}">
                                {!! checkKey('selected_delete',$data) !!}
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_active_employeePosition') || Auth::user()->all_companies == 1 )
                            <button  style="display: none;" id="selectedactivebuttonposition" class="btn btn-success btn-sm assign_class label{{getKeyid('selected_active',$data)}}"  data-id="{{getKeyid('selected_active',$data) }}" data-value="{{checkKey('selected_active',$data) }}" >
                                {!! checkKey('selected_active',$data) !!}
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_restore_delete_employeePosition') || Auth::user()->all_companies == 1 )
                            <button id="restorebuttonposition" data-id="{{getKeyid('restore',$data) }}" data-value="{{checkKey('restore',$data) }}" class="btn btn-primary btn-sm assign_class label{{getKeyid('restore',$data)}}">
                                {!! checkKey('restore',$data) !!}
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_active_employeePosition') || Auth::user()->all_companies == 1 )
                            <button  style="display: none;" class="btn btn-primary btn-sm assign_class label{{getKeyid('show_active',$data)}}" id="activebuttonposition" data-id="{{getKeyid('show_active',$data) }}" data-value="{{checkKey('show_active',$data) }}" >
                                {!! checkKey('show_active',$data) !!}
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('csv_employeePosition') || Auth::user()->all_companies == 1 )
                            <form class="form-style" id="export_excel_position_btn" method="POST" action="{{ url('employee/position/exportExcel') }}">
                                {!! csrf_field() !!}
                                <input type="hidden" class="position_export" name="excel_array" value="1">
                                <button  type="submit" id="export_excel_employee_btn" class="form_submit_check btn btn-warning btn-sm">
                      <span class="assign_class label{{getKeyid('excel_export',$data)}}" data-id="{{getKeyid('excel_export',$data) }}" data-value="{{checkKey('excel_export',$data) }}" >
                          {!! checkKey('excel_export',$data) !!}
                      </span>
                                </button>
                            </form>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('word_employeePosition') || Auth::user()->all_companies == 1 )
                            <form class="form-style" id="export_world_position_btn" method="POST" action="{{ url('employee/position/exportWord') }}">
                                {!! csrf_field() !!}
                                <input type="hidden" class="position_export" name="word_array" value="1">
                                <button  type="submit" id="export_word_note" class="form_submit_check btn btn-success btn-sm">
                      <span class="assign_class label{{getKeyid('word_export',$data)}}" data-id="{{getKeyid('word_export',$data) }}" data-value="{{checkKey('word_export',$data) }}" >
                          {!! checkKey('word_export',$data) !!}
                      </span>
                                </button>
                            </form>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('pdf_employeePosition') || Auth::user()->all_companies == 1 )
                            <form  class="form-style" id="export_pdf_position_btn" method="POST" action="{{ url('employee/position/exportPdf') }}">
                                {!! csrf_field() !!}
                                <input type="hidden" class="position_export" name="pdf_array" value="1">
                                <button  type="submit" id="export_pdf_note_btn"  class="form_submit_check btn btn-secondary btn-sm">
                      <span class="assign_class label{{getKeyid('pdf_export',$data)}}" data-id="{{getKeyid('pdf_export',$data) }}" data-value="{{checkKey('pdf_export',$data) }}" >
                          {!! checkKey('pdf_export',$data) !!}
                      </span>
                                </button>
                            </form>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('view_employeePosition') || Auth::user()->all_companies == 1 )
                            <div id="table" class="table-responsive table-editable">
                                <table id="position_table" style="width: 100%;" class="position_table table table-bordered table-responsive-md table-striped">
                                    <thead>
                                    <tr>
                                        <th class="no-sort all_checkboxes_style">
                                            <div class="form-check">
                                                <input type="checkbox" class="form-check-input position_checked" id="position_checkbox_all">
                                                <label class="form-check-label" for="position_checkbox_all">
                                                <span class="assign_class label{{getKeyid('all',$data)}}" data-id="{{getKeyid('all',$data) }}" data-value="{{checkKey('all',$data) }}" >
                                                    {!! checkKey('all',$data) !!}
                                                </span>
                                                </label>
                                            </div>
                                        </th>
                                        <th>
                                            <span class="assign_class label{{getKeyid('name',$data)}}" data-id="{{getKeyid('name',$data) }}" data-value="{{checkKey('name',$data) }}" >
                                                {!! checkKey('name',$data) !!}
                                            </span>
                                        </th>
                                        <th class="no-sort all_action_btn" id="action"></th>
{{--                                        <th class="no-sort"></th>--}}
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        @endif
                        @if(Auth()->user()->all_companies == 1)
                            <input type="hidden" id="employeePosition_checkbox" value="1">
                            <input type="hidden" id="edit_employeePosition" value="1">
                            <input type="hidden" id="create_employeePosition" value="1">
                            <input type="hidden" id="delete_employeePosition" value="1">
                            <input type="hidden" id="employeePosition_name" value="1">
                            <input type="hidden" id="employeePosition_name_create" value="1">
                            <input type="hidden" id="employeePosition_name_edit" value="1">
                        @else
                            <input type="hidden" id="employeePosition_checkbox" value="{{Auth()->user()->hasPermissionTo('employeePosition_checkbox')}}">
                            <input type="hidden" id="edit_employeePosition" value="{{Auth()->user()->hasPermissionTo('edit_employeePosition')}}">
                            <input type="hidden" id="create_employeePosition" value="{{Auth()->user()->hasPermissionTo('create_employeePosition')}}">
                            <input type="hidden" id="delete_employeePosition" value="{{Auth()->user()->hasPermissionTo('delete_employeePosition')}}">
                            <input type="hidden" id="employeePosition_name" value="{{Auth()->user()->hasPermissionTo('employeePosition_name')}}">
                            <input type="hidden" id="employeePosition_name_create" value="{{Auth()->user()->hasPermissionTo('employeePosition_name_create')}}">
                            <input type="hidden" id="employeePosition_name_edit" value="{{Auth()->user()->hasPermissionTo('employeePosition_name_edit')}}">
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" class="get_current_path" value="{{Request::path()}}">
    @include('backend.label.input_label')
    <script type="text/javascript">
        function position_data(url) {
            var table = $('#position_table').dataTable(
                {
                    processing: true,
                    language: {
                        'lengthMenu': '  _MENU_ ',
                        'search':'<span class="assign_class label'+$('#search_id').attr('data-id')+'" data-id="'+$('#search_id').attr('data-id')+'" data-value="'+$('#search_id').val()+'" >'+$('#search_id').val()+'</span>',
                        'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                        'info':'',
                        'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                        'paginate': {
                            'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                            'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                        },
                        'infoFiltered': "(filtered from _MAX_ total records)"
                    },
                    "ajax": {
                        "url": url,
                        "type": 'get',
                    },
                    "createdRow": function( row, data, dataIndex,columns )
                    {
                        var checkbox_permission=$('#checkbox_permission').val();
                        var checkbox='';
                        checkbox+='<div class="form-check"><input type="checkbox" class="form-check-input selectedrowposition" id="position_checked'+data.id+'"><label class="form-check-label" for="position_checked'+data.id+'""></label></div>';
                        var submit='';
                        submit+='<a type="button" class="btn btn-primary btn-xs my-0 waves-effect waves-light savepositiondata show_tick_btn'+data.id+'" style="display:none;"  id="'+data.id+'"><i class="fas fa-check"></i></a>';
                        if ($("#employeePosition_checkbox").val()==1){
                            $(columns[0]).html(checkbox);
                        }
                        $(columns[1]).attr('id', 'position_name'+data['id']);
                        $(columns[1]).attr('data-id', data['id']);
                        if ($("#employeePosition_name_edit").val()==1 && $("#edit_employeePosition").val()==1){
                            $(columns[1]).attr('Contenteditable', 'true');
                        }
                        $(columns[1]).attr('class','edit_inline_position');
                        $(columns[1]).attr('onkeydown','editSelectedRow(position_tr'+data['id']+')');
                        if ($("#edit_employeePosition").val()==1){
                            $(columns[2]).append(submit);
                        }
                        $(row).attr('id', 'position_tr'+data['id']);
                        // $(row).attr('class', 'selectedrowposition');
                        $(row).attr('data-id',data['id']);
                        var temp=data['id'];
                    },
                    columns:
                        [
                            {data: 'checkbox', name: 'checkbox',},
                            {data: 'name', name: 'name',visible:$('#employeePosition_name').val()},
                            {data: 'actions', name: 'actions'},
                            // {data: 'submit', name: 'submit',},
                        ],
                });
            if ($("#position_checkbox_all").prop('checked',true)){
                $("#position_checkbox_all").prop('checked',false)
            }
            if ($(".selectedrowposition").prop('checked',true)){
                $(".selectedrowposition").prop('checked',false)
            }
        }
        function doc_type_data(url) {
            var table = $('#doc_type_table').dataTable(
                {
                    processing: true,
                    language: {
                        'lengthMenu': '  _MENU_ ',
                        'search':'<span class="assign_class label'+$('#search_id').attr('data-id')+'" data-id="'+$('#search_id').attr('data-id')+'" data-value="'+$('#search_id').val()+'" >'+$('#search_id').val()+'</span>',
                        'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                        'info':'',
                        'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                        'paginate': {
                            'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                            'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                        },
                        'infoFiltered': "(filtered from _MAX_ total records)"
                    },
                    "ajax": {
                        "url": url,
                        "type": 'get',
                    },
                    "createdRow": function( row, data, dataIndex,columns )
                    {
                        var checkbox_permission=$('#checkbox_permission').val();
                        var checkbox='';
                        checkbox+='<div class="form-check"><input type="checkbox" class="form-check-input selectedrowdoc_type" id="doc_type'+data.id+'"><label class="form-check-label" for="doc_type'+data.id+'""></label></div>';
                        var submit='';
                        submit+='<a type="button" class="btn btn-primary btn-xs my-0 waves-effect all_action_btn_margin waves-light savedoc_typedata show_tick_btn'+data.id+'" style="display:none;" id="'+data.id+'"><i class="fas fa-check"></i></a>';
                        $(columns[0]).html(checkbox);
                        $(columns[1]).attr('id', 'doc_type_name'+data['id']);
                        $(columns[1]).attr('data-id', data['id']);
                        $(columns[1]).attr('Contenteditable', 'true');
                        $(columns[1]).attr('class','edit_inline_doc_type');
                        $(columns[1]).attr('onkeydown','editSelectedRow(doc_type_tr'+data['id']+')');
                        $(columns[2]).append(submit);
                        $(row).attr('id', 'doc_type_tr'+data['id']);
                        // $(row).attr('class', 'selectedrowdoc_type');
                        $(row).attr('data-id',data['id']);
                        var temp=data['id'];
                    },
                    columns:
                        [
                            {data: 'checkbox', name: 'checkbox',},
                            {data: 'name', name: 'name'},
                            {data: 'actions', name: 'actions'},
                            // {data: 'submit', name: 'submit',},
                        ],
                });
            if ($("#doc_type_table_checkbox_all").prop('checked',true)){
                $("#doc_type_table_checkbox_all").prop('checked',false)
            }
            if ($(".selectedrowdoc_type").prop('checked',true)){
                $(".selectedrowdoc_type").prop('checked',false)
            }
        }
        function checkSelected(value,checkValue)
        {
            if(value == checkValue)
            {
                return 'selected';
            }
            else
            {
                return "";
            }
        }
        function user_doc_type_data(url)
        {
            $.ajax({
                type: 'GET',
                url: url,
                success: function(response)
                {
                    $('#user_doc_type_table').DataTable().clear().destroy();
                    var table = $('#user_doc_type_table').dataTable(
                        {
                            processing: true,
                            language: {
                                'lengthMenu': '  _MENU_ ',
                                'search':'<span class="assign_class label'+$('#search_id').attr('data-id')+'" data-id="'+$('#search_id').attr('data-id')+'" data-value="'+$('#search_id').val()+'" >'+$('#search_id').val()+'</span>',
                                'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                                'info':'',
                                'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                                'paginate': {
                                    'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                                    'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                                },
                                'infoFiltered': "(filtered from _MAX_ total records)"
                            },
                            "ajax": {
                                "url": url,
                                "type": 'get',
                            },
                            "createdRow": function( row, data,dataIndex,columns )
                            {
                                var checkbox_permission=$('#checkbox_permission').val();
                                var checkbox='';
                                checkbox+='<div class="form-check"><input type="checkbox" class="form-check-input selectedrowuser_doc_type" id="user_doc_type'+data.id+'"><label class="form-check-label" for="user_doc_type'+data.id+'""></label></div>';
                                var submit='';
                                submit+='<a type="button" class="btn btn-primary btn-xs my-0 waves-effect waves-light saveuser_doc_typedata all_action_btn_margin show_tick_btn'+data.id+'" style="display:none;"  id="'+data.id+'"><i class="fas fa-check"></i></a>';
                                var doc_type_id='';
                                doc_type_id+='<select searchable="Search here.." onchange="editSelectedRow('+"user_doc_type_tr"+data.id+')" class="get_customer_data" data-id="'+data.id+'"  id="doc_type_id'+data.id+'"  style="background-color: inherit;border: 0px">';
                                for (var j = response.doc_type.length - 1; j >= 0; j--) {
                                    doc_type_id+='<option  value="'+response.doc_type[j].id+'" '+checkSelected(data.doc_type_id,response.doc_type[j].id)+'  >'+response.doc_type[j].name+'</option>';
                                }
                                doc_type_id+='</select>';
                                var position='';
                                position+='<select searchable="Search here.."  id="position'+data.id+'" onchange="editSelectedRow('+"user_doc_type_tr"+data.id+')"  style="background-color: inherit;border: 0px">';
                                for (var j = response.positions.length - 1; j >= 0; j--) {
                                    position+='<option  value="'+response.positions[j].id+'" '+checkSelected(data.position_id,response.positions[j].id)+'  >'+response.positions[j].name+'</option>';
                                }
                                position+='</select>';

                                $(columns[0]).html(checkbox);
                                $(columns[1]).attr('class','edit_inline_user_doc_type');
                                $(columns[1]).attr('data-id',data['id']);
                                $(columns[1]).html(doc_type_id);
                                $(columns[2]).attr('class','edit_inline_user_doc_type');
                                $(columns[2]).attr('data-id',data['id']);
                                $(columns[2]).html(position);
                                $(columns[3]).append(submit);
                                $(row).attr('id', 'user_doc_type_tr'+data['id']);
                                // $(row).attr('class', 'selectedrowuser_doc_type');
                                $(row).attr('data-id',data['id']);
                                var temp=data['id'];
                            },
                            columns:
                                [
                                    {data: 'checkbox', name: 'checkbox',},
                                    {data:'doc_type_id',name:'doc_type_id'},
                                    {data:'position_id',name:'position_id'},
                                    {data: 'actions', name: 'actions'},
                                    // {data: 'submit', name: 'submit'},
                                ],

                        }
                    );
                },
                error: function (error) {
                    console.log(error);
                }
            });
            if ($("#user_doc_type_table_checkbox_all").prop('checked',true)){
                $("#user_doc_type_table_checkbox_all").prop('checked',false)
            }
            if ($(".selectedrowuser_doc_type").prop('checked',true)){
                $(".selectedrowuser_doc_type").prop('checked',false)
            }
        }
        $(document).ready(function() {
            var current_path=$('.get_current_path').val();
            localStorage.setItem('current_path',current_path);
            var url='/doc_type/getall';
            doc_type_data(url);
            var position_url='/employee/position/getall';
            position_data(position_url);
            var url='/user/doc_type/getall';
            user_doc_type_data(url);
        });
    </script>
    <script src="{{ asset('custom/js/position.js') }}"></script>
    <script src="{{ asset('custom/js/doc_types.js') }}"></script>
    <script src="{{ asset('custom/js/user_doc_types.js') }}"></script>
@endsection
