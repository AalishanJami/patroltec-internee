 @php
        $data=localization();
@endphp

<style type="text/css">
 .form-custom-boutton{
      margin-left: unset!important;
      padding: 0.3rem!important;
   }
</style>


<div class="modal fade" id="modalEditForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Required Documents Edit</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
              <form id="registernewcompany" method="POST" >
                @csrf
                <div class="modal-body mx-3">
                    
                      <a class="text-success"  data-toggle="modal" data-target="#modalClientIndex" >
                              <button type="button" class="btn btn-sm btn-outline-primary waves-effect form-custom-boutton" style="float: left;">+Document Required</button>
                           </a>
                             
                    <div class="md-form mb-5">
                     
                        <input type="text" class="form-control" name="name" value="Appendix.docx" required autocomplete="name" autofocus>
                     
                    </div>
                     <a class="text-success"  data-toggle="modal" data-target="#modalClientIndex" >
                              <button type="button" class="btn btn-sm btn-outline-primary waves-effect form-custom-boutton" style="float: left;">+Employee Type</button>
                           </a>
                    <div class="md-form mb-5">
                        
                        <input type="text" class="form-control validate " name="name" value="Manager" required autocomplete="name" autofocus>
                      
                    </div>
                    

                </div>


                <div class="modal-footer d-flex justify-content-center">
                    <button class="btn btn-primary btn-sm" type="submit"><span class="assign_class label{{getKeyid('save',$data)}}" data-id="{{getKeyid('save',$data) }}" data-value="{{checkKey('save',$data) }}" >{!! checkKey('save',$data) !!} </span></button>
                </div>
            </form>

            <div class="modal-body mx-3">

           
            </div>
        </div>
    </div>
</div>
@component('backend.client.model.index')
@endcomponent
