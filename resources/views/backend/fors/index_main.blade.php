@extends('backend.layouts.backend')
@section('content')
    @php
       $data=localization();
    @endphp
        {{ Breadcrumbs::render('qr') }}

         <div class="card">

            <div class="card-body">
                <div id="table" class="table-editable">
        <span class="table-add float-right mb-3 mr-2">
            <a class="text-success"  data-toggle="modal" data-target="#modalRegisterCompany">
                <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
            </a>
        </span>
        <button class="btn btn-danger btn-sm">
            Delete Selected QR
        </button>
        <button class="btn btn-primary btn-sm">
          Restore Deleted QR
        </button>

        <button  style="display: none;">
            Show Active QR
        </button>
        <button class="btn btn-warning btn-sm">
           Excel Export
        </button>
        <button class="btn btn-success btn-sm">
            Word Export
        </button>
    <table id="company_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th> Name
            </th>
            <th> Directions
            </th>
            <th> Information
            </th>
            <th> Notes
            </th>
            <th> Actions
            </th>
        </tr>
        </thead>
            <tr>
                <td class="pt-3-half" contenteditable="true">dummy</td>
                <td class="pt-3-half" contenteditable="true">dummy</td>
                <td class="pt-3-half" contenteditable="true">dummy</td>
                <td class="pt-3-half" contenteditable="true">dummy</td>
                <td class="pt-3-half" contenteditable="true">
                    <a  data-toggle="modal" data-target="#modalEditForm" class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i></a>
                    <button type="button"class="btn btn-danger btn-sm my-0" onclick="deleteAddressdata()"><i class="fas fa-trash"></i></button>

                </td>
            </tr>
        <tbody>
        </tbody>
    </table>
</div>
</div>
</div>
     {{--    Edit Modal--}}
    @component('backend.qr.model.edit')
    @endcomponent
    {{--    END Edit Modal--}}

    {{--    Register Modal--}}
    @component('backend.qr.model.create')
    @endcomponent
    {{--end modal register--}}

@endsection
<script type="text/javascript">
function deleteAddressdata()
{

    swal({
        title:'Are you sure?',
        text: "Delete Record.",
        type: "error",
        showCloseButton: true,
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, Delete!",
        cancelButtonText: "Cancel!",
        closeOnConfirm: false,
        customClass: "confirm_class",
        closeOnCancel: false,
    },
    function(isConfirm){
        swal.close();
 });


}

</script>
