@extends('backend.layouts.backend')
@section('content')
    @php
       $data=localization();
    @endphp
        {{ Breadcrumbs::render('fors') }}
        <!-- <span class="table-add float-right mb-3 mr-2">
            <a class="text-success"  data-toggle="modal" data-target="#modalRegisterCompany">
                <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
            </a>
        </span> -->
        <div class="card">

            <div class="card-body">
                <div id="table" class="table-editable">
        <button class="btn btn-danger btn-sm">
            Delete Selected Fors
        </button>
        <button class="btn btn-primary btn-sm">
          Restore Deleted Fors
        </button>

        <button  style="display: none;">
            Show Active QR
        </button>
        <button class="btn btn-warning btn-sm">
           Excel Export
        </button>
        <button class="btn btn-success btn-sm">
            Word Export
        </button>
    <table  class="company_table_static table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th> Company 
            </th>
            <th> Fors Id Number
            </th>
            <th> Fors status
            </th>
            <th> Accreditation Type
            </th>
            <th> Accreditated Operating Centres
            </th>
            <th> Town City
            </th>
            <th> Business Sector
            </th>
            <th> Fleet Type
            </th>
        </tr>
        </thead>
            <tr>
                <td class="pt-3-half">J and S Wilkinson Services Ltd</td>
                
                <td class="pt-3-half">001085</td>
                
                <td class="pt-3-half">68</td>
                
                <td class="pt-3-half">General</td>
                
                <td class="pt-3-half">south asia</td>
                
                <td class="pt-3-half">new york</td>
                
                <td class="pt-3-half">Sector C</td>
                
                <td class="pt-3-half">General</td>
                
            </tr>
            <tr>
                 <td class="pt-3-half">J and S Wilkinson Services Ltd</td>
                  <td class="pt-3-half">001085</td>
                  <td class="pt-3-half">Gold</td>
                  <td class="pt-3-half">SOCA</td>
                  <td class="pt-3-half">RM16 3JX </td>
                  <td class="pt-3-half">Essex</td>
                  <td class="pt-3-half">Transportation and storage</td>
                  <td class="pt-3-half">Truck and Van</td>
              </tr>
        <tbody>
        </tbody>
    </table>

</div>
</div>
</div>
@endsection
