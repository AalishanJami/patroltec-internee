@extends('backend.layouts.backend')
@section('content')
    @php
        $data=localization();
    @endphp
        {{Breadcrumbs::render('manageusers')}}
        <div class="card">
            <div class="card-body">
                <div id="table" class="table-editable">
                    @if(Auth()->user()->hasPermissionTo('create_users') || Auth::user()->all_companies == 1 )
                    <span class="table-add float-right mb-3 mr-2">
                        <a class="text-success" id="create_user"  data-toggle="modal" data-target="#modalRegisterForm2">
                            <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
                        </a>
                    </span>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('selected_delete_users') || Auth::user()->all_companies == 1 )
                        <button id="deleteselected"  data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" class=" assign_class btn btn-danger btn-sm label{{getKeyid('selected_delete',$data)}}">
                            {!! checkKey('selected_delete',$data) !!}
                        </button>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('selected_restore_delete_users') || Auth::user()->all_companies == 1 )
                    <button id="restorebutton" data-id="{{getKeyid('restore',$data) }}" data-value="{{checkKey('restore',$data) }}"   class="btn btn-primary btn-sm assign_class label{{getKeyid('restore',$data)}}">
                        {!! checkKey('restore',$data) !!}
                    </button>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('active_users') || Auth::user()->all_companies == 1 )
                    <button id="showactivebutton" data-id="{{getKeyid('show_active',$data) }}" data-value="{{checkKey('show_active',$data) }}"  class="btn btn-primary btn-sm assign_class label{{getKeyid('show_active',$data)}}" style="display: none;">
                        {!! checkKey('show_active',$data) !!}
                    </button>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('csv_users') || Auth::user()->all_companies == 1 )
                    <form class="form-style" method="POST" action="{{ url('export_excel_user') }}">
                            <input type="hidden" id="export_excel_user" name="export_excel_user" value="1">
                            {!! csrf_field() !!}
                             <button  type="submit" data-id="{{getKeyid('excel_export',$data) }}" data-value="{{checkKey('excel_export',$data) }}"  class="btn btn-warning btn-sm assign_class label{{getKeyid('excel_export',$data)}}">
                            {!! checkKey('excel_export',$data) !!}
                        </button>
                    </form>       
                    @endif
                    @if(Auth()->user()->hasPermissionTo('word_users') || Auth::user()->all_companies == 1 )
                    <form  class="form-style"  method="POST" action="{{ url('export_word_user') }}">
                            <input type="hidden" id="export_word_user" name="export_word_user" value="1">
                            {!! csrf_field() !!}
                             <button  type="submit" data-id="{{getKeyid('word_export',$data) }}" data-value="{{checkKey('word_export',$data) }}"  class="btn btn-success btn-sm assign_class label{{getKeyid('word_export',$data)}}">
                                {!! checkKey('word_export',$data) !!}
                        </button>
                    </form>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('pdf_users') || Auth::user()->all_companies == 1 )
                    <form  class="form-style"  method="POST" action="{{ url('export_pdf_user') }}">
                            <input type="hidden" id="export_pdf_user" name="export_pdf_user" value="1">
                            {!! csrf_field() !!}
                             <button  type="submit" data-id="{{getKeyid('pdf_export',$data) }}" data-value="{{checkKey('pdf_export',$data) }}"  class="btn btn-secondary btn-sm assign_class label{{getKeyid('pdf_export',$data)}}">
                                {!! checkKey('pdf_export',$data) !!}
                        </button>
                    </form>
                    @endif
                    @include('backend.label.input_label')

                    <table id="usertable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th class="th-sm assign_class label{{getKeyid('action',$data)}}" data-id="{{getKeyid('action',$data) }}" data-value="{{ checkKey('action',$data) }}" >{!!checkKey('action',$data) !!}
                            </th>
                            <th></th>
                            <th class="th-sm assign_class label{{getKeyid('name',$data)}}" data-id="{{getKeyid('name',$data) }}" data-value="{{ checkKey('name',$data) }}" >{!! checkKey('name',$data) !!}
                            </th>
                            <th class="th-sm assign_class label{{getKeyid('email',$data)}}" data-id="{{getKeyid('email',$data) }}" data-value="{{ checkKey('email',$data) }}" >
                                {!!checkKey('email',$data) !!}
                            </th>
                        </tr>
                        </thead>

                        <tbody>
                        </tbody>
                    </table>
                    @if(Auth()->user()->all_companies == 1)
                    <input type="hidden" id="checkbox_permission" value="1">
                    <input type="hidden" id="name_permission" value="1">
                     <input type="hidden" id="email_permission" value="1">
                    @else
                    <input type="hidden" id="checkbox_permission" value="{{Auth()->user()->hasPermissionTo('user_checkbox')}}">
                    <input type="hidden" id="name_permission" value="{{Auth()->user()->hasPermissionTo('user_name')}}">
                    <input type="hidden" id="email_permission" value="{{Auth()->user()->hasPermissionTo('user_email')}}">
                    @endif
                     {{--    Edit Modal--}}
                    @component('backend.user.model.edit', ['companies' => $companies])
                    @endcomponent
                    {{--    END Edit Modal--}}

                    {{--    Register Modal--}}
                    @component('backend.user.model.create', ['companies' => $companies])
                    @endcomponent
                    {{--end modal register--}}

                    {{--    Register Modal--}}
                    @component('backend.user.model.role')
                    @endcomponent
                    {{--end modal register--}}
                </div>
            </div>
        </div>

    <script type="text/javascript">
        var company_id = 1;
        $(document).ready(function() {
            $(function () {
                userAppend();
            }); 
            (function ($) {
                var active ='<span class="assign_class label'+$('#manage_users_label').attr('data-id')+'" data-id="'+$('#manage_users_label').attr('data-id')+'" data-value="'+$('#manage_users_label').val()+'" >'+$('#manage_users_label').val()+'</span>';
                $('.breadcrumb-item.active').html(active);
                var parent ='<span class="assign_class label'+$('#dashboard_label').attr('data-id')+'" data-id="'+$('#dashboard_label').attr('data-id')+'" data-value="'+$('#dashboard_label').val()+'" >'+$('#dashboard_label').val()+'</span>';
                $('.breadcrumb-item a').html(parent);
            }(jQuery));
      
        }); 
        function userAppend()
        {
            var table = $('#usertable').dataTable({
                processing: true,
                serverSide: true,
                language: {
                    'lengthMenu': '<span class="assign_class label'+$('#show_label').attr('data-id')+'" data-id="'+$('#show_label').attr('data-id')+'" data-value="'+$('#show_label').val()+'" ></span>  _MENU_ <span class="assign_class label'+$('#entries_label').attr('data-id')+'" data-id="'+$('#entries_label').attr('data-id')+'" data-value="'+$('#entries_label').val()+'" ></span>',
                    'search':'<span class="assign_class label'+$('#search_id').attr('data-id')+'" data-id="'+$('#search_id').attr('data-id')+'" data-value="'+$('#search_id').val()+'" >'+$('#search_id').val()+'</span>',
                    'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                    'info': '<span class="assign_class label'+$('#showing_page_label').attr('data-id')+'" data-id="'+$('#showing_page_label').attr('data-id')+'" data-value="'+$('#showing_page_label').val()+'" >'+$('#showing_page_label').val()+'</span> _PAGE_ <span class="assign_class label'+$('#of_label').attr('data-id')+'" data-id="'+$('#of_label').attr('data-id')+'" data-value="'+$('#of_label').val()+'" >'+$('#of_label').val()+'</span> _PAGES_',
                    'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                    'paginate': {
                        'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                        'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                    },
                    'infoFiltered': " "
                },
                "ajax": {
                    "url": 'getalluser',
                    "type": 'get',
                    "data": {'id':company_id}
                },
              "createdRow": function( row, data, dataIndex ) {
                
                    var checkbox_permission=$('#checkbox_permission').val();
                   
                    console.log(data);
                    $(row).attr('id', data['id']);
                    var temp=data['id'];
                    $(row).attr('onclick',"selectedrowuser(this.id)");
                },
                columns: [
                {data: 'actions', name: 'actions'},
                {data: 'checkbox', name: 'checkbox',visible:$('#checkbox_permission').val(),},
                    {data: 'name', name: 'name',visible:$('#name_permission').val(),},
                    {data: 'email', name: 'email',visible:$('#email_permission').val(),},

                ],
               

                columnDefs: [{
                orderable: false,
                className: 'select-checkbox',
                targets: 1
                }],
                
            
                select: {
                style: 'os',
                selector: 'td:first-child'
                },

            });
            
        }
    </script>
@endsection
