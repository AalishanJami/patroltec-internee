@if(false)
<!--  <div class="md-form mb-4">
    <div class="row">
        <div class="col-md-12">
            <select searchable="Search here.."  id="companies_selected" class="mdb-select md-form" multiple  >
                <option value="" disabled selected><span class="assign_class label{{getKeyid('Choose_company',$data)}}" data-id="{{getKeyid('Choose_company',$data) }}" data-value="{{checkKey('Choose_company',$data) }}" >{!! checkKey('Choose_company',$data) !!} </span></option>
                <option value="0"> All Companies</option>
                @foreach($companies as $company)
                    <option value="{{$company->id}}">
                        {{$company->name}}
                    </option>
                @endforeach

            </select>
            <label class="mdb-main-label"><span class="assign_class label{{getKeyid('select_company',$data)}}" data-id="{{getKeyid('select_company',$data) }}" data-value="{{checkKey('select_company',$data) }}" >{!! checkKey('select_company',$data) !!} </span></label>
        </div>
    </div>
</div> -->
@endif


<ul class="list-group-flush">
    <li class="list-group-item">
        <div class="form-check">
            <input type="checkbox" class="form-check-input companies_selected" id="all_companies" value="0">
            <label class="form-check-label" for="all_companies">All Companies</label>
        </div>
    </li>
    @foreach($companies as $company)
        <li class="list-group-item">
            <div class="form-check">
                <input type="checkbox" class="form-check-input company companies_selected" id="company{{$company->id}}" value="{{$company->id}}">
                <label class="form-check-label" for="company{{$company->id}}">{{$company->name}}</label>
            </div>
        </li>

    @endforeach

</ul>

 