 @php
        $data=localization();
@endphp
<style type="text/css">
   .form-check-input {
    position: absolute !important;
    pointer-events: none !important;
    opacity: 0!important;
}
</style>
<div class="modal fade" id="modalRegisterForm2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold"><span class="assign_class label{{getKeyid('users.model.create.title',$data)}}" data-id="{{getKeyid('users.model.create.title',$data) }}" data-value="{{checkKey('users.model.create.title',$data) }}" >{!! checkKey('users.model.create.title',$data) !!} </span></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="registernewuser2" method="POST" >
                @csrf
                <div class="modal-body mx-3">
                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input id="newuser_name2" type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}"  autocomplete="name" autofocus>
                        @php
                            $id=getKeyid('users.model.create.name.msg',$data);
                            $value=checkKey('users.model.create.name.msg',$data);
                            $title=$id.'---'.$value;  
                        @endphp
                        <label data-error="wrong" data-success="right" id="newuser_name2_id" for="name">
                            <span class="assign_class label{{getKeyid('users.model.create.your_name',$data)}}" data-id="{{getKeyid('users.model.create.your_name',$data) }}" data-value="{{checkKey('users.model.create.your_name',$data) }}" >{!! checkKey('users.model.create.your_name',$data) !!}
                            </span>
                              <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                                data-content="{{$title}}"></i>
                        </label>
                    </div>
                    <div class="md-form mb-5">
                        <i class="fas fa-envelope prefix grey-text"></i>
                        <input id="newuser_email2" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"  autocomplete="email">
                         @php
                            $id=getKeyid('users.model.create.email.msg',$data);
                            $value=checkKey('users.model.create.email.msg',$data);
                            $title=$id.'---'.$value;  
                        @endphp
                        <label data-error="wrong" data-success="right" id="newuser_email2_id"  for="orangeForm-email"><span class="assign_class label{{getKeyid('users.model.create.your_email',$data)}}" data-id="{{getKeyid('users.model.create.your_email',$data) }}" data-value="{{checkKey('users.model.create.your_email',$data) }}" >{!! checkKey('users.model.create.your_email',$data) !!} </span>
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                                data-content="{{$title}}"></i>
                        </label>
                    </div>

                    <div class="md-form mb-5">
                        <i class="fas fa-envelope prefix grey-text"></i>
                        <div id="companyCreateModel"></div>
                        @php
                            $id=getKeyid('users.model.create.company.msg',$data);
                            $value=checkKey('users.model.create.company.msg',$data);
                            $title=$id.'---'.$value;  
                        @endphp
                        <label data-error="wrong" data-success="right" ><span class="assign_class label{{getKeyid('usersusers.model.create.company',$data)}}" data-id="{{getKeyid('users.model.create.company',$data) }}" data-value="{{checkKey('users.model.create.company',$data) }}" >{!! checkKey('users.model.create.company',$data) !!} </span>
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                                data-content="{{$title}}"></i>
                        </label>
                    </div>

                    

                    <div class="md-form mb-4">
                        <i class="fas fa-lock prefix grey-text"></i>
                        <input id="newuser_password2" type="password" class="form-control @error('password') is-invalid @enderror" name="password"  autocomplete="new-password">
                         @php
                            $id=getKeyid('users.model.create.password.msg',$data);
                            $value=checkKey('users.model.create.password.msg',$data);
                            $title=$id.'---'.$value;  
                        @endphp
                        <label data-error="wrong" data-success="right" for="orangeForm-pass"><span class="assign_class label{{getKeyid('users.model.create.your_password',$data)}}" data-id="{{getKeyid('users.model.create.your_password',$data) }}" data-value="{{checkKey('users.model.create.your_password',$data) }}" >{!! checkKey('users.model.create.your_password',$data) !!} </span>
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                                data-content="{{$title}}"></i>
                        </label>
                    </div>


                    <div class="md-form mb-4">
                        <i class="fas fa-lock prefix grey-text"></i>
                         @php
                            $id=getKeyid('users.model.create.confirm_password.msg',$data);
                            $value=checkKey('users.model.create.confirm_password.msg',$data);
                            $title=$id.'---'.$value;  
                        @endphp
                        <label data-error="wrong" data-success="right" for="orangeForm-pass"><span class="assign_class label{{getKeyid('users.model.create.confirm_password',$data)}}" data-id="{{getKeyid('users.model.create.confirm_password',$data) }}" data-value="{{checkKey('users.model.create.confirm_password',$data) }}" >{!! checkKey('users.model.create.confirm_password',$data) !!} </span>
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                                data-content="{{$title}}"></i>
                        </label>
                        <input id="newuser_password_confirm2" type="password" class="form-control" name="password_confirmation"  autocomplete="new-password">
                    </div>
                   


                </div>


                <div class="modal-footer d-flex justify-content-center">
                    <button class="btn btn-primary btn-sm" type="submit"><span class="assign_class label{{getKeyid('users.model.create.save',$data)}}" data-id="{{getKeyid('users.model.create.save',$data) }}" data-value="{{checkKey('users.model.create.save',$data) }}" >{!! checkKey('users.model.create.save',$data) !!} </span></button>
                </div>
            </form>
        </div>
    </div>
</div>
