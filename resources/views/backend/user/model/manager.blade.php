@php
    $data=localization();
@endphp
<div class="modal fade" id="modalmanagerIndex" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">
                    <span class="assign_class label{{getKeyid('manager',$data)}}" data-id="{{getKeyid('manager',$data) }}" data-value="{{checkKey('manager',$data) }}" >
                        {!! checkKey('manager',$data) !!}
                    </span>
                    @if(Auth()->user()->hasPermissionTo('view_cms') || Auth::user()->all_companies == 1 )
                        <button data-class="edit-disable" class="edit_cms_enable edit_cms_switch btn btn-primary btn-sm waves-effect waves-light" style="display: none;" >
                            Edit cms
                        </button>
                        <button  data-class="edit-enable" class="edit_cms_disable edit_cms_switch btn btn-danger btn-sm waves-effect waves-light">
                            Edit cms
                        </button>
                    @endif
                </h4>
                <button type="button" class="close manager_listing_close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="card">
                <div class="card-body">
                    <div id="table-manager" class="table-responsive table-editable">
                        @if(Auth()->user()->hasPermissionTo('create_managerName') || Auth::user()->all_companies == 1 )
                            <span class="add-manager table-add float-right mb-3 mr-2">
                                <a class="text-success">
                                    <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
                                </a>
                            </span>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_delete_managerName') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-danger btn-sm" id="deleteselectedmanager">
                                <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                                    {!! checkKey('selected_delete',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_delete_managerName') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-danger btn-sm" style="display: none;" id="deleteselectedmanagerSoft">
                                <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                                    {!! checkKey('selected_delete',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_active_managerName') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-success btn-sm" style="display: none;" id="selectedactivebuttonmanager">
                                <span class="assign_class label{{getKeyid('selected_active',$data)}}" data-id="{{getKeyid('selected_active',$data) }}" data-value="{{checkKey('selected_active',$data) }}" >
                                    {!! checkKey('selected_active',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('restore_delete_managerName') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-primary btn-sm" id="restorebuttonmanager">
                                <span class="assign_class label{{getKeyid('restore',$data)}}" data-id="{{getKeyid('restore',$data) }}" data-value="{{checkKey('restore',$data) }}" >
                                    {!! checkKey('restore',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('active_managerName') || Auth::user()->all_companies == 1 )
                            <button  style="display: none;" class="btn btn-primary btn-sm" id="activebuttonmanager">
                                <span class="assign_class label{{getKeyid('show_active',$data)}}" data-id="{{getKeyid('show_active',$data) }}" data-value="{{checkKey('show_active',$data) }}" >
                                    {!! checkKey('show_active',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('csv_managerName') || Auth::user()->all_companies == 1 )
                        <form class="form-style" method="POST" id="export_excel_manager" action="{{ url('manager/export/excel') }}">
                            {!! csrf_field() !!}
                            <input type="hidden" class="manager_export" name="excel_array" value="1">
                            <button  type="submit" class="form_submit_check btn btn-warning btn-sm">
                                <span class="assign_class label{{getKeyid('excel_export',$data)}}" data-id="{{getKeyid('excel_export',$data) }}" data-value="{{checkKey('excel_export',$data) }}" >
                                    {!! checkKey('excel_export',$data) !!}
                                </span>
                            </button>
                        </form>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('word_managerName') || Auth::user()->all_companies == 1 )
                        <form class="form-style" method="POST" id="export_world_manager" action="{{ url('manager/export/world') }}">
                            {!! csrf_field() !!}
                            <input type="hidden" class="manager_export" name="word_array" value="1">
                            <button  type="submit" class="form_submit_check btn btn-success btn-sm">
                                <span class="assign_class label{{getKeyid('word_export',$data)}}" data-id="{{getKeyid('word_export',$data) }}" data-value="{{checkKey('word_export',$data) }}" >
                                    {!! checkKey('word_export',$data) !!}
                                </span>
                            </button>
                        </form>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('pdf_managerName') || Auth::user()->all_companies == 1 )
                        <form  class="form-style" {{pdf_view('users')}} id="export_pdf_manager" method="POST" action="{{ url('manager/export/pdf') }}">
                            {!! csrf_field() !!}
                            <input type="hidden" class="manager_export" name="pdf_array" value="1">
                            <button  type="submit" class="form_submit_check btn btn-secondary btn-sm">
                                <span class="assign_class label{{getKeyid('pdf_export',$data)}}" data-id="{{getKeyid('pdf_export',$data) }}" data-value="{{checkKey('pdf_export',$data) }}" >
                                    {!! checkKey('pdf_export',$data) !!}
                                </span>
                            </button>
                        </form>
                        @endif
                        <div id="table" class="table-responsive table-editable">
                            <table id="manager_table" class="table table-bordered table-responsive-md table-striped">
                                <thead>
                                <tr>
                                    @if(Auth()->user()->hasPermissionTo('managerName_checkbox') || Auth::user()->all_companies == 1 )
                                        <th class="no-sort all_checkboxes_style">
                                            <div class="form-check">
                                                <input type="checkbox" class="form-check-input managername_checked" id="managername_checkbox_all">
                                                <label class="form-check-label" for="managername_checkbox_all">
                                                    <span class="assign_class label{{getKeyid('all',$data)}}" data-id="{{getKeyid('all',$data) }}" data-value="{{checkKey('all',$data) }}" >
                                                        {!! checkKey('all',$data) !!}
                                                    </span>
                                                </label>
                                            </div>
                                        </th>
                                    @endif
                                    @if(Auth()->user()->hasPermissionTo('managerName_name') || Auth::user()->all_companies == 1 )
                                        <th>
                                            <span class="assign_class label{{getKeyid('name',$data)}}" data-id="{{getKeyid('name',$data) }}" data-value="{{checkKey('name',$data) }}" >
                                                {!! checkKey('name',$data) !!}
                                            </span>
                                        </th>
                                    @endif
                                    @if(Auth()->user()->hasPermissionTo('managerName_name') || Auth::user()->all_companies == 1 )
                                        <th>
                                            Surname
                                        </th>
                                    @endif
                                    @if(Auth()->user()->hasPermissionTo('managerName_position') || Auth::user()->all_companies == 1 )
                                        <th>
                                            <span class="assign_class label{{getKeyid('position',$data)}}" data-id="{{getKeyid('position',$data) }}" data-value="{{checkKey('position',$data) }}" >
                                                {!! checkKey('position',$data) !!}
                                            </span>
                                        </th>
                                    @endif
                                    <th class="no-sort all_action_btn"></th>
                                </tr>
                                </thead>
                                <tbody id="manager_data">
                                </tbody>
                            </table>
                        </div>
                        @if(Auth()->user()->all_companies == 1)
                            <input type="hidden" id="managerName_checkbox" value="1">
                            <input type="hidden" id="managerName_name" value="1">
                            <input type="hidden" id="managerName_position" value="1">
                            <input type="hidden" id="edit_managerName" value="1">
                            <input type="hidden" id="delete_managerName" value="1">
                            <!----edit---->
                            <input type="hidden" id="managerName_name_edit" value="1">
                            <input type="hidden" id="managerName_position_edit" value="1">
                            <!----create---->
                            <input type="hidden" id="managerName_name_create" value="1">
                            <input type="hidden" id="managerName_position_create" value="1">
                        @else
                            <input type="hidden" id="managerName_checkbox" value="{{Auth()->user()->hasPermissionTo('managerName_checkbox')}}">
                            <input type="hidden" id="managerName_name" value="{{Auth()->user()->hasPermissionTo('managerName_name')}}">
                            <input type="hidden" id="managerName_position" value="{{Auth()->user()->hasPermissionTo('managerName_position')}}">
                            <input type="hidden" id="edit_managerName" value="{{Auth()->user()->hasPermissionTo('edit_managerName')}}">
                            <input type="hidden" id="delete_managerName" value="{{Auth()->user()->hasPermissionTo('delete_managerName')}}">
                            <!----edit---->
                            <input type="hidden" id="managerName_name_edit" value="{{Auth()->user()->hasPermissionTo('managerName_name_edit')}}">
                            <input type="hidden" id="managerName_position_edit" value="{{Auth()->user()->hasPermissionTo('managerName_position_edit')}}">
                            <!----create---->
                            <input type="hidden" id="managerName_name_create" value="{{Auth()->user()->hasPermissionTo('managerName_name_create')}}">
                            <input type="hidden" id="managerName_position_create" value="{{Auth()->user()->hasPermissionTo('managerName_position_create')}}">
                        @endif
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script type="text/javascript">

    function checkSelectedManger(value,checkValue) {
        if(value == checkValue) {
            return 'checked';
        } else {
            return "";
        }
    }
    function manager_data(url) {
        $.ajax({
            type: 'GET',
            url: url,
            success: function(response)
            {
                $('#manager_table').DataTable().clear().destroy();
                var table = $('#manager_table').dataTable(
                    {
                        processing: true,
                        language: {
                            'search':'<span class="assign_class label'+$('#search_id').attr('data-id')+'" data-id="'+$('#search_id').attr('data-id')+'" data-value="'+$('#search_id').val()+'" >'+$('#search_id').val()+'</span>',
                            'paginate':{
                                'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                                'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                            },
                            'lengthMenu': '_MENU_',
                            'info': ' ',
                        },
                        "ajax": {
                            "url": url,
                            "type": 'get',
                        },
                        "createdRow": function( row, data,dataIndex,columns )
                        {
                            var checkbox_permission=$('#checkbox_permission').val();
                            var checkbox='';
                            checkbox+='<div class="form-check"><input type="checkbox" class="form-check-input selectedrowmanager manager_checked" id="manager'+data.id+'"><label class="form-check-label" for="manager'+data.id+'""></label></div>';
                            var submit='';
                            submit+='<a type="button" class="btn btn-primary btn-xs my-0 waves-effect all_action_btn_margin waves-light save_manager_data manager_show_button_'+data.id+' show_tick_btn'+data.id+'" style="display: none;" id="'+data.id+'"><i class="fas fa-check"></i></a>';
                            $(columns[0]).html(checkbox);
                            $(columns[1]).attr('id', 'nameM'+data['id']);
                            $(columns[1]).attr('data-id', data['id']);
                            if ($("#managerName_name_edit").val()==1 && $('#edit_managerName').val())
                            {
                                $(columns[1]).attr('Contenteditable', 'true');
                            }

                            $(columns[1]).attr('onkeydown', 'editSelectedRow(manager_tr'+data['id']+')');
                            $(columns[2]).attr('id', 'nameS'+data['id']);
                            $(columns[2]).attr('data-id', data['id']);
                            $(columns[2]).attr('class','edit_inline_manager');
                            $(columns[2]).attr('Contenteditable', 'true');
                            $(columns[2]).attr('onkeydown', 'editSelectedRow(manager_tr'+data['id']+')');

                            var p_edit="";
                            if (!$("#managerName_position_edit").val() || !$('#edit_managerName').val()) {
                                var p_edit="disabled";
                            }
                            var f_id="manager_tr"+data.id;
                            var position = '';
                            position += '<div class="form-check float-left">';
                            position += ' <input type="radio" '+p_edit+' onclick="editSelectedRow('+f_id+')" value="1" ' + checkSelectedManger(data.position,1) + ' name="position' + data.id + '"  class="position' + data.id + ' form-check-input" id="staff_position' + data.id + '">';
                            position += '<label class="form-check-label" for="staff_position' + data.id + '">Staff</label>';
                            position += '</div>';
                            position += '<div class="form-check float-left">';
                            position += ' <input type="radio" '+p_edit+' onclick="editSelectedRow('+f_id+')" value="2" ' + checkSelectedManger(data.position, 2) + '  name="position' + data.id + '"  class="position' + data.id + ' form-check-input" id="manager_position' + data.id + '">';
                            position += '<label class="form-check-label" for="manager_position' + data.id + '">Manager</label>';
                            position += '</div>';
                            position += '<div class="form-check float-left">';
                            position += ' <input type="radio" '+p_edit+' onclick="editSelectedRow('+f_id+')" value="3" ' + checkSelectedManger(data.position, 3) + '  name="position' + data.id + '"  class="position' + data.id + ' form-check-input" id="contract_manager_position' + data.id + '">';
                            position += '<label class="form-check-label" for="contract_manager_position' + data.id + '">Contract Manager</label>';
                            position += '</div>';

                            $(columns[1]).attr('class','edit_inline_manager');
                            $(columns[3]).html(position);
                            $(columns[3]).attr('id', 'position'+data['id']);
                            $(columns[3]).attr('data-id', data['id']);
                            $(columns[3]).attr('class','edit_inline_manager');
                            if($('#edit_managerName').val()==1) {
                                $(columns[4]).append(submit);
                            }
                            $(row).attr('id', 'manager_tr'+data['id']);
                            //$(row).attr('class', 'selectedrowmanager');
                            $(row).attr('data-id', data['id']);
                        },
                        columns: [
                            {data: 'checkbox', name: 'checkbox',visible:$('#managerName_checkbox').val()},
                            {data: 'first_name', name: 'first_name',visible:$('#managerName_name').val()},
                            {data: 'surname', name: 'surname',visible:$('#managerName_name').val()},
                            {data: 'position', name: 'position',visible:$('#managerName_position').val()},
                            {data: 'actions', name: 'actions'},
                        ],
                        "columnDefs": [ {
                            "targets": [0,1],
                            "orderable": false
                        } ]

                    }
                );
            },
            error: function (error) {
                console.log(error);
            }
        });

        if ($(":checkbox").prop('checked',true)){
            $(":checkbox").prop('checked',false);
        }
    }

    $(document).ready(function () {
        var url='/manager/getall';
        manager_data(url);

    });
    $('.manager_listing_close').click(function(){
        $.ajax({
            type: "GET",
            url: '/manager/getalldropdown',
            success: function(response)
            {
                console.log(response);
                var html='';
                var manager_id=$("#manager_id").children('option:selected').val();
                for (var i = response.data.length - 1; i >= 0; i--) {
                    var selected='';
                    if(response.project.manager_id == response.data[i].id)
                    {
                        selected='selected';
                    }
                    html+='<option '+selected+' value="'+response.data[i].id+'">'+response.data[i].first_name+'</option>';
                }
                if (manager_id==''){
                    html+='<option selected disabled value="">---Please Select---</option>';
                }else{
                    html+='<option  disabled value="">---Please Select---</option>';
                }


                $('#manager_id').empty();
                $('#manager_id').html(html);

                html='';
                var contract_manager_id=$("#manager_id").children('option:selected').val();
                for (var i = response.contract_managers.length - 1; i >= 0; i--) {
                    var selected='';
                    if(response.project.contract_manager_id == response.contract_managers[i].id)
                    {
                        selected='selected';
                    }
                    html+='<option '+selected+' value="'+response.contract_managers[i].id+'">'+response.contract_managers[i].first_name+'</option>';
                }

                if (contract_manager_id==''){
                    html+='<option selected disabled value="">---Please Select---</option>';
                }else{
                    html+='<option  disabled value="">---Please Select---</option>';
                }
                $('#contract_manager_id').empty();
                $('#contract_manager_id').html(html);


            },
            error: function (error) {
                console.log(error);
            }
        });
    });
</script>
