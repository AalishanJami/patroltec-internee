@php
        $data=localization();
@endphp

<style type="text/css">
    .form-check-input {
        position: absolute !important;
        pointer-events: none !important;
        opacity: 0!important;
    }
    @media (min-width: 576px) {
        .modal-dialog {
            max-width: 75% !important;
        }
    }
    #contratc_manager_table>tbody>tr>td:nth-child(1),#contratc_manager_table>tbody>tr>td:nth-child(2),#contratc_manager_table>tbody>tr>td:nth-child(5){
        width: 7% !important;
        text-align: center;
    }
</style>
<div class="modal fade" id="modalcontrator_contratormanagerIndex" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold"><span>Contract Manager Name </span></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="card">
                <div class="card-body">
                    @if(Auth()->user()->hasPermissionTo('create_contractmanagerName') || Auth::user()->all_companies == 1 )
                        <span class="add-contratormanager table-add float-right mb-3 mr-2">
                        <a class="text-success"><i class="fas fa-plus fa-2x" aria-hidden="true"></i></a>
                    </span>
                    @endif
                    <div id="table-contratormanager" class="table-editable">
                        @if(Auth()->user()->hasPermissionTo('selected_delete_contractmanagerName') || Auth::user()->all_companies == 1 )
                        <button class="btn btn-danger btn-sm" id="deleteselectedcontratormanager">
                            Delete Selected contratormanager
                        </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_delete_contractmanagerName') || Auth::user()->all_companies == 1 )
                        <button class="btn btn-danger btn-sm" style="display: none;" id="deleteselectedcontratormanagerSoft">
                          Delete Selected contratormanager
                        </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_active_contractmanagerName') || Auth::user()->all_companies == 1 )
                        <button class="btn btn-success btn-sm" style="display: none;" id="selectedactivebuttoncontratormanager">
                            Active Selected contratormanager
                        </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_restore_delete_contractmanagerName') || Auth::user()->all_companies == 1 )
                        <button class="btn btn-primary btn-sm" id="restorebuttoncontratormanager">
                            Restore Deleted contratormanager
                        </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('active_contractmanagerName') || Auth::user()->all_companies == 1 )
                        <button  style="display: none;" class="btn btn-primary btn-sm" id="activebuttoncontratormanager">
                            Show Active contratormanager
                        </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('csv_contractmanagerName') || Auth::user()->all_companies == 1 )
                        <form class="form-style" method="POST" id="export_excel_contratormanager" action="{{ url('contratormanager/export/excel') }}">
                            {!! csrf_field() !!}
                            <input type="hidden" class="contratormanager_export" name="excel_array" value="1">
                            <button  type="submit" class="btn btn-warning btn-sm">
                                Excel Export
                            </button>
                        </form>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('word_contractmanagerName') || Auth::user()->all_companies == 1 )
                        <form class="form-style" method="POST" id="export_world_contratormanager" action="{{ url('contratormanager/export/world') }}">
                            {!! csrf_field() !!}
                            <input type="hidden" class="contratormanager_export" name="word_array" value="1">
                            <button  type="submit" class="btn btn-success btn-sm">
                               Word Export
                            </button>
                        </form>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('pdf_contractmanagerName') || Auth::user()->all_companies == 1 )
                        <form  class="form-style" id="export_pdf_contratormanager" method="POST" action="{{ url('contratormanager/export/pdf') }}">
                            {!! csrf_field() !!}
                            <input type="hidden" class="contratormanager_export" name="pdf_array" value="1">
                            <button  type="submit" class="btn btn-secondary btn-sm">
                                Pdf Export
                            </button>
                        </form>
                        @endif
                        <div id="table" class="table-editable">
                            <table id="contratc_manager_table" class="table table-bordered table-responsive-md table-striped">
                                <thead>
                                <tr>
                                    @if(Auth()->user()->hasPermissionTo('contractmanagerName_checkbox') || Auth::user()->all_companies == 1 )
                                        <th class="no-sort"></th>
                                    @endif
                                    @if(Auth()->user()->hasPermissionTo('delete_contractmanagerName') || Auth::user()->all_companies == 1 )
                                        <th id="action" class="no-sort"></th>
                                    @endif
                                    @if(Auth()->user()->hasPermissionTo('contractmanagerName_name') || Auth::user()->all_companies == 1 )
                                        <th> Name</th>
                                    @endif
                                    @if(Auth()->user()->hasPermissionTo('contractmanagerName_position') || Auth::user()->all_companies == 1 )
                                        <th> Postion</th>
                                    @endif
                                    @if(Auth()->user()->hasPermissionTo('edit_contractmanagerName') || Auth::user()->all_companies == 1 )
                                        <th class="no-sort"></th>
                                    @endif
                                </tr>
                                </thead>
                                <tbody id="contratormanager_data">
                                </tbody>
                            </table>
                        </div>
                        @if(Auth()->user()->all_companies == 1)
                            <input type="hidden" id="contractmanagerName_checkbox" value="1">
                            <input type="hidden" id="contractmanagerName_name" value="1">
                            <input type="hidden" id="contractmanagerName_position" value="1">
                            <input type="hidden" id="edit_contractmanagerName" value="1">
                            <input type="hidden" id="delete_contractmanagerName" value="1">

                            <!-----edit----->
                            <input type="hidden" id="contractmanagerName_name_edit" value="1">
                            <input type="hidden" id="contractmanagerName_position_edit" value="1">

                            <!-----create----->
                            <input type="hidden" id="contractmanagerName_name_create" value="1">
                            <input type="hidden" id="contractmanagerName_position_create" value="1">
                        @else
                            <input type="hidden" id="contractmanagerName_checkbox" value="{{Auth()->user()->hasPermissionTo('contractmanagerName_checkbox')}}">
                            <input type="hidden" id="contractmanagerName_name" value="{{Auth()->user()->hasPermissionTo('contractmanagerName_name')}}">
                            <input type="hidden" id="contractmanagerName_position" value="{{Auth()->user()->hasPermissionTo('contractmanagerName_position')}}">
                            <input type="hidden" id="edit_contractmanagerName" value="{{Auth()->user()->hasPermissionTo('edit_contractmanagerName')}}">
                            <input type="hidden" id="delete_contractmanagerName" value="{{Auth()->user()->hasPermissionTo('delete_contractmanagerName')}}">

                            <!-----edit----->
                            <input type="hidden" id="contractmanagerName_name_edit" value="{{Auth()->user()->hasPermissionTo('contractmanagerName_name_edit')}}">
                            <input type="hidden" id="contractmanagerName_position_edit" value="{{Auth()->user()->hasPermissionTo('contractmanagerName_position_edit')}}">
                            <!-----edit----->
                            <input type="hidden" id="contractmanagerName_name_create" value="{{Auth()->user()->hasPermissionTo('contractmanagerName_name_create')}}">
                            <input type="hidden" id="contractmanagerName_position_create" value="{{Auth()->user()->hasPermissionTo('contractmanagerName_position_create')}}">
                        @endif
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>



<script type="text/javascript">
    function checkSelectedContratorManger(value,checkValue)
    {
        if(value == checkValue)
        {

            return 'checked';

        }
        else
        {
            return "";
        }
    }
    function datatable_intialize_contracter() {
        $('#contratc_manager_table').dataTable({
            processing: true,
            language: {
                'lengthMenu': '_MENU_',
                'info': '',
            },

        });
    }
    function contratormanager_data(url) {
        $.ajax({
            type: 'GET',
            url: url,
            success: function(response)
            {
                var table = $('#contratc_manager_table').dataTable(
                    {
                        processing: true,
                        language: {
                            'search':'<span class="assign_class label'+$('#relationship_search_id').attr('data-id')+'" data-id="'+$('#relationship_search_id').attr('data-id')+'" data-value="'+$('#relationship_search_id').val()+'" >'+$('#relationship_search_id').val()+'</span>',
                            'paginate':{
                                'previous': '<span class="assign_class label'+$('#relationship_previous_label').attr('data-id')+'" data-id="'+$('#relationship_previous_label').attr('data-id')+'" data-value="'+$('#relationship_previous_label').val()+'" >'+$('#relationship_previous_label').val()+'</span>',
                                'next': '<span class="assign_class label'+$('#relationship_next_label').attr('data-id')+'" data-id="'+$('#relationship_next_label').attr('data-id')+'" data-value="'+$('#relationship_next_label').val()+'" >'+$('#relationship_next_label').val()+'</span>',
                            },
                            'lengthMenu': '_MENU_',
                            'info': ' ',
                        },
                        "ajax": {
                            "url": url,
                            "type": 'get',
                        },
                        "createdRow": function( row, data,dataIndex,columns )
                        {
                            var checkbox_permission=$('#checkbox_permission').val();
                            var checkbox='';
                            checkbox+='<div class="form-check"><input type="checkbox" class="form-check-input contratormanager_checked" id="contratormanager'+data.id+'"><label class="form-check-label" for="contratormanager'+data.id+'""></label></div>';
                            var submit='';
                            submit+='<a type="button" class="btn btn-primary btn-xs my-0 waves-effect save_contractor_manager_data waves-light save_contratormanager_data contratormanager_show_button_'+data.id+'" style="display: none;" id="'+data.id+'"><i class="fas fa-check"></i></a>';
                            $(columns[0]).html(checkbox);
                            $(columns[2]).attr('id', 'nameCM'+data['id']);
                            $(columns[2]).attr('data-id', data['id']);
                            if($('#contractmanagerName_name_edit').val()==1) {
                                $(columns[2]).attr('Contenteditable', 'true');
                            }
                            var p_edit=($("#contractmanagerName_position_edit").val() ==1) ? "":"disabled";
                            var position = '';
                            position += '<div class="form-check float-left">';
                            position += ' <input type="radio" '+p_edit+' value="C" ' + checkSelectedContratorManger(data.position, 'C') + ' name="cmposition' + data.id + '"  class="position' + data.id + ' form-check-input" id="cm_yes_position' + data.id + '">';
                            position += '<label class="form-check-label" for="cm_yes_position' + data.id + '">Yes</label>';
                            position += '</div>';
                            position += '<div class="form-check float-left">';
                            position += ' <input type="radio" '+p_edit+' value="0" ' + checkSelectedContratorManger(data.position, 0) + '  name="cmposition' + data.id + '"  class="position' + data.id + ' form-check-input" id="cm_no_position' + data.id + '">';
                            position += '<label class="form-check-label" for="cm_no_position' + data.id + '">No</label>';
                            position += '</div>';

                            $(columns[2]).attr('class','edit_inline_contratormanager');
                            $(columns[3]).html(position);
                            $(columns[3]).attr('id', 'position'+data['id']);
                            $(columns[3]).attr('data-id', data['id']);
                            $(columns[3]).attr('class','edit_inline_contratormanager');
                            if($('#edit_contractmanagerName').val()==1)
                            {
                                $(columns[4]).html(submit);
                            }
                            $(row).attr('id', 'contratormanager_tr'+data['id']);
                            $(row).attr('class', 'selectedrowcontratormanager');
                        },
                        columns:
                            [
                                {data: 'checkbox', name: 'checkbox',visible:$('#contractmanagerName_checkbox').val()},
                                {data: 'actions', name: 'actions'},
                                {data: 'name', name: 'name',visible:$('#contractmanagerName_name').val()},
                                {data: 'position', name: 'position',visible:$('#contractmanagerName_position').val()},
                                {data: 'submit', name: 'submit'},
                            ],

                    }
                );
            },
            error: function (error) {
                console.log(error);
            }
        });
        if ($(":checkbox").prop('checked',true)){
            $(":checkbox").prop('checked',false);
        }
    }

    $(document).ready(function () {
        $("body").on('click','.edit_inline_contratormanager',function () {
            $(".contratormanager_show_button_"+$(this).attr('data-id')).show();
        });
    });

    var url='/contratormanager/getall';
    contratormanager_data(url);
</script>
