 @php
        $data=localization();
@endphp

<div class="modal fade" id="modalEditForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold"><span class="assign_class label{{getKeyid('user',$data)}}" data-id="{{getKeyid('user',$data) }}" data-value="{{checkKey('user',$data) }}" >{!! checkKey('user',$data) !!} </span></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="edit_name_email" method="POST" action="{{ url('register') }}">
                @csrf
                <input type="hidden" id="user_id_hidden" >
                <div class="modal-body mx-3">
                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        @if(Auth()->user()->hasPermissionTo('user_name_edit') || Auth::user()->all_companies == 1 )
                            <input id="edit_user_name" type="text" class="form-control" name="name" required>
                            <label data-error="wrong" data-success="right" for="name" class="active"><span class="assign_class label{{getKeyid('name',$data)}}" data-id="{{getKeyid('name',$data) }}" data-value="{{checkKey('name',$data) }}" >{!! checkKey('name',$data) !!} </span>
                                @php
                                    $id=getKeyid('users.model.edit.name.msg',$data);
                                    $value=checkKey('users.model.edit.name.msg',$data);
                                    $title=$id.'---'.$value;
                                @endphp
                                <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                                data-content="{{$title}}"></i>

                            </label>
                        @else
                            <input id="edit_user_name" type="text" class="form-control" name="name" disabled required>
                        @endif
                    </div>
                    <div class="md-form mb-5">
                        <i class="fas fa-envelope prefix grey-text"></i>
                         @if(Auth()->user()->hasPermissionTo('user_email_edit') || Auth::user()->all_companies == 1 )
                             <input id="edit_user_email" type="email" class="form-control" name="email"  required>
                             <label data-error="wrong" data-success="right" for="orangeForm-email" class="active"><span class="assign_class label{{getKeyid('email',$data)}}" data-id="{{getKeyid('email',$data) }}" data-value="{{checkKey('email',$data) }}" >{!! checkKey('email',$data) !!} </span>
                                @php
                                    $id=getKeyid('users.model.edit.email.msg',$data);
                                    $value=checkKey('users.model.edit.email.msg',$data);
                                    $title=$id.'---'.$value;  
                                @endphp
                                <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                                data-content="{{$title}}"></i>
                             </label>
                        @else
                            <input id="edit_user_email" type="text" class="form-control" name="email" disabled required>
                        @endif
                    </div>

                      <div class="md-form mb-5">
                        <i class="fas fa-envelope prefix grey-text"></i>
                        <div id="select_edit"></div>
                        <label data-error="wrong" data-success="right" ><span class="assign_class label{{getKeyid('company',$data)}}" data-id="{{getKeyid('company',$data) }}" data-value="{{checkKey('company',$data) }}" >{!! checkKey('company',$data) !!} </span>
                            <i class="fa fa-question-circle" aria-hidden="true"></i>
                        </label>
                    </div>

                </div>

                <div class="modal-footer d-flex justify-content-center">
                    <button class="btn btn-primary btn-sm" type="submit"><span class="assign_class label{{getKeyid('update',$data)}}" data-id="{{getKeyid('update',$data) }}" data-value="{{checkKey('update',$data) }}" >{!! checkKey('update',$data) !!} </span></button>
                </div>
            </form>

            <div class="modal-body mx-3">
            @if(Auth()->user()->hasPermissionTo('user_password_edit') || Auth::user()->all_companies == 1 )
                <form id="edit_password" >
                    @csrf
                <div class="md-form mb-4">
                    <i class="fas fa-lock prefix grey-text"></i>
                    <input id="newuser_password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                    <label data-error="wrong" data-success="right" for="orangeForm-pass"><span class="assign_class label{{getKeyid('password',$data)}}" data-id="{{getKeyid('password',$data) }}" data-value="{{checkKey('password',$data) }}" >{!! checkKey('password',$data) !!} </span>
                        @php
                            $id=getKeyid('password',$data);
                            $value=checkKey('password',$data);
                            $title=$id.'---'.$value;  
                        @endphp
                        <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                        data-content="{{$title}}"></i>
                    </label>
                </div>
                <div class="md-form mb-4">
                    <i class="fas fa-lock prefix grey-text"></i>
                    <label data-error="wrong" data-success="right" for="orangeForm-pass"><span class="assign_class label{{getKeyid('confirm_password',$data)}}" data-id="{{getKeyid('confirm_password',$data) }}" data-value="{{checkKey('confirm_password',$data) }}" >{!! checkKey('confirm_password',$data) !!} </span>
                        @php
                            $id=getKeyid('confirm_password.msg',$data);
                            $value=checkKey('confirm_password.msg',$data);
                            $title=$id.'---'.$value;  
                        @endphp
                        <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                        data-content="{{$title}}"></i>
                    </label>
                    <input id="newuser_password_confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button class="btn btn-primary btn-sm" type="submit"><span class="assign_class label{{getKeyid('update',$data)}}" data-id="{{getKeyid('update',$data) }}" data-value="{{checkKey('update',$data) }}" >{!! checkKey('update',$data) !!} </span></button>
                </div>

                </form>
            @endif
            </div>
        </div>
    </div>
</div>
