<table>
  <thead>
    <tr>
      <th>ID</th>
      @if(Auth()->user()->hasPermissionTo('user_name_pdf_export') || Auth::user()->all_companies == 1 )
        <th>Name</th>
      @endif
      @if(Auth()->user()->hasPermissionTo('user_email_pdf_export') || Auth::user()->all_companies == 1 )
        <th>Email</th>
      @endif
    </tr>
  </thead>
  <tbody>
    @foreach($users as $data)
      <tr>
        <td>{{ $data->id }}</td>
          @if(Auth()->user()->hasPermissionTo('user_name_pdf_export') || Auth::user()->all_companies == 1 )
            <td>{{ $data->name }}</td>
          @endif
          @if(Auth()->user()->hasPermissionTo('user_email_pdf_export') || Auth::user()->all_companies == 1 )
            <td>{{ $data->email }}</td>
          @endif
      </tr>
    @endforeach
  </tbody>
</table>