
  <input type="hidden" name="user_id" value="{{$user->id}}">
   @if(Auth::user()->all_companies == 1)
                <div class="form-check">
                  <label>
                    <input type="checkbox" name="roles[]" id="Super Admin" value="0" class="form-check-input">
                    <label for="Super Admin" class="form-check-label">Super Admin</label>
                  </label>
                </div>


                @endif
    @foreach ($roles as $key=> $role)
  
            @if($user->roles->count()>0)
             @foreach ($user->roles as $data)
               @if($data->id == $role->id)
               <div class="form-check">
                 
                  <input type="checkbox"  class="form-check-input" name="roles[]" checked="checked" value="{{$role->id}}" id="role{{$role->id}}">
                    <label for="role{{$role->id}}" class="form-check-label">{{$role->name}}</label>


               </div>
               @else
                   <div class="form-check">
                 
                  <input type="checkbox" name="roles[]" class="form-check-input"  checked="checked" value="{{$role->id}}" id="role{{$role->id}}">
                    <label for="role{{$role->id}}" class="form-check-label">{{$role->name}}</label>


               </div>
               @endif
             @endforeach
             @else
                <div class="form-check">
                 
                  <input type="checkbox" name="roles[]" class="form-check-input" checked="checked" value="{{$role->id}}" id="role{{$role->id}}">
                    <label for="role{{$role->id}}" class="form-check-label">{{$role->name}}</label>


               </div>
            @endif
     
    @endforeach
