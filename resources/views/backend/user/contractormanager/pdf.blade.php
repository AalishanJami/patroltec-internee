<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
</head>
<body>

<table class="table table-striped table-bordered">
    <thead>
    <tr>
        @if(Auth()->user()->hasPermissionTo('contractmanagerName_name_pdf_export') || Auth::user()->all_companies == 1 )
            <th> Name</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('contractmanagerName_position_pdf_export') || Auth::user()->all_companies == 1 )
            <th> Position</th>
        @endif
    </tr>
    </thead>
    <tbody>
    @foreach($data as $value)
        <tr>
            @if(Auth()->user()->hasPermissionTo('contractmanagerName_name_pdf_export') || Auth::user()->all_companies == 1 )
                <td>{{ $value->name }}</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('contractmanagerName_position_pdf_export') || Auth::user()->all_companies == 1 )
                <td>{{ $value->position }}</td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>
</body>
