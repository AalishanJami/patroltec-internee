@extends('backend.layouts.user')
@section('title', 'Account')
@section('content')
    @php
       $data=localization();
    @endphp
    <link rel="stylesheet" href="{{ asset('custom/css/custom_style.css') }}">
    @include('backend.layouts.user_sidebar')
    @include('backend.label.input_label')
    <div class="padding-left-custom remove-padding">
        {!! breadcrumInner('account',Session::get('employee_id'),'users','/employee/detail/'.Session::get('employee_id'),'employees','/employee') !!}
        <div class="card">
            <h5 class="card-header info-color white-text text-center py-4">
                <strong>
                    <span class="assign_class label{{getKeyid('account',$data)}}" data-id="{{getKeyid('account',$data) }}" data-value="{{checkKey('account',$data) }}" >
                        {!! checkKey('account',$data) !!}
                    </span>
                </strong>
            </h5>
            <div class="card-body px-lg-5 pt-0">
                <form action="{{url('account/update')}}" method="POST" >
                    @csrf
                    <div class="modal-body mx-3">
                        @if(Auth()->user()->hasPermissionTo('employeeAccount_username_edit') || Auth::user()->all_companies == 1 )
                            <div class="md-form mb-5">
                                <i class="fas fa-user prefix grey-text"></i>
                                <input type="text" class="form-control" name="name" value="{{$user->name}}"  autocomplete="name" autofocus>
                                <label   for="name"  class="active">
                                    <span class="assign_class label{{getKeyid('user_name',$data)}}" data-id="{{getKeyid('user_name',$data) }}" data-value="{{checkKey('user_name',$data) }}" >
                                        {!! checkKey('user_name',$data) !!}
                                    </span>
                                </label>
                                @if ($errors->has('name'))
                                    <small class="text-danger" style="left: 4% !important;" id="form_name_required_message">{{ $errors->first('name') }}</small>
                                @endif
                            </div>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('employeeAccount_password_edit') || Auth::user()->all_companies == 1 )
                            <div class="md-form mb-5">
                                <i class="fas fa-user prefix grey-text"></i>
                                <input type="password" class="form-control" name="password" value=""  autofocus>
                                <label   for="password" class="active">
                                    <span class="assign_class label{{getKeyid('password',$data)}}" data-id="{{getKeyid('password',$data) }}" data-value="{{checkKey('password',$data) }}" >
                                        {!! checkKey('password',$data) !!}
                                    </span>
                                </label>
                            </div>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('employeeAccount_user_group_edit') || Auth::user()->all_companies == 1 )
                            <div class="md-form mb-5">
                                <i class="fas fa-user prefix grey-text"></i>
                                    <div class="md-form ml-4-5 col_left_margin">
                                       <select searchable="Search here.."  class="mdb-select" name="roles[]">
                                            <option value="" disabled>Please Select</option>
                                            @if(Auth::user()->all_companies == 1)
                                                <option value="0" {{$user->all_companies == 1 ? 'selected':'' }} >Super Admin</option>
                                            @endif
                                            @foreach($roles as $key =>$value)
                                                <option value="{{$value->id}}" {{isset($user->roles[0]->id) ? $user->roles[0]->id ==$value->id ? 'selected':'' :'' }} >{{$value->name}}</option>
                                            @endforeach()
                                         </select>
                                    </div>
                                <label   for="name" class="active">
                                    <span class="assign_class label{{getKeyid('roles',$data)}}" data-id="{{getKeyid('roles',$data) }}" data-value="{{checkKey('roles',$data) }}" >
                                        {!! checkKey('roles',$data) !!}
                                    </span>
                                    <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                                    data-content="tool tip for User Group"></i>
                                </label>
                            </div>
                        @endif
                        <input type="hidden" name="user_id" value="{{$user->id}}">
                        <div class="md-form mb-5">
                            <i class="fas fa-user prefix grey-text"></i>
                            <div class="md-form ml-5">
                                <select searchable="Search here.." class="md-form mdb-select" name="companies[]" multiple>
                                    <option disabled value="" selected>Company</option>
                                    @if(isset($companies))
                                        @foreach($companies as $key => $company)
                                            <option {{(in_array($company->id, $company_id)== 1)? 'selected' :''}} value="{{$company->id}}">
                                                {{$company->name}}
                                            </option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer d-flex justify-content-center">
                        <button class="form_submit_check btn btn-primary btn-sm" type="submit">
                            <span class="assign_class label{{getKeyid('update',$data)}}" data-id="{{getKeyid('update',$data) }}" data-value="{{checkKey('update',$data) }}" >
                                {!! checkKey('update',$data) !!}
                            </span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function() {
          // (function ($) {
          //   var active ='<span class="assign_class label'+$('#breadcrumb_account').attr('data-id')+'" data-id="'+$('#breadcrumb_account').attr('data-id')+'" data-value="'+$('#breadcrumb_account').val()+'" >'+$('#breadcrumb_account').val()+'</span>';
          //   $('.breadcrumb-item.active').html(active);
          //   var parent ='<span class="assign_class label'+$('#dashboard_label').attr('data-id')+'" data-id="'+$('#dashboard_label').attr('data-id')+'" data-value="'+$('#dashboard_label').val()+'" >'+$('#dashboard_label').val()+'</span>';
          //   $('.breadcrumb-item a').html(parent);
          // }(jQuery));
        });
    </script>
@endsection
