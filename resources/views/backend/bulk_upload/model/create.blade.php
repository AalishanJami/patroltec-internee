@php
    $data=localization();
@endphp
<style type="text/css">
    @media (min-width: 576px) {
        .modal-dialog {
            max-width: 75% !important;
        }
    }
    #bulk_upload_img_loader{
        position: absolute;
        z-index: 1000;
        top: 19%;
        left: 41%;
        width: 150px;
        height: 150px;
        display: none;
    }
    .title_required_filed,.file_required_filed{
        position: absolute;
        top: 92%;
        left: 3.5%;
    }
</style>
<div class="modal fade" id="modalUpload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">
                    <span class="assign_class label{{getKeyid('upload',$data)}}" data-id="{{getKeyid('upload',$data) }}" data-value="{{checkKey('upload',$data) }}" >
                        {!! checkKey('upload',$data) !!}
                    </span>
                    @if(Auth()->user()->hasPermissionTo('view_cms') || Auth::user()->all_companies == 1 )
                        <button data-class="edit-disable" class="edit_cms_enable edit_cms_switch btn btn-primary btn-sm waves-effect waves-light" style="display: none;" >
                            Edit cms
                        </button>
                        <button  data-class="edit-enable" class="edit_cms_disable edit_cms_switch btn btn-danger btn-sm waves-effect waves-light">
                            Edit cms
                        </button>
                    @endif
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <input type="hidden" name="break" id="break_location" value="m">
            @if(Auth()->user()->hasPermissionTo('create_documentUpload') || Auth::user()->all_companies == 1 )
                <form id="uploadForm" method="POST" action="{{url('client/storeUpload')}}" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="flag" value="{{$flag ?? ''}}">
                    <input type="hidden"  name="form_id" value="{{Session::get('form_id')}}">
                    <div class="modal-body mx-3">
                        <div class="row">

                            <div class="col">
                                <div class="md-form mb-5">
                                    @if(Auth()->user()->hasPermissionTo('documentUpload_title_create') || Auth::user()->all_companies == 1 )
                                        <i class="fas fa-user prefix grey-text"></i>
                                        <input type="text" class="form-control empty_input" name="title" id="title" placeholder="Title"  autocomplete="name" autofocus>
                                        <label data-error="wrong" data-success="right" for="name" class="active" >
                                            <span class="assign_class label{{getKeyid('title',$data)}}" data-id="{{getKeyid('title',$data) }}" data-value="{{checkKey('title',$data) }}" >
                                                {!! checkKey('title',$data) !!}
                                            </span>
                                        </label>
                                        <small class="title_required_filed text-danger"></small>
                                    @endif
                                </div>
                            </div>
                        <!-- <div class="col">

                                <div class="md-form mb-5">
                                    @if(Auth()->user()->hasPermissionTo('documentUpload_status_create') || Auth::user()->all_companies == 1 )
                            <div class="row">
                                <div class="col-md-1" style="max-width: 4.333333%;">
                                    <i class="fas fa-check prefix grey-text"></i>
                                </div>
                                <div class="col-md-11">
                                    <select name="status" id="status" class="empty_input mdb-select form-check-label">
                                        <option value="">Choose</option>
                                        <option value="extracted">Extracted </option>
                                        <option value="pending_extraction">Pending Extraction</option>
                                    </select>
                                    <label data-error="wrong" data-success="right" for="status" class="active" >
                                        <span class="assign_class label{{getKeyid('status',$data)}}" data-id="{{getKeyid('status',$data) }}" data-value="{{checkKey('status',$data) }}" >
                                                        {!! checkKey('status',$data) !!}
                                </span>
                            </label>
                        </div>
                    </div>
@endif
                            </div>
                        </div> -->
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="md-form mb-5">
                                     <label data-error="wrong" data-success="right" for="name" class="active" >
                                            <span class="assign_class label{{getKeyid('location',$data)}}" data-id="{{getKeyid('location',$data) }}" data-value="{{checkKey('location',$data) }}" >
                                                {!! checkKey('location',$data) !!}
                                            </span>
                                        </label>
                                    {!! Form::select('location_id',$locations,null, ['class' =>'mdb-select','id'=>'location_id','placeholder'=>'Select Location','searchable'=>'search here']) !!}
                                    <small id="er_project_name" style="left: 0%!important;margin-top:-15px;display:block; text-transform: initial !important;" class="text-danger error_message"></small>
                                </div>
                            </div>
                            <div class="col">
                                <div class="md-form">
                                    <label data-error="wrong" data-success="right" for="name" class="active" >
                                            <span class="assign_class label{{getKeyid('user_name',$data)}}" data-id="{{getKeyid('user_name',$data) }}" data-value="{{checkKey('user_name',$data) }}" >
                                                {!! checkKey('user_name',$data) !!}
                                            </span>
                                        </label>
                                    {!! Form::select('user_id',$users,null, ['class' =>' mdb-select colorful-select dropdown-primary md-form','id'=>'user_id','searchable'=>'search here','placeholder'=>'Person Name']) !!}
                                    <small id="er_user_id" style="left: 0%!important;margin-top:-15px;display:block; text-transform: initial !important;" class="text-danger error_message"></small>
                                </div>

                            </div>

                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="md-form">
                                    <label data-error="wrong" data-success="right" for="name" class="active" >
                                            <span class="assign_class label{{getKeyid('date',$data)}}" data-id="{{getKeyid('date',$data) }}" data-value="{{checkKey('date',$data) }}" >
                                                {!! checkKey('date',$data) !!}
                                            </span>
                                        </label>
                                    <input placeholder="Select date" type="text" value="{{$date}}" id="date_pick" name="date" class="form-control datepicker" >
                                    <small id="er_date" class="text-danger error_message"></small>

                                </div>
                            </div>
                            <div class="col">
                                <div class="md-form">
                                    <label data-error="wrong" data-success="right" for="name" class="active" >
                                            <span class="assign_class label{{getKeyid('time',$data)}}" data-id="{{getKeyid('time',$data) }}" data-value="{{checkKey('time',$data) }}" >
                                                {!! checkKey('time',$data) !!}
                                            </span>
                                        </label>
                                    <input placeholder="Selected time" value="{{$time}}" type="text" id="input_starttime" name="time" class="form-control timepicker" >
                                    <small id="er_time" class="text-danger error_message"></small>

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="md-form" id="qr_listing">

                                </div>
                                <small id="er_location_breakdown_id" style="left: 0%!important;margin-top:-15px;display:block; text-transform: initial !important;" class="text-danger error_message"></small>
                            </div>
                        </div>


                        @if(Auth()->user()->hasPermissionTo('documentUpload_upload_create') || Auth::user()->all_companies == 1 )
                            <div class="file-upload-wrapper">
                                <div class="spinner-border text-success" id="bulk_upload_img_loader"></div>
                                <input type="file" id="input-file-now" name="file" class="file-upload empty_input" />
                                <small class="file_required_filed text-danger"></small>
                            </div>
                        @endif
                        <br>
                        @if(Auth()->user()->hasPermissionTo('documentUpload_notes_create') || Auth::user()->all_companies == 1 )
                            <label data-error="wrong" data-success="right" for="name" class="active">
                                <span class="assign_class label{{getKeyid('notes',$data)}}" data-id="{{getKeyid('notes',$data) }}" data-value="{{checkKey('notes',$data) }}" >
                                    {!! checkKey('notes',$data) !!}
                                </span>
                            </label>
                            <div class="md-form mb-5" id="note">
                                <div class="summernote_inner body_notes" ></div>
                            </div>
                            <input type="hidden" name="notes" class="empty_input" id="notes_value">
                        @endif
                    </div>
                    <div class="modal-footer d-flex justify-content-center">
                        <button class="form_submit_check btn btn-primary btn-sm" type="submit">
                            <span class="assign_class label{{getKeyid('save',$data)}}" data-id="{{getKeyid('save',$data) }}" data-value="{{checkKey('save',$data) }}" >
                                {!! checkKey('save',$data) !!}
                            </span>
                        </button>
                    </div>
                </form>
            @endif
        </div>
    </div>
</div>

<script type="text/javascript">
    $("#uploadForm").on('submit',function (event) {
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        event.preventDefault();
        if(validate())
        {
            $("#bulk_upload_img_loader").show();
            var formData = $("#uploadForm").serializeObject();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: '{{route('client_storeUpload')}}',
                data:new FormData(this),
                // dataType:'JSON',
                contentType:false,
                cache:false,
                processData:false,
                success: function(data)
                {
                    toastr.options = {
                        "closeButton": false,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-top-center",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    };
                    if (data.status =="validation"){
                        $("#bulk_upload_img_loader").hide();
                        $(".file_required_filed").html(data.error);
                        toastr["error"](data.error);
                        return 0;
                    }

                    if(data.error_ms)
                    {
                         toastr["warning"](data.error_ms);
                    }

                    if(data.message)
                    {
                         toastr["success"](data.message);
                    }



                    $('#upload_table').DataTable().clear().destroy();
                    uploadAppend();
                    $("#bulk_upload_img_loader").hide();
                    $('#modalUpload').modal('hide');
                    $("#title").val('');
                    $("#status").children('option:selected').val('');
                    $("#note_value").val('');
                    $(".file-upload-preview").attr('style','display:none;');
                    $(".file-upload>button").attr('style','display:none;');
                    $('.note-editable').html('');
                    $('#uploadForm').trigger("reset");
                },
                error: function (error) {
                    console.log(error);
                }
            });

        }
    });
    $("body").on("change",'#input-file-now',function () {
        if ($(".has-preview>.file-upload-preview>.file-upload-render").html() !==""){
            $(".file-upload>button").attr('style','display:block;');
        }
    });
    function validate()
    {
        var location_id=$('#location_id').children("option:selected").val();
        var user_id=$('#user_id').val();
        var break_location=$('#break_location').val();
        var location_break_id=$('body').find('#location_breakdown_id').val();
        // var location_break_id = document.getElementById('location_breakdown_id').options[e.selectedIndex].value;

        var date = $('#date_pick').val();
        var time = $('#input_starttime').val();
        var fl = true;
        console.log(date,time,location_id);
        if($("#title").val() ==""){
            $("#title").attr('style','border-bottom:1px solid #e3342f');
            $(".title_required_filed").html('Title fileld required');
            fl=false;
        }
        if(location_id=='' || location_id==null )
        {
            console.log(location_id,"location");
            $("[data-activates=select-options-location_id]").attr('style','border-bottom:1px solid #e3342f');
            $("#project_name").focus();
            $("#er_project_name").html('Field required');
            fl= false;
        }else
        {
            $("[data-activates=select-options-location_id]").attr('style','border-bottom:1px solid #ced4da');
            $("#er_project_name").html('');

        }
        if(user_id=='' || user_id==null )
        {
            console.log(user_id,"user");

            $("[data-activates=select-options-user_id]").attr('style','border-bottom:1px solid #e3342f');
            $("#user_id").focus();
            $("#er_user_id").html('Field required');
            fl= false;
        } else
        {
            $("[data-activates=select-options-user_id]").attr('style','border-bottom:1px solid #ced4da');
            $("#er_user_id").html('');


        } if(date=='' || date==null )
    {
        console.log(date,"date");

        $("#date_pick").attr('style','border-bottom:1px solid #e3342f');
        $("#date_pick").focus();
        $("#er_date").html('Field required');
        fl= false;
    }else
    {
        $("#date_pick").attr('style','border-bottom:1px solid #ced4da');
        $("#er_date").html('');


    } if(time=='' || time==null )
    {
        console.log(time,"time");

        $("#input_starttime").attr('style','border-bottom:1px solid #e3342f');
        $("#input_starttime").focus();
        $("#er_time").html('Field required');
        fl= false;
    }else
    {
        $("#input_starttime").attr('style','border-bottom:1px solid #ced4da');
        $("#er_time").html('');


    }

        if(break_location!="m" )
        {
            console.log(break_location);
            if(location_break_id=='' || location_break_id==null )
            {
                console.log(location_break_id,"break");

                $("[data-activates=select-options-location_breakdown_id]").attr('style','border-bottom:1px solid #e3342f');
                $("#er_location_breakdown_id").html('Field required');
                fl= false;
            }
            else
            {
                $("[data-activates=select-options-location_breakdown_id]").attr('style','border-bottom:1px solid #ced4da');
                $("#er_location_breakdown_id").html('');


            }

        }

        return fl;

    }
    $('#input_starttime').pickatime();

</script>
