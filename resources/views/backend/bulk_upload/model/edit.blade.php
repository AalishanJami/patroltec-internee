 @php
        $data=localization();
@endphp
<div class="modal fade" id="modalEditbulk_import" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">
                    <span class="assign_class label{{getKeyid('upload',$data)}}" data-id="{{getKeyid('upload',$data) }}" data-value="{{checkKey('upload',$data) }}" >
                        {!! checkKey('upload',$data) !!}
                    </span>
                    @if(Auth()->user()->hasPermissionTo('view_cms') || Auth::user()->all_companies == 1 )
                        <button data-class="edit-disable" class="edit_cms_enable edit_cms_switch btn btn-primary btn-sm waves-effect waves-light" style="display: none;" >
                            Edit cms
                        </button>
                        <button  data-class="edit-enable" class="edit_cms_disable edit_cms_switch btn btn-danger btn-sm waves-effect waves-light">
                            Edit cms
                        </button>
                    @endif
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
              <form id="registernewcompany" method="POST" >
               <div class="modal-body mx-3">
                   
                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="title" required autocomplete="name" autofocus>
                        <label data-error="wrong" data-success="right" for="name" class="active" >
                            <span class="assign_class label{{getKeyid('title',$data)}}" data-id="{{getKeyid('title',$data) }}" data-value="{{checkKey('title',$data) }}" >
                                {!! checkKey('title',$data) !!}
                            </span>
                        </label>
                    </div>
                    
                    <div class="file-upload-wrapper">
                        <input type="file" id="input-file-now3" class="file-upload" />
                    </div>
                
                 
                </div>

                <div class="modal-footer d-flex justify-content-center">
                    <button class="form_submit_check btn btn-primary btn-sm" type="submit">
                        <span class="assign_class label{{getKeyid('update',$data)}}" data-id="{{getKeyid('update',$data) }}" data-value="{{checkKey('update',$data) }}" >
                            {!! checkKey('update',$data) !!}
                        </span>
                    </button>
                </div>
            </form>

            <div class="modal-body mx-3">

           
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
  
    $('#input-file-now3').file_upload();

</script>