<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
</head>
<body>
<table class="table table-striped table-bordered">
    <thead>
    <tr>
        @if($flag == 2)
            @if(Auth()->user()->hasPermissionTo('documentUpload_title_excel_export') || Auth::user()->all_companies == 1)
                <th> Title</th>
            @endif
            @if(Auth()->user()->hasPermissionTo('documentUpload_notes_excel_export') || Auth::user()->all_companies == 1)
                <th> Note</th>
            @endif
        @elseif($flag == 1)
            @if(Auth()->user()->hasPermissionTo('clientUpload_title_word_export') || Auth::user()->all_companies == 1)
                <th> Title</th>
            @endif
            @if(Auth()->user()->hasPermissionTo('clientUpload_notes_word_export') || Auth::user()->all_companies == 1)
                <th> Note</th>
            @endif
        @endif
    </tr>
    </thead>
    <tbody>
    @foreach($data as $value)
        <tr>
            @if($flag == 2)
                @if(Auth()->user()->hasPermissionTo('documentUpload_title_excel_export') || Auth::user()->all_companies == 1)
                    <td>{{ $value->name }}</td>
                @endif
                @if(Auth()->user()->hasPermissionTo('documentUpload_notes_excel_export') || Auth::user()->all_companies == 1)
                    <td>{{ $value->notes }}</td>
                @endif
            @elseif($flag == 1)
                @if(Auth()->user()->hasPermissionTo('clientUpload_title_word_export') || Auth::user()->all_companies == 1)
                    <td>{{ $value->name }}</td>
                @endif
                @if(Auth()->user()->hasPermissionTo('clientUpload_notes_word_export') || Auth::user()->all_companies == 1)
                    <td>{{ $value->notes }}</td>
                @endif
            @endif
        </tr>
    @endforeach
    </tbody>
</table>
</body>
