@extends('backend.layouts.backend')
@section('content')
    @php
       $data=localization();
    @endphp
        {{ Breadcrumbs::render('completed/list') }}

        <div class="card">
            <div class="card-body">
                <div id="table" class="table-editable">
                    <span class="table-add float-right mb-3 mr-2">
                        <!-- <a class="text-success"  data-toggle="modal" data-target="#modelaccount">
                            <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
                        </a> -->
                    </span>
                    <button class="btn btn-danger btn-sm">
                        Delete Selected Completed List
                    </button>
                    <button class="btn btn-primary btn-sm">
                      Restore Deleted Completed List
                    </button>

                    <button  style="display: none;">
                        Show Active Account
                    </button>
                    <button class="btn btn-warning btn-sm">
                       Excel Export
                    </button>
                    <button class="btn btn-success btn-sm">
                        Word Export
                    </button>
                    <div class="row mb-3 mr-2 mt-2">
                        <div class="form-check ">
                              <input type="checkbox" class="form-check-input company" id="date" value="1" onclick="columndate()">
                              <label class="form-check-label" for="date">Date</label>
                          </div>
                          <div class="form-check ">
                              <input type="checkbox" class="form-check-input company" id="time" value="1" onclick="columntime()">
                              <label class="form-check-label" for="time">Time</label>
                          </div>
                           <div class="form-check ">
                              <input type="checkbox" class="form-check-input company" id="location" value="1" onclick="columnlocation()">
                              <label class="form-check-label" for="location">Location</label>
                          </div>
                          <div class="form-check ">
                              <input type="checkbox" class="form-check-input company" id="columnistotalclean" value="1" onclick="columnistotalclean()">
                              <label class="form-check-label" for="columnistotalclean">Is totally clean</label>
                          </div>
                           <div class="form-check ">
                              <input type="checkbox" class="form-check-input company" id="columnishandclean" value="1" onclick="columnishandclean()">
                              <label class="form-check-label" for="columnishandclean">Are hands clean</label>
                          </div>
                    </div> 
                 
                    <table  class=" company_table_static table table-striped table-bordered" id="tablecompletedlists" cellspacing="0" width="100%">
                        <thead>
                        <tr id = "tr-head">
                                              
                            <th> Start
                            </th>
                            <th> End
                            </th>
                            <th>Form Name
                            </th>
                            <th> Name
                            </th>
                            <th>Title
                            </th>
                           
                            <th id="action">
                            </th>

                        </tr>
                        </thead>
                            <tr id="tr-body"> 
                                <td class="pt-3-half" contenteditable="true">02 Oct 2018</td>
                                <td class="pt-3-half" contenteditable="true">31 Oct 2019</td>
                                <td class="pt-3-half" contenteditable="true"> Welfare Checklist</td>
                                <td class="pt-3-half" contenteditable="true">shard Place </td>
                                <td class="pt-3-half" contenteditable="true">Test 2 </td>
                                <td>
                                    <a  data-toggle="modal" data-target="#modalEditcompelete" class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i></a>
                                    <button type="button"class="btn btn-danger btn-sm my-0" onclick="deletecomplete_listdata()"><i class="fas fa-trash"></i></button>

                                </td>
                            </tr>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        @component('backend.complete_list.model.edit')
        @endcomponent

        

@endsection
<script type="text/javascript">
function complete_list()
{

    swal({
        title:'Are you sure?',
        text: "Delete Record.",
        type: "error",
        showCloseButton: true,
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, Delete!",
        cancelButtonText: "Cancel!",
        closeOnConfirm: false,
        customClass: "confirm_class",
        closeOnCancel: false,
    },
    function(isConfirm){
        swal.close();
 });
}
    function columndate() {
        var tr = document.getElementById('tablecompletedlists').tHead.children[0];
        var row = document.getElementById("tr-body");
        
        if (document.getElementById('date').checked) {            
            tr.insertCell(4).outerHTML = "<th id='th-date'>Date</th>"
            row.insertCell(4).outerHTML = "<td id='td-date' class='pt-3-half' contenteditable='true'>12-09-10</td>"

        } else if (!document.getElementById('date').checked) {            
           document.getElementById("th-date").remove();
           document.getElementById("td-date").remove();
        }
    }
    function columntime() {
        var tr = document.getElementById('tablecompletedlists').tHead.children[0];
        var row = document.getElementById("tr-body");
        
        if (document.getElementById('time').checked) {            
            tr.insertCell(4).outerHTML = "<th id='th-time'>time</th>"
            row.insertCell(4).outerHTML = "<td id='td-time' class='pt-3-half' contenteditable='true'>09:01 AM</td>"

        } else if (!document.getElementById('time').checked) {            
           document.getElementById("th-time").remove();
           document.getElementById("td-time").remove();
        }
    }
    function columnlocation() {
        var tr = document.getElementById('tablecompletedlists').tHead.children[0];
        var row = document.getElementById("tr-body");
        
        if (document.getElementById('location').checked) {            
            tr.insertCell(4).outerHTML = "<th id='th-location'>location</th>"
            row.insertCell(4).outerHTML = "<td id='td-location' class='pt-3-half' contenteditable='true'>China</td>"

        } else if (!document.getElementById('location').checked) {            
           document.getElementById("th-location").remove();
           document.getElementById("td-location").remove();
        }
    }
    function columnishandclean() {
        var tr = document.getElementById('tablecompletedlists').tHead.children[0];
        var row = document.getElementById("tr-body");
        
        if (document.getElementById('columnishandclean').checked) {            
            tr.insertCell(4).outerHTML = "<th id='th-columnishandclean'>Is Hands Clean</th>"
            row.insertCell(4).outerHTML = "<td id='td-columnishandclean' class='pt-3-half' contenteditable='true'>True</td>"

        } else if (!document.getElementById('columnishandclean').checked) {            
           document.getElementById("th-columnishandclean").remove();
           document.getElementById("td-columnishandclean").remove();
        }
    }

    function columnistotalclean() {
        var tr = document.getElementById('tablecompletedlists').tHead.children[0];
        var row = document.getElementById("tr-body");
        
        if (document.getElementById('columnistotalclean').checked) {            
            tr.insertCell(4).outerHTML = "<th id='th-columnistotalclean'>Is Totally Clean</th>"
            row.insertCell(4).outerHTML = "<td id='td-columnistotalclean' class='pt-3-half' contenteditable='true'>True</td>"

        } else if (!document.getElementById('columnistotalclean').checked) {            
           document.getElementById("th-columnistotalclean").remove();
           document.getElementById("td-columnistotalclean").remove();
        }
    }




</script>
