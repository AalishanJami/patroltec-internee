@extends('backend.layouts.doc')
@section('title', 'View Register')
@section('content')
    @php
        $data=localization();
    @endphp
    <link rel="stylesheet" href="{{ asset('custom/css/custom_style.css') }}">
    @include('backend.layouts.doc_sidebar')
    <div class="padding-left-custom remove-padding">
        {!! breadcrumInner('view_register',Session::get('form_id'),'forms','/document/detail/'.Session::get('form_id'),'documents','/form_group') !!}
        @if(Auth::user()->all_companies == 1 )
            <div class="active_plus_btn">
                <ul class="float-right  mr-4">
                    <li class="hover_btn">
                        <a class="btn btn-cyan px-3 save_btn_view_register"><i class="fas fa-save" aria-hidden="true"></i></a>
                    </li>
                </ul>
            </div>
            <div class="accordion md-accordion accordion-1" id="accordionEx23" role="tablist">
                <div class="card">
                    <div class="card-header blue lighten-3 z-depth-1" role="tab" id="heading96">
                        <h5 class="text-uppercase mb-0 py-1">
                            <a class="white-text font-weight-bold" data-toggle="collapse" href="#collapse96" aria-expanded="true"
                               aria-controls="collapse96">
                               <span class="assign_class label{{getKeyid('questions',$data)}}" data-id="{{getKeyid('questions',$data) }}" data-value="{{checkKey('questions',$data) }}" >
                                    {!! checkKey('questions',$data) !!}
                                </span>
                            </a>
                        </h5>
                    </div>
                    <div id="collapse96" class="collapse hide" role="tabpanel" aria-labelledby="heading96"
                         data-parent="#accordionEx23">
                        <div class="card-body">
                            <div class="row my-4">
                                <div class="row mb-3 mr-2 mt-2 ml-3" style="padding-bottom: 10px !important;">
                                    <div class="view_register-form-check ">
                                        <input type="checkbox" checked class="form-check-input toggle-vis company" onclick="show('cb','0')" id="completed_by_1" >
                                        <label class="form-check-label word_limit" for="completed_by_1">
                                            <span class="assign_class label{{getKeyid('complete_by',$data)}}" data-id="{{getKeyid('complete_by',$data) }}" data-value="{{checkKey('complete_by',$data) }}" >
                                                {!! checkKey('complete_by',$data) !!}
                                            </span>
                                        </label>
                                    </div>
                                    <div class="view_register-form-check ">
                                        <input type="checkbox" checked class="form-check-input toggle-vis company" onclick="show('dc','1')" id="date_complete_1" >
                                        <label class="form-check-label word_limit" for="date_complete_1">
                                            <span class="assign_class label{{getKeyid('date_complete',$data)}}" data-id="{{getKeyid('date_complete',$data) }}" data-value="{{checkKey('date_complete',$data) }}" >
                                                {!! checkKey('date_complete',$data) !!}
                                            </span>
                                        </label>
                                    </div>
                                    <div class="view_register-form-check ">
                                        <input type="checkbox" checked class="form-check-input toggle-vis company" onclick="show('loc','2')" id="location_1" >
                                        <label class="form-check-label word_limit" for="location_1">
                                            <span class="assign_class label{{getKeyid('location',$data)}}" data-id="{{getKeyid('location',$data) }}" data-value="{{checkKey('location',$data) }}" >
                                                {!! checkKey('location',$data) !!}
                                            </span>
                                        </label>
                                    </div>
                                    @foreach($questions as $key=> $question)
                                        <div class="view_register-form-check ">
                                            <input type="checkbox" checked class="form-check-input toggle-vis company" onclick="show('{{$question->id}}','{{(int)$key+3}}')" data-column="{{$key}}" id="{{$question->id}}" >
                                            <label class="form-check-label" for="{{$question->id}}">
                                                {{ substr(str_replace("&nbsp;", "", strip_tags($question->question,'</span>')), 0,  30) }}
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div class="card">
            <div class="card-body">
                <div id="table" class="table-editable table-responsive">
                    <span class="table-add float-right mb-3 mr-2">
                    </span>
                    <table  id="view_register" class=" table table-striped table-bordered" id="tablecompletedlists" cellspacing="0" width="100%">
                        <thead>
                        <tr id = "tr-head">
                            <th id='cb'>
                                <span class="assign_class label{{getKeyid('complete_by',$data)}}" data-id="{{getKeyid('complete_by',$data) }}" data-value="{{checkKey('complete_by',$data) }}" >
                                    {!! checkKey('complete_by',$data) !!}
                                </span>
                            </th>
                            <th id='dc'>
                                <span class="assign_class label{{getKeyid('date_complete',$data)}}" data-id="{{getKeyid('date_complete',$data) }}" data-value="{{checkKey('date_complete',$data) }}" >
                                    {!! checkKey('date_complete',$data) !!}
                                </span>
                            </th>
                            <th id='loc'>
                                <span class="assign_class label{{getKeyid('location',$data)}}" data-id="{{getKeyid('location',$data) }}" data-value="{{checkKey('location',$data) }}" >
                                    {!! checkKey('location',$data) !!}
                                </span>
                            </th>
                            @foreach($questions as $question)
                                <th id='td{{$question->id}}'>
                                    {{ substr(str_replace("&nbsp;", "", strip_tags($question->question,'</span>')), 0,  30) }}
                                </th>
                            @endforeach
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @include('backend.label.input_label')
    @component('backend.complete_list.model.edit')
    @endcomponent
    <script src="{{ asset('custom/js/view_register.js') }}"></script>
    <script type="text/javascript">
        var question_permision=[];
        function show(id,key)
        {
            var column = $('#view_register').DataTable().column(key);
            // Toggle the visibility
            console.log(id);
            column.visible( ! column.visible() );
            if(column.visible()==true)
            {
                question_permision.push(parseInt(id));
            }else
            {
                var filteredItems = question_permision.filter(function(item) {
                    return item !== parseInt(id);
                })
                question_permision=filteredItems;
            }
            console.log(question_permision);
        }
        function getData(url)
        {
            var dataObject=[];
            dataObject.push(
                {data:"completed_by", name:"completed_by"},
                {data:"date_completed", name:"date_completed"},
                {data:"location", name:"location"},
            );
            $(<?php echo json_encode($questions); ?>).each(function(index,value) {
                    console.log(value['question']);
                    dataObject.push(
                        { 'data' : value['question'], 'name' : value['question'] },
                    );
                    question_permision.push(value['id']);
                }

            );
            console.log(dataObject);
            var table = $('#view_register').dataTable({
                processing: true,
                bInfo:false,
                language: {
                    'lengthMenu': '<span class="assign_class label'+$('#show_label').attr('data-id')+'" data-id="'+$('#show_label').attr('data-id')+'" data-value="'+$('#show_label').val()+'" ></span>  _MENU_ <span class="assign_class label'+$('#entries_label').attr('data-id')+'" data-id="'+$('#entries_label').attr('data-id')+'" data-value="'+$('#entries_label').val()+'" ></span>',
                    'search':'<span class="assign_class label'+$('#search_id').attr('data-id')+'" data-id="'+$('#search_id').attr('data-id')+'" data-value="'+$('#search_id').val()+'" >'+$('#search_id').val()+'</span>',
                    'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                    'info': '<span class="assign_class label'+$('#showing_page_label').attr('data-id')+'" data-id="'+$('#showing_page_label').attr('data-id')+'" data-value="'+$('#showing_page_label').val()+'" >'+$('#showing_page_label').val()+'</span> _PAGE_ <span class="assign_class label'+$('#of_label').attr('data-id')+'" data-id="'+$('#of_label').attr('data-id')+'" data-value="'+$('#of_label').val()+'" >'+$('#of_label').val()+'</span> _PAGES_',
                    'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                    'paginate': {
                        'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                        'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                    },
                    'infoFiltered': " "
                },
                "ajax": {
                    "url": url,
                    "type": 'get',

                },
                "createdRow": function( row, data, dataIndex,columns )
                {
                    console.log(data);
                    console.log(row);
                },
                columns:dataObject,
                columnDefs: [
                    {
                        "defaultContent": "not set",
                    }
                ]
            });
        }
        $(document).ready(function() {
            var url = "/view_register/getData";
            getData(url);
            // (function ($) {
            //     var active ='<span class="assign_class label'+$('#breadcrumb_view_register').attr('data-id')+'" data-id="'+$('#breadcrumb_view_register').attr('data-id')+'" data-value="'+$('#breadcrumb_view_register').val()+'" >'+$('#breadcrumb_view_register').val()+'</span>';
            //     $('.breadcrumb-item.active').html(active);
            //     var parent ='<span class="assign_class label'+$('#dashboard_label').attr('data-id')+'" data-id="'+$('#dashboard_label').attr('data-id')+'" data-value="'+$('#dashboard_label').val()+'" >'+$('#dashboard_label').val()+'</span>';
            //     $('.breadcrumb-item a').html(parent);
            // }(jQuery));
        });
    </script>
@endsection
