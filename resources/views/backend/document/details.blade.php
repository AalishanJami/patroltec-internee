@extends('backend.layouts.doc')
@section('title', 'Document Detail')
<style>
    .select-wrapper input.select-dropdown{
        height: 41px !important;
    }
    main {
        height: unset !important;
    }
</style>
<link rel="stylesheet" href="{{ asset('custom/css/custom_style.css') }}">
@section('content')
    @php
        $data=localization();
    @endphp
    @include('backend.layouts.doc_sidebar')
    <div class="padding-left-custom remove-padding">
        <ol class="breadcrumb">
        <!-- <li class="breadcrumb-item">
      <a href="{{url('dashboard')}}">
        <span class="assign_class label{{getKeyid('dashboard',$data)}}" data-id="{{getKeyid('dashboard',$data) }}" data-value="{{checkKey('dashboard',$data) }}" >
            {!! checkKey('dashboard',$data) !!}
            </span>
          </a>
        </li> -->
            <li class="breadcrumb-item ">
                <a href="{{url('form_group')}}">
          <span class="assign_class label{{getKeyid('documents',$data)}}" data-id="{{getKeyid('documents',$data) }}" data-value="{{checkKey('documents',$data) }}" >
              {!! checkKey('documents',$data) !!}
          </span>
                </a>
            </li>
            <li class="breadcrumb-item active_custom ">
        <span class="assign_class label{{getKeyid('detail',$data)}}" data-id="{{getKeyid('detail',$data) }}" data-value="{{checkKey('detail',$data) }}" >
            {!! checkKey('detail',$data) !!}
        </span>
            </li>
            <li class="breadcrumb-item active_custom ">{{$forms->name}}</li>
        </ol>
        <div class="locations-form container-fluid form_body">
            <div class="card">
                <ul  class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active document_details" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">
                            <span class="assign_class label{{getKeyid('detail',$data)}}" data-id="{{getKeyid('detail',$data) }}" data-value="{{checkKey('detail',$data) }}" >
                              {!! checkKey('detail',$data) !!}
                            </span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact"aria-selected="false">
                            <span class="assign_class label{{getKeyid('form_type',$data)}}" data-id="{{getKeyid('form_type',$data) }}" data-value="{{checkKey('form_type',$data) }}" >
                                {!! checkKey('form_type',$data) !!}
                            </span>
                        </a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <h5 class="card-header info-color white-text py-4">
                            <strong>
              <span class="assign_class label{{getKeyid('general',$data)}}" data-id="{{getKeyid('general',$data) }}" data-value="{{checkKey('general',$data) }}" >
                {!! checkKey('general',$data) !!}
              </span>
                            </strong>
                        </h5>
                        <div class="card-body px-lg-5 pt-0">
                            <form  style="color: #757575;" action="{{url('document\updateDetail')}}" id="document_form_update" method="post">
                                @csrf
                                <input type="hidden" name="company_id" class="company_id">
                                <div class="form-row">
                                    <div class="col">
                                        @if(Auth()->user()->hasPermissionTo('documentlibrary_name_edit') || Auth::user()->all_companies == 1 )
                                            <div class="md-form">
                                                {!! Form::text('name',$forms->name, ['class' => 'form-control','id'=>'edit_name']) !!}
                                                <label class="active" for="name">
                                                    <span class="assign_class label{{getKeyid('name',$data)}}" data-id="{{getKeyid('name',$data) }}" data-value="{{checkKey('name',$data) }}" >
                                                      {!! checkKey('name',$data) !!}
                                                    </span>
                                                </label>
                                                <small class="text-danger" style="left: 0%!important; text-transform: initial !important;" id="title_required_message"></small>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="col">
                                        @if(Auth()->user()->hasPermissionTo('documentlibrary_ref_edit') || Auth::user()->all_companies == 1 )
                                            <div class="md-form">
                                                {!! Form::text('ref',$forms->ref, ['class' => 'form-control']) !!}
                                                <label class="active" for="materialRegisterFormLastName">
                                                    <span class="assign_class label{{getKeyid('ref',$data)}}" data-id="{{getKeyid('ref',$data) }}" data-value="{{checkKey('ref',$data) }}" >
                                                      {!! checkKey('ref',$data) !!}
                                                    </span>
                                                </label>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <br>
                                <div class="form-row">
                                    <div class="col">
                                        @if(Auth()->user()->hasPermissionTo('documentlibrary_commonQuestion_edit') || Auth::user()->all_companies == 1 )
                                            <div class="md-form">
                                                <label class="mdb-main-label active">
                                                    <span class="assign_class label{{getKeyid('common_questions',$data)}}" data-id="{{getKeyid('common_questions',$data) }}" data-value="{{checkKey('common_questions',$data) }}" >
                                                      {!! checkKey('common_questions',$data) !!}
                                                    </span>
                                                </label>
                                                @if($forms->form_section_id==null)
                                                    @php $sections['please']='Please Select' @endphp
                                                    {!! Form::select('form_section_id[]',$sections,'please', ['class' =>'mdb-select colorful-select dropdown-primary md-form','id'=>'form_section_id_null','multiple']) !!}
                                                @else
                                                    {!! Form::select('form_section_id[]',$sections,$forms->form_section_id, ['class' =>'mdb-select colorful-select dropdown-primary md-form','id'=>'form_section_id','multiple']) !!}
                                                @endif
                                            </div>
                                        @endif
                                    </div>
                                    <div class="col">
                                        @if(Auth()->user()->hasPermissionTo('documentlibrary_locationType_edit') || Auth::user()->all_companies == 1 )
                                            <div class="md-form">
                                                <label class="mdb-main-label active">
                                                    <span class="assign_class label{{getKeyid('location_type',$data)}}" data-id="{{getKeyid('location_type',$data) }}" data-value="{{checkKey('location_type',$data) }}" >
                                                      {!! checkKey('location_type',$data) !!}
                                                    </span>
                                                </label>
                                                {!! Form::select('location_type',['m'=>'Main','t'=>'Tag','e'=>'Equipment'], $forms->location_type, ['class' =>'mdb-select','placeholder'=>'Please Select','id'=>'location_type']) !!}
                                                <small class="text-danger" style="left: 0%!important;margin-top:-15px;display:block; text-transform: initial !important;" id="location_type_required_message"></small>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="col">
                                        <div class="md-form">
                                            <label class="mdb-main-label active">
                                                <span class="assign_class label{{getKeyid('folder',$data)}}" data-id="{{getKeyid('folder',$data) }}" data-value="{{checkKey('folder',$data) }}" >
                                                  {!! checkKey('folder',$data) !!}
                                                </span>
                                            </label>
                                            <select name="form_group_id" searchable="Search here" class="mdb-select colorful-select dropdown-primary" id="form_group_id">
                                                <option value="0" @if($forms->form_group_id==0) selected @endif>Roote Folder</option>
                                                @foreach($form_groups as $key=>$value)
                                                    <option value="{{$key}}" @if($key==$forms->form_group_id) selected @endif>{{$value}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                @if(Auth()->user()->hasPermissionTo('documentlibrary_formsetting_edit') || Auth::user()->all_companies == 1 )
                                    <div class="card" style="padding-bottom: 20px;">
                                        <h5 class="card-header info-color white-text py-1">
                                            <strong>
                                              <span class="assign_class label{{getKeyid('form_settings',$data)}}" data-id="{{getKeyid('form_settings',$data) }}" data-value="{{checkKey('form_settings',$data) }}" >
                                                {!! checkKey('form_settings',$data) !!}
                                              </span>
                                            </strong>
                                        </h5>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="md-form " style="padding-left:20px;">
                                                    {!! Form::select('form_type',$forms_types, $forms->form_type, ['class' =>'mdb-select','placeholder'=>'Form Type','id'=>'form_type']) !!}
                                                    <small class="text-danger" style="left: 0%!important;margin-top: -15px;display:block; text-transform: initial !important;" id="form_type_required_message"></small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-check">
                                                    {!! Form::checkbox('pre_populate','1',$forms->pre_populate,['class'=>'form-check-input','id'=>'pre_populate']) !!}
                                                    <label class="form-check-label" for="pre_populate">
                                                        <span class="assign_class label{{getKeyid('pre_populate',$data)}}" data-id="{{getKeyid('pre_populate',$data) }}" data-value="{{checkKey('pre_populate',$data) }}" >
                                                            {!! checkKey('pre_populate',$data) !!}
                                                        </span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="form-check ">
                                                    {!! Form::checkbox('import_jobs','1',$forms->import_jobs,['class'=>'form-check-input','id'=>'import_jobs']) !!}
                                                    <label class="form-check-label" for="import_jobs">
                                                        <span class="assign_class label{{getKeyid('import_jobs',$data)}}" data-id="{{getKeyid('import_jobs',$data) }}" data-value="{{checkKey('import_jobs',$data) }}" >
                                                            {!! checkKey('import_jobs',$data) !!}
                                                        </span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-top: 6px;">
                                            <div class="col">
                                                <div class="form-check ">
                                                    {!! Form::checkbox('req_delete','1',$forms->req_delete,['class'=>'form-check-input','id'=>'req_delete']) !!}
                                                    <label class="form-check-label" for="req_delete">
                                                        <span class="assign_class label{{getKeyid('reason_for_delete',$data)}}" data-id="{{getKeyid('reason_for_delete',$data) }}" data-value="{{checkKey('reason_for_delete',$data) }}" >
                                                            {!! checkKey('reason_for_delete',$data) !!}
                                                        </span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="form-check ">
                                                    {!! Form::checkbox('req_sign_off','1',$forms->req_sign_off,['class'=>'form-check-input','id'=>'req_sign_off']) !!}
                                                    <label class="form-check-label" for="req_sign_off">
                                                        <span class="assign_class label{{getKeyid('require_sign_off',$data)}}" data-id="{{getKeyid('require_sign_off',$data) }}" data-value="{{checkKey('require_sign_off',$data) }}" >
                                                        {!! checkKey('require_sign_off',$data) !!}
                                                        </span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row" style="margin-top: 6px;">
                                            <div class="col">
                                                <div class="form-check ">
                                                    {!! Form::checkbox('app_form','1',$forms->app_form,['class'=>'form-check-input','id'=>'app_form']) !!}
                                                    <label class="form-check-label" for="app_form">
                                                        <span class="assign_class label{{getKeyid('show_form_onapp',$data)}}" data-id="{{getKeyid('show_form_onapp',$data) }}" data-value="{{checkKey('show_form_onapp',$data) }}" >
                                                            {!! checkKey('show_form_onapp',$data) !!}
                                                        </span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="form-check ">
                                                    {!! Form::checkbox('req_tag','1',$forms->req_tag,['class'=>'form-check-input','id'=>'req_tag']) !!}
                                                    <label class="form-check-label" for="req_tag">
                                                        <span class="assign_class label{{getKeyid('is_tag_job',$data)}}" data-id="{{getKeyid('is_tag_job',$data) }}" data-value="{{checkKey('is_tag_job',$data) }}" >
                                                            {!! checkKey('is_tag_job',$data) !!}
                                                        </span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <br>
                                @endif
                                @if(Auth()->user()->hasPermissionTo('documentlibrary_download_edit') || Auth::user()->all_companies == 1 )
                                    <div class="card">
                                        <h5 class="card-header info-color white-text py-1">
                                            <strong>
                                                <span class="assign_class label{{getKeyid('download',$data)}}" data-id="{{getKeyid('download',$data) }}" data-value="{{checkKey('download',$data) }}" >
                                                    {!! checkKey('download',$data) !!}
                                                </span>
                                            </strong>
                                        </h5>
                                        <ul class="list-group download_document_list list-group-flush">
                                            <li class="list-group-item">
                                                <div class="form-check ">
                                                    @if(empty($forms->pdf_download) && empty($forms->doc_download))
                                                        {!! Form::radio('view','1',true,['class'=>'form-check-input','checked'=>'true','id'=>'download1']) !!}
                                                    @else
                                                        {!! Form::radio('view','1',null,['class'=>'form-check-input','id'=>'download1']) !!}
                                                    @endif
                                                    <label class="form-check-label" for="download1">
                                                       <span class="assign_class label{{getKeyid('view_only',$data)}}" data-id="{{getKeyid('view_only',$data) }}" data-value="{{checkKey('view_only',$data) }}" >
                                                        {!! checkKey('view_only',$data) !!}
                                                       </span>
                                                    </label>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                                <div class="form-check ">
                                                    @if($forms->doc_download=='1' && empty($forms->pdf_download))
                                                        {!! Form::radio('view','2',true,['class'=>'form-check-input','checked'=>'true','id'=>'vdo2']) !!}
                                                    @else
                                                        {!! Form::radio('view','2',null,['class'=>'form-check-input','id'=>'vdo2']) !!}
                                                    @endif
                                                    <label class="form-check-label" for="vdo2">
                                                       <span class="assign_class label{{getKeyid('view_and_download_orignal_file_format',$data)}}" data-id="{{getKeyid('view_and_download_orignal_file_format',$data) }}" data-value="{{checkKey('view_and_download_orignal_file_format',$data) }}" >
                                                            {!! checkKey('view_and_download_orignal_file_format',$data) !!}
                                                        </span>
                                                    </label>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                                <div class="form-check ">
                                                    @if($forms->pdf_download=='1' && empty($forms->doc_download))
                                                        {!! Form::radio('view','3',true,['class'=>'form-check-input','checked'=>'true','id'=>'viewdpdf3']) !!}
                                                    @else
                                                        {!! Form::radio('view','3',null,['class'=>'form-check-input','id'=>'viewdpdf3']) !!}
                                                    @endif
                                                    <label class="form-check-label" for="viewdpdf3">
                                                        <span class="assign_class label{{getKeyid('view_and_download_pdf',$data)}}" data-id="{{getKeyid('view_and_download_pdf',$data) }}" data-value="{{checkKey('view_and_download_pdf',$data) }}" >
                                                            {!! checkKey('view_and_download_pdf',$data) !!}
                                                        </span>
                                                    </label>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                                <div class="form-check ">
                                                    @if(!empty($forms->pdf_download) && !empty($forms->doc_download))
                                                        {!! Form::radio('view','4',true,['class'=>'form-check-input true_clase','id'=>'viewdorgpfd4']) !!}
                                                    @else
                                                        {!! Form::radio('view','4',null,['class'=>'form-check-input false_clase','id'=>'viewdorgpfd4']) !!}
                                                    @endif
                                                    <label class="form-check-label" for="viewdorgpfd4">
                                                        <span class="assign_class label{{getKeyid('view_and_download_pdf_and_orignal_file_format',$data)}}" data-id="{{getKeyid('view_and_download_pdf_and_orignal_file_format',$data) }}" data-value="{{checkKey('view_and_download_pdf_and_orignal_file_format',$data) }}" >
                                                          {!! checkKey('view_and_download_pdf_and_orignal_file_format',$data) !!}
                                                        </span>
                                                    </label>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                @endif
                                <br>
                                @if(Auth()->user()->hasPermissionTo('documentlibrary_notes_edit') || Auth::user()->all_companies == 1 )
                                    <div class="md-form mb-5" id="document_note">
                                        <div class="summernote_inner body_notes document_notes_value" ></div>
                                    </div>
                                @endif
                                <div class="form-row">
                                    <div class="col-md-3">
                                        @if(Auth()->user()->hasPermissionTo('documentlibrary_lockJobFrom_edit') || Auth::user()->all_companies == 1 )
                                            <div class="md-form">
                                                {!! Form::number('lock_from',$forms->lock_from, ['class' => 'form-control','min'=>'0','max'=>'60']) !!}
                                                <label for="date-picker-example" class="active">
                                                    <span class="assign_class label{{getKeyid('lock_job_from',$data)}}" data-id="{{getKeyid('lock_job_from',$data) }}" data-value="{{checkKey('lock_job_from',$data) }}" >
                                                      {!! checkKey('lock_job_from',$data) !!}
                                                    </span>
                                                </label>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="col-md-2">
                                        @if(Auth()->user()->hasPermissionTo('documentlibrary_lockJobFrom_edit') || Auth::user()->all_companies == 1 )
                                            <div class="md-form">
                                                {!! Form::select('lock_job_after',['1'=>'Min','2'=>'Hrs','3'=>'Days','4'=>'Week'], $forms->lock_job_after, ['class' =>'mdb-select','id'=>'lock_job_after']) !!}
                                            </div>
                                        @endif
                                    </div>
                                    <div class="col-md-2">&nbsp;</div>
                                    <div class="col-md-3">
                                        @if(Auth()->user()->hasPermissionTo('documentlibrary_lockJobBefore_edit') || Auth::user()->all_companies == 1 )
                                            <div class="md-form">
                                                {!! Form::number('lock_to',$forms->lock_to, ['class' => 'form-control','min'=>'0','max'=>'60']) !!}
                                                <label for="date-picker-example" class="active">
                                                    <span class="assign_class label{{getKeyid('lock_job_before',$data)}}" data-id="{{getKeyid('lock_job_before',$data) }}" data-value="{{checkKey('lock_job_before',$data) }}" >
                                                      {!! checkKey('lock_job_before',$data) !!}
                                                    </span>
                                                </label>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="col-md-2">
                                        @if(Auth()->user()->hasPermissionTo('documentlibrary_lockJobBefore_edit') || Auth::user()->all_companies == 1 )
                                            <div class="md-form">
                                                {!! Form::select('lock_job_before',['1'=>'Min','2'=>'Hrs','3'=>'Days','4'=>'Week'], $forms->lock_job_before, ['class' =>'mdb-select','id'=>'lock_job_before']) !!}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col">
                                        @if(Auth()->user()->hasPermissionTo('documentlibrary_validFrom_edit') || Auth::user()->all_companies == 1 )
                                            <div class="md-form">
                                                {!! Form::text('valid_from',Carbon\Carbon::parse($forms->valid_from)->format('jS M Y'), ['class' => 'form-control datepicker','id'=>'valid_from']) !!}
                                                <label for="valid_from" class="active">
                                                    <span class="assign_class label{{getKeyid('valid_from',$data)}}" data-id="{{getKeyid('valid_from',$data) }}" data-value="{{checkKey('valid_from',$data) }}" >
                                                      {!! checkKey('valid_from',$data) !!}
                                                    </span>
                                                </label>
                                                <small class="text-danger" style="left: 0%!important;margin-top:-8px;display:block; text-transform: initial !important;" id="valid_from_required_message"></small>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="col">
                                        @if(Auth()->user()->hasPermissionTo('documentlibrary_validTo_edit') || Auth::user()->all_companies == 1 )
                                            <div class="md-form">
                                                {!! Form::text('valid_to',Carbon\Carbon::parse($forms->valid_to)->format('jS M Y'), ['class' => 'form-control datepicker','id'=>'valid_to']) !!}
                                                <label for="date-picker-example" class="active">
                                                    <span class="assign_class label{{getKeyid('valid_to',$data)}}" data-id="{{getKeyid('valid_to',$data) }}" data-value="{{checkKey('valid_to',$data) }}" >
                                                      {!! checkKey('valid_to',$data) !!}
                                                    </span>
                                                </label>
                                                <small class="text-danger" style="left: 0%!important;margin-top:-8px;display:block; text-transform: initial !important;" id="valid_to_required_message"></small>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                @if(Auth()->user()->hasPermissionTo('documentlibrary_notes_edit') || Auth::user()->all_companies == 1 )
                                    <input type="hidden" name="notes" value="{{$forms->notes}}" id="document_notes_id">
                                @endif
                                <input type="hidden" name="id" value="{{$forms->id}}">
                                <button class="form_submit_check btn btn-outline-info btn-rounded btn-block z-depth-0 my-4 waves-effect" id="document_form_update_btn" type="submit">
                                    <span class="assign_class label{{getKeyid('update',$data)}}" data-id="{{getKeyid('update',$data) }}" data-value="{{checkKey('update',$data) }}" >
                                      {!! checkKey('update',$data) !!}
                                    </span>
                                </button>
                            </form>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                        <div id="table-client" class="table-editable table-responsive">
                            @if(Auth()->user()->hasPermissionTo('create_form_type') || Auth::user()->all_companies == 1 )
                                <span class=" table-add float-right mb-3 mr-2">
                                  <a class="text-success formTypeCreate_btn" data-toggle="modal" data-target="#formTypeCreate"><i class="fas fa-plus fa-2x" aria-hidden="true"></i></a>
                              </span>
                            @endif
                            @if(Auth()->user()->hasPermissionTo('delete_selected_form_type') || Auth::user()->all_companies == 1 )
                                <button class="btn btn-danger btn-sm" id="deleteselectedform_type">
                                  <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                                      {!! checkKey('selected_delete',$data) !!}
                                    </span>
                                </button>
                                <button class="btn btn-danger btn-sm" style="display: none;" id="deleteselectedform_typeSoft">
                                  <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                                      {!! checkKey('selected_delete',$data) !!}
                                  </span>
                                </button>
                            @endif
                            @if(Auth()->user()->hasPermissionTo('selected_active_form_type') || Auth::user()->all_companies == 1 )
                                <button class="btn btn-success btn-sm" style="display: none;" id="selectedactivebuttonform_type">
                                    <span class="assign_class label{{getKeyid('selected_active',$data)}}" data-id="{{getKeyid('selected_active',$data) }}" data-value="{{checkKey('selected_active',$data) }}" >
                                        {!! checkKey('selected_active',$data) !!}
                                    </span>
                                </button>
                            @endif
                            @if(Auth()->user()->hasPermissionTo('selected_restore_form_type') || Auth::user()->all_companies == 1 )
                                <button class="btn btn-primary btn-sm" id="restorebuttonform_type">
                                    <span class="assign_class label{{getKeyid('restore',$data)}}" data-id="{{getKeyid('restore',$data) }}" data-value="{{checkKey('restore',$data) }}" >
                                        {!! checkKey('restore',$data) !!}
                                    </span>
                                </button>
                            @endif
                            @if(Auth()->user()->hasPermissionTo('select_active_form_type') || Auth::user()->all_companies == 1 )
                                <button  style="display: none;" class="btn btn-primary btn-sm" id="activebuttonform_type">
                                    <span class="assign_class label{{getKeyid('show_active',$data)}}" data-id="{{getKeyid('show_active',$data) }}" data-value="{{checkKey('show_active',$data) }}" >
                                        {!! checkKey('show_active',$data) !!}
                                    </span>
                                </button>
                            @endif
                            @if(Auth()->user()->hasPermissionTo('csv_form_type') || Auth::user()->all_companies == 1 )
                                <form class="form-style" id="export_excel_form_type" method="POST" action="{{ url('form_type/export/excel') }}">
                                    {!! csrf_field() !!}
                                    <input type="hidden" class="form_type_export" name="excel_array" value="1">
                                    <button  type="submit" id="export_excel_employee_btn" class="form_submit_check btn btn-warning btn-sm">
                                        <span class="assign_class label{{getKeyid('excel_export',$data)}}" data-id="{{getKeyid('excel_export',$data) }}" data-value="{{checkKey('excel_export',$data) }}" >
                                            {!! checkKey('excel_export',$data) !!}
                                        </span>
                                    </button>
                                </form>
                            @endif
                            @if(Auth()->user()->hasPermissionTo('word_form_type') || Auth::user()->all_companies == 1 )
                                <form class="form-style" id="export_world_form_type" method="POST" action="{{ url('form_type/export/world') }}">
                                    {!! csrf_field() !!}
                                    <input type="hidden" class="form_type_export" name="word_array" value="1">
                                    <button  type="submit" id="export_word_note" class="form_submit_check btn btn-success btn-sm">
                                        <span class="assign_class label{{getKeyid('word_export',$data)}}" data-id="{{getKeyid('word_export',$data) }}" data-value="{{checkKey('word_export',$data) }}" >
                                            {!! checkKey('word_export',$data) !!}
                                        </span>
                                    </button>
                                </form>
                            @endif
                            @if(Auth()->user()->hasPermissionTo('pdf_form_type') || Auth::user()->all_companies == 1 )
                                <form  class="form-style" id="export_pdf_form_type" method="POST" action="{{ url('form_type/export/pdf') }}">
                                    {!! csrf_field() !!}
                                    <input type="hidden" class="form_type_export" name="pdf_array" value="1">
                                    <button  type="submit" id="export_pdf_note_btn"  class="form_submit_check btn btn-secondary btn-sm">
                                        <span class="assign_class label{{getKeyid('pdf_export',$data)}}" data-id="{{getKeyid('pdf_export',$data) }}" data-value="{{checkKey('pdf_export',$data) }}" >
                                            {!! checkKey('pdf_export',$data) !!}
                                        </span>
                                    </button>
                                </form>
                            @endif
                            <div id="table_form_type" class="table-responsive table-editable">
                                <table id="form_type_table_data" style="width: 100%;" class="table table-bordered table-responsive-md table-striped">
                                    <thead>
                                    <tr>
                                        <th class="no-sort text-left all_checkboxes_style">
                                            <div class="form-check">
                                                <input type="checkbox" class="form-check-input form_type_checked" id="form_type_checkbox_all">
                                                <label class="form-check-label" for="form_type_checkbox_all">
                                                    <span class="assign_class label{{getKeyid('all',$data)}}" data-id="{{getKeyid('all',$data) }}" data-value="{{checkKey('all',$data) }}" >
                                                        {!! checkKey('all',$data) !!}
                                                    </span>
                                                </label>
                                            </div>
                                        </th>
                                        <th class="text-left">
                                            <span class="assign_class label{{getKeyid('name',$data)}}" data-id="{{getKeyid('name',$data) }}" data-value="{{checkKey('name',$data) }}" >
                                                {!! checkKey('name',$data) !!}
                                            </span>
                                        </th>
                                        <th class="text-left">
                                            <span class="assign_class label{{getKeyid('show_dashboard',$data)}}" data-id="{{getKeyid('show_dashboard',$data) }}" data-value="{{checkKey('show_dashboard',$data) }}" >
                                              {!! checkKey('show_dashboard',$data) !!}
                                            </span>
                                        </th>
                                    <!--  <th>
                                  <span class="assign_class label{{getKeyid('count_dashboard',$data)}}" data-id="{{getKeyid('count_dashboard',$data) }}" data-value="{{checkKey('count_dashboard',$data) }}" >
                                      {!! checkKey('count_dashboard',$data) !!}
                                        </span>
                                    </th>
                                    <th>
                                        <span class="assign_class label{{getKeyid('count_day',$data)}}" data-id="{{getKeyid('count_day',$data) }}" data-value="{{checkKey('count_day',$data) }}" >
                                      {!! checkKey('count_day',$data) !!}
                                        </span>
                                    </th>
                                    <th>
                                        <span class="assign_class label{{getKeyid('count_response_positive',$data)}}" data-id="{{getKeyid('count_response_positive',$data) }}" data-value="{{checkKey('count_response_positive',$data) }}" >
                                      {!! checkKey('count_response_positive',$data) !!}
                                        </span>
                                    </th>
                                    <th>
                                        <span class="assign_class label{{getKeyid('count_response_negative',$data)}}" data-id="{{getKeyid('count_response_negative',$data) }}" data-value="{{checkKey('count_response_negative',$data) }}" >
                                      {!! checkKey('count_response_negative',$data) !!}
                                        </span>
                                    </th>
                                    <th>
                                        <span class="assign_class label{{getKeyid('outstanding_count',$data)}}" data-id="{{getKeyid('outstanding_count',$data) }}" data-value="{{checkKey('outstanding_count',$data) }}" >
                                      {!! checkKey('outstanding_count',$data) !!}
                                        </span>
                                    </th>
                                    <th>
                                        <span class="assign_class label{{getKeyid('late_completion_count',$data)}}" data-id="{{getKeyid('late_completion_count',$data) }}" data-value="{{checkKey('late_completion_count',$data) }}" >
                                      {!! checkKey('late_completion_count',$data) !!}
                                        </span>
                                    </th>
                                    <th>
                                        <span class="assign_class label{{getKeyid('detailed_field_stats',$data)}}" data-id="{{getKeyid('detailed_field_stats',$data) }}" data-value="{{checkKey('detailed_field_stats',$data) }}" >
                                      {!! checkKey('detailed_field_stats',$data) !!}
                                        </span>
                                    </th> -->
                                        <th class="text-left">
                                            <span class="assign_class label{{getKeyid('action_score',$data)}}" data-id="{{getKeyid('action_score',$data) }}" data-value="{{checkKey('action_score',$data) }}" >
                                                {!! checkKey('action_score',$data) !!}
                                            </span>
                                        </th>
                                        <th class="no-sort all_action_btn" id="action"></th>
                                        <!--  <th class="no-sort"></th> -->
                                    </tr>
                                    </thead>
                                    <tbody ></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backend.document.model.form_type_permision')
    @include('backend.document.model.form_type_create')
    @include('backend.label.input_label')
    <script src="{{ asset('custom/js/form_type.js') }}"></script>
    <script src="{{asset('editor/sprite.svg.js')}}"></script>
    <script type="text/javascript">$('.datepicker').pickadate({selectYears:250,});</script>
    <script type="text/javascript">
        $("#document_form_update_btn").click(function(e){
            if( $('.edit_cms_disable').css('display') == 'none' ) {
                return 0;
            }
            e.preventDefault();
            // document.details
            var company_id=$('#companies_selected_nav').children("option:selected").val();
            $('.company_id').val(company_id);
            var notes=$('#document_note').find( '.note-editable, .card-block p' ).html();
            $('#document_notes_id').val(notes);
            $("#document_form_update").submit();
        });
        function checkSelectedFormType(value,checkValue)
        {
            if(value == checkValue)
            {
                return 'checked';
            }
            else
            {
                return "";
            }
        }
        function form_type_data(url) {
            $.ajax({
                type: 'GET',
                url: url,
                success: function(response)
                {
                    $('#form_type_table_data').DataTable().clear().destroy();
                    var table = $('#form_type_table_data').dataTable(
                        {
                            processing: true,
                            language: {
                                'lengthMenu': '  _MENU_ ',
                                'search':'<span class="assign_class label'+$('#search_id').attr('data-id')+'" data-id="'+$('#search_id').attr('data-id')+'" data-value="'+$('#search_id').val()+'" >'+$('#search_id').val()+'</span>',
                                'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                                'info':'',
                                'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                                'paginate': {
                                    'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                                    'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                                },
                                'infoFiltered': "(filtered from _MAX_ total records)"
                            },
                            "ajax": {
                                "url": url,
                                "type": 'get',
                            },

                            "createdRow": function( row, data, dataIndex,columns )
                            {
                                var checkbox_permission=$('#checkbox_permission').val();
                                var checkbox='';
                                checkbox+='<div class="form-check"><input type="checkbox" class="form-check-input selectedrowform_type" id="form_type'+data.id+'"><label class="form-check-label" for="form_type'+data.id+'""></label></div>';
                                var check_show_dashboard='';
                                var check_count_dashboard='';
                                var check_count_day='';
                                var check_count_response_postive='';
                                var check_count_response_negative='';
                                var check_outanding_count='';
                                var check_late_completion_count='';
                                var check_detailed_field_stats='';
                                if(!$('#show_dashboard_permission_edit').val() || !$('#edit_permission_form_type').val())
                                {
                                    check_show_dashboard='disabled';
                                }
                                /*if(!$('#count_dashboard_permission_edit').val() || ! $('#edit_permission_form_type').val())
                                {
                                    check_count_dashboard='disabled';
                                }
                                if(!$('#count_day_permission_edit').val() || !$('#edit_permission_form_type').val())
                                {
                                    check_count_day='disabled';
                                }
                                if(!$('#count_response_positive_permission_edit').val() || !$('#edit_permission_form_type').val())
                                {
                                    check_count_response_postive='disabled';
                                }
                                if(!$('#count_response_negative_permission_edit').val() || !$('#edit_permission_form_type').val())
                                {
                                    check_count_response_negative='disabled';
                                }
                                if(!$('#outanding_count_permission_edit').val() || !$('#edit_permission_form_type').val())
                                {
                                    check_outanding_count='disabled';
                                }
                                if(!$('#late_completion_count_permission_edit').val() || !$('#edit_permission_form_type').val())
                                {
                                    check_late_completion_count='disabled';
                                }
                                if(!$('#detailed_field_stats_permission_edit').val() || !$('#edit_permission_form_type').val())
                                {
                                    check_detailed_field_stats='disabled';
                                }*/
                                var submit='';
                                var show_dashboard='';
                                show_dashboard+='<div class="form-check float-left">';
                                // show_dashboard+='No';
                                if(data._show_dashboard == 1)
                                {
                                    show_dashboard+='Yes';
                                }
                                else
                                {
                                    show_dashboard+='No';
                                }
                                // show_dashboard+=' <input disabled="disabled" type="radio" '+check_show_dashboard+' '+checkSelectedFormType(data._show_dashboard,1)+'  value="1" name="show_dashboard'+data.id+'" class="show_dashboard'+data.id+' form-check-input show_button" data-id="'+data.id+'" id="yes_show_dashboard'+data.id+'">';
                                // show_dashboard+='<label class="form-check-label" for="yes_show_dashboard'+data.id+'">Yes</label>';
                                show_dashboard+='</div>';
                                // show_dashboard+='<div class="form-check float-left">';
                                //   show_dashboard+=' <input disabled="disabled" type="radio" '+check_show_dashboard+' '+checkSelectedFormType(data._show_dashboard,0)+'  value="0" name="show_dashboard'+data.id+'"  class="show_dashboard'+data.id+' form-check-input show_button" data-id="'+data.id+'" id="no_show_dashboard'+data.id+'">';
                                //   show_dashboard+='<label class="form-check-label" for="no_show_dashboard'+data.id+'">No</label>';
                                // show_dashboard+='</div>';
                                /*var count_dashboard='';
                                count_dashboard+='<div class="form-check float-left">';
                                count_dashboard+=' <input type="radio" '+check_count_dashboard+' '+checkSelectedFormType(data._count_dashboard,1)+'  value="1" name="count_dashboard'+data.id+'" class="count_dashboard'+data.id+' form-check-input show_button" data-id="'+data.id+'" id="yes_count_dashboard'+data.id+'">';
                                count_dashboard+='<label class="form-check-label" for="yes_count_dashboard'+data.id+'">Yes</label>';
                                count_dashboard+='</div>';
                                count_dashboard+='<div class="form-check float-left">';
                                count_dashboard+=' <input type="radio" '+check_count_dashboard+'  '+checkSelectedFormType(data._count_dashboard,0)+'  value="0" name="count_dashboard'+data.id+'"  class="count_dashboard'+data.id+' form-check-input show_button" data-id="'+data.id+'" id="no_count_dashboard'+data.id+'">';
                                count_dashboard+='<label class="form-check-label" for="no_count_dashboard'+data.id+'">No</label>';
                                count_dashboard+='</div>';
                                var count_day='';
                                count_day+='<div class="form-check float-left">';
                                count_day+=' <input '+check_count_day+'  type="radio" '+checkSelectedFormType(data._count_day,1)+'  value="1" name="count_day'+data.id+'" class="count_day'+data.id+' form-check-input show_button" data-id="'+data.id+'" id="yes_count_day'+data.id+'">';
                                count_day+='<label class="form-check-label" for="yes_count_day'+data.id+'">Yes</label>';
                                count_day+='</div>';
                                count_day+='<div class="form-check float-left">';
                                count_day+=' <input '+check_count_day+' type="radio" '+checkSelectedFormType(data._count_day,0)+'  value="0" name="count_day'+data.id+'"  class="count_day'+data.id+' form-check-input show_button" data-id="'+data.id+'" id="no_count_day'+data.id+'">';
                                count_day+='<label class="form-check-label" for="no_count_day'+data.id+'">No</label>';
                                count_day+='</div>';*/
                                // submit+='<a type="button" class="btn btn-primary btn-xs my-0 waves-effect waves-light saveform_typedata " id="'+data.id+'"><i class="fas fa-check"></i></a>';
                                /*var count_response_positive='';
                                count_response_positive+='<div class="form-check float-left">';
                                count_response_positive+=' <input type="radio" '+check_count_response_postive+' '+checkSelectedFormType(data._count_response_positive,1)+'  value="1" name="count_response_positive'+data.id+'" class="count_response_positive'+data.id+' form-check-input show_button" data-id="'+data.id+'" id="yes_count_response_positive'+data.id+'">';
                                count_response_positive+='<label class="form-check-label" for="yes_count_response_positive'+data.id+'">Yes</label>';
                                count_response_positive+='</div>';
                                count_response_positive+='<div class="form-check float-left">';
                                count_response_positive+=' <input  '+check_count_response_postive+' type="radio" '+checkSelectedFormType(data._count_response_positive,0)+'  value="0" name="count_response_positive'+data.id+'"  class="count_response_positive'+data.id+' form-check-input show_button" data-id="'+data.id+'" id="no_count_response_positive'+data.id+'">';
                                count_response_positive+='<label class="form-check-label" for="no_count_response_positive'+data.id+'">No</label>';
                                count_response_positive+='</div>';
                                var count_response_negative='';
                                count_response_negative+='<div class="form-check float-left">';
                                count_response_negative+=' <input '+check_count_response_negative+' type="radio" '+checkSelectedFormType(data._count_response_negative,1)+'  value="1" name="count_response_negative'+data.id+'" class="count_response_negative'+data.id+' form-check-input show_button" data-id="'+data.id+'" id="yes_count_response_negative'+data.id+'">';
                                count_response_negative+='<label class="form-check-label" for="yes_count_response_negative'+data.id+'">Yes</label>';
                                count_response_negative+='</div>';
                                count_response_negative+='<div class="form-check float-left">';
                                count_response_negative+=' <input '+check_count_response_negative+' type="radio" '+checkSelectedFormType(data._count_response_negative,0)+'  value="0" name="count_response_negative'+data.id+'"  class="count_response_negative'+data.id+' form-check-input show_button" data-id="'+data.id+'" id="no_count_response_negative'+data.id+'">';
                                count_response_negative+='<label class="form-check-label" for="no_count_response_negative'+data.id+'">No</label>';
                                count_response_negative+='</div>';
                                var outanding_count='';
                                outanding_count+='<div class="form-check float-left">';
                                outanding_count+=' <input '+check_outanding_count+' type="radio" '+checkSelectedFormType(data._outanding_count,1)+'  value="1" name="outanding_count'+data.id+'" class="outanding_count'+data.id+' form-check-input show_button" data-id="'+data.id+'" id="yes_outanding_count'+data.id+'">';
                                outanding_count+='<label class="form-check-label" for="yes_outanding_count'+data.id+'">Yes</label>';
                                outanding_count+='</div>';
                                outanding_count+='<div class="form-check float-left">';
                                outanding_count+=' <input '+check_outanding_count+' type="radio" '+checkSelectedFormType(data._outanding_count,0)+'  value="0" name="outanding_count'+data.id+'"  class="outanding_count'+data.id+' form-check-input show_button" data-id="'+data.id+'" id="no_outanding_count'+data.id+'">';
                                outanding_count+='<label class="form-check-label" for="no_outanding_count'+data.id+'">No</label>';
                                outanding_count+='</div>';
                                var late_completion_count='';
                                late_completion_count+='<div class="form-check float-left">';
                                late_completion_count+=' <input '+check_late_completion_count+' type="radio" '+checkSelectedFormType(data._late_completion_count,1)+'  value="1" name="late_completion_count'+data.id+'" class="late_completion_count'+data.id+' form-check-input show_button" data-id="'+data.id+'" id="yes_late_completion_count'+data.id+'">';
                                late_completion_count+='<label class="form-check-label" for="yes_late_completion_count'+data.id+'">Yes</label>';
                                late_completion_count+='</div>';
                                late_completion_count+='<div class="form-check float-left">';
                                late_completion_count+=' <input '+check_late_completion_count+'  type="radio" '+checkSelectedFormType(data._late_completion_count,0)+'  value="0" name="late_completion_count'+data.id+'"  class="late_completion_count'+data.id+' form-check-input show_button" data-id="'+data.id+'" id="no_late_completion_count'+data.id+'">';
                                late_completion_count+='<label class="form-check-label" for="no_late_completion_count'+data.id+'">No</label>';
                                late_completion_count+='</div>';
                                var detailed_field_stats='';
                                detailed_field_stats+='<div class="form-check float-left">';
                                detailed_field_stats+=' <input '+check_detailed_field_stats+' type="radio" '+checkSelectedFormType(data._detailed_field_stats,1)+'  value="1" name="detailed_field_stats'+data.id+'" class="detailed_field_stats'+data.id+' form-check-input show_button" data-id="'+data.id+'" id="yes_detailed_field_stats'+data.id+'">';
                                detailed_field_stats+='<label class="form-check-label" for="yes_detailed_field_stats'+data.id+'">Yes</label>';
                                detailed_field_stats+='</div>';
                                detailed_field_stats+='<div class="form-check float-left">';
                                detailed_field_stats+=' <input '+check_detailed_field_stats+' type="radio" '+checkSelectedFormType(data._detailed_field_stats,0)+'  value="0" name="detailed_field_stats'+data.id+'"  class="detailed_field_stats'+data.id+' form-check-input show_button" data-id="'+data.id+'" id="no_detailed_field_stats'+data.id+'">';
                                detailed_field_stats+='<label class="form-check-label" for="no_detailed_field_stats'+data.id+'">No</label>';
                                detailed_field_stats+='</div>';*/
                                $(columns[0]).html(checkbox);
                                $(columns[1]).attr('id', 'name'+data['id']);
                                /*if($('#name_permission_edit').val() && $('#edit_permission_form_type').val())
                                {
                                    $(columns[2]).attr('Contenteditable', 'true');
                                }
                                $(columns[2]).attr('class','edit_inline_form_type');

                                $(columns[11]).attr('Contenteditable', 'true');
                                $(columns[11]).attr('class','edit_inline_form_type');
                                $(columns[11]).attr('id', 'action_score'+data['id']);*/
                                $(columns[2]).html(show_dashboard);
                                /*  $(columns[4]).html(count_dashboard);
                                  $(columns[5]).html(count_day);
                                  $(columns[6]).html(count_response_positive);
                                  $(columns[7]).html(count_response_negative);
                                  $(columns[8]).html(outanding_count);
                                  $(columns[9]).html(late_completion_count);
                                  $(columns[10]).html(detailed_field_stats);*/
                                if($('#edit_permission_form_type').val())
                                {
                                    $(columns[3]).prepend(submit);
                                }
                                $(row).attr('id', 'form_type_tr'+data['id']);
                                //$(row).attr('class', 'selectedrowform_type');
                                var temp=data['id'];
                                $(row).attr('data-id',data['id']);
                            },
                            columns:
                                [
                                    {data: 'checkbox', name: 'checkbox',visible:$('#checkbox_permission').val()},
                                    {data: 'name', name: 'name',visible:$('#name_permission').val()},
                                    {data: '_show_dashboard', name: '_show_dashboard',visible:$('#show_dashboard_permission').val()},
                                    /*{data: 'count_dashboard', name: 'count_dashboard',visible:$('#count_dashboard_permission').val()},
                                    {data: 'count_day', name: 'count_day',visible:$('#count_day_permission').val()},
                                    {data: 'count_response_positive', name: 'count_response_positive',visible:$('#count_response_positive_permission').val()},
                                    {data: 'count_response_negative', name: 'count_response_negative',visible:$('#count_response_negative_permission').val()},
                                    {data: 'outanding_count', name: 'outanding_count',visible:$('#outanding_count_permission').val()},
                                    {data: 'late_completion_count', name: 'late_completion_count',visible:$('#late_completion_count_permission').val()},
                                    {data: 'detailed_field_stats', name: 'detailed_field_stats',visible:$('#detailed_field_stats_permission').val()},*/
                                    {data: 'action_score', name: 'action_score'},
                                    {data: 'actions', name: 'actions'},
                                    /*   {data: 'submit', name: 'submit'},*/
                                ],
                        }
                    );
                },
                error: function (error) {
                    console.log(error);
                }
            });

        }
        $(document).ready(function () {
            $("#document_notes_id").val('{{$forms->notes}}');
            setTimeout(function(){
                $('.note-editable').html('{{$forms->notes}}');
            },2000);

            {{--document.getElementsByClassName('note-editable').innerHTML='{{$forms->notes}}';--}}
            $('#form_section_id_null').children("option:selected").attr('disabled','true');
            if ($('#location_type').children("option:selected").val()==""){
                $('#location_type').children("option:selected").attr('disabled','true');
            }
            if ($('#form_type').children("option:selected").val()==""){
                $('#form_type').children("option:selected").attr('disabled','true');
            }

            $(".multiple-select-dropdown>.active").addClass('selected active');
            var form_type_url='/form_type/getall';
            form_type_data(form_type_url);
            $('.document_details').click(function(){
                $.ajax({
                    type: "GET",
                    url: '/form_type/refresh',
                    success: function(response)
                    {
                        var html='';
                        for (var i = response.data.length - 1; i >= 0; i--) {
                            html+='<option value="'+response.data[i].id+'">'+response.data[i].name+'</option>';
                        }
                        $('#form_type').empty();
                        $('#form_type').html(html);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            });
            $("#document_form_update").submit(function () {
                var validation = '';
                var location_type=$("#location_type").children('option:selected').val();
                var form_type=$("#form_type").children('option:selected').val();
                var edit_name= $("#edit_name").val();
                var valid_from= $("#valid_from").val();
                var valid_to= $("#valid_to").val();
                if (edit_name =="" || location_type =="" || form_type =="" || valid_from =="" || valid_to ==""){
                    if (edit_name ==""){
                        $("#edit_name").attr('style','border-bottom:1px solid #e3342f');
                        $("#title_required_message").html('Field required');
                    }
                    if (location_type==""){
                        $("[data-activates=select-options-location_type]").attr('style','border-bottom:1px solid #e3342f');
                        $("#location_type_required_message").html('Field required');
                    }
                    if (form_type ==""){
                        $("[data-activates=select-options-form_type]").attr('style','border-bottom:1px solid #e3342f');
                        $("#form_type_required_message").html('Field required');
                    }
                    if (valid_from ==""){
                        $("#valid_from").attr('style','border-bottom:1px solid #e3342f');
                        $("#valid_from_required_message").html('Field required');
                    }
                    if (valid_to ==""){
                        $("#valid_to").attr('style','border-bottom:1px solid #e3342f');
                        $("#valid_to_required_message").html('Field required');
                    }
                    validation = false;
                }else{
                    validation = true;
                }
                return validation;
            });
            // $.ajax({
            //     type: "GET",
            //     url: form_type_url,
            //     success: function(response)
            //     {
            //         var html='';
            //         for (var i = response.data.length - 1; i >= 0; i--) {
            //             html+='<option value="'+response.data[i].id+'">'+response.data[i].name+'</option>';
            //         }
            //         $('#sexual_orientation_id').empty();
            //         $('#sexual_orientation_id').html(html);
            //     },
            //     error: function (error) {
            //         console.log(error);
            //     }
            // });
        });
    </script>
@endsection
