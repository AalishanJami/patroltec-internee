<style type="text/css">
    .background-color
    {
        background-color:lightgreen !important;
    }
    .td-active
    {
        color: white !important;
    }

    #upload_static_table>tbody>tr>td:nth-child(1){
        width: 7%;
    }
    #upload_static_table>tbody>tr>td:nth-child(3){
        width: 22%;
        text-align: center;
    }
    #upload_static_table>tbody>tr>td:nth-child(3)>a{
        margin: 2%;
    }
</style>

<div id="table" class="table-editable table-responsive">
    @if(Auth()->user()->hasPermissionTo('create_documentStaticForm') || Auth::user()->all_companies == 1 )
        <span class="table-add float-right mb-3 mr-2">
        <a class="text-success" id="create_static" data-toggle="modal" data-target="#modalstatic_form">
            <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
        </a>
    </span>
    @endif
    @if(Auth()->user()->hasPermissionTo('selected_delete_documentStaticForm') || Auth::user()->all_companies == 1 )
        <button class="btn btn-danger btn-sm" id="deleteSelectedupload">
            <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                {!! checkKey('selected_delete',$data) !!}
            </span>
        </button>
    @endif
    @if(Auth()->user()->hasPermissionTo('selected_restore_delete_documentStaticForm') || Auth::user()->all_companies == 1 )
        <input type="hidden" id="flagStaticForm" value="1">
        <button class="btn btn-primary btn-sm" id="restore_staticform_upload">
            <span class="assign_class label{{getKeyid('restore',$data)}}" data-id="{{getKeyid('restore',$data) }}" data-value="{{checkKey('restore',$data) }}" >
                {!! checkKey('restore',$data) !!}
            </span>
        </button>
    @endif
    @if(Auth()->user()->hasPermissionTo('selected_active_documentStaticForm') || Auth::user()->all_companies == 1 )
        <button class="btn btn-success btn-sm" id="selectedactivebuttonupload" style="display:none;">
            <span class="assign_class label{{getKeyid('selected_active',$data)}}" data-id="{{getKeyid('selected_active',$data) }}" data-value="{{checkKey('selected_active',$data) }}" >
              {!! checkKey('selected_active',$data) !!}
            </span>
        </button>
    @endif
    @if(Auth()->user()->hasPermissionTo('show_active_documentStaticForm') || Auth::user()->all_companies == 1 )
        <button id="show_active_staticform_upload"  class="btn btn-primary btn-sm" style="display:none;">
           <span class="assign_class label{{getKeyid('show_active',$data)}}" data-id="{{getKeyid('show_active',$data) }}" data-value="{{checkKey('show_active',$data) }}" >
              {!! checkKey('show_active',$data) !!}
            </span>
        </button>
    @endif
    @if(Auth()->user()->hasPermissionTo('csv_documentStaticForm') || Auth::user()->all_companies == 1 )
        <form class="form-style" id="export_excel_upload" method="POST" action="{{ url('static_form/export/excel') }}">
            {!! csrf_field() !!}
            <input type="hidden" id="export_excel_upload" class="export_staticform" name="excel_array" value="1">
            <button  type="submit"  class="form_submit_check btn btn-warning btn-sm">
                <span class="assign_class label{{getKeyid('excel_export',$data)}}" data-id="{{getKeyid('excel_export',$data) }}" data-value="{{checkKey('excel_export',$data) }}" >
                  {!! checkKey('excel_export',$data) !!}
                </span>
            </button>
        </form>
    @endif
    @if(Auth()->user()->hasPermissionTo('word_documentStaticForm') || Auth::user()->all_companies == 1 )
        <form class="form-style " id="export_word_upload" method="POST"  action="{{ url('static_form/export/word') }}">
            <input type="hidden"  name="word_array" class="export_staticform" value="1">
            {!! csrf_field() !!}
            <button  type="submit" id="export_world_answer_group_btn" class="form_submit_check btn btn-success btn-sm">
                <span class="assign_class label{{getKeyid('word_export',$data)}}" data-id="{{getKeyid('word_export',$data) }}" data-value="{{checkKey('word_export',$data) }}" >
                  {!! checkKey('word_export',$data) !!}
                </span>
            </button>
        </form>
    @endif
    @if(Auth()->user()->hasPermissionTo('pdf_documentStaticForm') || Auth::user()->all_companies == 1 )
        <form  class="form-style" id="export_pdf_upload" {{pdf_view('static_form_uploads')}} method="POST"  action="{{ url('static_form/export/pdf') }}">
            <input type="hidden"  name="pdf_array" class="export_staticform" value="1">
            {!! csrf_field() !!}
            <button  type="submit" id="export_pdf_answer_group_btn"  class="form_submit_check btn btn-secondary btn-sm">
                <span class="assign_class label{{getKeyid('pdf_export',$data)}}" data-id="{{getKeyid('pdf_export',$data) }}" data-value="{{checkKey('pdf_export',$data) }}" >
                  {!! checkKey('pdf_export',$data) !!}
                </span>
            </button>
        </form>
    @endif
    @include('backend.label.input_label')
    <table id="upload_static_table"  class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th id="action" class="no-sort">
                <div class="form-check">
                    <input type="checkbox" class="form-check-input staticform_checkbox" id="staticform_checkbox_all">
                    <label class="form-check-label" for="staticform_checkbox_all">
                    </label>
                </div>
            </th>
            <th>
                <span class="assign_class label{{getKeyid('title',$data)}}" data-id="{{getKeyid('title',$data) }}" data-value="{{checkKey('title',$data) }}" >
                  {!! checkKey('title',$data) !!}
                </span>
            </th>
            <th class="no-sort"></th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
        @if(Auth()->user()->all_companies == 1)
            <input type="hidden" id="documentStaticForm_checkbox" value="1">
            <input type="hidden" id="documentStaticForm_title" value="1">
        @else
            <input type="hidden" id="documentStaticForm_checkbox" value="{{Auth()->user()->hasPermissionTo('documentStaticForm_checkbox')}}">
            <input type="hidden" id="documentStaticForm_title" value="{{Auth()->user()->hasPermissionTo('documentStaticForm_title')}}">
        @endif
</div>
@include('backend.static_forms.notes.model.create')
{{--    Edit Modal--}}
@include('backend.static_forms.notes.model.edit')
{{--    END Edit Modal--}}

<script>
    $('.file_upload').file_upload();
</script>
<script type="text/javascript" src="{{ asset('custom/js/static_form_upload.js') }}" ></script>

<script type="text/javascript">

    $(document).ready(function(){
        var url='/static_form/getAllUpload';
        uploadStaticAppend(url);
    });

    // create_static
    $('body').on('click', '#create_static', function() {
        $('.file-upload>.btn,.btn-sm .btn-danger')[0].click();
        $('.title_static').val('');
        $('.body_notes').summernote('code',' ');
    });
    function uploadStaticAppend(url)
    {
        var table = $('#upload_static_table').dataTable({
            processing: true,
            language: {
                'lengthMenu': ' _MENU_ ',
                'search':'<span class="assign_class label'+$('#search_label').attr('data-id')+'" data-id="'+$('#search_label').attr('data-id')+'" data-value="'+$('#search_label').val()+'" >'+$('#search_label').val()+'</span>',
                'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                'info': '',
                'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                'paginate': {
                    'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                    'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                },
                'infoFiltered': "(filtered from _MAX_ total records)"
            },
            "ajax": {
                "url": url,
                "type": 'get'

            },

            "createdRow": function( row, data, dataIndex,columns) {
                if(data.is_complete == 1)
                {
                    $(row).attr('class','background-color');
                }
                $(row).attr('id', 'staticform_tr'+data['id']);
                var checkbox='<div class="form-check"><input type="checkbox" class="form-check-input selectedRowStaticForm" id="staticform_check'+data.id+'"><label class="form-check-label" for="staticform_check'+data.id+'"></label></div>';
                $(columns[0]).html(checkbox);
                var temp=data['id'];
                $(row).attr('data-id', data['id']);
                // $(row).attr('onclick',"selectedRowUpload(this.id)");
            },
            columns: [
                {data: 'checkbox', name: 'checkbox',visible:$('#documentStaticForm_checkbox').val()},
                {data: 'name', name: 'name',visible:$('#documentStaticForm_title').val()},
                {data: 'actions', name: 'actions'},
            ],
            columnDefs: [
                { "orderable": false, "targets": [0,1] }
            ]

        });
        if($("#staticform_checkbox_all").prop('checked',true)){
            $("#staticform_checkbox_all").prop('checked',false);
        }
    }

</script>
