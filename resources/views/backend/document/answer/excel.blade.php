<table class="table table-bordered">
    <thead>
      <tr>
        @if(Auth()->user()->hasPermissionTo('document_answer_name_excel_export') || Auth::user()->all_companies == 1 )
            <th> Name</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('document_answer_grade_excel_export') || Auth::user()->all_companies == 1 )
            <th> Grade</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('document_answer_score_excel_export') || Auth::user()->all_companies == 1 )
            <th> Score</th>
        @endif
      </tr>
    </thead>
    <tbody>
    @foreach($data as $row)
      <tr>
        @if(Auth()->user()->hasPermissionTo('document_answer_name_excel_export') || Auth::user()->all_companies == 1 )
          <td>{{ $row->answer }}</td>
        @endif
        @if(Auth()->user()->hasPermissionTo('document_answer_grade_excel_export') || Auth::user()->all_companies == 1 )
            <td>{{ $row->grade }}</td>
        @endif
        @if(Auth()->user()->hasPermissionTo('document_answer_score_excel_export') || Auth::user()->all_companies == 1 )
            <td>{{ $row->score }}</td>
        @endif     
      </tr>
    @endforeach
    </tbody>
</table>