@include('backend.layouts.pdf_start')
    <thead>
      <tr>
        @if(Auth()->user()->hasPermissionTo('document_answer_image_pdf_export') || Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout"> Image</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('document_answer_name_pdf_export') || Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout"> Name</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('document_answer_grade_pdf_export') || Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout"> Grade</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('document_answer_score_pdf_export') || Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout"> Score</th>
        @endif
      </tr>
    </thead>
    <tbody>
    @foreach($data as $row)
      <tr>
        @if(Auth()->user()->hasPermissionTo('document_answer_image_pdf_export') || Auth::user()->all_companies == 1 )
            <td class="pdf_table_layout"><p class="col_text_style">
                @if($row->icon)
                    <img src="{{public_path('/uploads/icons/'.$row->icon)}}"  width="50" height="50" alt="No Icon">
                @endif
            </p></td>
        @endif
        @if(Auth()->user()->hasPermissionTo('document_answer_name_pdf_export') || Auth::user()->all_companies == 1 )
          <td class="pdf_table_layout"><p class="col_text_style">{{ $row->answer }}</p></td>
        @endif
        @if(Auth()->user()->hasPermissionTo('document_answer_grade_pdf_export') || Auth::user()->all_companies == 1 )
            <td class="pdf_table_layout"><p class="col_text_style">{{ $row->grade }}</p></td>
        @endif
        @if(Auth()->user()->hasPermissionTo('document_answer_score_pdf_export') || Auth::user()->all_companies == 1 )
            <td class="pdf_table_layout"><p class="col_text_style">{{ $row->score }}</p></td>
        @endif     
      </tr>
    @endforeach
    </tbody>
@include('backend.layouts.pdf_end')
