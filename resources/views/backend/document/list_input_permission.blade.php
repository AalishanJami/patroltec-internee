@if(Auth()->user()->all_companies == 1)
    <input type="hidden" id="documentfolder_checkbox" value="1">
    <input type="hidden" id="documentfolder_name_edit" value="1">
    <input type="hidden" id="documentfolder_name_create" value="1">
    <input type="hidden" id="documentfolder_name" value="1">
    <input type="hidden" id="create_documentfolder" value="1">
    <input type="hidden" id="edit_documentfolder" value="1">
    <input type="hidden" id="delete_documentfolder" value="1">
    <input type="hidden" id="view_documentfolder" value="1">
@else
    <input type="hidden" id="documentfolder_checkbox" value="{{Auth()->user()->hasPermissionTo('documentfolder_checkbox')}}">
    <input type="hidden" id="documentfolder_name_edit" value="{{Auth()->user()->hasPermissionTo('documentfolder_name_edit')}}">
    <input type="hidden" id="documentfolder_name_create" value="{{Auth()->user()->hasPermissionTo('documentfolder_name_create')}}">
    <input type="hidden" id="documentfolder_name" value="{{Auth()->user()->hasPermissionTo('documentfolder_name')}}">
    <input type="hidden" id="create_documentfolder" value="{{Auth()->user()->hasPermissionTo('create_documentfolder')}}">
    <input type="hidden" id="edit_documentfolder" value="{{Auth()->user()->hasPermissionTo('edit_documentfolder')}}">
    <input type="hidden" id="delete_documentfolder" value="{{Auth()->user()->hasPermissionTo('delete_documentfolder')}}">
    <input type="hidden" id="view_documentfolder" value="{{Auth()->user()->hasPermissionTo('view_documentfolder')}}">
@endif


@if(Auth()->user()->all_companies == 1)
    <input type="hidden" id="documentlibrary_checkbox" value="1">
    <input type="hidden" id="documentlibrary_name" value="1">
    <input type="hidden" id="documentlibrary_ref" value="1">
    <input type="hidden" id="documentlibrary_validTo" value="1">
@else
    <input type="hidden" id="documentlibrary_checkbox" value="{{Auth()->user()->hasPermissionTo('documentlibrary_checkbox')}}">
    <input type="hidden" id="documentlibrary_name" value="{{Auth()->user()->hasPermissionTo('documentlibrary_name')}}">
    <input type="hidden" id="documentlibrary_ref" value="{{Auth()->user()->hasPermissionTo('documentlibrary_ref')}}">
    <input type="hidden" id="documentlibrary_validTo" value="{{Auth()->user()->hasPermissionTo('documentlibrary_validTo')}}">
@endif
