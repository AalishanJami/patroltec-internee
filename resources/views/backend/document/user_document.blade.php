@extends('backend.layouts.user')
@section('title', 'Files')
@section('headscript')
    <script>
        $(document).ready(function() {
            $('#folder_permission_checkbox_all').click(function() {
                if ($(this).is(':checked')) {
                    $('.folder_permission_checked').attr('checked', true);
                    $("tr").addClass('selected');
                } else {
                    $('.folder_permission_checked').attr('checked', false);
                    $("tr").removeClass('selected');
                }
            });

            $('#file_permission_checkbox_all').click(function() {
                if ($(this).is(':checked')) {
                    $('.file_permission_checked').attr('checked', true);
                    $("tr").addClass('selected');
                } else {
                    $('.file_permission_checked').attr('checked', false);
                    $("tr").removeClass('selected');
                }
            });
        });
    </script>
    <link rel="stylesheet" href="{{ asset('custom/css/custom_style.css') }}">
@stop
@section('content')
    @php
       $data=localization();
    @endphp
    @include('backend.layouts.user_sidebar')
    @include('backend.label.input_label')
    <div class="padding-left-custom remove-padding">
        {!! breadcrumInner('files',Session::get('employee_id'),'users','/employee/detail/'.Session::get('employee_id'),'employees','/employee') !!}
        <div class="card">
            <div class="card-body">
                <ul  class="nav nav-tabs " id="myTab" role="tablist">
                    @if(Auth()->user()->hasPermissionTo('view_employeeFolderPermision') || Auth::user()->all_companies == 1 )
                        <li class="nav-item">
                            <a class="nav-link active" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile"aria-selected="false">
                                <span class="assign_class label{{getKeyid('folder',$data)}}" data-id="{{getKeyid('folder',$data) }}" data-value="{{checkKey('folder',$data) }}" >
                                    {!! checkKey('folder',$data) !!}
                                </span>
                            </a>
                        </li>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('view_employeeFilePermision') || Auth::user()->all_companies == 1 )
                    <li class="nav-item">
                        <a class="nav-link" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home"
                           aria-selected="true">
                            <span class="assign_class label{{getKeyid('file',$data)}}" data-id="{{getKeyid('file',$data) }}" data-value="{{checkKey('file',$data) }}" >
                                {!! checkKey('file',$data) !!}
                            </span>
                        </a>
                    </li>
                    @endif
                </ul>
                <div class="tab-content" id="myTabContentJust">
                    <div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <div class="table-responsive">
                            <form  action="{{url('/employee/documents/folder/update')}}" method="post">
                                @csrf
                                <table  class="company_table_static table  table-striped table-bordered " cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            @if(Auth()->user()->hasPermissionTo('employeeFolderPermision_checkbox') || Auth::user()->all_companies == 1 )
                                                <th class="no-sort all_checkboxes_style">
                                                    <div class="form-check">
                                                        <input type="checkbox" class="form-check-input" id="folder_permission_checkbox_all">
                                                        <label class="form-check-label" for="folder_permission_checkbox_all">
                                                            <span class="assign_class label{{getKeyid('all',$data)}}" data-id="{{getKeyid('all',$data) }}" data-value="{{checkKey('all',$data) }}" >
                                                                {!! checkKey('all',$data) !!}
                                                            </span>
                                                        </label>
                                                    </div>
                                                </th>
                                            @endif
                                            @if(Auth()->user()->hasPermissionTo('employeeFolderPermision_name') || Auth::user()->all_companies == 1 )
                                                <th>
                                                    <span class="assign_class label{{getKeyid('name',$data)}}" data-id="{{getKeyid('name',$data) }}" data-value="{{checkKey('name',$data) }}" >
                                                        {!! checkKey('name',$data) !!}
                                                    </span>
                                                </th>
                                            @endif

                                                <th>
                                                    <span class="assign_class label{{getKeyid('file_count',$data)}}" data-id="{{getKeyid('file_count',$data) }}" data-value="{{checkKey('file_count',$data) }}" >
                                                        {!! checkKey('file_count',$data) !!}
                                                    </span>
                                                </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($folders as $key=>$value)
                                            <tr>
                                                @if(Auth()->user()->hasPermissionTo('employeeFolderPermision_checkbox') || Auth::user()->all_companies == 1 )
                                                    @if($value->enable == 1)
                                                        <input type="hidden" name="folder_id[]" value="{{$value->id}}">
                                                    @endif
                                                    <td>
                                                        <div class="form-check">
                                                            <input type="checkbox"{{($value->enable == 1) ? 'checked' : '' }} class="form-check-input folder_permission_checked" id="folder{{$value->id}}" value="{{$value->id}}" name="folder[]">
                                                            <label class="form-check-label" for="folder{{$value->id}}">
                                                            </label>
                                                        </div>
                                                    </td>
                                                @endif
                                                @if(Auth()->user()->hasPermissionTo('employeeFolderPermision_name') || Auth::user()->all_companies == 1 )
                                                    <td>{!! $value->name !!}
                                                        @if($value->form_group_id == 0)
                                                            (Root)
                                                        @endif
                                                    </td>
                                                @endif

                                                    <td>{{$value->filecount }}
                                                    </td>

                                            </tr>
                                        @endforeach()
                                    </tbody>
                                </table>
                                <button class="btn btn-outline-info btn-rounded btn-block my-4 waves-effect z-depth-0" type="submit">
                                    <span class="assign_class label{{getKeyid('update',$data)}}" data-id="{{getKeyid('update',$data) }}" data-value="{{checkKey('update',$data) }}" >
                                        {!! checkKey('update',$data) !!}
                                    </span>
                                </button>
                            </form>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="table-responsive">
                            <form  action="{{url('/employee/documents/file/update')}}" method="post">
                                @csrf
                                <table  class="company_table_static table  table-striped table-bordered " cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        @if(Auth()->user()->hasPermissionTo('employeeFilePermision_checkbox') || Auth::user()->all_companies == 1 )
                                            <th class="no-sort all_checkboxes_style">
                                                <div class="form-check">
                                                    <input type="checkbox" class="form-check-input" id="file_permission_checkbox_all">
                                                    <label class="form-check-label" for="file_permission_checkbox_all">
                                                        <span class="assign_class label{{getKeyid('all',$data)}}" data-id="{{getKeyid('all',$data) }}" data-value="{{checkKey('all',$data) }}" >
                                                            {!! checkKey('all',$data) !!}
                                                        </span>
                                                    </label>
                                                </div>
                                            </th>
                                        @endif
                                        @if(Auth()->user()->hasPermissionTo('employeeFilePermision_name') || Auth::user()->all_companies == 1 )
                                            <th>
                                                <span class="assign_class label{{getKeyid('name',$data)}}" data-id="{{getKeyid('name',$data) }}" data-value="{{checkKey('name',$data) }}" >
                                                    {!! checkKey('name',$data) !!}
                                                </span>
                                            </th>
                                        @endif
                                        @if(Auth()->user()->hasPermissionTo('employeeFilePermision_folder_name') || Auth::user()->all_companies == 1 )
                                            <th>
                                                <span class="assign_class label{{getKeyid('folder_name',$data)}}" data-id="{{getKeyid('folder_name',$data) }}" data-value="{{checkKey('folder_name',$data) }}" >
                                                    {!! checkKey('folder_name',$data) !!}
                                                </span>
                                            </th>
                                        @endif
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($files as $key=>$value)
                                        <tr>
                                            @if(Auth()->user()->hasPermissionTo('employeeFilePermision_checkbox') || Auth::user()->all_companies == 1 )
                                                <td>
                                                    @if($value->enable == 1)
                                                        <input type="hidden" name="file_id[]" value="{{$value->id}}">
                                                    @endif
                                                    <div class="form-check">
                                                        <input type="checkbox" {{($value->enable == 1) ? 'checked' : '' }}  class="form-check-input file_permission_checked" id="file{{$value->id}}" value="{{$value->id}}" name="file[]">
                                                        <label class="form-check-label" for="file{{$value->id}}">
                                                        </label>
                                                    </div>
                                                </td>
                                            @endif
                                            @if(Auth()->user()->hasPermissionTo('employeeFilePermision_name') || Auth::user()->all_companies == 1 )
                                                <td>{!! $value->name !!}
                                                </td>
                                            @endif
                                            @if(Auth()->user()->hasPermissionTo('employeeFilePermision_folder_name') || Auth::user()->all_companies == 1 )
                                                <td>
                                                    @if(isset($value->form_group->name))
                                                        {{$value->form_group->name}}
                                                    @else
                                                        Root
                                                    @endif
                                                    </td>
                                             @endif
                                        </tr>
                                    @endforeach()
                                    </tbody>
                                </table>
                                <button class="btn btn-outline-info btn-rounded btn-block my-4 waves-effect z-depth-0" type="submit">
                                    <span class="assign_class label{{getKeyid('update',$data)}}" data-id="{{getKeyid('update',$data) }}" data-value="{{checkKey('update',$data) }}" >
                                        {!! checkKey('update',$data) !!}
                                    </span>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
