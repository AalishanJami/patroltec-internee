@extends('backend.layouts.doc')
@section('content')
@php
$data=localization();
@endphp
<style type="text/css">
  .white-color
  {
    color:#fff;
  }
  #pdf-viewer
  {
    height: 700px;
  }
</style>
@include('backend.layouts.doc_sidebar')
<div class="padding-left-custom remove-padding">
   {{ Breadcrumbs::render('statics/forms') }}
   <div class="locations-form container-fluid form_body">
      <div class="card">
         <h5 class="card-header  t py-4 white-color">
            <strong>Static Forms View</strong>
              <span class=" float-right mb-3 mr-2">
                <a href="{{url('static_form/download/pdf/'.$id)}}" class="btn btn-success btn-sm ">
                        Download As Pdf
                </a>
                <a  href="{{url('static_form/download/orginal/'.$id)}}" class="btn btn-secondary btn-sm ">
                        Download As Orginal
                </a>
              </span>
         </h5>
         <div id="pdf-viewer"></div>
      </div>
   </div>
</div>

<script src="{{asset('js/pdfview.js') }}"></script>
<script type="text/javascript">
  var options = {
    pdfOpenParams: {toolbar: '0'}
};

  PDFObject.embed("{{ route('view-pdf', ['id' => $id]) }}", "#pdf-viewer",options);
</script>
@endsection
