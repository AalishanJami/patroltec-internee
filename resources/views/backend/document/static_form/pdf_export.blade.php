@include('backend.layouts.pdf_start')
    <thead>
    <tr>
        @if(Auth()->user()->hasPermissionTo('documentStaticForm_title_pdf_export') || Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout">Title</th>
        @else
            <th style="vertical-align: text-top;" class="pdf_table_layout"></th>
        @endif
        @if(Auth()->user()->hasPermissionTo('documentStaticForm_notes_pdf_export') || Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout">Note</th>
        @else
            <th style="vertical-align: text-top;" class="pdf_table_layout"></th>
        @endif
    </tr>
    </thead>
    <tbody>
    @foreach($data as $list)
        <tr>
            @if(Auth()->user()->hasPermissionTo('documentStaticForm_title_pdf_export') || Auth::user()->all_companies == 1 )
                <td class="pdf_table_layout"><p class="col_text_style">{{ $list->name }}</p></td>
            @else
                <td class="pdf_table_layout"><p class="col_text_style"></p></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('documentStaticForm_notes_pdf_export') || Auth::user()->all_companies == 1 )
                <td class="pdf_table_layout"><p class="col_text_style">{!! $list->notes !!}</p></td>
            @else
                <td class="pdf_table_layout"><p class="col_text_style"></p></td>
            @endif
        </tr>
    @endforeach
    </tbody>
@include('backend.layouts.pdf_end')
