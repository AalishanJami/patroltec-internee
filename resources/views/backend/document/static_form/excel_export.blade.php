<table>
    <thead>
    <tr>
        @if(Auth()->user()->hasPermissionTo('documentStaticForm_title_excel_export') || Auth::user()->all_companies == 1 )
            <th>Title</th>
        @else
            <th></th>
        @endif
        @if(Auth()->user()->hasPermissionTo('documentStaticForm_notes_excel_export') || Auth::user()->all_companies == 1 )
            <th>Note</th>
        @else
            <th></th>
        @endif
    </tr>
    </thead>
    <tbody>
    @foreach($data as $list)
        <tr>
            @if(Auth()->user()->hasPermissionTo('documentStaticForm_title_excel_export') || Auth::user()->all_companies == 1 )
                <td style="width: 25% !important;">{{ $list->name }}</td>
            @else
                <td></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('documentStaticForm_notes_excel_export') || Auth::user()->all_companies == 1 )
                <td style="width: 75% !important;">{!! $list->notes !!}</td>
            @else
                <td></td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>
