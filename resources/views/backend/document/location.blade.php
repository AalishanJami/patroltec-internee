@extends('backend.layouts.doc')
@section('content')
    @php
       $data=localization();
    @endphp
    <div class="row">
        @include('backend.layouts.doc_sidebar')
        <div class="padding-left-custom remove-padding">
            {{ Breadcrumbs::render('location') }}
                  <div class="card">

            <div class="card-body">
                <div id="table" class="table-editable">
            <span class="table-add float-right mb-3 mr-2">
                <a class="text-success"  data-toggle="modal" data-target="#locationCreate">
                    <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
                </a>
            </span>
            <button class="btn btn-danger btn-sm">
                Delete Selected Location
            </button>
            <button class="btn btn-primary btn-sm">
              Restore Deleted Location
            </button>

            <button  style="display: none;">
                Show Active Location
            </button>
            <button class="btn btn-warning btn-sm">
               Excel Export
            </button>
            <button class="btn btn-success btn-sm">
                Word Export
            </button>
            <table id="company_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th> ID
                    </th>
                    <th> Ref
                    </th>
                    <th> Account Number
                    </th>
                    <th> Notes
                    </th>
                    <th id="action">
                    </th>
                </tr>
                </thead>
                    <tr>
                        <td class="pt-3-half" contenteditable="true">1</td>
                        <td class="pt-3-half" contenteditable="true">L1GG H67</td>
                        <td class="pt-3-half" contenteditable="true">9877666-8666</td>
                        <td class="pt-3-half" contenteditable="true">All Information</td>
                        <td class="pt-3-half" contenteditable="true">
                            <a  data-toggle="modal" data-target="#modelEditLocation" class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i></a>
                            <button type="button"class="btn btn-danger btn-sm my-0" onclick="deletelocationdata()"><i class="fas fa-trash"></i></button>

                        </td>
                    </tr>
                <tbody>
                </tbody>
            </table>
                        </div>
            </div>
        </div>
            @component('backend.location.model.create')
            @endcomponent

             {{--    Edit Modal--}}
            @component('backend.location.model.edit')
            @endcomponent
            {{--    END Edit Modal--}}
        </div>
    </div>



@endsection
<script type="text/javascript">
function deletelocationdata()
{

    swal({
        title:'Are you sure?',
        text: "Delete Record.",
        type: "error",
        showCloseButton: true,
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, Delete!",
        cancelButtonText: "Cancel!",
        closeOnConfirm: false,
        customClass: "confirm_class",
        closeOnCancel: false,
    },
    function(isConfirm){
        swal.close();
 });


}

</script>
