@extends('backend.layouts.doc')
@section('title', 'Notes')
@section('content')
    @php
        $data=localization();
    @endphp
    @include('backend.layouts.doc_sidebar')
    <div class="padding-left-custom remove-padding">
        <link rel="stylesheet" href="{{ asset('custom/css/custom_style.css') }}">
        {!! breadcrumInner('notes',Session::get('form_id'),'forms','/document/detail/'.Session::get('form_id'),'documents','/form_group') !!}
        @include('backend.label.input_label')
        <input type="hidden" name="flag" id="flagNote" value="1">
        <div class="locations-form container-fluid form_body">
            <div class="card">
                <div class="card-body">
                    <div id="table" class="table-editable table-responsive">
                        @if(Auth()->user()->hasPermissionTo('create_documentNotes') || Auth::user()->all_companies == 1 )
                            <span class="table-add float-right mb-3 mr-2">
                            <a class="text-success document_notes_create"  data-toggle="modal" data-target="#modalNotes">
                                <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
                            </a>
                        </span>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_delete_documentNotes') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-danger btn-sm" id="deleteselectednote">
                            <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                                {!! checkKey('selected_delete',$data) !!}
                            </span>
                            </button>
                            <button class="btn btn-danger btn-sm" id="deleteselectednoteSoft" style="display: none;">
                            <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                                {!! checkKey('selected_delete',$data) !!}
                            </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('restore_delete_documentNotes') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-primary btn-sm" id="restore_button_note">
                            <span class="assign_class label{{getKeyid('restore',$data)}}" data-id="{{getKeyid('restore',$data) }}" data-value="{{checkKey('restore',$data) }}" >
                                {!! checkKey('restore',$data) !!}
                            </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_active_button_documentNotes') || Auth::user()->all_companies == 1 )
                            <button  style="display: none;" id="selectedactivebuttonnote" class="btn btn-success btn-sm assign_class label{{getKeyid('selected_active',$data)}}"  data-id="{{getKeyid('selected_active',$data) }}" data-value="{{checkKey('selected_active',$data) }}" >
                                {!! checkKey('selected_active',$data) !!}
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('active_documentNotes') || Auth::user()->all_companies == 1 )
                            <button  style="display: none;"  class="btn btn-primary btn-sm" id="show_active_button_note">
                            <span class="assign_class label{{getKeyid('show_active',$data)}}" data-id="{{getKeyid('show_active',$data) }}" data-value="{{checkKey('show_active',$data) }}" >
                                {!! checkKey('show_active',$data) !!}
                            </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('csv_documentNotes') || Auth::user()->all_companies == 1 )
                            <form class="form-style" id="export_excel_note" method="POST" action="{{ url('note/export/excel') }}">
                                <input type="hidden"  name="link_id" value="2">
                                <input type="hidden"  name="link_name" value="document">
                                <input type="hidden" class="note_export" name="excel_array" value="1">
                                {!! csrf_field() !!}
                                <button  type="submit" id="export_excel_employee_btn" class="form_submit_check btn btn-warning btn-sm">
                            <span class="assign_class label{{getKeyid('excel_export',$data)}}" data-id="{{getKeyid('excel_export',$data) }}" data-value="{{checkKey('excel_export',$data) }}" >
                                {!! checkKey('excel_export',$data) !!}
                            </span>
                                </button>
                            </form>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('word_documentNotes') || Auth::user()->all_companies == 1 )
                            <form class="form-style" id="export_word_note" method="POST" action="{{ url('note/export/word') }}">
                                {!! csrf_field() !!}
                                <input type="hidden"  name="link_id" value="2">
                                <input type="hidden"  name="link_name" value="document">
                                <input type="hidden" class="note_export" name="word_array" value="1">
                                <button  type="submit" id="export_word_note" class="form_submit_check btn btn-success btn-sm">
                            <span class="assign_class label{{getKeyid('word_export',$data)}}" data-id="{{getKeyid('word_export',$data) }}" data-value="{{checkKey('word_export',$data) }}" >
                                {!! checkKey('word_export',$data) !!}
                            </span>
                                </button>
                            </form>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('pdf_documentNotes') || Auth::user()->all_companies == 1 )
                            <form  class="form-style" {{pdf_view('notes','2')}} id="export_pdf_note" method="POST" action="{{ url('note/export/pdf') }}">
                                <input type="hidden"  name="link_id" value="2">
                                <input type="hidden"  name="link_name" value="document">
                                <input type="hidden" class="note_export" name="pdf_array" value="1">
                                {!! csrf_field() !!}
                                <button  type="submit" id="export_pdf_note_btn"  class="form_submit_check btn btn-secondary btn-sm">
                            <span class="assign_class label{{getKeyid('pdf_export',$data)}}" data-id="{{getKeyid('pdf_export',$data) }}" data-value="{{checkKey('pdf_export',$data) }}" >
                                {!! checkKey('pdf_export',$data) !!}
                            </span>
                                </button>
                            </form>
                        @endif
                        <table  id="clientNoteTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th class="no-sort all_checkboxes_style">
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input notes_checked" id="notes_checkbox_all">
                                        <label class="form-check-label" for="notes_checkbox_all">
                                            <span class="assign_class label{{getKeyid('all',$data)}}" data-id="{{getKeyid('all',$data) }}" data-value="{{checkKey('all',$data) }}" >
                                                {!! checkKey('all',$data) !!}
                                            </span>
                                        </label>
                                    </div>
                                </th>
                                <th>
                                    <span class="assign_class label{{getKeyid('subject',$data)}}" data-id="{{getKeyid('subject',$data) }}" data-value="{{checkKey('subject',$data) }}" >
                                        {!! checkKey('subject',$data) !!}
                                    </span>
                                </th>
                                <th>
                                    <span class="assign_class label{{getKeyid('notes',$data)}}" data-id="{{getKeyid('notes',$data) }}" data-value="{{checkKey('notes',$data) }}" >
                                        {!! checkKey('notes',$data) !!}
                                    </span>
                                </th>
                                <th class="no-sort all_action_btn" id="action"></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backend.label.input_label')
    @include('backend.notes.model.documents.create')
    @include('backend.notes.model.documents.edit')
    @if(Auth()->user()->all_companies == 1)
        <input type="hidden" id="documentNotes_checkbox" value="1">
        <input type="hidden" id="documentNotes_notes" value="1">
        <input type="hidden" id="documentNotes_subject" value="1">
    @else
        <input type="hidden" id="documentNotes_checkbox" value="{{Auth()->user()->hasPermissionTo('documentNotes_checkbox')}}">
        <input type="hidden" id="documentNotes_notes" value="{{Auth()->user()->hasPermissionTo('documentNotes_notes')}}">
        <input type="hidden" id="documentNotes_subject" value="{{Auth()->user()->hasPermissionTo('documentNotes_subject')}}">
    @endif

    <script type="text/javascript" src="{{ asset('custom/js/note.js') }}" ></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $(function () {
                documentNoteForm
                var url='/note/getall';
                noteAppend(url);
            });
            $('body').on('click','.document_notes_create',function() {
                document.getElementById("documentNoteForm").reset();
                $('.body_notes').summernote('code', '');
            });
        });
        function noteAppend(url)
        {
            var table = $('#clientNoteTable').dataTable({
                processing: true,
                binfo:false,
                language: {
                    'lengthMenu': '  _MENU_ ',
                    'search':'<span class="assign_class label'+$('#search_id').attr('data-id')+'" data-id="'+$('#search_id').attr('data-id')+'" data-value="'+$('#search_id').val()+'" >'+$('#search_id').val()+'</span>',
                    'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                    'info':'',
                    'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                    'paginate': {
                        'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                        'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                    },
                    'infoFiltered': "(filtered from _MAX_ total records)"
                },
                "ajax": {
                    "headers": {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    "url": url,
                    "type": 'post',
                    "data":  {'flag':'2' },
                },
                "createdRow": function( row, data, dataIndex,columns ) {
                    var checkbox='';
                    checkbox+='<div class="form-check"><input type="checkbox" class="form-check-input notes_checked selectedrownote" id="note'+data.id+'"><label class="form-check-label" for="note'+data.id+'""></label></div>';
                    $(columns[0]).html(checkbox);
                    $(row).attr('data-id', data['id']);
                    $(row).attr('id', 'note_tr'+data['id']);
                    var temp=data['id'];
                    // $(row).attr('class', 'selectedrownote');
                },
                columns: [
                    {data: 'checkbox', name: 'checkbox',visible:$('#documentNotes_checkbox').val()},
                    {data: 'title', name: 'title',visible:$('#documentNotes_subject').val()},
                    {data: 'note', name: 'note',visible:$('#documentNotes_notes').val()},
                    {data: 'actions', name: 'actions'},
                ],
                columnDefs: [ {
                    'targets': [0,1], /* column index */
                    'orderable': false, /* true or false */
                }],
            });
            // (function ($) {
            //     var active ='<span class="assign_class label'+$('#breadcrumb_document_notes').attr('data-id')+'" data-id="'+$('#breadcrumb_document_notes').attr('data-id')+'" data-value="'+$('#breadcrumb_document_notes').val()+'" >'+$('#breadcrumb_document_notes').val()+'</span>';
            //     $('.breadcrumb-item.active').html(active);
            //     var parent ='<span class="assign_class label'+$('#dashboard_label').attr('data-id')+'" data-id="'+$('#dashboard_label').attr('data-id')+'" data-value="'+$('#dashboard_label').val()+'" >'+$('#dashboard_label').val()+'</span>';
            //     $('.breadcrumb-item a').html(parent);
            // }(jQuery));
            if ($(":checkbox").prop('checked',true)){
                $(":checkbox").prop('checked',false);
            }
        }
    </script>
@endsection
