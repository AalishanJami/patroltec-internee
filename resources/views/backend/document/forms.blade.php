@extends('backend.layouts.doc')
@section('title', 'Dynamic Form')
@section('content')
    <link href="https://fonts.googleapis.com/css2?family=Gloria+Hallelujah&display=swap" rel="stylesheet">
    @php
        $data=localization();
    @endphp
    <link rel="stylesheet" href="{{ asset('custom/css/custom_style.css') }}">
    <link rel="stylesheet" href="{{ asset('custom/css/dynamic_form_style.css') }}">
    @include('backend.layouts.doc_sidebar')
    <div class="padding-left-custom remove-padding">
        {!! breadcrumInner('dynamic_forms',Session::get('form_id'),'forms','/document/detail/'.Session::get('form_id'),'documents','/form_group') !!}
        <div class="active_plus_btn">
            <ul class="float-right  mr-4">
                <li class="hover">
                    <!-- <a class="text-success active_plus_btn_link" style="background-color: lightgreen!important" onclick="populate()">
                        <i class="white-color active_plus_btn_icon fas fa-save fa-md" aria-hidden="true"><span class="ml-1 active_plus_btn_text">
                            <span class="assign_class label{{getKeyid('save',$data)}}" data-id="{{getKeyid('save',$data) }}" data-value="{{checkKey('save',$data) }}" >
                              {!! checkKey('save',$data) !!}
                            </span>
                        </span></i>
                    </a> -->
                    <a class="btn btn-cyan px-3" data-toggle="tooltip" data-placement="left" title="Save" onclick="populate()"><i class="fas fa-save" aria-hidden="true"></i></a>
                </li>
                @if(Auth()->user()->hasPermissionTo('form_create_documentDynamicForm') || Auth::user()->all_companies == 1 )
                <li class="hover">
                    <a class="btn btn-cyan px-3 model_create_form_section_popup" datatoggle="tooltip" data-placement="left" title="Add Section" data-toggle="modal" data-target="#model_create_form_section"><i class="fas fa-plus" aria-hidden="true"></i></a>

                    <!-- <a class="text-success active_plus_btn_link" data-toggle="modal" data-target="#model_create_form_section">
                        <i class="white-color active_plus_btn_icon fas fa-plus fa-md" aria-hidden="true"><span class="ml-1 active_plus_btn_text">
                            <span class="assign_class label{{getKeyid('new',$data)}}" data-id="{{getKeyid('new',$data) }}" data-value="{{checkKey('new',$data) }}" >
                              {!! checkKey('new',$data) !!}
                            </span>
                        </span></i> -->
                    <!-- </a> -->
                </li>
                @endif
                @if(Auth()->user()->hasPermissionTo('form_activate_documentDynamicForm') || Auth::user()->all_companies == 1 )
                <li class="hover">
                    <a class="btn btn-cyan px-3" onClick="getform_sectioncompletedata({{Session::get('form_id')}})" data-toggle="tooltip" data-placement="left" title="Active"><i class="fas fa-check" aria-hidden="true"></i></a>

                   <!--  <a onClick="getform_sectioncompletedata({{Session::get('form_id')}})"  class="active_plus_btn_link">
                        <i class="white-color fa fa-check active_plus_btn_icon" aria-hidden="true"><span class="ml-1 active_plus_btn_text">
                    <li class="hover">
                        <a class="text-success active_plus_btn_link" data-toggle="modal" data-target="#model_create_form_section">
                            <i class="white-color active_plus_btn_icon fas fa-plus fa-md" aria-hidden="true"><span class="ml-1 active_plus_btn_text">
                            <span class="assign_class label{{getKeyid('new',$data)}}" data-id="{{getKeyid('new',$data) }}" data-value="{{checkKey('new',$data) }}" >
                              {!! checkKey('new',$data) !!}
                            </span>
                        </span></i>
                        </a>
                    </li> -->
                </li>
                @endif
            </ul>
        </div>
        <div class="locations-form container-fluid form_body">
            <div class="card">
                <ul  class="nav nav-tabs" id="myTab" role="tablist">
                    @if(Auth()->user()->hasPermissionTo('form_view_documentDynamicForm') || Auth::user()->all_companies == 1 )
                        <li class="nav-item">
                            <a class="nav-link active" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile"
                               aria-selected="false">
                           <span class="assign_class label{{getKeyid('draft',$data)}}" data-id="{{getKeyid('draft',$data) }}" data-value="{{checkKey('draft',$data) }}" >
                              {!! checkKey('draft',$data) !!}
                            </span>
                            </a>
                        </li>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('form_activate_documentDynamicForm') || Auth::user()->all_companies == 1 )
                        <li class="nav-item">
                            <a class="nav-link" id="contact-tab" onclick="getAllFormsActive()" data-toggle="tab" href="#contact" role="tab" aria-controls="contact"
                               aria-selected="false">
                               <span class="assign_class label{{getKeyid('active',$data)}}" data-id="{{getKeyid('active',$data) }}" data-value="{{checkKey('active',$data) }}" >
                                  {!! checkKey('active',$data) !!}
                                </span>
                            </a>
                        </li>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('form_share_documentDynamicForm') || Auth::user()->all_companies == 1 )
                        <li class="nav-item">
                            <a class="nav-link " id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home"
                               aria-selected="true">
                               <span class="assign_class label{{getKeyid('questions',$data)}}" data-id="{{getKeyid('questions',$data) }}" data-value="{{checkKey('questions',$data) }}" >
                                  {!! checkKey('questions',$data) !!}
                                </span>
                            </a>
                        </li>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('view_document_answer_group') || Auth::user()->all_companies == 1 )
                        <li class="nav-item">
                            <a class="nav-link " id="answer-tab" data-toggle="tab" href="#answer" role="tab" aria-controls="answer" aria-selected="true">
                                <span class="assign_class label{{getKeyid('answer_group',$data)}}" data-id="{{getKeyid('answer_group',$data) }}" data-value="{{checkKey('answer_group',$data) }}" >
                                  {!! checkKey('answer_group',$data) !!}
                                </span>
                            </a>
                        </li>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('view_documentStaticForm') || Auth::user()->all_companies == 1 )
                        <li class="nav-item">
                            <a class="nav-link" id="static-tab" data-toggle="tab" href="#static" role="tab" aria-controls="static"
                               aria-selected="true">
                            <span class="assign_class label{{getKeyid('static_form',$data)}}" data-id="{{getKeyid('static_form',$data) }}" data-value="{{checkKey('static_form',$data) }}" >
                              {!! checkKey('static_form',$data) !!}
                            </span>
                            </a>
                        </li>
                    @endif
                    @if(!empty($pre_populate))
                        <form class="nav-select pull-right" id='addPopulate'>
                            <div class="form-row" id="pop_div">
                                <div class="col" id="pre_populate_listing">
                                    {!! Form::select('pre_populate_id',$pre_populate,null, ['class' =>'mdb-select','placeholder'=>'Select Form','id'=>'pre_populate_id']) !!}
                                </div>
                                <div class="col-md-2 mt-1 pull-right ml-2">
                                    <a class="btn btn-cyan px-3" data-toggle="modal" data-target="#modalPopulate">
                                        <i class="fas fa-plus " aria-hidden="true"></i>
                                    </a>
<!--
                                    <a class="text-success modalPopulate_btn"  data-toggle="modal" data-target="#modalPopulate" >
                                        <i class="fa fa-plus fa-2x modalPopulate_btn_icon"></i>
                                    </a> -->
                                </div>

                            </div>
                        </form>
                    @endif
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <h5 class="card-header  t py-4 white-color">
                            <strong>{{$name}}</strong>
                            <span class=" float-right mb-3 mr-2">
{{--                      <a class="text-success" data-toggle="modal" data-target="#model_create_form_section">--}}
                                {{--                        <i class="white-color fas fa-plus fa-md" aria-hidden="true"></i>--}}
                                {{--                      </a>--}}
                                {{--                      <a onClick="getform_sectioncompletedata({{Session::get('form_id')}})"  class="btn-primary">--}}
                                {{--                      <i class="white-color fa fa-check" aria-hidden="true"></i>--}}
                                {{--                      </a>--}}
                    </span>
                        </h5>
                        <div id="loading_gif" style="display: none;">
                            <img src="{{url('Loading.gif')}}">
                        </div>
                        <form id="dynamicForm">

                            <div class="card-deck" style="margin-top: 20px" id="div_for_dynamic_forms_display">
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                        <h5 class="card-header  t py-4 white-color">
                            <strong>{{$name}}</strong>
                            <span class=" float-right mb-3 mr-2">
                         <a href="{{url('/document/forms/copyActive')}}" class="btn-primary" id="document-forms-copyActive">
                      <i class="white-color fa fa-copy" aria-hidden="true"></i>
                      </a>
                    </span>
                        </h5>
                        <div class="card-deck" style="margin-top: 20px" id="div_for_dynamic_forms_display_active">
                        </div>
                    </div>
                    <div class="tab-pane fade " id="home" role="tabpanel" aria-labelledby="home-tab">
                        <h5 class="card-header  t py-4 white-color">
                            <strong>
                                <span class="assign_class label{{getKeyid('shared_question',$data)}}" data-id="{{getKeyid('shared_question',$data) }}" data-value="{{checkKey('shared_question',$data) }}" >
                                  {!! checkKey('shared_question',$data) !!}
                                </span>
                            </strong>
                            <span class=" float-right mb-3 mr-2">
                            </span>
                        </h5>
                        <div class="card-deck" style="margin-top: 20px" id="div_for_dynamic_forms_display_question">
                        </div>
                    </div>
                    <div class="tab-pane fade " id="answer" role="tabpanel" aria-labelledby="answer-tab">
                        <div id="table-answer_group" class="table-editable table-responsive">
                            @if(Auth()->user()->hasPermissionTo('create_document_answer_group') || Auth::user()->all_companies == 1 )
                                <span class="add-answer_group float-right mb-3 mr-2">
                                  <a class="text-success">
                                    <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
                                  </a>
                                </span>
                            @endif
                            @if(Auth()->user()->hasPermissionTo('selected_delete_document_answer_group') || Auth::user()->all_companies == 1 )
                                <button class="btn btn-danger btn-sm" id="deleteselectedanswer_group">
                                    <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                                        {!! checkKey('selected_delete',$data) !!}
                                    </span>
                                </button>
                            @endif
                            @if(Auth()->user()->hasPermissionTo('selected_delete_document_answer_group') || Auth::user()->all_companies == 1 )
                                <button class="btn btn-danger btn-sm" style="display: none;" id="deleteselectedanswer_groupSoft">
                                    <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                                        {!! checkKey('selected_delete',$data) !!}
                                    </span>
                                </button>
                            @endif
                            @if(Auth()->user()->hasPermissionTo('selected_active_button_document_answer_group') || Auth::user()->all_companies == 1 )
                                <button class="btn btn-success btn-sm" style="display: none;" id="selectedactivebuttonanswer_group">
                                    <span class="assign_class label{{getKeyid('selected_active',$data)}}" data-id="{{getKeyid('selected_active',$data) }}" data-value="{{checkKey('selected_active',$data) }}" >
                                        {!! checkKey('selected_active',$data) !!}
                                    </span>
                                </button>
                            @endif
                            @if(Auth()->user()->hasPermissionTo('restore_delete_document_answer_group') || Auth::user()->all_companies == 1 )
                                <button class="btn btn-primary btn-sm" id="restorebuttonanswer_group">
                                    <span class="assign_class label{{getKeyid('restore',$data)}}" data-id="{{getKeyid('restore',$data) }}" data-value="{{checkKey('restore',$data) }}" >
                                        {!! checkKey('restore',$data) !!}
                                    </span>
                                </button>
                            @endif
                            @if(Auth()->user()->hasPermissionTo('active_document_answer_group') || Auth::user()->all_companies == 1 )
                                <button  style="display: none;" class="btn btn-primary btn-sm" id="activebuttonanswer_group">
                                    <span class="assign_class label{{getKeyid('show_active',$data)}}" data-id="{{getKeyid('show_active',$data) }}" data-value="{{checkKey('show_active',$data) }}" >
                                        {!! checkKey('show_active',$data) !!}
                                    </span>
                                </button>
                            @endif
                            @if(Auth()->user()->hasPermissionTo('csv_document_answer_group') || Auth::user()->all_companies == 1 )
                                <form class="form-style" method="POST" id="export_excel_answer_group" action="{{ url('answer_group/export/excel') }}">
                                    {!! csrf_field() !!}
                                    <input type="hidden" class="answer_group_export" name="excel_array" value="1">
                                    <button  type="submit" id="export_excel_answer_group_btn" class="form_submit_check btn btn-warning btn-sm">
                                        <span class="assign_class label{{getKeyid('excel_export',$data)}}" data-id="{{getKeyid('excel_export',$data) }}" data-value="{{checkKey('excel_export',$data) }}" >
                                            {!! checkKey('excel_export',$data) !!}
                                        </span>
                                    </button>
                                </form>
                            @endif
                            @if(Auth()->user()->hasPermissionTo('word_document_answer_group') || Auth::user()->all_companies == 1 )
                                <form class="form-style" method="POST" id="export_world_answer_group" action="{{ url('answer_group/export/world') }}">
                                    <input type="hidden" class="answer_group_export" name="word_array" value="1">
                                    {!! csrf_field() !!}
                                    <button  type="submit" id="export_world_answer_group_btn" class="form_submit_check btn btn-success btn-sm">
                                        <span class="assign_class label{{getKeyid('word_export',$data)}}" data-id="{{getKeyid('word_export',$data) }}" data-value="{{checkKey('word_export',$data) }}" >
                                            {!! checkKey('word_export',$data) !!}
                                        </span>
                                    </button>
                                </form>
                            @endif
                            @if(Auth()->user()->hasPermissionTo('pdf_document_answer_group') || Auth::user()->all_companies == 1 )
                                <form  class="form-style" {{pdf_view('answer_groups')}} method="POST" id="export_pdf_answer_group" action="{{ url('answer_group/export/pdf') }}">
                                    <input type="hidden" class="answer_group_export" name="pdf_array" value="1">
                                    {!! csrf_field() !!}
                                    <button  type="submit" id="export_pdf_answer_group_btn"  class="form_submit_check btn btn-secondary btn-sm">
                                        <span class="assign_class label{{getKeyid('pdf_export',$data)}}" data-id="{{getKeyid('pdf_export',$data) }}" data-value="{{checkKey('pdf_export',$data) }}" >
                                            {!! checkKey('pdf_export',$data) !!}
                                        </span>
                                    </button>
                                </form>
                            @endif
                            <table id="answer_group_table"  class="answer_group_table table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th class="no-sort all_checkboxes_style_dynamicform">
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input answer_group_checked" id="answer_group_checkbox_all">
                                            <label class="form-check-label" for="answer_group_checkbox_all">
                                                <span class="assign_class label{{getKeyid('all',$data)}}" data-id="{{getKeyid('all',$data) }}" data-value="{{checkKey('all',$data) }}" >
                                                    {!! checkKey('all',$data) !!}
                                                </span>
                                            </label>
                                        </div>
                                    </th>
                                    <th class="text-left all_name_style_dynamicform">
                                        <span class="assign_class label{{getKeyid('answer',$data)}}" data-id="{{getKeyid('answer',$data) }}" data-value="{{checkKey('answer',$data) }}" >
                                          {!! checkKey('answer',$data) !!}
                                        </span>
                                    </th>
                                    <th class="no-sort all_checkboxes_style_dynamicform">
                                        <span class="assign_class label{{getKeyid('sig_select',$data)}}" data-id="{{getKeyid('sig_select',$data) }}" data-value="{{checkKey('sig_select',$data) }}" >
                                          {!! checkKey('sig_select',$data) !!}
                                        </span>
                                    </th>
                                    <th class="text-center no-sort all_action_btn_dynamicform"></th>
                                </tr>
                                </thead>
                                <tbody id="answer_group_data"></tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade " id="static" role="tabpanel" aria-labelledby="static-tab">
                        @include('backend.document.static_form')
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- answer Group  -->
    @if(Auth()->user()->all_companies == 1)
        <input type="hidden" id="document_answer_group_checkbox" value="1">
        <input type="hidden" id="document_answer_group_name" value="1">
        <input type="hidden" id="document_answer_group_edit" value="1">
        <input type="hidden" id="document_answer_group_create" value="1">
        <input type="hidden" id="edit_permision_document_answer_group" value="1">
    @else
        <input type="hidden" id="document_answer_group_checkbox" value="{{Auth()->user()->hasPermissionTo('document_answer_group_checkbox')}}">
        <input type="hidden" id="document_answer_group_name" value="{{Auth()->user()->hasPermissionTo('document_answer_group_name')}}">
        <input type="hidden" id="document_answer_group_edit" value="{{Auth()->user()->hasPermissionTo('document_answer_group_name_edit')}}">
        <input type="hidden" id="document_answer_group_create" value="{{Auth()->user()->hasPermissionTo('document_answer_group_name_create')}}">
        <input type="hidden" id="edit_permision_document_answer_group" value="{{Auth()->user()->hasPermissionTo('edit_document_answer_group')}}">
    @endif

    @include('backend.document.model.question_edit_form')
    @include('backend.document.model.populate')
    @component('backend.document.model.edit_form')
    @endcomponent
    @component('backend.answer.model.answer')
    @endcomponent
    @include('backend.document.model.create_form')
    @include('backend.document.model.create_form_section')
    @include('backend.document.permission_input')
    <script src="{{asset('/custom/js/answer_group.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('body').on('change','.form_header_footer_height',function(){
                if ($(this).val() < 50){
                    $(this).val('50');
                }
            });
            $('.active_plus_btn').hide();

            // $("body").on("focusin",".edit_inline_answer_group",function(){
            //     $("#answer_group_tr"+$(this).attr('data-id')).addClass('selected');
            // });

            $('body').on('keyup','.form_header_footer_height',function(){
                if ($(this).val() < 50){
                    $(this).val('50');
                }
            });

            // (function ($) {
            //     var active ='<span class="assign_class label'+$('#breadcrumb_dynamic_forms').attr('data-id')+'" data-id="'+$('#breadcrumb_dynamic_forms').attr('data-id')+'" data-value="'+$('#breadcrumb_dynamic_forms').val()+'" >'+$('#breadcrumb_dynamic_forms').val()+'</span>';
            //     $('.breadcrumb-item.active').html(active);
            //     var parent ='<span class="assign_class label'+$('#dashboard_label').attr('data-id')+'" data-id="'+$('#dashboard_label').attr('data-id')+'" data-value="'+$('#dashboard_label').val()+'" >'+$('#dashboard_label').val()+'</span>';
            //     $('.breadcrumb-item a').html(parent);
            // }(jQuery));
            getAllFormsActive();
            getAllFormsQuestion();
            if ($("#form_view_documentDynamicForm").val()=='1'){
                getAllForms();
            }
            var url='/answer_group/getall';
            answer_group_data(url);
        });
        // $(".form_datetime").datetimepicker({format: 'yyyy-mm-dd hh:ii'});
        var answergroupBool =false;
        var answerCount = 0;
        var answerGroupCount=0;
        function populate()
        {
            if( $('.edit_cms_disable').css('display') == 'none' ) {
                console.log('Editer mode On Please change your mode');
                return 0;
            }
            var formData = $("#dynamicForm").serializeObject();
            formData.pre_populate_id = $('#pre_populate_id').children("option:selected").val();
            $.ajax({
                type: 'POST',
                url: '/form/populate',
                data: {
                    "_token": "{{ csrf_token() }}",formData
                },
                success: function(response)
                {
                    toastr["success"](response.message);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
        var token = '@csrf';
        var upload ="{{url('answer/uploadIcon')}}";
        function answer_group_data(url)
        {
            var table = $('.answer_group_table').dataTable(
                {
                    processing: true,
                    language: {
                        'lengthMenu': '  _MENU_ ',
                        'search':'<span class="assign_class label'+$('#search_id').attr('data-id')+'" data-id="'+$('#search_id').attr('data-id')+'" data-value="'+$('#search_id').val()+'" >'+$('#search_id').val()+'</span>',
                        'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                        'info':'',
                        'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                        'paginate': {
                            'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                            'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                        },
                        'infoFiltered': "(filtered from _MAX_ total records)"
                    },
                    "ajax": {
                        "url": url,
                        "type": 'get',
                    },
                    "createdRow": function( row, data, dataIndex,columns )
                    {
                        var checkbox='';
                        checkbox+='<div class="form-check"><input type="checkbox" class="form-check-input selectedrowanswer_group" id="answer_group'+data.id+'"><label class="form-check-label" for="answer_group'+data.id+'""></label></div>';
                        var sig_select='';
                        var sig_select_checked=(data.sig_select=='1') ? "checked":"";
                        sig_select+='<div class="form-check"><input '+sig_select_checked+' type="checkbox" class="form-check-input is_checked'+data.sig_select+'  sig_select" data-id="'+data.id+'" value="1" id="sig_select'+data.id+'"><label class="form-check-label" for="sig_select'+data.id+'""></label></div>';
                        $(columns[0]).html(checkbox);
                        $(columns[2]).html(sig_select);
                        $(columns[2]).attr('class','edit_inline_answer_group');
                        $(columns[2]).attr('data-id',data['id']);
                        if($('#edit_permision_document_answer_group').val() && $('#document_answer_group_edit').val())
                        {
                            $(columns[1]).attr('Contenteditable', 'true');
                            $(columns[1]).attr('class','edit_inline_answer_group');
                        }
                        $(columns[1]).attr('data-id',data['id']);
                        $(columns[1]).attr('id', 'answer_group_name'+data['id']);
                        $(columns[1]).attr('onkeydown', 'editSelectedRow(answer_group_tr'+data['id']+')');
                        var submit='';
                        submit+='<a type="button" class="btn btn-primary btn-xs my-0 waves-effect waves-light saveanswer_groupdata answer_group_show_button_'+data['id']+'" style="display: none;" id="'+data.id+'"><i class="fas fa-check"></i></a>';
                        $(columns[3]).append(submit);
                        $(row).attr('data-id', data['id']);
                        $(row).attr('id', 'answer_group_tr'+data['id']);
                        // $(row).attr('class', 'selectedrowanswer_group');
                    },
                    columns:
                        [
                            {data: 'checkbox', name: 'checkbox',visible:$('#document_answer_group_checkbox').val()},
                            {data: 'name', name: 'name',visible:$('#document_answer_group_name').val()},
                            {data: 'sig_select', name: 'sig_select'},
                            {data: 'actions', name: 'actions'},
                            // {data: 'submit', name: 'submit'},
                        ],
                }
            );
            if ($(":checkbox").prop('checked',true)){
                $(":checkbox").prop('checked',false);
            }
        }
        function get_group_answer_id($id){
            sessionStorage.setItem('answer_group_id', $id);
            $('#modalAnswertablelist').modal('show');
            var url='/answer/getall/'+$id;
            $('.answer_table').DataTable().clear().destroy();
            answer_data(url);
        }
        $("body").on('click','.sig_select',function(){
           $(".answer_group_show_button_"+$(this).attr('data-id')).show();
           $(".delete_answergrp"+$(this).attr('data-id')).hide();
           if ($(this).is(':checked')){
               $(this).val('1');
           }else{
               $(this).val('0');
           }
        });
    </script>
    <script src="{{asset('editor/sprite.svg.js')}}"></script>
    <script src="{{asset('custom/js/form_sections.js') }}"></script>
    <script type="text/javascript">
        $('body').on('mouseleave', '.sortable-card', function() {
            setTimeout(reOrder, 2000);
        });
        function reOrder()
        {
            var orderArray=[];
            $('.sortable-card ').each(function(i, obj) {
                var temp=$(this).attr('id');
                if(temp)
                {
                    temp=temp.split('-');
                    var obj={
                        'id':temp[1],
                        'order':temp[0],
                    };
                    orderArray.push(obj);
                }
            });
            $.ajax({
                type: 'POST',
                url: '/document/forms/reorder',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "array" : orderArray
                },
                success: function(response)
                {
                    console.log("success")
                },
                error: function (error) {
                    console.log(error);
                }
            });

        }
        function getAllForms(){
            $.ajax({
                type: 'GET',
                url: '/document/forms/get',
                // data: data,
                success: function(response)
                {
                    var form_section = response.form_section;
                    var html = '';
                    var setheight_option= '';
                    for(var i = 0; i < form_section.length; i++){
                        html += ' <div id="'+form_section[i].order+'-'+form_section[i].id+'" class=" sortable-card col-md-'+form_section[i].width+'">';
                        html += '<div class="mb-4  custom_dynamic_height" id="form_section_height'+form_section[i].id+'">';
                        html += '<div class="card text-center">';
                        if (form_section[i].header_height!==null){
                            var header_height='height: '+form_section[i].header_height+'px;';
                        }else{
                            var header_height='';
                        }
                        if (form_section[i].show_header ==1){
                            var header_bg = 'background-color:#4c8bf5 !important;';
                        }else{
                            var header_bg = 'background-color:#00bcd4 !important;';
                        }
                        html += '<div class="card-header" id="section_card_header'+form_section[i].id+'" style="display:flex;'+header_height+header_bg+'">';
                        if ($("#documentDynamicForm_form_header").val()=='1'){
                            if (form_section[i].header !==null){
                                html += '<span class="white-color z-index" style="flex: 1; text-align:left; ">';
                                if(form_section[i].width!=3 )
                                {
                                    if (form_section[i].show_header ==1){
                                        html +=form_section[i].header;
                                    }else{
                                        html +='&nbsp;&nbsp;&nbsp;&nbsp';
                                    }
                                }
                                if(form_section[i].is_repeatable == 1)
                                {
                                    html += '<span class="ml-2 white-color" style="flex: 1; text-align:left; "><i class="fas fa-check" aria-hidden="true"></i></span>';
                                }
                                html +='</span>';
                            }else{
                                html += '<span class="white-color" style="flex: 1; text-align:left; ">&nbsp;&nbsp;&nbsp;&nbsp;</span>';
                            }
                        }else{
                            html +='<span class="white-color" style="flex: 1; text-align:left; ">&nbsp;&nbsp;&nbsp;</span>';
                        }
                        html += '<span class=" float-right mb-3 mr-2">';
                        if ($("#form_delete_documentDynamicForm").val()=='1'){
                            html += '<a  class="text-success remove_div form_btn_circle" datatoggle="tooltip" data-placement="top" title="Delete Section" id="form_section'+form_section[i].id+'">';
                            html += '<i class=" white-color fas fa-minus fa-md" aria-hidden="true"></i>';
                            html += '</a>';
                        }
                        if ($("#form_duplicate_documentDynamicForm").val()=='1'){
                            html += '<a class="form_section_clone form_btn_circle" datatoggle="tooltip" data-placement="top" title="Copy Section" id="form_section_clone'+form_section[i].id+'">';
                            html += '<i class="white-color far fa-copy   fa-sm"></i>';
                            html += '</a>';
                        }
                        if ($("#form_edit_documentDynamicForm").val()=='1'){
                            html += ' <a onClick="getform_sectiondata('+form_section[i].id+')" datatoggle="tooltip" data-placement="top" title="Update Section"  class=" form_btn_circle">';
                            html += '<i class="white-color fas fa-pencil-alt fa-sm" style="margin-left: auto;"></i>';
                            html += '</a> ';
                        }
                        if ($("#form_share_documentDynamicForm").val()=='1'){
                            if(form_section[i].shared!=1 && form_section[i].width!=3 )
                            {
                                console.log(form_section);
                                html += ' <a onClick="share('+form_section[i].id+')" datatoggle="tooltip" data-placement="top" title="Share Section"  class=" form_btn_circle">';
                                html += '<i style="font-size:14px" class="white-color fa">&#xf064;</i>';
                                html += '</a> ';
                            }
                        }

                        if ($("#create_documentQuestion").val()=='1'){
                            if(form_section[i].shared == 0)
                            {
                                html += '<a  class="text-success open_popup form_btn_circle" datatoggle="tooltip" data-placement="left" title="Add Question" id="'+form_section[i].id+'">';
                                html += '<i class=" fas fa-plus fa-md" aria-hidden="true"></i>';
                                html += '</a>';
                                html += '</span>';
                            }
                        }

                        html += '</div>';
                        html += '<div class="overflow-auto card-body section_custom_height" id="section_custom_height'+form_section[i].id+'"> ';
                        html += '<input type="hidden" name="id" class="form_section" value="'+form_section[i].id+'">';
                        html += '<input type="hidden" id="formsectionheight_'+form_section[i].id+'">';

                        if(form_section[i].questions !==null)
                            if (form_section[i].form_section_column!==null){
                                html += '<div class="row">';
                            }
                            html+=questionSet(form_section[i].questions,form_section[i].shared,form_section[i].width,form_section[i].id,form_section[i].form_section_column);
                            if (form_section[i].form_section_column!==null){
                                html += '</div>';
                            }
                        html += '</div>';
                        if (form_section[i].footer_height!==null){
                            var footer_height='height: '+form_section[i].footer_height+'px;';
                        }else{
                            var footer_height='';
                        }
                        if (form_section[i].show_footer ==1){
                            html += '<div class="card-footer text-muted text-left" id="section_card_footer'+form_section[i].id+'" style="'+footer_height+'">';
                            if ($("#documentDynamicForm_form_footer").val()=='1'){
                                if (form_section[i].footer !==null){
                                    html +='<span class="pull-left">'+form_section[i].footer+'</span>';
                                }else{
                                    html +='<span>&nbsp;&nbsp;&nbsp;</span>';
                                }
                            }else{
                                html +='<span>&nbsp;&nbsp;&nbsp;</span>';
                            }

                            if (form_section[i].signature !==null){
                                // html +='<span class="pull-right signature_font">&nbsp;&nbsp;&nbsp;'+form_section[i].signature+'</span>';
                            }else{
                                html +='<span>&nbsp;&nbsp;&nbsp;</span>';
                            }
                            html += '</div>';
                        }
                        html += '</div>';
                        html += '</div>';
                        html += '</div>';
                    }

                    $('#div_for_dynamic_forms_display').html(html);
                    $('.input_starttime').pickatime({});
                    $('.datepicker').pickadate({selectYears:250,});
                    $('.mdb-select_forms').materialSelect();
                    $('select .mdb-select_forms, .initialized').hide();
                    $('.answertype_richtext').summernote();
                    // $('.note-editable, .card-block').focus();
                    setTimeout(function(){
                        $('[datatoggle="tooltip"]').tooltip();
                    },2000);
                    setFormSectionHeight('drafts');
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }

        function setFormSectionHeight(urltype=null,id=null){
            if (urltype!==null){
                if (urltype=='drafts'){
                    var url= '/document/forms/get';
                }else if(urltype=='active'){
                    var url= '/document/forms/get/active';
                }else if(urltype=='question'){
                    var url= '/document/forms/get/question'
                }else{
                    var url= '/document/forms/get';
                }
            }else{
                var url= '/document/forms/get';
            }
            $.ajax({
                type: 'GET',
                url: url,
                success: function(response)
                {
                    var form_section = response.form_section;
                    var setheight_option='';
                    for(var i = 0; i < form_section.length; i++){
                        if (id !==null){
                            if (id !== form_section[i].id){
                                setheight_option+='<option value="'+form_section[i].id+'">'+form_section[i].name+'</option>';
                            }
                        }else{
                            setheight_option+='<option value="'+form_section[i].id+'">'+form_section[i].name+'</option>';
                        }
                        if (form_section[i].form_sections_height!==null){
                            var active_height =parseFloat(form_section[i].form_sections_height);
                            var height =parseFloat(form_section[i].form_sections_height)+parseFloat(40);
                            $("#section_custom_height"+form_section[i].id).attr('style','height:'+height+'px;overflow:scroll;');
                            $("#question_section_custom_height"+form_section[i].id).attr('style','height:'+height+'px;overflow:scroll;');
                            $("#active_section_custom_height"+form_section[i].id).attr('style','height:'+active_height+'px;overflow:scroll;');
                        }
                    }
                    var setheight='';
                    if (id !==null){
                        var select_id = 'set_height_edit_select';
                        var select_class = 'mdb-select-setheight';
                        setheight+='<select placeholder="Set Height" searchable="Search here.."   class="'+select_class+' set_height_select colorful-select dropdown-primary" style="margin-left: auto !important;width:96%!important;" id="'+select_id+'">';
                        setheight+='<option value="" selected disabled>Set Height</option>';
                        setheight+=setheight_option;
                        setheight+='</select>';
                        console.log('Set Height : '+setheight);
                        $("#set_edit_height").html(setheight);
                        $('.mdb-select-setheight').materialSelect();
                        $('select .mdb-select-setheight, .initialized').hide();
                    }else{
                        var select_id = 'set_height_create_select';
                        var select_class = 'create_mdb-select-setheight';
                        setheight+='<select placeholder="Set Height" searchable="Search here.."   class="'+select_class+' set_height_select colorful-select dropdown-primary"  style="margin-left: auto !important;width:96%!important;" id="'+select_id+'">';
                        setheight+='<option value="" selected disabled>Set Height</option>';
                        setheight+=setheight_option;
                        setheight+='</select>';
                        $("#set_create_height").html(setheight);
                        $('.create_mdb-select-setheight').materialSelect();
                        $('select .create_mdb-select-setheight, .initialized').hide();
                    }
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }

        $("body").on('change','#set_height_edit_select',function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var id=$(this).val();
            $.ajax({
                type : 'POST',
                url : ' /document/forms/edit',
                data:{'id':id},
                success:function(data)
                {
                    if (data.data.header_height !==null){
                        $('#header_height_edit').val(data.data.header_height);
                    }
                    if (data.data.header_height !==null){
                        $('#header_height_create').val(data.data.header_height);
                    }
                    if (data.data.footer_height !==null){
                        $('#footer_height_edit').val(data.data.footer_height);
                    }
                    if (data.data.footer_height !==null){
                        $('#footer_height_create').val(data.data.footer_height);
                    }
                }
            });
            var height = $("#section_custom_height"+$(this).val()).height();
            $("#create_set_formsection_height").val(height);
            $("#set_formsection_height").val(height);
        });

        $("body").on('change','#set_height_create_select',function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var id=$(this).val();
            $.ajax({
                type : 'POST',
                url : ' /document/forms/edit',
                data:{'id':id},
                success:function(data)
                {
                    if (data.data.header_height !==null){
                        $('#header_height_edit').val(data.data.header_height);
                    }
                    if (data.data.header_height !==null){
                        $('#header_height_create').val(data.data.header_height);
                    }
                    if (data.data.footer_height !==null){
                        $('#footer_height_edit').val(data.data.footer_height);
                    }
                    if (data.data.footer_height !==null){
                        $('#footer_height_create').val(data.data.footer_height);
                    }
                }
            });
            var height = $("#section_custom_height"+$(this).val()).height();
            $("#create_set_formsection_height").val(height);
            $("#set_formsection_height").val(height);
        });

        function questionSet(data,flag,form_section_width=null,form_section_id,form_section_column)
        {
            var html='';
            if ($("#view_documentQuestion").val()=='1'){
                for(var j = 0 ; j < data.length; j++){
                    let question = data[j];
                    section_question =form_section_id+'.'+question.id;
                    if (form_section_column !==null){
                        var columnsize=parseFloat(12)/parseFloat(form_section_column);
                        html += '<div class="form_section_columnsize col-md-'+columnsize+'">';
                    }
                    html += '<div class="card card-cascade wider reverse">';
                    if(flag == 0)
                    {
                        html += '<div class="btn-primary text-right" style="padding:10px;padding-right:25px;">';
                        if($("#delete_documentQuestion").val()=='1'){
                            html += '<a datatoggle="tooltip" data-placement="left" title="Delete Question" onclick="delete_sectionquestiondata('+question.id+')" class=" text-center  form_btn_circle"><i class="white-color fas fa-minus fa-sm" ></i></a>';
                        }
                        if($("#edit_documentQuestion").val()=='1'){
                            html += '<a datatoggle="tooltip" data-placement="left" title="Update Question" onclick="getform_sectionquestiondata('+question.id+')" class=" text-center form_btn_circle"><i class="white-color fas fa-pencil-alt fa-sm" ></i></a>';
                        }
                        html += '</div>';
                    }
                    if(form_section_width<=6){
                        var p_style = "margin-left:10px !important;";
                    }else{
                        var p_style ="";
                    }
                    html += '<p class="card-title text-left" style="'+p_style+'">';
                    console.log(question);
                    if($("#documentQuestion_question").val()=='1'){
                        html += '<strong>'+question.question+'</strong>';
                        if(question.mandatory_message){
                            html += '<span> ('+question.mandatory_message+')</span>';
                        }
                    }
                    html += '<input type="hidden" value="'+form_section_id+'.'+question.id+'" name="section_question_id"/>';
                    html += '</p>';
                    if(question.layout == 1)
                    {
                        html += '<div class="card-body hori_layout'+question.layout+'">';
                    }
                    html += '<div class="row text-center">';
                    if($("#documentQuestion_answer").val()=='1'){
                        if(question.answer_type !=null && question.answer_type.name !==null){
                            var answer_type = question.answer_type.name;
                        }else{
                            var answer_type ='';
                        }
                        // var answer_type = question.answer_type.name;
                    }else{
                        var answer_type ='';
                    }
                    switch (answer_type) {
                        case 'dropdown':
                            if ($("#documentQuestion_answer").val()=='1'){
                                html += '<div class="col-md-12" style="padding: 35px;text-align: center">';
                                html += '<select searchable="Search here.." style="width: 100% !important;"  class="mdb-select_forms colorful-select dropdown-primary" name="dropdown.'+section_question+'"  >';
                                if (question.answer_group)
                                {
                                    html +=loopAnswer(question.answer_group.answer,1,section_question);
                                }
                                html +='</select>';
                                html += '</div>';
                            }
                            break;
                        case 'radio':
                            if ($("#documentQuestion_answer").val()=='1'){
                                var icon_size="width:90%;max-width:300px;height:auto;";
                                if (question.column_size !==null){
                                    var num=parseFloat(12)/parseFloat(question.column_size);
                                    var column_size =parseInt(num);
                                }else{
                                    var column_size="";
                                }
                                if(question.layout == 2)
                                {
                                    var layout_type ='col-md-6';
                                    var style ='img-class img_style_verti';
                                    var img_style= '';
                                    if(form_section_width <=3){
                                        var column = 'col-md-12';
                                    }else if(form_section_width <=5){
                                        var column ='col-md-9';
                                    }else if(form_section_width ==6){
                                        var column ='col-md-9';
                                    }else{
                                        var column ='col-md-6';
                                    }
                                    html += '<div class="'+column+' verti">';
                                }
                                else
                                {
                                    var img_style= 'img_style';
                                    var layout_type ='col-md-12';
                                    var style ='';
                                    html += '<div class="col-md-12 hori" style="padding-right: 40px;"><div class="row">';
                                }
                                if (question.answer_group)
                                {
                                    html +=loopAnswer(question.answer_group.answer,2,style,img_style,form_section_width,section_question,layout_type,question.layout,icon_size,column_size);
                                }
                                html += '</div>';
                                if(question.layout == 1)
                                {
                                    html += '</div>';
                                }
                            }
                            break;
                        case 'checkbox':
                            if ($("#documentQuestion_answer").val()=='1'){
                                    //var icon_size="width:"+question.icon_size+"%;height:auto;";
                                var icon_size="width:90%;max-width:300px;height:auto;";
                                if (question.column_size !==null){
                                    var num=parseFloat(12)/parseFloat(question.column_size);
                                    var column_size =parseInt(num);
                                }else{
                                    var column_size="";
                                }
                                if(question.layout == 2)
                                {
                                    var layout_type ='col-md-6';
                                    var style ='img-class img_style';
                                    var img_style= 'img_style_verti_check';

                                    if(form_section_width <=3){
                                        var column = 'col-md-12';
                                    }else if(form_section_width <=5){
                                        var column = 'col-md-9';
                                    }else if(form_section_width ==6){
                                        var column = 'col-md-9';
                                    }else{
                                        var column = 'col-md-6';
                                    }

                                    html += '<div class="'+column+' verti">';

                                } else {
                                    var img_style= 'img_style';
                                    var layout_type ='col-md-12';
                                    html += '<div class="col-md-12 hori" style="padding-right: 40px;"><div class="row">';
                                }
                                if (question.answer_group) {
                                    html +=loopAnswer(question.answer_group.answer,6,style,img_style,form_section_width,section_question,layout_type,question.layout,icon_size,column_size);
                                }
                                // id="answer_type_id"
                                html += '</div>';
                                if(question.layout == 1)
                                {
                                    html += '</div>';
                                }
                            }
                            break;
                        case 'multi':
                            if ($("#documentQuestion_answer").val()=='1'){
                                html += '<div class="col-md-12" style="padding: 35px;text-align: center"><div class="md-form">';
                                html += '<select searchable="Search here.."  class="mdb-select_forms " multiple name="multi.'+section_question+'" >';
                                if (question.answer_group)
                                {
                                    html +=loopAnswer(question.answer_group.answer,1,section_question);
                                }
                                html +='</select>';
                                html += '</div></div>';
                            }
                            break;
                        case 'date':
                            if ($("#documentQuestion_answer").val()=='1'){
                                html += '<div class="col-md-12">';
                                html +=withoutLoopAnswer('',3,section_question);
                                html += '</div>';
                            }
                            break;
                        case 'time':
                            if ($("#documentQuestion_answer").val()=='1'){
                                html += '<div class="col-md-12">';
                                html +=withoutLoopAnswer('',4,section_question);
                                html += '</div>';
                            }
                            break;
                        case 'text':
                            if ($("#documentQuestion_answer").val()=='1'){
                                html += '<div class="col-md-12">';
                                html +=withoutLoopAnswer('',5,section_question);
                                html += '</div>';
                            }
                            break;
                        case 'richtext':
                            if ($("#documentQuestion_answer").val()=='1'){
                                html += '<div class="col-md-12">';
                                html +=withoutLoopAnswer('',9,section_question);
                                html += '</div>';
                            }
                            break;
                        case 'comment':
                            if ($("#documentQuestion_answer").val()=='1'){
                                html += '<div class="col-md-12">';
                                html +=withoutLoopAnswer('',8,section_question,question.comment);
                                html += '</div>';
                            }
                            break;
                        case 'cloc':
                            if ($("#documentQuestion_answer").val()=='1'){
                                html += '<div class="col-md-12">';
                                html +=withoutLoopAnswer('',12,section_question);
                                html += '</div>';
                            }
                            break;
                        default:
                                if ($("#documentQuestion_answer").val()=='1'){
                                html += '<div class="col-md-12">';
                                // if (question.answer_group)
                                // {

                                html +=withoutLoopAnswer('',0,section_question);

                                // }
                                html += '</div>';
                            }
                    }
                    html += '</div>';
                    if(question.layout == 1)
                    {
                        html += '</div>';
                    }
                    html += '</div>';
                    if (form_section_column !==null){
                        html += '</div>';
                    }
                }
            }
            return html;
        }
        function loopAnswer(data,flag,custom_class=null,imgStyle=null,form_section_width=null,section_question,layout_type=null,layout=null,icon_size=null,column_size=null)
        {
            var html='';

            for(var k=0;k<data.length;k++)
            {
                let answer = data[k];
                if(answer.answer)
                {
                    switch(flag) {
                        case 1:
                            html += '<option value="'+answer.id+'">';
                            html += answer.answer;
                            html += '</option>';
                            break;
                        case 2:
                            if (layout==2){
                                if(form_section_width <=2){
                                    var img_style = 'margin-left:6px !important;';
                                    // var column = layout_type;
                                    var img_column = layout_type;
                                    var label_column =layout_type;
                                    var custom_sectionstart = "<div class='row'>";
                                    var custom_sectionend = "</div>";
                                }else{
                                    var img_style = 'margin-left:9px !important;';
                                    if (form_section_width <=8){
                                        var img_column = 'col-md-6';
                                        var label_column = 'col-md-6';
                                    }else{
                                        var img_column = 'col-md-4 ';
                                        var label_column = 'col-md-8';
                                    }
                                    // var column = layout_type;
                                    var custom_sectionstart = "";
                                    var custom_sectionend = "";
                                }
                                if(answer.icon!=null){
                                    var lineheight='line-height:60px;';
                                    if (form_section_width <=8){
                                        var img_column = 'col-md-6';
                                        var label_column = 'col-md-6';
                                    }else{
                                        var img_column = 'col-md-4 ';
                                        var label_column = 'col-md-8';
                                    }
                                    var verticla_abswer_label_radio='verticla_abswer_label_radio';
                                    var img='<img src="{{asset("/uploads/icons")}}/'+answer.icon+'" style="margin-left:10px !important;'+icon_size+'"   class=" '+imgStyle+'">';
                                }else{
                                    var lineheight ='';
                                    if (form_section_width <=6){
                                        var img_column = 'col-md-2';
                                        var label_column = 'col-md-10';
                                    }else{
                                        var img_column = 'col-md-1 ';
                                        var label_column = 'col-md-11';
                                    }
                                    var verticla_abswer_label_radio='';
                                    var img='';
                                }

                                html += custom_sectionstart;
                                html += '<div class="form-check form-check-form_section" style="padding-left:1rem !important;">';
                                html += '<div class="row" style="height: 100%;">';
                                html += '<div class="'+img_column+' text-left" style="'+lineheight+'">';

                                html += '<input type="radio" class="form-check-input" id="'+section_question+'.'+answer.id+'" value="'+answer.id+'" name="answer_id.'+section_question+'">';
                                html += '<label style="text-align: center;max-width: 200px;width:100%;max-height:200px;height:100%;" class="form-check-label abswer_label_radio '+verticla_abswer_label_radio+'" for="'+section_question+'.'+answer.id+'">'+img+'</label>';

                                html += '</div>';
                                html += '<div class="'+label_column+' text-left">';

                                if (form_section_width >4){
                                    html+='<span style="margin-left:10px !important;text-align: left !important;display: block;line-height:60px;">'+answer.answer+'</span>';
                                }else{
                                    html+='<span style="margin-left:25px !important;text-align: left !important;display: block;line-height:60px;">'+answer.answer+'</span>';
                                }

                                html += '</div>';
                                html += '</div>';
                                html += '</div>';
                                html += custom_sectionend;
                            }else{
                                if(form_section_width <=2){
                                    var img_style = 'margin-left:6px !important;';
                                    var column = layout_type;
                                    var custom_sectionstart = "<div class='row'>";
                                    var custom_sectionend = "</div>";
                                }else{
                                    var img_style = 'margin-left:9px !important;';
                                    var column = layout_type;
                                    var custom_sectionstart = "";
                                    var custom_sectionend = "";
                                }
                                html += custom_sectionstart;
                                html += '<div class="col-md-'+column_size+'"><div class="form-check form-check-form_section" style="padding-left:1rem !important;">';
                                html += '<div class="row" style="margin-bottom:40px !important;">';
                                html += '<div class="'+column+' text-left">';

                                html += '<input type="radio" class="form-check-input" id="'+section_question+'.'+answer.id+'" value="'+answer.id+'" name="answer_id.'+section_question+'">';
                                html += '<label class="form-check-label abswer_label_radio" for="'+section_question+'.'+answer.id+'">'+answer.answer+'</label>';

                                html += '</div>';
                                html += '<div class="'+column+' text-left">';
                                if(answer.icon!=null){
                                    if(k==0){
                                        var mystyle = img_style;
                                    }else{
                                        var mystyle ='';
                                    }
                                    html += '<img class="'+custom_class+' '+imgStyle+'" style="vertical-align:baseline;'+icon_size+'" src="{{asset("/uploads/icons")}}/'+answer.icon+'"  width="35" height="35"  class="ml-1 mb-1 rounded">';
                                }else{
                                    html += '<img class="'+custom_class+' '+imgStyle+'" img_style" src="{{asset("/uploads/icons/default/BlankEmailPixels-700px.png")}}" style="vertical-align:baseline;width:10%;height:auto;"  width="35" height="35"  class="ml-1 mb-1 rounded">';
                                }
                                html += '</div>';
                                html += '</div>';
                                html += '</div>';
                                html += '</div>';
                                html += custom_sectionend;
                            }
                            break;
                        case 3:
                            html += ' <div class="md-form">';
                            html+=' <input type="text" id="date-picker-form'+answer.id+'" name="start_date.'+section_question+'" class="form-control datepicker">';
                            html += ' <label for="date-picker-form'+answer.id+'">Time</label>';
                            html += '</div>';
                            break;
                        case 4:
                            html += ' <div class="md-form">';
                            html+=' <input placeholder="Selected time" type="text" id="time-picker-form'+answer.id+'"  name="time.'+section_question+'" class="input_starttime form-control timepicker">';
                            html += ' <label for="time-picker-form'+answer.id+'" >Time</label>';
                            html += '</div>';
                            break;
                        case 6:
                            if(form_section_width <=2){
                                var img_style = 'margin-left:6px !important;';
                                var column = 'col-md-4';
                                var custom_sectionstart = "<div class='row'>";
                                var custom_sectionend = "</div>";
                            }else{
                                var img_style = 'margin-left:9px !important;';
                                var column = 'col';
                                var custom_sectionstart = "";
                                var custom_sectionend = "";
                            }
                            if (form_section_width <=8){
                                var img_column = layout_type;
                                var label_column = layout_type;
                            }else{
                                var img_column = 'col-md-4';
                                var label_column = 'col-md-8';
                            }
                            if (layout==2)
                            {
                                if(answer.icon!=null){
                                    if (form_section_width <=8){
                                        var img_column = 'col-md-6';
                                        var label_column = 'col-md-6';
                                    }else{
                                        var img_column = 'col-md-4 ';
                                        var label_column = 'col-md-8';
                                    }
                                    var abswer_label_checkbox_verticle='abswer_label_checkbox_verticle';
                                    var img='<img src="{{asset("/uploads/icons")}}/'+answer.icon+'" style="margin-left:10px !important;'+icon_size+'"  width="35" height="35"  class="rounded '+imgStyle+'">';
                                }else{
                                    if (form_section_width <=6){
                                        var img_column = 'col-md-2';
                                        var label_column = 'col-md-10';
                                    }else{
                                        var img_column = 'col-md-1 ';
                                        var label_column = 'col-md-11';
                                    }
                                    var abswer_label_checkbox_verticle='';
                                    var img='';
                                }
                                html += custom_sectionstart;
                                html += '<div class="form-check checkbox_style"><div class="row" style="height:100%;"> <div class="'+img_column+' text-left" >';
                                html += '<input type="checkbox" class="form-check-input" id="'+section_question+'.'+answer.id+'" value="'+answer.id+'" name="checkbox.'+section_question+'" value="'+answer.id+'">';
                                html += '<label class="form-check-label abswer_label_checkbox '+abswer_label_checkbox_verticle+'" style="padding-left: 25px !important;height: 100%;" for="'+section_question+'.'+answer.id+'">'+img+'</label></div><div class="'+label_column+' text-left" style="padding-right: 0px !important;">';
                                if (form_section_width >4){
                                    html+='<span style="margin-left:25px !important;text-align:left;display: block;line-height:60px;">'+answer.answer+'</span>';
                                }else{
                                    html+='<span style="margin-left:25px !important;text-align:left;display: block;line-height:60px;">'+answer.answer+'</span>';
                                }
                                html += '</div>';
                                html += '</div>';
                                html += '</div>';
                                html += custom_sectionend;
                            }else{
                                html += custom_sectionstart;
                                html += '<div class="col-md-'+column_size+'" style="margin-bottom:40px !important;"><div class="form-check checkbox_style "><div class="row"> <div class="'+layout_type+' text-left" >';
                                html += '<input type="checkbox" class="form-check-input" id="'+section_question+'.'+answer.id+'" value="'+answer.id+'" name="checkbox.'+section_question+'" value="'+answer.id+'">';
                                html += '<label class="form-check-label abswer_label_checkbox" style="padding-left: 25px !important;" for="'+section_question+'.'+answer.id+'">'+answer.answer+'</label></div><div class="'+layout_type+' text-left " style="padding-right: 0px !important;">';
                                if(answer.icon!=null){
                                    if(k==0){
                                        var mystyle = 'margin-left:12px !important;';
                                    }else{
                                        var mystyle ='';
                                    }
                                    html += '<img src="{{asset("/uploads/icons")}}/'+answer.icon+'" style="'+icon_size+'"   class="rounded '+imgStyle+'">';
                                }else{
                                    html += '<img src="{{asset("/uploads/icons/default/BlankEmailPixels-700px.png")}}" style="width:10%;height:auto;"   class="'+imgStyle+' rounded">';
                                }
                                html += '</div>';
                                html += '</div>';
                                html += '</div>';
                                html += '</div>';
                                html += custom_sectionend;
                            }
                            break;
                    }
                }
                this.answerCount++;
            }
            this.answerGroupCount++;
            return html;
        }
        function withoutLoopAnswer(data=null,flag,section_question,comment='')
        {
            var html='';
            var str = section_question.split('.');
            var st = str[0]+'-'+str[1];
            let answer = data[1];
            switch(flag) {
                case 3:
                    html += ' <div class="md-form" style="padding-left:18px !important;width: 97% !important;">';
                    //html+=' <input type="text" id="date-picker-form'+answer.id+'" name="date" class="form-control datepicker">';
                    html+=' <input type="datetime-local" id="date-picker-form" name="'+section_question+'" class="form-control datepicker">';
                    //html += ' <label for="date-picker-form'+answer.id+'" style="margin-top:-22px !important;margin-left:16px !important;">Date</label>';
                    html += ' <label for="date-picker-form" style="margin-top:-22px !important;margin-left:16px !important;">Date</label>';
                    html += '</div>';
                    break;
                case 4:
                    html += ' <div class="md-form" style="padding-left:18px !important;width: 97% !important;">';
                    // html+=' <input placeholder="Selected time" type="text" id="time-form'+answer.id+'"  name="time" class="input_starttime form-control timepicker">';
                    html+=' <input placeholder="Selected time" type="text" id="time-form"  name="'+section_question+'" class="input_starttime form-control timepicker">';
                    html += ' <label for="time-form" class="active" style="margin-top:-22px !important;margin-left:16px !important;" >Time</label>';
                    //html += ' <label for="time-form'+answer.id+'" style="margin-top:-22px !important;margin-left:16px !important;" >Time</label>';
                    html += '</div>';
                    break;
                case 5:
                    html += ' <div class="md-form" style="padding-left:18px !important;width: 97% !important;">';
                    html+=' <input  type="text" id="input-text"  name="'+section_question+'" class=" form-control ">';
                    // html+=' <input  type="text" id="input-text'+answer.id+'"  name="text" class=" form-control ">';
                    html += ' <label for="input-text" style="margin-top:-22px !important;margin-left:16px !important;">Text</label>';
                    //html += ' <label for="input-text'+answer.id+'" style="margin-top:-22px !important;margin-left:16px !important;">Text</label>';
                    html += '</div>';
                    break;
                case 9:
                    html +='<div class="md-form" style="padding-left:18px !important;width: 97% !important;">';
                    html+='<textarea id="rich-text"  name="'+section_question+'" rows="5" class="'+st+' answertype_richtext "></textarea>';
                    // html += ' <label for="input-text" style="margin-top:-7% !important;margin-left:16px !important;">Rich Text</label>';
                    html += '</div>';
                    break;
                case 8:
                    html +='<div class="md-form ml-3" style="">';
                    html+='<div class="text-left">'+comment+'</div>';
                    html +='</div>';
                    break;
                case 12:
                    html += ' <div class="md-form" style="padding-left:18px !important;width: 97% !important;">';
                    html+=' <input  type="text" id="input-text" class=" form-control ">';

                    html += ' <label for="input-text" style="margin-top:-22px !important;margin-left:16px !important;">Clocs</label>';

                    html += '</div>';
                    break;
                default :
                    html += ' <div class="md-form" style="padding-left:18px !important;width: 97% !important;">';
                    html+=' <img src="{{asset("/img/no_si.jpeg")}}" width="200" height="100">';

                    html += '</div>';

            }
            return html;
        }

        function getAllFormsActive(){
            $.ajax({
                type: 'GET',
                url: '/document/forms/get/active',
                // data: data,
                success: function(response)
                {
                    var form_section = response.form_section;
                    var html = '';
                    for(var i = 0; i < form_section.length; i++){

                        html += ' <div id="'+form_section[i].order+'-'+form_section[i].id+'" class=" sortable-card col-md-'+form_section[i].width+'">';
                        html += '<div class="mb-4  custom_dynamic_height " id="active_form_section_height'+form_section[i].id+'">';
                        html += '<div class="card text-center ">';
                        if (form_section[i].header_height!==null){
                            var header_height='height: '+form_section[i].header_height+'px;';
                        }else{
                            var header_height='';
                        }

                        if (form_section[i].show_header ==1){
                            var header_bg = 'background-color:#4c8bf5 !important;';
                        }else{
                            var header_bg = 'background-color:#00bcd4 !important;';
                        }
                        html += '<div class="card-header" id="section_card_header'+form_section[i].id+'" style="display:flex;'+header_height+header_bg+'">';

                        if ($("#documentDynamicForm_form_header").val()=='1'){
                            if (form_section[i].header!==null){
                                html += '<span class="white-color z-index" style="flex: 1; text-align:left; ">';
                                if (form_section[i].show_header ==1){
                                    html += form_section[i].header;
                                }else{
                                    html += '<span class="white-color" style="flex: 1; text-align:left; ">&nbsp;&nbsp;&nbsp;</span>';
                                }
                                html += '</span>';
                            }else{
                                html += '<span class="white-color" style="flex: 1; text-align:left; ">&nbsp;&nbsp;&nbsp;</span>';
                            }
                        }

                        html += '</div>';
                        html += '<div class="card-body overflow-auto section_custom_height" id="active_section_custom_height'+form_section[i].id+'"> ';
                        if(form_section[i].questions)
                        {
                            //html+=questionSet(form_section[i].questions,1);
                            if (form_section[i].form_section_column!==null){
                                html += '<div class="row">';
                            }
                            html+=questionSet(form_section[i].questions,form_section[i].shared,form_section[i].width,form_section[i].id,form_section[i].form_section_column);
                            if (form_section[i].form_section_column!==null){
                                html += '</div>';
                            }
                        }
                        html += '</div>';
                        if (form_section[i].footer_height!==null){
                            var footer_height='height: '+form_section[i].footer_height+'px;';
                        }else{
                            var footer_height='';
                        }
                        if (form_section[i].show_footer ==1){
                            html += '<div class="card-footer text-muted text-left" id="section_card_footer'+form_section[i].id+'" style="'+footer_height+'">';
                            if ($("#documentDynamicForm_form_footer").val()=='1'){
                                if (form_section[i].footer !==null){
                                    html +='<span class="pull-left">'+form_section[i].footer+'</span>';
                                }else{
                                    html +='<span>&nbsp;&nbsp;&nbsp;</span>';
                                }
                            }else{
                                html +='<span>&nbsp;&nbsp;&nbsp;</span>';
                            }

                            if (form_section[i].signature !==null){
                                // html +='<span class="pull-right signature_font">&nbsp;&nbsp;&nbsp;'+form_section[i].signature+'</span>';
                            }else{
                                html +='<span>&nbsp;&nbsp;&nbsp;</span>';
                            }
                            html += '</div>';
                        }
                        html += '</div>';
                        html += '</div>';
                        html += '</div>';
                    }
                    $('#div_for_dynamic_forms_display_active').html(html);
                    $('.answertype_richtext').summernote();
                    // $('.note-editable, .card-block').focus();
                    setFormSectionHeight('active');
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }

        function getAllFormsQuestion(){
            $.ajax({
                type: 'GET',
                url: '/document/forms/get/question',
                // data: data,
                success: function(response)
                {
                    var form_section = response.form_section;
                    var html = '';
                    for(var i = 0; i < form_section.length; i++){

                        html += ' <div id="'+form_section[i].order+'-'+form_section[i].id+'" class=" sortable-card col-md-'+form_section[i].width+'">';
                        html += '<div class="mb-4  custom_dynamic_height" id="question_form_section_height'+form_section[i].id+'">';
                        html += '<div class="card text-center ">';

                        if (form_section[i].header_height!==null){
                            var header_height='height: '+form_section[i].header_height+'px;';
                        }else{
                            var header_height='';
                        }

                        if (form_section[i].show_header ==1){
                            var header_bg = 'background-color:#4c8bf5 !important;';
                        }else{
                            var header_bg = 'background-color:#00bcd4 !important;';
                        }
                        html += '<div class="card-header" id="section_card_header'+form_section[i].id+'" style="display:flex;'+header_height+header_bg+'">';

                        if ($("#documentDynamicForm_form_header").val()=='1'){
                            html += '<span class="white-color" style="flex: 1; text-align:left; ">';
                            if (form_section[i].header!==null)
                            {
                                if (form_section[i].show_header ==1){
                                    html += form_section[i].header;
                                }else{
                                    html += '<span class="white-color" style="flex: 1; text-align:left; ">&nbsp;&nbsp;&nbsp;</span>';
                                }

                            }
                            if(form_section[i].is_repeatable == 1)
                            {
                                html += '<span class="ml-2 white-color" style="flex: 1; text-align:left; "><i class="fas fa-check" aria-hidden="true"></i></span>';
                            }
                            html += '</span>';
                        }else{
                            html +='<span class="white-color" style="flex: 1; text-align:left; ">&nbsp;&nbsp;&nbsp;</span>';
                        }
                        html += '<span class=" float-right">';

                        if ($("#form_edit_documentDynamicForm").val()=='1'){
                            html += ' <a data-toggle="tooltip" data-placement="left" title="Update Section" onClick="getform_sectiondata('+form_section[i].id+')"  class=" form_btn_circle">';
                            html += '<i class="white-color fas fa-pencil-alt fa-sm" style="margin-left: auto;"></i>';
                            html += '</a> ';
                        }

                        if ($("#form_create_documentDynamicForm").val()=='1'){
                            html += '<a data-toggle="tooltip" data-placement="left" title="Add Question" class="text-success open_popup form_btn_circle" id="'+form_section[i].id+'">';
                            html += '<i class="white-color fas fa-plus fa-md" aria-hidden="true"></i>';
                            html += '</a>';
                        }
                        html += '</span>';

                        html += '</div>';
                        html += '<input type="hidden" name="id" class="form_section" value="'+form_section[i].id+'">';
                        // html += '<div class="card-body"> ';

                        html += '<div class="card-body overflow-auto section_custom_height" id="question_section_custom_height'+form_section[i].id+'"> ';

                        //html+=questionSet(form_section[i].questions,0);

                        if (form_section[i].form_section_column!==null){
                            html += '<div class="row">';
                        }
                        html+=questionSet(form_section[i].questions,0,form_section[i].width,form_section[i].id,form_section[i].form_section_column);
                        if (form_section[i].form_section_column!==null){
                            html += '</div>';
                        }

                        html += '</div>';

                        if (form_section[i].footer_height!==null){
                            var footer_height='height: '+form_section[i].footer_height+'px;';
                        }else{
                            var footer_height='';
                        }
                        if (form_section[i].show_footer ==1){
                            html += '<div class="card-footer text-muted text-left" id="section_card_footer'+form_section[i].id+'" style="'+footer_height+'">';
                            if ($("#documentDynamicForm_form_footer").val()=='1'){
                                if (form_section[i].footer !==null){
                                    html +='<span>'+form_section[i].footer+'</span>';
                                }else{
                                    html +='<span>&nbsp;&nbsp;&nbsp;</span>';
                                }
                            }else{
                                html +='<span>&nbsp;&nbsp;&nbsp;</span>';
                            }

                            if (form_section[i].signature !==null){
                                // html +='<span class="pull-right signature_font">&nbsp;&nbsp;&nbsp;'+form_section[i].signature+'</span>';
                            }else{
                                html +='<span>&nbsp;&nbsp;&nbsp;</span>';
                            }
                            html += '</div>';
                        }
                        html += '</div>';
                        html += '</div>';
                        html += '</div>';
                    }
                    $('#div_for_dynamic_forms_display_question').html(html);
                    $('.answertype_richtext').summernote();
                    // $('.note-editable, .card-block').focus();
                    setFormSectionHeight('question');
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }

        $('.datepicker').pickadate({selectYears:250,});

        $(".card-deck").sortable({
            connectWith: '.card-deck'
        });
        var toolbarOptions = [
            [{
                'header': [1, 2, 3, 4, 5, 6, false]
            }],
            ['bold', 'italic', 'underline', 'strike'], // toggled buttons
            ['blockquote', 'code-block'],

            [{
                'header': 1
            }, {
                'header': 2
            }], // custom button values
            [{
                'list': 'ordered'
            }, {
                'list': 'bullet'
            }],
            [{
                'script': 'sub'
            }, {
                'script': 'super'
            }], // superscript/subscript
            [{
                'indent': '-1'
            }, {
                'indent': '+1'
            }], // outdent/indent
            [{
                'direction': 'rtl'
            }], // text direction

            [{
                'size': ['small', false, 'large', 'huge']
            }], // custom dropdown

            [{
                'color': []
            }, {
                'background': []
            }], // dropdown with defaults from theme
            [{
                'font': []
            }],
            [{
                'align': []
            }],
            ['link', 'image'],

            ['clean'] // remove formatting button
        ];

        var quillFull = new Quill('#document-libery_detail', {
            modules: {
                toolbar: toolbarOptions,
                autoformat: true
            },
            theme: 'snow',
            placeholder: "Write something..."
        });

        function showHideCheked(ele_id){
            if ($("#"+ele_id).is(':checked')){
                $("#"+ele_id).val('1');
            }else{
                $("#"+ele_id).val('0');
            }
        }

        $(document).ready(function(){
            $("body").on('hover','.answertype_richtext',function(){
                $('.note-editable, .card-block').focus();
            });
            $("body").on('click','.note-editable, .card-block',function(){
                $(this).focus();
            });
            $("body").on('click','.answertype_richtext',function(){
                $('.note-editable, .card-block').focus();
            });

            setTimeout(function(){
                setFormSectionHeight();
            },3000);

            $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
                localStorage.setItem('activeTab', $(e.target).attr('href'));
            });

            var activeTab = localStorage.getItem('activeTab');
            if(activeTab){
                $('#myTab a[href="' + activeTab + '"]').tab('show');
                var x = document.getElementById("pop_div");
                console.log(x);
                if(x)
                {
                    if(activeTab=="#profile")
                    {
                       // x.style.display = "flex";
                        console.log("show",activeTab);
                    }
                    else
                    {
                      //  x.style.display = "none";
                        console.log("hide",activeTab);

                    }
                }

            }
            $('a[data-toggle="tab"]').on('click', function(e) {

                if($(e.target).attr('href')){
                    var x = document.getElementById("pop_div");
                    if($(e.target).attr('href')=="#profile")
                    {
                        //x.style.display = "flex";
                        console.log("show",$(e.target).attr('href'));
                    }
                    else
                    {
                       // x.style.display = "none";
                        console.log("hide",$(e.target).attr('href'));

                    }
                }
            });


            $('body').on('click', '#document-forms-copyActive', function() {

                localStorage.setItem('activeTab', $('#profile-tab').attr('href'));

            });

            $('body').on('change', '#pre_populate_id', function() {
                /*$('input').addClass( "custom_clear" );
                  $('#pre_populate_id').removeClass("custom_clear");
                  $(".custom_clear").prop('checked', false);
                  $(".custom_clear").val(' ');*/
                getAllForms();

                var pre_populate_id = $('#pre_populate_id').children("option:selected").val()
                url = '/version/log/getPopulate/'+pre_populate_id;
                setTimeout(function() {
                    $.ajax({
                        type: "GET",
                        url:url,

                        success: function(response)
                        {
                            console.log(response);

                            for (var i = response.length - 1; i >= 0; i--) {


                                if(response[i].answer_id==null)

                                    var  id = response[i].form_section_id+'.'+response[i].question_id;
                                else
                                    var id = response[i].form_section_id+'.'+response[i].question_id+'.'+response[i].answer_id;

                                console.log(id);
                                try
                                {

                                    var type = document.getElementsByName(id)[0].type;
                                }
                                catch
                                {
                                    try
                                    {
                                        var type =  document.getElementById(id).type;
                                    }
                                    catch
                                    {
                                        continue;
                                    }
                                }

                                console.log(type);
                                if(type=='radio' || type=='checkbox')
                                {
                                    try
                                    {
                                        document.getElementsByName(id)[0].checked=true;
                                    }
                                    catch
                                    {
                                        document.getElementById(id).checked = true;

                                    }
                                }
                                else if( type=='select')
                                {
                                    document.getElementById(id).selected = "true";

                                }
                                else if( type=='text') {
                                    id = response[i].form_section_id+'.'+response[i].question_id;
                                    console.log(response[i].answer_text);
                                    document.getElementsByName(id)[0].value=response[i].answer_text;



                                }else if(type=='textarea')
                                {
                                    document.getElementsByName(id)[0].value=response[i].answer_text;
                                    var id = id.split('.');
                                    var nid = '.'+id[0]+'-'+id[1];
                                    console.log(nid);
                                    $(nid).summernote('code',response[i].answer_text);
                                }
                                else
                                {
                                    document.getElementById(id).selected = "true";
                                }


                            }


                        }

                    });
                },800);
            });

            $('body').on('focusout', '#formName', function() {

                var formData = $("#addPopulate").serializeObject();


                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url:'/form/addPopulate',
                    data:formData,
                    success: function(response)
                    {
                        console.log(Object.keys(response.pre_populate).length );

                        var html='';

                        html+='<select class="mdb-select" name="pre_populate_id" id="pre_populate_id" >';
                        for (var key in response.pre_populate) {
                            if (response.pre_populate.hasOwnProperty(key))
                                html+='<option  value="'+key+'">'+response.pre_populate[key]+'</option>';
                        }
                        html+='</select> ';
                        $('#pre_populate_listing').empty();
                        console.log(html, "html");
                        document.getElementById('pre_populate_listing').innerHTML=html;
                        if (!$('#pre_populate_id').materialSelect()){
                            $('#pre_populate_id').materialSelect();
                        }
                        toastr["success"](response.message);
                    }
                });

                // console.log(data);

            });
        });

    </script>
    <script src="{{asset('/custom/js/answers.js')}}"></script>
    <script src="{{asset('/custom/js/populate.js')}}"></script>
@endsection
