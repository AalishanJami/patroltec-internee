<!-----form input permission start--->
@if(Auth()->user()->all_companies == 1)
    <!----Operation permission-->
    <input type="hidden" id="form_view_documentDynamicForm" value="1">
    <input type="hidden" id="form_create_documentDynamicForm" value="1">
    <input type="hidden" id="form_edit_documentDynamicForm" value="1">
    <input type="hidden" id="form_delete_documentDynamicForm" value="1">
    <input type="hidden" id="form_share_documentDynamicForm" value="1">
    <input type="hidden" id="form_activate_documentDynamicForm" value="1">
    <input type="hidden" id="form_duplicate_documentDynamicForm" value="1">

    <!----view column permission-->
    <input type="hidden" id="documentDynamicForm_form_name" value="1">
    <input type="hidden" id="documentDynamicForm_form_header" value="1">
    <input type="hidden" id="documentDynamicForm_form_footer" value="1">
    <input type="hidden" id="documentDynamicForm_form_formwidth" value="1">
    <input type="hidden" id="documentDynamicForm_form_border" value="1">

    <!----edit column permission-->
    <input type="hidden" id="documentDynamicForm_form_name_edit" value="1">
    <input type="hidden" id="documentDynamicForm_form_header_edit" value="1">
    <input type="hidden" id="documentDynamicForm_form_footer_edit" value="1">
    <input type="hidden" id="documentDynamicForm_form_formwidth_edit" value="1">
    <input type="hidden" id="documentDynamicForm_form_border_edit" value="1">

    <!----create column permission-->
    <input type="hidden" id="documentDynamicForm_form_name_create" value="1">
    <input type="hidden" id="documentDynamicForm_form_header_create" value="1">
    <input type="hidden" id="documentDynamicForm_form_footer_create" value="1">
    <input type="hidden" id="documentDynamicForm_form_formwidth_create" value="1">
    <input type="hidden" id="documentDynamicForm_form_border_create" value="1">

@else
    <!----Operation  permission-->
    <input type="hidden" id="form_view_documentDynamicForm" value="{{Auth()->user()->hasPermissionTo('form_view_documentDynamicForm')}}">
    <input type="hidden" id="form_create_documentDynamicForm" value="{{Auth()->user()->hasPermissionTo('form_create_documentDynamicForm')}}">
    <input type="hidden" id="form_edit_documentDynamicForm" value="{{Auth()->user()->hasPermissionTo('form_edit_documentDynamicForm')}}">
    <input type="hidden" id="form_delete_documentDynamicForm" value="{{Auth()->user()->hasPermissionTo('form_delete_documentDynamicForm')}}">
    <input type="hidden" id="form_share_documentDynamicForm" value="{{Auth()->user()->hasPermissionTo('form_share_documentDynamicForm')}}">
    <input type="hidden" id="form_activate_documentDynamicForm" value="{{Auth()->user()->hasPermissionTo('form_activate_documentDynamicForm')}}">
    <input type="hidden" id="form_duplicate_documentDynamicForm" value="{{Auth()->user()->hasPermissionTo('form_duplicate_documentDynamicForm')}}">

    <!----view column permission-->
    <input type="hidden" id="documentDynamicForm_form_name" value="{{Auth()->user()->hasPermissionTo('documentDynamicForm_form_name')}}">
    <input type="hidden" id="documentDynamicForm_form_header" value="{{Auth()->user()->hasPermissionTo('documentDynamicForm_form_header')}}">
    <input type="hidden" id="documentDynamicForm_form_footer" value="{{Auth()->user()->hasPermissionTo('documentDynamicForm_form_footer')}}">
    <input type="hidden" id="documentDynamicForm_form_formwidth" value="{{Auth()->user()->hasPermissionTo('documentDynamicForm_form_formwidth')}}">
    <input type="hidden" id="documentDynamicForm_form_border" value="{{Auth()->user()->hasPermissionTo('documentDynamicForm_form_border')}}">

    <!----edit column permission-->
    <input type="hidden" id="documentDynamicForm_form_name_edit" value="{{Auth()->user()->hasPermissionTo('documentDynamicForm_form_name_edit')}}">
    <input type="hidden" id="documentDynamicForm_form_header" value="{{Auth()->user()->hasPermissionTo('documentDynamicForm_form_header')}}">
    <input type="hidden" id="documentDynamicForm_form_footer" value="{{Auth()->user()->hasPermissionTo('documentDynamicForm_form_footer')}}">
    <input type="hidden" id="documentDynamicForm_form_formwidth" value="{{Auth()->user()->hasPermissionTo('documentDynamicForm_form_formwidth')}}">
    <input type="hidden" id="documentDynamicForm_form_border" value="{{Auth()->user()->hasPermissionTo('documentDynamicForm_form_border')}}">

    <!----create column permission-->
    <input type="hidden" id="documentDynamicForm_form_name_create" value="{{Auth()->user()->hasPermissionTo('documentDynamicForm_form_name_create')}}">
    <input type="hidden" id="documentDynamicForm_form_header_create" value="{{Auth()->user()->hasPermissionTo('documentDynamicForm_form_header_create')}}">
    <input type="hidden" id="documentDynamicForm_form_footer_create" value="{{Auth()->user()->hasPermissionTo('documentDynamicForm_form_footer_create')}}">
    <input type="hidden" id="documentDynamicForm_form_formwidth_create" value="{{Auth()->user()->hasPermissionTo('documentDynamicForm_form_formwidth_create')}}">
    <input type="hidden" id="documentDynamicForm_form_border_create" value="{{Auth()->user()->hasPermissionTo('documentDynamicForm_form_border_create')}}">

@endif
<!-----form input permission end--->


<!-----question input permission start--->
@if(Auth()->user()->all_companies == 1)
    <!----Operation permission-->
    <input type="hidden" id="view_documentQuestion" value="1">
    <input type="hidden" id="create_documentQuestion" value="1">
    <input type="hidden" id="edit_documentQuestion" value="1">
    <input type="hidden" id="delete_documentQuestion" value="1">
    <input type="hidden" id="duplicate_documentQuestion" value="1">


    <!----view column permission-->
    <input type="hidden" id="documentQuestion_question" value="1">
    <input type="hidden" id="documentQuestion_order" value="1">
    <input type="hidden" id="documentQuestion_answertype" value="1">
    <input type="hidden" id="documentQuestion_layout" value="1">
    <input type="hidden" id="documentQuestion_answer" value="1">

    <!----edit column permission-->
    <input type="hidden" id="documentQuestion_question_edit" value="1">
    <input type="hidden" id="documentQuestion_order_edit" value="1">
    <input type="hidden" id="documentQuestion_answertype_edit" value="1">
    <input type="hidden" id="documentQuestion_layout_edit" value="1">
    <input type="hidden" id="documentQuestion_answer_edit" value="1">

    <!----create column permission-->
    <input type="hidden" id="documentQuestion_question_edit" value="1">
    <input type="hidden" id="documentQuestion_order_edit" value="1">
    <input type="hidden" id="documentQuestion_answertype_edit" value="1">
    <input type="hidden" id="documentQuestion_layout_edit" value="1">
    <input type="hidden" id="documentQuestion_answer_edit" value="1">

@else
    <!----Operation  permission-->
    <input type="hidden" id="view_documentQuestion" value="{{Auth()->user()->hasPermissionTo('view_documentQuestion')}}">
    <input type="hidden" id="create_documentQuestion" value="{{Auth()->user()->hasPermissionTo('create_documentQuestion')}}">
    <input type="hidden" id="edit_documentQuestion" value="{{Auth()->user()->hasPermissionTo('edit_documentQuestion')}}">
    <input type="hidden" id="delete_documentQuestion" value="{{Auth()->user()->hasPermissionTo('delete_documentQuestion')}}">
    <input type="hidden" id="duplicate_documentQuestion" value="{{Auth()->user()->hasPermissionTo('duplicate_documentQuestion')}}">

    <!----view column permission-->
    <input type="hidden" id="documentQuestion_question" value="{{Auth()->user()->hasPermissionTo('documentQuestion_question')}}">
    <input type="hidden" id="documentQuestion_order" value="{{Auth()->user()->hasPermissionTo('documentQuestion_order')}}">
    <input type="hidden" id="documentQuestion_answertype" value="{{Auth()->user()->hasPermissionTo('documentQuestion_answertype')}}">
    <input type="hidden" id="documentQuestion_layout" value="{{Auth()->user()->hasPermissionTo('documentQuestion_layout')}}">
    <input type="hidden" id="documentQuestion_answer" value="{{Auth()->user()->hasPermissionTo('documentQuestion_answer')}}">

    <!----edit column permission-->
    <input type="hidden" id="documentQuestion_question_edit" value="{{Auth()->user()->hasPermissionTo('documentQuestion_question_edit')}}">
    <input type="hidden" id="documentQuestion_order_edit" value="{{Auth()->user()->hasPermissionTo('documentQuestion_order_edit')}}">
    <input type="hidden" id="documentQuestion_answertype_edit" value="{{Auth()->user()->hasPermissionTo('documentQuestion_answertype_edit')}}">
    <input type="hidden" id="documentQuestion_layout_edit" value="{{Auth()->user()->hasPermissionTo('documentQuestion_layout_edit')}}">
    <input type="hidden" id="documentQuestion_answer_edit" value="{{Auth()->user()->hasPermissionTo('documentQuestion_answer_edit')}}">

    <!----create column permission-->
    <input type="hidden" id="documentQuestion_question_edit" value="{{Auth()->user()->hasPermissionTo('documentQuestion_question_edit')}}">
    <input type="hidden" id="documentQuestion_order_edit" value="{{Auth()->user()->hasPermissionTo('documentQuestion_order_edit')}}">
    <input type="hidden" id="documentQuestion_answertype_edit" value="{{Auth()->user()->hasPermissionTo('documentQuestion_answertype_edit')}}">
    <input type="hidden" id="documentQuestion_layout_edit" value="{{Auth()->user()->hasPermissionTo('documentQuestion_layout_edit')}}">
    <input type="hidden" id="documentQuestion_answer_edit" value="{{Auth()->user()->hasPermissionTo('documentQuestion_answer_edit')}}">

@endif
<!-----question input permission end--->

