@include('backend.layouts.pdf_start')
    <thead>
        <tr>
            @if(Auth()->user()->hasPermissionTo('form_type_name_pdf_export') || Auth::user()->all_companies == 1 )
               <th style="vertical-align: text-top;" class="pdf_table_layout">Name</th>
            @endif
            @if(Auth()->user()->hasPermissionTo('form_type_show_dashboard_pdf_export') || Auth::user()->all_companies == 1 )
               <th style="vertical-align: text-top;" class="pdf_table_layout"> Show Dashboard</th>
            @endif
            @if(Auth()->user()->hasPermissionTo('form_type_count_dashboard_pdf_export') || Auth::user()->all_companies == 1 )
               <th style="vertical-align: text-top;" class="pdf_table_layout"> Count Dashboard</th>
            @endif
            @if(Auth()->user()->hasPermissionTo('form_type_count_day_pdf_export') || Auth::user()->all_companies == 1 )
               <th style="vertical-align: text-top;" class="pdf_table_layout"> Count Day</th>
            @endif
            @if(Auth()->user()->hasPermissionTo('form_type_count_response_positive_pdf_export') || Auth::user()->all_companies == 1 )
               <th style="vertical-align: text-top;" class="pdf_table_layout"> Count Response Positive</th>
            @endif
            @if(Auth()->user()->hasPermissionTo('form_type_count_response_negative_pdf_export') || Auth::user()->all_companies == 1 )
               <th style="vertical-align: text-top;" class="pdf_table_layout"> Count Response Negative</th>
            @endif
            @if(Auth()->user()->hasPermissionTo('form_type_outanding_count_pdf_export') || Auth::user()->all_companies == 1 )
               <th style="vertical-align: text-top;" class="pdf_table_layout"> Outanding Count</th>
            @endif
            @if(Auth()->user()->hasPermissionTo('form_type_late_completion_count_pdf_export') || Auth::user()->all_companies == 1 )
               <th style="vertical-align: text-top;" class="pdf_table_layout"> Late Completion Count</th>
            @endif
            @if(Auth()->user()->hasPermissionTo('form_type_detailed_field_stats_pdf_export') || Auth::user()->all_companies == 1 )
               <th style="vertical-align: text-top;" class="pdf_table_layout"> Detailed Field Stats</th>
            @endif
        </tr>
    </thead>
    <tbody>
    @foreach($data as $row)
        <tr>
            @if(Auth()->user()->hasPermissionTo('form_type_name_pdf_export') || Auth::user()->all_companies == 1 )
               <td class="pdf_table_layout"><p class="col_text_style">{{$row->name}}</p></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('form_type_show_dashboard_pdf_export') || Auth::user()->all_companies == 1 )
                <td class="pdf_table_layout"><p class="col_text_style">{{$row->_show_dashboard}}</p></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('form_type_count_dashboard_pdf_export') || Auth::user()->all_companies == 1 )
               <td class="pdf_table_layout"><p class="col_text_style">{{$row->count_dashboard}}</p></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('form_type_count_day_excel_export') || Auth::user()->all_companies == 1 )
                 <td class="pdf_table_layout"><p class="col_text_style">{{$row->count_day}}</p></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('form_type_count_response_positive_pdf_export') || Auth::user()->all_companies == 1 )
                 <td class="pdf_table_layout"><p class="col_text_style">{{$row->count_response_positive}}</p></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('form_type_count_response_negative_pdf_export') || Auth::user()->all_companies == 1 )
                <td class="pdf_table_layout"><p class="col_text_style">{{$row->count_response_negative}}</p></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('form_type_outanding_count_pdf_export') || Auth::user()->all_companies == 1 )
                  <td class="pdf_table_layout"><p class="col_text_style">{{$row->outanding_count}}</p></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('form_type_late_completion_count_pdf_export') || Auth::user()->all_companies == 1 )
                <td class="pdf_table_layout"><p class="col_text_style">{{$row->late_completion_count}}</p></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('form_type_detailed_field_stats_pdf_export') || Auth::user()->all_companies == 1 )
                <td class="pdf_table_layout"><p class="col_text_style">{{$row->detailed_field_stats}}</p></td>
            @endif 
        </tr>
    @endforeach
    </tbody>
@include('backend.layouts.pdf_end')
