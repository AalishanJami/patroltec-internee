<table>
    <thead>
        <tr>
            @if(Auth()->user()->hasPermissionTo('form_type_name_excel_export') || Auth::user()->all_companies == 1 )
                <th>Name</th>
            @endif
            @if(Auth()->user()->hasPermissionTo('form_type_show_dashboard_excel_export') || Auth::user()->all_companies == 1 )
                <th> Show Dashboard</th>
            @endif
            @if(Auth()->user()->hasPermissionTo('form_type_count_dashboard_excel_export') || Auth::user()->all_companies == 1 )
                <th> Count Dashboard</th>
            @endif
            @if(Auth()->user()->hasPermissionTo('form_type_count_day_excel_export') || Auth::user()->all_companies == 1 )
                <th> Count Day</th>
            @endif
            @if(Auth()->user()->hasPermissionTo('form_type_count_response_positive_excel_export') || Auth::user()->all_companies == 1 )
                <th> Count Response Positive</th>
            @endif
            @if(Auth()->user()->hasPermissionTo('form_type_count_response_negative_excel_export') || Auth::user()->all_companies == 1 )
                <th> Count Response Negative</th>
            @endif
            @if(Auth()->user()->hasPermissionTo('form_type_outanding_count_excel_export') || Auth::user()->all_companies == 1 )
                <th> Outanding Count</th>
            @endif
            @if(Auth()->user()->hasPermissionTo('form_type_late_completion_count_excel_export') || Auth::user()->all_companies == 1 )
                <th> Late Completion Count</th>
            @endif
            @if(Auth()->user()->hasPermissionTo('form_type_detailed_field_stats_excel_export') || Auth::user()->all_companies == 1 )
                <th> Detailed Field Stats</th>
            @endif
        </tr>
    </thead>
    <tbody>
    @foreach($data as $row)
        <tr>
            @if(Auth()->user()->hasPermissionTo('form_type_name_excel_export') || Auth::user()->all_companies == 1 )
               <td>{{$row->name}}</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('form_type_show_dashboard_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{$row->_show_dashboard}}</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('form_type_count_dashboard_excel_export') || Auth::user()->all_companies == 1 )
               <td>{{$row->count_dashboard}}</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('form_type_count_day_excel_export') || Auth::user()->all_companies == 1 )
                 <td>{{$row->count_day}}</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('form_type_count_response_positive_excel_export') || Auth::user()->all_companies == 1 )
                 <td>{{$row->count_response_positive}}</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('form_type_count_response_negative_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{$row->count_response_negative}}</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('form_type_outanding_count_excel_export') || Auth::user()->all_companies == 1 )
                  <td>{{$row->outanding_count}}</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('form_type_late_completion_count_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{$row->late_completion_count}}</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('form_type_detailed_field_stats_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{$row->detailed_field_stats}}</td>
            @endif 
        </tr>
    @endforeach
    </tbody>
</table>
