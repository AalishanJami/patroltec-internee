@extends('backend.layouts.doc')
@section('content')
    @php
       $data=localization();
    @endphp
        <style type="text/css">
           [type=checkbox]:checked, [type=checkbox]:not(:checked),[type=radio]:checked, [type=radio]:not(:checked) {
            position: unset;
            pointer-events: unset;
             opacity: unset;
        }
        </style>

    @include('backend.layouts.doc_sidebar')
      <div class="padding-left-custom remove-padding">
        {{ Breadcrumbs::render('document/detail') }} 
        <div class="locations-form container-fluid form_body">
           <form>
              <input type="hidden" name="_csrf-backend" value="o3P9KIBFoQ0d-4_FrL8468LQikg2gb613QiHzsBGuVtyUJ3ZAoEBV_tyUYNK48rkPsGGeTY4XBPmNdHB19OcsQ==">
              <div id="w1">
              </div>
              <div class="row">
                 <div class="col-lg-11">
                    <div class="row">
                       <!-- Left Block -->
                       <div class="col-lg-6">
                          <div class="col-lg-12 edit_sub_header h4 mb-4">General</div>
                          <div class="col-lg-12">
                             <div class="form-group field-locations-name required">
                                <label class="control-label" for="locations-name"> Name</label>
                                <input type="text"  class="form-control mb-4" name="Locations[name]" value="Lower Thames Crossing" maxlength="50" aria-required="true">
                                <div class="help-block"></div>
                             </div>
                          </div>
                          <div class="col-lg-6">
                             <div class="form-group field-locations-ref">
                                <label class="control-label" for="locations-ref">Ref</label>
                                <input type="text" id="locations-ref" value="HSM.Form.33 " class="form-control mb-4" name="Locations[ref]" value="">
                                <div class="help-block"></div>
                             </div>
                          </div>
                          <div class="col-lg-6">
                             <div class="form-group field-locations-ref">
                                <label class="control-label" for="locations-ref">Bulk Upload</label>
                                <input type="file" id="locations-ref" class="form-control mb-4" name="Locations[ref]" value="">
                                <div class="help-block"></div>
                             </div>
                          </div>
                          <div class="col-lg-12 edit_sub_header card">
                            <label class="control-label" for="locations-ref">Form settings</label>
                            <div class="row">
                               <div class="col-lg-6 edit_grid_data">
                                  <div class="form_check_field">
                                      <input type="checkbox" name="">
                                      <label class="field_title cbx-label" for="locations-keyholder">Delivery Form</label>
                                  </div>
                               </div>
                               <div class="col-lg-6 edit_grid_data">
                                  <div class="form_check_field">
                                     <input type="checkbox" name="">
                                     <label class="field_title cbx-label" for="locations-compliance">Actions Required</label>
                                  </div>
                               </div>
                               <div class="col-lg-6 edit_grid_data">
                                  <div class="form_check_field">
                                     <input type="checkbox" checked name="">
                                     <label class="field_title cbx-label" for="locations-performance_reporting">Incident</label>
                                  </div>
                               </div>
                               <div class="col-lg-6 edit_grid_data">
                                  <div class="form_check_field">
                                     <input type="checkbox" name="">
                                     <label class="field_title cbx-label" for="locations-showform">Show Form on App</label>
                                  </div>
                               </div>
                               <div class="col-lg-6 edit_grid_data">
                                  <div class="form_check_field">
                                     <input type="checkbox" name="">
                                     <label class="field_title cbx-label" checked for="locations-preimportform">Bulk Generate</label>
                                  </div>
                               </div>
                            </div>
                          </div>                 
                          
                          <div class="col-lg-12">
                            <label class="control-label" for="locations-ref">Notes</label>
                            <div id="document-libery_detail" class="ql-scroll-y" style="height: 300px;">
                                
                            </div>
                              
                          </div> 
                          
                       </div>
                       <!-- Right Block -->
                       <div class="col-lg-6">
                          <div class="col-lg-12">
                             <div class="form-group field-locations-office required">
                                <label class="control-label" for="locations-office">Form Type</label>
                                <select searchable="Search here.."  id="locations-office" class="form-control mb-4 select2-hidden-accessible" name="Locations[office]" aria-required="true" data-s2-options="s2options_d6851687" data-krajee-select2="select2_6a081501"  tabindex="-1" aria-hidden="true">
                                   <option value="">Please Select ...</option>
                                   <option value="1" selected="">Logistics</option>
                                   <option value="2">Scaffolding</option>
                                </select>
                                
                                <div class="help-block"></div>
                             </div>
                          </div>
                           <div class="col-lg-12">
                             <div class="form-group field-locations-office required">
                                <label class="control-label" for="locations-office">Location Type</label>
                                
                                <select searchable="Search here.."  id="locations-office" class="form-control mb-4 select2-hidden-accessible" name="Locations[office]" aria-required="true" data-s2-options="s2options_d6851687" data-krajee-select2="select2_6a081501"  tabindex="-1" aria-hidden="true">
                                   <option value="">Please Select ...</option>
                                   <option value="1" selected="">Logistics</option>
                                   <option value="2">Scaffolding</option>
                                </select>
                                
                                <div class="help-block"></div>
                             </div>
                          </div>
                          <div class="col-lg-12 edit_sub_header card">
                            <label class="control-label" for="locations-ref">Download</label>
                            <div class="row">
                               <div class="col-lg-12 edit_grid_data">
                                  <div class="form_check_field">
                                      <input type="checkbox" name="">
                                      <label class="field_title cbx-label" for="locations-keyholder">View only</label>
                                  </div>
                               </div>
                               <div class="col-lg-12 edit_grid_data">
                                  <div class="form_check_field">
                                     <input type="checkbox" checked name="">
                                     <label class="field_title cbx-label"  for="locations-compliance">View and download original file format</label>
                                  </div>
                               </div>
                               <div class="col-lg-12 edit_grid_data">
                                  <div class="form_check_field">
                                     <input type="checkbox" name="">
                                     <label class="field_title cbx-label" for="locations-performance_reporting">View and download PDF</label>
                                  </div>
                               </div>
                               <div class="col-lg-12 edit_grid_data">
                                  <div class="form_check_field">
                                     <input type="checkbox" name="">
                                     <label class="field_title cbx-label" checked for="locations-showform">View ,download PDF and original file format</label>
                                  </div>
                               </div>
                             
                            </div>
                          </div>
                          <div class="col-lg-12 row">   
                            <div class="col-lg-6 ">
                               <div class="col-sm-12 form_date_field">
                                  <label class="control-label form_date_field_lable"><label for="locations-contract_start">Start Date</label></label>
                                  <div id="locations-contract_start-kvdate" class="input-group date">
                                    <input type="date" name=""  class="form-control mb-4">
                                
                                  </div>
                               </div>
                            </div>
                             <div class="col-lg-6">
                               <div class="col-sm-12 form_date_field">
                                  <label class="control-label form_date_field_lable"><label for="locations-contract_start">End Date</label></label>
                                  <div id="locations-contract_start-kvdate" class="input-group date">
                                    <input type="date"  name=""  class="form-control mb-4">
                                 
                                  </div>
                               </div>
                            </div>
                          </div>
                          
                           
              
                            
                       </div>
                    </div>
                 </div>
              </div>
           </form>
        </div>
      </div>
 







<script src="{{asset('editor/sprite.svg.js')}}"></script>
<script type="text/javascript">
   var toolbarOptions = [
      [{
        'header': [1, 2, 3, 4, 5, 6, false]
      }],
      ['bold', 'italic', 'underline', 'strike'], // toggled buttons
      ['blockquote', 'code-block'],

      [{
        'header': 1
      }, {
        'header': 2
      }], // custom button values
      [{
        'list': 'ordered'
      }, {
        'list': 'bullet'
      }],
      [{
        'script': 'sub'
      }, {
        'script': 'super'
      }], // superscript/subscript
      [{
        'indent': '-1'
      }, {
        'indent': '+1'
      }], // outdent/indent
      [{
        'direction': 'rtl'
      }], // text direction

      [{
        'size': ['small', false, 'large', 'huge']
      }], // custom dropdown

      [{
        'color': []
      }, {
        'background': []
      }], // dropdown with defaults from theme
      [{
        'font': []
      }],
      [{
        'align': []
      }],
      ['link', 'image'],

      ['clean'] // remove formatting button
    ];

    var quillFull = new Quill('#document-libery_detail', {
      modules: {
        toolbar: toolbarOptions,
        autoformat: true
      },
      theme: 'snow',
      placeholder: "Write something..."
    });

    
</script>

  

@endsection