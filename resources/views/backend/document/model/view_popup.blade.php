@php
    $data=localization();
@endphp

<div class="modal fade" id="modelviewpopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Document </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <input type="hidden" name="break" id="break_location" value="m">
            <form id="raiseJob" method="POST" action="{{url('document/raiseJob')}}" onsubmit = "return(validate());">
                @csrf
                <input type="hidden" name="form_id" id="raise_job_form_id">

                <div class="modal-body mx-3">
                    <div class="md-form mb-5">
                        <label class="mdb-main-label active" >
                            <span class="assign_class label{{getKeyid('project_name',$data)}}" data-id="{{getKeyid('project_name',$data) }}" data-value="{{checkKey('project_name',$data) }}">
                                {!! checkKey('project_name',$data) !!}
                            </span>
                        </label>
                        <select name="location_id"  id="location_id" searchable="search here" class="mdb-select job_location_id">
                            <option selected disabled value="">Please Select</option>
                            @foreach($locations as $key=>$value)
                                <option value="{{$key}}">{{$value}}</option>
                            @endforeach
                        </select>
{{--                        {!! Form::select('location_id',$locations,0, ['class' =>'mdb-select job_location_id','id'=>'location_id','selected' => true,'searchable'=>'search here','placeholder'=>'Please select']) !!}--}}
                        <small id="er_project_name" style="left: 0%!important;margin-top:-15px;display:block; text-transform: initial !important;" class="text-danger error_message"></small>
                    </div>
                    <div class="md-form mb-5">
                        <div class="md-form" id="qr_listing"></div>
                    </div>
                    <div class="md-form mb-5">
                        <label class="mdb-main-label active" >
                            <span class="assign_class label{{getKeyid('assign_to',$data)}}" data-id="{{getKeyid('assign_to',$data) }}" data-value="{{checkKey('assign_to',$data) }}" >
                                {!! checkKey('assign_to',$data) !!}
                            </span>
                        </label>
                        <select name="user_id[]"  id="user_id" searchable="search here" multiple class="mdb-select colorful-select dropdown-primary md-form job_user_id">
                            <option selected disabled value="">Please Select</option>
                            @foreach($users as $key=>$value)
                                <option value="{{$key}}">{{$value}}</option>
                            @endforeach
                        </select>
{{--                        {!! Form::select('user_id[]',$users,0, ['class' =>' mdb-select colorful-select dropdown-primary md-form job_user_id','id'=>'user_id','searchable'=>'search here','placeholder'=>'Please select','multiple']) !!}--}}
                        <small id="er_user_id" style="left: 0%!important;margin-top:-15px;display:block; text-transform: initial !important;" class="text-danger error_message"></small>
                    </div>
                    <div class="md-form mb-5">
                        <label class="mdb-main-label active" >
                            <span class="assign_class label{{getKeyid('pre_populate',$data)}}" data-id="{{getKeyid('pre_populate',$data) }}" data-value="{{checkKey('pre_populate',$data) }}" >
                                {!! checkKey('pre_populate',$data) !!}
                            </span>
                        </label>
                        <div class="md-form" id="populate_div"></div>
                    </div>
                    <div class="md-form">
                        <label class="mdb-main-label active" >
                            <span class="assign_class label{{getKeyid('date',$data)}}" data-id="{{getKeyid('date',$data) }}" data-value="{{checkKey('date',$data) }}" >
                                {!! checkKey('date',$data) !!}
                            </span>
                        </label>
                        <input placeholder="Select date" type="text" value="{{$date}}" id="date_pick" name="date" class="form-control datepicker">
                        <small id="er_date" class="text-danger error_message"></small>
                    </div>
                    <div class="md-form">
                        <label class="mdb-main-label active" >
                            <span class="assign_class label{{getKeyid('time',$data)}}" data-id="{{getKeyid('time',$data) }}" data-value="{{checkKey('time',$data) }}" >
                                {!! checkKey('time',$data) !!}
                            </span>
                        </label>
                        <input placeholder="Selected time" value="{{$time}}" type="text" id="input_starttime" name="time" class="form-control timepicker">
                        <small id="er_time" class="text-danger error_message"></small>

                    </div>

                <!-- <div class="md-form">
                      <input placeholder="Signature Text" type="number" id="signature_text" name="signature_required_app_text_field" class="form-control">
                    </div>
                    <div class="md-form">
                      <input placeholder="Signature User" type="number" id="signature_user" name="signature_required_app_user" class="form-control">
                    </div>
                     <div class="md-form">
                     {!! Form::select('signature_required_app_select_menu',$answer_groups->pluck('name','id'),null, ['class' =>' mdb-select colorful-select dropdown-primary md-form','id'=>'sig_select','searchable'=>'search here','placeholder'=>'Sig Select']) !!}
                    </div> -->

                </div>


                <div class="modal-footer d-flex justify-content-center">
                    <a href="{{url('job')}}">
                        <button class="btn btn-primary btn-sm" type="submit"><span class="assign_class label{{getKeyid('save',$data)}}" data-id="{{getKeyid('save',$data) }}" data-value="{{checkKey('save',$data) }}" >{!! checkKey('save',$data) !!} </span></button> </a>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="{{asset('/custom/js/raise.js')}}"></script>

<script type="text/javascript">
    $('.datepicker').pickadate({selectYears:250,});
    $('#input_starttime').pickatime();
    function validate()
    {
        var location_id=$('#location_id').children("option:selected").val();
        var user_id=$('#user_id').val();
        var break_location=$('#break_location').val();
        var location_break_id=$('#location_breakdown_id').val();
        var date = $('#date_pick').val();
        var time = $('#input_starttime').val();
        var fl = true;
        console.log(date,time,location_id);
        if(location_id=='' || location_id==null )
        {
            console.log(location_id,"location");
            $("[data-activates=select-options-location_id]").attr('style','border-bottom:1px solid #e3342f');
            $("#project_name").focus();
            $("#er_project_name").html('Field required');
            fl= false;
        }
        else
        {
            $("[data-activates=select-options-location_id]").attr('style','border-bottom:1px solid #ced4da');
            $("#er_project_name").html('');
        }

        if(user_id=='' || user_id==null )
        {
            console.log(user_id,"user");

            $("[data-activates=select-options-user_id]").attr('style','border-bottom:1px solid #e3342f');
            $("#user_id").focus();
            $("#er_user_id").html('Field required');
            fl= false;
        }
        else
        {
            $("[data-activates=select-options-user_id]").attr('style','border-bottom:1px solid #ced4da');
            $("#er_user_id").html('');
        }
        if(date=='' || date==null )
        {
            console.log(date,"date");

            $("#date_pick").attr('style','border-bottom:1px solid #e3342f');
            $("#date_pick").focus();
            $("#er_date").html('Field required');
            fl= false;
        }
        else
        {
            $("#date_pick").attr('style','border-bottom:1px solid #ced4da');
            $("#er_date").html('');
        }
        if(time=='' || time==null )
        {
            console.log(time,"time");

            $("#input_starttime").attr('style','border-bottom:1px solid #e3342f');
            $("#input_starttime").focus();
            $("#er_time").html('Field required');
            fl= false;
        }
        else
        {
            $("#input_starttime").attr('style','border-bottom:1px solid #ced4da');
            $("#er_time").html('');
        }

        // console.log(break_location);
        if(break_location!="m" )
        {
            console.log(break_location);
            if(location_break_id=='' || location_break_id==null )
            {
                console.log(location_break_id,"break");
                $("[data-activates=select-options-location_breakdown_id]").attr('style','border-bottom:1px solid #e3342f');
                $("#er_location_breakdown_id").html('Field required');
                fl= false;
            }
            else
            {
                $("[data-activates=select-options-location_breakdown_id]").attr('style','border-bottom:1px solid #ced4da');
                $("#er_location_breakdown_id").html('');
            }
        }
        console.log(user_id,"true");
        return fl;
    }
</script>
