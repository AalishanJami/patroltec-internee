@php
    $data=localization();
@endphp

<div class="modal fade" id="modelAddDynamicFormEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog model_width" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">
                    <span class="assign_class label{{getKeyid('questions',$data)}}" data-id="{{getKeyid('questions',$data) }}" data-value="{{checkKey('questions',$data) }}" >
                        {!! checkKey('questions',$data) !!}
                    </span>
                    @if(Auth()->user()->hasPermissionTo('view_cms') || Auth::user()->all_companies == 1 )
                        <button data-class="edit-disable" class="edit_cms_enable edit_cms_switch btn btn-primary btn-sm waves-effect waves-light" style="display: none;" >
                            Edit cms
                        </button>
                        <button  data-class="edit-enable" class="edit_cms_disable edit_cms_switch btn btn-danger btn-sm waves-effect waves-light">
                            Edit cms
                        </button>
                    @endif
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="question_Form_edit">
                <input type="hidden" name="id" id="question_id">
                <div class="modal-body mx-3">
                    @if(Auth()->user()->hasPermissionTo('documentQuestion_question_edit') || Auth::user()->all_companies == 1 )

                            <div class="md-form mb-5">
                                <i class="fa fa-edit prefix grey-text"></i>
                                <label  for="name" >
                                    <span class="assign_class question_name label{{getKeyid('questions',$data)}}" data-id="{{getKeyid('questions',$data) }}" data-value="{{checkKey('questions',$data) }}" >
                                        {!! checkKey('questions',$data) !!}
                                    </span>
                                </label>
                            </div>
                            <!-- <textarea class="md-textarea form-control" name="question_name"  id="question_name"></textarea> -->
                            <small class="text-danger form_name_required_message" style="left: 3.5% !important;"></small>
                            <div class="md-form mt-10 question_name_edit question_name question_name_text_fielt">
                                <div class="summernote_inner body_notes" id="question_name_edit" ></div>
                                <small class="text-danger error_message_text form_name_required_message" id="question_name_edit_msg" style="left: 0.5%;top:101% !important;"></small>
                            </div>

                             <div class="md-form mb-5">
                                <i class="fa fa-edit prefix grey-text"></i>
                                <label  for="name" >
                                    <span class="assign_class question_mandatory label{{getKeyid('mandatory_message',$data)}}" data-id="{{getKeyid('mandatory_message',$data) }}" data-value="{{checkKey('mandatory_message',$data) }}" >
                                        {!! checkKey('mandatory_message',$data) !!}
                                    </span>
                                </label>
                            </div>
                            <!-- <textarea class="md-textarea form-control" name="question_mandatory"  id="question_mandatory"></textarea> -->
                            <small class="text-danger form_name_required_message" style="left: 3.5% !important;"></small>
                            <div class="md-form mt-10 question_mandatory_edit question_mandatory question_mandatory_text_fielt">
                                <div class="summernote_inner body_notes" id="question_mandatory_edit" ></div>
                                <small class="text-danger form_name_required_message" id="question_mandatory_edit_msg" style="left: 0.5%;top:101% !important;"></small>
                            </div>

                       <!--  <div class="md-form mb-5">
                            <i class="fa fa-edit prefix grey-text"></i>
                            <span for="question_name_edit" data-error="wrong" data-success="right" class="active ml-4-5" >
                                <span class="assign_class label{{getKeyid('questions',$data)}}" data-id="{{getKeyid('questions',$data) }}" data-value="{{checkKey('questions',$data) }}" >
                                    {!! checkKey('questions',$data) !!}
                                </span>
                            </span>
                            <textarea  class="md-textarea form-control" name="question_name" id="question_name_edit"></textarea>
                        <input type="text" class="form-control " id="question_name_edit" name="question_name" value="" required autocomplete="name" autofocus>
                             <textarea></textarea>
                        </div> -->
                    @endif
                    @if(Auth()->user()->hasPermissionTo('documentQuestion_order_edit') || Auth::user()->all_companies == 1 )

                        <div class="md-form form_type_text_field mb-5">
                            <i class="fa fa-sort prefix icon_top_align grey-text fa-2x" ></i>
                            {{--                            <label data-error="wrong"  class="active" for="name" >--}}
                            <span class="question_span question_order assign_class label{{getKeyid('order',$data)}}" data-id="{{getKeyid('order',$data) }}" data-value="{{checkKey('order',$data) }}" >
                                    {!! checkKey('order',$data) !!}
                                </span>
                            {{--                            </label>--}}
                            <input type="number"  class="form-control form_type_text_field question_number_edit question_order_value" name="question_number" id="question_number_edit" max="12" min="1" required autocomplete="name" autofocus>
                            <small class="text-danger error_message_text form_name_required_message" id="numberBox-edit-msg" style="left: 5.5% !important;"></small>
                        </div>
                    @endif
{{--                    <div class="md-form mb-5">--}}
{{--                        <div class="md-form">--}}
{{--                            <i class="fa fa-sort prefix grey-text fa-2x" ></i>--}}
{{--                            <span class="ml-5">--}}
{{--                                Icon Size--}}
{{--                            </span>--}}
{{--                            <div class="md-form  ml-4-5 mt-1 icon_size_edit">--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}

                    @if(Auth()->user()->hasPermissionTo('documentQuestion_answertype_edit') || Auth::user()->all_companies == 1 )
                        <div class="md-form form_type_text_field mb-5">
                            <div class="md-form form_type_text_field">
                                <i class="fa fa-reply prefix icon_top_align grey-text"></i>
                                <span class="ml-5">
                                   <span class="question_span assign_class margin_lebel label{{getKeyid('answer_type',$data)}}" data-id="{{getKeyid('answer_type',$data) }}" data-value="{{checkKey('answer_type',$data) }}" >
                                        {!! checkKey('answer_type',$data) !!}
                                    </span>
                                </span>
                                <div class="md-form form_type_text_field ml-4-5 answer_type_id_edit">
                                    <select searchable="Search here.."  id="answer_type_id_edit"  class="answer_type_id browser-default custom-select" name="answer_number">
                                        <option value="" disabled>Answer Type</option>
                                        @foreach($answer_types as $key=>$item)
                                            <option value={{$item->id}}>
                                                {{$item->name}}
                                            </option>
                                        @endforeach()
                                    </select>
                                    <small class="text-danger error_message_text form_name_required_message" id="answer_type_id_msg_edit" style="left: 5.5% !important;"></small>

                                </div>
                            </div>
                        </div>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('documentQuestion_layout_edit') || Auth::user()->all_companies == 1 )

                        <div class="md-form form_type_text_field mb-5 layout_group_div" >
                            <div class="md-form form_type_text_field">
                                <i class="fa fa-angle-right prefix icon_center_align grey-text"></i>
                                <div class="md-form form_type_text_field ml-4" style="margin-left: 0.7rem !important;">
                                    <div class="form-check float-left ml-2">
                                        <input type="radio"  value="1" class="form-check-input layout_vertical" id="layouts_edit1" name="layout">
                                        <label class="form-check-label" for="layouts_edit1">
                                            <span class="question_span assign_class label{{getKeyid('horizontal',$data)}}" data-id="{{getKeyid('horizontal',$data) }}" data-value="{{checkKey('horizontal',$data) }}" >
                                                {!! checkKey('horizontal',$data) !!}
                                            </span>
                                        </label>
                                    </div>
                                    <div class="form-check float-left">
                                        <input type="radio" value="2" class="form-check-input layout_horizontal" id="layouts_edit2" name="layout">
                                        <label class="form-check-label" for="layouts_edit2">
                                            <span class="question_span assign_class label{{getKeyid('vertical',$data)}}" data-id="{{getKeyid('vertical',$data) }}" data-value="{{checkKey('vertical',$data) }}" >
                                                {!! checkKey('vertical',$data) !!}
                                            </span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </br>
                        </br>
                    @endif
                    <input type="hidden" name="section_id" class="question_section_id_edit">
                     <div class="md-form form_type_text_field mb-5 comment_summer_note">
                            <div class="md-form form_type_text_field">
                                <i class="fa fa-reply prefix icon_top_align grey-text"></i>
                                <span class="ml-5 margin_lebel_comment question_span margin_lebel">
                                    Comments
                                </span>
                                <div class="md-form form_type_text_field ml-4-5 comment_section ">
                                    <div id="sc" class="comment_summer"></div>
                                    <input type="hidden" name="comment" id="cmnt">
                                </div>
                            </div>
                        </div>

                        <div class="md-form form_type_text_field mb-5 si_text">
                            <div class="md-form form_type_text_field">
                                <i class="fa fa-question prefix icon_top_align grey-text"></i>
                                <span class="question_span margin_lebel ml-4-5">
                                    How many
                                </span>
                                <input placeholder="Signature Text" type="number" id="signature_text" name="signature_required_app_text_field" class="form-control signature_text">
                            </div>
                        </div>
                         <div class="md-form form_type_text_field mb-5 si_select">
                            <div class="md-form form_type_text_field">
                                <i class="fa fa-user prefix icon_top_align grey-text"></i>
                                <span class="question_span margin_lebel ml-4-5">
                                    Select User
                                </span>
                                <div class="md-form  ml-4-5 mt-1 user_sig_edit">
                                </div>
                            </div>
                        </div>

                          <div class="md-form form_type_text_field mb-5 si_select">
                            <div class="md-form form_type_text_field">
                                <i class="fa fa-question prefix icon_top_align grey-text"></i>
                                <span class="question_span margin_lebel ml-4-5">
                                    How many
                                </span>
                                <input placeholder="Signature User" type="number" id="signature_user" name="signature_required_app_select_menu" class="form-control signature_user">
                            </div>
                        </div>
                         <div class="md-form form_type_text_field import">
                            <i class="fa fa-upload prefix icon_top_align grey-text fa-2x" ></i>
                                <span class="ml-5 margin_lebel form_type_text_field question_span">
                                    Import field
                                </span>
                            <div class="md-form form_type_text_field ml-4-5">
                                <input type="text" id="signature_user_edit" name="import_name" class="form-control import_field">
                            </div>
                        </div>
                    <div class="md-form form_type_text_field column_size">
                            <div class="md-form form_type_text_field">
                                <i class="fa fa-sort prefix grey-text fa-2x" ></i>
                                <span class="margin_lebel_comment question_span">
                                    Column Size
                                </span>
                                <div class="md-form form_type_text_field ml-4-5 mt-1 column_size_edit">
                                    <select searchable="Search here.."   class="icon_size  browser-default custom-select" name="column_size">
                                        <option value="" disabled>Column Size</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    @if(Auth()->user()->hasPermissionTo('documentQuestion_answer_edit') || Auth::user()->all_companies == 1 )
                        <div class="md-form form_type_text_field mb-5 answer_group_div"  >
                            <div class="md-form form_type_text_field">
                                <i class="fa fa-times prefix icon_center_top_align grey-text"></i>
                                {{--                                <label data-error="wrong" data-success="right" for="name" class="active">--}}
                                <span class="question_span answer_text_color margin_lebel question_order assign_class label{{getKeyid('answer',$data)}}" data-id="{{getKeyid('answer',$data) }}" data-value="{{checkKey('answer',$data) }}" >
                                        {!! checkKey('answer',$data) !!}
                                    </span>
                                {{--                                </label>--}}

                                <div class="md-form  ml-4-5 answer_group_id_edit">
                                    <select searchable="Search here.."  class="mdb-select" name="answer_group">
                                        <option value="" selected>Please Select</option>
                                        @foreach($answer_groups as $key =>$value)
                                            <option value="{{$value->id}}">{{$value->name}}</option>
                                        @endforeach()
                                    </select>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                @if(Auth()->user()->hasPermissionTo('edit_documentQuestion') || Auth::user()->all_companies == 1 )
                    <div class="modal-footer d-flex justify-content-center">
                        <button class="form_submit_check btn btn-primary btn-sm question_edit_submit" type="submit">
                            <span class="assign_class label{{getKeyid('update',$data)}}" data-id="{{getKeyid('update',$data) }}" data-value="{{checkKey('update',$data) }}" >
                                {!! checkKey('update',$data) !!}
                            </span>
                        </button>
                    </div>
                @endif
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $( "#question_number_edit" ).change(function() {
            var max = parseInt($(this).attr('max'));
            var min = parseInt($(this).attr('min'));
            if ($(this).val() > max)
            {
                $(this).val(max);
            }
            else if ($(this).val() < min)
            {
                $(this).val(min);
            }
        });
    });


</script>
