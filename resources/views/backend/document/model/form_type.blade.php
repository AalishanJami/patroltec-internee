@php
    $data=localization();
@endphp
<style type="text/css">
    .form-check-input {
        position: absolute !important;
        pointer-events: none !important;
        opacity: 0!important;
    }
    @media (min-width: 576px) {
        .modal-dialog {
            max-width: 75% !important;
        }
    }
    #form_type_table>tbody>tr>td:nth-child(2){
        width: 5%;
        text-align: center;
    }

    #form_type_table>tbody>tr>td:nth-child(1){
        width: 5%;
    }
    #form_type_table>tbody>tr>td:nth-child(4){
        width: 5%;
        text-align: center;
    }
    .selected {
        background-color: #B0BED9 !important;
    }
</style>
<div class="modal fade" id="formtypeModal" tabindex="-1" data-keyboard="false" data-backdrop="static" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">
                    <span class="assign_class label{{getKeyid('form_type_header',$data)}}" data-id="{{getKeyid('form_type_header',$data) }}" data-value="{{checkKey('form_type_header',$data) }}" >
                      {!! checkKey('form_type_header',$data) !!}
                    </span>
                    @if(Auth()->user()->hasPermissionTo('view_cms') || Auth::user()->all_companies == 1 )
                        <button data-class="edit-disable" class="edit_cms_enable edit_cms_switch btn btn-primary btn-sm waves-effect waves-light" style="display: none;" >
                            Edit cms
                        </button>
                        <button  data-class="edit-enable" class="edit_cms_disable edit_cms_switch btn btn-danger btn-sm waves-effect waves-light">
                            Edit cms
                        </button>
                    @endif
                </h4>
                <button type="button" class="close form_type_close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="card">
                <div class="card-body">
                    <div id="table-client" class="table-editable table-responsive">
                        @if(Auth()->user()->hasPermissionTo('create_form_type') || Auth::user()->all_companies == 1 )
                            <span class="add_form_type table-add float-right mb-3 mr-2">
                                <a class="text-success"><i class="fas fa-plus fa-2x" aria-hidden="true"></i></a>
                            </span>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('delete_selected_form_type') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-danger btn-sm" id="deleteselectedform_type">
                                <span class="assign_class label{{getKeyid('form_type_delete_selected',$data)}}" data-id="{{getKeyid('form_type_delete_selected',$data) }}" data-value="{{checkKey('form_type_delete_selected',$data) }}" >
                                    {!! checkKey('form_type_delete_selected',$data) !!}
                                  </span>
                            </button>
                            <button class="btn btn-danger btn-sm" style="display: none;" id="deleteselectedform_typeSoft">
                                <span class="assign_class label{{getKeyid('form_type_delete_selected',$data)}}" data-id="{{getKeyid('form_type_delete_selected',$data) }}" data-value="{{checkKey('form_type_delete_selected',$data) }}" >
                                    {!! checkKey('form_type_delete_selected',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_active_form_type') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-success btn-sm" style="display: none;" id="selectedactivebuttonform_type">
                                <span class="assign_class label{{getKeyid('form_type_active_selected',$data)}}" data-id="{{getKeyid('form_type_active_selected',$data) }}" data-value="{{checkKey('form_type_active_selected',$data) }}" >
                                    {!! checkKey('form_type_active_selected',$data) !!}
                                </span>   
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_restore_form_type') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-primary btn-sm" id="restorebuttonform_type">
                                <span class="assign_class label{{getKeyid('form_type_restore_delete',$data)}}" data-id="{{getKeyid('form_type_restore_delete',$data) }}" data-value="{{checkKey('form_type_restore_delete',$data) }}" >
                                    {!! checkKey('form_type_restore_delete',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('select_active_form_type') || Auth::user()->all_companies == 1 )
                            <button  style="display: none;" class="btn btn-primary btn-sm" id="activebuttonform_type">
                                <span class="assign_class label{{getKeyid('form_type_show_active',$data)}}" data-id="{{getKeyid('form_type_show_active',$data) }}" data-value="{{checkKey('form_type_show_active',$data) }}" >
                                    {!! checkKey('form_type_show_active',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('csv_form_type') || Auth::user()->all_companies == 1 )
                            <form class="form-style" id="export_excel_form_type" method="POST" action="{{ url('form_type/export/excel') }}">
                                {!! csrf_field() !!}
                                <input type="hidden" class="form_type_export" name="excel_array" value="1">
                                <button  type="submit" id="export_excel_employee_btn" class="form_submit_check btn btn-warning btn-sm">
                                   <span class="assign_class label{{getKeyid('form_type_excel_export',$data)}}" data-id="{{getKeyid('form_type_excel_export',$data) }}" data-value="{{checkKey('form_type_excel_export',$data) }}" >
                                        {!! checkKey('form_type_excel_export',$data) !!}
                                    </span>
                                </button>
                            </form>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('word_form_type') || Auth::user()->all_companies == 1 )
                            <form class="form-style" id="export_world_form_type" method="POST" action="{{ url('form_type/export/world') }}">
                                {!! csrf_field() !!}
                                <input type="hidden" class="form_type_export" name="word_array" value="1">
                                <button  type="submit" id="export_word_note" class="form_submit_check btn btn-success btn-sm">
                                   <span class="assign_class label{{getKeyid('form_type_word_export',$data)}}" data-id="{{getKeyid('form_type_word_export',$data) }}" data-value="{{checkKey('form_type_word_export',$data) }}" >
                                        {!! checkKey('form_type_word_export',$data) !!}
                                    </span>
                                </button>
                            </form>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('pdf_form_type') || Auth::user()->all_companies == 1 )
                            <form  class="form-style" id="export_pdf_form_type" method="POST" action="{{ url('form_type/export/pdf') }}">
                                {!! csrf_field() !!}
                                <input type="hidden" class="form_type_export" name="pdf_array" value="1">
                                <button  type="submit" id="export_pdf_note_btn"  class="form_submit_check btn btn-secondary btn-sm">
                                    <span class="assign_class label{{getKeyid('form_type_pdf_export',$data)}}" data-id="{{getKeyid('form_type_pdf_export',$data) }}" data-value="{{checkKey('form_type_pdf_export',$data) }}" >
                                        {!! checkKey('form_type_pdf_export',$data) !!}
                                    </span>
                                </button>
                            </form>
                        @endif
                        <div id="table_form_type" class="table-responsive table-editable">
                            <table id="form_type_table" style="width: 100%;" class="table table-bordered table-responsive-md table-striped">
                                <thead>
                                    <tr>
                                        <th class="no-sort"></th>
                                        <th class="no-sort" id="action"></th>
                                        <th>
                                            <span class="assign_class label{{getKeyid('form_type_name',$data)}}" data-id="{{getKeyid('form_type_name',$data) }}" data-value="{{checkKey('form_type_name',$data) }}" >
                                                {!! checkKey('form_type_name',$data) !!}
                                            </span>
                                        </th>
                                        <th>
                                            <span class="assign_class label{{getKeyid('form_type_show_dashboard',$data)}}" data-id="{{getKeyid('form_type_show_dashboard',$data) }}" data-value="{{checkKey('form_type_show_dashboard',$data) }}" >
                                                {!! checkKey('form_type_show_dashboard',$data) !!}
                                            </span>
                                        </th>
                                        <th>
                                            <span class="assign_class label{{getKeyid('form_type_count_dashboard',$data)}}" data-id="{{getKeyid('form_type_count_dashboard',$data) }}" data-value="{{checkKey('form_type_count_dashboard',$data) }}" >
                                                {!! checkKey('form_type_count_dashboard',$data) !!}
                                            </span>
                                        </th>
                                        <th> 
                                            <span class="assign_class label{{getKeyid('form_type_count_day',$data)}}" data-id="{{getKeyid('form_type_count_day',$data) }}" data-value="{{checkKey('form_type_count_day',$data) }}" >
                                                {!! checkKey('form_type_count_day',$data) !!}
                                            </span>
                                        </th>
                                        <th>
                                            <span class="assign_class label{{getKeyid('form_type_count_response_positive',$data)}}" data-id="{{getKeyid('form_type_count_response_positive',$data) }}" data-value="{{checkKey('form_type_count_response_positive',$data) }}" >
                                                {!! checkKey('form_type_count_response_positive',$data) !!}
                                            </span>
                                        </th>
                                        <th> 
                                            <span class="assign_class label{{getKeyid('form_type_count_response_negative',$data)}}" data-id="{{getKeyid('form_type_count_response_negative',$data) }}" data-value="{{checkKey('form_type_count_response_negative',$data) }}" >
                                                {!! checkKey('form_type_count_response_negative',$data) !!}
                                            </span>
                                        </th>
                                        <th> 
                                            <span class="assign_class label{{getKeyid('form_type_outstanding_count',$data)}}" data-id="{{getKeyid('form_type_outstanding_count',$data) }}" data-value="{{checkKey('form_type_outstanding_count',$data) }}" >
                                                {!! checkKey('form_type_outstanding_count',$data) !!}
                                            </span>
                                        </th>
                                        <th>
                                            <span class="assign_class label{{getKeyid('form_type_late_completion_count',$data)}}" data-id="{{getKeyid('form_type_late_completion_count',$data) }}" data-value="{{checkKey('form_type_late_completion_count',$data) }}" >
                                                {!! checkKey('form_type_late_completion_count',$data) !!}
                                            </span>
                                        </th>
                                        <th>
                                            <span class="assign_class label{{getKeyid('form_type_detailed_field_stats',$data)}}" data-id="{{getKeyid('form_type_detailed_field_stats',$data) }}" data-value="{{checkKey('form_type_detailed_field_stats',$data) }}" >
                                                {!! checkKey('form_type_detailed_field_stats',$data) !!}
                                            </span>
                                        </th>
                                        <th class="no-sort"></th>
                                    </tr>
                                </thead>
                                <tbody ></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('backend.document.model.form_type_permision')
@include('backend.label.input_label')
<script type="text/javascript">
    function checkSelectedFormType(value,checkValue)
    {
      if(value == checkValue)
      {
        return 'checked';
      }
      else
      {
        return "";
      }

    }
    function form_type_data(url) {
        $.ajax({
            type: 'GET',
            url: url,
            success: function(response)
            {
                var table = $('#form_type_table').dataTable(
                    {
                        processing: true,
                        language: {
                              'lengthMenu': '  _MENU_ ',
                              'search':'<span class="assign_class label'+$('#form_type_search_id').attr('data-id')+'" data-id="'+$('#form_type_search_id').attr('data-id')+'" data-value="'+$('#form_type_search_id').val()+'" >'+$('#form_type_search_id').val()+'</span>',
                              'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                              'info':'',
                              'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                              'paginate': {
                                  'previous': '<span class="assign_class label'+$('#form_type_previous_label').attr('data-id')+'" data-id="'+$('#form_type_previous_label').attr('data-id')+'" data-value="'+$('#form_type_previous_label').val()+'" >'+$('#form_type_previous_label').val()+'</span>',
                                  'next': '<span class="assign_class label'+$('#form_type_next_label').attr('data-id')+'" data-id="'+$('#form_type_next_label').attr('data-id')+'" data-value="'+$('#form_type_next_label').val()+'" >'+$('#form_type_next_label').val()+'</span>',
                              },
                              'infoFiltered': "(filtered from _MAX_ total records)"
                        },
                        "ajax": {
                            "url": url,
                            "type": 'get',
                        },
                        
                        "createdRow": function( row, data, dataIndex,columns )
                        {
                            var checkbox_permission=$('#checkbox_permission').val();
                            var checkbox='';
                            checkbox+='<div class="form-check"><input type="checkbox" class="form-check-input" id="form_type'+data.id+'"><label class="form-check-label" for="form_type'+data.id+'""></label></div>';
                            var check_show_dashboard='';
                            var check_count_dashboard='';
                            var check_count_day='';
                            var check_count_response_postive='';
                            var check_count_response_negative='';
                            var check_outanding_count='';
                            var check_late_completion_count='';
                            var check_detailed_field_stats='';
                            if(!$('#show_dashboard_permission_edit').val() || !$('#edit_permission_form_type').val())
                            {
                                check_show_dashboard='disabled';
                            }
                            if(!$('#count_dashboard_permission_edit').val() || ! $('#edit_permission_form_type').val())
                            {
                                check_count_dashboard='disabled';
                            }
                            if(!$('#count_day_permission_edit').val() || !$('#edit_permission_form_type').val())
                            {
                                check_count_day='disabled';
                            }
                            if(!$('#count_response_positive_permission_edit').val() || !$('#edit_permission_form_type').val())
                            {
                                check_count_response_postive='disabled';
                            }
                            if(!$('#count_response_negative_permission_edit').val() || !$('#edit_permission_form_type').val())
                            {
                                check_count_response_negative='disabled';
                            }
                            if(!$('#outanding_count_permission_edit').val() || !$('#edit_permission_form_type').val())
                            {
                                check_outanding_count='disabled';
                            }
                            if(!$('#late_completion_count_permission_edit').val() || !$('#edit_permission_form_type').val())
                            {
                                check_late_completion_count='disabled';
                            }
                            if(!$('#detailed_field_stats_permission_edit').val() || !$('#edit_permission_form_type').val())
                            {
                                check_detailed_field_stats='disabled';
                            }
                            var submit='';
                            var show_dashboard='';
                            show_dashboard+='<div class="form-check float-left">';
                            show_dashboard+=' <input type="radio" '+check_show_dashboard+' '+checkSelectedFormType(data._show_dashboard,1)+'  value="1" name="show_dashboard'+data.id+'" class="show_dashboard'+data.id+' form-check-input show_button" data-id="'+data.id+'" id="yes_show_dashboard'+data.id+'">';
                            show_dashboard+='<label class="form-check-label" for="yes_show_dashboard'+data.id+'">Yes</label>';
                            show_dashboard+='</div>';
                            show_dashboard+='<div class="form-check float-left">';
                            show_dashboard+=' <input type="radio" '+check_show_dashboard+' '+checkSelectedFormType(data._show_dashboard,0)+'  value="0" name="show_dashboard'+data.id+'"  class="show_dashboard'+data.id+' form-check-input show_button" data-id="'+data.id+'" id="no_show_dashboard'+data.id+'">';
                            show_dashboard+='<label class="form-check-label" for="no_show_dashboard'+data.id+'">No</label>';
                            show_dashboard+='</div>';
                            var count_dashboard='';
                            count_dashboard+='<div class="form-check float-left">';
                            count_dashboard+=' <input type="radio" '+check_count_dashboard+' '+checkSelectedFormType(data._count_dashboard,1)+'  value="1" name="count_dashboard'+data.id+'" class="count_dashboard'+data.id+' form-check-input show_button" data-id="'+data.id+'" id="yes_count_dashboard'+data.id+'">';
                            count_dashboard+='<label class="form-check-label" for="yes_count_dashboard'+data.id+'">Yes</label>';
                            count_dashboard+='</div>';
                            count_dashboard+='<div class="form-check float-left">';
                            count_dashboard+=' <input type="radio" '+check_count_dashboard+'  '+checkSelectedFormType(data._count_dashboard,0)+'  value="0" name="count_dashboard'+data.id+'"  class="count_dashboard'+data.id+' form-check-input show_button" data-id="'+data.id+'" id="no_count_dashboard'+data.id+'">';
                            count_dashboard+='<label class="form-check-label" for="no_count_dashboard'+data.id+'">No</label>';
                            count_dashboard+='</div>';
                            var count_day='';
                            count_day+='<div class="form-check float-left">';
                            count_day+=' <input '+check_count_day+'  type="radio" '+checkSelectedFormType(data._count_day,1)+'  value="1" name="count_day'+data.id+'" class="count_day'+data.id+' form-check-input show_button" data-id="'+data.id+'" id="yes_count_day'+data.id+'">';
                            count_day+='<label class="form-check-label" for="yes_count_day'+data.id+'">Yes</label>';
                            count_day+='</div>';
                            count_day+='<div class="form-check float-left">';
                            count_day+=' <input '+check_count_day+' type="radio" '+checkSelectedFormType(data._count_day,0)+'  value="0" name="count_day'+data.id+'"  class="count_day'+data.id+' form-check-input show_button" data-id="'+data.id+'" id="no_count_day'+data.id+'">';
                            count_day+='<label class="form-check-label" for="no_count_day'+data.id+'">No</label>';
                            count_day+='</div>';
                            submit+='<a type="button" class="btn btn-primary btn-xs my-0 waves-effect waves-light saveform_typedata " id="'+data.id+'"><i class="fas fa-check"></i></a>';
                            var count_response_positive='';
                            count_response_positive+='<div class="form-check float-left">';
                            count_response_positive+=' <input type="radio" '+check_count_response_postive+' '+checkSelectedFormType(data._count_response_positive,1)+'  value="1" name="count_response_positive'+data.id+'" class="count_response_positive'+data.id+' form-check-input show_button" data-id="'+data.id+'" id="yes_count_response_positive'+data.id+'">';
                            count_response_positive+='<label class="form-check-label" for="yes_count_response_positive'+data.id+'">Yes</label>';
                            count_response_positive+='</div>';
                            count_response_positive+='<div class="form-check float-left">';
                            count_response_positive+=' <input  '+check_count_response_postive+' type="radio" '+checkSelectedFormType(data._count_response_positive,0)+'  value="0" name="count_response_positive'+data.id+'"  class="count_response_positive'+data.id+' form-check-input show_button" data-id="'+data.id+'" id="no_count_response_positive'+data.id+'">';
                            count_response_positive+='<label class="form-check-label" for="no_count_response_positive'+data.id+'">No</label>';
                            count_response_positive+='</div>';
                            var count_response_negative='';
                            count_response_negative+='<div class="form-check float-left">';
                            count_response_negative+=' <input '+check_count_response_negative+' type="radio" '+checkSelectedFormType(data._count_response_negative,1)+'  value="1" name="count_response_negative'+data.id+'" class="count_response_negative'+data.id+' form-check-input show_button" data-id="'+data.id+'" id="yes_count_response_negative'+data.id+'">';
                            count_response_negative+='<label class="form-check-label" for="yes_count_response_negative'+data.id+'">Yes</label>';
                            count_response_negative+='</div>';
                            count_response_negative+='<div class="form-check float-left">';
                            count_response_negative+=' <input '+check_count_response_negative+' type="radio" '+checkSelectedFormType(data._count_response_negative,0)+'  value="0" name="count_response_negative'+data.id+'"  class="count_response_negative'+data.id+' form-check-input show_button" data-id="'+data.id+'" id="no_count_response_negative'+data.id+'">';
                            count_response_negative+='<label class="form-check-label" for="no_count_response_negative'+data.id+'">No</label>';
                            count_response_negative+='</div>';
                            var outanding_count='';
                            outanding_count+='<div class="form-check float-left">';
                            outanding_count+=' <input '+check_outanding_count+' type="radio" '+checkSelectedFormType(data._outanding_count,1)+'  value="1" name="outanding_count'+data.id+'" class="outanding_count'+data.id+' form-check-input show_button" data-id="'+data.id+'" id="yes_outanding_count'+data.id+'">';
                            outanding_count+='<label class="form-check-label" for="yes_outanding_count'+data.id+'">Yes</label>';
                            outanding_count+='</div>';
                            outanding_count+='<div class="form-check float-left">';
                            outanding_count+=' <input '+check_outanding_count+' type="radio" '+checkSelectedFormType(data._outanding_count,0)+'  value="0" name="outanding_count'+data.id+'"  class="outanding_count'+data.id+' form-check-input show_button" data-id="'+data.id+'" id="no_outanding_count'+data.id+'">';
                            outanding_count+='<label class="form-check-label" for="no_outanding_count'+data.id+'">No</label>';
                            outanding_count+='</div>';
                            var late_completion_count='';
                            late_completion_count+='<div class="form-check float-left">';
                            late_completion_count+=' <input '+check_late_completion_count+' type="radio" '+checkSelectedFormType(data._late_completion_count,1)+'  value="1" name="late_completion_count'+data.id+'" class="late_completion_count'+data.id+' form-check-input show_button" data-id="'+data.id+'" id="yes_late_completion_count'+data.id+'">';
                            late_completion_count+='<label class="form-check-label" for="yes_late_completion_count'+data.id+'">Yes</label>';
                            late_completion_count+='</div>';
                            late_completion_count+='<div class="form-check float-left">';
                            late_completion_count+=' <input '+check_late_completion_count+'  type="radio" '+checkSelectedFormType(data._late_completion_count,0)+'  value="0" name="late_completion_count'+data.id+'"  class="late_completion_count'+data.id+' form-check-input show_button" data-id="'+data.id+'" id="no_late_completion_count'+data.id+'">';
                            late_completion_count+='<label class="form-check-label" for="no_late_completion_count'+data.id+'">No</label>';
                            late_completion_count+='</div>';
                            var detailed_field_stats='';
                            detailed_field_stats+='<div class="form-check float-left">';
                            detailed_field_stats+=' <input '+check_detailed_field_stats+' type="radio" '+checkSelectedFormType(data._detailed_field_stats,1)+'  value="1" name="detailed_field_stats'+data.id+'" class="detailed_field_stats'+data.id+' form-check-input show_button" data-id="'+data.id+'" id="yes_detailed_field_stats'+data.id+'">';
                            detailed_field_stats+='<label class="form-check-label" for="yes_detailed_field_stats'+data.id+'">Yes</label>';
                            detailed_field_stats+='</div>';
                            detailed_field_stats+='<div class="form-check float-left">';
                            detailed_field_stats+=' <input '+check_detailed_field_stats+' type="radio" '+checkSelectedFormType(data._detailed_field_stats,0)+'  value="0" name="detailed_field_stats'+data.id+'"  class="detailed_field_stats'+data.id+' form-check-input show_button" data-id="'+data.id+'" id="no_detailed_field_stats'+data.id+'">';
                            detailed_field_stats+='<label class="form-check-label" for="no_detailed_field_stats'+data.id+'">No</label>';
                            detailed_field_stats+='</div>';
                            $(columns[0]).html(checkbox);
                            $(columns[2]).attr('id', 'name'+data['id']);
                            if($('#name_permission_edit').val() && $('#edit_permission_form_type').val())
                            {
                                $(columns[2]).attr('Contenteditable', 'true');
                            }
                            $(columns[2]).attr('class','edit_inline_form_type');
                            $(columns[3]).html(show_dashboard);
                            $(columns[4]).html(count_dashboard);
                            $(columns[5]).html(count_day);
                            $(columns[6]).html(count_response_positive);
                            $(columns[7]).html(count_response_negative);
                            $(columns[8]).html(outanding_count);
                            $(columns[9]).html(late_completion_count);
                            $(columns[10]).html(detailed_field_stats);
                            if($('#edit_permission_form_type').val())
                            {
                                $(columns[11]).html(submit);
                            }
                            $(row).attr('id', 'form_type_tr'+data['id']);
                            $(row).attr('class', 'selectedrowform_type');
                            var temp=data['id'];
                        },
                        columns:
                            [
                                {data: 'checkbox', name: 'checkbox',visible:$('#checkbox_permission').val()},
                                {data: 'actions', name: 'actions'},
                                {data: 'name', name: 'name',visible:$('#name_permission').val()},
                                {data: '_show_dashboard', name: '_show_dashboard',visible:$('#show_dashboard_permission').val()},
                                {data: 'count_dashboard', name: 'count_dashboard',visible:$('#count_dashboard_permission').val()},
                                {data: 'count_day', name: 'count_day',visible:$('#count_day_permission').val()},
                                {data: 'count_response_positive', name: 'count_response_positive',visible:$('#count_response_positive_permission').val()},
                                {data: 'count_response_negative', name: 'count_response_negative',visible:$('#count_response_negative_permission').val()},
                                {data: 'outanding_count', name: 'outanding_count',visible:$('#outanding_count_permission').val()},
                                {data: 'late_completion_count', name: 'late_completion_count',visible:$('#late_completion_count_permission').val()},
                                {data: 'detailed_field_stats', name: 'detailed_field_stats',visible:$('#detailed_field_stats_permission').val()},
                                {data: 'submit', name: 'submit'},
                            ],
                    }
                );
            },
            error: function (error) {
                console.log(error);
            }
        });
    }
    var form_type_url='/form_type/getall';
    form_type_data(form_type_url);
    $('.form_type_close').click(function(){
        $.ajax({
            type: "GET",
            url: '/form_type/refresh',
            success: function(response)
            {
                var html='';
                for (var i = response.data.length - 1; i >= 0; i--) {
                    html+='<option value="'+response.data[i].id+'">'+response.data[i].name+'</option>';
                }
                $('#form_type').empty();
                $('#form_type').html(html);
            },
            error: function (error) {
                console.log(error);
            }
        });
    });
    $(document).ready(function () {
        $.ajax({
            type: "GET",
            url: form_type_url,
            success: function(response)
            {
                var html='';
                for (var i = response.data.length - 1; i >= 0; i--) {
                    html+='<option value="'+response.data[i].id+'">'+response.data[i].name+'</option>';
                }
                $('#sexual_orientation_id').empty();
                $('#sexual_orientation_id').html(html);
            },
            error: function (error) {
                console.log(error);
            }
        });
    });

</script>
