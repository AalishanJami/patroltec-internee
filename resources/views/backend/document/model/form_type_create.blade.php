@php
        $data=localization();
@endphp

<div class="modal fade" id="formTypeCreate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Form Type </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form_type_form" method="POST" >
                <input type="hidden" name="form_type_id" class="form_type_id" value="" id="form_type_id">
                <div class="modal-body">
                    <div class="md-form mb-5">
                       
                            <div class="row">
                                <div class="col">
                                    <div class="md-form">
                                        <span class="assign_class label{{getKeyid('name',$data)}}" data-id="{{getKeyid('name',$data) }}" data-value="{{checkKey('name',$data) }}" >
                                            {!! checkKey('name',$data) !!}
                                        </span>
                                    {!! Form::text('name',null,['class' => 'form_type_name form-control ','id'=>'form_type_name']) !!}
                                    </div>
                                </div>
                                 <div class="col">
                                    <div class="md-form">
                                        <span class="assign_class label{{getKeyid('action_score',$data)}}" data-id="{{getKeyid('action_score',$data) }}" data-value="{{checkKey('action_score',$data) }}" >
                                            {!! checkKey('action_score',$data) !!}
                                        </span>
                                    {!! Form::number('action_score',null,['class' => 'action_score form-control ','id'=>'action_score']) !!}
                                    
                                    </div>
                                </div>
                            </div>
                        
                        <div class="row">
                             <div class="col">
                                <div class="form-check">
                                {!! Form::checkbox('_show_dashboard','',false,['class'=>'show_dashboard form-check-input check_form_type','id'=>'show_dashboard']) !!}
                                <label class="form-check-label" for="show_dashboard">
                                show dashboard
                                </label>
                                </div>

                                </div>
                                <div class="col">
                                <div class="form-check">
                                {!! Form::checkbox('count_dashboard','',false,['class'=>'count_dashboard form-check-input check_form_type','id'=>'count_dashboard']) !!}
                                <label class="form-check-label" for="count_dashboard">
                                count dashboard
                                </label>
                                </div>

                                </div>

                        </div> 

                        <div class="row">
                        <div class="col">
                        <div class="form-check">
                        {!! Form::checkbox('count_day','',false,['class'=>'count_day form-check-input check_form_type','id'=>'count_day']) !!}
                        <label class="form-check-label" for="count_day">
                        count day
                        </label>
                        </div>

                        </div>
                        <div class="col">
                        <div class="form-check">
                        {!! Form::checkbox('count_response_positive','',false,['class'=>'form-check-input count_response_positive check_form_type','id'=>'count_response_positive']) !!}
                        <label class="form-check-label" for="count_response_positive">
                        count response postive
                        </label>
                        </div>

                        </div>

                        </div>   

                        <div class="row">
                        <div class="col">
                        <div class="form-check">
                        {!! Form::checkbox('count_response_negative','',false,['class'=>'form-check-input check_form_type','id'=>'count_response_negative']) !!}
                        <label class="form-check-label" for="count_response_negative">
                        count response negative
                        </label>
                        </div>

                        </div>
                        <div class="col">
                        <div class="form-check">
                        {!! Form::checkbox('outanding_count','',false,['class'=>'form-check-input check_form_type','id'=>'outanding_count']) !!}
                        <label class="form-check-label" for="outanding_count">
                        outanding count
                        </label>
                        </div>

                        </div>

                        </div> 
                        <div class="row">
                        <div class="col">
                        <div class="form-check">
                        {!! Form::checkbox('late_completion_count','',false,['class'=>'form-check-input check_form_type','id'=>'late_completion_count']) !!}
                        <label class="form-check-label" for="late_completion_count">
                        late completion count
                        </label>
                        </div>

                        </div>
                        <div class="col">
                        <div class="form-check">
                        {!! Form::checkbox('detailed_field_stats','',false,['class'=>'form-check-input check_form_type','id'=>'detailed_field_stats']) !!}
                        <label class="form-check-label" for="detailed_field_stats">
                        detailed field stats
                        </label>
                        </div>

                        </div>

                        </div> 
                   
                    </div>
                 
                </div>


                <div class="modal-footer d-flex justify-content-center">
                    <button class="btn btn-primary btn-sm saveform_typedata" type="button"><span class="assign_class label{{getKeyid('save',$data)}}" data-id="{{getKeyid('save',$data) }}" data-value="{{checkKey('save',$data) }}" >{!! checkKey('save',$data) !!} </span></button>
                </div>
            </form>
        </div>
    </div>
</div>
