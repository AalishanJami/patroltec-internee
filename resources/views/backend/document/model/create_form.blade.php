@php
    $data=localization();
@endphp
<style>
    .question_name{
        margin-top: -5px;
        display:block;
    }
    .question_textarea_editor{
        width: 94.5%;
        margin-left: auto;
    }
    .question_order{
        margin-left:40px;
    }
    .icon_top_align{
        margin-top: -6px;
    }
    .icon_center_align{
        margin-top: 5px;
    }
    .icon_center_top_align{
        margin-top: -8px;
    }
</style>
<div class="modal fade " id="modelAddDynamicForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog model_width" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">
                    <span class="assign_class label{{getKeyid('questions',$data)}}" data-id="{{getKeyid('questions',$data) }}" data-value="{{checkKey('questions',$data) }}" >
                        {!! checkKey('questions',$data) !!}
                    </span>
                    @if(Auth()->user()->hasPermissionTo('view_cms') || Auth::user()->all_companies == 1 )
                        <button data-class="edit-disable" class="edit_cms_enable edit_cms_switch btn btn-primary btn-sm waves-effect waves-light" style="display: none;" >
                            Edit cms
                        </button>
                        <button  data-class="edit-enable" class="edit_cms_disable edit_cms_switch btn btn-danger btn-sm waves-effect waves-light">
                            Edit cms
                        </button>
                    @endif
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="question_Form">
                <input type="hidden" name="section_id" id="question_section_id">
                <div class="modal-body mx-3">
                    @if(Auth()->user()->hasPermissionTo('documentQuestion_question_create') || Auth::user()->all_companies == 1 )

                        <div class="md-form  mb-5">
                            <i class="fa fa-edit prefix grey-text"></i>
                            <label  for="name" >
                                <span class="question_name assign_class label{{getKeyid('questions',$data)}}" data-id="{{getKeyid('questions',$data) }}" data-value="{{checkKey('questions',$data) }}" >
                                    {!! checkKey('questions',$data) !!}
                                </span>
                            </label>
                        </div>
                        <!-- <textarea class="md-textarea form-control" name="question_name"  id="question_name"></textarea> -->
                        {{--<small class="text-danger form_name_required_message" style="left: 3.5% !important;"></small>--}}
                        <div class="md-form  mt-10 question_name_create question_textarea_editor">
                            <div class="summernote_inner body_notes quesion_add_part" ></div>
                            <small class="text-danger error_message_text form_name_required_message" id="question_name_create" style="left: 0.5%;top:101% !important;"></small>

                        </div>
                        <!--  <input type="text" class="form-control " id="question_name" name="question_name" value=""  autocomplete="name" autofocus> -->
                        <!-- <textarea></textarea> -->

                    @endif
                    @if(Auth()->user()->hasPermissionTo('documentQuestion_question_create') || Auth::user()->all_companies == 1 )

                        <div class="md-form  mb-5">
                            <i class="fa fa-edit prefix grey-text"></i>
                            <label  for="name" >
                                <span class="question_name assign_class label{{getKeyid('mandatory_message',$data)}}" data-id="{{getKeyid('mandatory_message',$data) }}" data-value="{{checkKey('mandatory_message',$data) }}" >
                                    {!! checkKey('mandatory_message',$data) !!}
                                </span>
                            </label>
                        </div>
                        <div class="md-form  mt-10 question_mandatory_create question_textarea_editor question_mandatory_text_fielt">
                            <div class="summernote_inner body_notes quesion_add_mandatory" ></div>
                            <small class="text-danger form_name_required_message" id="question_mandatory_create"
                                   style="left: 0.5%;top:101% !important;">
                            </small>
                        </div>
                        <!--  <input type="text" class="form-control " id="question_name" name="question_name" value=""  autocomplete="name" autofocus> -->
                        <!-- <textarea></textarea> -->

                    @endif
                    @if(Auth()->user()->hasPermissionTo('documentQuestion_question_create') || Auth::user()->all_companies == 1 )
                        <div class="md-form f mb-5 " >
                            <div class="md-form">
                                <i class="fa fa-angle-right icon_center_align prefix grey-text"></i>
                                <div class="md-form  ml-4" style="margin-left: 1rem !important;">
                                    <div class="form-check float-left">
                                        <input type="radio" value="0" class="form-check-input" id="layout2" name="layout">
                                        <label class="form-check-label" for="layout2">
                                            <span class=" assign_class label{{getKeyid('display_picture',$data)}}" data-id="{{getKeyid('display_picture',$data) }}" data-value="{{checkKey('display_picture',$data) }}" >
                                                {!! checkKey('display_picture',$data) !!}
                                            </span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                        <!--  <input type="text" class="form-control " id="question_name" name="question_name" value=""  autocomplete="name" autofocus> -->
                        <!-- <textarea></textarea> -->
                        <br>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('documentQuestion_order_create') || Auth::user()->all_companies == 1 )

                        <div class="md-form form_type_text_field form_type_text_file mb-5">

                            <i class="fa fa-sort icon_top_align prefix grey-text fa-2x" ></i>
                            {{--                            <label for="numberBox-create" class="active">--}}
                            <span class="question_span question_order assign_class label{{getKeyid('order',$data)}}" data-id="{{getKeyid('order',$data) }}" data-value="{{checkKey('order',$data) }}" >
                                    {!! checkKey('order',$data) !!}
                                </span>
                            {{--                            </label>--}}
                            <input type="number" id="numberBox-create" class="form_type_text_field form-control numberBox-create" name="question_number" max="12" min="1" required autocomplete="name" autofocus>
                            <small class="text-danger error_message_text form_name_required_message" id="numberBox-create-msg" style="left: 5.5% !important;"></small>

                            {{--<small id="numberBox-create-msg" class="text-danger error_message"></small>--}}
                        </div>
                    @endif
                <!--   <div class="md-form mb-5">
                        <i class="fa fa-sort prefix grey-text fa-2x" ></i>
                        <input type="number" id="icon_size" class="form-control " name="icon_size" autocomplete="name" autofocus>

                        <label data-error="wrong" data-success="right" class="active" for="icon_size" >
                            Icon Size
                        </label>
                    </div> -->


                    {{--                    <div class="md-form mb-5">--}}
                    {{--                        <div class="md-form">--}}
                    {{--                            <i class="fa fa-sort prefix grey-text fa-2x" ></i>--}}
                    {{--                            <span class="ml-5">--}}
                    {{--                                Icon Size--}}
                    {{--                            </span>--}}
                    {{--                            <div class="md-form  ml-4-5 mt-1 question_create_iconsize">--}}

                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}



                    @if(Auth()->user()->hasPermissionTo('documentQuestion_answertype_create') || Auth::user()->all_companies == 1 )

                        <div class="md-form form_type_text_field question_field_align mb-5">
                            <div class="md-form form_type_text_field question_field_align">
                                <i class="fa fa-reply icon_top_align prefix grey-text"></i>
                                <span class="ml-5">
                                    <span class="question_span margin_lebel assign_class label{{getKeyid('answer_type',$data)}}" data-id="{{getKeyid('answer_type',$data) }}" data-value="{{checkKey('answer_type',$data) }}" >
                                        {!! checkKey('answer_type',$data) !!}
                                    </span>
                                </span>
                                <div class="md-form form_type_text_field question_field_align ml-4-5">
                                    <select searchable="Search here.."  id="answer_type_id" class="browser-default custom-select form_type_text_field answer_type_id" name="answer_number"  >
                                        <option value="" selected>Answer Type</option>
                                        @foreach($answer_types as $key=>$item)
                                            <option value={{$item->id}}>
                                                {{$item->name}}
                                            </option>
                                        @endforeach()
                                    </select>
                                    <small class="text-danger error_message_text form_name_required_message" id="answer_number_msg" style="left: 0.2% !important;"></small>

                                    {{--<small id="answer_number_msg" class="text-danger error_message"></small>--}}
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="md-form  form_type_text_field mb-5 comment_summer_note">
                        <div class="md-form form_type_text_field">
                            <i class="fa fa-reply icon_top_align prefix grey-text"></i>
                            <span class="question_span margin_lebel_comment ml-5  assign_class label{{getKeyid('comment',$data)}}" data-id="{{getKeyid('comment',$data) }}" data-value="{{checkKey('comment',$data) }}" >
                                    {!! checkKey('comment',$data) !!}
                                </span>
                            <div class="md-form form_type_text_field  ml-4-5 comment_section_create ">
                                <div id="sc" class="comment_summer comment_section_remove"></div>
                                <input type="hidden" name="comment" id="cmnt">

                            </div>
                        </div>
                    </div>

                    <div class="md-form question_field_align form_type_text_field mb-5 si_text">
                        <div class="md-form form_type_text_field form_type_text_field">
                            <i class="fa fa-question icon_top_align prefix grey-text"></i>
                            <span class="question_span margin_lebel ml-4-5">
                                    How many
                                </span>
                            <input placeholder="Signature Text" type="number" id="signature_text" name="signature_required_app_text_field" class="form_type_text_field form-control">
                        </div>
                    </div>
                    <div class="md-form form_type_text_field question_field_align mb-5 si_select">
                        <div class="md-form question_field_align form_type_text_field form_type_text_field">
                            <i class="fa fa-user prefix icon_top_align grey-text"></i>
                            <span class="question_span margin_lebel ml-4-5">
                                    Select User
                                </span>
                            {!! Form::select('sig_select',$sig_select,null, ['class' =>'ml-4-5 form_type_text_field mdb-select colorful-select dropdown-primary md-form','id'=>'sig_select','searchable'=>'search here','placeholder'=>'Sig Select']) !!}
                        </div>
                    </div>

                    <div class="md-form form_type_text_field mb-5 si_select">
                        <div class="md-form form_type_text_field">
                            <i class="fa fa-question icon_top_align prefix grey-text"></i>
                            <span class="question_span margin_lebel ml-4-5">
                                    How many
                                </span>
                            <input placeholder="Signature User" type="number" id="signature_user" name="signature_required_app_select_menu" class="form_type_text_field form-control">
                        </div>
                    </div>
                    <div class="md-form  form_type_text_field mb-5 import">
                        <i class="fa fa-upload icon_top_align prefix grey-text fa-2x" ></i>
                        <span class="ml-5 margin_lebel_comment question_span label{{getKeyid('import_name',$data)}}" data-id="{{getKeyid('import_name',$data) }}" data-value="{{checkKey('import_name',$data) }}" >
                                {!! checkKey('import_name',$data) !!}
                            </span>
                        <div class="md-form  form_type_text_field ml-4-5">
                            <input type="text" id="signature_user" name="import_name" class="form-control">
                        </div>
                    </div>
                    <div class="md-form form_type_text_field mb-5 column_size">
                        <div class="md-form form_type_text_field">
                            <i class="fa fa-sort icon_top_align prefix grey-text fa-2x" ></i>
                            <span class="ml-5 margin_lebel_comment question_span">
                                Column Size
                            </span>
                            <div class="md-form form_type_text_field  ml-4-5 mt-1">
                                <select searchable="Search here.."  placeholder="Please Select"  id="columnsize_selected_nav" class="form_type_text_field columnsize_selected_nav mdb-select icon_size" name="column_size">
                                    <option value="" selected>Column Size</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    @if(Auth()->user()->hasPermissionTo('documentQuestion_layout_create') || Auth::user()->all_companies == 1 )
                        <div class="md-form form_type_text_field mb-5 layout_group_div" >
                            <div class="md-form form_type_text_field form_type_text_field">
                                <i class="fa fa-angle-right icon_center_align prefix grey-text"></i>
                                <div class="md-form form_type_text_field ml-4" style="margin-left: 1rem !important;">
                                    <div class="form-check float-left">
                                        <input type="radio" value="1" class="form-check-input" id="layout1" name="layout">
                                        <label class="form-check-label" for="layout1">
                                            <span class="question_span assign_class label{{getKeyid('horizontal',$data)}}" data-id="{{getKeyid('horizontal',$data) }}" data-value="{{checkKey('horizontal',$data) }}" >
                                                {!! checkKey('horizontal',$data) !!}
                                            </span>
                                        </label>
                                    </div>
                                    <div class="form-check float-left">
                                        <input type="radio" value="2" class="form-check-input" id="layout2" name="layout">
                                        <label class="form-check-label" for="layout2">
                                            <span class="question_span assign_class label{{getKeyid('vertical',$data)}}" data-id="{{getKeyid('vertical',$data) }}" data-value="{{checkKey('vertical',$data) }}" >
                                                {!! checkKey('vertical',$data) !!}
                                            </span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </br>
                        </br>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('documentQuestion_answer_create') || Auth::user()->all_companies == 1 )
                        <div class="md-form form_type_text_field mb-5 answer_group_div" >
                            <div class="md-form form_type_text_field">
                                {{--                                <i class="fal fa-comment-dots"></i>--}}
                                <i class="far fa-comment-dots icon_center_top_align prefix grey-text"></i>
                                {{--                                <label data-error="wrong" data-success="right" for="name" class="active">--}}
                                <span class="assign_class margin_lebel answer_text_color question_order label{{getKeyid('answer',$data)}}" data-id="{{getKeyid('answer',$data) }}" data-value="{{checkKey('answer',$data) }}" >
                                        {!! checkKey('answer',$data) !!}
                                </span>
                                {{--                                </label>--}}
                                <div class="md-form  form_type_text_field ml-4-5 ">
                                    <select searchable="Search here.." id="answer_group_create"  class="mdb-select" name="answer_group">
                                        <option value="" selected>Chose an answer</option>
                                        @foreach($answer_groups as $key =>$value)
                                            <option value="{{$value->id}}">{{$value->name}}</option>
                                        @endforeach()
                                    </select>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>

                @if(Auth()->user()->hasPermissionTo('create_documentQuestion') || Auth::user()->all_companies == 1 )

                    <div class="modal-footer d-flex justify-content-center">
                        <button class="form_submit_check btn btn-primary btn-sm question_submit" type="submit">
                            <span class="assign_class label{{getKeyid('save',$data)}}" data-id="{{getKeyid('save',$data) }}" data-value="{{checkKey('save',$data) }}" >
                                {!! checkKey('save',$data) !!}
                            </span>
                        </button>
                    </div>
                @endif
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('.comment_summer').summernote();
    $(function () {
        $( "#numberBox-create" ).change(function() {
            var max = parseInt($(this).attr('max'));
            var min = parseInt($(this).attr('min'));
            if ($(this).val() > max)
            {
                $(this).val(max);
            }
            else if ($(this).val() < min)
            {
                $(this).val(min);
            }
        });
    });
</script>
