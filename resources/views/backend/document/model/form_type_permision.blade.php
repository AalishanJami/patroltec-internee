@if(Auth()->user()->all_companies == 1)
    <input type="hidden" id="checkbox_permission" value="1">
    <input type="hidden" id="name_permission" value="1">
    <input type="hidden" id="show_dashboard_permission" value="1">
    <input type="hidden" id="count_dashboard_permission" value="1">
    <input type="hidden" id="count_day_permission" value="1">
    <input type="hidden" id="count_response_positive_permission" value="1">
    <input type="hidden" id="count_response_negative_permission" value="1">
    <input type="hidden" id="outanding_count_permission" value="1">
    <input type="hidden" id="late_completion_count_permission" value="1">
    <input type="hidden" id="detailed_field_stats_permission" value="1">
    <!-- edit -->
    <input type="hidden" id="name_permission_edit" value="1">
    <input type="hidden" id="show_dashboard_permission_edit" value="1">
    <input type="hidden" id="count_dashboard_permission_edit" value="1">
    <input type="hidden" id="count_day_permission_edit" value="1">
    <input type="hidden" id="count_response_positive_permission_edit" value="1">
    <input type="hidden" id="count_response_negative_permission_edit" value="1">
    <input type="hidden" id="outanding_count_permission_edit" value="1">
    <input type="hidden" id="late_completion_count_permission_edit" value="1">
    <input type="hidden" id="detailed_field_stats_permission_edit" value="1">
    <!-- create -->
    <input type="hidden" id="name_permission_create" value="1">
    <input type="hidden" id="show_dashboard_permission_create" value="1">
    <input type="hidden" id="count_dashboard_permission_create" value="1">
    <input type="hidden" id="count_day_permission_create" value="1">
    <input type="hidden" id="count_response_positive_permission_create" value="1">
    <input type="hidden" id="count_response_negative_permission_create" value="1">
    <input type="hidden" id="outanding_count_permission_create" value="1">
    <input type="hidden" id="late_completion_count_permission_create" value="1">
    <input type="hidden" id="detailed_field_stats_permission_create" value="1">
    <!-- edit permision -->
    <input type="hidden" id="edit_permission_form_type" value="1">
@else
    <input type="hidden" id="checkbox_permission" value="{{Auth()->user()->hasPermissionTo('form_type_checkbox')}}">
    <input type="hidden" id="name_permission" value="{{Auth()->user()->hasPermissionTo('form_type_name')}}">
    <input type="hidden" id="show_dashboard_permission" value="{{Auth()->user()->hasPermissionTo('form_type_show_dashboard')}}">
    <input type="hidden" id="count_dashboard_permission" value="{{Auth()->user()->hasPermissionTo('form_type_count_dashboard')}}">
    <input type="hidden" id="count_day_permission" value="{{Auth()->user()->hasPermissionTo('form_type_count_day')}}">
    <input type="hidden" id="count_response_positive_permission" value="{{Auth()->user()->hasPermissionTo('form_type_count_response_positive')}}">
    <input type="hidden" id="count_response_negative_permission" value="{{Auth()->user()->hasPermissionTo('form_type_count_response_negative')}}">
    <input type="hidden" id="outanding_count_permission" value="{{Auth()->user()->hasPermissionTo('form_type_outanding_count')}}">
    <input type="hidden" id="late_completion_count_permission" value="{{Auth()->user()->hasPermissionTo('form_type_late_completion_count')}}">
    <input type="hidden" id="detailed_field_stats_permission" value="{{Auth()->user()->hasPermissionTo('form_type_detailed_field_stats')}}">
    <!-- edit -->
    <input type="hidden" id="name_permission_edit" value="{{Auth()->user()->hasPermissionTo('form_type_name_edit')}}">
    <input type="hidden" id="show_dashboard_permission_edit" value="{{Auth()->user()->hasPermissionTo('form_type_show_dashboard_edit')}}">
    <input type="hidden" id="count_dashboard_permission_edit" value="{{Auth()->user()->hasPermissionTo('form_type_count_dashboard_edit')}}">
    <input type="hidden" id="count_day_permission_edit" value="{{Auth()->user()->hasPermissionTo('form_type_count_day_edit')}}">
    <input type="hidden" id="count_response_positive_permission_edit" value="{{Auth()->user()->hasPermissionTo('form_type_count_response_positive_edit')}}">
    <input type="hidden" id="count_response_negative_permission_edit" value="{{Auth()->user()->hasPermissionTo('form_type_count_response_negative_edit')}}">
    <input type="hidden" id="outanding_count_permission_edit" value="{{Auth()->user()->hasPermissionTo('form_type_outanding_count_edit')}}">
    <input type="hidden" id="late_completion_count_permission_edit" value="{{Auth()->user()->hasPermissionTo('form_type_late_completion_count_edit')}}">
    <input type="hidden" id="detailed_field_stats_permission_edit" value="{{Auth()->user()->hasPermissionTo('form_type_detailed_field_stats_edit')}}">
     <!-- create -->
    <input type="hidden" id="name_permission_create" value="{{Auth()->user()->hasPermissionTo('form_type_name_create')}}">
    <input type="hidden" id="show_dashboard_permission_create" value="{{Auth()->user()->hasPermissionTo('form_type_show_dashboard_create')}}">
    <input type="hidden" id="count_dashboard_permission_create" value="{{Auth()->user()->hasPermissionTo('form_type_count_dashboard_create')}}">
    <input type="hidden" id="count_day_permission_create" value="{{Auth()->user()->hasPermissionTo('form_type_count_day_create')}}">
    <input type="hidden" id="count_response_positive_permission_create" value="{{Auth()->user()->hasPermissionTo('form_type_count_response_positive_create')}}">
    <input type="hidden" id="count_response_negative_permission_create" value="{{Auth()->user()->hasPermissionTo('form_type_count_response_negative_create')}}">
    <input type="hidden" id="outanding_count_permission_create" value="{{Auth()->user()->hasPermissionTo('form_type_outanding_count_create')}}">
    <input type="hidden" id="late_completion_count_permission_create" value="{{Auth()->user()->hasPermissionTo('form_type_late_completion_count_create')}}">
    <input type="hidden" id="detailed_field_stats_permission_create" value="{{Auth()->user()->hasPermissionTo('form_type_detailed_field_stats_create')}}">

    <!-- edit permision -->
    <input type="hidden" id="edit_permission_form_type" value="{{Auth()->user()->hasPermissionTo('edit_form_type')}}">
    
@endif