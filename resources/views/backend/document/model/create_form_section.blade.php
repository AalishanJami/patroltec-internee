@php
    $data=localization();
@endphp
<style>
    .form_name_required_message{
        position: absolute;
        top: 97%;
        left: 4%;
    }
    .valid_width_number_message{
        position: absolute;
        top: 92%;
        left: 0%;
    }
</style>
<div class="modal fade" id="model_create_form_section" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">
                    <span class="assign_class label{{getKeyid('new_dynamic_form',$data)}}" data-id="{{getKeyid('new_dynamic_form',$data) }}" data-value="{{checkKey('new_dynamic_form',$data) }}" >
                        {!! checkKey('new_dynamic_form',$data) !!}
                    </span>
                    @if(Auth()->user()->hasPermissionTo('view_cms') || Auth::user()->all_companies == 1 )
                        <button data-class="edit-disable" class="edit_cms_enable edit_cms_switch btn btn-primary btn-sm waves-effect waves-light" style="display: none;" >
                            Edit cms
                        </button>
                        <button  data-class="edit-enable" class="edit_cms_disable edit_cms_switch btn btn-danger btn-sm waves-effect waves-light">
                            Edit cms
                        </button>
                    @endif
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="add_new_form_section_form">
                <div class="modal-body mx-3">
                    @if(Auth()->user()->hasPermissionTo('documentDynamicForm_form_name_create') || Auth::user()->all_companies == 1 )
                        <div class="md-form mb-5">
                            <i class="fas fa-user prefix grey-text"></i>
                            <input type="text" class="form-control " name="name"  value="" id="form_name_required" autocomplete="name" autofocus>
                            <label  for="name" >
                               <span class="assign_class label{{getKeyid('name',$data)}}" data-id="{{getKeyid('name',$data) }}" data-value="{{checkKey('name',$data) }}" >
                                    {!! checkKey('name',$data) !!}
                                </span>
                            </label>
                            <small class="text-danger error_message_text form_name_required_message" style="left: 3.5% !important;" ></small>
                        </div>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('documentDynamicForm_form_header_create') || Auth::user()->all_companies == 1 )
                        <div class="row">
                            <div class="col-md-6">
                                <div class="md-form mb-5">
                                    <i class="fas fa-heading prefix grey-text"></i>
                                    <input type="text" class="form-control " name="header" value=""  autocomplete="name" autofocus>
                                    <label data-error="wrong" data-success="right" for="name" >
                                        <span class="assign_class label{{getKeyid('header',$data)}}" data-id="{{getKeyid('header',$data) }}" data-value="{{checkKey('header',$data) }}" >
                                            {!! checkKey('header',$data) !!}
                                        </span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="md-form mb-5">
                                    <input type="number" class="form-control form_header_footer_height validate @error('header_height') is-invalid @enderror" name="header_height" id="header_height_create" value="50"  autocomplete="header_height" autofocus>
                                    <label data-error="wrong" data-success="right" for="name" >
                                        <span class="assign_class label{{getKeyid('header_height',$data)}}" data-id="{{getKeyid('header_height',$data) }}" data-value="{{checkKey('header_height',$data) }}" >
                                            {!! checkKey('header_height',$data) !!}
                                        </span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="md-form mb-5">
                                    <div class="form-check">
                                        <input type="checkbox" value="1" onclick="showHideCheked('show_header')" name="show_header" checked class="form-check-input client_checked" id="show_header">
                                        <label class="form-check-label" for="show_header">
                                            <span class="assign_class label{{getKeyid('show_header',$data)}}" data-id="{{getKeyid('show_header',$data) }}" data-value="{{checkKey('show_header',$data) }}" >
                                                {!! checkKey('show_header',$data) !!}
                                            </span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('documentDynamicForm_form_footer_create') || Auth::user()->all_companies == 1 )
                        <div class="row">
                            <div class="col-md-6">
                                <div class="md-form ">
                                    <i class="fas fa-text-width prefix grey-text"></i>
                                    <input type="text" class="form-control  " name="footer" value=""  autocomplete="name" autofocus>
                                    <label data-error="wrong" data-success="right" for="name" >
                                        <span class="assign_class label{{getKeyid('footer',$data)}}" data-id="{{getKeyid('footer',$data) }}" data-value="{{checkKey('footer',$data) }}" >
                                            {!! checkKey('footer',$data) !!}
                                        </span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="md-form ">
                                    <input type="number" class="form-control form_header_footer_height validate @error('footer_height') is-invalid @enderror" name="footer_height" id="footer_height_create" value="50"  autocomplete="footer_height" autofocus>
                                    <label  for="name" >
                                        <span class="assign_class label{{getKeyid('footer_height',$data)}}" data-id="{{getKeyid('footer_height',$data) }}" data-value="{{checkKey('footer_height',$data) }}" >
                                            {!! checkKey('footer_height',$data) !!}
                                        </span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="md-form">
                                    <div class="form-check">
                                        <input type="checkbox" name="show_footer" onclick="showHideCheked('show_footer')" checked value="1" class="form-check-input show_footer_checked" id="show_footer">
                                        <label class="form-check-label" for="show_footer">
                                            <span class="assign_class label{{getKeyid('show_footer',$data)}}" data-id="{{getKeyid('show_footer',$data) }}" data-value="{{checkKey('show_footer',$data) }}" >
                                                {!! checkKey('show_footer',$data) !!}
                                            </span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="row">
                        <div class="md-form md_form_margin">
                            <div class="form-check">
                                <input type="checkbox" name="is_repeatable" onclick="showHideCheked('is_repeatable')" value="1" class="form-check-input is_repeatable_checked" id="is_repeatable_create">
                                <label class="form-check-label" for="is_repeatable_create">
                                    <span class="assign_class label{{getKeyid('is_repeatable',$data)}}" data-id="{{getKeyid('is_repeatable',$data) }}" data-value="{{checkKey('is_repeatable',$data) }}" >
                                        {!! checkKey('is_repeatable',$data) !!}
                                    </span>
                                </label>
                            </div>
                        </div>
                    </div>
                     <div class="row">
                        <div class="md-form md_form_margin">
                            <div class="form-check">
                                <input type="checkbox" name="is_take_pic" onclick="showHideCheked('is_take_pic')" value="1" class="form-check-input is_take_pic_checked" id="is_take_pic_create">
                                <label class="form-check-label" for="is_take_pic_create">
                                    <span class="assign_class label{{getKeyid('is_take_pic',$data)}}" data-id="{{getKeyid('is_take_pic',$data) }}" data-value="{{checkKey('is_take_pic',$data) }}" >
                                        {!! checkKey('is_take_pic',$data) !!}
                                    </span>
                                </label>
                            </div>
                        </div>
                    </div>
                     <div class="row">
                        <div class="md-form md_form_margin">
                            <div class="form-check">
                                <input type="checkbox" name="is_select_pic" onclick="showHideCheked('is_select_pic')" value="1" class="form-check-input is_select_pic_checked" id="is_select_pic_create">
                                <label class="form-check-label" for="is_select_pic_create">
                                    <span class="assign_class label{{getKeyid('is_select_pic',$data)}}" data-id="{{getKeyid('is_select_pic',$data) }}" data-value="{{checkKey('is_select_pic',$data) }}" >
                                        {!! checkKey('is_select_pic',$data) !!}
                                    </span>
                                </label>
                            </div>
                        </div>
                    </div>
                     <div class="row">
                        <div class="md-form md_form_margin">
                            <div class="form-check">
                                <input type="checkbox" name="is_select_doc" onclick="showHideCheked('is_select_doc')" value="1" class="form-check-input is_select_doc_checked" id="is_select_doc_create">
                                <label class="form-check-label" for="is_select_doc_create">
                                    <span class="assign_class label{{getKeyid('is_select_doc',$data)}}" data-id="{{getKeyid('is_select_doc',$data) }}" data-value="{{checkKey('is_select_doc',$data) }}" >
                                        {!! checkKey('is_select_doc',$data) !!}
                                    </span>
                                </label>
                            </div>
                        </div>
                    </div>
                     <div class="row">
                        <div class="md-form md_form_margin">
                            <div class="form-check">
                                <input type="checkbox" name="is_notes" onclick="showHideCheked('is_notes')" value="1" class="form-check-input is_notes_checked" id="is_notes_create">
                                <label class="form-check-label" for="is_notes_create">
                                    <span class="assign_class label{{getKeyid('is_notes',$data)}}" data-id="{{getKeyid('is_notes',$data) }}" data-value="{{checkKey('is_notes',$data) }}" >
                                        {!! checkKey('is_notes',$data) !!}
                                    </span>
                                </label>
                            </div>
                        </div>
                    </div>
                    @if(Auth()->user()->hasPermissionTo('documentDynamicForm_form_formwidth_create') || Auth::user()->all_companies == 1 )
                        <div class="row">
                            <div class="col-sm-1" style="padding-right: 0px !important;margin-right: 0px !important; max-width: 3.333333% !important;">
                                <i class="fas fa-arrows-alt-h prefix grey-text" style="font-size: 25px !important;"></i>
                            </div>
                            <div class="col-sm-11">
                                <div class="md-form" style="margin-top: 0rem !important;">
                                    <select searchable="Search here.."   class="mdb-select colorful-select show_hide_section_column dropdown-primary" style="margin-left: auto !important;width:96%!important;"  name="width" id="valid_width_number">
                                        <option value="" selected disabled>Width</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                    </select>
                                    <small class="text-danger error_message_text valid_width_number_message"></small>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="row form_section_show_hide_column" >
                        <div class="col-sm-1" style="padding-right: 0px !important;margin-right: 0px !important; max-width: 3.333333% !important;">
                            <i class="fas fa-arrows-alt-h prefix grey-text" style="font-size: 25px !important;"></i>
                        </div>
                        <div class="col-sm-11">
                            <div class="md-form" style="margin-top: 0rem !important;">
                                <select searchable="Search here.."   class=" mdb-select colorful-select dropdown-primary" style="margin-left: auto !important;width:96%!important;"  name="form_section_column" id="form_section_column" >
                                    <option value="" selected disabled>Column Size</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-1" style="padding-right: 0px !important;margin-right: 0px !important; max-width: 3.333333% !important;">
                            <i class="fas fa-arrows-alt-v prefix grey-text" style="font-size: 25px !important;"></i>
                        </div>
                        <div class="col-sm-11">
                            <div class="md-form" id="set_create_height" style="margin-top: 0rem !important;"></div>
                            <input type="hidden" id="create_set_formsection_height" name="form_sections_height">
                        </div>
                    </div>
                        <input type="hidden" class="form-control " name="signature"  value="" id="signature" autocomplete="signature" autofocus>

                    {{--                    <div class="md-form mb-5">--}}
{{--                        <i class=" fas fa-pencil prefix grey-text"></i>--}}
{{--                        <input type="text" class="form-control " name="signature"  value="" id="signature" autocomplete="signature" autofocus>--}}
{{--                        <label  for="signature" >--}}
{{--                            <span class="assign_class label{{getKeyid('signature',$data)}}" data-id="{{getKeyid('signature',$data) }}" data-value="{{checkKey('signature',$data) }}" >--}}
{{--                                {!! checkKey('signature',$data) !!}--}}
{{--                            </span>--}}
{{--                        </label>--}}
{{--                    </div>--}}


                <!--      <div class="md-form mb-5">
                         <i class="fas fa-border-none prefix grey-text"></i>
                        <input type="text" class="form-control " name="border_style" value="" required autocomplete="name" autofocus>
                        <label data-error="wrong" data-success="right" for="name" >
                            Border Style
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                            data-content="tool tip for User Group"></i>
                        </label>
                    </div> -->
                    <!--  <p>Show on Dashboard</p>

                   <div class="form-check">
                     <input type="radio" class="form-check-input" id="dashboard1" value="1" name="dashboard">
                     <label class="form-check-label" for="dashboard1">YES</label>
                   </div>

                   <div class="form-check">
                     <input type="radio" class="form-check-input" id="dashboard0" name="dashboard" value="0" checked>
                     <label class="form-check-label" for="dashboard0">NO</label>
                   </div> -->
                    <br>
                    <div class="modal-footer d-flex justify-content-center">
                        @if(Auth()->user()->hasPermissionTo('form_create_documentDynamicForm') || Auth::user()->all_companies == 1 )
                            <button class="form_submit_check btn btn-primary btn-sm" type="submit" id="button_create_form_id">
                                <span class="assign_class label{{getKeyid('save',$data)}}" data-id="{{getKeyid('save',$data) }}" data-value="{{checkKey('save',$data) }}" >
                                    {!! checkKey('save',$data) !!}
                                </span>
                            </button>
                        @endif
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

