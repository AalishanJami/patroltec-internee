 @php
        $data=localization();
@endphp
<style type="text/css">
   .form-check-input {
    position: absolute !important;
    pointer-events: none !important;
    opacity: 0!important;
}
 @media (min-width: 576px) {
      .modal-dialog {
        max-width: 75% !important;
      }
    }


   #populate_table>tbody>tr>td:nth-child(2){
       width: 7%;
       text-align: center;
   }
   #populate_table>tbody>tr>td:nth-child(1){
       width: 7%;
   }

   #populate_table>tbody>tr>td:nth-child(4){
       width: 7%;
       text-align: center;
   }
    .selected {
    background-color: #B0BED9 !important;
  }
</style>
<div class="modal fade" id="modalPopulate" tabindex="-1" data-keyboard="false" data-backdrop="static" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold"><span> Location</span></h4>
                <button type="button" class="close division_listing_close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="card">
              <div class="card-body">
                  <div id="table-populate" class="table-editable">
                    <span class="add-populate table-add float-right mb-3 mr-2"><a class="text-success"><i
                    class="fas fa-plus fa-2x" aria-hidden="true"></i></a></span>
                    <div id="table" class="table-editable">

                      <table id="populate_table" class=" table table-bordered table-responsive-md table-striped">
                        <thead>
                          <tr>
                              <th class="no-sort"></th>
                              <th id="action" class="no-sort"></th>
                              <th> Name</th>
                              <th class="no-sort"></th>
                          </tr>
                        </thead>
                        <tbody id="populate_data">
                        </tbody>
                      </table>
                    </div>

                  </div>
              </div>
            </div>


        </div>
    </div>
</div>

<script type="text/javascript">

 function populate_data(url) {
          $.ajax({
            type: 'GET',
            url: url,
            success: function(response)
            {
                var html='';
                for (var i = 0; i < response.data.length; i++) {
                    console.log(response.data[i].id);
                    html+= '<tr id="populate_tr'+response.data[i].id+'" class="selectedrowpopulate"  role="row" class="odd" >';
                    html+='<td><div class="form-check"><input type="checkbox" class="form-check-input" id="populate'+response.data[i].id+'"><label class="form-check-label" for="populate'+response.data[i].id+'""></label></div></td>';
                    if(response.flag == 1)
                    {
                        html+='<td class="sorting_1"><a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light" onclick="deletepopulatedata('+response.data[i].id+')"><i class="fas fa-trash"></i></a></td>';
                    }
                    else
                    {
                        html+='<td class="sorting_1"><a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light" onclick="deletesoftpopulatedata('+response.data[i].id+')"><i class="fas fa-trash"></i></a><a type="button" class="btn btn-success btn-xs my-0 waves-effect waves-light" onclick="restorepopulatedata('+response.data[i].id+')"><i class="fa fa-retweet" aria-hidden="true"></i></a></td>';
                    }
                    // '+parseInt(response.data[i].month)+'
                    html+='<td class="pt-3-half edit_inline_populate" id="nameD'+response.data[i].id+'"   contenteditable="true">'+response.data[i].name+'</td>';
                    html+='<td class="pt-3-half"><a type="button" class="btn btn-primary btn-xs my-0 waves-effect waves-light savepopulatedata " id="'+response.data[i].id+'"><i class="fas fa-check"></i></a></td>';
                    html+='</tr>';

                }

                $('#populate_data').empty();
                $('#populate_data').html(html);


                   var html='';

              html+='<select class="mdb-select" name="pre_populate_id" id="pre_populate_id" >';

                for (var key in response.pre_populate) {
                     if (response.pre_populate.hasOwnProperty(key))
                        html+='<option  value="'+key+'">'+response.pre_populate[key]+'</option>';
                }

                html+='</select> ';
                $('#pre_populate_listing').empty();
                console.log(html, "html");
                $('#pre_populate_listing').html(html);
                // document.getElementById('pre_populate_listing').innerHTML=html;
                $('#pre_populate_id').materialSelect();


            },
            error: function (error) {
                console.log(error);
            }
        });
        setTimeout(datatable_intialize_papulate, 5000);
    }
 function datatable_intialize_papulate() {

     $('#populate_table').dataTable({
         processing: true,
         language: {
             'lengthMenu': '_MENU_',
             'info': '',
         },

     });
 }
    var url='/document/getAllPopulate';
    populate_data(url);

</script>
