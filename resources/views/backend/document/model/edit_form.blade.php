@php
    $data=localization();
@endphp
<style type="text/css">
    .ml-4-5 {
        margin-left: 2.5rem !important;
    }
</style>

<div class="modal fade" id="modelEditDynamicForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">
                    <span class="assign_class label{{getKeyid('new_dynamic_form',$data)}}" data-id="{{getKeyid('new_dynamic_form',$data) }}" data-value="{{checkKey('new_dynamic_form',$data) }}" >
                        {!! checkKey('new_dynamic_form',$data) !!}
                    </span>
                    @if(Auth()->user()->hasPermissionTo('view_cms') || Auth::user()->all_companies == 1 )
                        <button data-class="edit-disable" class="edit_cms_enable edit_cms_switch btn btn-primary btn-sm waves-effect waves-light" style="display: none;" >
                            Edit cms
                        </button>
                        <button  data-class="edit-enable" class="edit_cms_disable edit_cms_switch btn btn-danger btn-sm waves-effect waves-light">
                            Edit cms
                        </button>
                    @endif
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="edit_form_section_form" >
                <input type="hidden" name="id" id="id_edit">
                <div class="modal-body mx-3">
                    @if(Auth()->user()->hasPermissionTo('documentDynamicForm_form_name_edit') || Auth::user()->all_companies == 1 )                 <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input type="text" class="form-control validate @error('name') is-invalid @enderror" id="name_edit" name="name" value="" required autocomplete="name" autofocus>
                        <label data-error="wrong" class="active" data-success="right" for="name" >
                            <span class="assign_class label{{getKeyid('name',$data)}}" data-id="{{getKeyid('name',$data) }}" data-value="{{checkKey('name',$data) }}" >
                                {!! checkKey('name',$data) !!}
                            </span>
                        </label>
                        <small class="text-danger error_message_text form_name_required_message" style="left: 3.5% !important;"></small>
                    </div>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('documentDynamicForm_form_header_edit') || Auth::user()->all_companies == 1 )
                        <div class="row">
                            <div class="col-md-6">
                                <div class="md-form mb-5">
                                    <i class="fas fa-heading prefix grey-text"></i>
                                    <input type="text" class="form-control validate @error('name') is-invalid @enderror" id="header_edit" name="header" value="" required autocomplete="name" autofocus>
                                    <label data-error="wrong" class="active" data-success="right" for="name" >
                                            <span class="assign_class label{{getKeyid('header',$data)}}" data-id="{{getKeyid('header',$data) }}" data-value="{{checkKey('header',$data) }}" >
                                                {!! checkKey('header',$data) !!}
                                            </span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="md-form mb-5">
                                    <input type="number" class="form-control form_header_footer_height validate @error('header_height') is-invalid @enderror" name="header_height" value="50" id="header_height_edit"  autocomplete="header_height" autofocus>
                                    <label data-error="wrong" data-success="right" for="name" >
                                        <span class="assign_class label{{getKeyid('header_height',$data)}}" data-id="{{getKeyid('header_height',$data) }}" data-value="{{checkKey('header_height',$data) }}" >
                                            {!! checkKey('header_height',$data) !!}
                                        </span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="md-form mb-5">
                                    <div class="form-check">
                                        <input type="checkbox"  onclick="showHideCheked('show_header_edit')" name="show_header"  class="form-check-input client_checked" id="show_header_edit">
                                        <label class="form-check-label" for="show_header_edit">
                                            <span class="assign_class label{{getKeyid('show_header',$data)}}" data-id="{{getKeyid('show_header',$data) }}" data-value="{{checkKey('show_header',$data) }}" >
                                                {!! checkKey('show_header',$data) !!}
                                            </span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('documentDynamicForm_form_footer_edit') || Auth::user()->all_companies == 1 )
                        <div class="row">
                            <div class="col-md-6">
                                <div class="md-form mb-5">
                                    <i class="fas fa-text-width prefix grey-text"></i>
                                    <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="footer" id="footer_edit" value="" required autocomplete="name" autofocus>
                                    <label data-error="wrong" class="active" data-success="right" for="name" >
                                            <span class="assign_class label{{getKeyid('footer',$data)}}" data-id="{{getKeyid('footer',$data) }}" data-value="{{checkKey('footer',$data) }}" >
                                                {!! checkKey('footer',$data) !!}
                                            </span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="md-form mb-5">
                                    <input type="number" class="form-control form_header_footer_height validate @error('footer_height') is-invalid @enderror" name="footer_height" value="50" id="footer_height_edit"  autocomplete="footer_height" autofocus>
                                    <label data-error="wrong" data-success="right" for="name" >
                                        <span class="assign_class label{{getKeyid('footer_height',$data)}}" data-id="{{getKeyid('footer_height',$data) }}" data-value="{{checkKey('footer_height',$data) }}" >
                                            {!! checkKey('footer_height',$data) !!}
                                        </span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="md-form mb-5">
                                    <div class="form-check">
                                        <input type="checkbox" name="show_footer" onclick="showHideCheked('show_footer_edit')" class="form-check-input show_footer_checked" id="show_footer_edit">
                                        <label class="form-check-label" for="show_footer_edit">
                                            <span class="assign_class label{{getKeyid('show_footer',$data)}}" data-id="{{getKeyid('show_footer',$data) }}" data-value="{{checkKey('show_footer',$data) }}" >
                                                {!! checkKey('show_footer',$data) !!}
                                            </span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                     <div class="row">
                        <div class="md-form mb-5">
                            <div class="form-check">
                                <input type="checkbox" name="is_repeatable" onclick="showHideCheked('is_repeatable_edit')" checked value="1" class="form-check-input is_repeatable_checked" id="is_repeatable_edit">
                                <label class="form-check-label" for="is_repeatable_edit">
                                    <span class="assign_class label{{getKeyid('is_repeatable',$data)}}" data-id="{{getKeyid('is_repeatable',$data) }}" data-value="{{checkKey('is_repeatable',$data) }}" >
                                        {!! checkKey('is_repeatable',$data) !!}
                                    </span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="md-form mb-5">
                            <div class="form-check">
                                <input type="checkbox" name="is_take_pic" onclick="showHideCheked('is_take_pic_edit')" checked value="1" class="form-check-input is_take_pic_checked" id="is_take_pic_edit">
                                <label class="form-check-label" for="is_take_pic_edit">
                                    <span class="assign_class label{{getKeyid('is_take_pic',$data)}}" data-id="{{getKeyid('is_take_pic',$data) }}" data-value="{{checkKey('is_take_pic',$data) }}" >
                                        {!! checkKey('is_take_pic',$data) !!}
                                    </span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="md-form mb-5">
                            <div class="form-check">
                                <input type="checkbox" name="is_select_doc" onclick="showHideCheked('is_select_doc_edit')" checked value="1" class="form-check-input is_select_doc_checked" id="is_select_doc_edit">
                                <label class="form-check-label" for="is_select_doc_edit">
                                    <span class="assign_class label{{getKeyid('is_select_doc',$data)}}" data-id="{{getKeyid('is_select_doc',$data) }}" data-value="{{checkKey('is_select_doc',$data) }}" >
                                        {!! checkKey('is_select_doc',$data) !!}
                                    </span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="md-form mb-5">
                            <div class="form-check">
                                <input type="checkbox" name="is_select_pic" onclick="showHideCheked('is_select_pic_edit')" checked value="1" class="form-check-input is_select_pic_checked" id="is_select_pic_edit">
                                <label class="form-check-label" for="is_select_pic_edit">
                                    <span class="assign_class label{{getKeyid('is_select_pic',$data)}}" data-id="{{getKeyid('is_select_pic',$data) }}" data-value="{{checkKey('is_select_pic',$data) }}" >
                                        {!! checkKey('is_select_pic',$data) !!}
                                    </span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="md-form mb-5">
                            <div class="form-check">
                                <input type="checkbox" name="is_notes" onclick="showHideCheked('is_notes_edit')" checked value="1" class="form-check-input is_notes_checked" id="is_notes_edit">
                                <label class="form-check-label" for="is_notes_edit">
                                    <span class="assign_class label{{getKeyid('is_notes',$data)}}" data-id="{{getKeyid('is_notes',$data) }}" data-value="{{checkKey('is_notes',$data) }}" >
                                        {!! checkKey('is_notes',$data) !!}
                                    </span>
                                </label>
                            </div>
                        </div>
                    </div>

                    @if(Auth()->user()->hasPermissionTo('documentDynamicForm_form_formwidth_edit') || Auth::user()->all_companies == 1 )
                        <div class="row">
                            <div class="col-sm-1" style="padding-right: 0px !important;margin-right: 0px !important; max-width: 3.333333% !important;">
                                <i class="fas fa-arrows-alt-h prefix grey-text" style="font-size: 25px !important;"></i>
                            </div>
                            <div class="col-sm-11">
                                <div class="md-form width_edit" style="margin-top: 0rem !important;">
                                    <select searchable="Search here.."   class="show_hide_section_column mdb-select colorful-select dropdown-primary" style="margin-left: auto !important;width:96%!important;"  name="width" id="width_edit">
                                        <option value="">Width</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                    </select>
                                    <small class="text-danger error_message_text valid_width_number_message"></small>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="row form_section_show_hide_column">
                        <div class="col-sm-1" style="padding-right: 0px !important;margin-right: 0px !important; max-width: 3.333333% !important;">
                            <i class="fas fa-arrows-alt-h prefix grey-text" style="font-size: 25px !important;"></i>
                        </div>
                        <div class="col-sm-11">
                            <div class="md-form" style="margin-top: 0rem !important;" id="form_section_column_edit">
                                <select searchable="Search here.."   class="colorful-select form_section_column dropdown-primary" style="margin-left: auto !important;width:96%!important;"  name="form_section_column" id="form_section_column_edit">

                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-1" style="padding-right: 0px !important;margin-right: 0px !important; max-width: 3.333333% !important;">
                            <i class="fas fa-arrows-alt-h prefix grey-text" style="font-size: 25px !important;"></i>
                        </div>
                        <div class="col-sm-11">
                            <div class="md-form" id="set_edit_height" style="margin-top: 0rem !important;"></div>
                            <input type="hidden" id="set_formsection_height" name="form_sections_height">
                        </div>
                    </div>
{{--                    @if(Auth()->user()->hasPermissionTo('documentDynamicForm_form_border_edit') || Auth::user()->all_companies == 1 )--}}
{{--                        <div class="md-form mb-5">--}}
{{--                            <i class="fas fa-border-none prefix grey-text"></i>--}}
{{--                            <input type="text" class="form-control validate @error('name') is-invalid @enderror" id="border_style_edit" name="border_style" value="" required autocomplete="name" autofocus>--}}
{{--                            <label data-error="wrong" class="active" data-success="right" for="name" >--}}
{{--                                <span class="assign_class label{{getKeyid('border_style',$data)}}" data-id="{{getKeyid('border_style',$data) }}" data-value="{{checkKey('border_style',$data) }}" >--}}
{{--                                    {!! checkKey('border_style',$data) !!}--}}
{{--                                </span>--}}
{{--                            </label>--}}
{{--                        </div>--}}
{{--                    @endif--}}

                        <input type="hidden" class="form-control validate @error('name') is-invalid @enderror" name="signature"  value="" id="signature_edit" autocomplete="signature" autofocus>
{{--                        <div class="md-form mb-5">--}}
{{--                            <i class=" fas fa-pencil prefix grey-text"></i>--}}
{{--                            <label  for="signature" >--}}
{{--                            <span class="assign_class label{{getKeyid('signature',$data)}}" data-id="{{getKeyid('signature',$data) }}" data-value="{{checkKey('signature',$data) }}" >--}}
{{--                                {!! checkKey('signature',$data) !!}--}}
{{--                            </span>--}}
{{--                            </label>--}}
{{--                        </div>--}}

                <!--      <p>Show on Dashboard</p>

                         <div class="form-check">
                           <input type="radio" class="form-check-input" id="dashboardedit1" value="1" name="dashboard">
                           <label class="form-check-label" for="dashboardedit1">YES</label>
                         </div>


                         <div class="form-check">
                           <input type="radio" class="form-check-input" id="dashboardedit0" name="dashboard" value="0" checked>
                           <label class="form-check-label" for="dashboardedit0">NO</label>
                         </div> -->

                    <br>
                    <div class="modal-footer d-flex justify-content-center">
                        @if(Auth()->user()->hasPermissionTo('form_edit_documentDynamicForm') || Auth::user()->all_companies == 1 )
                            <button class="form_submit_check btn btn-primary btn-sm" type="submit" id="button_edit_form_id">
                            <span class="assign_class label{{getKeyid('update',$data)}}" data-id="{{getKeyid('update',$data) }}" data-value="{{checkKey('update',$data) }}" >
                                {!! checkKey('update',$data) !!}
                            </span>
                            </button>
                        @endif
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        $( "#numberBox" ).change(function() {
            var max = parseInt($(this).attr('max'));
            var min = parseInt($(this).attr('min'));
            if ($(this).val() > max)
            {
                $(this).val(max);
            }
            else if ($(this).val() < min)
            {
                $(this).val(min);
            }
        });
    });
</script>

