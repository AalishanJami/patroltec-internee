@include('backend.layouts.pdf_start')
    <thead>
    <tr>
        @if(Auth()->user()->hasPermissionTo('documentlibrary_name_pdf_export') || Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout"> Name</th>
        @else
            <th style="vertical-align: text-top;" class="pdf_table_layout"></th>
        @endif
        @if(Auth()->user()->hasPermissionTo('documentlibrary_ref_pdf_export') || Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout"> Ref</th>
        @else
            <th style="vertical-align: text-top;" class="pdf_table_layout"></th>
        @endif
    </tr>
    </thead>
    <tbody>
    @foreach($data as $value)
        <tr>
            @if(Auth()->user()->hasPermissionTo('documentlibrary_name_pdf_export') || Auth::user()->all_companies == 1 )
                <td class="pdf_table_layout"><p class="col_text_style">{{ $value->name }}</p></td>
            @else
                <td class="pdf_table_layout"><p class="col_text_style"></p></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('documentlibrary_ref_pdf_export') || Auth::user()->all_companies == 1 )
                <td class="pdf_table_layout"><p class="col_text_style">{{ $value->ref }}</p></td>
            @else
                <td class="pdf_table_layout"><p class="col_text_style"></p></td>
            @endif
        </tr>
    @endforeach
    </tbody>
@include('backend.layouts.pdf_end')
