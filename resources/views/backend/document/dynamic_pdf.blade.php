<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Document</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap.min.css"  media="all" rel="stylesheet">
    <style type="text/css"  media="all">
           .mt-5
           {
            margin-top: 20px;
           }
           .md-5
           {
            margin-bottom: 20px;
           }
           .mt-10
           {
            margin-top: 40px;
           }
           .mt-2
           {
               margin-top: 5px;
           }
    </style>
</head>
<body>
@foreach ($pdf_data as $key => $value) 
<div id="{{$value->order}}-{{$value->id}}" class=" sortable-card col-md-{{$value->width}}">
  <div class="mb-4 custom_dynamic_height">
    <div class="card text-center ">
      <div class="card-header text-muted text-left md-5">
            <span class="white-color" style="flex: 1; text-align:left;padding-bottom: 40px">{{$value->header}}</span>
      </div>
      <div class="card-body mt-5">
        @foreach($value->questions as $question)       
          <div class="overflow-auto ">
            <div class="card card-cascade wider reverse">
              <p class="card-title text-left">
                <strong>{{($question->question)}}</strong>
              </p>
              <div class="row text-center mt-5">
                @switch ($question->answer_type->name)  
                  @case ('dropdown')
                      <div class="col-md-4 ml-2 mt-5">
                        <select searchable="Search here.."  class="mdb-select_forms mt-5"  >
                          @if($question->answer_group)
                            @foreach($question->answer_group->answer as $answer)
                              @if($answer->anwser)
                                <option value="{{$answer->id}}">{{$answer->answer}}</option>
                              @endif
                            @endforeach  
                          @endif
                        </select>
                      </div>
                  @break
                  @case ('radio') 
                    <div class="col-md-12 mt-5">
                      @if($question->answer_group)
                        @foreach($question->answer_group->answer as $answer)
                          <div class="form-check float-left mt-5">
                             <input type="radio" class="form-check-input" id="answer_group{{$answer->id}}" name="answer_group">
                             <label class="form-check-label" for="answer_group{{$answer->id}}">{{$answer->answer}}</label>
                          </div>
                        @endforeach  
                      @endif
                    </div>
                  @break
                  @case ('multi') 
                    <div class="col-md-4 ml-2 mt-5">
                        <select searchable="Search here.."  class="mdb-select_forms" multiple  >
                          @if($question->answer_group)
                            @foreach($question->answer_group->answer as $answer)
                              @if($answer->anwser)
                                <option value="{{$answer->id}}">{{$answer->answer}}</option>
                              @endif
                            @endforeach 
                          @endif
                        </select>
                    </div>
                  @break
                  @case ('date')
                    <div class="col-md-12 mt-5">
                      @if($question->answer_group)
                        <div class="md-form mt-5">
                          <input type="date" placeholder="Date" id="date-picker-form{{$question->answer_group->answer[0]->id}}" name="date" class="form-control ">
                        </div>
                      @endif
                    </div>
                  @break
                  @case ('time')
                    <div class="col-md-12 mt-5">
                      @if($question->answer_group)
                        <div class="md-form mt-5">
                          <input type="time" placeholder="Time" id="date-picker-form{{$question->answer_group->answer[0]->id}}" name="start_date" class="form-control ">
                        </div>     
                      @endif
                    </div>
                  @break
                  @case ('datetime')
                    <div class="col-md-12 mt-5">
                      @if($question->answer_group)
                        <div class="md-form mt-5">
                          <input type="date" placeholder="Date" id="date-picker-form{{$question->answer_group->answer[0]->id}}" name="date" class="form-control ">
                        </div>
                      @endif
                    </div>
                  @break
                  @case ('text')
                      <div class="col-md-12 mt-5">
                        @if($question->answer_group)
                          <div class="md-form mt-5">
                            <input  type="text" placeholder="Text" id="time-picker-form{{$question->answer_group->answer[0]->id}}"  name="time" class="input_starttime form-control ">
                          </div>
                        @endif
                      </div>
                  @break
                @endswitch

              </div>
            </div>
          </div>
        @endforeach()
          
      </div>
    </div>
    <div class="card-footer text-muted text-left">
            {{$value->footer}}
    </div>
  </div>
</div>   
@endforeach()
</body>
</html>
