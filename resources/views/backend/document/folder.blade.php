@extends('backend.layouts.doc')
@section('content')
    @php
       $data=localization();
    @endphp
    <div class="row">
        @include('backend.layouts.doc_sidebar')
        <div class="padding-left-custom remove-padding">
            {{ Breadcrumbs::render('document/folder') }}
            <!-- Editable table -->
            <div class="card">
              <div class="card-body">
                <div id="table" class="table-editable">
                   <span class="table-add float-right mb-3 mr-2"><a href="#!" class="text-success"><i
                        class="fas fa-plus fa-2x" aria-hidden="true"></i></a></span>

                  <table class="company_table_static table table-bordered table-responsive-md table-striped text-center">
                    <thead>
                      <tr>
                        <th class="text-center">Folder Name</th>
                        <th class="text-center">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td class="pt-3-half" contenteditable="true">Folder 1</td>
                       <td>
                            <span><a href="{{url('documents/folder/1')}}" type="button"
                              class="btn btn-primary btn-rounded btn-sm my-0">View</a>
                            </span>


                          <span class="table-remove"><button type="button"
                              class="btn btn-danger btn-rounded btn-sm my-0">Remove</button></span>
                        </td>
                      </tr>
                      <!-- This is our clonable table line -->
                      <tr>
                        <td class="pt-3-half" contenteditable="true">Folder 2</td>
                       <td>
                            <span><a href="{{url('documents/folder/2')}}" type="button"
                              class="btn btn-primary btn-rounded btn-sm my-0">View</a>
                            </span>

                          <span class="table-remove"><button type="button"
                              class="btn btn-danger btn-rounded btn-sm my-0">Remove</button></span>
                        </td>
                      </tr>
                      <!-- This is our clonable table line -->
                      <tr>
                       <td class="pt-3-half" contenteditable="true">Folder 3</td>
                       <td>
                            <span><a  href="{{url('documents/folder/3')}}" type="button"
                              class="btn btn-primary btn-rounded btn-sm my-0">View</a>
                            </span>

                          <span class="table-remove"><button type="button"
                              class="btn btn-danger btn-rounded btn-sm my-0">Remove</button></span>
                        </td>
                      </tr>
                      <!-- This is our clonable table line -->
                      <tr class="hide">
                        <td class="pt-3-half" contenteditable="true">Folder 4</td>
                       <td>
                            <span><a  href="{{url('documents/folder/4')}}" type="button"
                              class="btn btn-primary btn-rounded btn-sm my-0">View</a>
                            </span>

                          <span class="table-remove"><button type="button"
                              class="btn btn-danger btn-rounded btn-sm my-0">Remove</button></span>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
<!-- Editable table -->
            @component('backend.clients.notes.model.create')
            @endcomponent

             {{--    Edit Modal--}}
            @component('backend.clients.notes.model.edit')
            @endcomponent
            {{--    END Edit Modal--}}
        </div>
    </div>



<script type="text/javascript">
function deletenotesdata()
{

    swal({
        title:'Are you sure?',
        text: "Delete Record.",
        type: "error",
        showCloseButton: true,
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, Delete!",
        cancelButtonText: "Cancel!",
        closeOnConfirm: false,
        customClass: "confirm_class",
        closeOnCancel: false,
    },
    function(isConfirm){
        swal.close();
 });


}

const $tableID = $('#table');
 const $BTN = $('#export-btn');
 const $EXPORT = $('#export');

 const newTr = `
<tr class="hide">
  <td class="pt-3-half" contenteditable="true">Example</td>
  <td class="pt-3-half" contenteditable="true">Example</td>
  <td class="pt-3-half" contenteditable="true">Example</td>
  <td class="pt-3-half" contenteditable="true">Example</td>
  <td class="pt-3-half" contenteditable="true">Example</td>
  <td class="pt-3-half">
    <span class="table-up"><a href="#!" class="indigo-text"><i class="fas fa-long-arrow-alt-up" aria-hidden="true"></i></a></span>
    <span class="table-down"><a href="#!" class="indigo-text"><i class="fas fa-long-arrow-alt-down" aria-hidden="true"></i></a></span>
  </td>
  <td>
    <span class="table-remove"><button type="button" class="btn btn-danger btn-rounded btn-sm my-0 waves-effect waves-light">Remove</button></span>
  </td>
</tr>`;

 $('.table-add').on('click', 'i', () => {

   const $clone = $tableID.find('tbody tr').last().clone(true).removeClass('hide table-line');

   if ($tableID.find('tbody tr').length === 0) {

     $('tbody').append(newTr);
   }

   $tableID.find('table').append($clone);
 });

 $tableID.on('click', '.table-remove', function () {

   $(this).parents('tr').detach();
 });

 $tableID.on('click', '.table-up', function () {

   const $row = $(this).parents('tr');

   if ($row.index() === 1) {
     return;
   }

   $row.prev().before($row.get(0));
 });

 $tableID.on('click', '.table-down', function () {

   const $row = $(this).parents('tr');
   $row.next().after($row.get(0));
 });

 // A few jQuery helpers for exporting only
 jQuery.fn.pop = [].pop;
 jQuery.fn.shift = [].shift;

 $BTN.on('click', () => {

   const $rows = $tableID.find('tr:not(:hidden)');
   const headers = [];
   const data = [];

   // Get the headers (add special header logic here)
   $($rows.shift()).find('th:not(:empty)').each(function () {

     headers.push($(this).text().toLowerCase());
   });

   // Turn all existing rows into a loopable array
   $rows.each(function () {
     const $td = $(this).find('td');
     const h = {};

     // Use the headers from earlier to name our hash keys
     headers.forEach((header, i) => {

       h[header] = $td.eq(i).text();
     });

     data.push(h);
   });

   // Output the result
   $EXPORT.text(JSON.stringify(data));
 });

</script>

@endsection
