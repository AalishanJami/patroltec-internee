<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
<table>
    <thead>
    <tr>
        @if(Auth()->user()->hasPermissionTo('documentlibrary_name_excel_export') || Auth::user()->all_companies == 1 )
            <th> Name</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('documentlibrary_ref_excel_export') || Auth::user()->all_companies == 1 )
            <th> Ref</th>
        @endif
    </tr>
    </thead>
    <tbody>
    @foreach($data as $value)
        <tr>
            @if(Auth()->user()->hasPermissionTo('documentlibrary_name_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{ $value->name }}</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('documentlibrary_ref_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{ $value->ref }}</td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>
</body>
</html>
