@extends('backend.layouts.doc')
@section('title', 'Document Library')
@section('content')
    @php
        $data=localization();
    @endphp

    <link rel="stylesheet" href="{{ asset('custom/css/custom_style.css') }}">

    <ol class="breadcrumb">
        @foreach ($breadcrum as $key => $value)
            @if($key == 0)
                <li>
                    @if(isset($check[count($breadcrum)-2]))
                        <a href="{{url('form_group/folder/').'/'.$check[count($breadcrum)-1]}}" class="back">
                    @else
                        <a href="{{url('form_group/')}}" class="back">
                    @endif
                            <i class="fas fa-arrow-left"></i>
                        </a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{url('form_group')}}">
                        <span class="assign_class label{{getKeyid('documents',$data)}}" data-id="{{getKeyid('documents',$data) }}" data-value="{{checkKey('documents',$data) }}" >
                            {!! checkKey('documents',$data) !!}
                        </span>
                    </a>
                </li>
            @endif
            @if(count($breadcrum)-1 == $key)
                <li class="breadcrumb-item active_custom ">{!! $value !!}</li>
            @else
                <li class="breadcrumb-item custom_breadcrum  "><a href="{{url('form_group/folder/').'/'.$check[$key+1]}}">{!! $value !!}</a></li>
            @endif
        @endforeach()
    </ol>
    <div class="card">
        <h3 class="card-header text-left py-2 color-white ">{!! $folder_name !!}</h3>
        <div class="card-body">
            <div id="table-form_group" class="table-editable">
                @if(Auth()->user()->hasPermissionTo('create_documentfolder') || Auth::user()->all_companies == 1 )
                    <span class="add-form_group float-right mb-3 mr-2">
                    <a class="text-success">
                        <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
                    </a>
                </span>
                @endif
                <table border="0" id="document_folder" class="table table-responsive-md table-striped text-center" id="myTable">
                    <thead>
                    @if(Auth()->user()->hasPermissionTo('documentfolder_name') || Auth::user()->all_companies == 1 )
                        <th class="text-left">
                            <span class="assign_class label{{getKeyid('folder_name',$data)}}" data-id="{{getKeyid('folder_name',$data) }}" data-value="{{checkKey('folder_name',$data) }}" >
                                {!! checkKey('folder_name',$data) !!}
                            </span>
                        </th>
                    @endif
                    <th class="no-sort"></th>
                    </thead>
                    <tbody  id="form_group_data" class="form_group_data"></tbody>
                </table>
            </div>
        </div>
    </div>
    <br><br>
    <div class="card">
        <div class="card-body">
            <div id="table-form" class="table-editable">
                <span class="add-form float-right mb-3 mr-2">
                    @if(Auth()->user()->hasPermissionTo('create_documentlibrary') || Auth::user()->all_companies == 1 )
                        <a class="text-success" href="{{url('document/createDetail')}}" >
                        <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
                    </a>
                    @endif
                </span>
                @if(Auth()->user()->hasPermissionTo('delete_selected_documentlibrary') || Auth::user()->all_companies == 1 )
                    <button class="btn btn-danger btn-sm" id="deleteselectedform">
                        <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                            {!! checkKey('selected_delete',$data) !!}
                        </span>
                    </button>
                @endif
                @if(Auth()->user()->hasPermissionTo('delete_selected_documentlibrary') || Auth::user()->all_companies == 1 )
                    <button class="btn btn-danger btn-sm" style="display: none;" id="deleteselectedformSoft">
                        <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                            {!! checkKey('selected_delete',$data) !!}
                        </span>
                    </button>
                @endif
                @if(Auth()->user()->hasPermissionTo('selected_active_documentlibrary') || Auth::user()->all_companies == 1 )
                    <button class="btn btn-success btn-sm" style="display: none;" id="selectedactivebuttonform">
                        <span class="assign_class label{{getKeyid('selected_active',$data)}}" data-id="{{getKeyid('selected_active',$data) }}" data-value="{{checkKey('selected_active',$data) }}" >
                            {!! checkKey('selected_active',$data) !!}
                        </span>
                    </button>
                @endif
                @if(Auth()->user()->hasPermissionTo('selected_restore_documentlibrary') || Auth::user()->all_companies == 1 )
                    <button class="btn btn-primary btn-sm" id="restorebuttonform">
                        <span class="assign_class label{{getKeyid('restore',$data)}}" data-id="{{getKeyid('restore',$data) }}" data-value="{{checkKey('restore',$data) }}" >
                            {!! checkKey('restore',$data) !!}
                        </span>
                    </button>
                @endif
                @if(Auth()->user()->hasPermissionTo('active_documentlibrary') || Auth::user()->all_companies == 1 )
                    <button  style="display: none;" class="btn btn-primary btn-sm" id="activebuttonform">
                        <span class="assign_class label{{getKeyid('show_active',$data)}}" data-id="{{getKeyid('show_active',$data) }}" data-value="{{checkKey('show_active',$data) }}" >
                            {!! checkKey('show_active',$data) !!}
                        </span>
                    </button>
                @endif
                @if(Auth()->user()->hasPermissionTo('csv_documentlibrary') || Auth::user()->all_companies == 1 )
                    <form class="form-style" id="export_excel_form_btn" method="POST" action="{{ url('form/export/excel') }}">
                        <input type="hidden" class="form_export" name="excel_array" value="1">
                        <input type="hidden" class="form_export" name="folder" value="2">
                        {!! csrf_field() !!}
                        <button  type="submit" class="form_submit_check btn btn-warning btn-sm">
                            <span class="assign_class label{{getKeyid('excel_export',$data)}}" data-id="{{getKeyid('excel_export',$data) }}" data-value="{{checkKey('excel_export',$data) }}" >
                                {!! checkKey('excel_export',$data) !!}
                            </span>
                        </button>
                    </form>
                @endif
                @if(Auth()->user()->hasPermissionTo('word_documentlibrary') || Auth::user()->all_companies == 1 )
                    <form class="form-style" id="export_world_form_btn" method="POST" action="{{ url('form/export/world') }}">
                        <input type="hidden" class="form_export" name="word_array" value="1">
                        <input type="hidden" class="form_export" name="folder" value="2">
                        {!! csrf_field() !!}
                        <button  type="submit" class="form_submit_check btn btn-success btn-sm">
                            <span class="assign_class label{{getKeyid('word_export',$data)}}" data-id="{{getKeyid('word_export',$data) }}" data-value="{{checkKey('word_export',$data) }}" >
                                {!! checkKey('word_export',$data) !!}
                            </span>
                        </button>
                    </form>
                @endif
                @if(Auth()->user()->hasPermissionTo('pdf_documentlibrary') || Auth::user()->all_companies == 1 )
                    <form  class="form-style" {{pdf_view('forms')}} id="export_pdf_form_btn" method="POST" action="{{ url('form/export/pdf') }}">
                        {!! csrf_field() !!}
                        <input type="hidden" class="form_export" name="pdf_array" value="1">
                        <input type="hidden" class="form_export" name="folder" value="2">
                        <button  type="submit" class="form_submit_check btn btn-secondary btn-sm">
                           <span class="assign_class label{{getKeyid('pdf_export',$data)}}" data-id="{{getKeyid('pdf_export',$data) }}" data-value="{{checkKey('pdf_export',$data) }}" >
                                {!! checkKey('pdf_export',$data) !!}
                            </span>
                        </button>
                    </form>
                @endif
                <div id="table" class="table-editable">
                    <table id="document" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="no-sort">
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input client_checked" id="document_checkbox_all">
                                        <label class="form-check-label" for="document_checkbox_all">
                                            <span class="assign_class label{{getKeyid('all',$data)}}" data-id="{{getKeyid('all',$data) }}" data-value="{{checkKey('all',$data) }}" >
                                                {!! checkKey('all',$data) !!}
                                            </span>
                                        </label>
                                    </div>
                                </th>
                                <th>
                                    <span class="assign_class label{{getKeyid('doc_reference',$data)}}" data-id="{{getKeyid('doc_reference',$data) }}" data-value="{{checkKey('doc_reference',$data) }}" >
                                        {!! checkKey('doc_reference',$data) !!}
                                    </span>
                                </th>
                                <th>
                                    <span class="assign_class label{{getKeyid('name',$data)}}" data-id="{{getKeyid('name',$data) }}" data-value="{{checkKey('name',$data) }}" >
                                        {!! checkKey('name',$data) !!}
                                    </span>
                                </th>
                                <th>
                                    <span class="assign_class label{{getKeyid('expiry_date',$data)}}" data-id="{{getKeyid('expiry_date',$data) }}" data-value="{{checkKey('expiry_date',$data) }}" >
                                        {!! checkKey('expiry_date',$data) !!}
                                    </span>
                                </th>
                                <th class="no-sort" id="action"></th>
                            </tr>
                        </thead>
                        <tbody >
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @include('backend.document.model.view_popup')
    @component('backend.document.model.create')
    @endcomponent
    @component('backend.document.model.edit')
    @endcomponent
    @component('backend.document.list_input_permission')
    @endcomponent
    @include('backend.label.input_label')
    <script src="{{asset('/custom/js/folder_append.js')}}"></script>
    <script src="{{asset('/custom/js/file_append.js')}}"></script>
    <script type="text/javascript">

        let formgroup_id = '<?php echo $id; ?>';
        $(document).ready(function () {
            // folder
            var url='/form_group/getall/'+formgroup_id;
            form_group_data(url);
            // file
            var url='/form/getall';
            form_data(url);
        });

        $('body').on('change', '.datepicker', function() {
            var id=$(this).attr('id');
            var value=$(this).val();
            console.log(value);
            if(value)
            {
                $('#'+id).val(value);
            }
        });
    </script>
    <script src="{{asset('/custom/js/form.js')}}"></script>
    <script src="{{asset('/custom/js/form_group.js')}}"></script>
@endsection
