@php
        $data=localization();
@endphp
<style type="text/css">
  @media (min-width: 576px) {
      .modal-dialog {
        max-width: 75% !important;
      }
    }
</style>
<div class="modal fade" id="modalstatic_form_view" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Upload</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div> 
         <div id="pdf-viewer"></div>
      </div>
    </div>
</div>



<script src="{{asset('js/pdfview.js') }}"></script>
<script type="text/javascript">
  var options = {
    pdfOpenParams: {toolbar: '0'}
};
PDFObject.embed("{{ route('show-pdf', ['id' => $id]) }}", "#pdf-viewer",options); 
</script>
@endsection