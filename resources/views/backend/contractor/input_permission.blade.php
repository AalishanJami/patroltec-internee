@if(Auth()->user()->all_companies == 1)
    <input type="hidden" id="contractor_checkbox" value="1">
    <input type="hidden" id="contractor_name" value="1">
    <input type="hidden" id="contractor_start_date" value="1">
    <input type="hidden" id="contractor_end_date" value="1">
    <input type="hidden" id="contractor_allowed" value="1">
    <input type="hidden" id="contractor_banned" value="1">
    <input type="hidden" id="edit_contractor" value="1">
    <input type="hidden" id="delete_contractor" value="1">
    <!------create--------->
    <input type="hidden" id="contractor_name_create" value="1">
    <input type="hidden" id="contractor_start_date_create" value="1">
    <input type="hidden" id="contractor_end_date_create" value="1">
    <input type="hidden" id="contractor_allowed_create" value="1">
    <input type="hidden" id="contractor_banned_create" value="1">

    <!------edit--------->
    <input type="hidden" id="contractor_name_edit" value="1">
    <input type="hidden" id="contractor_start_date_edit" value="1">
    <input type="hidden" id="contractor_end_date_edit" value="1">
    <input type="hidden" id="contractor_allowed_edit" value="1">
    <input type="hidden" id="contractor_banned_edit" value="1">
@else
    <input type="hidden" id="contractor_checkbox" value="{{Auth()->user()->hasPermissionTo('contractor_checkbox')}}">
    <input type="hidden" id="contractor_name" value="{{Auth()->user()->hasPermissionTo('contractor_name')}}">
    <input type="hidden" id="contractor_start_date" value="{{Auth()->user()->hasPermissionTo('contractor_start_date')}}">
    <input type="hidden" id="contractor_end_date" value="{{Auth()->user()->hasPermissionTo('contractor_end_date')}}">
    <input type="hidden" id="contractor_allowed" value="{{Auth()->user()->hasPermissionTo('contractor_allowed')}}">
    <input type="hidden" id="contractor_banned" value="{{Auth()->user()->hasPermissionTo('contractor_banned')}}">
    <input type="hidden" id="edit_contractor" value="{{Auth()->user()->hasPermissionTo('edit_contractor')}}">
    <input type="hidden" id="delete_contractor" value="{{Auth()->user()->hasPermissionTo('delete_contractor')}}">

    <!------create--------->
    <input type="hidden" id="contractor_name_create" value="{{Auth()->user()->hasPermissionTo('contractor_name_create')}}">
    <input type="hidden" id="contractor_start_date_create" value="{{Auth()->user()->hasPermissionTo('contractor_start_date_create')}}">
    <input type="hidden" id="contractor_end_date_create" value="{{Auth()->user()->hasPermissionTo('contractor_end_date_create')}}">
    <input type="hidden" id="contractor_allowed_create" value="{{Auth()->user()->hasPermissionTo('contractor_allowed_create')}}">
    <input type="hidden" id="contractor_banned_create" value="{{Auth()->user()->hasPermissionTo('contractor_banned_create')}}">

    <!------edit--------->
    <input type="hidden" id="contractor_name_edit" value="{{Auth()->user()->hasPermissionTo('contractor_name_edit')}}">
    <input type="hidden" id="contractor_start_date_edit" value="{{Auth()->user()->hasPermissionTo('contractor_start_date_edit')}}">
    <input type="hidden" id="contractor_end_date_edit" value="{{Auth()->user()->hasPermissionTo('contractor_end_date_edit')}}">
    <input type="hidden" id="contractor_allowed_edit" value="{{Auth()->user()->hasPermissionTo('contractor_allowed_edit')}}">
    <input type="hidden" id="contractor_banned_edit" value="{{Auth()->user()->hasPermissionTo('contractor_banned_edit')}}">
@endif
