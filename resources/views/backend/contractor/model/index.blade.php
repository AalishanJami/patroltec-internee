@php
    $data=localization();
@endphp
<div class="modal fade" id="modalContractIndex" tabindex="-1" data-keyboard="false" data-backdrop="static" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">
                    <span class="assign_class label{{getKeyid('contractor',$data)}}" data-id="{{getKeyid('contractor',$data) }}" data-value="{{checkKey('contractor',$data) }}" >
                        {!! checkKey('contractor',$data) !!}
                    </span>
                    @if(Auth()->user()->hasPermissionTo('view_cms') || Auth::user()->all_companies == 1 )
                        <button data-class="edit-disable" class="edit_cms_enable edit_cms_switch btn btn-primary btn-sm waves-effect waves-light" style="display: none;" >
                            Edit cms
                        </button>
                        <button  data-class="edit-enable" class="edit_cms_disable edit_cms_switch btn btn-danger btn-sm waves-effect waves-light">
                            Edit cms
                        </button>
                    @endif
                </h4>
                <button type="button" class="close contract_listing_close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="card">
                <div class="card-body">
                    <div id="table-contractor" class="table-editable table-responsive">
                        @if(Auth()->user()->hasPermissionTo('create_contractor') || Auth::user()->all_companies == 1 )
                            <span class="add-contractor table-add float-right mb-3 mr-2">
                              <a class="text-success"><i class="fas fa-plus fa-2x" aria-hidden="true"></i></a>
                            </span>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_delete_contractor') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-danger btn-sm" id="deleteselectedcontractor">
                                <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                                    {!! checkKey('selected_delete',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_delete_contractor') || Auth::user()->all_companies == 1 )
                        <button class="btn btn-danger btn-sm" style="display: none;" id="deleteselectedcontractorSoft">
                            <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                                {!! checkKey('selected_delete',$data) !!}
                            </span>
                        </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_active_contractor') || Auth::user()->all_companies == 1 )
                        <button class="btn btn-success btn-sm" style="display: none;" id="selectedactivebuttoncontractor">
                            <span class="assign_class label{{getKeyid('selected_active',$data)}}" data-id="{{getKeyid('selected_active',$data) }}" data-value="{{checkKey('selected_active',$data) }}" >
                                {!! checkKey('selected_active',$data) !!}
                            </span>
                        </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('restore_delete_contractor') || Auth::user()->all_companies == 1 )
                        <button class="btn btn-primary btn-sm" id="restorebuttoncontractor">
                            <span class="assign_class label{{getKeyid('restore',$data)}}" data-id="{{getKeyid('restore',$data) }}" data-value="{{checkKey('restore',$data) }}" >
                                {!! checkKey('restore',$data) !!}
                            </span>
                        </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('active_contractor') || Auth::user()->all_companies == 1 )
                        <button  style="display: none;" class="btn btn-primary btn-sm" id="activebuttoncontractor">
                            <span class="assign_class label{{getKeyid('show_active',$data)}}" data-id="{{getKeyid('show_active',$data) }}" data-value="{{checkKey('show_active',$data) }}" >
                                {!! checkKey('show_active',$data) !!}
                            </span>
                        </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('csv_contractor') || Auth::user()->all_companies == 1 )
                        <form class="form-style" method="POST" id="export_excel_contractor" action="{{ url('contractor/export/excel') }}">
                            {!! csrf_field() !!}
                            <input type="hidden" class="contractor_export" name="excel_array" value="1">
                            <button  type="submit" class="form_submit_check btn btn-warning btn-sm">
                                <span class="assign_class label{{getKeyid('excel_export',$data)}}" data-id="{{getKeyid('excel_export',$data) }}" data-value="{{checkKey('excel_export',$data) }}" >
                                    {!! checkKey('excel_export',$data) !!}
                                </span>
                            </button>
                        </form>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('word_contractor') || Auth::user()->all_companies == 1 )
                        <form class="form-style" method="POST" id="export_world_contractor" action="{{ url('contractor/export/world') }}">
                            {!! csrf_field() !!}
                            <input type="hidden" class="contractor_export" name="word_array" value="1">
                            <button  type="submit" class="form_submit_check btn btn-success btn-sm">
                               <span class="assign_class label{{getKeyid('word_export',$data)}}" data-id="{{getKeyid('word_export',$data) }}" data-value="{{checkKey('word_export',$data) }}" >
                                    {!! checkKey('word_export',$data) !!}
                                </span>
                            </button>
                        </form>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('pdf_contractor') || Auth::user()->all_companies == 1 )
                        <form  class="form-style" {{pdf_view('contractors')}} method="POST" id="export_pdf_contractor" action="{{ url('contractor/export/pdf') }}">
                            {!! csrf_field() !!}
                            <input type="hidden" class="form_submit_check contractor_export" name="pdf_array" value="1">
                            <button  type="submit" class="form_submit_check btn btn-secondary btn-sm">
                                <span class="assign_class label{{getKeyid('pdf_export',$data)}}" data-id="{{getKeyid('pdf_export',$data) }}" data-value="{{checkKey('pdf_export',$data) }}" >
                                    {!! checkKey('pdf_export',$data) !!}
                                </span>
                            </button>
                        </form>
                        @endif
                        <div id="contractor_table" class="table-responsive table-editable">
                            <table id="contractor_table_id" class="table table-bordered table-responsive-md table-striped" style="width: 100%;">
                                <thead>
                                <tr>
                                    <th class="no-sort all_checkboxes_style">
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input contractor_checked" id="contractor_checkbox_all">
                                            <label class="form-check-label" for="contractor_checkbox_all">
                                                <span class="assign_class label{{getKeyid('all',$data)}}" data-id="{{getKeyid('all',$data) }}" data-value="{{checkKey('all',$data) }}" >
                                                    {!! checkKey('all',$data) !!}
                                                </span>
                                            </label>
                                        </div>
                                    </th>
{{--                                    <th class="no-sort" id="action"></th>--}}
                                    <th>
                                        <span class="assign_class label{{getKeyid('name',$data)}}" data-id="{{getKeyid('name',$data) }}" data-value="{{checkKey('name',$data) }}" >
                                            {!! checkKey('name',$data) !!}
                                        </span>
                                    </th>
                                    <th>
                                        <span class="assign_class label{{getKeyid('start_date',$data)}}" data-id="{{getKeyid('start_date',$data) }}" data-value="{{checkKey('start_date',$data) }}" >
                                            {!! checkKey('start_date',$data) !!}
                                        </span>
                                    </th>
                                    <th>
                                        <span class="assign_class label{{getKeyid('expiry_date',$data)}}" data-id="{{getKeyid('expiry_date',$data) }}" data-value="{{checkKey('expiry_date',$data) }}" >
                                            {!! checkKey('expiry_date',$data) !!}
                                        </span>
                                    </th>
                                    <th>
                                        <span class="assign_class label{{getKeyid('allowed',$data)}}" data-id="{{getKeyid('allowed',$data) }}" data-value="{{checkKey('allowed',$data) }}" >
                                            {!! checkKey('allowed',$data) !!}
                                        </span>
                                    </th>
                                    <th>
                                        <span class="assign_class label{{getKeyid('banned',$data)}}" data-id="{{getKeyid('banned',$data) }}" data-value="{{checkKey('banned',$data) }}" >
                                            {!! checkKey('banned',$data) !!}
                                        </span>
                                    </th>
                                    <th class="no-sort all_action_btn"> </th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('backend.contractor.input_permission')
@component('backend.contractor.model.create')
@endcomponent
@component('backend.contractor.model.edit')
@endcomponent
<script type="text/javascript">
    function checkSelectedContrator(value,checkValue)
    {
        if(value == checkValue)
        {
            return 'checked';
        }
        else
        {
            return "";
        }

    }
    function contractor_data(url)
    {
        var table = $('#contractor_table_id').dataTable(
            {
                processing: true,
                language: {
                    'lengthMenu': '  _MENU_ ',
                    'search':'<span class="assign_class label'+$('#search_id').attr('data-id')+'" data-id="'+$('#search_id').attr('data-id')+'" data-value="'+$('#search_id').val()+'" >'+$('#search_id').val()+'</span>',
                    'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                    'info':'',
                    'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                    'paginate': {
                      'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                    'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                  },
                  'infoFiltered': "(filtered from _MAX_ total records)"
                },
                "ajax": {
                    "url": url,
                    "type": 'get',
                },
                "createdRow": function( row, data, dataIndex,columns )
                {

                    var checkbox_permission=$('#checkbox_permission').val();
                    var p_disable="";
                    if (!$("#contractor_allowed_edit").val() || !$('#edit_contractor').val()) {
                        var p_disable="disabled";
                    }
                    var preferred='';
                    preferred+='<div class="form-check float-left">';
                    preferred+=' <input type="radio" '+p_disable+' value="1" '+checkSelectedContrator(data.preferred,1)+' name="preferred'+data.id+'"  class="preferred'+data.id+' form-check-input show_button" data-id="'+data.id+'" id="yes_preferred'+data.id+'">';
                    preferred+='<label class="form-check-label" for="yes_preferred'+data.id+'">Yes</label>';
                    preferred+='</div>';
                    preferred+='<div class="form-check float-left">';
                    preferred+=' <input type="radio" '+p_disable+' value="0" '+checkSelectedContrator(data.preferred,0)+'  name="preferred'+data.id+'"  class="preferred'+data.id+' form-check-input show_button" data-id="'+data.id+'" id="no_preferred'+data.id+'">';
                    preferred+='<label class="form-check-label" for="no_preferred'+data.id+'">No</label>';
                    preferred+='</div>';
                    var banned='';
                    var b_disable="";
                    if (!$("#contractor_banned_edit").val() || !$('#edit_contractor').val()) {
                        var b_disable="disabled";
                    }
                    banned+='<div class="form-check float-left">';
                    banned+=' <input type="radio" '+checkSelectedContrator(data.banned,1)+' '+b_disable+'  value="1" name="banned'+data.id+'" class="banned'+data.id+' form-check-input show_button" data-id="'+data.id+'" id="yes_banned'+data.id+'">';
                    banned+='<label class="form-check-label" for="yes_banned'+data.id+'">Yes</label>';
                    banned+='</div>';
                    banned+='<div class="form-check float-left">';
                    banned+=' <input type="radio" '+checkSelectedContrator(data.banned,0)+' '+b_disable+'  value="0" name="banned'+data.id+'"  class="banned'+data.id+' form-check-input show_button" data-id="'+data.id+'" id="no_banned'+data.id+'">';
                    banned+='<label class="form-check-label" for="no_banned'+data.id+'">No</label>';
                    banned+='</div>';
                    var s_disable="";
                    if (!$("#contractor_start_date_edit").val() || !$('#edit_contractor').val()) {
                        var s_disable="disabled";
                    }
                    var start='';
                    start+='<input placeholder="Selected date" '+s_disable+' value="'+data.start+'" type="date" id="start'+data.id+'" data-id="'+data.id+'"  class="form-control show_button datepicker">';
                    var end='';
                    var e_disable="";
                    if (!$("#contractor_end_date_edit").val() || !$('#edit_contractor').val()) {
                        var e_disable="disabled";
                    }
                    end+='<input placeholder="Selected date" '+e_disable+' value="'+data.end+'" type="date" id="end'+data.id+'" data-id="'+data.id+'"  class="form-control show_button datepicker">';
                    var checkbox='';
                    checkbox+='<div class="form-check"><input type="checkbox" class="form-check-input selectedrowcontractor contractor_checked show_button" id="contractor'+data.id+'"><label class="form-check-label" for="contractor'+data.id+'""></label></div>';
                    var submit='';
                    submit+='<a type="button" class="btn btn-primary btn-xs my-0 waves-effect all_action_btn_margin waves-light hide_tick_btn savecontractordata contractor_show_button_'+data.id+' show_tick_btn'+data.id+'" style="display:none;" data-id="'+data.id+'" id="'+data.id+'"><i class="fas fa-check"></i></a>';
                    $(columns[0]).html(checkbox);
                    $(columns[1]).attr('id', 'name'+data['id']);
                    $(columns[1]).attr('onkeydown', 'editSelectedRow(contractor_tr'+data['id']+')');
                    $(columns[1]).attr('data-id', data['id']);
                    if ($("#contractor_name_edit").val()=='1' && $('#edit_contractor').val()){

                        $(columns[1]).attr('Contenteditable', 'true');
                    }
                    $(columns[1]).attr('class','edit_inline_contractor');
                    $(columns[2]).attr('id', 'start'+data['id']);
                    $(columns[2]).attr('onchange', 'editSelectedRow(contractor_tr'+data['id']+')');
                    $(columns[2]).attr('data-id', data['id']);
                    $(columns[2]).attr('class','edit_inline_contractor');
                    $(columns[2]).html(start);
                    $(columns[3]).attr('id','end'+data['id']);
                    $(columns[3]).attr('data-id', data['id']);
                    $(columns[3]).attr('onchange', 'editSelectedRow(contractor_tr'+data['id']+')');
                    $(columns[3]).attr('class','edit_inline_contractor');

                    $(columns[3]).html(end);
                    $(columns[4]).attr('id', 'preferred'+data['id']);
                    $(columns[4]).attr('data-id', data['id']);
                    $(columns[4]).attr('class','edit_inline_contractor');
                    $(columns[4]).attr('onclick', 'editSelectedRow(contractor_tr'+data['id']+')');
                    $(columns[4]).html(preferred);
                    $(columns[5]).attr('id', 'banned'+data['id']);
                    $(columns[5]).attr('data-id', data['id']);
                    $(columns[5]).attr('class','edit_inline_contractor');
                    $(columns[5]).attr('onclick', 'editSelectedRow(contractor_tr'+data['id']+')');
                    $(columns[5]).html(banned);
                    $(columns[6]).append(submit);
                    $(row).attr('id', 'contractor_tr'+data['id']);

                    $(row).attr('data-id', data['id']);
                    //$(row).attr('class', 'selectedrowcontractor');
                },
                columns:
                    [
                        {data: 'checkbox', name: 'checkbox',visible:$('#contractor_checkbox').val()},
                        // {data: 'actions', name: 'actions',visible:$('#delete_contractor').val()},
                        {data: 'name', name: 'name',visible:$('#contractor_name').val()},
                        {data: 'start', name: 'start',visible:$('#contractor_start_date').val()},
                        {data: 'end', name: 'end',visible:$('#contractor_end_date').val()},
                        {data: 'preferred', name: 'preferred',visible:$('#contractor_allowed').val()},
                        {data: 'banned', name: 'banned',visible:$('#contractor_banned').val()},
                        {data: 'actions', name: 'actions'},
                    ],
                "columnDefs": [ {
                    "targets": [0,1],
                    "orderable": false
                } ]
            });

        if ($(".contractor_checked").prop('checked',true)){
            $(".contractor_checked").prop('checked',false);
        }
    }

    $(document).ready(function () {
        var url='/contractor/getall';
        contractor_data(url);

        $('body').on('change', '.datepicker', function() {
            var id=$(this).attr('id');
            var value=$(this).val();
            if(value)
            {
                $('#'+id).val(value);
            }
        });




        $('.contract_listing_close').click(function(){
            $.ajax({
                type: "GET",
                url: '/contractor/getall',
                success: function(response)
                {
                    var html='';
                    var contractor_id=$("#contractor_id").children('option:selected').val();
                    for (var i = response.data.length - 1; i >= 0; i--) {
                        var selected='';
                        if (response.data[i].id==contractor_id){
                            selected='selected';
                        }
                        html+='<option '+selected+' value="'+response.data[i].id+'">'+response.data[i].name+'</option>';
                    }

                    if (contractor_id==''){
                        html+='<option selected disabled value="">---Please Select---</option>';
                    }else{
                        html+='<option disabled value="">---Please Select---</option>';
                    }
                    $('#contractor_id').empty();
                    $('#contractor_id').html(html);

                },
                error: function (error) {
                    console.log(error);
                }
            });
        });
    });

</script>
