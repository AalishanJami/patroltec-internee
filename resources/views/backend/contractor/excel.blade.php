<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
</head>
<body>

<table class="table table-striped table-bordered">
    <thead>
    <tr>
        @if(Auth()->user()->hasPermissionTo('contractor_name_excel_export') || Auth::user()->all_companies == 1 )
            <th> Name</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('contractor_start_date_excel_export') || Auth::user()->all_companies == 1 )
            <th> Start</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('contractor_end_date_excel_export') || Auth::user()->all_companies == 1 )
            <th> End</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('contractor_allowed_excel_export') || Auth::user()->all_companies == 1 )
            <th> Allowed</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('contractor_banned_excel_export') || Auth::user()->all_companies == 1 )
            <th> Banned</th>
        @endif
    </tr>
    </thead>
    <tbody>
    @foreach($data as $value)
        <tr>
            @if(Auth()->user()->hasPermissionTo('contractor_name_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{ $value->name }}</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('contractor_start_date_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{ Carbon\Carbon::parse($value->start)->format('jS M Y') }}</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('contractor_end_date_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{ Carbon\Carbon::parse($value->end)->format('jS M Y') }}</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('contractor_allowed_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{$value->preferred}}</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('contractor_banned_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{$value->banned}}</td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>
</body>
