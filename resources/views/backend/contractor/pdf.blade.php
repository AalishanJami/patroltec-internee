@include('backend.layouts.pdf_start')
    <thead>
    <tr>
        @if(Auth()->user()->hasPermissionTo('contractor_name_pdf_export') || Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout"> Name</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('contractor_start_date_pdf_export') || Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout"> Start</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('contractor_end_date_pdf_export') || Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout"> End</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('contractor_allowed_pdf_export') || Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout"> Allowed</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('contractor_banned_pdf_export') || Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout"> Banned</th>
        @endif
    </tr>
    </thead>
    <tbody>
    @foreach($data as $value)
        <tr>
            @if(Auth()->user()->hasPermissionTo('contractor_name_pdf_export') || Auth::user()->all_companies == 1 )
               <td class="pdf_table_layout"><p class="col_text_style">{{ $value->name }}</p></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('contractor_start_date_pdf_export') || Auth::user()->all_companies == 1 )
               <td class="pdf_table_layout"><p class="col_text_style">{{ Carbon\Carbon::parse($value->start)->format('jS M Y') }}</p></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('contractor_end_date_pdf_export') || Auth::user()->all_companies == 1 )
               <td class="pdf_table_layout"><p class="col_text_style">{{ Carbon\Carbon::parse($value->end)->format('jS M Y') }}</p></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('contractor_allowed_pdf_export') || Auth::user()->all_companies == 1 )
               <td class="pdf_table_layout"><p class="col_text_style">{{$value->preferred}}</p></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('contractor_banned_pdf_export') || Auth::user()->all_companies == 1 )
               <td class="pdf_table_layout"><p class="col_text_style">{{$value->banned}}</p></td>
            @endif
        </tr>
    @endforeach
    </tbody>
@include('backend.layouts.pdf_end')