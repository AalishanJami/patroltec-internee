@php
    $data=localization();
@endphp

<div class="modal fade" id="modalcustomerIndex" tabindex="-1" data-keyboard="false" data-backdrop="static" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">
                    <span class="assign_class label{{getKeyid('customer',$data)}}" data-id="{{getKeyid('customer',$data) }}" data-value="{{checkKey('customer',$data) }}" >
                        {!! checkKey('customer',$data) !!}
                    </span>
                     @if(Auth()->user()->hasPermissionTo('view_cms') || Auth::user()->all_companies == 1 )
                        <button data-class="edit-disable" class="edit_cms_enable edit_cms_switch btn btn-primary btn-sm waves-effect waves-light" style="display: none;" >
                            Edit cms
                        </button>
                        <button  data-class="edit-enable" class="edit_cms_disable edit_cms_switch btn btn-danger btn-sm waves-effect waves-light">
                            Edit cms
                        </button>
                    @endif
                </h4>
                <button type="button" class="close customer_listing_close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="card">
                <div class="card-body">
                    <div id="table_customer" class="table-editable">
                        @if(Auth()->user()->hasPermissionTo('create_customer') || Auth::user()->all_companies == 1 )
                            <span class="add-customer table-add float-right mb-3 mr-2">
                                <a class="text-success"><i class="fas fa-plus fa-2x" aria-hidden="true"></i></a>
                            </span>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_delete_customer') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-danger btn-sm" id="deleteselectedcustomer">
                                <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                                    {!! checkKey('selected_delete',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_delete_customer') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-danger btn-sm" style="display: none;" id="deleteselectedcustomerSoft">
                                <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                                    {!! checkKey('selected_delete',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_active_customer') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-success btn-sm" style="display: none;" id="selectedactivebuttoncustomer">
                                <span class="assign_class label{{getKeyid('selected_active',$data)}}" data-id="{{getKeyid('selected_active',$data) }}" data-value="{{checkKey('selected_active',$data) }}" >
                                    {!! checkKey('selected_active',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('restore_delete_customer') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-primary btn-sm" id="restorebuttoncustomer">
                               <span class="assign_class label{{getKeyid('restore',$data)}}" data-id="{{getKeyid('restore',$data) }}" data-value="{{checkKey('restore',$data) }}" >
                                    {!! checkKey('restore',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('active_customer') || Auth::user()->all_companies == 1 )
                            <button  style="display: none;" class="btn btn-primary btn-sm" id="activebuttoncustomer">
                                <span class="assign_class label{{getKeyid('show_active',$data)}}" data-id="{{getKeyid('show_active',$data) }}" data-value="{{checkKey('show_active',$data) }}" >
                                    {!! checkKey('show_active',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('csv_customer') || Auth::user()->all_companies == 1 )
                        <form class="form-style" method="POST" id="export_excel_customer" action="{{ url('customer/export/excel') }}">
                            {!! csrf_field() !!}
                            <input type="hidden" class="customer_export" name="excel_array" value="1">
                            <button  type="submit" class="form_submit_check btn btn-warning btn-sm">
                                <span class="assign_class label{{getKeyid('excel_export',$data)}}" data-id="{{getKeyid('excel_export',$data) }}" data-value="{{checkKey('excel_export',$data) }}" >
                                    {!! checkKey('excel_export',$data) !!}
                                </span>
                            </button>
                        </form>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('word_customer') || Auth::user()->all_companies == 1 )
                        <form class="form-style" method="POST" id="export_world_customer" action="{{ url('customer/export/world') }}">
                            {!! csrf_field() !!}
                            <input type="hidden" class="customer_export" name="word_array" value="1">
                            <button  type="submit" class="form_submit_check btn btn-success btn-sm">
                               <span class="assign_class label{{getKeyid('word_export',$data)}}" data-id="{{getKeyid('word_export',$data) }}" data-value="{{checkKey('word_export',$data) }}" >
                                    {!! checkKey('word_export',$data) !!}
                                </span>
                            </button>
                        </form>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('pdf_customer') || Auth::user()->all_companies == 1 )
                        <form  class="form-style" {{pdf_view('customers')}} method="POST" id="export_pdf_customer" action="{{ url('customer/export/pdf') }}">
                            {!! csrf_field() !!}
                            <input type="hidden" class="customer_export" name="pdf_array" value="1">
                            <button  type="submit" class="form_submit_check btn btn-secondary btn-sm">
                               <span class="assign_class label{{getKeyid('pdf_export',$data)}}" data-id="{{getKeyid('pdf_export',$data) }}" data-value="{{checkKey('pdf_export',$data) }}" >
                                    {!! checkKey('pdf_export',$data) !!}
                                </span>
                            </button>
                        </form>
                        @endif
                        <div id="table" class="table-editable">
                            <table id="customer_table" class="table table-bordered table-responsive-md table-striped">
                                <thead>
                                <tr>
                                    @if(Auth()->user()->hasPermissionTo('customer_checkbox') || Auth::user()->all_companies == 1 )
                                        <th class="no-sort all_checkboxes_style">
                                            <div class="form-check">
                                                <input type="checkbox" class="form-check-input customer_checked" id="customer_checkbox_all">
                                                <label class="form-check-label" for="customer_checkbox_all">
                                                    <span class="assign_class label{{getKeyid('all',$data)}}" data-id="{{getKeyid('all',$data) }}" data-value="{{checkKey('all',$data) }}" >
                                                        {!! checkKey('all',$data) !!}
                                                    </span>
                                                </label>
                                            </div>
                                        </th>
                                    @endif
                                    @if(Auth()->user()->hasPermissionTo('customer_name') || Auth::user()->all_companies == 1 )
                                        <th>
                                            <span class="assign_class label{{getKeyid('name',$data)}}" data-id="{{getKeyid('name',$data) }}" data-value="{{checkKey('name',$data) }}" >
                                                {!! checkKey('name',$data) !!}
                                            </span>
                                        </th>
                                    @endif
                                    @if(Auth()->user()->hasPermissionTo('customer_division') || Auth::user()->all_companies == 1 )
                                        <th>
                                            <span class="assign_class label{{getKeyid('devision',$data)}}" data-id="{{getKeyid('devision',$data) }}" data-value="{{checkKey('devision',$data) }}" >
                                                {!! checkKey('devision',$data) !!}
                                            </span>
                                        </th>
                                    @endif
                                    @if(Auth()->user()->hasPermissionTo('edit_customer') || Auth::user()->all_companies == 1 )
                                        <th class="no-sort all_action_btn"></th>
                                    @endif
                                </tr>
                                </thead>
                                <tbody id="customer_data">
                                </tbody>
                            </table>
                        </div>
                        @if(Auth()->user()->all_companies == 1)
                            <input type="hidden" id="customer_checkbox" value="1">
                            <input type="hidden" id="delete_customer" value="1">
                            <input type="hidden" id="hard_delete_customer" value="1">
                            <input type="hidden" id="customer_name" value="1">
                            <input type="hidden" id="customer_division" value="1">
                            <input type="hidden" id="edit_customer" value="1">
                            <input type="hidden" id="customer_name_create" value="1">
                            <input type="hidden" id="customer_division_create" value="1">
                            <input type="hidden" id="customer_name_edit" value="1">
                            <input type="hidden" id="customer_division_edit" value="1">
                        @else
                            <input type="hidden" id="customer_checkbox" value="{{Auth()->user()->hasPermissionTo('customer_checkbox')}}">
                            <input type="hidden" id="delete_customer" value="{{Auth()->user()->hasPermissionTo('delete_customer')}}">
                            <input type="hidden" id="hard_delete_customer" value="{{Auth()->user()->hasPermissionTo('hard_delete_customer')}}">
                            <input type="hidden" id="customer_name" value="{{Auth()->user()->hasPermissionTo('customer_name')}}">
                            <input type="hidden" id="customer_division" value="{{Auth()->user()->hasPermissionTo('customer_division')}}">
                            <input type="hidden" id="edit_customer" value="{{Auth()->user()->hasPermissionTo('edit_customer')}}">

                            <input type="hidden" id="customer_name_create" value="{{Auth()->user()->hasPermissionTo('customer_name_create')}}">
                            <input type="hidden" id="customer_division_create" value="{{Auth()->user()->hasPermissionTo('customer_division_create')}}">
                            <input type="hidden" id="customer_name_edit" value="{{Auth()->user()->hasPermissionTo('customer_name_edit')}}">
                            <input type="hidden" id="customer_division_edit" value="{{Auth()->user()->hasPermissionTo('customer_division_edit')}}">
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    function checkSelected(value,checkValue)
    {
        if(value == checkValue)
        {
            return 'selected';
        }
        else
        {
            return "";
        }

    }
    function customer_data(url) {
        $.ajax({
            type: 'GET',
            url: url,
            success: function(response)
            {
                $('#customer_table').DataTable().clear().destroy();
                var table = $('#customer_table').dataTable(
                    {
                        processing: true,
                        language: {
                            'lengthMenu': '  _MENU_ ',
                            'search':'<span class="assign_class label'+$('#search_id').attr('data-id')+'" data-id="'+$('#search_id').attr('data-id')+'" data-value="'+$('#search_id').val()+'" >'+$('#search_id').val()+'</span>',
                            'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                            'info':'',
                            'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                            'paginate': {
                              'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                            'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                          },
                          'infoFiltered': "(filtered from _MAX_ total records)"
                        },
                        "ajax": {
                            "url": url,
                            "type": 'get',
                        },
                        "createdRow": function( row, data,dataIndex,columns )
                        {
                            var checkbox_permission=$('#checkbox_permission').val();
                            var checkbox='';
                            checkbox+='<div class="form-check"><input type="checkbox" class="form-check-input selectedrowcustomer" id="customer'+data.id+'"><label class="form-check-label" for="customer'+data.id+'""></label></div>';
                            var submit='';
                            submit+='<a type="button" class="btn btn-primary btn-xs my-0 all_action_btn_margin waves-effect waves-light savecustomerdata customer_show_button_'+data.id+' show_tick_btn'+data.id+'" style="display: none;" id="'+data.id+'"><i class="fas fa-check"></i></a>';
                            var devision='';
                            devision+='<select searchable="Search here.." onchange="editSelectedRow('+"customer_tr"+data.id+')"  id="division_id'+data.id+'"  style="background-color: inherit;border: 0px">';
                            for (var j = response.divisions.length - 1; j >= 0; j--) {
                                devision+='<option value="'+response.divisions[j].id+'" '+checkSelected(data.division_id,response.divisions[j].id)+'  >'+response.divisions[j].name+'</option>';
                            }
                            devision+='</select>';

                            $(columns[0]).html(checkbox);
                            $(columns[1]).attr('id', 'nameC'+data['id']);
                            if ($("#customer_name_edit").val()==1){
                                $(columns[1]).attr('Contenteditable', 'true');
                            }
                            $(columns[1]).attr('class','edit_inline_customer');
                            $(columns[1]).attr('data-id',data['id']);
                            $(columns[1]).attr('onkeydown', 'editSelectedRow(customer_tr'+data['id']+')');
                            if ($("#customer_division_edit").val()==1){
                                $(columns[1]).attr('Contenteditable', 'true');
                            }
                            $(columns[2]).attr('class','edit_inline_customer');
                            $(columns[2]).attr('data-id',data['id']);
                            $(columns[2]).html(devision);
                            $(columns[3]).append(submit);
                            $(row).attr('id', 'customer_tr'+data['id']);
                            //$(row).attr('class', 'selectedrowcustomer');
                            $(row).attr('data-id', data['id']);
                        },
                        columns:
                            [
                                {data: 'checkbox', name: 'checkbox',visible:$('#customer_checkbox').val()},
                                {data: 'name', name: 'name',visible:$('#customer_name').val()},
                                {data:'division_id',name:'division_id',visible:$('#customer_division').val()},
                                {data: 'actions', name: 'actions',visible:$('#delete_customer').val()},
                                // {data: 'submit', name: 'submit',visible:$('#edit_customer').val()},
                            ],
                    }
                );
            },
            error: function (error) {
                console.log(error);
            }
        });
        if ($(":checkbox").prop('checked',true)){
            $(":checkbox").prop('checked',false);
        }
    }
    $(document).ready(function () {
        var url='/customer/getall';
        customer_data(url);
        $("body").on('keyup','.edit_inline_customer',function () {
            $(".customer_show_button_"+$(this).attr('data-id')).show();
        });

    });

    $('.customer_listing_close').click(function(){
        // $.ajax({
        //     type: "GET",
        //     url: '/customer/getall',
        //     success: function(response)
        //     {
        //         var html='';
        //         for (var i = response.data.length - 1; i >= 0; i--) {
        //             var selected='';
        //             if(response.project.customer_id == response.data[i].id)
        //             {
        //                 selected='selected';
        //             }
        //             html+='<option '+selected+' value="'+response.data[i].id+'">'+response.data[i].name+'</option>';
        //         }
        //         $('#customer_id').empty();
        //         $('#customer_id').html(html);

        //     },
        //     error: function (error) {
        //         console.log(error);
        //     }
        // });
        var division_id=$('#division_id').children("option:selected").val();
            $.ajax({
                type: "GET",
                url: '/customer/getall/bydivision',
                data: {
                    'division_id': division_id
                },
                success: function(response)
                {
                    var html='';
                    if(response.data.length<=0){
                        html+='<option value="">No Customer Found ....</option>';
                    }
                    var customer_id=$('#customer_id').children("option:selected").val();
                    for (var i = response.data.length - 1; i >= 0; i--) {
                        var selected='';
                        if(response.project.customer_id == response.data[i].id)
                        {
                            selected='selected';
                        }
                        html+='<option '+selected+' value="'+response.data[i].id+'">'+response.data[i].name+'</option>';
                    }
                    if (customer_id==''){
                        html+='<option selected disabled value="">---Please Select---</option>';
                    }else{
                        html+='<option disabled value="">---Please Select---</option>';
                    }
                    $('#customer_id').html(html);
                },
                error: function (error) {
                    console.log(error);
                }
            });
    });
</script>
