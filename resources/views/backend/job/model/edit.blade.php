 @php
        $data=localization();
@endphp


<div class="modal fade" id="modalEditJob" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Job Edit</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
              <form >
                <div class="modal-body">
                    <div class="card-body px-lg-5 pt-0">
                        <!-- Form -->
                        <form style="color: #757575;" action="#!">
                            <!-- Name -->
                            <div class="form-row">
                                <div class="col">
                                    <!-- First name -->
                                    <div class="md-form">
                                        <span>Project Name</span>
                                        <select searchable="Search here.."  class="mdb-select">
                                            <option value="" disabled>Please Select</option>
                                            <option value="1">John</option>
                                            <option value="2">Doe</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col">
                                    <!-- Last name -->
                                    <div class="md-form">
                                        <span>Contractor</span>
                                        <select searchable="Search here.."  class="mdb-select">
                                            <option value="" disabled>Please Select</option>
                                            <option value="1">New York</option>
                                            <option value="2">London</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col">
                                    <!-- First name -->
                                    <div class="md-form">
                                        <span>Title</span>
                                        <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                    </div>
                                </div>
                                <div class="col">
                                    <!-- Last name -->
                                    <div class="md-form">
                                        <span>Contract Manager</span>
                                        <select searchable="Search here.."  class="mdb-select">
                                            <option value="" disabled>Please Select</option>
                                            <option value="1">New York</option>
                                            <option value="2">London</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                           
                             <br>
                            <div class="card">
                                <h5 class="card-header info-color white-text text-center py-1s">
                                     <strong>Time select</strong>
                                  </h5>
                                <div class="card-body">
                                     <div class="row">
                                <div class="col">
                                    <!-- First name -->
                                    <div class="md-form">
                                        <span>Time</span>
                                         <input type="time" class="form-control validate @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                    </div>
                                    </div>
                                    <div class="col">
                                        <!-- Last name -->
                                        <div class="md-form">
                                            <span>Pre Time</span>
                                             <input type="time" class="form-control validate @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <!-- First name -->
                                        <div class="md-form">
                                            <span>Post Time</span>
                                             <input type="time" class="form-control validate @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                        </div>
                                    </div>
                                </div>
                                    
                              
                                </div>
                            </div>
                            <br>
                            <div class="card">
                                <h5 class="card-header info-color white-text text-center py-1s">
                                     <strong>Questions</strong>
                                  </h5>
                                <div class="card-body">
                                    <div class="row">
                                         <div class="col">
                                            <div class="md-form">
                                                <input type="text" id="date-picker-example" class="form-control datepicker">
                                                <label for="date-picker-example">Question</label>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <div class="md-form">
                                                <span>Question Type</span>
                                                <select searchable="Search here.."  class="mdb-select">
                                                    <option value="" disabled>Please Select</option>
                                                    <option value="1">30</option>
                                                    <option value="2">60</option>
                                                    <option value="2">90</option>
                                                </select>
                                            </div>
                                        </div>
                                       
                                        <div class="col">
                                            <div class="md-form">
                                                <span>Task Type</span>
                                                <select searchable="Search here.."  class="mdb-select">
                                                    <option value="" disabled>Please Select</option>
                                                    <option value="1">30</option>
                                                    <option value="2">60</option>
                                                    <option value="2">90</option>
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                   
                                </div>
                            </div>
                            <br>
                            <div class="card">
                                <h5 class="card-header info-color white-text text-center py-1s">
                                     <strong>Billing and Linking</strong>
                                  </h5>
                                <div class="card-body">
                                   
                                    <div class="row">
                                        <div class="col">
                                            <div class="md-form">
                                                <span>Task Group</span>
                                                <select searchable="Search here.."  class="mdb-select">
                                                    <option value="" disabled>Please Select</option>
                                                    <option value="1">30</option>
                                                    <option value="2">60</option>
                                                    <option value="2">90</option>
                                                </select>
                                            </div>
                                        </div>
                                       
                                        <div class="col">
                                            <div class="md-form">
                                                <span>Choose Payment Type</span>
                                                <select searchable="Search here.."  class="mdb-select">
                                                    <option value="" disabled>Please Select</option>
                                                    <option value="1">30</option>
                                                    <option value="2">60</option>
                                                    <option value="2">90</option>
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                   
                                </div>
                            </div>
                            <br>
                             <div class="md-form mb-5">
                                <div id="document-full-1" class="ql-scroll-y" style="height: 300px;">
                                    
                                </div>
                              
                            </div> 

                        </form>
                        <!-- Form -->
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button class="btn btn-primary btn-sm" type="submit"><span class="assign_class label{{getKeyid('save',$data)}}" data-id="{{getKeyid('save',$data) }}" data-value="{{checkKey('save',$data) }}" >{!! checkKey('save',$data) !!} </span></button>
                </div>
            </form>

            <div class="modal-body mx-3">

           
            </div>
        </div>
    </div>
</div>

<script src="{{asset('editor/sprite.svg.js')}}"></script>
<script type="text/javascript">
   var toolbarOptions = [
      [{
        'header': [1, 2, 3, 4, 5, 6, false]
      }],
      ['bold', 'italic', 'underline', 'strike'], // toggled buttons
      ['blockquote', 'code-block'],

      [{
        'header': 1
      }, {
        'header': 2
      }], // custom button values
      [{
        'list': 'ordered'
      }, {
        'list': 'bullet'
      }],
      [{
        'script': 'sub'
      }, {
        'script': 'super'
      }], // superscript/subscript
      [{
        'indent': '-1'
      }, {
        'indent': '+1'
      }], // outdent/indent
      [{
        'direction': 'rtl'
      }], // text direction

      [{
        'size': ['small', false, 'large', 'huge']
      }], // custom dropdown

      [{
        'color': []
      }, {
        'background': []
      }], // dropdown with defaults from theme
      [{
        'font': []
      }],
      [{
        'align': []
      }],
      ['link', 'image'],

      ['clean'] // remove formatting button
    ];

    var quillFull = new Quill('#document-full-1', {
      modules: {
        toolbar: toolbarOptions,
        autoformat: true
      },
      theme: 'snow',
      placeholder: "Write something..."
    });

    
</script>
