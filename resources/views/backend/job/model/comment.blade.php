<div class="modal fade" id="commentModal{{$job->form_Section->id.$question->id}}" role="dialog">
    <div class="modal-dialog modal-lg">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          Comments and Raising Job
        </div>
        <div class="modal-body">
                  <div class="md-form">

                    <textarea id="comment{{$job->form_Section->id.'.'.$question->id}}" name="comment.{{$job->form_Section->id.'.'.$question->id}}" class="md-textarea form-control" rows="3"></textarea>
                    <label for="comment">Comments</label>
                    <small id="er_comment" class="text-danger error_message"></small>
                  </div>
                  <label>Do you want to Raise Job?</label>
                  <br>

                   <div class="form-check form-check-inline">
                      <input type="radio" class="form-check-input" id="yes" checked value="1" name="toggleDiv">
                      <label class="form-check-label" for="yes"><span class="assign_class label{{getKeyid('report_label_yes',$data)}}" data-id="{{getKeyid('report_label_yes',$data) }}" data-value="{{checkKey('report_label_yes',$data) }}" >
                        {!! checkKey('report_label_yes',$data) !!}
                    </span></label>
                    </div>

                    <!-- Group of material radios - option 2 -->
                    <div class="form-check form-check-inline">
                      <input type="radio" class="form-check-input" id="no"   value="0" name="toggleDiv">
                      <label class="form-check-label" for="no"><span class="assign_class label{{getKeyid('report_label_no',$data)}}" data-id="{{getKeyid('report_label_no',$data) }}" data-value="{{checkKey('report_label_no',$data) }}" >
                        {!! checkKey('report_label_no',$data) !!}
                    </span></label>
                    </div>





              <div id='off_div'>
                   <input type="hidden" name="location_id" value="{{$job_group->location_id}}">
                   <input type="hidden" name="location_breakdown_id" value="{{$job_group->location_breakdown_id}}">
                       <div class="md-form">
                          {!! Form::select('user_id2',$users,null, ['class' =>' mdb-select ','id'=>'user_id2','searchable'=>'search here','placeholder'=>'Users']) !!}
                           <small id="er_user_id" style="left: 0%!important;margin-top:-15px;display:block; text-transform: initial !important;" class="text-danger error_message"></small>

                        </div>


                        <div class="md-form">
                          {!! Form::select('form_type_id',$types, null, ['class' =>'mdb-select','placeholder'=>'Form Type','id'=>'form_type2','searchable'=>'search here']) !!}
                           <small id="er_form_type" style="left: 0%!important;margin-top:-15px;display:block; text-transform: initial !important;" class="text-danger error_message"></small>
                        </div>

                        <div class="md-form" id="form_div2">
                            {!! Form::select('form_id2',[],null, ['class' =>' mdb-select ','id'=>'form_id2','searchable'=>'search here','placeholder'=>'Forms']) !!}
                        </div>
                         <small id="er_form_id" style="left: 0%!important;margin-top:-15px;display:block; text-transform: initial !important;" class="text-danger error_message"></small>
                   <div class="md-form">
                      <input placeholder="Select date" type="text" id="date_pick" name="date" class="form-control datepicker">
                      <small id="er_date" class="text-danger error_message"></small>
                    </div>

                    <div class="md-form">
                      <input placeholder="Selected time" type="text" id="input_starttime" name="time" class="form-control timepicker">
                      <small id="er_time" class="text-danger error_message"></small>
                    </div>

            </div>


        </div>
        <div class="modal-footer">
              <button type="button" class="btn btn-default"  onclick="fillComment('{{$job->form_Section->id.'.'.$question->id}}')">Add</button>
              <button type="button" class="btn btn-default" data-dismiss="modal"  onclick="closeRadio('{{$job->form_Section->id.'.'.$question->id}}')">Close</button>
        </div>
      </div>

    </div>
  </div>
  <script type="text/javascript">
      $('.datepicker').pickadate({selectYears:250,});
    $('.timepicker').pickatime();
        $("input[name='toggleDiv']").change(function(){
      console.log("asdasdadad");
      console.log($("input[name='toggleDiv']:checked").val());
      if($("input[name='toggleDiv']:checked").val()==0)
      {
        $("#off_div").hide();

      }
      else
      {
        $("#off_div").show();

      }

});
  </script>
