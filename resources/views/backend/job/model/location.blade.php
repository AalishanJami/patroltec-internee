@php
    $data=localization();
@endphp
<link rel="stylesheet" href="{{ asset('custom/css/custom_style.css') }}">

<style type="text/css">

    /*    #location_table>tbody>tr>td:nth-child(2){*/
    /*        width: 7%;*/
    /*        text-align: center;*/
    /*    }*/
    /*    #location_table>tbody>tr>td:nth-child(1){*/
    /*        width: 7%;*/
    /*    }*/

    /*    #location_table>tbody>tr>td:nth-child(4){*/
    /*        width: 7%;*/
    /*        text-align: center;*/
    /*    }*/
    /* */
</style>
<div class="modal fade" id="modallocationsIndex" tabindex="-1" data-keyboard="false" data-backdrop="static" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">
                    <span class="assign_class label{{getKeyid('location',$data)}}" data-id="{{getKeyid('location',$data) }}" data-value="{{checkKey('location',$data) }}" >
                        {!! checkKey('location',$data) !!}
                    </span>
                    @if(Auth()->user()->hasPermissionTo('view_cms') || Auth::user()->all_companies == 1 )
                        <button data-class="edit-disable" class="edit_cms_enable edit_cms_switch btn btn-primary btn-sm waves-effect waves-light" style="display: none;" >
                            Edit cms
                        </button>
                        <button  data-class="edit-enable" class="edit_cms_disable edit_cms_switch btn btn-danger btn-sm waves-effect waves-light">
                            Edit cms
                        </button>
                    @endif
                </h4>
                <button type="button" class="close location_listing_close" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="card">
                <div class="card-body">
                    <div id="table-location" class="table-editable">
                        @if(Auth()->user()->hasPermissionTo('create_division') || Auth::user()->all_companies == 1 )
                            <span class="add-location table-add float-right mb-3 mr-2">
                                <a class="text-success"><i class="fas fa-plus fa-2x" aria-hidden="true"></i></a>
                            </span>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_delete_division') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-danger btn-sm" id="deleteselectedlocation">
                                <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                                    {!! checkKey('selected_delete',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_delete_division') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-danger btn-sm" style="display: none;" id="deleteselectedlocationSoft">
                                <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                                    {!! checkKey('selected_delete',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_active_division') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-success btn-sm" style="display: none;" id="selectedactivebuttonlocation">
                                <span class="assign_class label{{getKeyid('selected_active',$data)}}" data-id="{{getKeyid('selected_active',$data) }}" data-value="{{checkKey('selected_active',$data) }}" >
                                    {!! checkKey('selected_active',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('restore_delete_division') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-primary btn-sm" id="restorebuttonlocation">
                                <span class="assign_class label{{getKeyid('restore',$data)}}" data-id="{{getKeyid('restore',$data) }}" data-value="{{checkKey('restore',$data) }}" >
                                    {!! checkKey('restore',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('active_division') || Auth::user()->all_companies == 1 )
                            <button  style="display: none;" class="btn btn-primary btn-sm" id="activebuttonlocation">
                               <span class="assign_class label{{getKeyid('show_active',$data)}}" data-id="{{getKeyid('show_active',$data) }}" data-value="{{checkKey('show_active',$data) }}" >
                                    {!! checkKey('show_active',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('csv_division') || Auth::user()->all_companies == 1 )
                        <!--  <form class="form-style" method="POST" id="export_excel_division" action="{{ url('location/export/excel') }}">
                            {!! csrf_field() !!}
                            <input type="hidden" class="location_export" name="excel_array" value="1">
                            <button  type="submit" class="form_submit_check btn btn-warning btn-sm">
                                <span class="assign_class label{{getKeyid('excel_export',$data)}}" data-id="{{getKeyid('excel_export',$data) }}" data-value="{{checkKey('excel_export',$data) }}" >
                                    {!! checkKey('excel_export',$data) !!}
                            </span>
                        </button>
                    </form> -->
                        @endif
                        @if(Auth()->user()->hasPermissionTo('word_division') || Auth::user()->all_companies == 1 )
                        <!--     <form class="form-style" method="POST" id="export_world_division" action="{{ url('location/export/world') }}">
                            {!! csrf_field() !!}
                            <input type="hidden" class="location_export" name="word_array" value="1">
                            <button  type="submit" class="form_submit_check btn btn-success btn-sm">
                                <span class="assign_class label{{getKeyid('word_export',$data)}}" data-id="{{getKeyid('word_export',$data) }}" data-value="{{checkKey('word_export',$data) }}" >
                                    {!! checkKey('word_export',$data) !!}
                            </span>
                        </button>
                    </form> -->
                        @endif
                        @if(Auth()->user()->hasPermissionTo('pdf_division') || Auth::user()->all_companies == 1 )
                        <!--  <form  class="form-style"  method="POST" id="export_pdf_division" action="{{ url('location/export/pdf') }}">
                            {!! csrf_field() !!}
                            <input type="hidden" class="location_export" name="pdf_array" value="1">
                            <button  type="submit" class="form_submit_check btn btn-secondary btn-sm">
                                <span class="assign_class label{{getKeyid('pdf_export',$data)}}" data-id="{{getKeyid('pdf_export',$data) }}" data-value="{{checkKey('pdf_export',$data) }}" >
                                    {!! checkKey('pdf_export',$data) !!}
                            </span>
                        </button>
                    </form> -->
                        @endif
                        <div id="table" class="table-editable">
                            <table id="location_table" class=" table table-bordered table-responsive-md table-striped">
                                <thead>
                                <tr>
                                    @if(Auth()->user()->hasPermissionTo('division_checkbox') || Auth::user()->all_companies == 1 )
                                        <th class="no-sort all_checkboxes_style">
                                            <div class="form-check">
                                                <input type="checkbox" class="form-check-input location_checked" id="location_checkbox_all">
                                                <label class="form-check-label" for="location_checkbox_all">
                                                    <span class="assign_class label{{getKeyid('all',$data)}}" data-id="{{getKeyid('all',$data) }}" data-value="{{checkKey('all',$data) }}" >
                                                        {!! checkKey('all',$data) !!}
                                                    </span>
                                                </label>
                                            </div>
                                        </th>
                                    @endif
                                    @if(Auth()->user()->hasPermissionTo('division_name') || Auth::user()->all_companies == 1 )
                                        <th>
                                            <span class="assign_class label{{getKeyid('name',$data)}}" data-id="{{getKeyid('name',$data) }}" data-value="{{checkKey('name',$data) }}" >
                                                {!! checkKey('name',$data) !!}
                                            </span>
                                        </th>
                                    @endif
                                    @if(Auth()->user()->hasPermissionTo('edit_division') || Auth::user()->all_companies == 1 )
                                        <th class="no-sort all_action_btn"></th>
                                    @endif
                                </tr>
                                </thead>
                                <tbody id="location_table_body"></tbody>
                            </table>
                        </div>
                        @if(Auth()->user()->all_companies == 1)
                            <input type="hidden" id="joblocation_checkbox" value="1">
                            <input type="hidden" id="delete_joblocation" value="1">
                            <input type="hidden" id="hard_delete_joblocation" value="1">
                            <input type="hidden" id="joblocation_name" value="1">
                            <input type="hidden" id="edit_joblocation" value="1">
                            <input type="hidden" id="joblocation_name_edit" value="1">
                            <input type="hidden" id="joblocation_name_create" value="1">
                        @else
                            <input type="hidden" id="joblocation_checkbox" value="{{Auth()->user()->hasPermissionTo('joblocation_checkbox')}}">
                            <input type="hidden" id="delete_joblocation" value="{{Auth()->user()->hasPermissionTo('delete_joblocation')}}">
                            <input type="hidden" id="hard_delete_joblocation" value="{{Auth()->user()->hasPermissionTo('hard_delete_joblocation')}}">
                            <input type="hidden" id="joblocation_name" value="{{Auth()->user()->hasPermissionTo('joblocation_name')}}">
                            <input type="hidden" id="edit_joblocation" value="{{Auth()->user()->hasPermissionTo('edit_joblocation')}}">
                            <input type="hidden" id="joblocation_name_edit" value="{{Auth()->user()->hasPermissionTo('joblocation_name_edit')}}">
                            <input type="hidden" id="joblocation_name_create" value="{{Auth()->user()->hasPermissionTo('joblocation_name_create')}}">
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function location_data(url) {
        $.ajax({
            type: 'GET',
            url: url,
            success: function(response)
            {
                var table = $('#location_table').dataTable(
                    {
                        processing: true,
                        language: {
                            'search':'<span class="assign_class label'+$('#search_id').attr('data-id')+'" data-id="'+$('#search_id').attr('data-id')+'" data-value="'+$('#search_id').val()+'" >'+$('#search_id').val()+'</span>',
                            'paginate':{
                                'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                                'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                            },
                            'lengthMenu': '_MENU_',
                            'info': ' ',
                        },
                        "ajax": {
                            "url": url,
                            "type": 'get',
                        },
                        "createdRow": function( row, data,dataIndex,columns )
                        {
                            var checkbox_permission=$('#checkbox_permission').val();
                            var checkbox='';
                            checkbox+='<div class="form-check"><input type="checkbox" class="form-check-input location_checked selectedrowlocation" id="location'+data.id+'"><label class="form-check-label" for="location'+data.id+'""></label></div>';
                            var submit='';
                            submit+='<a type="button" class="btn btn-primary btn-xs my-0 all_action_btn_margin waves-effect waves-light savelocationdata location_show_button_'+data.id+'" style="display: none;" id="'+data.id+'"><i class="fas fa-check"></i></a>';
                            $(columns[0]).html(checkbox);
                            $(columns[1]).attr('id', 'nameD'+data['id']);
                            $(columns[1]).attr('data-id', data['id']);
                            $(columns[1]).attr('onkeydown', 'editSelectedRow('+"location_tr"+data['id']+')');
                            if ($('#division_name_edit').val()==1){
                                $(columns[1]).attr('Contenteditable', 'true');
                            }
                            if($('#edit_division').val()==1) {
                                $(columns[2]).append(submit);
                            }
                            $(columns[1]).attr('class','edit_inline_location');
                            $(row).attr('id', 'location_tr'+data['id']);
                            //$(row).attr('class', 'selectedrowlocation');
                        },
                        columns:
                            [
                                {data: 'checkbox', name: 'checkbox'},
                                {data: 'name', name: 'name'},
                                {data: 'actions', name: 'actions'},
                            ],

                    }
                );
            },
            error: function (error) {
                console.log(error);
            }
        });
        if ($(":checkbox").prop('checked',true)){
            $(":checkbox").prop('checked',false);
        }
    }
 

    $(document).ready(function () {
        $("body").on('click','.edit_inline_location',function () {
            $(".location_show_button_"+$(this).attr('data-id')).show();
        });
        $('.location_listing_close').click(function(){
            $.ajax({
                type: "GET",
                url: 'job/location/getall/'+($('#location_id').children("option:selected").val()),
                success: function(response)
                {
                    var html='';
                    html+='<select name="location_breakdown_name" class="mdb-select mdb-select-custom-form" id="location_break_id" required placeholder="please select">';
                    html+='<option value="0">please Select</option>';
                    console.log(response);
                    for (var i = response.break.length - 1; i >= 0; i--) {
                        var selected='';

                        html+='<option '+selected+' value="'+response.break[i].id+'">'+response.break[i].name+'</option>';
                    }
                    html+='</select> ';
                    $('#location_break_div').empty();
                    $('#location_break_div').html(html);
                    $('#location_break_div').empty();
                    console.log(html, "html");
                    $('#location_break_div').html(html);
                    $('#location_break_id').materialSelect();
                    $('select.mdb-select-custom-form').hide();
                    $('#modallocationsIndex').modal('hide');

                },
                error: function (error) {
                    console.log(error);
                }
            });

        });
    });
</script>
<script src="{{asset('/custom/js/location.js')}}"></script>

