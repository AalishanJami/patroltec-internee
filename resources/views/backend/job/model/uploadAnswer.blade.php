<div class="modal fade" id="answer_upload{{$question->id}}" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h1>{{$question->question}}</h1>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
                     <div class="file-upload-wrapper">
                        <input type="file" id="input-file-now"                                              name="attachments.{{$job->form_Section->id.'.'.$question->id}}" class="file-upload" data-default-file="{{asset('/uploads/response').'/'.($response_groups[$index]->responses[$key]->file_name ?? false)}}" />
                  </div>
        </div>
        <div class="modal-footer">
       <button type="button" class="btn btn-default" data-dismiss="modal">Add</button>
        </div>
      </div>
      
    </div>
  </div>