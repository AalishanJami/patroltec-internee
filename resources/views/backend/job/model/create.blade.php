@php
    $data=localization();
@endphp
<style type="text/css">
    #bulk_upload_img_loader{
        position: absolute;
        z-index: 1000;
        top: 19%;
        left: 41%;
        width: 150px;
        height: 150px;
        display: none;
    }
</style>
<div class="modal fade" id="modalJob" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">
                  <span class="assign_class label{{getKeyid('jobs',$data)}}" data-id="{{getKeyid('jobs',$data) }}" data-value="{{checkKey('jobs',$data) }}" >
                        {!! checkKey('jobs',$data) !!}
                    </span>
                    @if(Auth()->user()->hasPermissionTo('view_cms') || Auth::user()->all_companies == 1 )
                        <button data-class="edit-disable" class="edit_cms_enable edit_cms_switch btn btn-primary btn-sm waves-effect waves-light" style="display: none;" >
                            Edit cms
                        </button>
                        <button  data-class="edit-enable" class="edit_cms_disable edit_cms_switch btn btn-danger btn-sm waves-effect waves-light">
                            Edit cms
                        </button>
                    @endif
                </h4>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{url('/job/store')}}" novalidate id="job_store_submit" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">

                    <!--Card content-->
                    <div class="card-body px-lg-5 pt-0">
                        <!-- Form -->

                        <!-- Name -->
                        <div class="form-row">
                            <div class="col">
                                <!-- First name -->
                                <div class="md-form">
                                         <span class="assign_class label{{getKeyid('project_name',$data)}}" data-id="{{getKeyid('project_name',$data) }}" data-value="{{checkKey('project_name',$data) }}">
                                         {!! checkKey('project_name',$data) !!}
                                        </span>
                                    <select name="location_id" class="mdb-select location_id_select" searchable='search here' id="location_id">
                                        <option value="" selected disabled>Please Select</option>
                                        @foreach($locations as $key=>$value)
                                            <option value="{{$key}}">{{$value}}</option>
                                        @endforeach
                                    </select>
{{--                                    {!! Form::select('location_id',$locations,0, ['class' =>'mdb-select location_id_select','id'=>'location_id','placeholder'=>'Please Select','searchable'=>'search here']) !!}--}}
                                    <small id="er_project_name" class="text-danger error_message"></small>
                                </div>
                            </div>
                            <div class="col">
                                <!-- Last name -->
                                <div class="md-form">
                                    <span class="assign_class label{{getKeyid('assign_to',$data)}}" data-id="{{getKeyid('assign_to',$data) }}" data-value="{{checkKey('assign_to',$data) }}" >
                                        {!! checkKey('assign_to',$data) !!}
                                    </span>
                                    <select name="user_id[]" class="mdb-select user_id_select" searchable='search here' id="user_id" multiple>
                                        <option value="" selected disabled>Please Select</option>
                                        @foreach($users as $key=>$value)
                                            <option value="{{$key}}">{{$value}}</option>
                                        @endforeach
                                    </select>

{{--                                    {!! Form::select('user_id[]',$users,null, ['class' =>' mdb-select user_id_select','id'=>'user_id','placeholder'=>'Please Select','searchable'=>'search here','multiple']) !!}--}}
                                    <small id="er_user_id" class="text-danger error_message"></small>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                <!-- First name -->
                                <div class="col modal_location_col">
                                    <a class="text-success modal_location">
                                        <button type="button" class="btn btn-sm btn-outline-primary waves-effect form-custom-boutton" style="float: left;">
                                            <span class="assign_class label{{getKeyid('location_break_down',$data)}}" data-id="{{getKeyid('location_break_down',$data) }}" data-value="{{checkKey('location_break_down',$data) }}" >
                                                    {!! checkKey('location_break_down',$data) !!}
                                                </span>
                                        </button>
                                    </a>
                                </div>

                                <div class="md-form" id="location_break_div">
                                    <select name="location_breakdown_name" class="mdb-select location_break_id" searchable='search here' id="location_break_id">
                                        <option value="" selected disabled>Please Select</option>
                                        @foreach($locationbreaks as $key=>$value)
                                            <option value="{{$key}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                    <small id="er_location_break_id" class="text-danger error_message"></small>
                                </div>


                            </div>
                            <div class="col">
                                <!-- Last name -->
                                <div class="md-form job_date_pick_section">
                                          <span class="assign_class label{{getKeyid('date',$data)}}" data-id="{{getKeyid('date',$data) }}" data-value="{{checkKey('date',$data) }}" >
                                            {!! checkKey('date',$data) !!}
                                        </span>
                                    <input placeholder="Select date" value="{{$date}}" type="text" id="date_pick" name="date" class="form-control datepicker" >
                                    <small id="er_date" class="text-danger error_message"></small>
                                </div>
                            </div>
                        </div>

                        <br>
                        <div class="card">
                            <h5 class="card-header info-color white-text  py-1s">
                                <strong>
                                    <span class="assign_class label{{getKeyid('time_select',$data)}}" data-id="{{getKeyid('time_select',$data) }}" data-value="{{checkKey('time_select',$data) }}" >
                                            {!! checkKey('time_select',$data) !!}
                                        </span>
                                </strong>
                            </h5>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <!-- First name -->
                                        <div class="md-form">
                                          <span class="assign_class label{{getKeyid('time',$data)}}" data-id="{{getKeyid('time',$data) }}" data-value="{{checkKey('time',$data) }}" >
                                            {!! checkKey('time',$data) !!}
                                        </span>
                                            <input placeholder="Selected time" type="text" id="input_starttime" name="time" class="form-control timepicker">
                                            <small id="er_time" class="text-danger error_message"></small>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="md-form">
                                                 <span class="assign_class label{{getKeyid('pre',$data)}}" data-id="{{getKeyid('pre',$data) }}" data-value="{{checkKey('pre',$data) }}" >
                                            {!! checkKey('pre',$data) !!}
                                        </span>
                                            <input type="number" min="1" value="1" class="form-control pre_time" name="pre_time">
                                            <small id="pre_time" class="text-danger error_message"></small>
                                        </div>

                                    </div>
                                    <div class="col">
                                        <div class="md-form">
                                                  <span class="assign_class label{{getKeyid('time',$data)}}" data-id="{{getKeyid('time',$data) }}" data-value="{{checkKey('time',$data) }}" >
                                            {!! checkKey('time',$data) !!}
                                        </span>
                                            {!! Form::select('pre_time_select',['1'=>'Min','2'=>'Hrs','3'=>'Days','4'=>'Week'], '1', ['class' =>'mdb-select','id'=>'pre_time_select']) !!}
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="md-form">
                                                 <span class="assign_class label{{getKeyid('post',$data)}}" data-id="{{getKeyid('post',$data) }}" data-value="{{checkKey('post',$data) }}" >
                                            {!! checkKey('post',$data) !!}
                                        </span>
                                            <input type="number" min="1" value="1" class="form-control post_time" name="post_time">
                                            <small id="post_time" class="text-danger error_message"></small>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="md-form">
                                                  <span class="assign_class label{{getKeyid('time',$data)}}" data-id="{{getKeyid('time',$data) }}" data-value="{{checkKey('time',$data) }}" >
                                            {!! checkKey('time',$data) !!}
                                        </span>
                                            {!! Form::select('post_time_select',['1'=>'Min','2'=>'Hrs','3'=>'Days','4'=>'Week'], null, ['class' =>'mdb-select','id'=>'post_time_select']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="card">
                                <h5 class="card-header info-color white-text  py-1s">
                                    <strong>  <span class="assign_class label{{getKeyid('clear_up_notices',$data)}}" data-id="{{getKeyid('clear_up_notices',$data) }}" data-value="{{checkKey('clear_up_notices',$data) }}" >
                                            {!! checkKey('clear_up_notices',$data) !!}
                                        </span></strong>
                                </h5>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col">
                                            <div class="md-form" id="contact_div">
                                                  <span class="assign_class label{{getKeyid('user_name',$data)}}" data-id="{{getKeyid('user_name',$data) }}" data-value="{{checkKey('user_name',$data) }}" >
                                                    {!! checkKey('user_name',$data) !!}
                                                </span>
                                                {!! Form::select('notifiable[]',$users,0, ['class' =>' mdb-select notifiable_select','id'=>'notifiable','placeholder'=>'Please Select','searchable'=>'search here','multiple']) !!}
                                                <small id="er_notifiable" class="text-danger error_message"></small>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <div class="md-form">
                                                  <span class="assign_class label{{getKeyid('form_type',$data)}}" data-id="{{getKeyid('form_type',$data) }}" data-value="{{checkKey('form_type',$data) }}" >
                                                      {!! checkKey('form_type',$data) !!}
                                                  </span>
                                                {!! Form::select('form_type_id',$types, 0, ['class' =>'mdb-select form_type_id_select','placeholder'=>'Form Type','id'=>'form_type','searchable'=>'search here']) !!}
                                                <small id="er_form_type" class="text-danger error_message"></small>

                                            </div>
                                        </div>

                                        <div class="col">
                                            <div class="md-form" id="form_div">
                                                 <span class="assign_class label{{getKeyid('form_name',$data)}}" data-id="{{getKeyid('form_name',$data) }}" data-value="{{checkKey('form_name',$data) }}" >
                                            {!! checkKey('form_name',$data) !!}
                                        </span>
                                                {!! Form::select('form_id',[],0, ['class' =>' mdb-select form_id_select','id'=>'form_id','placeholder'=>'Please Select','searchable'=>'search here']) !!}
                                                <small id="er_form_id" class="text-danger error_message"></small>

                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="version_id" id="version_id">

                                    <div class="row">

                                        <div class="col">
                                            <div class="md-form" id='populate_div' >
                                                  <span class="assign_class label{{getKeyid('populate_check',$data)}}" data-id="{{getKeyid('populate_check',$data) }}" data-value="{{checkKey('populate_check',$data) }}" >
                                                      {!! checkKey('populate_check',$data) !!}
                                                  </span>
                                                {!! Form::select('pre_populate_id',[],0, ['class' =>' mdb-select pre_populate_id_select','id'=>'pre_populate_id','placeholder'=>'Please Select','searchable'=>'search here']) !!}
                                            </div>
                                        </div>
                                    <!--    <div class="col">
                                                <input type="checkbox" class="form-check-input" value="1" id="populate_check" name="populate_check" checked>
                                                <label class="form-check-label" for="populate_check"> <span class="assign_class label{{getKeyid('pre_populate',$data)}}" data-id="{{getKeyid('pre_populate',$data) }}" data-value="{{checkKey('pre_populate',$data) }}" >
                                                      {!! checkKey('pre_populate',$data) !!}
                                        </span></label>
                                  </div> -->

                                    </div>

                                </div>
                            </div>
                            <br>
                            <div class="card">
                                <h5 class="card-header info-color white-text  py-1s">
                                    <strong><span class="assign_class label{{getKeyid('bill_link',$data)}}" data-id="{{getKeyid('bill_link',$data) }}" data-value="{{checkKey('bill_link',$data) }}" >
                                                      {!! checkKey('bill_link',$data) !!}
                                                  </span></strong>
                                </h5>
                                <div class="card-body">

                                    <div class="row">
                                        <div class="col">
                                            <div class="md-form">
                                                <span class="assign_class label{{getKeyid('total_cost',$data)}}" data-id="{{getKeyid('total_cost',$data) }}" data-value="{{checkKey('total_cost',$data) }}" >
                                                      {!! checkKey('total_cost',$data) !!}
                                                  </span>
                                                <input type="number" name="cost" class="form-control">
                                            </div>
                                        </div>

                                        <div class="col">
                                            <div class="md-form">
                                               <span class="assign_class label{{getKeyid('bill_info',$data)}}" data-id="{{getKeyid('bill_info',$data) }}" data-value="{{checkKey('bill_info',$data) }}" >
                                                      {!! checkKey('bill_info',$data) !!}
                                                  </span>
                                                <input type="text" name="billing_info" class="form-control ">
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>

                            <br>
                            <div class="md-form mb-5" id="note">
                                <div class="summernote_inner body_notes" ></div>
                            </div>
                            <input type="hidden" name="notes" id="note_value">
                            <br>
                            <div class="spinner-border text-success" id="bulk_upload_img_loader"></div>
                            <div class="file-upload-wrapper">

                                <input type="file" id="input-file-now" name='attachments' class="file-upload" />
                            </div>

                            <!-- Form -->
                        </div>

                    </div>

                    <div class="modal-footer d-flex justify-content-center">
                        <button class="btn btn-primary job_store_submit_button" type="submit">Save</button>
                    </div>

            </form>
        </div>
    </div>
</div>
@include('backend.job.model.location')

<script src="{{asset('editor/sprite.svg.js')}}"></script>
<script type="text/javascript">
    $('#input-file-now').file_upload();
    $(document).ready(function () {
        $("body").on('focusout','.note-editable',function () {
            $("#note_value").val($(this).html());
        });
    });
    var toolbarOptions = [
        ['bold', 'italic', 'underline'], // toggled buttons
        ['blockquote'],

        // custom button values
        [{
            'list': 'ordered'
        }, {
            'list': 'bullet'
        }],
        [{
            'indent': '-1'
        }, {
            'indent': '+1'
        }], // outdent/indent


        // dropdown with defaults from theme

        [{
            'align': []
        }],
        // remove formatting button
    ];

    var quillFull = new Quill('#document-full', {
        modules: {
            toolbar: toolbarOptions,
            autoformat: true
        },
        theme: 'snow',
        placeholder: "Write something..."
    });

    $('.datepicker').pickadate({selectYears:250,});
    $('.timepicker').pickatime();
    $('.modal_location').click(function(e) {
        if ($('.edit_cms_disable').css('display') == 'none') {
            return 0;
        }

       var location_id =  $('#location_id').children("option:selected").val();
       if(location_id == "" || location_id==null )
       {
        return toastr["error"]("please select project first!");
       }
        $('#modallocationsIndex').modal('show');
    });
</script>
