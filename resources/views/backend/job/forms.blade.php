@extends('backend.layouts.backend')
@section('content')
@php
$data=localization();
@endphp
   <style type="text/css">
        .nav-select{
            margin-left: 3rem !important;
            width: 40%;
        }
        .white-color
        {
            color:#fff;
        }
        .card-title
        {
            padding: 2%;
        }
        .md-tabs .nav-link.active, .md-tabs .nav-item.open .nav-link {
            color: #fff;
            background-color: rgba(0, 0, 0, 0.2);
            border-radius: 0.25rem;
            transition: all 1s;
        }
        #myTabMD
        {
            background-color: #33b5e5;
        }
        #loading_gif {
            position: absolute;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            z-index: 9999999;
        }

        .custom_dynamic_height>div>div>div>div{
            padding-bottom: 6px !important;
            padding-top: 6px !important;
            padding-right: 6px;
        }

        #answer_group_data>tr>td:nth-child(2){
            width: 19%;
            text-align: center;
        }
        #answer_group_data>tr>td:nth-child(2)>span{
            margin: 2%;
        }
        .img-class{
            width: 35px !important;
            max-width:100% !important;
            vertical-align: baseline !important;
        }
        .col-img-padding{
            padding-left: 0px !important;
        }

        .abswer_label_checkbox {
            width: 160px;
            text-align: left;
            overflow: hidden;
            text-overflow: ellipsis;
        }

        .abswer_label_radio {
            width: 100px;
            text-align: left;
            overflow: hidden;
            text-overflow: ellipsis;
        }
        .hover span {
            max-width: 0;
            -webkit-transition: max-width 1s;
            transition: max-width 1s;
            display: inline-block;
            vertical-align: top;
            white-space: nowrap;
            overflow: hidden;
        }
        .hover:hover span {
            max-width: 7rem;
        }
        .active_plus_btn_text{
            font-size: 16px !important;
            font-family: Roboto, Sans-Serif;
            font-weight: 400;
            line-height: 25px !important;
            margin-top: -4px;
        }
        .form_btn_circle>.fa-minus{
            margin-top: 6px;
        }
        .form_btn_circle>.fa-plus{
            margin-top:6px;
        }
        .form_btn_circle{
            vertical-align:middle;
        }
        .multiple-select-dropdown li [type="checkbox"] + label {
            height: 1.3rem !important;
        }
        .multiple-select-dropdown>.dropdown-content li{
            line-height: 2rem !important;
        }
        .img_style{
            margin-left: -50px;
        }
        .img_style_verti
        {
            margin-left:30px;
        }
        .img_style_verti_check
        {
            margin-left: 30px;
            margin-bottom: 5px;
        }

        #answerTableMain>tbody>tr>td:nth-child(1),#answerTableMain>tbody>tr>td:nth-child(4),#answerTableMain>tbody>tr>td:nth-child(3){
            width: 2%;
        }
    </style>
<style type="text/css">
  .white-color
  {
    color:#fff;
  }
  #pdf-viewer,#pdf-viewer-static
  {
    height: 700px;
  }
</style>
<div class="custom_clear locations-form container-fluid form_body">
  <div class="custom_clear card">

    <ul  class="custom_clear nav nav-tabs" id="myTab" role="tablist">
        <li class="custom_clear nav-item">
            <a class="custom_clear nav-link active" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile"
          aria-selected="false"> Dynamic</a>
        </li>
        @if($id_static)
        <li class="custom_clear nav-item">
            <a class="custom_clear nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact"
          aria-selected="false">Static</a>
        </li>
        @endif
          <li class="custom_clear nav-item">
            <a class="custom_clear nav-link" id="ins-tab" data-toggle="tab" href="#ins" role="tab" aria-controls="ins"
          aria-selected="false">Instructions</a>
        </li>
    </ul>
  <div class="custom_clear tab-content" id="myTabContent">
    <div class="custom_clear tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">

        <div class="active_plus_btn">
            <ul class="float-right  mr-4">
                <li class="hover">
                    <a class="btn btn-cyan px-3" onclick="save(false)"><i class="fas fa-save" aria-hidden="true"></i></a>
                </li>
                <li class="hover">
                    <a class="btn btn-cyan px-3" data-toggle="modal" data-target="#model_create_form_section" onclick="save(true)" ><i class="fas fa-check" aria-hidden="true"></i></a>
                </li>

            </ul>
        </div>


      <div class="card">
         <h5 class="card-header t py-4 white-color">
          <div class="row">
            <div class="col">
                <strong>
                  Form Name
                </strong>
            </div>
            <div class="col">
                <strong>
                  Person Name
                </strong>
            </div>
             <div class="col">
                <strong>
                  Location  Name
                </strong>
            </div>
            <div class="col">
                <strong>
                 Location BreakDown
                </strong>
            </div>
              <div class="col">
                <strong>
                 Date and Time
                </strong>
            </div>
          </div>
         <div class="row">
            <div class="col text-dark">
                <strong>
                 {{$job_group->forms->name?? "" }}
                </strong>
            </div>
            <div class="col text-dark">
                <strong>
                 {{$job_group->users->name ?? ""}}
                </strong>
            </div>
             <div class="col text-dark">
                <strong>
                 {{$job_group->locations->name ?? ""}}
                </strong>
            </div>
            <div class="col text-dark">
                <strong>
                 {{$job_group->locationBreakdowns->name ?? ""}}
                </strong>
            </div>
              <div class="col text-dark">
                @if(isset($jobs[0]))
                <strong>
                {{Carbon\Carbon::parse($jobs[0]->date_time)->format('jS M Y H:i')}}
                </strong>
                @endif
            </div>
          </div>

         </h5>
            @foreach($jobs as $index=> $job)
          <div id="{{$job->form_Section->order}}-{{$job->form_Section->id}}" class="custom_clear  sortable-card col-md-{{$job->form_Section->width}}">
            <div class="custom_clear mb-4 custom_dynamic_height" id="form_section_height{{$job->form_Section->id}}">
                <br>
              <div class="custom_clear card text-center  ">
                <div class="card-header" id="section_card_header{{$job->form_Section->id}}" style="display:flex; height:{{$job->form_Section->header_height ?? ''}}px">
                  <span class="custom_clear white-color ml-2" style="flex: 1; text-align:left;">{{$job->form_Section->header}}

                <!--   <button type="button" id='responseButton_{{$job->id}}' class="float-right btn btn-success btn-sm" onclick="response({{$job->id}},true)"><i class="fa fa-check"></i></button> -->

                </span>
                 <form method="post" action="{{url('job/replicate')}}">
                    @csrf
                    <input type="hidden" name="job_id" value="{{$job->id}}">
                    <button type="submit" class="btn btn-warning btn-sm"  style=" display:flex; flex: 1;"><i class="fa fa-copy"></i></button>
                  </form>
                </div>
                <form id="jobForm_{{$job->id}}" enctype="multipart/form-data">

                <input type="hidden" name="job_id" value="{{$job->id}}">
                <input type="hidden" name="populate_response_id" value="{{$job->pre_populate_id}}">

                <input type="hidden" name="job_group_id" value="{{$job_group->id}}">
                <input type="hidden" name="complete" id="complete_{{$job->id}}" value="false">
                <input type="hidden" name="form_id"  value="{{$job_group->form_id}}" id="form_id">
                <input type="hidden" name="location_id" value="{{$job_group->location_id}}">
                <input type="hidden" name="user_id" value="{{$job_group->user_id}}">
                @if(!$empty)

                  <input type="hidden" name="response_group_id" value="{{$response_groups[$index]->id}}">

                  @endif

                <div class="overflow-auto card-body section_custom_height" id="section_custom_height{{$job->form_Section->id}}">
                  <div class="row">
                  @foreach($job->form_Section->questionsS as $key=> $question)

                    @if($question->answer_type->name!='signature text' && $question->answer_type->name!='signature user' && $question->answer_type->name!='signature select' )
                    @include('backend.job.model.uploadAnswer')

                    @if($job->form_Section->form_section_column!== null)
                    <div class="form_section_columnsize col-md-{{(float)12/(float)$job->form_Section->form_section_column}}">
                    @else
                    <div class="form_section_columnsize col-md-12">
                    @endif
                    <div class="custom_clear card card-cascade wider reverse" style="padding-top: 10px;padding-bottom: 10px;">
                      <div class="row">
                          <p class="custom_clear card-title text-left" style="padding-left: 20px;">
                            <strong>{!!($question->question)!!}</strong> <button type="button" class="btn btn-sm  float-right" onclick="tp({{$question->id}})"><i class="far fa-image fa-2x"></i></button>
                          </p>
                        </div>
                        @if($question->layout == 1)
                          <div class="card-body hori_layout{{$question->layout}}">
                        @endif
                        <div class="row text-center">
                        @if($question->layout == 2)
                          @if($job->form_Section->width<=3)
                            <div class="col-md-12  verti">
                          @elseif($job->form_Section->width<=5)
                            <div class="col-md-9  verti">
                          @elseif($job->form_Section->width==6)
                            <div class="col-md-9  verti">
                          @else
                            <div class="col-md-6  verti">
                          @endif
                        @else
                          <div class="col-md-12 hori" style="padding-right: 40px;">
                        @endif

                              <div class="custom_clear row">
                                @switch ($question->answer_type->name)
                                  @case ('dropdown')
                                      <div class="custom_clear col-md-12" style="padding: 35px;text-align: center">
                                        <select searchable="Search here.." style="width: 100%;"  class="custom_clear mdb-select form_alignment" name="dropdown.{{$job->form_Section->id.'.'.$question->id}}">
                                          @if($question->answer_group)
                                            @foreach($question->answer_group->answer as $k=> $answer)
                                                @if($empty)
                                                   <option id="{{$job->form_Section->id.'.'.$question->id.'.'.$answer->id}}" value="{{$answer->id}}" >{{$answer->answer}}</option>
                                                @elseif($answer->id==($response_groups[$index]->responses[$key]->answer_id ?? false))
                                                <option id="{{$job->form_Section->id.'.'.$question->id.'.'.$answer->id}}" value="{{$answer->id}}" selected>{{$answer->answer}}</option>
                                                @else
                                                 <option id="{{$job->form_Section->id.'.'.$question->id.'.'.$answer->id}}" value="{{$answer->id}}" >{{$answer->answer}}</option>
                                                 @endif
                                            @endforeach
                                          @endif
                                        </select>
                                      </div>
                                  @break
                                  @case ('radio')
                                  @include('backend.job.model.comment')
                                      @if($question->layout == 2)
                                        @if($job->form_Section->width<=3)
                                          <div class="col-md-12  verti">
                                        @elseif($job->form_Section->width<=5)
                                          <div class="col-md-9  verti">
                                        @elseif($job->form_Section->width==6)
                                          <div class="col-md-9  verti">
                                        @else
                                          <div class="col-md-6  verti">
                                        @endif
                                      @else
                                        <div class="col-md-12 hori" style="padding-right: 40px;">
                                      @endif
                                      @if($question->answer_group)
                                        @foreach($question->answer_group->answer as $answer)
                                            @if($empty)
                                          <div class="custom_clear form-check float-left mt-3">
                                              <input type="radio" class="custom_clear form-check-input {{$answer->grade}}" id="answer_group{{$answer->id.'.'.$question->id}}" name="radio.{{$job->form_Section->id.'.'.$question->id}}" value="{{$answer->id}}" >
                                             <label class="custom_clear form-check-label radio_checkbox_label" for="answer_group{{$answer->id.'.'.$question->id}}">{{$answer->answer}}</label>
                                             <img src="{{url('/uploads/icons/'.$answer->icon)}}" style="width: 35px;height: 35px;margin-left: 0px;" alt="">
                                           </div>
                                             @elseif((!$response_groups[$index]->responses->where('answer_id',$answer->id)->where('question_id',$question->id)->isEmpty() )?? false)
                                            <div class="custom_clear form-check float-left mt-3">
                                             <input type="radio" class="custom_clear form-check-input {{$answer->grade}}" id="answer_group{{$answer->id.'.'.$question->id}}" name="radio.{{$job->form_Section->id.'.'.$question->id}}" value="{{$answer->id}}" checked>
                                             <label class="custom_clear form-check-label radio_checkbox_label" for="answer_group{{$answer->id.'.'.$question->id}}">{{$answer->answer}}</label>
                                             <img src="{{url('/uploads/icons/'.$answer->icon)}}" style="width: 35px;height: 35px;margin-left: 0px;" alt="">
                                           </div>
                                             @else
                                            <div class="custom_clear form-check float-left mt-3">
                                             <input type="radio" class="custom_clear form-check-input {{$answer->grade}}" id="answer_group{{$answer->id.'.'.$question->id}}" name="radio.{{$job->form_Section->id.'.'.$question->id}}" value="{{$answer->id}}">
                                             <label class="custom_clear form-check-label radio_checkbox_label" for="answer_group{{$answer->id.'.'.$question->id}}">{{$answer->answer}}</label>
                                             <img src="{{url('/uploads/icons/'.$answer->icon)}}" style="width: 35px;height: 35px;margin-left: 0px;" alt="">
                                           </div>
                                             @endif
                                        @endforeach
                                      @endif
                                    </div>
                                  @break
                                  @case ('checkbox')

                                                  @if($question->layout == 2)
                        @if($job->form_Section->width<=3)
                            <div class="col-md-12  verti">
                                @elseif($job->form_Section->width<=5)
                                    <div class="col-md-9  verti">
                                        @elseif($job->form_Section->width==6)
                                            <div class="col-md-9  verti">
                                                @else
                                                    <div class="col-md-6  verti">
                                                        @endif
                                                        @else
                                                            <div class="col-md-12 hori" style="padding-right: 40px;">
                                                                @endif
                                                                @if($question->answer_group)
                                                                  @foreach($question->answer_group->answer as $x=>$answer)
                                                          <div class="custom_clear form-check float-left mt-3">
                                                              @if($empty)
                                                                  <input type="checkbox" class="custom_clear form-check-input" id="answer_group_checkbox{{$question->id.'.'.$answer->id}}" name="checkbox.{{$job->form_Section->id.'.'.$question->id.'.'.$answer->id}}[]" value="{{$answer->id}}" >
                                                                  <label class="custom_clear form-check-label radio_checkbox_label" for="answer_group_checkbox{{$question->id.'.'.$answer->id}}">{{$answer->answer}}</label>
                                                                  <img src="{{url('/uploads/icons/'.$answer->icon)}}" style="width: 35px;height: 35px;margin-left: 0px;" alt="">

                                                              @elseif((!$response_groups[$index]->responses->where('answer_id',$answer->id)->where('question_id',$question->id)->isEmpty() )?? false)

                                                                  <input type="checkbox" class="custom_clear form-check-input" id="answer_group_checkbox{{$question->id.'.'.$answer->id}}" name="checkbox.{{$job->form_Section->id.'.'.$question->id.'.'.$answer->id}}" value="{{$answer->id}}" checked="checked">
                                                                  <label class="custom_clear form-check-label radio_checkbox_label" for="answer_group_checkbox{{$question->id.'.'.$answer->id}}">{{$answer->answer}}</label>
                                                                  <img src="{{url('/uploads/icons/'.$answer->icon)}}" style="width: 35px;height: 35px;margin-left: 0px;" alt="">
                                                              @else
                                                                  <input type="checkbox" class="custom_clear form-check-input" id="answer_group_checkbox{{$question->id.'.'.$answer->id}}" name="checkbox.{{$job->form_Section->id.'.'.$question->id.'.'.$answer->id}}" value="{{$answer->id}}">
                                                                  <label class="custom_clear form-check-label radio_checkbox_label" for="answer_group_checkbox{{$question->id.'.'.$answer->id}}">{{$answer->answer}}</label>
                                                                  <img src="{{url('/uploads/icons/'.$answer->icon)}}" style="width: 35px;height: 35px;margin-left: 0px;" alt="">
                                                              @endif
                                                          </div>

                                                      @endforeach
                                                  @endif
                                              </div>
                                              @if($question->layout == 2)

                                      </div>
                                  @endif
                                  @break
                                  @case ('multi')
                                    <div class="custom_clear col-md-12">
                                        <select searchable="Search here.." style="width: 100%;"  class="custom_clear mdb-select form_alignment" multiple  name="multi.{{$job->form_Section->id.'.'.$question->id}}">
                                          @if($question->answer_group)
                                            @foreach($question->answer_group->answer as $answer)
                                                @if($empty)
                                              <option id="{{$job->form_Section->id.'.'.$question->id.'.'.$answer->id}}" value="{{$answer->id}}" >{{$answer->answer}}</option>

                                             @elseif($answer->id==($response_groups[$index]->responses[$key]->answer_id ?? false))
                                                <option id="{{$job->form_Section->id.'.'.$question->id.'.'.$answer->id}}" value="{{$answer->id}}" selected>{{$answer->answer}}</option>
                                                @else
                                                <option id="{{$job->form_Section->id.'.'.$question->id.'.'.$answer->id}}" value="{{$answer->id}}" >{{$answer->answer}}</option>
                                                @endif
                                            @endforeach
                                          @endif
                                        </select>
                                    </div>
                                  @break
                                  @case ('date')
                                    <div class="custom_clear col-md-12 ">
                                      @if($question->answer_group)
                                        @if(isset($response_groups[$index]->responses[$key]))
                                        <div class="custom_clear md-form mt-5">
                                          <input type="text" placeholder="Date" id="date-picker-form" name="date.{{$job->form_Section->id.'.'.$question->id}}" class="custom_clear form_alignment form-control datepicker" value="{{$empty ? '' : $response_groups[$index]->responses[$key]->answer_text }}">
                                        </div>
                                         @else
                                           <div class="custom_clear md-form mt-5">
                                          <input type="text" placeholder="Date" id="date-picker-form" name="date.{{$job->form_Section->id.'.'.$question->id}}" class="custom_clear form_alignment form-control datepicker">
                                        </div>
                                        @endif
                                        @else
                                           <div class="custom_clear md-form mt-5">
                                          <input type="text" placeholder="Date" id="date-picker-form" name="date.{{$job->form_Section->id.'.'.$question->id}}" class="custom_clear form_alignment form-control datepicker">
                                        </div>
                                      @endif
                                    </div>
                                  @break
                                  @case ('time')
                                    <div class="custom_clear col-md-12 ">
                                      @if($question->answer_group)
                                        @if(isset($response_groups[$index]->responses[$key]))

                                        <div class="custom_clear md-form mt-5">
                                          <input type="text" placeholder="Time" id="date-picker-form" name="time.{{$job->form_Section->id.'.'.$question->id}}" class="custom_clear form-control form_alignment timepicker" value="{{$empty ? '' : $response_groups[$index]->responses[$key]->answer_text }}">
                                        </div>
                                        @else
                                           <div class="custom_clear md-form mt-5">
                                          <input type="text" placeholder="Time" id="date-picker-form" name="time.{{$job->form_Section->id.'.'.$question->id}}" class="custom_clear form-control form_alignment timepicker">
                                        </div>

                                      @endif
                                      @else
                                           <div class="custom_clear md-form mt-5">
                                          <input type="text" placeholder="Time" id="date-picker-form" name="time.{{$job->form_Section->id.'.'.$question->id}}" class="custom_clear form-control form_alignment timepicker">
                                        </div>
                                      @endif
                                    </div>
                                  @break
                                  @case ('datetime')
                                    <div class="custom_clear col-md-12 ">
                                      @if($question->answer_group)
                                        @if(isset($response_groups[$index]->responses[$key]))

                                        <div class="custom_clear md-form mt-5">
                                          <input type="text" placeholder="Date" id="date-picker-form" name="datetime.{{$job->form_Section->id.'.'.$question->id}}" class="custom_clear form-control form_alignment" value="{{$empty ? '' : $response_groups[$index]->responses[$key]->answer_text }}">
                                        </div>
                                        @else
                                         <div class="custom_clear md-form mt-5">
                                          <input type="text" placeholder="Date" id="date-picker-form" name="datetime.{{$job->form_Section->id.'.'.$question->id}}" class="custom_clear form-control form_alignment" >
                                        </div>
                                      @endif
                                      @else
                                         <div class="custom_clear md-form mt-5">
                                          <input type="text" placeholder="Date" id="date-picker-form" name="datetime.{{$job->form_Section->id.'.'.$question->id}}" class="custom_clear form-control form_alignment" >
                                        </div>
                                      @endif
                                    </div>
                                  @break
                                  @case ('text')
                                      <div class="custom_clear col-md-12 ">
                                        @if($question->answer_group)
                                        @if(isset($response_groups[$index]->responses[$key]))

                                          <div class="custom_clear md-form mt-5">
                                            <input  type="text" placeholder="Text" id="text-form"  name="text.{{$job->form_Section->id.'.'.$question->id}}" class="custom_clear  form-control form_alignment" value="{{$empty ? '' : $response_groups[$index]->responses[$key]->answer_text }}">
                                          </div>
                                          @else
                                            <div class="custom_clear md-form mt-5">
                                            <input  type="text" placeholder="Text" id="text-form"  name="text.{{$job->form_Section->id.'.'.$question->id}}" class="custom_clear  form-control form_alignment" >
                                          </div>
                                        @endif
                                        @else
                                            <div class="custom_clear md-form mt-5">
                                            <input  type="text" placeholder="Text" id="text-form"  name="text.{{$job->form_Section->id.'.'.$question->id}}" class="custom_clear  form-control form_alignment" >
                                          </div>
                                        @endif
                                      </div>
                                  @break
                                  @case ('comment')
                                      <div class="custom_clear col-md-12 text-left">

                                         {!! $question->comment !!}


                                      </div>
                                  @break
                                  @case ('richtext')
                                     @if($question->answer_group)
                                        @if(isset($response_groups[$index]->responses[$key]))

                                      <div class="md-form" style="padding-left:18px !important;width: 97% !important;">
                                        <textarea id="rich-text"  name="textarea.{{$job->form_Section->id.'.'.$question->id}}" rows="5" class="{{$job->form_Section->id.'-'.$question->id}} answertype_richtext " value="{{ $response_groups[$index]->responses[$key]->answer_text ?? ''}}"  ></textarea>
                                      </div>
                                      <script type="text/javascript">

                                       $(".{{$job->form_Section->id.'-'.$question->id}}").summernote('code',@json($response_groups[$index]->responses[$key]->answer_text ?? ''));
                                      </script>
                                        @else
                                        <div class="md-form" style="padding-left:18px !important;width: 97% !important;">
                                        <textarea id="rich-text"  name="textarea.{{$job->form_Section->id.'.'.$question->id}}" rows="5" class="{{$job->form_Section->id.'-'.$question->id}} answertype_richtext "  ></textarea>
                                      </div>
                                      @endif
                                      @else
                                        <div class="md-form" style="padding-left:18px !important;width: 97% !important;">
                                        <textarea id="rich-text"  name="textarea.{{$job->form_Section->id.'.'.$question->id}}" rows="5" class="{{$job->form_Section->id.'-'.$question->id}} answertype_richtext "  ></textarea>
                                      </div>
                                      @endif
                                  @break
                                @endswitch

                              </div>
                            </div>
                          </div>
                        @if($question->layout == 1)
                          </div>
                        @endif

                    </div>

                           @if($question->answer_type->name!='comment')
                          <div class="file-upload-wrapper" id="pic_div{{$question->id}}" style="display: none;">
                            <input type="file" id="input-file-now" name="attachments.{{$job->form_Section->id.'.'.$question->id}}" class="file-upload" data-default-file="{{asset('/uploads/response').'/'.($response_groups[$index]->responses[$key]->file_name ?? false)}}"  data-height="150" />
                          </div>
                        @endif
                    <input type="text" id="c_answer{{$job->form_Section->id.'.'.$question->id}}" class="custom_clear  form-control form_alignment" readonly>
                    @endif
                    @if($job->form_Section->form_section_column!== null)
                    </div>
                    @else
                    </div>
                    @endif
                  @endforeach()
                  </div>
                </div>

              </form>
                <div class="custom_clear card-footer text-muted text-left">
                    {{$job->form_Section->footer}}
                </div>
              </div>
            </div>
          </div>

         @endforeach
      </div>
     <br>
          <form id="ov_form" enctype="multipart/form-data">
                <input type="hidden" name="form_id"  value="{{$job_group->form_id}}" id="form_id">
                  <div class="file-upload-wrapper">
                    <input type="file" id="local_upload" name='ov_attachments' class="file-upload" />
                  </div>
                  <br>
                    <div class="md-form">

                    <textarea id="note" name="ov_note" class="md-textarea form-control" rows="3"></textarea>
                    <label for="note">Note</label>
                  </div>
                  <button class="btn btn-primary" type="button" onclick="saveOv()">Upload Notes</button>
                </form>
    </div>
    <div class="custom_clear tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
      <h5 class="custom_clear card-header  t py-4 white-color">
          <strong>Static Forms View</strong>
      </h5>
      <div id="pdf-viewer-static"></div>

  </div>

  <div class="tab-pane fade" id="ins" role="tabpanel" aria-labelledby="ins-tab">
      <h5 class="custom_clear card-header  t py-4 white-color">
          <strong>Instructions</strong>
      </h5>

       <div class="md-form">

    <textarea id="form10" class="md-textarea form-control" rows="3">{{$job_group->notes}}</textarea>

</div>
      <br>
      @if($job_group->file_name!=null)
      <div id="pdf-viewer-ins" style="height: 700px"></div>
      @endif
  </div>


<script src="{{asset('js/pdfview.js') }}"></script>
<script type="text/javascript">
 $('.answertype_richtext').summernote();
  var ids = @json($ids);
$('.file-upload').file_upload();
$('.datepicker').pickadate({selectYears:250,});
 $('.timepicker').pickatime();
    $(document).ready(function () {
        $("body").on('focusout','.note-editable',function () {
            $("#note_value").val($(this).html());
        });
    });



var options = {
  pdfOpenParams: {toolbar: '0'}
};
PDFObject.embed("{{ url('static_form/view-pdf', ['id' => $id_static]) }}", "#pdf-viewer-static",options);
var options = {
  pdfOpenParams: {toolbar: '0'}
};
PDFObject.embed("{{ url('job/view-pdf', ['id' => $job_group->id]) }}", "#pdf-viewer-ins",options);
</script>
<script src="{{asset('/custom/js/job.js')}}"></script>

@endsection
