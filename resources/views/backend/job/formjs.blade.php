@extends('backend.layouts.backend')
@section('title', 'view Jobs')
@section('content')
    @php
        $data=localization();
    @endphp
    <link rel="stylesheet" href="{{ asset('custom/css/custom_style.css') }}">

        <div class="custom_clear locations-form container-fluid form_body">
            <div class="custom_clear card">



                <ul  class="custom_clear nav nav-tabs" id="myTab" role="tablist">
                      <li class="custom_clear nav-item">
                          <a class="custom_clear nav-link" href="{{url('/job')}}" role="tab">
                            <i class="fas fa-arrow-alt-circle-left"></i>
                        </a>
                    </li>
                     <li class="custom_clear nav-item">
                        <a class="custom_clear nav-link active" id="job-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">
                            <span class="assign_class label{{getKeyid('jobs',$data)}}" data-id="{{getKeyid('jobs',$data) }}" data-value="{{checkKey('jobs',$data) }}" >
                                {!! checkKey('jobs',$data) !!}
                            </span>
                        </a>
                    </li>
                    @if($id_static)
                        <li class="custom_clear nav-item">
                            <a class="custom_clear nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">
                                <span class="assign_class label{{getKeyid('static_form',$data)}}" data-id="{{getKeyid('static_form',$data) }}" data-value="{{checkKey('static_form',$data) }}" >
                                    {!! checkKey('static_form',$data) !!}
                                </span>
                            </a>
                        </li>
                    @endif
                     <li class="custom_clear nav-item">
                            <a class="custom_clear nav-link" id="ins-tab" data-toggle="tab" href="#ins" role="tab" aria-controls="ins"
                          aria-selected="false">Instructions</a>
                    </li>

                    <li class="custom_clear nav-item ">
                        <div id="link_view">

                                 <a class="custom_clear nav-link" href="{{url('/completed_forms/viewDocument')}}/{{$job_group->id}}" role="tab">
                                <i class="fas fa-eye"></i>
                            </a>
                        </div>
                    </li>

                </ul>
                <div class="custom_clear tab-content" id="myTabContent">
              @if($job_group->completed!=2)
                <div id="complete_btn">
                    <div class="active_plus_btn">
                        <ul class="float-right  mr-4">
                            <li class="hover">
                            <a class="btn btn-cyan px-3" href="{{url('/job')}}" role="tab">
                            <i class="fas fa-arrow-left" aria-hidden="true"></i>
                            </a>
                            </li>
                            <li class="hover">
                                <a class="btn btn-cyan px-3" onclick="save(false)"><i class="fas fa-save" aria-hidden="true"></i></a>
                            </li>
                            <li class="hover">
                                <a class="btn btn-cyan px-3" data-toggle="modal" data-target="#model_create_form_section" onclick="save(true)" ><i class="fas fa-check" aria-hidden="true"></i></a>
                            </li>

                        </ul>
                    </div>
                </div>
                @endif
                    <div class="custom_clear tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="job-tab">
                        <h6 class="custom_clear card-header  t py-4 white-color">
                             <div class="row">
            <div class="col">
                <strong>
                  Form Name
                </strong>
            </div>
            <div class="col">
                <strong>
                  Person Name
                </strong>
            </div>
             <div class="col">
                <strong>
                  Location  Name
                </strong>
            </div>
            <div class="col">
                <strong>
                 Location BreakDown
                </strong>
            </div>
              <div class="col">
                <strong>
                 Date and Time
                </strong>
            </div>
          </div>
         <div class="row">
            <div class="col text-dark">
                <strong>
                 {{$job_group->forms->name?? "" }}
                </strong>
            </div>
            <div class="col text-dark">
                <strong>
                 {{$job_group->users->first_name ?? ""}}
                </strong>
            </div>
             <div class="col text-dark">
                <strong>
                 {{$job_group->locations->name ?? ""}}
                </strong>
            </div>
            <div class="col text-dark">
                <strong>
                 {{$job_group->locationBreakdowns->name ?? ""}}
                </strong>
            </div>
              <div class="col text-dark">
                @if(isset($jobs[0]))
                <strong>
                {{Carbon\Carbon::parse($jobs[0]->date_time)->format('jS M Y H:i')}}
                </strong>
                @endif
            </div>
          </div>
                        </h6>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card-deck" style="margin-top: 20px" id="div_for_dynamic_forms_display">
                                </div>

                            </div>
                                   @foreach($jobs as $index=> $job)
                                @foreach($job->form_Section->questionsS as $key=> $question)
                                    @if($question->answer_type->name == 'radio')
                                    @include('backend.job.model.comment')
                                    @endif
                                @endforeach
                              @endforeach
                        </div>
                         <br>
                <form id="ov_form" enctype="multipart/form-data">
                <input type="hidden" name="form_id"  value="{{$job_group->form_id}}" id="form_id">
                  <div class="file-upload-wrapper">
                    <input type="file" id="local_upload" name='ov_attachments' class="file-upload" />
                  </div>
                  <br>
                    <div class="md-form">

                    <textarea id="note" name="ov_note" class="md-textarea form-control" rows="3"></textarea>
                    <label for="note">Note</label>
                  </div>
                  <button class="btn btn-primary" type="button" onclick="saveOv()">Upload Notes</button>
                </form>
                    </div>
                    @if($id_static)
                        <div class="custom_clear tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                            <h5 class="custom_clear card-header  t py-4 white-color">
                                <strong>
                                    <span class="assign_class label{{getKeyid('static_form',$data)}}" data-id="{{getKeyid('static_form',$data) }}" data-value="{{checkKey('static_form',$data) }}" >
                                        {!! checkKey('static_form',$data) !!}
                                    </span>
                                </strong>
                                <span class="custom_clear  float-right mb-3 mr-2">
                                    <a href="{{url('static_form/download/pdf/'.$id_static)}}" class="custom_clear btn btn-success btn-sm ">
                                        <span class="assign_class label{{getKeyid('download_as_Pdf',$data)}}" data-id="{{getKeyid('download_as_Pdf',$data) }}" data-value="{{checkKey('download_as_Pdf',$data) }}" >
                                            {!! checkKey('download_as_Pdf',$data) !!}
                                        </span>
                                    </a>
                                    <a  href="{{url('static_form/download/orginal/'.$id_static)}}" class="custom_clear btn btn-secondary btn-sm ">
                                        <span class="assign_class label{{getKeyid('download_as_orginal',$data)}}" data-id="{{getKeyid('download_as_orginal',$data) }}" data-value="{{checkKey('download_as_orginal',$data) }}" >
                                            {!! checkKey('download_as_orginal',$data) !!}
                                        </span>
                                    </a>
                                </span>
                            </h5>
                            <div id="pdf-viewer-static"></div>
                        </div>
                    @endif
              <div class="tab-pane fade" id="ins" role="tabpanel" aria-labelledby="ins-tab">
                          <h5 class="custom_clear card-header  t py-4 white-color">
                              <strong>Instructions</strong>
                          </h5>
                    <div class="md-form">

                    <textarea id="form10" class="md-textarea form-control" rows="3">{{$job_group->notes}}</textarea>

                    </div>
                        <div class="row">
                                        <div class="col">
                                            <div class="md-form">
                                                <span class="assign_class label{{getKeyid('total_cost',$data)}}" data-id="{{getKeyid('total_cost',$data) }}" data-value="{{checkKey('total_cost',$data) }}" >
                                                      {!! checkKey('total_cost',$data) !!}
                                                  </span>
                                                : £{{$job_group->total_cost}}
                                            </div>
                                        </div>

                                        <div class="col">
                                            <div class="md-form">
                                               <span class="assign_class label{{getKeyid('bill_info',$data)}}" data-id="{{getKeyid('bill_info',$data) }}" data-value="{{checkKey('bill_info',$data) }}" >
                                                      {!! checkKey('bill_info',$data) !!}
                                                  </span>
                                                : {{$job_group->cost_info}}
                                            </div>
                                        </div>

                                    </div>  
                    <br>
                  @if($job_group->file_name!=null)
                  <div id="pdf-viewer-ins" style="height: 700px"></div>
                  @endif
          </div>
                </div>
            </div>
        </div>

    @include('backend.version.input_permission')
    @include('backend.label.input_label')
    <script src="{{asset('js/pdfview.js') }}"></script>
    <script type="text/javascript">

        $(document).ready(function(){
             var options = {
            pdfOpenParams: {toolbar: '0'}
        };
        PDFObject.embed("{{ url('static_form/view-pdf', ['id' => $id_static]) }}", "#pdf-viewer-static",options);
        PDFObject.embed("{{ url('job/view-pdf', ['id' => $job_group->id]) }}", "#pdf-viewer-ins",options);
            // (function ($) {
            //     var active ='<span class="assign_class label'+$('#breadcrumb_versionlogview').attr('data-id')+'" data-id="'+$('#breadcrumb_versionlogview').attr('data-id')+'" data-value="'+$('#breadcrumb_versionlogview').val()+'" >'+$('#breadcrumb_versionlogview').val()+'</span>';
            //     $('.breadcrumb-item.active').html(active);
            //     var parent ='<span class="assign_class label'+$('#dashboard_label').attr('data-id')+'" data-id="'+$('#dashboard_label').attr('data-id')+'" data-value="'+$('#dashboard_label').val()+'" >'+$('#dashboard_label').val()+'</span>';
            //     $('.breadcrumb-item a').html(parent);
            // }(jQuery));
        });
        $('.datepicker').pickadate({selectYears:250,});
    </script>
    <script src="{{asset('/custom/js/version_log.js')}}"></script>
    <script src="{{asset('/custom/js/answer_group.js')}}"></script>

    <script type="text/javascript">
        // $(".form_datetime").datetimepicker({format: 'yyyy-mm-dd hh:ii'});
        var answergroupBool =false;
        var answerCount = 0;
        var answerGroupCount=0;
        function populate()
        {
            var formData = $("#dynamicForm").serializeObject();
            formData.pre_populate_id = $('#pre_populate_id').children("option:selected").val();
            $.ajax({
                type: 'POST',
                url: '/form/populate',
                data: {
                    "_token": "{{ csrf_token() }}",formData
                },
                success: function(response)
                {
                    toastr["success"](response.message);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
        var token = '@csrf';
        var upload ="{{url('answer/uploadIcon')}}";

        var url='/answer_group/getall';
        answer_group_data(url);
        function answer_group_data(url)
        {
            var table = $('.answer_group_table').dataTable(
                {
                    processing: true,
                    language: {
                        'lengthMenu': '  _MENU_ ',
                        'search':'<span class="assign_class label'+$('#answer_group_search_id').attr('data-id')+'" data-id="'+$('#answer_group_search_id').attr('data-id')+'" data-value="'+$('#answer_group_search_id').val()+'" >'+$('#answer_group_search_id').val()+'</span>',
                        'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                        'info':'',
                        'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                        'paginate': {
                            'previous': '<span class="assign_class label'+$('#answer_group_previous_label').attr('data-id')+'" data-id="'+$('#answer_group_previous_label').attr('data-id')+'" data-value="'+$('#answer_group_previous_label').val()+'" >'+$('#answer_group_previous_label').val()+'</span>',
                            'next': '<span class="assign_class label'+$('#answer_group_next_label').attr('data-id')+'" data-id="'+$('#answer_group_next_label').attr('data-id')+'" data-value="'+$('#answer_group_next_label').val()+'" >'+$('#answer_group_next_label').val()+'</span>',
                        },
                        'infoFiltered': "(filtered from _MAX_ total records)"
                    },
                    "ajax": {
                        "url": url,
                        "type": 'get',
                    },
                    "createdRow": function( row, data, dataIndex,columns )
                    {

                        var checkbox='';
                        checkbox+='<div class="form-check"><input type="checkbox" class="form-check-input" id="answer_group'+data.id+'"><label class="form-check-label" for="answer_group'+data.id+'""></label></div>';
                        $(columns[0]).html(checkbox);
                        if($('#edit_permision_document_answer_group').val() && $('#document_answer_group_edit').val())
                        {
                            $(columns[1]).attr('Contenteditable', 'true');
                            $(columns[1]).attr('class','edit_inline_answer_group');
                        }

                        $(columns[1]).attr('id', 'answer_group_name'+data['id']);

                        $(row).attr('id', 'answer_group_tr'+data['id']);
                        $(row).attr('class', 'selectedrowanswer_group');
                    },
                    columns:
                        [
                            {data: 'checkbox', name: 'checkbox',visible:$('#document_answer_group_checkbox').val()},
                            {data: 'name', name: 'name',visible:$('#document_answer_group_name').val()},
                            {data: 'actions', name: 'actions'},
                        ],
                }
            );
        }
        function get_group_answer_id($id){
            sessionStorage.setItem('answer_group_id', $id);
            $('#modalAnswertablelist').modal('show');
            var url='/answer/getall/'+$id;
            $('.answer_table').DataTable().clear().destroy();
            answer_data(url);
        }
    </script>
    <script type="text/javascript">
          var ids = @json($ids);

        function getAllForms(){
            $.ajax({
                type: 'GET',
                url: '/job/get/version',
                data:{
                    'varsion_id':'{{$job_group->id}}',
                },
                success: function(response)
                {
                    var form_section = response.form_section;
                    var jobs = response.jobs;
                    console.log(response,ids);
                    var html = '';

                    for(var i = 0; i < form_section.length; i++){
                        html += ' <div id="'+form_section[i].order+'-'+form_section[i].id+'" class=" sortable-card col-md-'+form_section[i].width+'">';
                        html += '<div class="mb-4  custom_dynamic_height" id="form_section_height'+form_section[i].id+'">';
                        html += '<div class="card text-center">';
                        if (form_section[i].header_height!==null){
                            var header_height='height: '+form_section[i].header_height+'px;';
                        }else{
                            var header_height='';
                        }

                        if (form_section[i].show_header ==1){
                            html += '<div class="card-header" id="section_card_header'+form_section[i].id+'" style="display:flex;'+header_height+'">';
                            if ($("#versionLogDetail_form_header").val()=='1'){
                                if (form_section[i].header !==null){
                                    html += '<span class="white-color" style="flex: 1; text-align:left; ">'+form_section[i].header+'</span>';
                                }else{
                                    html += '<span class="white-color" style="flex: 1; text-align:left; ">&nbsp;&nbsp;&nbsp;&nbsp;</span>';
                                }
                            }else{
                                html +='<span class="white-color" style="flex: 1; text-align:left; ">&nbsp;&nbsp;&nbsp;</span>';
                            }
                            html += '<span class=" float-right mb-3 mr-2" style=" margin-top: -1%;" >';
                            if(form_section[i].is_repeatable==1)
                            {
                            html += '<form method="post" action="{{url("job/replicate")}}">';
                            html += '@csrf';
                            html += ' <input type="hidden" name="job_id" value="'+ids[i]+'">';
                            html += ' <button type="submit" class="btn btn-warning btn-sm" style=" display:flex; flex: 1;"><i class="fa fa-copy"></i></button>';
                            html += '</form>';
                            }
                            html += '</span>';
                            html += '</div>';
                        }
                        html += ' <form id="jobForm_'+ids[i]+'" enctype="multipart/form-data">';
                        html += '<input type="hidden" name="job_id" value="'+ids[i]+'">';
                        html += '<input type="hidden" name="populate_response_id" value="'+jobs[i].pre_populate_id+'">';
                        html += '<input type="hidden" name="job_group_id" value="{{$job_group->id}}">';
                        html += '<input type="hidden" name="complete" id="complete_'+ids[i]+'" value="false">';
                        html += '<input type="hidden" name="form_id"  value="{{$job_group->form_id}}" id="form_id">';
                        html += '<input type="hidden" name="location_id" value="{{$job_group->location_id}}">';
                        html += '<input type="hidden" name="user_id" value="{{$job_group->user_id}}">';
                        html += '  <input type="hidden" name="response_group_id" value="" id="rp'+form_section[i].id+'">';
                        html += '<div class="overflow-auto card-body section_custom_height" id="section_custom_height'+form_section[i].id+'"> ';
                        html += '<input type="hidden" name="id" class="form_section" value="'+form_section[i].id+'">';
                        html += '<input type="hidden" id="formsectionheight_'+form_section[i].id+'">';

                        if(form_section[i].questions_s !=null)
                            if (form_section[i].form_section_column!==null){
                                html += '<div class="row">';
                            }
                            html+=questionSet(form_section[i].questions_s,form_section[i].shared,form_section[i].width,form_section[i].id,form_section[i].form_section_column);
                        if (form_section[i].form_section_column!==null){
                            html += '</div>';
                        }
                        html += '</div>';
                        if (form_section[i].footer_height!==null){
                            var footer_height='height: '+form_section[i].footer_height+'px;';
                        }else{
                            var footer_height='';
                        }

                        if (form_section[i].show_footer ==1){
                            html += '<div class="card-footer text-muted text-left" id="section_card_footer'+form_section[i].id+'" style="'+footer_height+'">';
                            if ($("#versionLogDetail_form_footer").val()=='1'){
                                if (form_section[i].footer !==null){
                                    html += form_section[i].footer;
                                }else{
                                    html +='<span>&nbsp;&nbsp;&nbsp;</span>';
                                }
                            }else{
                                html +='<span>&nbsp;&nbsp;&nbsp;</span>';
                            }
                            html += '</div>';
                        }
                        html += '</form>';
                        html += '</div>';
                        html += '</div>';
                        html += '</div>';
                    }

                    $('#div_for_dynamic_forms_display').html(html);
                    $('#div_for_dynamic_forms_display_pdf').html(html);
                    $('.mdb-select_forms').materialSelect();
                    $('.input_starttime').pickatime();
                    $('.datepicker').pickadate({selectYears:250,});
                    $('select .mdb-select_forms, .initialized').hide();
                    $('.answertype_richtext').summernote();
                    setFormSectionHeight();
                    $('.file-upload').file_upload();
                    pop(response.response_groups);


                },
                error: function (error) {
                    console.log(error);
                }
            });
        }

        function setFormSectionHeight(){
            $.ajax({
                  url: '/job/get/version',
                    data:{
                    'varsion_id':'{{$job_group->id}}',
                },
                success: function(response)
                {
                    var form_section = response.form_section;
                    var setheight_option='';
                    for(var i = 0; i < form_section.length; i++){
                        var height =parseFloat(form_section[i].form_sections_height)+parseFloat(40);
                        $("#section_custom_height"+form_section[i].id).attr('style','height:'+height+'px;overflow:scroll;');
                    }
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }

        function questionSet(data,flag,form_section_width=null,form_section_id,form_section_column) {
            var html='';
            if(data!=null)
            {


                    for(var j = 0 ; j < data.length; j++){
                        let question = data[j];
                        section_question =form_section_id+'.'+question.id;
                        if (form_section_column !==null){
                            var columnsize=parseFloat(12)/parseFloat(form_section_column);
                            html += '<div class="form_section_columnsize col-md-'+columnsize+'">';
                        }

                        html += '<div class="card card-cascade wider reverse">';
                        if(flag == 0)
                        {
                            html += '<div class="btn-primary text-right" style="height: 40px;">';
                            html += '</div>';
                        }
                        if(form_section_width<=6){
                            var p_style = "margin-left:10px !important;";
                        }else{
                            var p_style ="";
                        }
                        html += '<p class="card-title text-left" style="'+p_style+'">';
                        if($("#versionLogDetail_question").val()=='1'){
                            html += '<strong>'+question.question+'</strong>';
                            html += '<button type="button" class="btn btn-xs  float-right" onclick="tp('+question.id+')"><i class="far fa-image"></i></button>';
                        }
                        html += '<input type="hidden" value="'+form_section_id+'.'+question.id+'" name="section_question_id"/>';
                        html += '</p>';
                        if(question.layout == 1)
                        {
                            html += '<div class="card-body hori_layout'+question.layout+'">';
                        }
                        html += '<div class="row text-center">';
                        if($("#versionLogDetail_answertype").val()=='1'){
                            var answer_type = question.answer_type.name;
                            // var answer_type = question.answer_type.name;
                        }else{
                            var answer_type ='';
                        }
                        switch (answer_type) {
                            case 'dropdown':
                                if ($("#versionLogDetail_answer").val()=='1'){
                                    html += '<div class="col-md-12" style="padding: 35px;text-align: center">';
                                    html += '<select searchable="Search here.." style="width: 100% !important;"  class="mdb-select_forms colorful-select dropdown-primary" name="dropdown.'+section_question+'" id="dropdown_'+form_section_id+'_'+question.id+'"   >';
                                    if (question.answer_group)
                                    {
                                        html +=loopAnswer(question.answer_group.answer,1,null,null,null,section_question);
                                    }
                                    html +='</select>';
                                    html += '</div>';
                                }
                                break;
                            case 'radio':
                                if ($("#versionLogDetail_answer").val()=='1'){
                                    //var icon_size="width:"+question.icon_size+"%;height:auto;";
                                    var icon_size="max-width:90%;height:auto;";
                                    if (question.column_size !==null){
                                        var num=parseFloat(12)/parseFloat(question.column_size);
                                        var column_size =parseInt(num);
                                    }else{
                                        var column_size="";
                                    }
                                    if(question.layout == 2)
                                    {
                                        var layout_type ='col-md-6';
                                        var style ='img-class img_style_verti';
                                        var img_style= '';
                                        if(form_section_width <=3){
                                            var column = 'col-md-12';
                                        }else if(form_section_width <=5){
                                            var column ='col-md-9';
                                        }else if(form_section_width ==6){
                                            var column ='col-md-9';
                                        }else{
                                            var column ='col-md-6';
                                        }
                                        html += '<div class="'+column+' verti">';
                                    }
                                    else
                                    {
                                        var img_style= 'img_style';
                                        var layout_type ='col-md-12';
                                        var style ='';
                                        html += '<div class="col-md-12 hori" style="padding-right: 40px;"><div class="row">';
                                    }
                                    if (question.answer_group)
                                    {
                                        html +=loopAnswer(question.answer_group.answer,2,style,img_style,form_section_width,section_question,layout_type,question.layout,icon_size,column_size);
                                    }
                                    html += '</div>';
                                    if(question.layout == 1)
                                    {
                                        html += '</div>';
                                    }
                                }
                                break;
                            case 'checkbox':
                                if ($("#versionLogDetail_answer").val()=='1'){
                                    //var icon_size="width:"+question.icon_size+"%;height:auto;";
                                    var icon_size="max-width:90%;height:auto;";
                                    if (question.column_size !==null){
                                        var num=parseFloat(12)/parseFloat(question.column_size);
                                        var column_size =parseInt(num);
                                    }else{
                                        var column_size="";
                                    }
                                    if(question.layout == 2)
                                    {
                                        var layout_type ='col-md-6';
                                        var style ='img-class img_style';
                                        var img_style= 'img_style_verti_check';

                                        if(form_section_width <=3){
                                            var column = 'col-md-12';
                                        }else if(form_section_width <=5){
                                            var column = 'col-md-9';
                                        }else if(form_section_width ==6){
                                            var column = 'col-md-9';
                                        }else{
                                            var column = 'col-md-6';
                                        }

                                        html += '<div class="'+column+' verti">';

                                    } else {
                                        var img_style= 'img_style';
                                        var layout_type ='col-md-12';
                                        html += '<div class="col-md-12 hori" style="padding-right: 40px;"><div class="row">';
                                    }
                                    if (question.answer_group) {
                                        html +=loopAnswer(question.answer_group.answer,6,style,img_style,form_section_width,section_question,layout_type,question.layout,icon_size,column_size);
                                    }
                                    // id="answer_type_id"
                                    html += '</div>';
                                    if(question.layout == 1)
                                    {
                                        html += '</div>';
                                    }
                                }
                                break;
                            case 'multi':
                                if ($("#versionLogDetail_answer").val()=='1'){
                                    html += '<div class="col-md-12" style="padding: 35px;text-align: center"><div class="md-form">';
                                    html += '<select searchable="Search here.."  class="mdb-select_forms " multiple name="multi.'+section_question+'" >';
                                    if (question.answer_group)
                                    {
                                        html +=loopAnswer(question.answer_group.answer,1,section_question);
                                    }
                                    html +='</select>';
                                    html += '</div></div>';
                                }
                                break;
                            case 'date':
                                if ($("#versionLogDetail_answer").val()=='1'){
                                    html += '<div class="col-md-12">';

                                        html +=withoutLoopAnswer(null,3,section_question);

                                    html += '</div>';
                                }
                                break;
                            case 'time':
                                if ($("#versionLogDetail_answer").val()=='1'){
                                    html += '<div class="col-md-12">';

                                        html +=withoutLoopAnswer(null,4,section_question);


                                    html += '</div>';
                                }
                                break;
                            case 'datetime':
                                // html += '<div class="col-md-12">';
                                // if (question.answer_group)
                                //       {
                                //         html +=withoutLoopAnswer(question.answer_group.answer,3,section_question);
                                //
                                //       }
                                //   html += '</div>';
                                break;
                            case 'text':
                                if ($("#versionLogDetail_answer").val()=='1'){
                                    html += '<div class="col-md-12">';

                                        html +=withoutLoopAnswer(null,5,section_question);


                                    html += '</div>';
                                }
                                break;
                            case 'richtext':
                                if ($("#versionLogDetail_answer").val()=='1'){
                                    html += '<div class="col-md-12">';

                                        html +=withoutLoopAnswer(null,8,section_question);

                                    html += '</div>';
                                }
                                break;
                            case 'comment':
                                if ($("#versionLogDetail_answer").val()=='1'){
                                    html += '<div class="col-md-12">';


                                        html +=withoutLoopAnswer(null,10,section_question,question.comment);


                                    html += '</div>';
                                }
                                break;
                            case 'cloc':
                                if ($("#versionLogDetail_answer").val()=='1'){
                                    html += '<div class="col-md-12">';


                                        html +=withoutLoopAnswer(null,12,section_question);


                                    html += '</div>';
                                }
                                break;
                            default:
                                if ($("#versionLogDetail_answer").val()=='1'){
                                    html += '<div class="col-md-12">';


                                        html +=withoutLoopAnswer(null,0,section_question);


                                    html += '</div>';
                                }
                        }

                        html += '</div>';
                        html += '<div  id="pic_div'+question.id+'" style="display: none;">';
                         html += ' <div class="file-field">';
                         html += '<div class="z-depth-1-half mb-4">';
                        html += '</div>';
                        html += '<div class="d-flex justify-content-center">';
                        html += '<div class="btn btn-outline-primary waves-effect btn-rounded float-left btn-sm">';
                        html += '<div  id="file_div'+question.id+'">';
                        html += '<span>Choose file</span>';
                        html += '<input type="file" id="input-file'+section_question+'" name="attachments.'+section_question+'"/>';
                        html += ' </div>';
                        html += ' </div>';
                          html += '<a type="button" class="btn btn-danger btn-sm waves-effect waves-light" onclick="rim('+section_question+','+question.id+')"><i class="fas fa-trash"></i></a>';
                        html += ' </div>';
                        html += ' </div>';
                        html += ' </div>';
                        if(answer_type=='radio' && answer_type=='checkbox')
                        {
                        html += '<div>';
                        }
                        else
                        {

                        html += ' <div class="pb-2">';
                        }
                        html += ' <input type="text" id="c_answer'+section_question+'" class="custom_clear  form-control form_alignment" readonly>';
                        html += ' </div>';

                        if(question.layout == 1)
                        {
                            html += '</div>';
                        }

                        html += '</div>';
                        if (form_section_column !==null){
                            html += '</div>';
                        }

                    }
             }

            return html;
        }

        function loopAnswer_old(data,flag,custom_class=null,imgStyle=null,form_section_width=null,section_question,layout_type=null,layout=null) {
            var html='';
            for(var k=0;k<data.length;k++)
            {
                let answer = data[k];
                if(answer.answer)
                {
                    switch(flag) {
                        case 1:
                            html += '<option value="'+answer.id+'">';
                            html += answer.answer;
                            html += '</option>';
                            break;
                        case 2:
                            if (layout==2){
                                if(form_section_width <=2){
                                    var img_style = 'margin-left:6px !important;';
                                    var column = layout_type;
                                    var custom_sectionstart = "<div class='row'>";
                                    var custom_sectionend = "</div>";
                                }else{
                                    var img_style = 'margin-left:9px !important;';
                                    var column = layout_type;
                                    var custom_sectionstart = "";
                                    var custom_sectionend = "";
                                }
                                if(answer.icon!=null){
                                    var img='<img src="{{asset("/uploads/icons")}}/'+answer.icon+'" style="margin-left:0px !important;"  width="35" height="35"  class="rounded '+imgStyle+'">';
                                }else{
                                    var img='';
                                }

                                html += custom_sectionstart;
                                html += '<div class="form-check float-left form-check-form_section" style="padding-left:1rem !important;">';
                                html += '<div class="row">';
                                html += '<div class="'+column+'">';

                                html += '<input type="radio" class="form-check-input '+answer.grade+'" id="'+section_question+'.'+answer.id+'" value="'+answer.id+'" name="answer_id.'+section_question+'"radio.'+section_question+'>';
                                html += '<label class="form-check-label abswer_label_radio" for="'+section_question+'.'+answer.id+'">'+img+'</label>';

                                html += '</div>';
                                html += '<div class="'+column+' col-img-padding text-center">';

                                if (form_section_width >4){
                                    html+='<span style="margin-left:0px !important;">'+answer.answer+'</span>';
                                }else{
                                    html+='<span style="margin-left:25px !important;">'+answer.answer+'</span>';
                                }

                                html += '</div>';
                                html += '</div>';
                                html += '</div>';
                                html += custom_sectionend;
                            }else{

                                if(form_section_width <=2){
                                    var img_style = 'margin-left:6px !important;';
                                    var column = layout_type;
                                    var custom_sectionstart = "<div class='row'>";
                                    var custom_sectionend = "</div>";
                                }else{
                                    var img_style = 'margin-left:9px !important;';
                                    var column = layout_type;
                                    var custom_sectionstart = "";
                                    var custom_sectionend = "";
                                }
                                html += custom_sectionstart;
                                html += '<div class="form-check float-left form-check-form_section" style="padding-left:1rem !important;">';
                                html += '<div class="row">';
                                html += '<div class="'+column+'">';

                                html += '<input type="radio" class="form-check-input '+answer.grade+'" id="'+section_question+'.'+answer.id+'" value="'+answer.id+'" name="radio.'+section_question+'" >';
                                html += '<label class="form-check-label abswer_label_radio" for="'+section_question+'.'+answer.id+'">'+answer.answer+'</label>';

                                html += '</div>';
                                html += '<div class="'+column+' col-img-padding text-center">';
                                if(answer.icon!=null){
                                    if(k==0){
                                        var mystyle = img_style;
                                    }else{
                                        var mystyle ='';
                                    }
                                    html += '<img class="'+custom_class+' '+imgStyle+'" style="vertical-align:baseline;" src="{{asset("/uploads/icons")}}/'+answer.icon+'"  width="35" height="35"  class="ml-1 mb-1 rounded">';
                                }else{
                                    html += '<img class="'+custom_class+' '+imgStyle+'" img_style" src="{{asset("/uploads/icons/default/BlankEmailPixels-700px.png")}}" style="vertical-align:baseline;"  width="35" height="35"  class="ml-1 mb-1 rounded">';
                                }

                                html += '</div>';
                                html += '</div>';
                                html += '</div>';
                                html += custom_sectionend;
                            }
                            break;
                        case 3:
                            html += ' <div class="md-form">';
                            html+=' <input type="text" id="date-picker-form'+answer.id+'" name="start_date.'+section_question+'" class="form-control datepicker">';
                            html += ' <label for="date-picker-form'+answer.id+'">Time</label>';
                            html += '</div>';
                            break;
                        case 4:
                            html += ' <div class="md-form">';
                            html+=' <input placeholder="Selected time" type="text" id="time-picker-form'+answer.id+'"  name="time.'+section_question+'" class="input_starttime form-control timepicker">';
                            html += ' <label for="time-picker-form'+answer.id+'" >Time</label>';
                            html += '</div>';
                            break;
                        case 6:

                            if(form_section_width <=2){
                                var img_style = 'margin-left:6px !important;';
                                var column = 'col-md-4';
                                var custom_sectionstart = "<div class='row'>";
                                var custom_sectionend = "</div>";
                            }else{
                                var img_style = 'margin-left:9px !important;';
                                var column = 'col';
                                var custom_sectionstart = "";
                                var custom_sectionend = "";
                            }
                            if (layout==2){
                                if(answer.icon!=null){
                                    var img='<img src="{{asset("/uploads/icons")}}/'+answer.icon+'" style="margin-left:0px !important;"  width="35" height="35"  class="rounded '+imgStyle+'">';
                                }else{
                                    var img='';
                                }

                                html += custom_sectionstart;
                                html += '<div class="form-check checkbox_style float-left"><div class="row"> <div class="'+layout_type+'" >';

                                html += '<input type="checkbox" class="form-check-input" id="'+section_question+'.'+answer.id+'" value="'+answer.id+'" name="checkbox.'+section_question+'" value="'+answer.id+'">';
                                html += '<label class="form-check-label abswer_label_checkbox" style="padding-left: 25px !important;" for="'+section_question+'.'+answer.id+'">'+img+'</label></div><div class="'+layout_type+' text-center " style="padding-right: 0px !important;padding-left: 0px !important;">';
                                if (form_section_width >4){
                                    html+='<span style="margin-left:0px !important;">'+answer.answer+'</span>';
                                }else{
                                    html+='<span style="margin-left:25px !important;">'+answer.answer+'</span>';
                                }
                                html += '</div></div></div>';
                                html += custom_sectionend;
                            }else{
                                html += custom_sectionstart;
                                html += '<div class="form-check checkbox_style float-left"><div class="row"> <div class="'+layout_type+'" >';
                                html += '<input type="checkbox" class="form-check-input" id="'+section_question+'.'+answer.id+'" value="'+answer.id+'" name="checkbox.'+section_question+'" value="'+answer.id+'">';
                                html += '<label class="form-check-label abswer_label_checkbox" style="padding-left: 25px !important;" for="'+section_question+'.'+answer.id+'">'+answer.answer+'</label></div><div class="'+layout_type+' text-center " style="padding-right: 0px !important;padding-left: 0px !important;">';
                                if(answer.icon!=null){
                                    if(k==0){
                                        var mystyle = 'margin-left:12px !important;';
                                    }else{
                                        var mystyle ='';
                                    }
                                    html += '<img src="{{asset("/uploads/icons")}}/'+answer.icon+'" style=""  width="35" height="35"  class="rounded '+imgStyle+'">';
                                }else{
                                    html += '<img src="{{asset("/uploads/icons/default/BlankEmailPixels-700px.png")}}" style=""  width="35" height="35"  class="'+imgStyle+' rounded">';
                                }

                                html += '</div></div></div>';
                                html += custom_sectionend;
                            }
                            break;
                    }
                }
                this.answerCount++;
            }
            this.answerGroupCount++;
            return html;
        }

        function loopAnswer(data,flag,custom_class=null,imgStyle=null,form_section_width=null,section_question,layout_type=null,layout=null,icon_size=null,column_size=null)
        {
            var html='';
            for(var k=0;k<data.length;k++)
            {
                let answer = data[k];
                if(answer.answer)
                {
                    switch(flag) {
                        case 1:
                            html += '<option id="'+section_question+'.'+answer.id+'" value="'+answer.id+'">';
                            html += answer.answer;
                            html += '</option>';
                            break;
                        case 2:
                            if (layout==2){
                                if(form_section_width <=2){
                                    var img_style = 'margin-left:6px !important;';
                                    // var column = layout_type;
                                    var img_column = layout_type;
                                    var label_column =layout_type;
                                    var custom_sectionstart = "<div class='row'>";
                                    var custom_sectionend = "</div>";
                                }else{
                                    var img_style = 'margin-left:9px !important;';
                                    if (form_section_width <=8){
                                        var img_column = 'col-md-6';
                                        var label_column = 'col-md-6';
                                    }else{
                                        var img_column = 'col-md-4 ';
                                        var label_column = 'col-md-8';
                                    }
                                    // var column = layout_type;
                                    var custom_sectionstart = "";
                                    var custom_sectionend = "";
                                }
                                if(answer.icon!=null){
                                    var lineheight='line-height:60px;';
                                    if (form_section_width <=8){
                                        var img_column = 'col-md-6';
                                        var label_column = 'col-md-6';
                                    }else{
                                        var img_column = 'col-md-4 ';
                                        var label_column = 'col-md-8';
                                    }
                                    var verticla_abswer_label_radio='verticla_abswer_label_radio';
                                    var img='<img src="{{asset("/uploads/icons")}}/'+answer.icon+'" style="margin-left:10px !important;'+icon_size+'"   class=" '+imgStyle+'">';
                                }else{
                                    var lineheight ='';
                                    if (form_section_width <=6){
                                        var img_column = 'col-md-2';
                                        var label_column = 'col-md-10';
                                    }else{
                                        var img_column = 'col-md-1 ';
                                        var label_column = 'col-md-11';
                                    }
                                    var verticla_abswer_label_radio='';
                                    var img='';
                                }

                                html += custom_sectionstart;
                                html += '<div class="form-check form-check-form_section" style="padding-left:1rem !important;">';
                                html += '<div class="row" style="height: 100%;">';
                                html += '<div class="'+img_column+' text-left" style="'+lineheight+'">';

                                html += '<input type="radio" class="form-check-input '+answer.grade+'" id="'+section_question+'.'+answer.id+'" value="'+answer.id+'" name="radio.'+section_question+'">';
                                html += '<label style="text-align: center;height: 100%;" class="form-check-label abswer_label_radio '+verticla_abswer_label_radio+'" for="'+section_question+'.'+answer.id+'">'+img+'</label>';

                                html += '</div>';
                                html += '<div class="'+label_column+' text-left">';

                                if (form_section_width >4){
                                    html+='<span style="margin-left:10px !important;text-align: left !important;display: block;line-height:60px;">'+answer.answer+'</span>';
                                }else{
                                    html+='<span style="margin-left:25px !important;text-align: left !important;display: block;line-height:60px;">'+answer.answer+'</span>';
                                }

                                html += '</div>';
                                html += '</div>';
                                html += '</div>';
                                html += custom_sectionend;
                            }else{
                                if(form_section_width <=2){
                                    var img_style = 'margin-left:6px !important;';
                                    var column = layout_type;
                                    var custom_sectionstart = "<div class='row'>";
                                    var custom_sectionend = "</div>";
                                }else{
                                    var img_style = 'margin-left:9px !important;';
                                    var column = layout_type;
                                    var custom_sectionstart = "";
                                    var custom_sectionend = "";
                                }
                                html += custom_sectionstart;
                                html += '<div class="col-md-'+column_size+'"><div class="form-check form-check-form_section" style="padding-left:1rem !important;">';
                                html += '<div class="row" style="margin-bottom:40px !important;">';
                                html += '<div class="'+column+' text-left">';

                                html += '<input type="radio" class="form-check-input '+answer.grade+'" id="'+section_question+'.'+answer.id+'" value="'+answer.id+'" name="radio.'+section_question+'">';
                                html += '<label class="form-check-label abswer_label_radio" for="'+section_question+'.'+answer.id+'">'+answer.answer+'</label>';

                                html += '</div>';
                                html += '<div class="'+column+' text-left">';
                                if(answer.icon!=null){
                                    if(k==0){
                                        var mystyle = img_style;
                                    }else{
                                        var mystyle ='';
                                    }
                                    html += '<img class="'+custom_class+' '+imgStyle+'" style="vertical-align:baseline;'+icon_size+'" src="{{asset("/uploads/icons")}}/'+answer.icon+'"  width="35" height="35"  class="ml-1 mb-1 rounded">';
                                }else{
                                    html += '<img class="'+custom_class+' '+imgStyle+'" img_style" src="{{asset("/uploads/icons/default/BlankEmailPixels-700px.png")}}" style="vertical-align:baseline;width:10%;height:auto;"  width="35" height="35"  class="ml-1 mb-1 rounded">';
                                }
                                html += '</div>';
                                html += '</div>';
                                html += '</div>';
                                html += '</div>';
                                html += custom_sectionend;
                            }
                            break;
                        case 3:
                            html += ' <div class="md-form">';
                            html+=' <input type="text" id="date-picker-form'+answer.id+'" name="start_date.'+section_question+'" class="form-control datepicker">';
                            html += ' <label for="date-picker-form'+answer.id+'">Time</label>';
                            html += '</div>';
                            break;
                        case 4:
                            html += ' <div class="md-form">';
                            html+=' <input placeholder="Selected time" type="text" id="time-picker-form'+answer.id+'"  name="time.'+section_question+'" class="input_starttime form-control timepicker">';
                            html += ' <label for="time-picker-form'+answer.id+'" >Time</label>';
                            html += '</div>';
                            break;
                        case 6:
                            if(form_section_width <=2){
                                var img_style = 'margin-left:6px !important;';
                                var column = 'col-md-4';
                                var custom_sectionstart = "<div class='row'>";
                                var custom_sectionend = "</div>";
                            }else{
                                var img_style = 'margin-left:9px !important;';
                                var column = 'col';
                                var custom_sectionstart = "";
                                var custom_sectionend = "";
                            }
                            if (form_section_width <=8){
                                var img_column = layout_type;
                                var label_column = layout_type;
                            }else{
                                var img_column = 'col-md-4';
                                var label_column = 'col-md-8';
                            }
                            if (layout==2){
                                if(answer.icon!=null){
                                    if (form_section_width <=8){
                                        var img_column = 'col-md-6';
                                        var label_column = 'col-md-6';
                                    }else{
                                        var img_column = 'col-md-4 ';
                                        var label_column = 'col-md-8';
                                    }
                                    var abswer_label_checkbox_verticle='abswer_label_checkbox_verticle';
                                    var img='<img src="{{asset("/uploads/icons")}}/'+answer.icon+'" style="margin-left:10px !important;'+icon_size+'"  width="35" height="35"  class="rounded '+imgStyle+'">';
                                }else{
                                    if (form_section_width <=6){
                                        var img_column = 'col-md-2';
                                        var label_column = 'col-md-10';
                                    }else{
                                        var img_column = 'col-md-1 ';
                                        var label_column = 'col-md-11';
                                    }
                                    var abswer_label_checkbox_verticle='';
                                    var img='';
                                }
                                html += custom_sectionstart;
                                html += '<div class="form-check checkbox_style"><div class="row" style="height:100%;"> <div class="'+img_column+' text-left" >';
                                html += '<input type="checkbox" class="form-check-input" id="'+section_question+'.'+answer.id+'" value="'+answer.id+'" name="checkbox.'+section_question+'" value="'+answer.id+'">';
                                html += '<label class="form-check-label abswer_label_checkbox '+abswer_label_checkbox_verticle+'" style="padding-left: 25px !important;height: 100%;" for="'+section_question+'.'+answer.id+'">'+img+'</label></div><div class="'+label_column+' text-left" style="padding-right: 0px !important;">';
                                if (form_section_width >4){
                                    html+='<span style="margin-left:25px !important;text-align:left;display: block;line-height:60px;">'+answer.answer+'</span>';
                                }else{
                                    html+='<span style="margin-left:25px !important;text-align:left;display: block;line-height:60px;">'+answer.answer+'</span>';
                                }
                                html += '</div>';
                                html += '</div>';
                                html += '</div>';
                                html += custom_sectionend;
                            }else{
                                html += custom_sectionstart;
                                html += '<div class="col-md-'+column_size+'" style="margin-bottom:40px !important;"><div class="form-check checkbox_style "><div class="row"> <div class="'+layout_type+' text-left" >';
                                html += '<input type="checkbox" class="form-check-input" id="'+section_question+'.'+answer.id+'" value="'+answer.id+'" name="checkbox.'+section_question+'" value="'+answer.id+'">';
                                html += '<label class="form-check-label abswer_label_checkbox" style="padding-left: 25px !important;" for="'+section_question+'.'+answer.id+'">'+answer.answer+'</label></div><div class="'+layout_type+' text-left " style="padding-right: 0px !important;">';
                                if(answer.icon!=null){
                                    if(k==0){
                                        var mystyle = 'margin-left:12px !important;';
                                    }else{
                                        var mystyle ='';
                                    }
                                    html += '<img src="{{asset("/uploads/icons")}}/'+answer.icon+'" style="'+icon_size+'"   class="rounded '+imgStyle+'">';
                                }else{
                                    html += '<img src="{{asset("/uploads/icons/default/BlankEmailPixels-700px.png")}}" style="width:10%;height:auto;"   class="'+imgStyle+' rounded">';
                                }

                                html += '</div>';
                                html += '</div>';
                                html += '</div>';
                                html += '</div>';
                                html += custom_sectionend;
                            }
                            break;
                    }
                }
                this.answerCount++;
            }
            this.answerGroupCount++;
            return html;
        }

        function withoutLoopAnswer(data,flag,section_question,comment)
        {
            var html='';
            var str = section_question.split('.');
            var st = str[0]+'-'+str[1];
            //let answer = data[1];
            let answer = null;
            switch(flag) {
                case 3:
                    html += ' <div class="md-form" style="padding-left:18px !important;width: 97% !important;">';
                    //html+=' <input type="text" id="date-picker-form'+answer.id+'" name="date" class="form-control datepicker">';
                    html+=' <input type="datetime-local" id="date-picker-form" name="'+section_question+'" class="form-control datepicker">';
                    //html += ' <label for="date-picker-form'+answer.id+'" style="margin-top:-22px !important;margin-left:16px !important;">Date</label>';
                    html += ' <label for="date-picker-form" style="margin-top:-22px !important;margin-left:16px !important;">Date</label>';
                    html += '</div>';
                    break;
                case 4:
                    html += ' <div class="md-form" style="padding-left:18px !important;width: 97% !important;">';
                    // html+=' <input placeholder="Selected time" type="text" id="time-form'+answer.id+'"  name="time" class="input_starttime form-control timepicker">';
                    html+=' <input placeholder="Selected time" type="text" id="time-form"  name="'+section_question+'" class="input_starttime form-control timepicker">';
                    html += ' <label for="time-form" style="margin-top:-22px !important;margin-left:16px !important;" >Time</label>';
                    //html += ' <label for="time-form'+answer.id+'" style="margin-top:-22px !important;margin-left:16px !important;" >Time</label>';
                    html += '</div>';
                    break;
                case 5:
                    html += ' <div class="md-form" style="padding-left:18px !important;width: 97% !important;">';
                    html+=' <input  type="text" id="input-text"  name="'+section_question+'" class=" form-control ">';
                    // html+=' <input  type="text" id="input-text'+answer.id+'"  name="text" class=" form-control ">';
                    html += ' <label for="input-text" style="margin-top:-22px !important;margin-left:16px !important;">Text</label>';
                    //html += ' <label for="input-text'+answer.id+'" style="margin-top:-22px !important;margin-left:16px !important;">Text</label>';
                    html += '</div>';
                    break;
                case 8:
                    html +='<div class="md-form" style="padding-left:18px !important;width: 97% !important;">';
                    html+='<textarea id="rich-text"  name="'+section_question+'" rows="5" class="answertype_richtext '+st+'"></textarea>';
                    // html+='<div class="answertype_richtext">summernote 2</div>';
                    //html+='<label data-error="wrong" data-success="right" for="name" class="active">Rich Text</label>';
                    //html+='<div class="summernote" id="body-text"></div>';

                    html +='<label for="rich-text" style="margin-top:-35px !important;margin-left:16px !important;">&nbsp;</label>';
                    html +='</div>';
                    break;
                case 10:

                    html+='<div class="text-left" style="margin-left:10px;padding-bottom:20px;">'+comment+'</div>';

                    break;
                case 12:
                    html += ' <div class="md-form" style="padding-left:18px !important;width: 97% !important;">';
                    html+=' <input  type="text" id="input-text" class=" form-control ">';

                    html += ' <label for="input-text" style="margin-top:-22px !important;margin-left:16px !important;">Clocs</label>';

                    html += '</div>';
                    break;
                default :
                    html += ' <div class="md-form" style="padding-left:18px !important;width: 97% !important;">';
                    html+=' <img src="{{asset("/img/no_si.jpeg")}}" width="200" height="100">';

                    html += '</div>';
            }
            return html;
        }

        function getAllFormsQuestion(){
            $.ajax({
                type: 'GET',
                url: '/document/forms/get/question',
                // data: data,
                success: function(response)
                {

                    var form_section = response.form_section;
                    var html = '';
                    for(var i = 0; i < form_section.length; i++){

                        html += ' <div class="col-md-'+form_section[i].width+'">';
                        html += '<div class="mb-4 custom_dynamic_height">';
                        html += '<div class="card text-center ">';
                        html += '<div class="card-header" style="display:flex">';
                        html += '<span class="white-color" style="flex: 1; text-align:left; ">'+form_section[i].header+'</span>';
                        html += '<span class=" float-right">';

                        html += ' <a onClick="getform_sectiondata('+form_section[i].id+')"  class=" form_btn_circle">';
                        html += '<i class="white-color fas fa-pencil-alt fa-sm" style="margin-left: auto;"></i>';
                        html += '</a> ';

                        html += '<a  class="text-success open_popup form_btn_circle" id="'+form_section[i].id+'">';
                        html += '<i class="white-color fas fa-plus fa-md" aria-hidden="true"></i>';
                        html += '</a>';
                        html += '</span>';

                        html += '</div>';
                        html += '<div class="card-body"> ';
                        html += '<input type="hidden" name="id" class="form_section" value="'+form_section[i].id+'">';
                        html+=questionSet(form_section[i].questions_s,form_section[i].shared,form_section[i].width,form_section[i].id,form_section[i].form_section_column);

                        html += '</div>';
                        html += '<div class="card-footer text-muted text-left">';
                        html += form_section[i].footer;
                        html += '</div>';
                        html += '</div>';
                        html += '</div>';
                        html += '</div>';
                    }
                    $('#div_for_dynamic_forms_display_question').html(html);

                },
                error: function (error) {
                    console.log(error);
                }
            });
        }

        $('.datepicker').pickadate({selectYears:250,});

        var toolbarOptions = [
            [{
                'header': [1, 2, 3, 4, 5, 6, false]
            }],
            ['bold', 'italic', 'underline', 'strike'], // toggled buttons
            ['blockquote', 'code-block'],

            [{
                'header': 1
            }, {
                'header': 2
            }], // custom button values
            [{
                'list': 'ordered'
            }, {
                'list': 'bullet'
            }],
            [{
                'script': 'sub'
            }, {
                'script': 'super'
            }], // superscript/subscript
            [{
                'indent': '-1'
            }, {
                'indent': '+1'
            }], // outdent/indent
            [{
                'direction': 'rtl'
            }], // text direction

            [{
                'size': ['small', false, 'large', 'huge']
            }], // custom dropdown

            [{
                'color': []
            }, {
                'background': []
            }], // dropdown with defaults from theme
            [{
                'font': []
            }],
            [{
                'align': []
            }],
            ['link', 'image'],

            ['clean'] // remove formatting button
        ];

        $(document).ready(function(){
            getAllFormsQuestion();
            if ($("#versionLogDetail_form_view").val()=='1'){
                    getAllForms();
            }
            $.getScript("{{asset('/custom/js/job.js')}}");
           /* $("body").on('change','#pre_populate_id',function(){
                if (parseInt($(this).val())){
                    $(".pre_populate_id").val($(this).val());
                }
            });
            $("body").on('change','#pre_populate_id',function(){
                if (parseInt($(this).val())){
                    var url='{{url('/version/log/pdf/form/export/')}}'+'/'+$(this).val();
                    $("#pdf_export_link").attr('href',url);
                }
            });*/

            $('a[data-toggle="tab"]').on('click', function(e) {

                if($(e.target).attr('href')){
                    var x = document.getElementById("pop_div");
                    if($(e.target).attr('href')=="#profile")
                    {
                        x.style.display = "flex";
                        console.log("show",$(e.target).attr('href'));
                    }
                    else
                    {
                        x.style.display = "none";
                        console.log("hide",$(e.target).attr('href'));
                    }
                }
            });


            $('body').on('change', '#pre_populate_id', function() {
                /*$('input').addClass( "custom_clear" );
                  $('#pre_populate_id').removeClass("custom_clear");
                  $(".custom_clear").prop('checked', false);
                  $(".custom_clear").val(' ');*/
                getAllForms();

                var pre_populate_id = $('#pre_populate_id').children("option:selected").val()
                url = '/version/log/getPopulate/'+pre_populate_id;
                setTimeout(function() {
                    $.ajax({
                        type: "GET",
                        url:url,

                        success: function(response)
                        {
                            console.log(response);

                            for (var i = response.length - 1; i >= 0; i--) {


                                if(response[i].answer_id==null)

                                    var  id = response[i].form_section_id+'.'+response[i].question_id;
                                else
                                    var id = response[i].form_section_id+'.'+response[i].question_id+'.'+response[i].answer_id;

                                console.log(id);
                                try
                                {

                                    var type = document.getElementsByName(id)[0].type;
                                }
                                catch
                                {
                                    try
                                    {
                                        var type =  document.getElementById(id).type;
                                    }
                                    catch
                                    {
                                        continue;
                                    }
                                }

                                console.log(type);
                                if(type=='radio' || type=='checkbox')
                                {
                                    try
                                    {
                                        document.getElementsByName(id)[0].checked=true;
                                    }
                                    catch
                                    {
                                        document.getElementById(id).checked = true;

                                    }
                                }
                                else if( type=='select')
                                {
                                    document.getElementById(id).selected = "true";

                                }
                                else if( type=='text') {
                                    id = response[i].form_section_id+'.'+response[i].question_id;
                                    console.log(response[i].answer_text);
                                    document.getElementsByName(id)[0].value=response[i].answer_text;



                                }else if(type=='textarea')
                                {
                                    document.getElementsByName(id)[0].value=response[i].answer_text;
                                    var id = id.split('.');
                                    var nid = '.'+id[0]+'-'+id[1];
                                    console.log(nid);
                                    $(nid).summernote('code',response[i].answer_text);
                                }
                                else
                                {
                                    document.getElementById(id).selected = "true";
                                }


                            }


                        }

                    });
                },800);
            });


            $('body').on('focusout', '#formName', function() {

                var formData = $("#addPopulate").serializeObject();


                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url:'/form/addPopulate',
                    data:formData,
                    success: function(response)
                    {
                        console.log(Object.keys(response.pre_populate).length );

                        var html='';

                        html+='<select class="mdb-select" name="pre_populate_id" id="pre_populate_id" >';

                        for (var key in response.pre_populate) {
                            if (response.pre_populate.hasOwnProperty(key))
                                html+='<option  value="'+key+'">'+response.pre_populate[key]+'</option>';
                        }
                        html+='</select> ';
                        $('#pre_populate_listing').empty();
                        console.log(html, "html");
                        document.getElementById('pre_populate_listing').innerHTML=html;
                        $('#pre_populate_id').materialSelect();
                        toastr["success"](response.message);
                    }
                });
                // console.log(data);
            });
        });


    function pop(response)
    {
         for (var i = response.length - 1; i >= 0; i--) {


                                if(response[i].answer_id==null)

                                    var  id = response[i].form_section_id+'.'+response[i].question_id;
                                else
                                    var id = response[i].form_section_id+'.'+response[i].question_id+'.'+response[i].answer_id;

                                var rid = '#rp'+response[i].form_section_id
                                console.log(rid);
                                $(rid).val(response[i].response_group_id);
                                   if(response[i].file_name!=null)
                                  {

                                $("#pic_div"+response[i].question_id).prepend('<img src="{{asset("/uploads/response")}}/'+response[i].file_name+'" class="img_pop"   width="80%" height="60%">');
                                  }

                                try
                                {

                                    var type = document.getElementsByName(id)[0].type;
                                }
                                catch
                                {
                                    try
                                    {
                                        var type =  document.getElementById(id).type;
                                    }
                                    catch
                                    {
                                         id ='dropdown.'+response[i].form_section_id+'.'+response[i].question_id;
                                        console.log(id);
                                        try{

                                        var type =  document.getElementsByName(id)[0].type;
                                        }catch{
                                            var type=null;
                                        }


                                        //continue;
                                    }
                                }

                                console.log(type);

                                if(type=='radio' || type=='checkbox')
                                {
                                    try
                                    {
                                        document.getElementsByName(id)[0].checked=true;
                                    }
                                    catch
                                    {
                                        document.getElementById(id).checked = true;

                                    }
                                }
                                else if( type=='select-one' || type==null)
                                {
                                     id ='dropdown.'+response[i].form_section_id+'.'+response[i].question_id;
                                     var selected =null;
                                     try
                                     {
                                        document.getElementsByName(id)[0].value = response[i].answer_id;
                                     }catch{

                                            continue;
                                     }
                                     console.log(id,response[i].answer_id);
                                        var val = response[i].answer_id;
                                        try{
                                            var sel = document.getElementsByName(id)[0];
                                               var opts = sel.options;
                                          for (var opt, j = 0; opt = opts[j]; j++) {
                                            if (opt.value == val) {
                                              sel.selectedIndex = j;
                                              selected=j;
                                              break;
                                            }
                                          }
                                        }catch
                                        {
                                            var sel=0;
                                        }


                                           var x="select#dropdown_"+response[i].form_section_id+'_'+response[i].question_id;
                                           var option = 'option:eq('+selected+')';
                                           $(x).find("[selected]").attr("selected", false);
                                            $(x).find(option).attr("selected", true);



                                }
                                else if( type=='text') {
                                    id = response[i].form_section_id+'.'+response[i].question_id;
                                    console.log(response[i].answer_text);
                                    document.getElementsByName(id)[0].value=response[i].answer_text;

                                }else if(type=='textarea')
                                {
                                    document.getElementsByName(id)[0].value=response[i].answer_text;
                                    var id = id.split('.');
                                    var nid = '.'+id[0]+'-'+id[1];
                                    console.log(nid);
                                    $(nid).summernote('code',response[i].answer_text);
                                }
                                else
                                {
                                    try{
                                    document.getElementById(id).selected = "true";

                                    }
                                    catch{
                                        continue;
                                    }
                                }


                            }
    }




    </script>
    <script src="{{asset('editor/sprite.svg.js')}}"></script>
    <script src="{{asset('custom/js/form_sections.js') }}"></script>
    <script src="{{asset('/custom/js/answers.js')}}"></script>
    <script src="{{asset('/custom/js/populate.js')}}"></script>
    <script src="{{asset('/js/Backend/mdb.js')}}"></script>
    <!-- <script src="{{asset('/custom/js/job.js')}}"></script> -->
@endsection
