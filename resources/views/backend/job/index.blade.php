@extends('backend.layouts.detail')
@section('title', 'Jobs')
@section('content')
    @php
        $data=localization();
    @endphp
    <style>
        .error_message{
            position: absolute;
            top: 98%;
            text-transform: initial !important;
            left: 0.3% !important;
        }
    </style>
    <link rel="stylesheet" href="{{ asset('custom/css/custom_style.css') }}">
    {{ Breadcrumbs::render('job') }}
    <div class="card">
        <div class="card-body">
            <div id="table" class="table-editable table-responsive">
                @if(Auth()->user()->hasPermissionTo('create_project') || Auth::user()->all_companies == 1 )
                    <span class="table-add float-right mb-3 mr-2">
                        <a class="text-success modalJob"  data-toggle="modal" data-target="#modalJob">
                            <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
                        </a>
                    </span>
                @endif
                <button id="deleteSelectedJob" class="btn btn-danger btn-sm">
                    Delete Selected Job
                </button>
                <!-- <button class="btn btn-primary btn-sm">
                  Restore Deleted Job
                </button>

                <button  style="display: none;">
                    Show Active Job
                </button>
                <button class="btn btn-warning btn-sm">
                   Excel Export
                </button>
                <button class="btn btn-success btn-sm">
                    Word Export
                </button> -->
                <table id="job_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="no-sort all_checkboxes_style">
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input project_checked" id="job_checkbox_all">
                                <label class="form-check-label" for="job_checkbox_all">
                                    <span class="assign_class label{{getKeyid('all',$data)}}" data-id="{{getKeyid('all',$data) }}" data-value="{{checkKey('all',$data) }}" >
                                        {!! checkKey('all',$data) !!}
                                    </span>
                                </label>
                            </div>
                        </th>
                        <th>   <span class="assign_class label{{getKeyid('form_name',$data)}}" data-id="{{getKeyid('form_name',$data) }}" data-value="{{checkKey('form_name',$data) }}" >
                            {!! checkKey('form_name',$data) !!}
                        </span>
                        </th>
                        <th> <span class="assign_class label{{getKeyid('location',$data)}}" data-id="{{getKeyid('location',$data) }}" data-value="{{checkKey('location',$data) }}" >
                            {!! checkKey('location',$data) !!}
                        </span>
                        </th>
                        <th> <span class="assign_class label{{getKeyid('location_break_down',$data)}}" data-id="{{getKeyid('location_break_down',$data) }}" data-value="{{checkKey('location_break_down',$data) }}" >
                            {!! checkKey('location_break_down',$data) !!}
                        </span>
                        </th>
                        <th>  <span class="assign_class label{{getKeyid('user_name',$data)}}" data-id="{{getKeyid('user_name',$data) }}" data-value="{{checkKey('user_name',$data) }}" >
                            {!! checkKey('user_name',$data) !!}
                        </span>
                        </th>
                        <th>  <span class="assign_class label{{getKeyid('date_time',$data)}}" data-id="{{getKeyid('date_time',$data) }}" data-value="{{checkKey('date_time',$data) }}" >
                            {!! checkKey('date_time',$data) !!}
                        </span>
                        </th>
                        <th id="action" class="no-sort all_action_btn"></th>
                    </tr>
                    </thead>

                        </tr>
                        </thead>

                        <tbody>
                        </tbody>
                      <!--   <tfoot>
                         <th></th>
                         <th></th>
                         <th></th>
                         <th></th>
                         <th></th>
                         <th></th>
                         <th></th>
                        </tfoot> -->
                    </table>
                     {{--    Edit Modal--}}
                    @include('backend.job.model.edit')

                {{--END Edit Modal--}}

                {{--Register Modal--}}
                @include('backend.job.model.create')

                {{--end modal register--}}

            </div>
        </div>
    </div>
    <!-- modal start -->
    <div class="modal fade" id="deleteModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1>Reason For Delete</h1>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <form method="POST" action="{{url('/job/delete')}}" onsubmit = "return(validate());">
                    @csrf
                    <div class="modal-body">
                        <input type="hidden" id="job_group_id" name="id">
                        <div class="md-form">

                            <textarea id="comment" name="delete_reason" class="md-textarea form-control" rows="3"></textarea>
                            <label for="comment">Comments</label>
                             <small id="er_comment" class="text-danger error_message"></small>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                        <button type="submit" class="btn btn-danger">Delete</button>

                    </div>
                </form>
            </div>
        </div>
    </div>
    @include('backend.label.input_label')
    <input type="hidden" class="get_current_path" value="{{Request::path()}}">
    @if(Auth()->user()->all_companies == 1)
        <input type="hidden" id="checkbox_permission" value="1">
        <input type="hidden" id="form_name_permission" value="1">
        <input type="hidden" id="location_permission" value="1">
        <input type="hidden" id="locatin_break_permission" value="1">
        <input type="hidden" id="user_permission" value="1">
        <input type="hidden" id="date_time_permission" value="1">
    @else
        <input type="hidden" id="checkbox_permission" value="{{Auth()->user()->hasPermissionTo('job_checkbox')}}">
        <input type="hidden" id="form_name_permission" value="{{Auth()->user()->hasPermissionTo('job_form_name')}}">
        <input type="hidden" id="location_permission" value="{{Auth()->user()->hasPermissionTo('job_location')}}">
        <input type="hidden" id="locatin_break_permission" value="{{Auth()->user()->hasPermissionTo('job_location_break_down')}}">
        <input type="hidden" id="user_permission" value="{{Auth()->user()->hasPermissionTo('job_user')}}">
        <input type="hidden" id="date_time_permission" value="{{Auth()->user()->hasPermissionTo('job_date_time')}}">
    @endif

    <script type="text/javascript">
        $(document).ready(function() {
            var current_path=$('.get_current_path').val();
            localStorage.setItem('current_path',current_path);

                $.fn.dataTable.moment = function ( format, locale ) {
                var types = $.fn.dataTable.ext.type;

                // Add type detection
                types.detect.unshift( function ( d ) {
                    return moment( d, format, locale, true ).isValid() ?
                        'moment-'+format :
                        null;
                } );

                // Add sorting method - use an integer for the sorting
                types.order[ 'moment-'+format+'-pre' ] = function ( d ) {
                    return moment( d, format, locale, true ).unix();
                };
            };

                                                        // UMD
                                (function( factory ) {
                                    "use strict";

                                    if ( typeof define === 'function' && define.amd ) {
                                        // AMD
                                        define( ['jquery'], function ( $ ) {
                                            return factory( $, window, document );
                                        } );
                                    }
                                    else if ( typeof exports === 'object' ) {
                                        // CommonJS
                                        module.exports = function (root, $) {
                                            if ( ! root ) {
                                                root = window;
                                            }

                                            if ( ! $ ) {
                                                $ = typeof window !== 'undefined' ?
                                                    require('jquery') :
                                                    require('jquery')( root );
                                            }

                                            return factory( $, root, root.document );
                                        };
                                    }
                                    else {
                                        // Browser
                                        factory( jQuery, window, document );
                                    }
                                }
                                (function( $, window, document ) {


                                $.fn.dataTable.render.moment = function ( from, to, locale ) {
                                    // Argument shifting
                                    if ( arguments.length === 1 ) {
                                        locale = 'en';
                                        to = from;
                                        from = 'YYYY-MM-DD';
                                    }
                                    else if ( arguments.length === 2 ) {
                                        locale = 'en';
                                    }

                                    return function ( d, type, row ) {
                                        if (! d) {
                                            return type === 'sort' || type === 'type' ? 0 : d;
                                        }

                                        var m = window.moment( d, from, locale, true );

                                        // Order and type get a number value from Moment, everything else
                                        // sees the rendered value
                                        return m.format( type === 'sort' || type === 'type' ? 'x' : to );
                                    };
                                };


                                }));


        });
         function validate()
      {

         var comment = $('#comment').val();
         var fl = true;

          if(comment=='' || comment==null )
               {
              console.log(comment,"comment");
              $("#comment").attr('style','border-bottom:1px solid #e3342f');
              $("#comment").focus();
              $("#er_comment").html('Field required');
              fl= false;
               }

             console.log(fl);
                return fl;

      }
        function checkSelected(value,checkValue)
        {
            console.log(value);
            if(value == checkValue)
            {
                return 'selected';
            }
            else
            {
                return "";
            }

        }

        function job_data(url) {
            console.log('interval');
            $('#job_table').DataTable().clear().destroy();
            $.fn.dataTable.moment('Do MMM YYYY');
            $.ajax({
                type: 'GET',
                url: url,
                success: function(response)
                {
                    var table = $('#job_table').dataTable(
                        {
                            processing: true,
                            language: {
                                'lengthMenu': '<span class="assign_class label'+$('#show_label').attr('data-id')+'" data-id="'+$('#show_label').attr('data-id')+'" data-value="'+$('#show_label').val()+'" ></span>  _MENU_ <span class="assign_class label'+$('#entries_label').attr('data-id')+'" data-id="'+$('#entries_label').attr('data-id')+'" data-value="'+$('#entries_label').val()+'" ></span>',
                                'search':'<span class="assign_class label'+$('#search_id').attr('data-id')+'" data-id="'+$('#search_id').attr('data-id')+'" data-value="'+$('#search_id').val()+'" >'+$('#search_id').val()+'</span>',
                                'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                                'info': '<span class="assign_class label'+$('#showing_page_label').attr('data-id')+'" data-id="'+$('#showing_page_label').attr('data-id')+'" data-value="'+$('#showing_page_label').val()+'" >'+$('#showing_page_label').val()+'</span> _PAGE_ <span class="assign_class label'+$('#of_label').attr('data-id')+'" data-id="'+$('#of_label').attr('data-id')+'" data-value="'+$('#of_label').val()+'" >'+$('#of_label').val()+'</span> _PAGES_',
                                'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                                'paginate': {
                                    'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                                    'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                                },
                                'infoFiltered': " "
                            },
                            "ajax": {
                                "url": url,
                                "type": 'get',
                            },
                            "createdRow": function( row, data,dataIndex,columns )
                            {
                                var checkbox_permission=$('#checkbox_permission').val();
                                var checkbox='';
                                checkbox+='<div class="form-check"><input type="checkbox" class="form-check-input selectedrowjob_checked" id="job'+data.id+'"><label class="form-check-label" for="job'+data.id+'"></label></div>';

                                if(data['forms']['req_delete']!=1)
                                {
                                    $(columns[0]).html(checkbox);
                                    $(row).attr('class', 'selectedrowjob');
                                }
                                var current = new Date();
                                $(row).attr('id', 'job_tr'+data['id']);
                                try
                                {
                                    if(current == new Date(data.jobs[0].date_time))
                                    {
                                        $(row).attr('style',"background-color: #00c851");
                                    }
                                    else if(current< new Date(data.jobs[0].date_time) && current>=new Date(data.jobs[0].pre_time))
                                    {
                                        $(row).attr('style',"background-color: #ffb347");

                                    }
                                    else if(current>= new Date(data.jobs[0].post_time))
                                    {
                                        $(row).attr('style',"background-color: #fc685f");

                                    }
                                }catch(e)
                                {
                                    console.log(e);
                                }

                                if(data['completed']==2)
                                {
                                    $(row).attr('style',"background-color: #00c851");
                                }
                                if(data['completed']==-1)
                                {
                                    $(row).attr('style',"background-color: #DDA0DD");
                                }

                                var temp=data['id'];
                            },
                            columns:
                                [
                                    {data:'checkbox', name:'checkbox',visible:$('#checkbox_permission').val()},
                                    {data:'form_name',name:'form_name',visible:$('#form_name_permission').val()},
                                    {data:'location_name',name:'location_name',visible:$('#location_permission').val()},
                                    {data:'location_breakdown',name:'location_breakdown',visible:$('#locatin_break_permission').val()},
                                    {data:'employee_name', name:'employee_name',visible:$('#user_permission').val()},
                                    {data:'date_time',name:'date_time',visible:$('#date_time_permission').val()},
                                    {data:'actions', name:'actions'},
                                ], columnDefs: [ {
                                          targets: 5,
                                          render: $.fn.dataTable.render.moment( 'Do MMM YYYY' )
                                        } ],

                        }
                    );

                },
                error: function (error) {
                    console.log(error);
                }
            });

        }

        var id = @json($id);
        if(id==0)
        {
            var url='/job/getall/0';
        }
        else
            var url='/job/getall/'+id;

        console.log(url);
        // job_data(url);


    </script>
    <script type="text/javascript">
            @if(Session::has('message'))
        var type = "{{ Session::get('alert-type', 'info') }}";
        switch(type){
            case 'info':
                toastr.info("{{ Session::get('message') }}");
                break;
            case 'warning':
                toastr.warning("{{ Session::get('message') }}");
                break;
            case 'success':
                toastr.success("{{ Session::get('message') }}");
                break;

            case 'error':
                toastr.error("{{ Session::get('message') }}");
                break;
        }
        @endif
    </script>
       <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/plug-ins/1.10.21/sorting/datetime-moment.js"></script>
     <script type="text/javascript" src="//cdn.datatables.net/plug-ins/1.10.21/dataRender/datetime.js"></script>
     <script src="{{ asset('js/Backend/mdb.min.js') }}"></script>
    <script src="{{ asset('js/Backend/mdb.js') }}"></script>
    <script src="{{asset('/custom/js/job.js')}}"></script>

@endsection
