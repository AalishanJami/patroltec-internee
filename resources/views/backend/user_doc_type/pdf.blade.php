@include('backend.layouts.pdf_start')
  <thead>
    <tr>
       <th style="vertical-align: text-top;" class="pdf_table_layout">Doc Type Name</th>
       <th style="vertical-align: text-top;" class="pdf_table_layout">Position Name</th>
    </tr>
  </thead>
  <tbody>
  @foreach($data as $value)
    <tr>
      <td class="pdf_table_layout"><p class="col_text_style">{{ isset($value->doc_type->name) ? $value->doc_type->name : ''}}
      </p></td>
      <td class="pdf_table_layout"><p class="col_text_style">{{ isset($value->position->name) ? $value->position->name : ''}}
      </p></td>
    </tr>
  @endforeach
  </tbody>
@include('backend.layouts.pdf_end')
