<table>
    <thead>
    <tr>
        <th>Doc Type Name</th>
        <th>Position Name</th>
    </tr>
    </thead>
    <tbody>
    @foreach($data as $value)
        <tr>
            <td>{{ $value->doc_type->name }}</td>
            <td>{{ $value->position->name }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
