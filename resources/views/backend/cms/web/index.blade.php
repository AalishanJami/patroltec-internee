@extends('backend.layouts.company')
@section('content')
    @php
        $data=localization();
    @endphp
    @include('backend.layouts.company_sidebar')
    <div class="padding-left-custom remove-padding">
        {{ Breadcrumbs::render('cms/web') }}
        <div class="card">
            <div class="card-body">
                    <span class="table-add float-right mb-3 mr-2">
                        <a class="text-success"  data-toggle="modal" data-target="#modalcmsLogin">
                            <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
                        </a>
                    </span>
                @include('backend.label.input_label')
                <table id="editLoginDatatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="th-sm assign_class label{{getKeyid('key',$data)}}" data-id="{{getKeyid('key',$data) }}" data-value="{{ checkKey('key',$data) }}" >{!!checkKey('key',$data) !!}</th>
                        <th class="th-sm assign_class label{{getKeyid('value',$data)}}" data-id="{{getKeyid('value',$data) }}" data-value="{{ checkKey('value',$data) }}" >{!!checkKey('value',$data) !!}</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @component('backend.edit_login.model.create')
    @endcomponent
    {{--    Edit Modal--}}
    @component('backend.edit_login.model.edit')
    @endcomponent
    {{--    END Edit Modal--}}
    {{--    Edit Modal--}}
    @component('backend.edit_login.model.create')
    @endcomponent

    <script type="text/javascript">
        $(document).ready(function() {
            $(function () {
                editLoginAppend();
            });
            (function ($) {
                var active ='<span class="assign_class label'+$('#cms_label').attr('data-id')+'" data-id="'+$('#cms_label').attr('data-id')+'" data-value="'+$('#cms_label').val()+'" >'+$('#cms_label').val()+'</span>';
                $('.breadcrumb-item.active').html(active);
                var parent ='<span class="assign_class label'+$('#dashboard_label').attr('data-id')+'" data-id="'+$('#dashboard_label').attr('data-id')+'" data-value="'+$('#dashboard_label').val()+'" >'+$('#dashboard_label').val()+'</span>';
                $('.breadcrumb-item a').html(parent);
            }(jQuery));

        });
        function editLoginAppend()
        {
            var table = $('#editLoginDatatable').dataTable({
                processing: true,
                language: {
                    'lengthMenu':'_MENU_ ',
                    'search':'<span class="assign_class label'+$('#search_label').attr('data-id')+'" data-id="'+$('#search_label').attr('data-id')+'" data-value="'+$('#search_label').val()+'" >'+$('#search_label').val()+'</span>',
                    'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                    'info': '<span class="assign_class label'+$('#showing_page_label').attr('data-id')+'" data-id="'+$('#showing_page_label').attr('data-id')+'" data-value="'+$('#showing_page_label').val()+'" >'+$('#showing_page_label').val()+'</span> _PAGE_ <span class="assign_class label'+$('#of_label').attr('data-id')+'" data-id="'+$('#of_label').attr('data-id')+'" data-value="'+$('#of_label').val()+'" >'+$('#of_label').val()+'</span> _PAGES_',
                    'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                    'paginate': {
                        'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                        'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                    },
                    'infoFiltered': "(filtered from _MAX_ total records)"
                },
                "ajax": {
                    "url": '/getalllogincms',
                    "type": 'get',
                },
                "createdRow": function( row, data, dataIndex ) {
                    // $(row).attr('id', data['id']);
                    // var temp=data['id'];
                    // $(row).attr('onclick',"selectedrowuser(this.id)");
                },
                columns: [
                    {data: 'key', name: 'key'},
                    {data: 'value', name: 'value'},
                    {data: 'actions', name: 'actions'},
                ],
            });
        }
    </script>


@endsection
