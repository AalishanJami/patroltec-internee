@extends('backend.layouts.company')
@section('content')
    @php
        $data=localization();
    @endphp
    @include('backend.layouts.company_sidebar')
    <div class="padding-left-custom remove-padding">
        {{ Breadcrumbs::render('cms/app') }}
        <div class="accordion md-accordion accordion-1" id="accordionEx23" role="tablist">
            <div class="card">
                <div class="card-body">
                    <div class="card">
                        <div class="card-header blue lighten-3 z-depth-1" role="tab" id="heading97">
                            <h5 class="text-uppercase mb-0 py-1">
                                <a class="collapsed font-weight-bold white-text" data-toggle="collapse" href="#collapse97"
                                   aria-expanded="false" aria-controls="collapse97">
                                    General Setting
                                </a>
                            </h5>
                        </div>
                        <div id="collapse97" class="collapse" role="tabpanel" aria-labelledby="heading97"
                             data-parent="#accordionEx23">
                            <div class="card-body">
                                <div class="row my-4">
                                    <div class="col-md-8">
                                        <div class="md-form mb-5">
                                            <i class="fas fa-tags prefix grey-text"></i>
                                            <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="value" required autocomplete="name" autofocus>
                                            <label data-error="wrong" data-success="right" for="name" class="active">
                                                Key
                                                <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                                                   data-content="tool tip for Key"></i>
                                            </label>
                                        </div>
                                        <div class="md-form mb-5">
                                            <i class="fas fa-tags prefix grey-text"></i>
                                            <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="value" required autocomplete="name" autofocus>
                                            <label data-error="wrong" data-success="right" for="name" class="active">
                                                Key
                                                <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                                                   data-content="tool tip for Key"></i>
                                            </label>
                                        </div>
                                        <div class="md-form mb-5">
                                            <i class="fas fa-tags prefix grey-text"></i>
                                            <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="value" required autocomplete="name" autofocus>
                                            <label data-error="wrong" data-success="right" for="name" class="active">
                                                Key
                                                <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                                                   data-content="tool tip for Key"></i>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header blue lighten-3 z-depth-1" role="tab" id="heading98">
                            <h5 class="text-uppercase mb-0 py-1">
                                <a class="collapsed font-weight-bold white-text" data-toggle="collapse" href="#collapse98"
                                   aria-expanded="false" aria-controls="collapse98">
                                    Login
                                </a>
                            </h5>
                        </div>
                        <div id="collapse98" class="collapse" role="tabpanel" aria-labelledby="heading98"
                             data-parent="#accordionEx23">
                            <div class="card-body">
                                <div class="row my-4">
                                    <div class="col-md-8">
                                        <div class="md-form mb-5">
                                            <i class="fas fa-tags prefix grey-text"></i>
                                            <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="value" required autocomplete="name" autofocus>
                                            <label data-error="wrong" data-success="right" for="name" class="active">
                                                Key
                                                <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                                                   data-content="tool tip for Key"></i>
                                            </label>
                                        </div>
                                        <div class="md-form mb-5">
                                            <i class="fas fa-tags prefix grey-text"></i>
                                            <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="value" required autocomplete="name" autofocus>
                                            <label data-error="wrong" data-success="right" for="name" class="active">
                                                Key
                                                <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                                                   data-content="tool tip for Key"></i>
                                            </label>
                                        </div>
                                        <div class="md-form mb-5">
                                            <i class="fas fa-tags prefix grey-text"></i>
                                            <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="value" required autocomplete="name" autofocus>
                                            <label data-error="wrong" data-success="right" for="name" class="active">
                                                Key
                                                <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                                                   data-content="tool tip for Key"></i>
                                            </label>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header blue lighten-3 z-depth-1" role="tab" id="heading98">
                            <h5 class="text-uppercase mb-0 py-1">
                                <a class="collapsed font-weight-bold white-text" data-toggle="collapse" href="#collapse99"
                                   aria-expanded="false" aria-controls="collapse99">
                                    Forms
                                </a>
                            </h5>
                        </div>
                        <div id="collapse99" class="collapse" role="tabpanel" aria-labelledby="heading98"
                             data-parent="#accordionEx23">
                            <div class="card-body">
                                <div class="row my-4">
                                    <div class="col-md-8">
                                        <div class="md-form mb-5">
                                            <i class="fas fa-tags prefix grey-text"></i>
                                            <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="value" required autocomplete="name" autofocus>
                                            <label data-error="wrong" data-success="right" for="name" class="active">
                                                Key
                                                <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                                                   data-content="tool tip for Key"></i>
                                            </label>
                                        </div>
                                        <div class="md-form mb-5">
                                            <i class="fas fa-tags prefix grey-text"></i>
                                            <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="value" required autocomplete="name" autofocus>
                                            <label data-error="wrong" data-success="right" for="name" class="active">
                                                Key
                                                <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                                                   data-content="tool tip for Key"></i>
                                            </label>
                                        </div>
                                        <div class="md-form mb-5">
                                            <i class="fas fa-tags prefix grey-text"></i>
                                            <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="value" required autocomplete="name" autofocus>
                                            <label data-error="wrong" data-success="right" for="name" class="active">
                                                Key
                                                <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                                                   data-content="tool tip for Key"></i>
                                            </label>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backend.label.input_label')
    <script type="text/javascript">
        setInterval(ceck(),1000);
        function ceck()
        {
            console.log('ere');
        }
    </script>
@endsection
