@extends('backend.layouts.backend')
@section('title', 'QR Report')
@section('content')
    @php
        $data=localization();
    @endphp
    {{ Breadcrumbs::render('project') }}
    <style type="text/css">
        td{
            color: black !important;
        }
        .remove-all-styles {
            all: revert;
        }
    </style>
    <div class="card">
        <div class="card-body">
            @if(Auth()->user()->hasPermissionTo('csv_qrReport') || Auth::user()->all_companies == 1 )
                <form class="form-style" id="export_excel_project" method="POST" action="{{ url('/report/qr/excel/export') }}">
                    {!! csrf_field() !!}
                    <input type="hidden" class="report_qr_export" name="excel_array" value="1">
                    <button  type="submit" class="form_submit_check btn btn-warning btn-sm">
                        <span class="assign_class label{{getKeyid('excel_export',$data)}}" data-id="{{getKeyid('excel_export',$data) }}" data-value="{{checkKey('excel_export',$data) }}" >
                          {!! checkKey('excel_export',$data) !!}
                        </span>
                    </button>
                </form>
            @endif
            @if(Auth()->user()->hasPermissionTo('word_qrReport') || Auth::user()->all_companies == 1 )
                <form class="form-style" method="POST" id="export_world_project" action="{{ url('/report/qr/word/export') }}">
                    {!! csrf_field() !!}
                    <input type="hidden" class="project_export" name="word_array" value="1">
                    <button  type="submit" class="form_submit_check btn btn-success btn-sm">
                        <span class="assign_class label{{getKeyid('word_export',$data)}}" data-id="{{getKeyid('word_export',$data) }}" data-value="{{checkKey('word_export',$data) }}" >
                            {!! checkKey('word_export',$data) !!}
                        </span>
                    </button>
                </form>
            @endif
            @if(Auth()->user()->hasPermissionTo('pdf_qrReport') || Auth::user()->all_companies == 1 )
                <form  class="form-style" {{pdf_view('location_breakdowns','','t')}} method="POST" id="export_pdf_project" action="{{ url('/report/qr/pdf/export') }}">
                    {!! csrf_field() !!}
                    <input type="hidden" class="project_export" name="pdf_array" value="1">
                    <button  type="submit" class="form_submit_check btn btn-secondary btn-sm">
                        <span class="assign_class label{{getKeyid('pdf_export',$data)}}" data-id="{{getKeyid('pdf_export',$data) }}" data-value="{{checkKey('pdf_export',$data) }}" >
                            {!! checkKey('pdf_export',$data) !!}
                        </span>
                    </button>
                </form>
            @endif
            <input type="hidden" class="type" value="2">
            <div class="row">
                <div class="col-md-2 md-form active-cyan-2 mb-3">
                  <input class="form-control custom_search" id="location_id" type="text" placeholder="Project" aria-label="Project">
                </div>
                <div class="col-md-2 md-form active-cyan-2 mb-3">
                  <input class="form-control custom_search" id="name" type="text" placeholder="Location Break Down" aria-label="Location Break Down">
                </div>
                <div class="col-md-2 md-form active-cyan-2 mb-3">
                    <button  type="button" class="btn btn-secondary btn-sm search">
                        <span class="assign_class label{{getKeyid('filter',$data)}}" data-id="{{getKeyid('filter',$data) }}" data-value="{{checkKey('filter',$data) }}" >
                            {!! checkKey('filter',$data) !!}
                        </span>
                    </button>
                </div>
            </div>
            <div id="table-qr" class="table-editable table-responsive">
                <table id="qr_table" class=" table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                       <th class="no-sort">
                            <span class="assign_class label{{getKeyid('qr',$data)}}" data-id="{{getKeyid('qr',$data) }}" data-value="{{checkKey('qr',$data) }}" >
                                {!! checkKey('qr',$data) !!}
                            </span>
                        </th>
                        <th>
                            <span class="assign_class label{{getKeyid('project',$data)}}" data-id="{{getKeyid('project',$data) }}" data-value="{{checkKey('project',$data) }}" >
                                {!! checkKey('project',$data) !!}
                            </span>
                        </th>
                        <th>
                            <span class="assign_class label{{getKeyid('name',$data)}}" data-id="{{getKeyid('name',$data) }}" data-value="{{checkKey('name',$data) }}" >
                                {!! checkKey('name',$data) !!}
                            </span>
                        </th>
                        <th>
                            <span class="assign_class label{{getKeyid('directions',$data)}}" data-id="{{getKeyid('directions',$data) }}" data-value="{{checkKey('directions',$data) }}" >
                                {!! checkKey('directions',$data) !!}
                            </span>
                        </th>
                        <th>
                            <span class="assign_class label{{getKeyid('information',$data)}}" data-id="{{getKeyid('information',$data) }}" data-value="{{checkKey('information',$data) }}" >
                                {!! checkKey('information',$data) !!}
                            </span>
                        </th>
                    </tr>
                    </thead>
                    <tbody id="qr_data"></tbody>
                </table>
            </div>
            @if(Auth()->user()->all_companies == 1)
                <input type="hidden" id="qrReport_checkbox" value="1">
                <input type="hidden" id="qrReport_qr" value="1">
                <input type="hidden" id="qrReport_project" value="1">
                <input type="hidden" id="qrReport_name" value="1">
                <input type="hidden" id="qrReport_directions" value="1">
                <input type="hidden" id="qrReport_information" value="1">
            @else
                <input type="hidden" id="qrReport_checkbox" value="{{Auth()->user()->hasPermissionTo('qrReport_checkbox')}}">
                <input type="hidden" id="qrReport_qr" value="{{Auth()->user()->hasPermissionTo('qrReport_qr')}}">
                <input type="hidden" id="qrReport_project" value="{{Auth()->user()->hasPermissionTo('qrReport_project')}}">
                <input type="hidden" id="qrReport_name" value="{{Auth()->user()->hasPermissionTo('qrReport_name')}}">
                <input type="hidden" id="qrReport_directions" value="{{Auth()->user()->hasPermissionTo('qrReport_directions')}}">
                <input type="hidden" id="qrReport_information" value="{{Auth()->user()->hasPermissionTo('qrReport_information')}}">
            @endif
        </div>
    </div>
    <input type="hidden" class="get_current_path" value="{{Request::path()}}">
    @include('backend.label.input_label')
    <script type="text/javascript" src="{{ asset('custom/js/custom_search.js') }}" ></script>
    <script type="text/javascript">
        function qr_data(url,methodType,serach_array){
            var table = $('#qr_table').dataTable(
                {
                    processing: true,
                    language: {
                        'lengthMenu': '  _MENU_ ',
                        'search':'<span class="assign_class label'+$('#search_id').attr('data-id')+'" data-id="'+$('#search_id').attr('data-id')+'" data-value="'+$('#search_id').val()+'" >'+$('#search_id').val()+'</span>',
                        'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                        'info':'',
                        'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                        'paginate': {
                            'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                            'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                        },
                        'infoFiltered': "(filtered from _MAX_ total records)"
                    },
                    "ajax": {
                        "headers": {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        "url": url,
                        "method": methodType,
                        "data":  {'data': serach_array},
                    },
                    "createdRow": function( row, data, dataIndex,columns )
                    {
                        var qr='';
                        qr+= '<img src="{{asset("/qr")}}/'+data.name+'.png"  width="50" height="50">';
                        $(columns[0]).html(qr);
                        $(columns[0]).attr('class','qr_image'+data['id']);
                        $(row).attr('data-id',data['id']);
                    },
                    columns:
                        [
                            {data: 'qr', name: 'qr',visible:$('#qrReport_qr').val()},
                            {data: 'project', name: 'project',visible:$('#qrReport_project').val()},
                            {data: 'name', name: 'name',visible:$('#qrReport_name').val()},
                            {data: 'directions', name: 'directions',visible:$('#qrReport_directions').val()},
                            {data: 'information', name: 'information',visible:$('#qrReport_information').val()},
                        ],
                }
            );
            (function ($) {
                var active ='<span class="assign_class label'+$('#breadcrumb_qr').attr('data-id')+'" data-id="'+$('#breadcrumb_qr').attr('data-id')+'" data-value="'+$('#breadcrumb_qr').val()+'" >'+$('#breadcrumb_qr').val()+'</span>';
                $('.breadcrumb-item.active').html(active);
                var parent ='<span class="assign_class label'+$('#dashboard_label').attr('data-id')+'" data-id="'+$('#dashboard_label').attr('data-id')+'" data-value="'+$('#dashboard_label').val()+'" >'+$('#dashboard_label').val()+'</span>';
                $('.breadcrumb-item a').html(parent);
            }(jQuery));
        }

        $(document).ready(function () {
            var current_path=$('.get_current_path').val();
            localStorage.setItem('current_path',current_path);
            var url='/report/qr/list/getAllQRList';
            qr_data(url,'get','');
            $("body").on('click','.edit_inlinepe',function () {
                $(".equipment_show_button_"+$(this).attr('data-id')).show();
            });
        });
        $(document).ready(function () {
            $("body").on('click','.edit_inlineqr',function () {
                $(".equipment_show_button_"+$(this).attr('data-id')).show();
            });
        });
    </script>
@endsection
