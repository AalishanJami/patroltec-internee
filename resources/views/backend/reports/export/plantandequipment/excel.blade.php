<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
<table>
    <thead>
    <tr>
        @if(Auth()->user()->hasPermissionTo('plantandequipmentReport_project_excel_export') || Auth::user()->all_companies == 1 )
            <th>Project</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('plantandequipmentReport_name_excel_export') || Auth::user()->all_companies == 1 )
            <th>Name</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('plantandequipmentReport_equipment_excel_export') || Auth::user()->all_companies == 1 )
            <th>Equipment</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('plantandequipmentReport_serialnumber_excel_export') || Auth::user()->all_companies == 1 )
            <th>Serial Number</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('plantandequipmentReport_make_excel_export') || Auth::user()->all_companies == 1 )
            <th>Make</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('plantandequipmentReport_model_excel_export') || Auth::user()->all_companies == 1 )
            <th>Model</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('plantandequipmentReport_lastservicedate_excel_export') || Auth::user()->all_companies == 1 )
            <th>Last Service Date</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('plantandequipmentReport_nextservicedate_excel_export') || Auth::user()->all_companies == 1 )
            <th>Next Service Date</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('plantandequipmentReport_condition_excel_export') || Auth::user()->all_companies == 1 )
            <th>Condition</th>
        @endif
    </tr>
    </thead>
    <tbody>
    @foreach($data as $value)
        <tr>
            @if(Auth()->user()->hasPermissionTo('plantandequipmentReport_project_excel_export') || Auth::user()->all_companies == 1 )
                <td>
                    @if (!empty($value->location->name))
                        {{$value->location->name}}
                    @endif
                </td>
            @endif
            @if(Auth()->user()->hasPermissionTo('plantandequipmentReport_name_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{$value->name}}</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('plantandequipmentReport_equipment_excel_export') || Auth::user()->all_companies == 1 )
                <td>
                    @if (!empty($value->equipment->name))
                        {{$value->equipment->name}}
                    @endif
                </td>
            @endif
            @if(Auth()->user()->hasPermissionTo('plantandequipmentReport_serialnumber_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{$value->serial_number}}</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('plantandequipmentReport_make_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{$value->make}}</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('plantandequipmentReport_model_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{$value->model}}</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('plantandequipmentReport_lastservicedate_excel_export') || Auth::user()->all_companies == 1 )
                    @if(isset($value->last_service_date))
                    <td>{{date('jS M Y',strtotime($value->last_service_date))}}</td>
                    @else
                        <td></td>
                    @endif
            @endif
            @if(Auth()->user()->hasPermissionTo('plantandequipmentReport_nextservicedate_excel_export') || Auth::user()->all_companies == 1 )
                    @if(isset($value->next_service_date))
                        <td>{{date('jS M Y',strtotime($value->next_service_date))}}</td>
                    @else
                        <td></td>
                    @endif
            @endif
            @if(Auth()->user()->hasPermissionTo('plantandequipmentReport_condition_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{$value->condition}}</td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>
</body>
</html>
