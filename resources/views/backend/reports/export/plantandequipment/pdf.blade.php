@include('backend.layouts.pdf_start')
    <thead>
    <tr>
            <th style="vertical-align: text-top;" class="pdf_table_layout">QR</th>
        @if(Auth()->user()->hasPermissionTo('plantandequipmentReport_project_pdf_export') || Auth::user()->all_companies == 1 )
           <th style="vertical-align: text-top;" class="pdf_table_layout">Project</th>
        @else
            <td class="pdf_table_layout">
                <p class="col_text_style"></p>
            </td>
        @endif
        @if(Auth()->user()->hasPermissionTo('plantandequipmentReport_name_pdf_export') || Auth::user()->all_companies == 1 )
           <th style="vertical-align: text-top;" class="pdf_table_layout">Name</th>
        @else
            <td class="pdf_table_layout"><p class="col_text_style">
            </p></td>
        @endif
        @if(Auth()->user()->hasPermissionTo('plantandequipmentReport_equipment_pdf_export') || Auth::user()->all_companies == 1 )
           <th style="vertical-align: text-top;" class="pdf_table_layout">Equipment</th>
        @else
            <td class="pdf_table_layout"><p class="col_text_style">
            </p></td>
        @endif
        @if(Auth()->user()->hasPermissionTo('plantandequipmentReport_serialnumber_pdf_export') || Auth::user()->all_companies == 1 )
           <th style="vertical-align: text-top;" class="pdf_table_layout">Serial Number</th>
        @else
            <td class="pdf_table_layout"><p class="col_text_style">
            </p></td>
        @endif
        @if(Auth()->user()->hasPermissionTo('plantandequipmentReport_make_pdf_export') || Auth::user()->all_companies == 1 )
           <th style="vertical-align: text-top;" class="pdf_table_layout">Make</th>
        @else
            <td class="pdf_table_layout"><p class="col_text_style">
            </p></td>
        @endif
        @if(Auth()->user()->hasPermissionTo('plantandequipmentReport_model_pdf_export') || Auth::user()->all_companies == 1 )
           <th style="vertical-align: text-top;" class="pdf_table_layout">Model</th>
        @else
            <td class="pdf_table_layout"><p class="col_text_style">
            </p></td>
        @endif
        @if(Auth()->user()->hasPermissionTo('plantandequipmentReport_lastservicedate_pdf_export') || Auth::user()->all_companies == 1 )
           <th style="vertical-align: text-top;" class="pdf_table_layout">Last Service Date</th>
        @else
            <td class="pdf_table_layout"><p class="col_text_style">
            </p></td>
        @endif
        @if(Auth()->user()->hasPermissionTo('plantandequipmentReport_nextservicedate_pdf_export') || Auth::user()->all_companies == 1 )
           <th style="vertical-align: text-top;" class="pdf_table_layout">Next Service Date</th>
        @else
            <td class="pdf_table_layout"><p class="col_text_style">
            </p></td>
        @endif
        @if(Auth()->user()->hasPermissionTo('plantandequipmentReport_condition_pdf_export') || Auth::user()->all_companies == 1 )
           <th style="vertical-align: text-top;" class="pdf_table_layout">Condition</th>
        @else
            <td class="pdf_table_layout"><p class="col_text_style">
            </p></td>
        @endif
    </tr>
    </thead>
    <tbody>
        @foreach($data as $value)
            <tr>
                <td class="pdf_table_layout">
                    <img src="{{public_path("/qr/".$value->name.".png")}}"  width="50" height="50">
                </td>
                @if(Auth()->user()->hasPermissionTo('plantandequipmentReport_project_pdf_export') || Auth::user()->all_companies == 1 )
                    <td class="pdf_table_layout">
                        <p class="col_text_style">
                        @if (!empty($value->location->name))
                            {{$value->location->name}}
                        @endif
                        </p>
                    </td>
                @else
                    <td class="pdf_table_layout"><p class="col_text_style">
                    </p></td>
                @endif
                @if(Auth()->user()->hasPermissionTo('plantandequipmentReport_name_pdf_export') || Auth::user()->all_companies == 1 )
                    <td class="pdf_table_layout"><p class="col_text_style">{{$value->name}}
                    </p></td>
                @else
                    <td class="pdf_table_layout"><p class="col_text_style">
                    </p></td>
                @endif
                @if(Auth()->user()->hasPermissionTo('plantandequipmentReport_equipment_pdf_export') || Auth::user()->all_companies == 1 )
                    <td class="pdf_table_layout"><p class="col_text_style">
                        @if (!empty($value->equipment->name))
                            {{$value->equipment->name}}
                        @endif

                    </p></td>
                @else
                    <td class="pdf_table_layout"><p class="col_text_style">
                    </p></td>
                @endif
                @if(Auth()->user()->hasPermissionTo('plantandequipmentReport_serialnumber_pdf_export') || Auth::user()->all_companies == 1 )
                    <td class="pdf_table_layout"><p class="col_text_style">{{$value->serial_number}}
                    </p></td>
                @else
                    <td class="pdf_table_layout"><p class="col_text_style">
                    </p></td>
                @endif
                @if(Auth()->user()->hasPermissionTo('plantandequipmentReport_make_pdf_export') || Auth::user()->all_companies == 1 )
                    <td class="pdf_table_layout"><p class="col_text_style">{{$value->make}}
                    </p></td>
                @else
                    <td class="pdf_table_layout"><p class="col_text_style">
                    </p></td>
                @endif
                @if(Auth()->user()->hasPermissionTo('plantandequipmentReport_model_pdf_export') || Auth::user()->all_companies == 1 )
                    <td class="pdf_table_layout"><p class="col_text_style">{{$value->model}}
                    </p></td>
                @else
                    <td class="pdf_table_layout"><p class="col_text_style">
                    </p></td>
                @endif

                @if(Auth()->user()->hasPermissionTo('plantandequipmentReport_lastservicedate_pdf_export') || Auth::user()->all_companies == 1 )
                    @if(isset($value->last_service_date))
                        <td class="pdf_table_layout"><p class="col_text_style">{{date('jS M Y',strtotime($value->last_service_date))}}
                        </p></td>
                    @else
                        <td class="pdf_table_layout"><p class="col_text_style">
                            </p></td>
                    @endif
                @else
                    <td class="pdf_table_layout"><p class="col_text_style">
                    </p></td>
                @endif

                @if(Auth()->user()->hasPermissionTo('plantandequipmentReport_nextservicedate_pdf_export') || Auth::user()->all_companies == 1 )
                    @if(isset($value->next_service_date))
                       <td class="pdf_table_layout"><p class="col_text_style">{{date('jS M Y',strtotime($value->next_service_date))}}
                        </p></td>
                    @else
                        <td class="pdf_table_layout"><p class="col_text_style">
                            </p></td>

                    @endif
                @else
                    <td class="pdf_table_layout"><p class="col_text_style">
                    </p></td>
                @endif
                @if(Auth()->user()->hasPermissionTo('plantandequipmentReport_condition_pdf_export') || Auth::user()->all_companies == 1 )
                    <td class="pdf_table_layout"><p class="col_text_style">{{$value->condition}}
                    </p></td>
                @else
                    <td class="pdf_table_layout"><p class="col_text_style">
                    </p></td>
                @endif
            </tr>
        @endforeach
    </tbody>
@include('backend.layouts.pdf_end')
