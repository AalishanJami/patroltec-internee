@include('backend.layouts.pdf_start')
    <thead>
    <tr>
        <th style="vertical-align: text-top;" class="pdf_table_layout">Qr</th>
        @if(Auth()->user()->hasPermissionTo('qrReport_project_excel_export') || Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout">Project</th>
        @else
            <th style="vertical-align: text-top;" class="pdf_table_layout"></th>
        @endif
        @if(Auth()->user()->hasPermissionTo('qrReport_name_excel_export') || Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout">Name</th>
        @else
            <th style="vertical-align: text-top;" class="pdf_table_layout"></th>
        @endif
        @if(Auth()->user()->hasPermissionTo('qrReport_directions_excel_export') || Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout">Direction</th>
        @else
            <th style="vertical-align: text-top;" class="pdf_table_layout"></th>
        @endif
        @if(Auth()->user()->hasPermissionTo('qrReport_information_excel_export') || Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout">Information</th>
        @else
            <th style="vertical-align: text-top;" class="pdf_table_layout"></th>
        @endif
    </tr>
    </thead>
    <tbody>
    @foreach($data as $value)
        <tr>
            <td class="pdf_table_layout">
                <img src="{{public_path("/qr/".$value->name.".png")}}"  width="50" height="50">
            </td>
            @if(Auth()->user()->hasPermissionTo('qrReport_project_excel_export') || Auth::user()->all_companies == 1 )
                <td class="pdf_table_layout"><p class="col_text_style">
                    @if (!empty($value->location->name))
                        {{$value->location->name}}
                    @endif
                </p></td>
            @else
                <td class="pdf_table_layout"><p class="col_text_style"></p></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('qrReport_name_excel_export') || Auth::user()->all_companies == 1 )
                <td class="pdf_table_layout"><p class="col_text_style">{{$value->name}}</p></td>
            @else
                <td class="pdf_table_layout"><p class="col_text_style"></p></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('qrReport_directions_excel_export') || Auth::user()->all_companies == 1 )
                <td class="pdf_table_layout"><p class="col_text_style">{{$value->directions}}</p></td>
            @else
                <td class="pdf_table_layout"><p class="col_text_style"></p></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('qrReport_information_excel_export') || Auth::user()->all_companies == 1 )
                <td class="pdf_table_layout"><p class="col_text_style">{{$value->information}}</p></td>
            @else
                <td class="pdf_table_layout"><p class="col_text_style"></p></td>
            @endif
        </tr>
    @endforeach
    </tbody>
@include('backend.layouts.pdf_end')
