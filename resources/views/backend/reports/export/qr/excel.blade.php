<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
<table>
    <thead>
    <tr>
        @if(Auth()->user()->hasPermissionTo('qrReport_project_excel_export') || Auth::user()->all_companies == 1 )
            <th>Project</th>
        @else
            <th></th>
        @endif
        @if(Auth()->user()->hasPermissionTo('qrReport_name_excel_export') || Auth::user()->all_companies == 1 )
            <th>Name</th>
        @else
            <th></th>
        @endif
        @if(Auth()->user()->hasPermissionTo('qrReport_directions_excel_export') || Auth::user()->all_companies == 1 )
            <th>Direction</th>
        @else
            <th></th>
        @endif
        @if(Auth()->user()->hasPermissionTo('qrReport_information_excel_export') || Auth::user()->all_companies == 1 )
            <th>Information</th>
        @else
            <th></th>
        @endif
    </tr>
    </thead>
    <tbody>
    @foreach($data as $value)
        <tr>
            @if(Auth()->user()->hasPermissionTo('qrReport_project_excel_export') || Auth::user()->all_companies == 1 )
                <td>
                    @if (!empty($value->location->name))
                        {{$value->location->name}}
                    @endif
                </td>
            @else
                <td></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('qrReport_name_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{$value->name}}</td>
            @else
                <td></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('qrReport_directions_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{$value->directions}}</td>
            @else
                <td></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('qrReport_information_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{$value->information}}</td>
            @else
                <td></td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>
</body>
</html>
