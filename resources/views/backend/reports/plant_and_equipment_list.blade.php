@extends('backend.layouts.backend')
@section('title', 'Plant And Equipment')
@section('content')
    @php
        $data=localization();
    @endphp
    {{ Breadcrumbs::render('plant_and_equipment') }}
    <div class="card">
        <div class="card-body">
            @if(Auth()->user()->hasPermissionTo('csv_plantandequipmentReport') || Auth::user()->all_companies == 1 )
                <form class="form-style" id="export_excel_project" method="POST" action="{{ url('/report/plantAndEquipment/excel/export') }}">
                    {!! csrf_field() !!}
                    <input type="hidden" class="report_qr_export" name="excel_array" value="1">
                    <button  type="submit" class="form_submit_check btn btn-warning btn-sm">
                        <span class="assign_class label{{getKeyid('excel_export',$data)}}" data-id="{{getKeyid('excel_export',$data) }}" data-value="{{checkKey('excel_export',$data) }}" >
                          {!! checkKey('excel_export',$data) !!}
                        </span>
                    </button>
                </form>
            @endif
            @if(Auth()->user()->hasPermissionTo('word_plantandequipmentReport') || Auth::user()->all_companies == 1 )
                <form class="form-style" method="POST" id="export_world_project" action="{{ url('/report/plantAndEquipment/word/export') }}">
                    {!! csrf_field() !!}
                    <input type="hidden" class="project_export" name="word_array" value="1">
                    <button  type="submit" class="form_submit_check btn btn-success btn-sm">
                        <span class="assign_class label{{getKeyid('word_export',$data)}}" data-id="{{getKeyid('word_export',$data) }}" data-value="{{checkKey('word_export',$data) }}" >
                            {!! checkKey('word_export',$data) !!}
                        </span>
                    </button>
                </form>
            @endif
            @if(Auth()->user()->hasPermissionTo('pdf_plantandequipmentReport') || Auth::user()->all_companies == 1 )
                <form  class="form-style"  method="POST" {{pdf_view('location_breakdowns','','e')}} id="export_pdf_project" action="{{ url('/report/plantAndEquipment/pdf/export') }}">
                    {!! csrf_field() !!}
                    <input type="hidden" class="project_export" name="pdf_array" value="1">
                    <button  type="submit" class="form_submit_check btn btn-secondary btn-sm">
                        <span class="assign_class label{{getKeyid('pdf_export',$data)}}" data-id="{{getKeyid('pdf_export',$data) }}" data-value="{{checkKey('pdf_export',$data) }}" >
                            {!! checkKey('pdf_export',$data) !!}
                        </span>
                    </button>
                </form>
            @endif
            <input type="hidden" class="type" value="1">
            <div class="row">
                <!-- <div class="col-md-2 md-form active-cyan-2 mb-3">
                    <select searchable="Search here.." id="company_id" class="mdb-select custom_search"  >
                    @if(isset($companies))
                        @foreach($companies as $key => $company)
                            @if($key == 0)
                                <option value="{{$company->id}}" selected>
                                    {{$company->name}}
                                </option>
                            @else
                            <option value="{{$company->id}}">
                                {{$company->name}}
                            </option>
                            @endif
                        @endforeach
                    @endif
                    </select>
                </div> -->
                <div class="col-md-2 md-form active-cyan-2 mb-3">
                  <input class="form-control custom_search" id="location_id" type="text" placeholder="Project" aria-label="Project">
                </div>
                <div class="col-md-2 md-form active-cyan-2 mb-3">
                  <input class="form-control custom_search" id="name" type="text" placeholder="Location Break Down" aria-label="Location Break Down">
                </div>
                <!-- <div class="col-md-2 md-form active-cyan-2 mb-3">
                  <input class="form-control custom_search" id="user" type="text" placeholder="User" aria-label="User">
                </div> -->
                <!-- <div class="col-md-2 md-form active-cyan-2 mb-3">
                  <input class="form-control custom_search" id="form" type="text" placeholder="Form" aria-label="Form">
                </div> -->
                <div class="col-md-2 md-form active-cyan-2 mb-3">
                  <input class="form-control datepicker custom_search" id="next_service_date" type="text" placeholder="Date" aria-label="Date">
                </div>
                <div class="col-md-2 md-form active-cyan-2 mb-3">
                    <button  type="button" class="btn btn-secondary btn-sm search">
                        <span class="assign_class label{{getKeyid('filter',$data)}}" data-id="{{getKeyid('filter',$data) }}" data-value="{{checkKey('filter',$data) }}" >
                            {!! checkKey('filter',$data) !!}
                        </span>
                    </button>
                </div>
            </div>
            <div id="table-equipment" class=" table-editable table-responsive">
                <table id="plant_and_equipment_table" class=" table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>
                            <span class="assign_class label{{getKeyid('qr',$data)}}" data-id="{{getKeyid('qr',$data) }}" data-value="{{checkKey('qr',$data) }}" >
                                {!! checkKey('qr',$data) !!}
                            </span>
                        </th>
                        <th>
                            <span class="assign_class label{{getKeyid('project',$data)}}" data-id="{{getKeyid('project',$data) }}" data-value="{{checkKey('project',$data) }}" >
                                {!! checkKey('project',$data) !!}
                            </span>
                        </th>
                        <th>
                            <span class="assign_class label{{getKeyid('name',$data)}}" data-id="{{getKeyid('name',$data) }}" data-value="{{checkKey('name',$data) }}" >
                                {!! checkKey('name',$data) !!}
                            </span>
                        </th>
                        <th>
                            <span class="assign_class label{{getKeyid('equipment',$data)}}" data-id="{{getKeyid('equipment',$data) }}" data-value="{{checkKey('equipment',$data) }}" >
                                {!! checkKey('equipment',$data) !!}
                            </span>
                        </th>
                        <th>
                            <span class="assign_class label{{getKeyid('serial_number',$data)}}" data-id="{{getKeyid('serial_number',$data) }}" data-value="{{checkKey('serial_number',$data) }}" >
                                {!! checkKey('serial_number',$data) !!}
                            </span>
                        </th>
                        <th>
                            <span class="assign_class label{{getKeyid('make',$data)}}" data-id="{{getKeyid('make',$data) }}" data-value="{{checkKey('make',$data) }}" >
                                {!! checkKey('make',$data) !!}
                            </span>
                        </th>
                        <th>
                            <span class="assign_class label{{getKeyid('model',$data)}}" data-id="{{getKeyid('model',$data) }}" data-value="{{checkKey('model',$data) }}" >
                                {!! checkKey('model',$data) !!}
                            </span>
                        </th>
                        <th>
                            <span class="assign_class label{{getKeyid('last_service_date',$data)}}" data-id="{{getKeyid('last_service_date',$data) }}" data-value="{{checkKey('last_service_date',$data) }}" >
                                {!! checkKey('last_service_date',$data) !!}
                            </span>
                        </th>
                        <th>
                            <span class="assign_class label{{getKeyid('next_service_date',$data)}}" data-id="{{getKeyid('next_service_date',$data) }}" data-value="{{checkKey('next_service_date',$data) }}" >
                                {!! checkKey('next_service_date',$data) !!}
                            </span>
                        </th>
                        <th>
                            <span class="assign_class label{{getKeyid('condition',$data)}}" data-id="{{getKeyid('condition',$data) }}" data-value="{{checkKey('condition',$data) }}" >
                                {!! checkKey('condition',$data) !!}
                            </span>
                        </th>
                        </tr>
                    </thead>
                    <tbody id="equipment_data"></tbody>
                </table>
                @if(Auth()->user()->all_companies == 1)
                    <input type="hidden" id="plantandequipmentReport_checkbox" value="1">
                    <input type="hidden" id="plantandequipmentReport_qr" value="1">
                    <input type="hidden" id="plantandequipmentReport_project" value="1">
                    <input type="hidden" id="plantandequipmentReport_name" value="1">
                    <input type="hidden" id="plantandequipmentReport_equipment" value="1">
                    <input type="hidden" id="plantandequipmentReport_serialnumber" value="1">
                    <input type="hidden" id="plantandequipmentReport_make" value="1">
                    <input type="hidden" id="plantandequipmentReport_model" value="1">
                    <input type="hidden" id="plantandequipmentReport_lastservicedate" value="1">
                    <input type="hidden" id="plantandequipmentReport_nextservicedate" value="1">
                    <input type="hidden" id="plantandequipmentReport_condition" value="1">
                @else
                    <input type="hidden" id="plantandequipmentReport_checkbox" value="{{Auth()->user()->hasPermissionTo('plantandequipmentReport_checkbox')}}">
                    <input type="hidden" id="plantandequipmentReport_qr" value="{{Auth()->user()->hasPermissionTo('plantandequipmentReport_qr')}}">
                    <input type="hidden" id="plantandequipmentReport_project" value="{{Auth()->user()->hasPermissionTo('plantandequipmentReport_project')}}">
                    <input type="hidden" id="plantandequipmentReport_name" value="{{Auth()->user()->hasPermissionTo('plantandequipmentReport_name')}}">
                    <input type="hidden" id="plantandequipmentReport_equipment" value="{{Auth()->user()->hasPermissionTo('plantandequipmentReport_equipment')}}">
                    <input type="hidden" id="plantandequipmentReport_serialnumber" value="{{Auth()->user()->hasPermissionTo('plantandequipmentReport_serialnumber')}}">
                    <input type="hidden" id="plantandequipmentReport_make" value="{{Auth()->user()->hasPermissionTo('plantandequipmentReport_make')}}">
                    <input type="hidden" id="plantandequipmentReport_model" value="{{Auth()->user()->hasPermissionTo('plantandequipmentReport_model')}}">
                    <input type="hidden" id="plantandequipmentReport_lastservicedate" value="{{Auth()->user()->hasPermissionTo('plantandequipmentReport_lastservicedate')}}">
                    <input type="hidden" id="plantandequipmentReport_nextservicedate" value="{{Auth()->user()->hasPermissionTo('plantandequipmentReport_nextservicedate')}}">
                    <input type="hidden" id="plantandequipmentReport_condition" value="{{Auth()->user()->hasPermissionTo('plantandequipmentReport_condition')}}">
                @endif
            </div>
        </div>
    </div>
    <input type="hidden" class="get_current_path" value="{{Request::path()}}">
    @include('backend.label.input_label')
    <script type="text/javascript" src="{{ asset('custom/js/custom_search.js') }}" ></script>
    <script type="text/javascript">
        function checkSelected(value,checkValue)
        {
            if(value == checkValue)
            {
                return 'selected';
            }
            else
            {
                return "";
            }

        }
        function equipment_data(url,methodType,serach_array) {
            var table = $('#plant_and_equipment_table').dataTable({
                    processing: true,
                    language: {
                        'lengthMenu': '  _MENU_ ',
                        'search':'<span class="assign_class label'+$('#search_id').attr('data-id')+'" data-id="'+$('#search_id').attr('data-id')+'" data-value="'+$('#search_id').val()+'" >'+$('#search_id').val()+'</span>',
                        'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                        'info':'',
                        'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                        'paginate': {
                            'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                            'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                        },
                        'infoFiltered': "(filtered from _MAX_ total records)"
                    },
                    "ajax": {
                        "headers": {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        "url": url,
                        "method": methodType,
                        "data":  {'data': serach_array},
                    },
                    "createdRow": function( row, data, dataIndex,columns )
                    {
                        var last_date='';
                        if ($("#equipmentGroup_lastserviceDate_edit").val()=='1' && $('#edit_equipmentGroup').val() ){
                            var l_disable='';
                        }else{
                            var l_disable='disabled';
                        }
                        last_date+='<input '+l_disable+' placeholder="Selected date" value="'+data.last_service_date+'" type="date" id="last_service_date'+data.id+'"  class="form-control datepicker">';

                        if ($("#equipmentGroup_nextserviceDate_edit").val()=='1' && $('#edit_equipmentGroup').val() ){
                            var n_disable='';
                        }else{
                            var n_disable='disabled';
                        }
                        var next_date='';
                        next_date+='<input '+n_disable+' placeholder="Selected date" value="'+data.next_service_date+'" type="date" id="next_service_date'+data.id+'"  class="form-control datepicker">';
                        var qr='';
                        qr+= '<img src="{{asset("/qr")}}/'+data.name+'.png"  width="50" height="50">';

                        $(columns[0]).html(qr);
                        // $(columns[7]).html(last_date);
                        // $(columns[8]).html(next_date);
                        $(row).attr('data-id',data['id']);

                    },
                    columns:[
                        {data: 'qr', name: 'qr',visible:$('#plantandequipmentReport_qr').val()},
                        {data: 'project', name: 'project',visible:$('#plantandequipmentReport_project').val()},
                        {data: 'name', name: 'name',visible:$('#plantandequipmentReport_name').val()},
                        {data: 'equipment', name: 'equipment',visible:$('#plantandequipmentReport_equipment').val()},
                        {data: 'serial_number', name: 'serial_number',visible:$('#plantandequipmentReport_serialnumber').val()},
                        {data: 'make', name: 'make',visible:$('#plantandequipmentReport_make').val()},
                        {data: 'model', name: 'model',visible:$('#plantandequipmentReport_model').val()},
                        {data: 'last_service_date', name: 'last_service_date',visible:$('#plantandequipmentReport_lastservicedate').val()},
                        {data: 'next_service_date', name: 'next_service_date',visible:$('#plantandequipmentReport_nextservicedate').val()},
                        {data: 'condition', name: 'condition',visible:$('#plantandequipmentReport_condition').val()},
                    ],
                    "columnDefs": [ {
                        "targets": [0,1],
                        "orderable": false
                    } ]
                }
            );
            (function ($) {
                var active ='<span class="assign_class label'+$('#breadcrumb_equipmentGroup').attr('data-id')+'" data-id="'+$('#breadcrumb_equipmentGroup').attr('data-id')+'" data-value="'+$('#breadcrumb_equipmentGroup').val()+'" >'+$('#breadcrumb_equipmentGroup').val()+'</span>';
                $('.breadcrumb-item.active').html(active);
                var parent ='<span class="assign_class label'+$('#dashboard_label').attr('data-id')+'" data-id="'+$('#dashboard_label').attr('data-id')+'" data-value="'+$('#dashboard_label').val()+'" >'+$('#dashboard_label').val()+'</span>';
                $('.breadcrumb-item a').html(parent);
            }(jQuery));

        }

        $(document).ready(function () {
            var current_path=$('.get_current_path').val();
            localStorage.setItem('current_path',current_path);
            var url='/report/getAllplantAndEquipmentList';

            equipment_data(url,'get','');
            $("body").on('click','.edit_inlinepe',function () {
                $(".equipment_show_button_"+$(this).attr('data-id')).show();
            });
            $("body").on('click','.equipment_group_refresh',function () {
                var url='/plant_and_equipment/getall';
                equipment_data(url);
            });

            $("body").on("focusin",".edit_inline_client",function(){
                $("#client_tr"+$(this).attr('data-id')).addClass('selected');
            });
        });
        $('body').on('change', '.datepicker', function() {
            var id=$(this).attr('id');
            var value=$(this).val();
            if(value)
            {
                $('#'+id).val(value);
            }
        });
    </script>
@endsection
