@extends('backend.layouts.backend')
@section('title', 'Complete Forms')
@section('content')
    @php
       $data=localization();
    @endphp
    <link rel="stylesheet" href="{{ asset('custom/css/custom_style.css') }}">
    <div class="card">
        <div class="card-body">
            <div id="table" class="table-editable">
                <button id="un_signed" class="btn btn-default btn-sm" onclick="filterReport('all')">
                    All
                </button>
                <button id="un_signed" class="btn btn-default btn-sm" onclick="filterReport('not_required')">
                    Not Required
                </button>
                <button id="signed" class="btn btn-primary btn-sm" onclick="signedReport()">
                    <span class="assign_class label{{getKeyid('show_signed_forms',$data)}}" data-id="{{getKeyid('show_signed_forms',$data) }}" data-value="{{checkKey('show_signed_forms',$data) }}" >
                        {!! checkKey('show_signed_forms',$data) !!}
                    </span>
                </button>
                <button id="un_signed" class="btn btn-success btn-sm" onclick="filterReport('approved')">
                    Approved
                </button>
                <button id="un_signed" class="btn btn-danger btn-sm" onclick="filterReport('rejected')">
                    Rejected
                </button>
                <button id="un_signed" class="btn btn-warning btn-sm" onclick="unSignedReport()">
                    <span class="assign_class label{{getKeyid('show_unsigned_forms',$data)}}" data-id="{{getKeyid('show_unsigned_forms',$data) }}" data-value="{{checkKey('show_unsigned_forms',$data) }}" >
                        {!! checkKey('show_unsigned_forms',$data) !!}
                    </span>
                </button>

                <div class="row">
                    <div class="col-md-2 md-form active-cyan-2 mb-3">
                        <input class="form-control custom_search" id="form_name" type="text" placeholder="Form Name" aria-label="Form Name">
                    </div>
                    <div class="col-md-2 md-form active-cyan-2 mb-3">
                        <input class="form-control custom_search" id="location_name" type="text" placeholder="Location" aria-label="Location">
                    </div>
                    <div class="col-md-2 md-form active-cyan-2 mb-3">
                        <input class="form-control custom_search" id="location_break_down" type="text" placeholder="Location Break Down" aria-label="Location Break Down">
                    </div>
                    <div class="col-md-2 md-form active-cyan-2 mb-3">
                        <input class="form-control  custom_search" id="user_name" type="text" placeholder="User" aria-label="User">
                    </div>
                    <div class="col-md-2 md-form active-cyan-2 mb-3">
                        <input class="form-control datepicker custom_search" id="date_time" type="text" placeholder="Date" aria-label="Date">
                    </div>
                    <div class="col-md-2 md-form active-cyan-2 mb-3">
                        <button  type="button" class="btn btn-secondary btn-sm search">
                        <span class="assign_class label{{getKeyid('filter',$data)}}" data-id="{{getKeyid('filter',$data) }}" data-value="{{checkKey('filter',$data) }}" >
                            {!! checkKey('filter',$data) !!}
                        </span>
                        </button>
                    </div>
                </div>
                <table id="complete_table"  class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th id="action" class="no-sort all_checkboxes_style"></th>
                            <th> <span class="assign_class label{{getKeyid('form_name',$data)}}" data-id="{{getKeyid('form_name',$data) }}" data-value="{{checkKey('form_name',$data) }}" >
                            {!! checkKey('form_name',$data) !!}
                            </span>
                            </th>
                            <th> <span class="assign_class label{{getKeyid('location',$data)}}" data-id="{{getKeyid('location',$data) }}" data-value="{{checkKey('location',$data) }}" >
                            {!! checkKey('location',$data) !!}
                            </span>
                            </th>
                            <th> <span class="assign_class label{{getKeyid('location_break_down',$data)}}" data-id="{{getKeyid('location_break_down',$data) }}" data-value="{{checkKey('location_break_down',$data) }}" >
                            {!! checkKey('location_break_down',$data) !!}
                            </span>
                            </th>
                            <th> <span class="assign_class label{{getKeyid('user_name',$data)}}" data-id="{{getKeyid('user_name',$data) }}" data-value="{{checkKey('user_name',$data) }}" >
                            {!! checkKey('user_name',$data) !!}
                            </span>
                            </th>
                            <th> <span class="assign_class label{{getKeyid('date_time',$data)}}" data-id="{{getKeyid('date_time',$data) }}" data-value="{{checkKey('date_time',$data) }}" >
                            {!! checkKey('date_time',$data) !!}
                            </span>
                            </th>
                            <th> <span class="assign_class label{{getKeyid('score',$data)}}" data-id="{{getKeyid('score',$data) }}" data-value="{{checkKey('score',$data) }}" >
                            {!! checkKey('score',$data) !!}
                            </span>
                            </th>
                            <th>status</th>
                            <th class="no-sort all_action_btn"></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    @if(Auth()->user()->all_companies == 1)
        <input type="hidden" id="checkbox_permission" value="1">
        <input type="hidden" id="form_name_permission" value="1">
        <input type="hidden" id="location_permission" value="1">
        <input type="hidden" id="locatin_break_permission" value="1">
        <input type="hidden" id="user_permission" value="1">
        <input type="hidden" id="date_time_permission" value="1">
        <input type="hidden" id="answer_score_permission" value="1">
        <input type="hidden" id="form_type_score_permission" value="1">
        <input type="hidden" id="form_score_permission" value="1">
    @else
        <input type="hidden" id="checkbox_permission" value="{{Auth()->user()->hasPermissionTo('reportCompletedForm_checkbox')}}">
        <input type="hidden" id="form_name_permission" value="{{Auth()->user()->hasPermissionTo('reportCompletedForm_form_name')}}">
        <input type="hidden" id="location_permission" value="{{Auth()->user()->hasPermissionTo('reportCompletedForm_location')}}">
        <input type="hidden" id="locatin_break_permission" value="{{Auth()->user()->hasPermissionTo('reportCompletedForm_location_break_down')}}">
        <input type="hidden" id="user_permission" value="{{Auth()->user()->hasPermissionTo('reportCompletedForm_user')}}">
        <input type="hidden" id="date_time_permission" value="{{Auth()->user()->hasPermissionTo('reportCompletedForm_form_score')}}">
         <input type="hidden" id="form_score_permission" value="{{Auth()->user()->hasPermissionTo('reportCompletedForm_answer_score')}}">
        <input type="hidden" id="answer_score_permission" value="{{Auth()->user()->hasPermissionTo('reportCompletedForm_form_type')}}">
        <input type="hidden" id="form_type_score_permission" value="{{Auth()->user()->hasPermissionTo('reportCompletedForm_form_type')}}">
    @endif
    <input type="hidden" class="get_current_path" value="{{Request::path()}}">
    @include('backend.complete_form.model.sign_off')
    @include('backend.label.input_label')
    <script src="{{asset('/custom/js/complete_form.js')}}"></script>
    <script type="text/javascript" src="{{ asset('custom/js/complete_form_custom_search.js') }}" ></script>
    <script type="text/javascript">
          window.onload = function() {
        document.getElementById("action").classList.remove('sorting_asc');
    };
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            var current_path=$('.get_current_path').val();
            localStorage.setItem('current_path',current_path);
        });
        function checkSelected(value,checkValue)
        {
          if(value == checkValue)
          {
            return 'selected';
          }
          else
          {
            return "";
          }

        }
        function complete_data(url,methodType,serach_array) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: methodType,
                url: url,
                success: function(response)
                {
                  var table = $('#complete_table').dataTable(
                  {
                    processing: true,
                    language: {
                    'lengthMenu': '_MENU_',
                    'info': ' ',
                    'search':'<span class="assign_class label'+$('#search_id').attr('data-id')+'" data-id="'+$('#search_id').attr('data-id')+'" data-value="'+$('#search_id').val()+'" >'+$('#search_id').val()+'</span>',
                    },
                      "headers": {
                          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                      },
                    "ajax": {
                        "url": url,
                        "type": methodType,
                        "data":  {'data': serach_array},

                    },
                    "createdRow": function( row, data,dataIndex,columns )
                    {

                        var checkbox_permission=$('#checkbox_permission').val();
                        var checkbox='';
                        checkbox+='<div class="form-check"><input type="checkbox" class="form-check-input" id="job'+data.id+'"><label class="form-check-label" for="job'+data.id+'""></label></div>';

                        $(columns[0]).html(checkbox);

                        $(row).attr('id', 'job_tr'+data['id']);
                        $(row).attr('class', 'selectedrowjob');
                         if(data.sign_off==1)
                            {
                            $(row).attr('style',"background-color: #00c851");
                            }
                            else
                            {
                            $(row).attr('style',"background-color: #fc685f");

                            }
                        var temp=data['id'];

                    },

                    columns:
                    [
                        {data:'checkbox', name:'checkbox',visible:$('#checkbox_permission').val()},
                        {data:'form_name',name:'form_name',visible:$('#form_name_permission').val()},
                        {data:'location_name',name:'location_name',visible:$('#location_permission').val()},
                        {data:'location_breakdown',name:'location_breakdown',visible:$('#location_break_permission').val()},
                        {data:'employee_name', name:'employee_name',visible:$('#user_permission').val()},
                        {data:'date_time',name:'date_time',visible:$('#date_time_permission').val()},
                        {data:'score',name:'score',visible:$('#answer_score_permission').val()},
                        {data:'status', name:'status'},
                        {data:'actions', name:'actions'},

                    ]
                  }
                  );
                },
                error: function (error) {
                  console.log(error);
                }
            });
        }
        var id = @json($id);
        if(id==0)
        {
            var url='/completed_forms/getAllReport';
        }
        else
        {
            var url='/completed_forms/getAllReport/'+id;
        }
        complete_data(url,'get','');
        @if(Session::has('message'))
            var type = "{{ Session::get('alert-type', 'info') }}";
            switch(type){
                case 'info':
                    toastr.info("{{ Session::get('message') }}");
                    break;

                case 'warning':
                    toastr.warning("{{ Session::get('message') }}");
                    break;

                case 'success':
                    toastr.success("{{ Session::get('message') }}");
                    break;

                case 'error':
                    toastr.error("{{ Session::get('message') }}");
                    break;
            }
        @endif
    </script>
@endsection
