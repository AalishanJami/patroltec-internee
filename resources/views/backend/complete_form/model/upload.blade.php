<div class="modal fade" id="answer_upload{{$question->id}}" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h1>{{$question->question}}</h1>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
                 <!--    <div id="pdf-viewer-static{{$question->id}}"></div> -->
                  @if($response_groups[$index]->responses[$key]->file_name ?? false)
                 <img src="{{asset('/uploads/response').'/'.($response_groups[$index]->responses[$key]->file_name ?? false)}}"  width="500" height="350" class="img_pop">
                 @endif
        </div>
        <div class="modal-footer">
      
        </div>
      </div>
      
    </div>
<script src="{{asset('js/pdfview.js') }}"></script>
@if($response_groups[$index]->responses[$key]->file_name ?? false)
<script type="text/javascript">
var options = {
    pdfOpenParams: {toolbar: '0'}
};

PDFObject.embed("{{ url('completed_forms/view-pdf', ['id' =>$response_groups[$index]->responses[$key]->file_name]) }}", "#pdf-viewer-static{{$question->id}}",options);
</script>
@endif
  </div>
