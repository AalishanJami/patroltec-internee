<div class="modal fade" id="modelSignOff" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
           <span class="assign_class label{{getKeyid('sign_off_header',$data)}}" data-id="{{getKeyid('sign_off_header',$data) }}" data-value="{{checkKey('sign_off_header',$data) }}" >
                        {!! checkKey('sign_off_header',$data) !!}
                    </span>
                @if(Auth()->user()->hasPermissionTo('view_cms') || Auth::user()->all_companies == 1 )
                    <button data-class="edit-disable" class="edit_cms_enable edit_cms_switch btn btn-primary btn-sm waves-effect waves-light" style="display: none;" >
                        Edit cms
                    </button>
                    <button  data-class="edit-enable" class="edit_cms_disable edit_cms_switch btn btn-danger btn-sm waves-effect waves-light">
                        Edit cms
                    </button>
                @endif

            </div>

            <form method="POST" action="{{url('completed_forms/signOff')}}" onsubmit = "return(validate());">
                @csrf
                <div class="modal-body">
                    <input type="hidden" name="job_group_id" id="job_group_id">
                    <input type="hidden" name="location_id" id="location_id">
                    <input type="hidden" name="location_breakdown_id" id="location_breakdown_id">
                    <div class="md-form">

                        <textarea id="comment" name="comment" class="md-textarea form-control"></textarea>
                        <label for="comment">   <span class="assign_class label{{getKeyid('comment',$data)}}" data-id="{{getKeyid('comment',$data) }}" data-value="{{checkKey('comment',$data) }}" >
                        {!! checkKey('comment',$data) !!}
                    </span></label>
                        <small id="er_comment" class="text-danger error_message"></small>

                    </div>
                    <label>   <span class="assign_class label{{getKeyid('report_label',$data)}}" data-id="{{getKeyid('report_label',$data) }}" data-value="{{checkKey('report_label',$data) }}" >
                        {!! checkKey('report_label',$data) !!}
                    </span></label>
                    <br>
                    <!-- Group of material radios - option 1 -->
                    <div class="form-check form-check-inline">
                        <input type="radio" class="form-check-input" id="yes" value="1" name="toggleDiv">
                        <label class="form-check-label" for="yes"><span class="assign_class label{{getKeyid('report_label_yes',$data)}}" data-id="{{getKeyid('report_label_yes',$data) }}" data-value="{{checkKey('report_label_yes',$data) }}" >
                        {!! checkKey('report_label_yes',$data) !!}
                    </span></label>
                    </div>

                    <!-- Group of material radios - option 2 -->
                    <div class="form-check form-check-inline">
                        <input type="radio" class="form-check-input" id="no" checked  value="0" name="toggleDiv">
                        <label class="form-check-label" for="no"><span class="assign_class label{{getKeyid('report_label_no',$data)}}" data-id="{{getKeyid('report_label_no',$data) }}" data-value="{{checkKey('report_label_no',$data) }}" >
                        {!! checkKey('report_label_no',$data) !!}
                    </span></label>
                    </div>

                    <div id='off_div'>
                        <div class="md-form">
                            {!! Form::select('user_id',$users,0, ['class' =>' mdb-select select_user_id','id'=>'user_id','searchable'=>'search here','placeholder'=>'Users']) !!}
                            <small id="er_user" style="left: 0%!important;margin-top:-15px;display:block; text-transform: initial !important;" class="text-danger error_message"></small>
                        </div>
                        <div class="md-form">
                            {!! Form::select('form_type_id',$types, null, ['class' =>'mdb-select','placeholder'=>'Form Type','id'=>'form_type','searchable'=>'search here']) !!}
                            <small id="er_form_type" style="left: 0%!important;margin-top:-15px;display:block; text-transform: initial !important;" class="text-danger error_message"></small>
                        </div>
                        <div class="md-form" id="form_div">
                            {!! Form::select('form_id',[],null, ['class' =>' mdb-select ','id'=>'form_id','searchable'=>'search here','placeholder'=>'Forms']) !!}
                        </div>
                        <small id="er_form" style="left: 0%!important;margin-top:-15px;display:block; text-transform: initial !important;" class="text-danger error_message"></small>
                        <div class="md-form">
                            <input placeholder="Select date" type="text" id="date_pick" name="date" class="form-control datepicker" >
                            <small id="er_date" class="text-danger error_message"></small>

                        </div>

                        <div class="md-form">
                            <input placeholder="Selected time" type="text" id="input_starttime" name="time" class="form-control timepicker">
                            <small id="er_time" class="text-danger error_message"></small>

                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">SignOff</button>
                </div>
            </form>
        </div>

    </div>
</div>
<script type="text/javascript">
    $(document).ready(function (){
        setTimeout(function(){
            $(".select_user_id option:first").val('');
            $(".select_user_id option:first").attr('selected',true);
            $(".select_user_id option:first").attr('disabled',true);
        },1000);
    });
    $('.datepicker').pickadate({selectYears:250,});
    $('.timepicker').pickatime();
    $("input[name='toggleDiv']").change(function(){
        console.log("asdasdadad");
        console.log($("input[name='toggleDiv']:checked").val());
        if($("input[name='toggleDiv']:checked").val()==0)
        {
            $("#off_div").show();
        }
        else
        {
            $("#off_div").hide();
        }

    });

    function validate()
    {
        var user_id=$('#user_id').val();
        var form_id=$('#form_id').val();
        var form_type_id=$('#form_type').val();
        var date = $('#date_pick').val();
        var comment = $('#comment').val();
        var time = $('#input_starttime').val();
        var fl = true;

        if(comment=='' || comment==null )
        {
            console.log(comment,"comment");

            $("#comment").attr('style','border-bottom:1px solid #e3342f');
            $("#comment").focus();
            $("#er_comment").html('Field required');
            fl= false;
        }

        if($("input[name='toggleDiv']:checked").val()==0)
        {
            if(user_id=='' || user_id==null )
            {
                console.log(user_id,"user");

                $("[data-activates=select-options-user_id]").attr('style','border-bottom:1px solid #e3342f');
                $("#user_id").focus();
                $("#er_user").html('Field required');
                fl= false;
            }

            if(form_id=='' || form_id==null )
            {
                console.log(user_id,"user");

                $("[data-activates=select-options-form_id]").attr('style','border-bottom:1px solid #e3342f');
                $("#form_id").focus();
                $("#er_form").html('Field required');
                fl= false;
            }
            if(form_type_id=='' || form_type_id==null )
            {
                console.log(user_id,"user");

                $("[data-activates=select-options-form_type]").attr('style','border-bottom:1px solid #e3342f');
                $("#form_type_id").focus();
                $("#er_form_type").html('Field required');
                fl= false;
            }

            if(date=='' || date==null )
            {
                console.log(date,"date");

                $("#date_pick").attr('style','border-bottom:1px solid #e3342f');
                $("#date_pick").focus();
                $("#er_date").html('Field required');
                fl= false;
            }
            if(time=='' || time==null )
            {
                console.log(time,"time");

                $("#input_starttime").attr('style','border-bottom:1px solid #e3342f');
                $("#input_starttime").focus();
                $("#er_time").html('Field required');
                fl= false;
            }

        }
        console.log(fl);
        return fl;
    }

</script>
