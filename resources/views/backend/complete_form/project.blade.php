@extends('backend.layouts.detail')
@section('title', 'Complete Forms')
@section('content')
    @php
        $data=localization();
    @endphp
    <link rel="stylesheet" href="{{ asset('custom/css/custom_style.css') }}">
    @include('backend.layouts.detail_sidebar')
    <div class="padding-left-custom remove-padding">
        {!! breadcrumInner('completed_forms',Session::get('project_id'),'locations','/project/detail/'.Session::get('project_id') ,'project','/project') !!}
        <div class="locations-form container-fluid form_body">
            <div class="card">
                <div class="card-body">
                    <div id="table" class="table-editable table-responsive">
                        <button id="un_signed" class="btn btn-default btn-sm" onclick="filterProject('all')">
                            All
                        </button>
                        <button id="un_signed" class="btn btn-default btn-sm" onclick="filterProject('not_required')">
                            Not Required
                        </button>
                        <button id="signed" class="btn btn-primary btn-sm" onclick="signedProject()">
                            <span class="assign_class label{{getKeyid('show_signed_forms',$data)}}" data-id="{{getKeyid('show_signed_forms',$data) }}" data-value=" {{checkKey('show_signed_forms',$data) }}" >
                                {!! checkKey('show_signed_forms',$data) !!}
                            </span>
                        </button>
                        <button id="un_signed" class="btn btn-success btn-sm" onclick="filterProject('approved')">
                            Approved
                        </button>
                        <button id="un_signed" class="btn btn-danger btn-sm" onclick="filterProject('rejected')">
                            Rejected
                        </button>
                        <button id="un_signed" class="btn btn-warning btn-sm" onclick="unSignedProject()">
                            <span class="assign_class label{{getKeyid('show_unsigned_forms',$data)}}" data-id="{{getKeyid('show_unsigned_forms',$data) }}" data-value="{{checkKey('show_unsigned_forms',$data) }}" >
                                {!! checkKey('show_unsigned_forms',$data) !!}
                            </span>
                        </button>
                        <table id="complete_table"  class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th id="action" class="no-sort all_checkboxes_style"></th>

                                    <th> <span class="assign_class label{{getKeyid('form_name',$data)}}" data-id="{{getKeyid('form_name',$data) }}" data-value="{{checkKey('form_name',$data) }}" >
                                            {!! checkKey('form_name',$data) !!}
                                        </span>
                                    </th>
                                    <th>
                                        <span class="assign_class label{{getKeyid('location',$data)}}" data-id="{{getKeyid('location',$data) }}" data-value="{{checkKey('location',$data) }}" >
                                            {!! checkKey('location',$data) !!}
                                        </span>
                                    </th>
                                    <th>
                                        <span class="assign_class label{{getKeyid('location_break_down',$data)}}" data-id="{{getKeyid('location_break_down',$data) }}" data-value="{{checkKey('location_break_down',$data) }}" >
                                            {!! checkKey('location_break_down',$data) !!}
                                        </span>
                                    </th>
                                    <th>
                                        <span class="assign_class label{{getKeyid('user_name',$data)}}" data-id="{{getKeyid('user_name',$data) }}" data-value="{{checkKey('user_name',$data) }}" >
                                            {!! checkKey('user_name',$data) !!}
                                        </span>
                                    </th>
                                    <th>
                                        <span class="assign_class label{{getKeyid('date_time',$data)}}" data-id="{{getKeyid('date_time',$data) }}" data-value="{{checkKey('date_time',$data) }}" >
                                            {!! checkKey('date_time',$data) !!}
                                        </span>
                                    </th>
                                    <th>
                                       <span class="assign_class label{{getKeyid('score',$data)}}" data-id="{{getKeyid('score',$data) }}" data-value="{{checkKey('score',$data) }}" >
                            {!! checkKey('score',$data) !!}
                            </span>
                                    </th>
                                    <th>
                                        <span class="assign_class label{{getKeyid('form_type_score',$data)}}" data-id="{{getKeyid('form_type_score',$data) }}" data-value="{{checkKey('form_type_score',$data) }}" >
                                            {!! checkKey('form_type_score',$data) !!}
                                        </span>
                                    </th>
                                    <th>
                                        <span class="assign_class label{{getKeyid('form_score',$data)}}" data-id="{{getKeyid('form_score',$data) }}" data-value="{{checkKey('form_score',$data) }}" >
                                            {!! checkKey('form_score',$data) !!}
                                        </span>
                                    </th>

                                    <th>total score</th>
                                    <th>status</th>
                                    <th class="no-sort all_action_btn"></th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(Auth()->user()->all_companies == 1)
        <input type="hidden" id="checkbox_permission" value="1">
        <input type="hidden" id="form_name_permission" value="1">
        <input type="hidden" id="location_permission" value="1">
        <input type="hidden" id="locatin_break_permission" value="1">
        <input type="hidden" id="user_permission" value="1">
        <input type="hidden" id="date_time_permission" value="1">
        <input type="hidden" id="answer_score_permission" value="1">
        <input type="hidden" id="form_type_score_permission" value="1">
        <input type="hidden" id="form_score_permission" value="1">
    @else
        <input type="hidden" id="checkbox_permission" value="{{Auth()->user()->hasPermissionTo('projectCompletedForm_checkbox')}}">
        <input type="hidden" id="form_name_permission" value="{{Auth()->user()->hasPermissionTo('projectCompletedForm_form_name')}}">
        <input type="hidden" id="location_permission" value="{{Auth()->user()->hasPermissionTo('projectCompletedForm_location')}}">
        <input type="hidden" id="locatin_break_permission" value="{{Auth()->user()->hasPermissionTo('projectCompletedForm_location_break_down')}}">
        <input type="hidden" id="user_permission" value="{{Auth()->user()->hasPermissionTo('projectCompletedForm_user')}}">
        <input type="hidden" id="date_time_permission" value="{{Auth()->user()->hasPermissionTo('projectCompletedForm_form_score')}}">
        <input type="hidden" id="form_score_permission" value="{{Auth()->user()->hasPermissionTo('projectCompletedForm_answer_score')}}">
        <input type="hidden" id="answer_score_permission" value="{{Auth()->user()->hasPermissionTo('projectCompletedForm_form_type')}}">
        <input type="hidden" id="form_type_score_permission" value="{{Auth()->user()->hasPermissionTo('projectCompletedForm_form_type')}}">
    @endif
    @include('backend.complete_form.model.sign_off')
    @include('backend.label.input_label')
    <script type="text/javascript">
        window.onload = function() {
            document.getElementById("action").classList.remove('sorting_asc');
        };
    </script>
    <script type="text/javascript">
        function checkSelected(value,checkValue)
        {
            if(value == checkValue)
            {
                return 'selected';
            }
            else
            {
                return "";
            }

        }
        function complete_data(url) {
            $.ajax({
                type: 'GET',
                url: url,
                success: function(response)
                {
                    var table = $('#complete_table').dataTable(
                        {
                            processing: true,
                            language: {
                                'lengthMenu': '_MENU_',
                                'info': ' ',
                                'search':'<span class="assign_class label'+$('#search_id').attr('data-id')+'" data-id="'+$('#search_id').attr('data-id')+'" data-value="'+$('#search_id').val()+'" >'+$('#search_id').val()+'</span>',
                            },
                            "ajax": {
                                "url": url,
                                "type": 'get',
                            },
                            "createdRow": function( row, data,dataIndex,columns )
                            {

                                var total =0;
                                total+=data['score']+data['form_type_score']+data['form_score'];
                                var checkbox_permission=$('#checkbox_permission').val();
                                var checkbox='';
                                checkbox+='<div class="form-check"><input type="checkbox" class="form-check-input" id="job'+data['id']+'"><label class="form-check-label" for="job'+data['id']+'""></label></div>';

                                $(columns[0]).html(checkbox);
                                $(columns[9]).html(total);

                                $(row).attr('id', 'job_tr'+data['id']);
                                $(row).attr('class', 'selectedrowjob');
                                if(data.sign_off==1)
                                {
                                    $(row).attr('style',"background-color: #00c851");
                                }
                                else
                                {
                                    $(row).attr('style',"background-color: #fc685f");

                                }
                                var temp=data['id'];

                            },

                            columns:
                                [
                                    {data:'checkbox', name:'checkbox',visible:$('#checkbox_permission').val()},
                                    {data:'form_name',name:'form_name',visible:$('#form_name_permission').val()},
                                    {data:'location_name',name:'location_name',visible:$('#location_permission').val()},
                                    {data:'location_breakdown',name:'location_breakdown',visible:$('#location_break_permission').val()},
                                    {data:'employee_name', name:'employee_name',visible:$('#user_permission').val()},
                                    {data:'date_time',name:'date_time',visible:$('#date_time_permission').val()},
                                    {data:'score',name:'score',visible:$('#answer_score_permission').val()},
                                    {data:'form_type_score',name:'form_type_score',visible:$('#form_type_permission').val()},
                                    {data:'form_score',name:'form_score',visible:$('#form_score_permission').val()},
                                    {data:'total_score', name:'total_score'},
                                    {data:'status', name:'status'},
                                    {data:'actions', name:'actions'},
                                ],

                        }
                    );
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
        var id = @json($id);
        if(id==0)
        {
            var url='/completed_forms/getAllProject';
        }
        else
        {
            var url='/completed_forms/getAllProject/'+id;
        }
        complete_data(url);
    </script>
    <script type="text/javascript">
        @if(Session::has('message'))
            var type = "{{ Session::get('alert-type', 'info') }}";
            switch(type){
                case 'info':
                    toastr.info("{{ Session::get('message') }}");
                    break;

                case 'warning':
                    toastr.warning("{{ Session::get('message') }}");
                    break;

                case 'success':
                    toastr.success("{{ Session::get('message') }}");
                    break;

                case 'error':
                    toastr.error("{{ Session::get('message') }}");
                    break;
            }
        @endif
    </script>
    <script src="{{asset('/custom/js/complete_form.js')}}"></script>
@endsection
