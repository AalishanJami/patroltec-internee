@extends('backend.layouts.backend')
@section('content')
@php
$data=localization();
@endphp
<style type="text/css">
  .white-color
  {
    color:#fff;
  }
  #pdf-viewer,#pdf-viewer-static
  {
    height: 700px;
  }
</style>
<div class="custom_clear locations-form container-fluid form_body">
  <div class="custom_clear card">
  <ul  class="custom_clear nav nav-tabs" id="myTab" role="tablist">
      <li class="custom_clear nav-item">
          <a class="custom_clear nav-link active" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile"
        aria-selected="false"> Dynamic</a>
      </li>
      @if($id_static)
      <li class="custom_clear nav-item">
          <a class="custom_clear nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact"
        aria-selected="false">Static</a>
      </li>
      @endif
  </ul>
  <div class="custom_clear tab-content" id="myTabContent">
    <div class="custom_clear tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
      <div class="card">
         <h5 class="card-header t py-4 white-color">
           <div class="row">
            <div class="col">
                <strong>
                  Form Name
                </strong>
            </div>
            <div class="col">
                <strong>
                  Person Name
                </strong>
            </div>
             <div class="col">
                <strong>
                  Location  Name
                </strong>
            </div>
            <div class="col">
                <strong>
                 Location BreakDown
                </strong>
            </div>
              <div class="col">
                <strong>
                 Date and Time
                </strong>
            </div>
             <div class="col">
                <strong>
                 Score
                </strong>
            </div>
          </div>
         <div class="row">
            <div class="col text-warning">
                <strong>
                 {{$job_group->forms->name ?? ""}}
                </strong>
            </div>
            <div class="col text-warning">
                <strong>
                 {{$job_group->users->name  ?? ""}}
                </strong>
            </div>
             <div class="col text-warning">
                <strong>
                 {{$job_group->locations->name  ?? ""}}
                </strong>
            </div>
            <div class="col text-warning">
                <strong>
                 {{$job_group->locationBreakdowns->name  ?? ""}}
                </strong>
            </div>
              <div class="col text-warning">
                <strong>
                {{Carbon\Carbon::parse($jobs[0]->date_time)->format('jS M Y H:i')}}
                </strong>
            </div>
            <div class="col text-warning">
                <strong>
                  @if($response_groups->sum('total_score')!=0)
               {{round($response_groups->sum('score')/$response_groups->sum('total_score')*100)}}%({{$response_groups->sum('score')}}/{{$response_groups->sum('total_score')}})
               @endif
                </strong>
            </div>
          </div>
         </h5>
             @foreach($jobs as $index => $job)

          <div id="{{$job->form_Section->order}}-{{$job->form_Section->id}}" class="custom_clear  sortable-card col-md-{{$job->form_Section->width}}">
            <div class="custom_clear mb-4 custom_dynamic_height">
                <br>
              <div class="custom_clear card text-center ">
                <div class="custom_clear card-header text-muted text-left md-5">
                  <span class="custom_clear white-color" style="flex: 1; text-align:left;padding-bottom: 40px">{{$job->form_Section->header}}</span><span class=" white-color float-right">
                    @if(isset($response_groups[$index]))
                    Score: {{$response_groups[$index]->score}}/{{$response_groups[$index]->total_score}}
                    @endif
                  </span>
                </div>
              <form id="jobForm" enctype="multipart/form-data">



                <div class="custom_clear card-body mt-2">
                  @foreach($job->form_Section->questionsS as $key=> $question)

                    @include('backend.complete_form.model.upload')
                    @if(isset($response_groups[$index]))
                    @if($response_groups[$index]->responses[$key]->fail ?? false)
                    <div class="custom_clear card card-cascade wider reverse" style="padding-top: 10px;padding-bottom: 10px; border-color: coral; border-width: 3px;">
                      @elseif($response_groups[$index]->responses[$key]->pass ?? false)
                            <div class="custom_clear card card-cascade wider reverse" style="padding-top: 10px;padding-bottom: 10px; border-color: #00c851;  border-width: 3px;">
                      @endif
                       @else
                            <div class="custom_clear card card-cascade wider reverse" style="padding-top: 10px;padding-bottom: 10px;">
                        @endif


                        <p class="custom_clear card-title text-left" style="padding-left: 20px;">
                          <strong>{!!($question->question)!!}:</strong>
                          @if(false)
                          <strong class="text-primary">({{$response_groups[$index]->responses[$key]->score ?? 0}})</strong>
                          @endif
                           @if($question->answer_type->name!='comment')
                          @if($response_groups[$index]->responses[$key]->file_name ?? false)
                          <button type="button" class="btn btn-success float-right" data-toggle="modal" data-target="#answer_upload{{$question->id}}"><i class="fa fa-check"></i></button>
                          @else
                            <button type="button" class="btn btn-danger float-right" data-toggle="modal" data-target="#answer_upload{{$question->id}}"><i class="fas fa-times"></i></button>

                            @endif
                            @endif

                        </p>
                        <div class="custom_clear row text-center">
                          @switch ($question->answer_type->name)
                            @case ('dropdown')

                                <div class="custom_clear col-md-12">
                                  <select searchable="Search here.." style="width: 100%;"  class="custom_clear mdb-select form_alignment" name="dropdown.{{$job->form_Section->id.'.'.$question->id}}">
                                    @if($question->answer_group)
                                      @foreach($question->answer_group->answer as $answer)
                                          @if($answer->id==($response_groups[$index]->responses[$key]->answer_id ?? false))
                                          <option id="{{$job->form_Section->id.'.'.$question->id.'.'.$answer->id}}" value="{{$answer->id}}" selected>{{$answer->answer}}</option>
                                          @else
                                           <option id="{{$job->form_Section->id.'.'.$question->id.'.'.$answer->id}}" value="{{$answer->id}}" selected>{{$answer->answer}}</option>
                                           @endif
                                      @endforeach
                                    @endif
                                  </select>
                                </div>
                            @break
                            @case ('radio')

                              <div class="custom_clear col-md-12 ">
                                @if($question->answer_group)
                                  @foreach($question->answer_group->answer as $answer)
                                    <div class="custom_clear form-check float-left mt-3">
                                      @if(isset($response_groups[$index]))
                                      @if((!$response_groups[$index]->responses->where('answer_id',$answer->id)->where('question_id',$question->id)->isEmpty() )?? false)

                                      <strong class="text-primary">({{$answer->score}})</strong>

                                       <input type="radio" class="custom_clear form-check-input {{$answer->grade}}" id="answer_group{{$answer->id}}" name="radio.{{$job->form_Section->id.'.'.$question->id}}" value="{{$answer->id}}" checked>
                                       <label class="custom_clear form-check-label radio_checkbox_label" for="answer_group{{$answer->id}}">{{$answer->answer}}</label>
                                       <img src="{{url('/uploads/icons/'.$answer->icon)}}" style="width: 35px;height: 35px;margin-left: 0px;" alt="">
                                       @endif
                                       @else

                                        <strong class="text-primary">({{$answer->score}})</strong>

                                       <input type="radio" class="custom_clear form-check-input {{$answer->grade}}" id="answer_group{{$answer->id}}" name="radio.{{$job->form_Section->id.'.'.$question->id}}" value="{{$answer->id}}">
                                       <label class="custom_clear form-check-label radio_checkbox_label" for="answer_group{{$answer->id}}">{{$answer->answer}}</label>
                                       <img src="{{url('/uploads/icons/'.$answer->icon)}}" style="width: 35px;height: 35px;margin-left: 0px;" alt="">
                                       @endif
                                    </div>
                                  @endforeach
                                @endif
                              </div>
                            @break
                            @case ('checkbox')
                            <div class="custom_clear col-md-12 ">
                                @if($question->answer_group)
                                    @foreach($question->answer_group->answer as $answer)
                                        <div class="custom_clear form-check float-left mt-3">
                                   @if(isset($response_groups[$index]))
                                  @if((!$response_groups[$index]->responses->where('answer_id',$answer->id)->where('question_id',$question->id)->isEmpty() )?? false)

                                       <strong class="text-primary">({{$answer->score}})</strong>

                                            <input type="checkbox" class="custom_clear form-check-input" id="answer_group_checkbox{{$question->id.'.'.$answer->id}}" name="checkbox.{{$job->form_Section->id.'.'.$question->id.'.'.$answer->id}}" value="{{$answer->id}}" checked>
                                            <label class="custom_clear form-check-label radio_checkbox_label" for="answer_group_checkbox{{$question->id.'.'.$answer->id}}">{{$answer->answer}}</label>
                                            <img src="{{url('/uploads/icons/'.$answer->icon)}}" style="width: 35px;height: 35px;margin-left: 0px;" alt="">
                                          @endif
                                          @else

                          <strong class="text-primary">({{$answer->score}})</strong>

                                           <input type="checkbox" class="custom_clear form-check-input" id="answer_group_checkbox{{$question->id.'.'.$answer->id}}" name="checkbox.{{$job->form_Section->id.'.'.$question->id.'.'.$answer->id}}" value="{{$answer->id}}" >
                                            <label class="custom_clear form-check-label radio_checkbox_label" for="answer_group_checkbox{{$question->id.'.'.$answer->id}}">{{$answer->answer}}</label>
                                            <img src="{{url('/uploads/icons/'.$answer->icon)}}" style="width: 35px;height: 35px;margin-left: 0px;" alt="">
                                            @endif
                                        </div>

                                    @endforeach
                                @endif
                            </div>
                            @break
                            @case ('multi')
                              <div class="custom_clear col-md-12">
                                  <select searchable="Search here.." style="width: 100%;"  class="custom_clear mdb-select form_alignment" multiple  name="multi.{{$job->form_Section->id.'.'.$question->id}}">
                                    @if($question->answer_group)
                                      @foreach($question->answer_group->answer as $answer)
                                      @if(isset($response_groups[$index]))
                                       @if($answer->id==($response_groups[$index]->responses[$key]->answer_id ?? false))
                                          <option id="{{$job->form_Section->id.'.'.$question->id.'.'.$answer->id}}" value="{{$answer->id}}" selected>{{$answer->answer}}</option>
                                          @endif
                                          @else
                                          <option id="{{$job->form_Section->id.'.'.$question->id.'.'.$answer->id}}" value="{{$answer->id}}" selected>{{$answer->answer}}</option>
                                          @endif
                                      @endforeach
                                    @endif
                                  </select>
                              </div>
                            @break
                            @case ('date')
                              <div class="custom_clear col-md-12 ">
                                @if($question->answer_group)
                                 @if(isset($response_groups[$index]->responses[$key]))
                                  <div class="custom_clear md-form mt-5">
                                    <input type="text" placeholder="Date" id="date-picker-form" name="date.{{$job->form_Section->id.'.'.$question->id}}" class="custom_clear form_alignment form-control " value="{{$response_groups[$index]->responses[$key]->answer_text}}">
                                  </div>

                                @endif
                                @endif
                              </div>
                            @break
                            @case ('time')
                              <div class="custom_clear col-md-12 ">
                                @if($question->answer_group)
                                   @if(isset($response_groups[$index]->responses[$key]))
                                  <div class="custom_clear md-form mt-5">
                                    <input type="text" placeholder="Time" id="date-picker-form" name="time.{{$job->form_Section->id.'.'.$question->id}}" class="custom_clear form-control form_alignment" value="{{$response_groups[$index]->responses[$key]->answer_text}}">
                                  </div>

                                @endif
                                @endif
                              </div>
                            @break
                            @case ('datetime')
                              <div class="custom_clear col-md-12 ">
                                @if($question->answer_group)
                                 @if(isset($response_groups[$index]->responses[$key]))
                                  <div class="custom_clear md-form mt-5">
                                    <input type="text" placeholder="Date" id="date-picker-form" name="datetime.{{$job->form_Section->id.'.'.$question->id}}" class="custom_clear form-control form_alignment" value="{{$response_groups[$index]->responses[$key]->answer_text}}">
                                  </div>
                                @endif
                                @endif
                              </div>
                            @break
                            @case ('text')
                                <div class="custom_clear col-md-12 ">
                                  @if($question->answer_group)
                                   @if(isset($response_groups[$index]->responses[$key]))
                                    <div class="custom_clear md-form mt-5">
                                      <input  type="text" placeholder="Text" id="text-form"  name="text.{{$job->form_Section->id.'.'.$question->id}}" class="custom_clear  form-control form_alignment" value="{{$response_groups[$index]->responses[$key]->answer_text}}">
                                    </div>
                                  @endif
                                  @endif
                                </div>
                            @break
                                @case ('comment')
                                <div class="custom_clear col-md-12 text-left">

                                   {!! $question->comment !!}


                                </div>
                            @break
                             @case ('richtext')
                               @if($question->answer_group)
                                @if(isset($response_groups[$index]->responses[$key]))
                                <div class="md-form" style="padding-left:18px !important;width: 97% !important;">
                                  <textarea id="rich-text"  name="textarea.{{$job->form_Section->id.'.'.$question->id}}" rows="5" class="{{$job->form_Section->id.'-'.$question->id}} answertype_richtext " value="{{ $response_groups[$index]->responses[$key]->answer_text ?? ''}}"  ></textarea>
                                </div>
                                <script type="text/javascript">

                                 $(".{{$job->form_Section->id.'-'.$question->id}}").summernote('code',@json($response_groups[$index]->responses[$key]->answer_text ?? ''));
                                </script>
                                @endif
                                @endif
                            @break
                          @endswitch

                        </div>
                    @if($response_groups[$index]->responses[$key]->fail ?? false)
                       <div class="custom_clear md-form mt-5">
                                      <input  type="text" class="custom_clear form-control form_alignment" value="{{$response_groups[$index]->responses[$key]->comments}}">
                                    </div>
                        @endif
                    </div>
                  @endforeach()
                </div>
              </form>
                <div class="custom_clear card-footer text-muted text-left">
                    {{$job->form_Section->footer}}
                </div>
              </div>
            </div>
          </div>
         @endforeach
      </div>
         <br>
                <div class="col-md-12">

                 <img src="{{asset('/uploads/response').'/'.($job_group->forms->ov_file ?? false)}}"  width="80%" height="60%" class="img_pop">
                </div>
                  <br>
                    <div class="md-form">

                    <textarea id="note" name="ov_note" class="md-textarea form-control" rows="3" readonly>{{$job_group->forms->ov_note}}</textarea>
                    <label for="note" class="active">Note</label>
                  </div>

    </div>
    <div class="custom_clear tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
      <h5 class="custom_clear card-header  t py-4 white-color">
          <strong>Static Forms View</strong>
      </h5>
      <div id="pdf-viewer-static"></div>

  </div>


<script src="{{asset('js/pdfview.js') }}"></script>
<script type="text/javascript">
$('.answertype_richtext').summernote();
$('#input-file-now').file_upload();
$('.datepicker').pickadate({selectYears:250,});
var options = {
  pdfOpenParams: {toolbar: '0'}
};
PDFObject.embed("{{ url('static_form/view-pdf', ['id' => $id_static]) }}", "#pdf-viewer-static",options);
</script>
<script src="{{asset('/custom/js/job.js')}}"></script>

@endsection
