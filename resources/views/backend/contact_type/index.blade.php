@extends('backend.layouts.detail')
@section('content')
    @php
       $data=localization();
    @endphp
    <div class="row">
      @include('backend.layouts.detail_sidebar')
        <div class="padding-left-custom remove-padding">
            {{ Breadcrumbs::render('contact_type') }}
 <div class="card">

            <div class="card-body">
                <div id="table" class="table-editable">

            <span class="table-add float-right mb-3 mr-2">
                <a class="text-success"  data-toggle="modal" data-target="#modalRegisterCompany">
                    <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
                </a>
            </span>
            <button class="btn btn-danger btn-sm">
                Delete Selected Contact Type
            </button>
            <button class="btn btn-primary btn-sm">
              Restore Deleted Contact Type
            </button>

            <button  style="display: none;">
                Show Active Contact Type
            </button>
            <button class="btn btn-warning btn-sm">
               Excel Export
            </button>
            <button class="btn btn-success btn-sm">
                Word Export
            </button>
            <table id="company_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th> Name
                    </th>
                    <th id="action">
                    </th>
                </tr>
                </thead>
                    <tr>
                        <td  class="pt-3-half" contenteditable="true">Via Email</td>
                        <td class="pt-3-half" contenteditable="true">
                            <a  data-toggle="modal" data-target="#modalEditForm" class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i></a>
                            <button type="button"class="btn btn-danger btn-sm my-0" onclick="deletecontact_typedata()"><i class="fas fa-trash"></i></button>

                        </td>
                    </tr>
                <tbody>
                </tbody>
            </table>
                          </div>
            </div>
        </div>
            @component('backend.contact_type.model.create')
            @endcomponent

             {{--    Edit Modal--}}
            @component('backend.contact_type.model.edit')
            @endcomponent
            {{--    END Edit Modal--}}
        </div>
    </div>


@endsection
<script type="text/javascript">
function deletecontact_typedata()
{

    swal({
        title:'Are you sure?',
        text: "Delete Record.",
        type: "error",
        showCloseButton: true,
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, Delete!",
        cancelButtonText: "Cancel!",
        closeOnConfirm: false,
        customClass: "confirm_class",
        closeOnCancel: false,
    },
    function(isConfirm){
        swal.close();
 });


}

</script>
