<table class="table table-bordered">
    <thead>
      <tr>
        @if(Auth()->user()->hasPermissionTo('contactType_name_excel_export') || Auth::user()->all_companies == 1 )
            <th> Name</th>
        @endif
      </tr>
    </thead>
    <tbody>
    @foreach($data as $row)
      <tr>
        @if(Auth()->user()->hasPermissionTo('contactType_name_excel_export') || Auth::user()->all_companies == 1 )
          <td>{{ $row->name }}</td>
        @endif  
      </tr>
    @endforeach
    </tbody>
</table>