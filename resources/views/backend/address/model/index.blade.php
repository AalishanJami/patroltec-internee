@php
    $data=localization();
@endphp
<link rel="stylesheet" href="{{ asset('custom/css/custom_style.css') }}">
<div class="modal fade" id="modalAddressTypeIndexpage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">
                    <span class="assign_class label{{getKeyid('address_type',$data)}}" data-id="{{getKeyid('address_type',$data) }}" data-value="{{checkKey('address_type',$data) }}" >
                        {!! checkKey('address_type',$data) !!}
                    </span>
                    @if(Auth()->user()->hasPermissionTo('view_cms') || Auth::user()->all_companies == 1 )
                        <button data-class="edit-disable" class="edit_cms_enable edit_cms_switch btn btn-primary btn-sm waves-effect waves-light" style="display: none;" >
                            Edit cms
                        </button>
                        <button  data-class="edit-enable" class="edit_cms_disable edit_cms_switch btn btn-danger btn-sm waves-effect waves-light">
                            Edit cms
                        </button>
                    @endif
                </h4>
                <button type="button" class="close address_listing_close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="card">
                <div class="card-body">
                    <div id="table-addresstype" class="table-editable">
                        @if(Auth()->user()->hasPermissionTo('create_addressType')  || Auth::user()->all_companies == 1 )
                            <span class="add-addresstype table-add float-right mb-3 mr-2">
                                <a class="text-success"><i class="fas fa-plus fa-2x" aria-hidden="true"></i></a>
                            </span>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_delete_addressType')  || Auth::user()->all_companies == 1 )
                            <button class="btn btn-danger btn-sm" id="deleteselectedaddresstype">
                                <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                                    {!! checkKey('selected_delete',$data) !!}
                                </span>
                            </button>
                            <button class="btn btn-danger btn-sm" style="display: none;" id="deleteselectedaddresstypeSoft">
                                <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                                    {!! checkKey('selected_delete',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_active_addresstype') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-success btn-sm" style="display: none;" id="selectedactivebuttonaddresstype">
                                <span class="assign_class label{{getKeyid('selected_active',$data)}}" data-id="{{getKeyid('selected_active',$data) }}" data-value="{{checkKey('selected_active',$data) }}" >
                                    {!! checkKey('selected_active',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_restore_delete_addressType') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-primary btn-sm" id="restorebuttonaddresstype">
                                <span class="assign_class label{{getKeyid('restore',$data)}}" data-id="{{getKeyid('restore',$data) }}" data-value="{{checkKey('restore',$data) }}" >
                                    {!! checkKey('restore',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('active_addressType') || Auth::user()->all_companies == 1 )
                            <button  style="display: none;" class="btn btn-primary btn-sm" id="activebuttonaddresstype">
                                <span class="assign_class label{{getKeyid('show_active',$data)}}" data-id="{{getKeyid('show_active',$data) }}" data-value="{{checkKey('show_active',$data) }}" >
                                    {!! checkKey('show_active',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('csv_addressType') || Auth::user()->all_companies == 1 )
                            <form class="form-style" method="POST" id="export_excel_addresstype" action="{{ url('addresstype/export/excel') }}">
                                {!! csrf_field() !!}
                                <input type="hidden" class="addresstype_export" name="excel_array" value="1">
                                <button  type="submit" class="form_submit_check btn btn-warning btn-sm">
                                    <span class="assign_class label{{getKeyid('excel_export',$data)}}" data-id="{{getKeyid('excel_export',$data) }}" data-value="{{checkKey('excel_export',$data) }}" >
                                        {!! checkKey('excel_export',$data) !!}
                                    </span>
                                </button>
                            </form>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('word_addressType') || Auth::user()->all_companies == 1 )
                            <form class="form-style" method="POST" id="export_world_addresstype" action="{{ url('addresstype/export/world') }}">
                                {!! csrf_field() !!}
                                <input type="hidden" class="addresstype_export" name="word_array" value="1">
                                <button  type="submit"class="form_submit_check btn btn-success btn-sm">
                                    <span class="assign_class label{{getKeyid('word_export',$data)}}" data-id="{{getKeyid('word_export',$data) }}" data-value="{{checkKey('word_export',$data) }}" >
                                        {!! checkKey('word_export',$data) !!}
                                    </span>
                                </button>
                            </form>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('pdf_addressType') || Auth::user()->all_companies == 1 )
                            <form  class="form-style" {{pdf_view("address_types")}} id="export_pdf_addresstype" method="POST" action="{{ url('addresstype/export/pdf') }}">
                                {!! csrf_field() !!}
                                <input type="hidden" class="addresstype_export" name="pdf_array" value="1">
                                <button  type="submit" class="form_submit_check btn btn-secondary btn-sm">
                                    <span class="assign_class label{{getKeyid('pdf_export',$data)}}" data-id="{{getKeyid('pdf_export',$data) }}" data-value="{{checkKey('pdf_export',$data) }}" >
                                        {!! checkKey('pdf_export',$data) !!}
                                    </span>
                                </button>
                            </form>
                        @endif
                        <div id="table" class="table-editable">
                            <table id="addressTypeTable" class="address_table table table-bordered table-responsive-md table-striped">
                                <thead>
                                <tr>
                                    <th class="no-sort all_checkboxes_style">
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input addresstype_checked" id="addresstype_checkbox_all">
                                            <label class="form-check-label" for="addresstype_checkbox_all">
                                                <span class="assign_class label{{getKeyid('all',$data)}}" data-id="{{getKeyid('all',$data) }}" data-value="{{checkKey('all',$data) }}" >
                                                    {!! checkKey('all',$data) !!}
                                                </span>
                                            </label>
                                        </div>
                                    </th>
                                    {{--                                    <th id="action" class="no-sort"></th>--}}
                                    <th>
                                        <span class="assign_class label{{getKeyid('name',$data)}}" data-id="{{getKeyid('name',$data) }}" data-value="{{checkKey('name',$data) }}" >
                                            {!! checkKey('name',$data) !!}
                                        </span>
                                    </th>
                                    <th id="action" class="no-sort all_action_btn"></th>
                                </tr>
                                </thead>
                                <tbody id="addresstype_data"></tbody>
                            </table>
                        </div>
                    </div>
                    @if(Auth()->user()->all_companies == 1)
                        <input type="hidden" id="addressType_name" value="1">
                        <input type="hidden" id="addressType_checkbox" value="1">
                        <!-- edit permision -->
                        <input type="hidden" id="edit_addressType" value="1">
                        <input type="hidden" id="delete_addressType" value="1">
                        <!-- create -->
                        <input type="hidden" id="addressType_name_create" value="1">
                        <!-- edit -->
                        <input type="hidden" id="addressType_name_edit" value="1">
                    @else
                        <input type="hidden" id="addressType_name" value="{{Auth()->user()->hasPermissionTo('addressType_name')}}">
                        <input type="hidden" id="addressType_checkbox" value="{{Auth()->user()->hasPermissionTo('addressType_checkbox')}}">
                        <!-- edit permision -->
                        <input type="hidden" id="edit_addressType" value="{{Auth()->user()->hasPermissionTo('edit_addressType')}}">
                        <input type="hidden" id="delete_addressType" value="{{Auth()->user()->hasPermissionTo('delete_addressType')}}">
                        <!-- create -->
                        <input type="hidden" id="addressType_name_create" value="{{Auth()->user()->hasPermissionTo('addressType_name_create')}}">
                        <!-- edit -->
                        <input type="hidden" id="addressType_name_edit" value="{{Auth()->user()->hasPermissionTo('addressType_name_edit')}}">
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="{{ asset('custom/js/addresstype.js') }}" ></script>
<script type="text/javascript">
    function addresstype_data(url) {
        var table = $('.address_table').dataTable(
            {
                "columnDefs": [
                    { "orderable": false, "targets": [0,2] }
                ],
                processing: true,
                language: {
                    'search':'<span class="assign_class label'+$('#search_id').attr('data-id')+'" data-id="'+$('#search_id').attr('data-id')+'" data-value="'+$('#search_id').val()+'" >'+$('#search_id').val()+'</span>',
                    'paginate':{
                        'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                        'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                    },
                    'lengthMenu': '_MENU_',
                    'info': ' ',
                },
                "ajax": {
                    "url": url,
                    "type": 'get',
                },
                "createdRow": function( row, data,dataIndex,columns )
                {
                    var checkbox_permission=$('#checkbox_permission').val();
                    var checkbox='';
                    checkbox+='<div class="form-check"><input type="checkbox" class="selectedrowaddresstype form-check-input addresstype_checked" id="addresstype'+data.id+'"><label class="form-check-label" for="addresstype'+data.id+'""></label></div>';
                    var submit='';
                    submit+='<a type="button" class="btn btn-primary all_action_btn_margin btn-xs my-0 waves-effect waves-light  saveaddresstypedata show_tick_btn'+data.id+'" style="display: none;"  id="'+data.id+'"><i class="fas fa-check"></i></a>';
                    $(columns[0]).html(checkbox);
                    $(columns[1]).attr('id', 'nameD'+data['id']);
                    $(columns[1]).attr('data-id',data['id']);
                    $(columns[1]).attr('onkeydown', 'editSelectedRow(addresstype_tr'+data['id']+')');
                    if($('#addressType_name_edit').val() && $('#edit_addressType').val())
                    {
                        $(columns[1]).attr('Contenteditable', 'true');
                        $(columns[2]).append(submit);
                    }
                    $(columns[1]).attr('class','edit_inline_addresstype');
                    $(row).attr('id', 'addresstype_tr'+data['id']);
                    $(row).attr('data-id',data['id']);
                    //$(row).attr('class', 'selectedrowaddresstype');
                },
                columns:
                    [
                        {data: 'checkbox', name: 'checkbox',visible:$('#addressType_checkbox').val()},
                        {data: 'name', name: 'name',visible:$('#addressType_name').val()},
                        {data: 'actions', name: 'actions'},
                        // {data: 'submit', name: 'submit',},
                    ],

            }
        );

        $(".hideinline_tick_btn").hide();
        if ($(":checkbox").prop('checked',true)){
            $(":checkbox").prop('checked',false);
        }
    }
    $(document).ready(function () {
        var url='/addresstype/getall';
        addresstype_data(url);
        // $("body").on('click','.edit_inline_addresstype',function () {
        //     // $(".show_tick_btn"+$(this).attr('data-id')).show();
        // });
    });
</script>
