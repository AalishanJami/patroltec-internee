@php
$data=localization();
@endphp
<style type="text/css">
   .ml-4-5 {
   margin-left: 2.5rem !important;
   }
    .form-custom-boutton{
      margin-left: unset!important;
      padding: 0.3rem!important;
   }
</style>
<div class="modal fade" id="modalAddressEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">
                    <span class="assign_class label{{getKeyid('address',$data)}}" data-id="{{getKeyid('address',$data) }}" data-value="{{checkKey('address',$data) }}" >
                        {!! checkKey('address',$data) !!}
                    </span>
                     @if(Auth()->user()->hasPermissionTo('view_cms') || Auth::user()->all_companies == 1 )
                        <button data-class="edit-disable" class="edit_cms_enable edit_cms_switch btn btn-primary btn-sm waves-effect waves-light" style="display: none;" >
                            Edit cms
                        </button>
                        <button  data-class="edit-enable" class="edit_cms_disable edit_cms_switch btn btn-danger btn-sm waves-effect waves-light">
                            Edit cms
                        </button>
                    @endif
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @if(Auth()->user()->hasPermissionTo('edit_address') || Auth::user()->all_companies == 1 )
                <form id="address_type_form_edit" >
                    <div class="modal-body">
                        <div class="md-form mb-5">
                            <div class="row">
                                <div class="col">
                                    <input type="hidden" name="id" id="edit_id">
                                    @if(Auth()->user()->hasPermissionTo('address_addressType_edit') || Auth::user()->all_companies == 1 )
                                    <a class="text-success modal_addresstype" >
                                        <button type="button" class="btn btn-sm btn-outline-primary waves-effect form-custom-boutton" style="float: left;">
                                            <span class="assign_class label{{getKeyid('address_type',$data)}}" data-id="{{getKeyid('address_type',$data) }}" data-value="{{checkKey('address_type',$data) }}" >
                                                {!! checkKey('address_type',$data) !!}
                                            </span>
                                        </button>
                                    </a>
                                    <div class="md-form ml-4-5 address_listing">
                                        <select searchable="Search here.."  class="mdb-select" name="address_type_id"  >
                                            <option value="" disabled>Please Select</option>
                                            @foreach($addressType as $value)
                                                <option value="{{$value->id}}">{{$value->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @endif
                                </div>
                                @if(Auth()->user()->hasPermissionTo('address_phone_edit') || Auth::user()->all_companies == 1 )
                                    <div class="col">
                                        <div class="md-form mb-5">
                                            <i class="far fa-address-card prefix grey-text"></i>
                                            <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="telephone" id="edit_telephone" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                            <label class="active" data-error="wrong" for="name">
                                               <span class="assign_class label{{getKeyid('number',$data)}}" data-id="{{getKeyid('number',$data) }}" data-value="{{checkKey('number',$data) }}" >
                                                    {!! checkKey('number',$data) !!}
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <div class="row">
                                @if(Auth()->user()->hasPermissionTo('address_email_edit') || Auth::user()->all_companies == 1 )
                                    <div class="col">
                                        <div class="md-form mb-5">
                                            <i class="fas fa-envelope prefix grey-text"></i>
                                            <input type="text" class="form-control " name="email" id="edit_email" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                            <label class="active"  for="name">
                                                <span class="assign_class label{{getKeyid('email',$data)}}" data-id="{{getKeyid('email',$data) }}" data-value="{{checkKey('email',$data) }}" >
                                                    {!! checkKey('email',$data) !!}
                                                </span>
                                            </label>
                                            <small class="text-danger" style="text-transform:initial;position:absolute;top:98%;left: 7% !important;" id="edit_email_address"></small>
                                        </div>
                                    </div>
                                @endif
                                @if(Auth()->user()->hasPermissionTo('address_building_edit') || Auth::user()->all_companies == 1 )
                                    <div class="col">
                                        <div class="md-form mb-5">
                                            <i class="fas fa-building prefix grey-text"></i>
                                            <input type="text" class="form-control" name="name" id="edit_name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                            <label class="active"  for="name">
                                                <span class="assign_class label{{getKeyid('building',$data)}}" data-id="{{getKeyid('building',$data) }}" data-value="{{checkKey('building',$data) }}" >
                                                    {!! checkKey('building',$data) !!}
                                                </span>
                                            </label>
                                            <small class="text-danger" style="text-transform:initial;position:absolute;top:98%;left: 7% !important;" id="building_name">{{ $errors->first('name') }}</small>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <div class="row">
                                @if(Auth()->user()->hasPermissionTo('address_addressType_edit') || Auth::user()->all_companies == 1 )
                                    <div class="col">
                                        <div class="md-form mb-5">
                                            <i class="fas fa-road prefix grey-text"></i>
                                            <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="address_street" id="edit_address_street" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                            <label class="active" data-error="wrong" for="name">
                                               <span class="assign_class label{{getKeyid('street',$data)}}" data-id="{{getKeyid('street',$data) }}" data-value="{{checkKey('street',$data) }}" >
                                                    {!! checkKey('street',$data) !!}
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                @endif
                                @if(Auth()->user()->hasPermissionTo('address_town_edit') || Auth::user()->all_companies == 1 )
                                    <div class="col">
                                        <div class="md-form mb-5">
                                            <i class="fas fa-road prefix grey-text"></i>
                                            <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="town" id="edit_town" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                            <label class="active" data-error="wrong" for="name">
                                                <span class="assign_class label{{getKeyid('town',$data)}}" data-id="{{getKeyid('town',$data) }}" data-value="{{checkKey('town',$data) }}" >
                                                    {!! checkKey('town',$data) !!}
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <div class="row">
                                @if(Auth()->user()->hasPermissionTo('address_state_edit') || Auth::user()->all_companies == 1 )
                                    <div class="col">
                                        <div class="md-form mb-5">
                                            <i class="fas fa-city prefix grey-text"></i>
                                            <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="state" id="edit_state" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                            <label class="active" data-error="wrong" for="name">
                                               <span class="assign_class label{{getKeyid('state',$data)}}" data-id="{{getKeyid('state',$data) }}" data-value="{{checkKey('state',$data) }}" >
                                                    {!! checkKey('state',$data) !!}
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                @endif
                                @if(Auth()->user()->hasPermissionTo('address_country_edit') || Auth::user()->all_companies == 1 )
                                    <div class="col">
                                        <div class="md-form mb-5">

                                            <i class="fas fa-globe prefix grey-text"></i>
                                            <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="country" id="edit_country" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                            <label class="active" data-error="wrong" for="name">
                                                <span class="assign_class label{{getKeyid('country',$data)}}" data-id="{{getKeyid('country',$data) }}" data-value="{{checkKey('country',$data) }}" >
                                                    {!! checkKey('country',$data) !!}
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <div class="row">
                                @if(Auth()->user()->hasPermissionTo('address_postcode_edit') || Auth::user()->all_companies == 1 )
                                    <div class="col">
                                        <div class="md-form mb-5">
                                            <i class="fas fa-user prefix grey-text"></i>
                                            <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="post_code" id="edit_post_code" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                            <label class="active" data-error="wrong" for="name">
                                                <span class="assign_class label{{getKeyid('post_code',$data)}}" data-id="{{getKeyid('post_code',$data) }}" data-value="{{checkKey('post_code',$data) }}" >
                                                    {!! checkKey('post_code',$data) !!}
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <div class="row justify-content-center">
                                <a class="btn btn-primary btn-sm" onclick="update()">
                                    <span class="assign_class label{{getKeyid('update',$data)}}" data-id="{{getKeyid('update',$data) }}" data-value="{{checkKey('update',$data) }}" >
                                        {!! checkKey('update',$data) !!}
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </form>
            @endif
        </div>
    </div>
    @component('backend.address_type.index')
    @endcomponent
</div>
