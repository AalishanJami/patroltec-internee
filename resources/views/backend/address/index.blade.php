@extends('backend.layouts.detail')
@section('title', 'Address')
@section('headscript')
    <script src="{{ asset('custom/js/addresstype.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('custom/css/custom_style.css') }}">
@stop
@section('content')
    @php
        $data=localization();
    @endphp
    @include('backend.layouts.detail_sidebar')
    <div class="padding-left-custom remove-padding">
        {!! breadcrumInner('address',Session::get('project_id'),'locations','/project/detail/'.Session::get('project_id') ,'project','/project') !!}
        <div class="card">
            <div class="card-body">
                <div id="table" class="table-responsive table-editable">
                    <span class="table-add float-right mb-3 mr-2">
                        @if(Auth()->user()->hasPermissionTo('create_address') || Auth::user()->all_companies == 1 )
                            <a class="text-success"  data-toggle="modal" data-target="#modalAddressCreate">
                            <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
                        </a>
                        @endif
                    </span>
                    @if(Auth()->user()->hasPermissionTo('delete_address') || Auth::user()->all_companies == 1 )
                        <button id="deleteSelectedAddressLocation" class="btn btn-danger btn-sm">
                            <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                                {!! checkKey('selected_delete',$data) !!}
                            </span>
                        </button>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('restore_delete_address') || Auth::user()->all_companies == 1 )
                        <button id="restore_button_address" class="btn btn-primary btn-sm">
                            <span class="assign_class label{{getKeyid('restore',$data)}}" data-id="{{getKeyid('restore',$data) }}" data-value="{{checkKey('restore',$data) }}" >
                                {!! checkKey('restore',$data) !!}
                            </span>
                        </button>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('active_address') || Auth::user()->all_companies == 1 )
                        <button id="selected_active_button_address" class="btn btn-success btn-sm" style="display: none;">
                            <span class="assign_class label{{getKeyid('selected_active',$data)}}" data-id="{{getKeyid('selected_active',$data) }}" data-value="{{checkKey('selected_active',$data) }}" >
                                {!! checkKey('selected_active',$data) !!}
                            </span>
                        </button>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('active_address') || Auth::user()->all_companies == 1 )
                        <button id="show_active_button_address" class="btn btn-primary btn-sm" style="display: none;">
                            <span class="assign_class label{{getKeyid('show_active',$data)}}" data-id="{{getKeyid('show_active',$data) }}" data-value="{{checkKey('show_active',$data) }}" >
                                {!! checkKey('show_active',$data) !!}
                            </span>
                        </button>
                    @endif

                    @if(Auth()->user()->hasPermissionTo('csv_address') || Auth::user()->all_companies == 1 )
                        <form class="form-style" method="POST" action="{{ url('/address/exportExcel') }}">
                            {!! csrf_field() !!}
                            <input type="hidden" id="export_excel_address" name="export_excel_address" value="1">
                            <input type="hidden" class="address_export" name="excel_array" value="1">
                            <button  type="submit" id="export_excel_address_btn" class="form_submit_check btn btn-warning btn-sm">
                                <span class="assign_class label{{getKeyid('excel_export',$data)}}" data-id="{{getKeyid('excel_export',$data) }}" data-value="{{checkKey('excel_export',$data) }}" >
                                    {!! checkKey('excel_export',$data) !!}
                                </span>
                            </button>
                        </form>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('word_address') || Auth::user()->all_companies == 1 )
                        <form class="form-style" method="POST" action="{{ url('/address/exportWord') }}">
                            {!! csrf_field() !!}
                            <input type="hidden" id="export_word_address" name="export_word_address" value="1">
                            <input type="hidden" class="address_export" name="word_array" value="1">
                            <button  type="submit" id="export_word_address_btn" class="form_submit_check btn btn-success btn-sm">
                                <span class="assign_class label{{getKeyid('word_export',$data)}}" data-id="{{getKeyid('word_export',$data) }}" data-value="{{checkKey('word_export',$data) }}" >
                                    {!! checkKey('word_export',$data) !!}
                                </span>
                            </button>
                        </form>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('pdf_address') || Auth::user()->all_companies == 1 )
                        <form  class="form-style" {{pdf_view("location_addresses")}}  method="POST" action="{{ url('address/exportPdf') }}">
                            <input type="hidden" id="export_pdf_address" name="export_pdf_address" value="1">
                            <input type="hidden" class="address_export" name="pdf_array" value="1">
                            {!! csrf_field() !!}
                            <button  type="submit" id="export_pdf_address_btn"  class="form_submit_check btn btn-secondary btn-sm">
                                <span class="assign_class label{{getKeyid('pdf_export',$data)}}" data-id="{{getKeyid('pdf_export',$data) }}" data-value="{{checkKey('pdf_export',$data) }}" >
                                    {!! checkKey('pdf_export',$data) !!}
                                </span>
                            </button>
                        </form>
                    @endif
                    @include('backend.label.input_label')
                    <input type="hidden" name="flag" id="flagAddress" value="1">
                    <table id="location_address" class="table  table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th class="no-sort all_checkboxes_style">
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input addresslocation_checked" id="addresslocation_checkbox_all">
                                    <label class="form-check-label" for="addresslocation_checkbox_all">
                                        <span class="assign_class label{{getKeyid('all',$data)}}" data-id="{{getKeyid('all',$data) }}" data-value="{{checkKey('all',$data) }}" >
                                            {!! checkKey('all',$data) !!}
                                        </span>
                                    </label>
                                </div>
                            </th>
                            <th class="th-sm">
                                <span class="assign_class label{{getKeyid('building',$data)}}" data-id="{{getKeyid('building',$data) }}" data-value="{{checkKey('building',$data) }}" >
                                    {!! checkKey('building',$data) !!}
                                </span>
                            </th>
                            <th>
                                <span class="assign_class label{{getKeyid('address_type',$data)}}" data-id="{{getKeyid('address_type',$data) }}" data-value="{{checkKey('address_type',$data) }}" >
                                    {!! checkKey('address_type',$data) !!}
                                </span>
                            </th>
                            <th>
                                <span class="assign_class label{{getKeyid('number',$data)}}" data-id="{{getKeyid('number',$data) }}" data-value="{{checkKey('number',$data) }}" >
                                    {!! checkKey('number',$data) !!}
                                </span>
                            </th>
                            <th>
                                <span class="assign_class label{{getKeyid('email',$data)}}" data-id="{{getKeyid('email',$data) }}" data-value="{{checkKey('email',$data) }}" >
                                    {!! checkKey('email',$data) !!}
                                </span>
                            </th>
                            <th>
                                <span class="assign_class label{{getKeyid('post_code',$data)}}" data-id="{{getKeyid('post_code',$data) }}" data-value="{{checkKey('post_code',$data) }}" >
                                    {!! checkKey('post_code',$data) !!}
                                </span>
                            </th>
                            <th class="no-sort all_action_btn"></th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
        @include('backend.address.model.edit')
        @include('backend.address.model.create')
        @if(Auth()->user()->all_companies == 1)
            <input type="hidden" id="address_building" value="1">
            <input type="hidden" id="address_addressType" value="1">
            <input type="hidden" id="address_phone" value="1">
            <input type="hidden" id="address_email" value="1">
            <input type="hidden" id="address_postcode" value="1">
            <input type="hidden" id="edit_address" value="1">
            <input type="hidden" id="edit_bonusScore" value="1">
            <input type="hidden" id="address_checkbox" value="1">
        @else
            <input type="hidden" id="address_building" value="{{Auth()->user()->hasPermissionTo('address_building')}}">
            <input type="hidden" id="address_addressType" value="{{Auth()->user()->hasPermissionTo('address_addressType')}}">
            <input type="hidden" id="address_phone" value="{{Auth()->user()->hasPermissionTo('address_phone')}}">
            <input type="hidden" id="address_email" value="{{Auth()->user()->hasPermissionTo('address_email')}}">
            <input type="hidden" id="address_postcode" value="{{Auth()->user()->hasPermissionTo('address_postcode')}}">
            <input type="hidden" id="address_checkbox" value="{{Auth()->user()->hasPermissionTo('address_checkbox')}}">
        @endif
    </div>
    <script type="text/javascript" src="{{ asset('custom/js/address.js') }}" ></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.modal_addresstype').click(function(e) {
                if ($('.edit_cms_disable').css('display') == 'none') {
                    return 0;
                }
                $('#modalAddressTypeIndexpage').modal('show');
            });
            $(function () {
                locationAppend();
            });

            // (function ($) {
            //     var active ='<span class="assign_class label'+$('#breadcrumb_address').attr('data-id')+'" data-id="'+$('#breadcrumb_address').attr('data-id')+'" data-value="'+$('#breadcrumb_address').val()+'" >'+$('#breadcrumb_address').val()+'</span>';
            //     $('.breadcrumb-item.active').html(active);
            //     var parent ='<span class="assign_class label'+$('#dashboard_label').attr('data-id')+'" data-id="'+$('#dashboard_label').attr('data-id')+'" data-value="'+$('#dashboard_label').val()+'" >'+$('#dashboard_label').val()+'</span>';
            //     $('.breadcrumb-item a').html(parent);
            // }(jQuery));
        });
        function locationAppend() {
            var table = $('#location_address').dataTable({
                "columnDefs": [
                    { "orderable": false, "targets": [0,6] }
                ],
                processing: true,
                bInfo:false,
                language: {
                    'lengthMenu': '  _MENU_ ',
                    'search':'<span class="assign_class label'+$('#search_label').attr('data-id')+'" data-id="'+$('#search_label').attr('data-id')+'" data-value="'+$('#search_label').val()+'" >'+$('#search_label').val()+'</span>',
                    'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                    'info': ' _PAGE_ <span class="assign_class label'+$('#of_label').attr('data-id')+'" data-id="'+$('#of_label').attr('data-id')+'" data-value="'+$('#of_label').val()+'" >'+$('#of_label').val()+'</span> _PAGES_',
                    'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                    'paginate': {
                        'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                        'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                    },
                    'infoFiltered': "(filtered from _MAX_ total records)"
                },
                "ajax": {
                    "url": '/address/getAllLocations',
                    "type": 'get',
                },
                "createdRow": function( row, data, dataIndex,columns ) {
                    var checkbox='<div class="form-check"><input type="checkbox"  class="form-check-input selectedRowLocation address_checked" id="checked'+data.id+'"><label class="form-check-label" for="checked'+data.id+'"></label></div>';
                    $(columns[0]).html(checkbox);
                    console.log(data);
                    $(row).attr('id', 'addressLocation_tr'+data['id']);
                    var temp=data['id'];
                    $(row).attr('data-id', data['id']);
                    // $(row).attr('onclick',"selectedRowLocation(this.id)");
                },
                columns: [
                    {data: 'checkbox', name: 'checkbox',visible:$('#address_checkbox').val()},
                    {data: 'name', name: 'name',visible:$('#address_building').val()},
                    {data: 'addresstype', addresstype: 'addresstype',visible:$('#address_addressType').val()},
                    {data: 'telephone', name: 'telephone',visible:$('#address_phone').val()},
                    // {data: 'address_street', name: 'address_street'},
                    {data: 'email', name: 'email',visible:$('#address_email').val()},
                    {data: 'post_code', name: 'post_code',visible:$('#address_postcode').val()},
                    {data: 'actions', name: 'actions'},
                ],

            });
            if ($(":checkbox").prop('checked',true)){
                $(":checkbox").prop('checked',false);
            }
        }
    </script>
    <script type="text/javascript">
        window.onload = function() {
            document.getElementById("action").classList.remove('sorting_asc');
        };
    </script>
@endsection
