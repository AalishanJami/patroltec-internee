<table>
    <thead>
      <tr>
          @if(Auth()->user()->hasPermissionTo('address_building_excel_export') || Auth::user()->all_companies == 1 )
              <th>Name</th>
          @endif
          @if(Auth()->user()->hasPermissionTo('address_addressType_excel_export') || Auth::user()->all_companies == 1 )
          <th>Address Street</th>
          @endif
          @if(Auth()->user()->hasPermissionTo('address_addressType_excel_export') || Auth::user()->all_companies == 1 )
              <th>Address Village</th>
          @endif
          @if(Auth()->user()->hasPermissionTo('address_phone_excel_export') || Auth::user()->all_companies == 1 )
              <th>Town</th>
          @endif
          @if(Auth()->user()->hasPermissionTo('address_state_excel_export') || Auth::user()->all_companies == 1 )
              <th>State</th>
          @endif
          @if(Auth()->user()->hasPermissionTo('address_postcode_excel_export') || Auth::user()->all_companies == 1 )
              <th>Post Code</th>
          @endif
          @if(Auth()->user()->hasPermissionTo('address_country_excel_export') || Auth::user()->all_companies == 1 )
              <th>Country</th>
          @endif
          @if(Auth()->user()->hasPermissionTo('address_phone_excel_export') || Auth::user()->all_companies == 1 )
              <th>Telephone</th>
          @endif
          @if(Auth()->user()->hasPermissionTo('address_email_excel_export') || Auth::user()->all_companies == 1 )
              <th>Email</th>
          @endif
      </tr>
    </thead>
    <tbody>
    @foreach($data as $row)
      <tr>
          @if(Auth()->user()->hasPermissionTo('address_building_excel_export') || Auth::user()->all_companies == 1 )
              <td>{{ $row->name }}</td>
          @endif
          @if(Auth()->user()->hasPermissionTo('address_addressType_excel_export') || Auth::user()->all_companies == 1 )
              <td>{{ $row->address_street }}</td>
          @endif
          @if(Auth()->user()->hasPermissionTo('address_addressType_excel_export') || Auth::user()->all_companies == 1 )
              <td>{{ $row->address_village }}</td>
          @endif
          @if(Auth()->user()->hasPermissionTo('address_phone_excel_export') || Auth::user()->all_companies == 1 )
              <td>{{ $row->town }}</td>
          @endif
          @if(Auth()->user()->hasPermissionTo('address_state_excel_export') || Auth::user()->all_companies == 1 )
              <td>{{ $row->state }}</td>
          @endif
          @if(Auth()->user()->hasPermissionTo('address_postcode_excel_export') || Auth::user()->all_companies == 1 )
              <td>{{ $row->post_code }}</td>
          @endif
          @if(Auth()->user()->hasPermissionTo('address_country_excel_export') || Auth::user()->all_companies == 1 )
             <td>{{ $row->country }}</td>
          @endif
          @if(Auth()->user()->hasPermissionTo('address_phone_excel_export') || Auth::user()->all_companies == 1 )
              <td>{{ $row->telephone }}</td>
          @endif
          @if(Auth()->user()->hasPermissionTo('address_email_excel_export') || Auth::user()->all_companies == 1 )
              <td>{{ $row->email }}</td>
          @endif
      </tr>
    @endforeach
    </tbody>
</table>
