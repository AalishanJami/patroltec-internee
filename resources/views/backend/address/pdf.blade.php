@include('backend.layouts.pdf_start')
  <thead>
    <tr>
        @if(Auth()->user()->hasPermissionTo('address_building_pdf_export') || Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout"> Building</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('address_addressType_pdf_export') || Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout">Street</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('address_phone_pdf_export') || Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout">Telephone Number</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('address_email_pdf_export') || Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout">Email address</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('address_postcode_pdf_export') || Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout">Post Code</th>
        @endif
    </tr>
  </thead>
  <tbody>
    @foreach($data as $value)
      <tr>
          @if(Auth()->user()->hasPermissionTo('address_building_pdf_export') || Auth::user()->all_companies == 1 )
              <td class="pdf_table_layout"><p class="col_text_style">{{ $value->name }}</p></td>
          @endif
          @if(Auth()->user()->hasPermissionTo('address_addressType_pdf_export') || Auth::user()->all_companies == 1 )
              <td class="pdf_table_layout"><p class="col_text_style">{{ $value->address_street }}</p></td>
          @endif
          @if(Auth()->user()->hasPermissionTo('address_phone_pdf_export') || Auth::user()->all_companies == 1 )
              <td class="pdf_table_layout"><p class="col_text_style">{{ $value->telephone }}</p></td>
          @endif
          @if(Auth()->user()->hasPermissionTo('address_email_pdf_export') || Auth::user()->all_companies == 1 )
              <td class="pdf_table_layout"><p class="col_text_style">{{ $value->email }}</p></td>
          @endif
          @if(Auth()->user()->hasPermissionTo('address_postcode_pdf_export') || Auth::user()->all_companies == 1 )
               <td class="pdf_table_layout"><p class="col_text_style">{{ $value->post_code }}</p></td>
          @endif
      </tr>
    @endforeach
  </tbody>
@include('backend.layouts.pdf_end')
