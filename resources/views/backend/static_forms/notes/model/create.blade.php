@php
    $data=localization();
@endphp
<style type="text/css">
    @media (min-width: 576px) {
        .modal-dialog {
            max-width: 75% !important;
        }
    }
    #img_loader{
        position: absolute;
        z-index: 1000;
        top: 19%;
        left: 41%;
        width: 150px;
        height: 150px;
        display: none;
    }
</style>
<div class="modal fade" id="modalstatic_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">
                    <span class="assign_class label{{getKeyid('static_form',$data)}}" data-id="{{getKeyid('static_form',$data) }}" data-value="{{checkKey('static_form',$data) }}" >
                      {!! checkKey('static_form',$data) !!}
                    </span>
                    @if(Auth()->user()->hasPermissionTo('view_cms') || Auth::user()->all_companies == 1 )
                        <button data-class="edit-disable" class="edit_cms_enable edit_cms_switch btn btn-primary btn-sm waves-effect waves-light" style="display: none;" >
                            Edit cms
                        </button>
                        <button  data-class="edit-enable" class="edit_cms_disable edit_cms_switch btn btn-danger btn-sm waves-effect waves-light">
                            Edit cms
                        </button>
                  @endif
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="uploadForm" method="POST" action="{{url('static_form/storeUpload')}}" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="company_id" id="company_id_static_form">
                <div class="modal-body mx-3">
                    <div class="md-form mb-5">
                        @if(Auth()->user()->hasPermissionTo('documentStaticForm_title_create') || Auth::user()->all_companies == 1 )
                            <i class="fas fa-user prefix grey-text"></i>
                                <span class="ml-5 assign_class label{{getKeyid('title',$data)}}" data-id="{{getKeyid('title',$data) }}" data-value="{{checkKey('title',$data) }}" >
                                  {!! checkKey('title',$data) !!}
                                </span>
                            <input type="text" class="title_static" class="form-control validate @error('name') is-invalid @enderror" name="title" id="title" value="" required autocomplete="name" autofocus>
                                <!-- 
                            <label  for="name" class="active" >
                            </label> -->
                            <small class="title_required_message text-danger" style="text-transform: initial;left: 3.5%;" id="title_required_message"></small>
                        @endif
                    </div>
                    @if(Auth()->user()->hasPermissionTo('documentStaticForm_upload_create') || Auth::user()->all_companies == 1 )
                        <div class="file-upload-wrapper">
                            <div class="spinner-border text-success" id="img_loader"></div>
                            <input type="file" id="input-file-static_form" name="file" class="file-upload" />
                            <input type="hidden" name="notes" id="notes">
                        </div>
                        <br>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('documentStaticForm_notes_create') || Auth::user()->all_companies == 1 )
                        <label data-error="wrong" data-success="right" for="name" class="active">
                            <span class="assign_class label{{getKeyid('notes',$data)}}" data-id="{{getKeyid('notes',$data) }}" data-value="{{checkKey('notes',$data) }}" >
                              {!! checkKey('notes',$data) !!}
                            </span>
                        </label>
                        <div class="md-form mb-5" id="note">
                            <div class="summernote_inner body_notes" ></div>
                        </div>
                    @endif
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button class="btn btn-primary btn-sm" id="static_form_create" type="submit">
                        <span class="assign_class label{{getKeyid('save',$data)}}" data-id="{{getKeyid('save',$data) }}" data-value="{{checkKey('save',$data) }}" >
                          {!! checkKey('save',$data) !!}
                        </span>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        var company_id=$('#companies_selected_nav').children("option:selected").val();
        $('#company_id_static_form').val(company_id);
        $("#companies_selected_nav").change(function () {
            var company_id=$('#companies_selected_nav').children("option:selected").val();
            $('#company_id_static_form').val(company_id);
        });
        $('#input-file-static_form').file_upload();
        $("#static_form_create").click(function(e){
            if( $('.edit_cms_disable').css('display') == 'none' ) {
                return 0;
            }
            e.preventDefault();
            var notes=$('#note').find( '.note-editable, .card-block p' ).html();
            $('#notes').val(notes);
            $("#uploadForm").submit();
        });


        $("#uploadForm").on('submit',function (event) {
            if($("#title").val() ==""){
                $("#title_required_message").html('Title field required');
                return false;
            }
            $("#img_loader").show();

            event.preventDefault();

            var formData = $("#uploadForm").serializeObject();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: '{{route('static_form_storeUpload')}}',
                data:new FormData(this),
                // dataType:'JSON',
                contentType:false,
                cache:false,
                processData:false,
                success: function(data)
                {
                    toastr["success"]('uploaded successfully');
                    $("#img_loader").hide();
                    $('#upload_static_table').DataTable().clear().destroy();
                    $(".title_required_message").html();
                    var url='/static_form/getAllUpload';
                    uploadStaticAppend(url);
                    $('#modalstatic_form').modal('hide');
                },
                error: function (error) {
                    console.log(error);
                }
            });

        });
    });
</script>
