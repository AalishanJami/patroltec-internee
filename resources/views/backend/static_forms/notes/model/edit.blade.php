<style type="text/css">
    @media (min-width: 576px) {
        .modal-dialog {
            max-width: 75% !important;
        }
    }
    #edit_form_img_loader{
        position: absolute;
        z-index: 1000;
        top: 19%;
        left: 41%;
        width: 150px;
        height: 150px;
        display: none;
    }
</style>
<div class="modal fade" id="modalstaticEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">
                    <span class="assign_class label{{getKeyid('static_form',$data)}}" data-id="{{getKeyid('static_form',$data) }}" data-value="{{checkKey('static_form',$data) }}" >
                      {!! checkKey('static_form',$data) !!}
                    </span>
                    @if(Auth()->user()->hasPermissionTo('view_cms') || Auth::user()->all_companies == 1 )
                        <button data-class="edit-disable" class="edit_cms_enable edit_cms_switch btn btn-primary btn-sm waves-effect waves-light" style="display: none;" >
                            Edit cms
                        </button>
                        <button  data-class="edit-enable" class="edit_cms_disable edit_cms_switch btn btn-danger btn-sm waves-effect waves-light">
                            Edit cms
                        </button>
                    @endif
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="uploadFormEdit" method="POST" action="{{url('static_form/updateUpload')}}" enctype="multipart/form-data">
                @csrf
                <div class="modal-body mx-3">
                    <div class="md-form mb-5">
                        @if(Auth()->user()->hasPermissionTo('documentStaticForm_title_edit') || Auth::user()->all_companies == 1 )
                            <input type="hidden" name="id" id="edit_id">
                            <i class="fas fa-user prefix grey-text"></i>
                            <input type="text" class="form-control validate @error('name') is-invalid @enderror" id="edit_title" name="name" value="Title" required autocomplete="name" autofocus>
                            <label for="name" class="active" >
                                <span class="assign_class label{{getKeyid('title',$data)}}" data-id="{{getKeyid('title',$data) }}" data-value="{{checkKey('title',$data) }}" >
                                  {!! checkKey('title',$data) !!}
                                </span>
                            </label>
                            <small class="title_required_message edit_title_required_message text-danger" style="text-transform: initial;left: 3.5%;" id="title_required_message"></small>
                        @endif
                    </div>
                    @if(Auth()->user()->hasPermissionTo('documentStaticForm_upload_edit') || Auth::user()->all_companies == 1 )
                        <div class="file-upload-wrapper">
                            <div class="spinner-border text-success" id="edit_form_img_loader"></div>
                            <input type="file" id="input-file-static_form_edit" name="file" class="file-upload" />
                        </div>
                    @endif
                    <br>
                    @if(Auth()->user()->hasPermissionTo('documentStaticForm_notes_edit') || Auth::user()->all_companies == 1 )
                        <input type="hidden" name="notes" id="notes_edit">
                        <div class="md-form mb-5" id="notes_edit_div">
                            <div class="summernote_inner body_notes notes_value" ></div>
                        </div>
                    @endif
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button class="btn btn-primary btn-sm" id="static_form_edit" type="submit">
                        <span class="assign_class label{{getKeyid('update',$data)}}" data-id="{{getKeyid('update',$data) }}" data-value="{{checkKey('update',$data) }}" >
                            {!! checkKey('update',$data) !!}
                        </span>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="{{asset('editor/sprite.svg.js')}}"></script>
<script type="text/javascript">
    $('#input-file-static_form_edit').file_upload();
    $("#static_form_edit").click(function(e){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            return 0;
        }
        e.preventDefault();
        var notes=$('#notes_edit_div').find( '.note-editable, .card-block p' ).html();
        $('#notes_edit').val(notes);
        $("#uploadFormEdit").submit();
    });

    $("#uploadFormEdit").on('submit',function (event) {

        if($("#edit_title").val() ==""){
            $(".edit_title_required_message").html('Title field required');
            return false;
        }
        event.preventDefault();
        $("#edit_form_img_loader").show();
        var formData = $("#uploadForm").serializeObject();
        console.log(formData);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            url: '{{route('static_form_updateUpload')}}',
            data:new FormData(this),
            // dataType:'JSON',
            contentType:false,
            cache:false,
            processData:false,
            success: function(data)
            {
                toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-top-center",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };
                toastr["success"]('uploaded successfully');
                $("#edit_form_img_loader").hide();
                $('#upload_static_table').DataTable().clear().destroy();
                var url='/static_form/getAllUpload';
                uploadStaticAppend(url);
                $('#modalstaticEdit').modal('hide');
            },
            error: function (error) {
                console.log(error);
            }
        });

    });
</script>
