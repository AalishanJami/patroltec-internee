@include('backend.layouts.pdf_start')
    <thead>
    <tr>
        @if(Auth()->user()->hasPermissionTo('emailReport_ticketid_pdf_export') || Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout"> Ticket ID</th>
        @else
            <th style="vertical-align: text-top;" class="pdf_table_layout"></th>
        @endif
        @if(Auth()->user()->hasPermissionTo('emailReport_username_pdf_export') || Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout"> User Name</th>
        @else
            <th style="vertical-align: text-top;" class="pdf_table_layout"></th>
        @endif
        @if(Auth()->user()->hasPermissionTo('emailReport_title_pdf_export') || Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout"> Title</th>
        @else
            <th style="vertical-align: text-top;" class="pdf_table_layout"></th>
        @endif
        @if(Auth()->user()->hasPermissionTo('emailReport_message_pdf_export') || Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout"> Message</th>
        @else
            <th style="vertical-align: text-top;" class="pdf_table_layout"></th>
        @endif
        @if(Auth()->user()->hasPermissionTo('emailReport_senddate_pdf_export') || Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout"> Send Date</th>
        @else
            <th style="vertical-align: text-top;" class="pdf_table_layout"></th>
        @endif
    </tr>
    </thead>
    <tbody>
    @foreach($data as $value)
        <tr>
            @if(Auth()->user()->hasPermissionTo('emailReport_ticketid_pdf_export') || Auth::user()->all_companies == 1 )
                <td class="pdf_table_layout"><p class="col_text_style">{{$value->id}}</p></td>
            @else
                <td class="pdf_table_layout"><p class="col_text_style"></p></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('emailReport_username_pdf_export') || Auth::user()->all_companies == 1 )
                <td class="pdf_table_layout"><p class="col_text_style">{{$value->user_id}}</p></td>
            @else
                <td class="pdf_table_layout"><p class="col_text_style"></p></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('emailReport_title_pdf_export') || Auth::user()->all_companies == 1 )
                <td class="pdf_table_layout"><p class="col_text_style">{{$value->title}}</p></td>
            @else
                <td class="pdf_table_layout"><p class="col_text_style"></p></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('emailReport_message_pdf_export') || Auth::user()->all_companies == 1 )
                <td class="pdf_table_layout"><p class="col_text_style">{{$value->message}}</p></td>
            @else
                <td class="pdf_table_layout"><p class="col_text_style"></p></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('emailReport_senddate_pdf_export') || Auth::user()->all_companies == 1 )
                <td class="pdf_table_layout"><p class="col_text_style">{{$value->created_at}}</p></td>
            @else
                <td class="pdf_table_layout"><p class="col_text_style"></p></td>
            @endif
        </tr>
    @endforeach
    </tbody>
@include('backend.layouts.pdf_end')
