@extends('backend.layouts.backend')
@section('title', 'Emails')
@section('content')
    @php
        $data=localization();
    @endphp
    {{ Breadcrumbs::render('emails') }}
    <div class="card">
        <div class="card-body">
            <div id="table" class="table-editable table-responsive">
                @if(Auth()->user()->hasPermissionTo('csv_emailReport') || Auth::user()->all_companies == 1 )
                    <form class="form-style" id="export_excel_project" method="POST" action="{{ url('/emails/export/excel') }}">
                        {!! csrf_field() !!}
                        <input type="hidden" class="export_array" name="excel_array" value="1">
                        <input type="hidden" class="report_qr_export" name="excel_array" value="1">
                        <button  type="submit" class="form_submit_check btn btn-warning btn-sm">
                            <span class="assign_class label{{getKeyid('excel_export',$data)}}" data-id="{{getKeyid('excel_export',$data) }}" data-value="{{checkKey('excel_export',$data) }}" >
                              {!! checkKey('excel_export',$data) !!}
                            </span>
                        </button>
                    </form>
                @endif
                @if(Auth()->user()->hasPermissionTo('word_emailReport') || Auth::user()->all_companies == 1 )
                    <form class="form-style" method="POST" id="export_world_project" action="{{ url('/emails/export/word') }}">
                        {!! csrf_field() !!}
                        <input type="hidden" class="project_export" name="word_array" value="1">
                         <input type="hidden" class="export_array" name="word_array" value="1">
                        <button  type="submit" class="form_submit_check btn btn-success btn-sm">
                            <span class="assign_class label{{getKeyid('word_export',$data)}}" data-id="{{getKeyid('word_export',$data) }}" data-value="{{checkKey('word_export',$data) }}" >
                                {!! checkKey('word_export',$data) !!}
                            </span>
                        </button>
                    </form>
                @endif
                @if(Auth()->user()->hasPermissionTo('pdf_emailReport') || Auth::user()->all_companies == 1 )
                    <form  class="form-style" {{pdf_view('supports')}} method="POST" id="export_pdf_project" action="{{ url('/emails/export/pdf') }}">
                        {!! csrf_field() !!}
                        <input type="hidden" class="project_export" name="pdf_array" value="1">
                        <input type="hidden" class="export_array" name="pdf_array" value="1">
                        <button  type="submit" class="form_submit_check btn btn-secondary btn-sm">
                            <span class="assign_class label{{getKeyid('pdf_export',$data)}}" data-id="{{getKeyid('pdf_export',$data) }}" data-value="{{checkKey('pdf_export',$data) }}" >
                                {!! checkKey('pdf_export',$data) !!}
                            </span>
                        </button>
                    </form>
                @endif
                <table id="email_data" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>
                                <span class="assign_class label{{getKeyid('ticket_id',$data)}}" data-id="{{getKeyid('ticket_id',$data) }}" data-value="{{checkKey('ticket_id',$data) }}" >
                                    {!! checkKey('ticket_id',$data) !!}
                                </span>
                            </th>
                            <th>
                                <span class="assign_class label{{getKeyid('user_name',$data)}}" data-id="{{getKeyid('user_name',$data) }}" data-value="{{checkKey('user_name',$data) }}" >
                                    {!! checkKey('user_name',$data) !!}
                                </span>
                            </th>
                            <th>
                                <span class="assign_class label{{getKeyid('title',$data)}}" data-id="{{getKeyid('title',$data) }}" data-value="{{checkKey('title',$data) }}" >
                                    {!! checkKey('title',$data) !!}
                                </span>
                            </th>
                            <th>
                                <span class="assign_class label{{getKeyid('message',$data)}}" data-id="{{getKeyid('message',$data) }}" data-value="{{checkKey('message',$data) }}" >
                                    {!! checkKey('message',$data) !!}
                                </span>
                            </th>
                            <th>
                                <span class="assign_class label{{getKeyid('send_date',$data)}}" data-id="{{getKeyid('send_date',$data) }}" data-value="{{checkKey('send_date',$data) }}" >
                                    {!! checkKey('send_date',$data) !!}
                                </span>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>

            </div>
            @if(Auth()->user()->all_companies == 1)
                <input type="hidden" id="emailReport_checkbox" value="1">
                <input type="hidden" id="emailReport_ticketid" value="1">
                <input type="hidden" id="emailReport_username" value="1">
                <input type="hidden" id="emailReport_title" value="1">
                <input type="hidden" id="emailReport_message" value="1">
                <input type="hidden" id="emailReport_senddate" value="1">
            @else
                <input type="hidden" id="emailReport_checkbox" value="{{Auth()->user()->hasPermissionTo('emailReport_checkbox')}}">
                <input type="hidden" id="emailReport_ticketid" value="{{Auth()->user()->hasPermissionTo('emailReport_ticketid')}}">
                <input type="hidden" id="emailReport_username" value="{{Auth()->user()->hasPermissionTo('emailReport_username')}}">
                <input type="hidden" id="emailReport_title" value="{{Auth()->user()->hasPermissionTo('emailReport_title')}}">
                <input type="hidden" id="emailReport_message" value="{{Auth()->user()->hasPermissionTo('emailReport_message')}}">
                <input type="hidden" id="emailReport_senddate" value="{{Auth()->user()->hasPermissionTo('emailReport_senddate')}}">
            @endif

        </div>
    </div>
    <input type="hidden" class="get_current_path" value="{{Request::path()}}">
    @include('backend.label.input_label')
    <script type="text/javascript">
        function email_data(url) {
            $.ajax({
                type: 'GET',
                url: url,
                success: function(response)
                {
                    var table = $('#email_data').dataTable(
                    {
                        processing: true,
                        language: {
                              'lengthMenu': '  _MENU_ ',
                              'search':'<span class="assign_class label'+$('#search_id').attr('data-id')+'" data-id="'+$('#search_id').attr('data-id')+'" data-value="'+$('#search_id').val()+'" >'+$('#search_id').val()+'</span>',
                              'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                              'info':'',
                              'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                              'paginate': {
                                  'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                                  'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                              },
                              'infoFiltered': "(filtered from _MAX_ total records)"
                            },
                        "ajax": {
                            "url": url,
                            "type": 'get',
                        },
                        "createdRow": function( row, data, dataIndex,columns )
                        {
                            $(columns[1]).html(data['user']['name']);
                            $(row).attr('data-id',data['id']);

                        },
                        columns:
                            [
                                {data: 'id', name: 'id',visible:$('#emailReport_ticketid').val()},
                                {data: 'user_id', name: 'user_id',visible:$('#emailReport_username').val()},
                                {data: 'title', name: 'title',visible:$('#emailReport_title').val()},
                                {data: 'message', name: 'message',visible:$('#emailReport_message').val()},
                                {data: 'created_at', name: 'created_at',visible:$('#emailReport_senddate').val()},

                            ],

                    }
                    );
                },
                error: function (error) {
                    console.log(error);
                }
            });

        }
        $(document).ready(function () {
            var current_path=$('.get_current_path').val();
            localStorage.setItem('current_path',current_path);
            (function ($) {
                var active ='<span class="assign_class label'+$('#breadcrumb_email').attr('data-id')+'" data-id="'+$('#breadcrumb_email').attr('data-id')+'" data-value="'+$('#breadcrumb_email').val()+'" >'+$('#breadcrumb_email').val()+'</span>';
                $('.breadcrumb-item.active').html(active);
                var parent ='<span class="assign_class label'+$('#dashboard_label').attr('data-id')+'" data-id="'+$('#dashboard_label').attr('data-id')+'" data-value="'+$('#dashboard_label').val()+'" >'+$('#dashboard_label').val()+'</span>';
                $('.breadcrumb-item a').html(parent);
            }(jQuery));
            var url='/emails/getall';
            email_data(url);
        });
    </script>
@endsection
