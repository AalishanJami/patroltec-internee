@php
        $data=localization();
@endphp
<style type="text/css">
    @media (min-width: 576px) {
      .modal-dialog {
        max-width: 75% !important;
      }
    }
</style>
<div class="modal fade" id="modalSupport" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Support</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="container contact-form">
            <div class="contact-image">
                <img src="https://image.ibb.co/kUagtU/rocket_contact.png" alt="rocket_contact"/>
            </div>
            <form method="post">
                <h3>Drop Us a Message</h3>
               <div class="row">
                


                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="text" name="txtName" class="form-control" placeholder="Title" value="" />
                        </div>
                       <!--  <div class="form-group">
                            <input type="text" name="txtEmail" class="form-control" placeholder="Your Email *" value="" />
                        </div> -->
                      <!--   <div class="form-group">
                            <input type="text" name="txtPhone" class="form-control" placeholder="Your Phone Number *" value="" />
                        </div> -->
                    <!-- <div class="col-md-6"> -->
                        <div class="form-group">
                            <textarea name="txtMsg" class="form-control" placeholder="Description " style="width: 100%; height: 150px;"></textarea>
                        </div>
                    <!-- </div> -->
                        <div class="form-group">
                            <input type="submit" name="btnSubmit" class="btnContact" value="Send Message" />
                        </div>
                    </div>
                </div>
            </form>
</div>
        </div>
    </div>
</div>

