<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
<table>
    <thead>
    <tr>
        @if(Auth()->user()->hasPermissionTo('emailReport_ticketid_excel_export') || Auth::user()->all_companies == 1 )
            <th> Ticket ID</th>
        @else
            <th></th>
        @endif
        @if(Auth()->user()->hasPermissionTo('emailReport_username_excel_export') || Auth::user()->all_companies == 1 )
            <th> User Name</th>
        @else
            <th></th>
        @endif
        @if(Auth()->user()->hasPermissionTo('emailReport_title_excel_export') || Auth::user()->all_companies == 1 )
            <th> Title</th>
        @else
            <th></th>
        @endif
        @if(Auth()->user()->hasPermissionTo('emailReport_message_excel_export') || Auth::user()->all_companies == 1 )
            <th> Message</th>
        @else
            <th></th>
        @endif
        @if(Auth()->user()->hasPermissionTo('emailReport_senddate_excel_export') || Auth::user()->all_companies == 1 )
            <th> Send Date</th>
        @else
            <th></th>
        @endif
    </tr>
    </thead>
    <tbody>
    @foreach($data as $value)
        <tr>
            @if(Auth()->user()->hasPermissionTo('emailReport_ticketid_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{$value->id}}</td>
            @else
                <td></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('emailReport_username_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{$value->user_id}}</td>
            @else
                <td></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('emailReport_title_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{$value->title}}</td>
            @else
                <td></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('emailReport_message_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{$value->message}}</td>
            @else
                <td></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('emailReport_senddate_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{$value->created_at}}</td>
            @else
                <td></td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>
</body>
</html>
