@php
  $data=localization();
@endphp
<style type="text/css">
  @media (min-width: 576px) {
      .modal-dialog {
        max-width: 75% !important;
      }
    }
</style>
<div class="modal" id="modalNoteEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">
                  <span class="assign_class label{{getKeyid('notes',$data)}}" data-id="{{getKeyid('notes',$data) }}" data-value="{{checkKey('notes',$data) }}" >
                    {!! checkKey('notes',$data) !!} 
                  </span>
                  @if(Auth()->user()->hasPermissionTo('view_cms') || Auth::user()->all_companies == 1 )
                    <button data-class="edit-disable" class="edit_cms_enable edit_cms_switch btn btn-primary btn-sm waves-effect waves-light" style="display: none;" >
                        Edit cms
                    </button>
                    <button  data-class="edit-enable" class="edit_cms_disable edit_cms_switch btn btn-danger btn-sm waves-effect waves-light">
                        Edit cms
                    </button>
                  @endif
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="clientNoteFormEdit" >
              <input type="hidden" name="id" id="edit_id">
                <div class="modal-body mx-3">
                  @if(Auth()->user()->hasPermissionTo('clientNotes_subject_edit') || Auth::user()->all_companies == 1 )
                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input type="text" name="title" id="edit_title" class="form-control validate @error('name') is-invalid @enderror" name="name" value="" required autocomplete="name" autofocus>
                        <label class="active" data-error="wrong" data-success="right" for="name"  >
                          <span class="assign_class label{{getKeyid('title',$data)}}" data-id="{{getKeyid('title',$data) }}" data-value="{{checkKey('title',$data) }}" >
                            {!! checkKey('title',$data) !!} 
                          </span>
                        </label>
                         <strong class="title_required_message text-danger" id="title_required_message"></strong>
                    </div>
                    @endif
                  @if(Auth()->user()->hasPermissionTo('clientNotes_notes_edit') || Auth::user()->all_companies == 1 )
                    <input type="hidden" name="note" id="notes_edit">
                    <label data-error="wrong" data-success="right" for="name" class="active">
                      <span class="assign_class label{{getKeyid('notes',$data)}}" data-id="{{getKeyid('notes',$data) }}" data-value="{{checkKey('notes',$data) }}" >
                        {!! checkKey('notes',$data) !!} 
                      </span>
                    </label>
                    <div class="md-form mb-5" id="notes_edit_div">
                      <div class="summernote_inner body_notes notes_value" ></div>
                    </div>
                  @endif
                </div>

                <div class="modal-footer d-flex justify-content-center">
                  <button class="btn btn-primary btn-sm" id="client_notes_edit" type="submit">
                    <span class="assign_class label{{getKeyid('update',$data)}}" data-id="{{getKeyid('update',$data) }}" data-value="{{checkKey('update',$data) }}" >
                      {!! checkKey('update',$data) !!} 
                    </span>
                  </button>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="{{asset('editor/sprite.svg.js')}}"></script>
<script type="text/javascript">
 $("#client_notes_edit").click(function(e){
    e.preventDefault();
    var notes=$('#notes_edit_div').find('.note-editable, .card-block p' ).html();
    $('#notes_edit').val(notes);
    $("#clientNoteFormEdit").submit();
  });
  $("#clientNoteFormEdit").on('submit',function (event) {
      if ($('.edit_cms_disable').css('display') == 'none') {
          e.preventDefault();
          return 0;
      }
      if($("#edit_title").val() ==""){
          $("#title_required_message").html('Title field required');
          return false;
      }
      event.preventDefault();
      var formData = $("#clientNoteFormEdit").serializeObject();
      $.ajax({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          type: "POST",
          url: '{{url('note/update')}}',
          data:new FormData(this),
          contentType:false,
          cache:false,
          processData:false,
          success: function(respone)
          {
            toastr["success"](respone.message);
            $('#clientNoteTable').DataTable().clear().destroy();
            $(".title_required_message").html();
            var url='/note/getall';
            noteAppend(url);
            $('#modalNoteEdit').modal('hide');
          },
          error: function (error) {
              console.log(error);
          }
      });

  });


</script>
