@include('backend.layouts.pdf_start')
  <thead>
    <tr>
      @if($flag == 3)
        @if(Auth()->user()->hasPermissionTo('employeeNotes_subject_pdf_export') || Auth::user()->all_companies == 1)
          <th style="vertical-align: text-top;" class="pdf_table_layout"> Title</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('employeeNotes_notes_pdf_export') || Auth::user()->all_companies == 1)
          <th style="vertical-align: text-top;" class="pdf_table_layout"> Note</th>
        @endif
      @elseif($flag == 2)
        @if(Auth()->user()->hasPermissionTo('documentNotes_subject_pdf_export') || Auth::user()->all_companies == 1)
          <th style="vertical-align: text-top;" class="pdf_table_layout"> Title</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('documentNotes_notes_pdf_export') || Auth::user()->all_companies == 1)
          <th style="vertical-align: text-top;" class="pdf_table_layout"> Note</th>
        @endif
      @elseif($flag == 1)
        @if(Auth()->user()->hasPermissionTo('clientNotes_subject_pdf_export') || Auth::user()->all_companies == 1)
          <th style="vertical-align: text-top;" class="pdf_table_layout"> Title</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('clientNotes_notes_pdf_export') || Auth::user()->all_companies == 1)
          <th style="vertical-align: text-top;" class="pdf_table_layout"> Note</th>
        @endif
      @endif
    </tr>
  </thead>
  <tbody>
    @foreach($data as $value)
      <tr>
        @if($flag == 3)
          @if(Auth()->user()->hasPermissionTo('employeeNotes_subject_pdf_export') || Auth::user()->all_companies == 1)
            <td class="pdf_table_layout"><p class="col_text_style">{{ $value->title }}</p></td>
          @endif
          @if(Auth()->user()->hasPermissionTo('employeeNotes_notes_pdf_export') || Auth::user()->all_companies == 1)
            <td class="pdf_table_layout"><p class="col_text_style">{{ $value->note }}</p></td>
          @endif
        @elseif($flag == 2)
          @if(Auth()->user()->hasPermissionTo('documentNotes_subject_pdf_export') || Auth::user()->all_companies == 1)
            <td class="pdf_table_layout"><p class="col_text_style">{{ $value->title }}</p></td>
          @endif
          @if(Auth()->user()->hasPermissionTo('documentNotes_notes_pdf_export') || Auth::user()->all_companies == 1)
            <td class="pdf_table_layout"><p class="col_text_style">{{ $value->note }}</p></td>
          @endif
        @elseif($flag == 1)
          @if(Auth()->user()->hasPermissionTo('clientNotes_subject_pdf_export') || Auth::user()->all_companies == 1)
            <td class="pdf_table_layout"><p class="col_text_style">{{ $value->title }}</p></td>
          @endif
          @if(Auth()->user()->hasPermissionTo('clientNotes_notes_pdf_export') || Auth::user()->all_companies == 1)
            <td class="pdf_table_layout"><p class="col_text_style">{{ $value->note }}</p></td>
          @endif
        @endif
      </tr>
    @endforeach
  </tbody>
@include('backend.layouts.pdf_end')
