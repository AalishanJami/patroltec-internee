  <head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  </head>
  <body>
<table class="table table-striped table-bordered">
  <thead>
    <tr>
      @if($flag == 3)
        @if(Auth()->user()->hasPermissionTo('employeeNotes_subject_excel_export') || Auth::user()->all_companies == 1)
          <th> Title</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('employeeNotes_notes_excel_export') || Auth::user()->all_companies == 1)
          <th> Note</th>
        @endif
      @elseif($flag == 2)
        @if(Auth()->user()->hasPermissionTo('documentNotes_subject_excel_export') || Auth::user()->all_companies == 1)
          <th> Title</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('documentNotes_notes_excel_export') || Auth::user()->all_companies == 1)
          <th> Note</th>
        @endif
      @elseif($flag == 1)
        @if(Auth()->user()->hasPermissionTo('clientNotes_subject_excel_export') || Auth::user()->all_companies == 1)
          <th> Title</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('clientNotes_notes_excel_export') || Auth::user()->all_companies == 1)
          <th> Note</th>
        @endif
      @endif
    </tr>
  </thead>
  <tbody>
    @foreach($data as $value)
      <tr>
        @if($flag == 3)
          @if(Auth()->user()->hasPermissionTo('employeeNotes_subject_excel_export') || Auth::user()->all_companies == 1)
            <td>{{ $value->title }}</td>
          @endif
          @if(Auth()->user()->hasPermissionTo('employeeNotes_notes_excel_export') || Auth::user()->all_companies == 1)
            <td>{{ $value->note }}</td>
          @endif
        @elseif($flag == 2)
          @if(Auth()->user()->hasPermissionTo('documentNotes_subject_excel_export') || Auth::user()->all_companies == 1)
            <td>{{ $value->title }}</td>
          @endif
          @if(Auth()->user()->hasPermissionTo('documentNotes_notes_excel_export') || Auth::user()->all_companies == 1)
            <td>{{ $value->note }}</td>
          @endif
        @elseif($flag == 1)
          @if(Auth()->user()->hasPermissionTo('clientNotes_subject_excel_export') || Auth::user()->all_companies == 1)
            <td>{{ $value->title }}</td>
          @endif
          @if(Auth()->user()->hasPermissionTo('clientNotes_notes_excel_export') || Auth::user()->all_companies == 1)
            <td>{{ $value->note }}</td>
          @endif
        @endif
      </tr>
    @endforeach
  </tbody>
</table>
</body>
