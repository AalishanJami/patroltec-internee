@extends('backend.layouts.backend')
@section('title', 'Roles')
@section('content')
    @php
        $data=localization();
    @endphp
    <link rel="stylesheet" href="{{ asset('custom/css/custom_style.css') }}">
    {{ Breadcrumbs::render('roles') }}
    <div class="card">
        <div class="card-body">
            <div id="table" class="table-editable">
                @if(Auth()->user()->hasPermissionTo('create_roles') || Auth::user()->all_companies == 1 )
                    <span class="table-add float-right mb-3 mr-2">
                        <a class="text-success" href=""  data-toggle="modal" data-target="#modalrole">
                            <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
                        </a>
                    </span>
                @endif
                @if(Auth()->user()->hasPermissionTo('selected_delete_roles') || Auth::user()->all_companies == 1 )
                    <button id="deleteselectedrole" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" class=" assign_class btn btn-danger btn-sm label{{getKeyid('selected_delete',$data)}}">
                        {!! checkKey('selected_delete',$data) !!}
                    </button>
                @endif
                @if(Auth()->user()->hasPermissionTo('selected_restore_delete_roles') || Auth::user()->all_companies == 1 )
                    <button id="restorebuttonrole" data-id="{{getKeyid('restore',$data) }}" data-value="{{checkKey('restore',$data) }}"   class="btn btn-primary btn-sm assign_class label{{getKeyid('restore',$data)}}">
                        {!! checkKey('restore',$data) !!}
                    </button>
                @endif
                @if(Auth()->user()->hasPermissionTo('active_roles') || Auth::user()->all_companies == 1 )
                    <button id="showactivebuttonrole" data-id="{{getKeyid('show_active',$data) }}" data-value="{{checkKey('show_active',$data) }}"  class="btn btn-primary btn-sm assign_class label{{getKeyid('show_active',$data)}}" style="display: none;">
                        {!! checkKey('show_active',$data) !!}
                    </button>
                @endif
               <!--  <div class="form-style col-md-5">
                    <select searchable="Search here.."  id="companies_selected_roles" class="mdb-select md-form"  >
                        <option disabled selected><span class="assign_class label{{getKeyid('company',$data)}}" data-id="{{getKeyid('company',$data) }}" data-value="{{checkKey('company',$data) }}" >{!! checkKey('company',$data) !!} </span></option>
                        @foreach($companies as $company)
                            <option value="{{$company->id}}">
                                {{$company->name}}
                            </option>
                        @endforeach
                    </select>
                </div> -->
                @include('backend.label.input_label')
                <table id="role_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="th-sm all_checkboxes_style"></th>
                        <th class="th-sm assign_class label{{getKeyid('name',$data)}}" data-id="{{getKeyid('name',$data) }}" data-value="{{ checkKey('name',$data) }}" >
                            {!! checkKey('name',$data) !!}
                        </th>
                        <th class="th-sm assign_class label{{getKeyid('module',$data)}}" data-id="{{getKeyid('module',$data) }}" data-value="{{ checkKey('module',$data) }}" >
                            {!! checkKey('module',$data) !!}
                        </th>
                        <th class="no-sort"></th>
                    </tr>
                    </thead>

                    <tbody>
                    </tbody>
                </table>
                @component('backend.roles.model.edit')
                @endcomponent
                {{--    END Edit Modal--}}

                {{--    Register Modal--}}
                @component('backend.roles.model.create', ['permissions' => $permissions,'companies'=>$companies,])
                @endcomponent
                {{--end modal register--}}
                @if(Auth()->user()->all_companies == 1)
                    <input type="hidden" id="checkbox_permission" value="1">
                    <input type="hidden" id="name_permission" value="1">
                    <input type="hidden" id="module_permission" value="1">
                @else
                    <input type="hidden" id="checkbox_permission" value="{{Auth()->user()->hasPermissionTo('role_checkbox')}}">
                    <input type="hidden" id="name_permission" value="{{Auth()->user()->hasPermissionTo('role_name')}}">
                    <input type="hidden" id="module_permission" value="{{Auth()->user()->hasPermissionTo('role_module')}}">
                @endif
            </div>
        </div>
    </div>
    <input type="hidden" class="get_current_path" value="{{Request::path()}}">
    <script type="text/javascript">
        $(document).ready(function() {
            var current_path=$('.get_current_path').val();
            localStorage.setItem('current_path',current_path);
            $(function () {
                roleAppend();
            });
            (function ($) {
                var active ='<span class="assign_class label'+$('#breadcrums_roles').attr('data-id')+'" data-id="'+$('#breadcrums_roles').attr('data-id')+'" data-value="'+$('#breadcrums_roles').val()+'" >'+$('#breadcrums_roles').val()+'</span>';
                $('.breadcrumb-item.active').html(active);
                var parent ='<span class="assign_class label'+$('#dashboard_label').attr('data-id')+'" data-id="'+$('#dashboard_label').attr('data-id')+'" data-value="'+$('#dashboard_label').val()+'" >'+$('#dashboard_label').val()+'</span>';
                $('.breadcrumb-item a').html(parent);
            }(jQuery));
        });
        function roleAppend() {
            var table = $('#role_table').dataTable({
                processing: true,
                language: {
                    'lengthMenu': '  _MENU_ ',
                    'search':'<span class="assign_class label'+$('#search_label').attr('data-id')+'" data-id="'+$('#search_label').attr('data-id')+'" data-value="'+$('#search_label').val()+'" >'+$('#search_label').val()+'</span>',
                    'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                    'info': '  ',
                    'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                    'paginate': {
                        'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                        'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                    },
                    'infoFiltered': " "
                },
                "ajax": {
                    "url": 'getallroll',
                    "type": 'get',
                },
                "createdRow": function( row, data, dataIndex,columns ) {
                    var checkbox='<div class="form-check" id="'+data.id+'" onclick="selectedrow(this.id)"><input  type="checkbox" class="form-check-input selectedrowrole role_checked" id="role'+data.id+'"><label class="form-check-label" for="role'+data.id+'"></label></div>';
                    $(columns[0]).html(checkbox);
                    $(row).attr('id', data['id']);
                   // $(row).attr('onclick',"selectedrow(this.id)");
                },
                columns: [
                    {data: 'checkbox', name: 'checkbox',visible:$('#checkbox_permission').val(),},
                    {data: 'name', name: 'name',visible:$('#name_permission').val(),},
                    {data: 'module_name', name: 'module_name',visible:$('#module_permission').val(),},
                    {data: 'actions', name: 'actions'},

                ],
            });
        }
    </script>


@endsection
