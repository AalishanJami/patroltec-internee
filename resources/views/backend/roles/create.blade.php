@extends('backend.layouts.backend')
@section('content')
<?php 
  $lang=\Config::get('app.locale');
?>
<style type="text/css">
   [type=checkbox]:checked, [type=checkbox]:not(:checked) {
    position: unset;
    pointer-events: unset;
     opacity: unset;
}
</style>
<div class="container body">
   <div class="main_container">
      <div class="right_col" role="main">
         <div class="">
            <div class="page-title">
               <div class="title_left">
                  <h3>Role</h3>
               </div>
            </div>
            <div class="clearfix"></div>
            <div class="x_panel">
               <div class="x_title">
                  <h2>Role <small>Create</small></h2>
                   
                  <div class="clearfix"></div>
               </div>
               <div class="x_content">
                  <br />
                  @include('errors.errorDisplay')
                  <div class="col-md-12 center-margin">
                     {{ Form::open(array('url' => 'roles')) }}
                     @csrf
                     <div class="panel panel-default">
                       <div class="panel-heading"><b>Create Role</b></div>
                       <div class="panel-body">
                           <div class="col-md-4 form-group has-feedback arabic_right">
                              <input type="text" name="name" class="form-control " placeholder="Enter Name">
                              <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                           </div>
                       </div>
                     </div>
                     <div class="form-group"> 
                        <div class="panel panel-default">
                           <div class="panel-body row col-md-12">
                              @php
                                 $temp = 1;
                              @endphp
                              @foreach ($permissions as $key=>$permission_data)
                              @if($temp % 4 == 0)
                              <div class="row">
                              @endif
                                 <div class="col-md-3 col-sm-3 col-xs-12 arabic_left">
                                    <div class="panel panel-info">
                                       <div class="panel-heading">{{Form::label($key, ucfirst($key)) }}</div>
                                          <div class="panel-body">

                                             @foreach ($permission_data as $permission)
                                                @if($lang == 'en')
                                                   {{ Form::checkbox('permissions[]',  $permission->id ) }}
                                                     {{ Form::label($permission->permission_name, ucfirst($permission->permission_name)) }}
                                                @else
                                                   {{Form::checkbox('permissions[]',  $permission->id ) }}
                                                   {{Form::label($permission->ar_permission_name, ucfirst($permission->ar_permission_name)) }}
                                                @endif
                                               <br>
                                             @endforeach
                                          </div>
                                    </div>
                                 </div>
                                 @if($temp % 4 == 0)
                              </div>
                              @endif
                              @php
                                 $temp++;
                              @endphp
                              @endforeach
                           </div>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          
                           <button type="submit" class="btn btn-success">{{__('user.Submit')}}</button>
                        </div>
                     </div>
                     {{ Form::close() }}    
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- /page content -->
   </div>
</div>

@endsection