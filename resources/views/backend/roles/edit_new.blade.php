@php
$data=localization();
@endphp
{{ Form::model($role, array('url' => array('roles/update', $role->id), 'method' => 'POST')) }}
@csrf
<div class="modal-body mx-3">
  <div class="md-form mb-5">
    <i class="fas fa-user prefix grey-text"></i>
    @if(Auth()->user()->hasPermissionTo('role_name_edit') || Auth::user()->all_companies == 1 )
      <input id="new_company" type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="{{$role->name}}" required autocomplete="name" autofocus>
    @else
        <input id="new_company" type="text" class="form-control" name="name" value="{{$role->name}}" disabled >
    @endif
  </div>
  <input id="role_id" type="hidden"  value="{{$role->id}}">
  <div class="accordion md-accordion accordion-1" id="accordionEx23" role="tablist">
    <div class="card">
      <div class="card-header blue lighten-3 z-depth-1" role="tab" id="heading96">
        <h5 class="text-uppercase mb-0 py-1">
          <a class="white-text" data-toggle="collapse" href="#collapse96" aria-expanded="true"
            aria-controls="collapse96">
            Company
          </a>
        </h5>
      </div>
      <div id="collapse96" class="collapse " role="tabpanel" aria-labelledby="heading96"
        data-parent="#accordionEx23">
        <div class="card-body">
          @foreach($companies as $key=>$value)
            <div class="col-md-6 form-check">
              <input type="radio" class="form-check-input" {{ ($role->company_id == $value->id)? 'checked' : "" }} hidden id="{{$value->name}}" name="company_id" value="{{$value->id}}">
              <label for="{{$value->name}}" class="form-check-label">{{$value->name}}
              </label>
            </div>
          @endforeach()
        </div>
      </div>
    </div>
    <div class="accordion md-accordion" id="accordionEx" role="tablist" aria-multiselectable="true">
      @foreach ($groups as $key_group=>$permission_group)
        <div class="accordion md-accordion accordion-1 "  id="accordionEx23{{$permission_group->id}}" role="tablist">
          <div class="card">
            <div class="card-header blue lighten-3 z-depth-1" role="tab" id="heading{{$permission_group->id}}">
              <a class="collapsed font-weight-bold white-text" data-toggle="collapse" href="#collapse{{$permission_group->id}}"
                aria-expanded="false" aria-controls="collapse{{$permission_group->id}}">
                <h5 class="text-uppercase mb-0 py-1">
                  <span class="assign_class label{{getKeyid($permission_group->group,$data)}}" data-id="{{getKeyid($permission_group->group,$data) }}" data-value="{{checkKey($permission_group->group,$data) }}" >
                    {!! checkKey($permission_group->group,$data) !!}
                  </span>
                </h5>
              </a>
            </div>
            <div id="collapse{{$permission_group->id}}" class="collapse" role="tabpanel" aria-labelledby="heading{{$permission_group->id}}"
              data-parent="#accordionEx23{{$permission_group->id}}">
              <div class="card-body">
                <div class="row my-4">
                  <div class="col-md-12">
                    @foreach ($permission_group->PermissionGroupModule as $key=>$permission_data)
                      <div class="card">
                        <div class="card-header module_header_name" data-value="{{$permission_data->id}}" role="tab" id="headingTwo{{$permission_data->id}}">
                          <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseTwo{{$permission_data->id}}"
                            aria-expanded="false" aria-controls="collapseTwo{{$permission_data->id}}">
                            <h5 class="mb-0">
                             {{Form::label($permission_data->module_name, ucfirst($permission_data->module_name)) }}
                             <i class="fas fa-angle-down rotate-icon"></i>
                            </h5>
                          </a>
                        </div>
                        <div id="collapseTwo{{$permission_data->id}}" class="collapse" role="tabpanel" aria-labelledby="headingTwo{{$permission_data->id}}"
                          data-parent="#accordionEx">
                          <div class="card-body">
                             <div class="panel panel-info">
                                <div class="panel-body">
                                  <div class="row col-md-12" id="module_content_name{{$permission_data->id}}">
                                    <div class="spinner-border text-success loader" id="loader{{$permission_data->id}}"></div>
                                  </div>
                                </div>
                             </div>
                             <br>
                          </div>
                        </div>
                      </div>
                    @endforeach
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      @endforeach
    </div>
  </div>
  <div class="modal-footer d-flex justify-content-center">
    <button class="form_submit_check btn btn-primary btn-sm" type="submit">
      <span class="assign_class label{{getKeyid('update',$data)}}" data-id="{{getKeyid('update',$data) }}" data-value="{{checkKey('update',$data) }}" >
        {{checkKey('update',$data) }}
      </span>
    </button>
  </div>
</div>
{{ Form::close() }}
