@php
        $data=localization();
@endphp
<style type="text/css">
   [type=checkbox]:checked, [type=checkbox]:not(:checked),[type=radio]:checked, [type=radio]:not(:checked) {
    position: unset;
    pointer-events: unset;
     opacity: unset;
}
</style>
<div class="modal fade" id="modalrole" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">
                  <span class="assign_class label{{getKeyid('roles',$data)}}" data-id="{{getKeyid('roles',$data) }}" data-value="{{checkKey('roles',$data) }}" >
                    {{checkKey('roles',$data) }}
                  </span>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {{ Form::open(array('url' => 'roles')) }}
              @csrf
                <div class="modal-body mx-3">
                  <div class="md-form mb-5">
                    <i class="fas fa-user prefix grey-text"></i>
                    <input id="new_company" type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                  </div>
                  <div class="accordion md-accordion accordion-1" id="accordionEx23" role="tablist">
                  <div class="card">
                    <div class="card-header blue lighten-3 z-depth-1" role="tab" id="heading96">
                      <h5 class="text-uppercase mb-0 py-1">
                        <a class="white-text" data-toggle="collapse" href="#collapse96" aria-expanded="true"
                          aria-controls="collapse96">
                          Company
                        </a>
                      </h5>
                    </div>
                    <div id="collapse96" class="collapse " role="tabpanel" aria-labelledby="heading96"
                      data-parent="#accordionEx23">
                      <div class="card-body">
                        @foreach($companies as $key=>$value)
                          <div class="col-md-6 form-check">
                            <input type="radio" hidden class="form-check-input" id="create{{$value->name}}" name="company_id" value="{{$value->id}}">
                            <label for="create{{$value->name}}" class="form-check-label">{{$value->name}}
                            </label>
                          </div>
                        @endforeach()
                      </div>
                    </div>
                  </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                  <button class="btn btn-primary btn-sm" type="submit">
                    <span class="assign_class label{{getKeyid('save',$data)}}" data-id="{{getKeyid('save',$data) }}" data-value="{{checkKey('save',$data) }}" >
                      {{checkKey('save',$data) }}
                    </span>
                  </button>
                </div>
             {{ Form::close() }}
        </div>
    </div>
</div>
