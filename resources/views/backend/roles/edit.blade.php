@php
$data=localization();
@endphp
{{ Form::model($role, array('url' => array('roles/update', $role->id), 'method' => 'POST')) }}
@csrf
<div class="modal-body mx-3">
   <div class="md-form mb-5">
      <i class="fas fa-user prefix grey-text"></i>
      @if(Auth()->user()->hasPermissionTo('role_name_edit') || Auth::user()->all_companies == 1 )
        <input id="new_company" type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="{{$role->name}}" required autocomplete="name" autofocus>
        <!-- <label data-error="wrong" data-success="right" class="active" for="name"><span class="assign_class label{{getKeyid('role.model.edit.name',$data)}}" data-id="{{getKeyid('role.model.edit.name',$data) }}" data-value="{{checkKey('role.model.edit.name',$data) }}" >{{checkKey('role.model.edit.name',$data) }} </span>
           @php
                $id=getKeyid('role.model.edit.name.msg',$data);
                $value=checkKey('role.model.edit.name.msg',$data);
                $title=$id.'---'.$value;
            @endphp
            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
            data-content="{{$title}}"></i>
        </label> -->
      @else
          <input id="new_company" type="text" class="form-control" name="name" value="{{$role->name}}" disabled >
      @endif
   </div>
   
   <input id="role_id" type="hidden"  value="{{$role->id}}">
  <!--Accordion wrapper-->

    <!-- <label class="mdb-main-label"><span class="assign_class label{{getKeyid('role.model.edit.company',$data)}}" data-id="{{getKeyid('role.model.edit.company',$data) }}" data-value="{{checkKey('role.model.edit.company',$data) }}" >{!! checkKey('role.model.edit.company',$data) !!} </span></label>
        @php
            $id=getKeyid('role.model.edit.company.msg',$data);
            $value=checkKey('role.model.edit.company.msg',$data);
            $title=$id.'---'.$value;
        @endphp
        <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
        data-content="{{$title}}"></i>
      </label> -->

<div class="accordion md-accordion accordion-1" id="accordionEx23" role="tablist">
                      <div class="card">
                        <div class="card-header blue lighten-3 z-depth-1" role="tab" id="heading96">
                          <h5 class="text-uppercase mb-0 py-1">
                            <a class="white-text" data-toggle="collapse" href="#collapse96" aria-expanded="true"
                              aria-controls="collapse96">
                              Company
                            </a>
                          </h5>
                        </div>

                        <div id="collapse96" class="collapse " role="tabpanel" aria-labelledby="heading96"
                          data-parent="#accordionEx23">
                          <div class="card-body">
                             @foreach($companies as $key=>$value)
                              <div class="col-md-6 form-check">
                                <input type="radio" class="form-check-input" {{ ($role->company_id == $value->id)? 'checked' : "" }} hidden id="{{$value->name}}" name="company_id" value="{{$value->id}}">
                                <label for="{{$value->name}}" class="form-check-label">{{$value->name}}
                                </label>
                              </div>
                              @endforeach()
                          </div>
                        </div>
                      </div>

<!-- Accordion wrapper -->
   <div class="accordion md-accordion" id="accordionEx" role="tablist" aria-multiselectable="true">
      @php
      $temp = 1;
      @endphp
      @foreach ($permissions as $key_group=>$permission_group)
        <div class="accordion md-accordion accordion-1 "  id="accordionEx23{{str_slug($key_group , '-')}}" role="tablist">
          <div class="card">
            <div class="card-header blue lighten-3 z-depth-1" role="tab" id="heading{{str_slug($key_group , '-')}}">
                <a class="collapsed font-weight-bold white-text" data-toggle="collapse" href="#collapse{{str_slug($key_group , '-')}}"
                  aria-expanded="false" aria-controls="collapse{{str_slug($key_group , '-')}}">
                  <h5 class="text-uppercase mb-0 py-1">
                     {{$key_group}}
                  </h5>
                </a>
            </div>
            <div id="collapse{{str_slug($key_group , '-')}}" class="collapse" role="tabpanel" aria-labelledby="heading{{str_slug($key_group , '-')}}"
              data-parent="#accordionEx23{{str_slug($key_group , '-')}}">
              <div class="card-body">
                <div class="row my-4">
                  <div class="col-md-12">
                    @foreach ($permission_group->groupby('module_name') as $key=>$permission_data)
                      <div class="card">
                          <!-- Card header -->
                          <div class="card-header" role="tab" id="headingTwo{{str_slug($key , '-')}}">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseTwo{{str_slug($key , '-')}}"
                              aria-expanded="false" aria-controls="collapseTwo{{str_slug($key , '-')}}">
                              <h5 class="mb-0">
                               {{Form::label($key, ucfirst($key)) }} <i class="fas fa-angle-down rotate-icon"></i>
                              </h5>
                            </a>
                          </div>
                          <!-- Card body -->
                          <div id="collapseTwo{{str_slug($key , '-')}}" class="collapse" role="tabpanel" aria-labelledby="headingTwo{{str_slug($key , '-')}}"
                            data-parent="#accordionEx">
                            <div class="card-body">
                               <div class="panel panel-info">
                                  <div class="panel-body">
                                     @php
                                        $flag=0;
                                     @endphp
                                     @foreach ($permission_data->groupby('sub_module_name') as $key_permission=> $permission)
                                        @if($key_permission == 'operation')
                                           <h4 class="text-center">{{ucfirst($key_permission)}}</h4>
                                           <div class="row col-md-12">
                                              @foreach($permission as $index)
                                                 <div class="col-md-4">
                                                    <div class="card">
                                                      <div class="card-body">
                                                        @if(Auth()->user()->hasPermissionTo('operation_roles') || Auth::user()->all_companies == 1 )
                                                          {{Form::checkbox('permissions[]',  $index->id,$role->permissions,['class'=>'form-check-input permisionChange','id'=>'role'.$index->id,'hidden'] ) }}
                                                          {{Form::label('role'.$index->id, ucfirst($index->permission_name),['class'=>'form-check-label']) }}
                                                        @else
                                                           {{Form::checkbox('permissions[]',  $index->id,$role->permissions,['class'=>'form-check-input permisionChange','id'=>'role'.$index->id,'hidden','disabled' => 'disabled'] ) }}
                                                          {{Form::label('role'.$index->id, ucfirst($index->permission_name),['class'=>'form-check-label']) }}
                                                        @endif
                                                      </div>
                                                    </div>
                                                 </div>
                                              @endforeach
                                           </div>
                                        @else
                                         @php
                                           $flag++;
                                         @endphp
                                         @if($flag == 1)
                                           <h4 class="text-center">{{ucfirst($key_permission)}}</h4>
                                         @endif
                                          <div class="card">
                                              <div class="card-body">
                                                <div class="row col-md-12">
                                                  @foreach($permission->groupby('type') as $key_index=> $index)
                                                      <div class="col-md-2">
                                                            <p class="card-title card-title-custom">{{ucfirst($key_index)}}</p>
                                                            @foreach($index as $listing_index)
                                                            <div class="row custom-height">
                                                               <div class="col-md-2">
                                                                @if($listing_index->enable == 1)
                                                                  @if(Auth()->user()->hasPermissionTo('listing_roles') || Auth::user()->all_companies == 1 )
                                                                    {{Form::checkbox('permissions[]',  $listing_index->id,$role->permissions,['class'=>'form-check-input permisionChange','id'=>'role'.$listing_index->id,'hidden'] ) }}
                                                                    {{Form::label('role'.$listing_index->id, " ", ['class'=>'form-check-label']) }}
                                                                  @else
                                                                    {{Form::checkbox('permissions[]',  $listing_index->id,$role->permissions,['class'=>'form-check-input permisionChange','id'=>'role'.$listing_index->id,'hidden','disabled' => 'disabled'] ) }}
                                                                    {{Form::label('role'.$listing_index->id, " ", ['class'=>'form-check-label']) }}
                                                                  @endif
                                                                @endif
                                                                </div>
                                                                     @if($key_index == 'edit')
                                                                      @php
                                                                        $id=getKeyid($listing_index->permission_name,$data);
                                                                        $value=checkKey($listing_index->permission_name,$data);
                                                                        $variable='<span class="role_font assign_class label'.$id.'" data-id="'.$id.'" data-value="'.$value.'" >'.$value.'
                                                                        </span>';
                                                                      @endphp
                                                                        <div class="col-md-8">
                                                                          <span class="role_font">
                                                                          {{$listing_index->permission_name}}
                                                                        </span>
                                                                        <!--   {!! printHtml($variable) !!} -->
                                                                        </div>
                                                                    @else
                                                                      @endif
                                                            </div>
                                                          @endforeach
                                                      </div>
                                                  @endforeach
                                                </div>
                                               </div>
                                          </div>
                                        @endif
                                     <br>
                                     @endforeach
                                  </div>
                               </div>
                               <br>
                            </div>
                          </div>
                      </div>
                    @php
                    $temp++;
                    @endphp
                    @endforeach
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      @endforeach
   </div>

</div>
<div class="modal-footer d-flex justify-content-center">
   <button class="btn btn-primary btn-sm" type="submit"><span class="assign_class label{{getKeyid('role.model.edit.update',$data)}}" data-id="{{getKeyid('role.model.edit.update',$data) }}" data-value="{{checkKey('role.model.edit.update',$data) }}" >{{checkKey('role.model.edit.update',$data) }} </span></button>
</div>
{{ Form::close() }}
