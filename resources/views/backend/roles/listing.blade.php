<div class="col-md-12">
  <h4 class="text-center">Operation</h4>
</div>
@foreach ($permissionsArray as $key_permission=> $index)
  @php
    $exist=rolePermissionExist($role,$index);
  @endphp
   <div class="col-md-4">
      <div class="card">
        <div class="card-body">
          @if(Auth()->user()->hasPermissionTo('operation_roles') || Auth::user()->all_companies == 1 )
          <input class="form-check-input permisionChange" id="role{{$index->id}}" hidden="" {{$exist}} name="permissions[]" type="checkbox" value="{{$index->id}}">
          @else
            <input class="form-check-input permisionChange" id="role{{$index->id}}" hidden="" {{$exist}} name="permissions[]" disabled="disabled" type="checkbox" value="{{$index->id}}">
          @endif
          <label for="role{{$index->id}}" class="form-check-label">
            {{ucfirst($index->permission_name)}}
          </label>
        </div>
      </div>
   </div>
@endforeach
@if($permissionsArrayListing->count()>0)
  <div class="col-md-12 mt-5">
    <div class="card">
      <div class="card-body">
        <div class="row col-md-12 text-center">
          <h4 class="col-md-12">Listing</h4>
        </div>
        <div id="table" class="table-editable table-responsive">
          <table class=" table table-striped table-bordered" id="tablecompletedlists" cellspacing="0" width="100%">
            <tr>
              <th></th>
              <th>View</th>
              <th>Create</th>
              <th>Word Export</th>
              <th>Excel Export</th>
              <th>Pdf Export</th>
              <th>Edit</th>
            </tr>
            @foreach ($permissionsArrayListing as $key_permission=> $index)
              <tr>
                <td>{{($key_permission)}}</td>
                @foreach ($index as $listing_index)
                  @php
                    $exist=rolePermissionExist($role,$listing_index);
                  @endphp
                  <td>
                    @if($listing_index->enable == 1)
                      @if(Auth()->user()->hasPermissionTo('listing_roles') || Auth::user()->all_companies == 1 )
                        <input class="form-check-input permisionChange" id="role{{$listing_index->id}}" hidden="" {{$exist}} name="permissions[]" type="checkbox" value="{{$listing_index->id}}">
                        @else
                        <input class="form-check-input permisionChange" id="role{{$listing_index->id}}" hidden="" {{$exist}} name="permissions[]" disabled="disabled" type="checkbox" value="{{$listing_index->id}}">
                      @endif
                      <label for="role{{$listing_index->id}}" class="form-check-label">
                      </label>
                    @endif
                  </td>
                @endforeach()
              </tr>
            @endforeach()
          </table>
        </div>
      </div>
    </div>
  </div>
@endif
@if($permissionsArrayForm->count()>0)
 <div class="col-md-12 mt-5">
    <div class="card">
      <div class="card-body">
        <h4 class="text-center">Form</h4>
        <div class="row col-md-12">
          @foreach ($permissionsArrayForm as $key_permission_question=> $index)
            @php
              $exist=rolePermissionExist($role,$index);
            @endphp
             <div class="row col-md-4">
                <div class="card">
                  <div class="card-body">
                    @if(Auth()->user()->hasPermissionTo('operation_roles') || Auth::user()->all_companies == 1 )
                    <input class="form-check-input permisionChange" id="role{{$index->id}}" hidden="" {{$exist}} name="permissions[]" type="checkbox" value="{{$index->id}}">
                    @else
                      <input class="form-check-input permisionChange" id="role{{$index->id}}" hidden="" {{$exist}} name="permissions[]" disabled="disabled" type="checkbox" value="{{$index->id}}">
                    @endif
                    <label for="role{{$index->id}}" class="form-check-label">
                      {{ucfirst($index->permission_name)}}
                    </label>
                  </div>
                </div>
             </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
@endif 
@if($permissionsArrayQuestion->count()>0)
 <div class="col-md-12 mt-5">
    <div class="card">
      <div class="card-body">
        <h4 class="text-center">Question</h4>
        <div class="row col-md-12">
          @foreach ($permissionsArrayQuestion as $key_permission_question=> $index)
            @php
              $exist=rolePermissionExist($role,$index);
            @endphp
             <div class="col-md-4">
                <div class="card">
                  <div class="card-body">
                    @if(Auth()->user()->hasPermissionTo('operation_roles') || Auth::user()->all_companies == 1 )
                    <input class="form-check-input permisionChange" id="role{{$index->id}}" hidden="" {{$exist}} name="permissions[]" type="checkbox" value="{{$index->id}}">
                    @else
                      <input class="form-check-input permisionChange" id="role{{$index->id}}" hidden="" {{$exist}} name="permissions[]" disabled="disabled" type="checkbox" value="{{$index->id}}">
                    @endif
                    <label for="role{{$index->id}}" class="form-check-label">
                      {{ucfirst($index->permission_name)}}
                    </label>
                  </div>
                </div>
             </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
@endif 


              
                             