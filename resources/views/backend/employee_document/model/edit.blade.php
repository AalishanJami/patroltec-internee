@php
    $data=localization();
@endphp
<style>
    #bulk_upload_img_loader{
        position: absolute;
        z-index: 1000;
        top: 19%;
        left: 41%;
        width: 150px;
        height: 150px;
        display: none;
    }
</style>
<div class="modal fade" id="modelemployeedocumentEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">
                    <span class="assign_class label{{getKeyid('documents',$data)}}" data-id="{{getKeyid('documents',$data) }}" data-value="{{checkKey('documents',$data) }}" >
                        {!! checkKey('documents',$data) !!}
                    </span>
                    @if(Auth()->user()->hasPermissionTo('view_cms') || Auth::user()->all_companies == 1 )
                        <button data-class="edit-disable" class="edit_cms_enable edit_cms_switch btn btn-primary btn-sm waves-effect waves-light" style="display: none;" >
                            Edit cms
                        </button>
                        <button  data-class="edit-enable" class="edit_cms_disable edit_cms_switch btn btn-danger btn-sm waves-effect waves-light">
                            Edit cms
                        </button>
                    @endif
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="employee_document_form_edit"  method="POST" action="{{url('user/document/update')}}" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="company_id" class="company_id">
                <input type="hidden" name="id" id="id_employee_document">
                <input type="hidden" name="link_table" value="employee">
                <input type="hidden" name="link_id" value="{{$link_id}}">
                <div class="modal-body">
                    <div class="card-body px-lg-5 pt-0">
                        <div class="form-row">
                            @if(Auth()->user()->hasPermissionTo('employee_document_name_edit')|| Auth::user()->all_companies == 1 )
                                <div class="col">
                                    <div class="md-form">
                                        <i class="fas fa-user prefix grey-text"></i>
                                        <input type="text" class="form-control" id="name_employee_document" name="name" value="{{ old('name') }}">
                                        <label data-error="wrong" data-success="right" for="name" class="active">
                                            <span class="assign_class label{{getKeyid('title',$data)}}" data-id="{{getKeyid('title',$data) }}" data-value="{{checkKey('title',$data) }}" >
                                                {!! checkKey('title',$data) !!}
                                            </span>
                                        </label>
                                        <small class="text-danger title_required_message" style="left: 8.4% !important;position:absolute;" id="form_name_required_message_edit"></small>
                                    </div>
                                </div>
                            @endif
                            @if(Auth()->user()->hasPermissionTo('employee_document_type_edit') || Auth::user()->all_companies == 1 )
                                <div class="col-0 ml-3">
                                    <i class="fas fa-keyboard prefix grey-text fa-2x" style="margin-top:32px;font-size: 28px;"></i>
                                </div>
                                <div class="col">
                                    <div class="md-form">
                                        <label data-error="wrong" data-success="right" for="name" class="active">
                                            <span class="assign_class label{{getKeyid('doc_type',$data)}}" data-id="{{getKeyid('doc_type',$data) }}" data-value="{{checkKey('doc_type',$data) }}" >
                                                {!! checkKey('doc_type',$data) !!}
                                            </span>
                                        </label>
                                        <select searchable="Search here.." class="mdb-select" name="doc_type_id" id="doc_type_id_employee_document">>
                                            <option value="" disabled>Please Select</option>
                                            @foreach($doc_types as $key =>$value)
                                                @if(isset($value->id))
                                                    <option value="{{$value->id}}">
                                                        {{$value->name}}
                                                        @if(isset($position_id))
                                                            @if(!empty($value->user_doc_type->position_id))
                                                               @if (in_array($value->id, $user_doc_type))
                                                                    {{$match}}
                                                                @endif
                                                            @endif
                                                        @endif
                                                    </option>
                                                @endif
                                            @endforeach
                                        </select>
                                        <small class="text-danger title_required_message" style="left: 8.4% !important;" id="form_doctype_required_message_edit"></small>

                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="form-row">
                            @if(Auth()->user()->hasPermissionTo('employee_document_issue_date_edit') || Auth::user()->all_companies == 1 )
                                <div class="col">
                                    <div class="md-form">
                                        <i class="far fa-calendar-check prefix grey-text"></i>
                                        {!! Form::text('issue',Carbon\Carbon::parse()->format('jS M Y'), ['class' => 'form-control datepicker issue_date']) !!}
                                        <label data-error="wrong" data-success="right" for="issue" class="active">
                                            <span class="assign_class label{{getKeyid('issue_date',$data)}}" data-id="{{getKeyid('issue_date',$data) }}" data-value="{{checkKey('issue_date',$data) }}" >
                                                {!! checkKey('issue_date',$data) !!}
                                            </span>
                                        </label>
                                    </div>
                                </div>
                            @endif
                            @if(Auth()->user()->hasPermissionTo('employee_document_expiry_date_edit') || Auth::user()->all_companies == 1 )
                                <div class="col">
                                    <div class="md-form">
                                        <i class="far fa-calendar-times prefix grey-text"></i>
                                        {!! Form::text('expiry',Carbon\Carbon::parse()->format('jS M Y'), ['class' => 'form-control datepicker expiry_date']) !!}
                                        <label data-error="wrong" data-success="right" for="expiry" class="active">
                                            <span class="assign_class label{{getKeyid('expiry_date',$data)}}" data-id="{{getKeyid('expiry_date',$data) }}" data-value="{{checkKey('expiry_date',$data) }}" >
                                                {!! checkKey('expiry_date',$data) !!}
                                            </span>
                                        </label>
                                    </div>
                                </div>
                        </div>
                        @endif
                    </div>
                    @if(Auth()->user()->hasPermissionTo('employee_document_file_edit') || Auth::user()->all_companies == 1 )
                        <input type="hidden" value="{{url('/')}}" id="edit_base_url">
                        <div class="file-upload-wrapper file_upload_wrapper_edit "></div>
                        <small class="text-danger title_required_message" style="left: 8.4% !important;" id="form_file_required_message_edit"></small>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('employee_document_notes_edit') || Auth::user()->all_companies == 1 )
                        <div class="md-form mb-5" id="note_edit">
                            <div class="summernote_inner body_notes body_notes_edit" ></div>
                        </div>
                        <input type="hidden" name="notes" id="note_value_edit">
                    @endif
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button class="btn btn-primary btn-sm" type="submit">
                        <span class="assign_class label{{getKeyid('update',$data)}}" data-id="{{getKeyid('update',$data) }}" data-value="{{checkKey('update',$data) }}" >
                            {!! checkKey('update',$data) !!}
                        </span>
                    </button>
                </div>
        </div>
        </form>
    </div>
</div>
</div>
<script type="text/javascript">
    $("#employee_document_form_edit").on('submit',function (event) {
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        event.preventDefault();
        var company_id=$('#companies_selected_nav').children("option:selected").val();
        $("#bulk_upload_img_loader").show();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var notes=$('#note_edit').find( '.note-editable, .card-block p' ).html();
        $('#note_value_edit').val(notes);
        $('.company_id').val(company_id);
        $('#document_form_create').submit();
        var formData = $("#employee_document_form_edit").serializeObject();
        $.ajax({
            type: 'POST',
            url: '/user/document/update',
            data:new FormData(this),
            contentType:false,
            cache:false,
            processData:false,
            success: function(response)
            {

                if(response.error)
                {
                    $("#bulk_upload_img_loader").hide();
                    if (response.error.name){
                        $("#form_name_required_message_edit").html(response.error.name);
                        $("#name_employee_document").attr('style','border-bottom:1px solid #e3342f;');
                    }
                    if (response.error.doc_type_id){
                        $("#form_doctype_required_message_edit").html(response.error.doc_type_id);
                        $("[data-activates=select-options-doc_type_id_employee_document]").attr('style','border-bottom:1px solid #e3342f;');
                    }
                    if (response.error.file){
                        $("#form_file_required_message_edit").html(response.error.file);
                    }
                    return 0;
                }
                $("#bulk_upload_img_loader").show();
                setTimeout(function(){
                    toastr["success"](response.message);
                    $("#form_name_required_message").html('');
                    $("#name_field").val('');
                    $("#bulk_upload_img_loader").hide();
                    $('#employee_document_table').DataTable().clear().destroy();
                    var url='/user/document/getall';
                    employee_document_data(url);
                    $('#modelemployeedocumentEdit').modal('hide');
                },1000);

            },
            error: function (error) {
                console.log(error);
            }
        });

    });

</script>
