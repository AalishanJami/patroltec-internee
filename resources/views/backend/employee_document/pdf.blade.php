@include('backend.layouts.pdf_start')
    <thead>
      <tr>
        @if(Auth()->user()->hasPermissionTo('employee_document_name_excel_export') || Auth::user()->all_companies == 1 )
          <th style="vertical-align: text-top;" class="pdf_table_layout"> Name</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('employee_document_file_excel_export') || Auth::user()->all_companies == 1 )
          <th style="vertical-align: text-top;" class="pdf_table_layout"> File</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('employee_document_expiry_date_excel_export') || Auth::user()->all_companies == 1 )
          <th style="vertical-align: text-top;" class="pdf_table_layout"> Expiry</th>
        @endif
      </tr>
    </thead>
    <tbody>
    @foreach($data as $row)
      <tr>
        @if(Auth()->user()->hasPermissionTo('employee_document_name_excel_export') || Auth::user()->all_companies == 1 )
          <td class="pdf_table_layout"><p class="col_text_style">{{ $row->name }}</p></td>
        @endif
        @if(Auth()->user()->hasPermissionTo('employee_document_file_excel_export') || Auth::user()->all_companies == 1 )     
          <td class="pdf_table_layout"><p class="col_text_style">{{ $row->file }}</p></td>
        @endif
        @if(Auth()->user()->hasPermissionTo('employee_document_expiry_date_excel_export') || Auth::user()->all_companies == 1 )
          <td class="pdf_table_layout"><p class="col_text_style">{{ $row->expiry }}</p></td>
        @endif   
      </tr>
    @endforeach
    </tbody>
@include('backend.layouts.pdf_end')