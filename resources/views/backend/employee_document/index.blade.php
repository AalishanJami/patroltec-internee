@extends('backend.layouts.user')
@section('title', 'Documents')
@section('headscript')
    <link rel="stylesheet" href="{{ asset('custom/css/custom_style.css') }}">
@stop
@section('content')
    @php
        $data=localization();
    @endphp
    @include('backend.layouts.user_sidebar')
    <div class="padding-left-custom remove-padding">
        {!! breadcrumInner('documents',Session::get('employee_id'),'users','/employee/detail/'.Session::get('employee_id'),'employees','/employee') !!}
        <div class="card">
            <div class="card-body">
                @if(Auth()->user()->hasPermissionTo('create_employee_document') || Auth::user()->all_companies == 1 )
                    <span class="employee_document_add table-add float-right mb-3 mr-2">
                        <a class="text-success modelemployeedocument"  data-toggle="modal" data-target="#modelemployeedocument">
                            <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
                        </a>
                    </span>
                @endif
                @if(Auth()->user()->hasPermissionTo('selected_delete_employee_document') || Auth::user()->all_companies == 1 )
                    <button class="btn btn-danger btn-sm" id="deleteselectedemployee_document">
                            <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                                {!! checkKey('selected_delete',$data) !!}
                            </span>
                    </button>
                    <button class="btn btn-danger btn-sm" style="display: none;" id="deleteselectedemployee_documentSoft">
                            <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                                {!! checkKey('selected_delete',$data) !!}
                            </span>
                    </button>
                @endif
                @if(Auth()->user()->hasPermissionTo('selected_active_button_employee_document') || Auth::user()->all_companies == 1 )
                    <button class="btn btn-success btn-sm" style="display: none;" id="selectedactivebuttonemployee_document">
                            <span class="assign_class label{{getKeyid('selected_active',$data)}}" data-id="{{getKeyid('selected_active',$data) }}" data-value="{{checkKey('selected_active',$data) }}" >
                                {!! checkKey('selected_active',$data) !!}
                            </span>
                    </button>
                @endif
                @if(Auth()->user()->hasPermissionTo('selected_restore_delete_employee_document') || Auth::user()->all_companies == 1 )
                    <button class="btn btn-primary btn-sm" id="restorebuttonemployee_document">
                            <span class="assign_class label{{getKeyid('restore',$data)}}" data-id="{{getKeyid('restore',$data) }}" data-value="{{checkKey('restore',$data) }}" >
                                {!! checkKey('restore',$data) !!}
                            </span>
                    </button>
                @endif
                @if(Auth()->user()->hasPermissionTo('active_employee_document') || Auth::user()->all_companies == 1 )
                    <button  style="display: none;" class="btn btn-primary btn-sm" id="activebuttonemployee_document">
                            <span class="assign_class label{{getKeyid('show_active',$data)}}" data-id="{{getKeyid('show_active',$data) }}" data-value="{{checkKey('show_active',$data) }}" >
                                {!! checkKey('show_active',$data) !!}
                            </span>
                    </button>
                @endif
                @if(Auth()->user()->hasPermissionTo('csv_employee_document') || Auth::user()->all_companies == 1 )
                    <form class="form-style" method="POST" action="{{ url('/user/document/export/excel') }}">
                        <input type="hidden" id="export_excel_employee_document" name="export_excel_employee_document" value="1">
                        <input type="hidden" class="employee_document_export" name="excel_array" value="1">
                        {!! csrf_field() !!}
                        <button  type="submit" id="export_excel_employee_document_btn" class="form_submit_check btn btn-warning btn-sm">
                            <span class="assign_class label{{getKeyid('excel_export',$data)}}" data-id="{{getKeyid('excel_export',$data) }}" data-value="{{checkKey('excel_export',$data) }}" >
                                {!! checkKey('excel_export',$data) !!}
                            </span>
                        </button>
                    </form>
                @endif
                @if(Auth()->user()->hasPermissionTo('word_employee_document') || Auth::user()->all_companies == 1 )
                    <form class="form-style" method="POST" action="{{ url('/user/document/export/world') }}">
                        <input type="hidden" id="export_world_employee_document" name="export_world_employee_document" value="1">
                        <input type="hidden" class="employee_document_export" name="word_array" value="1">
                        {!! csrf_field() !!}
                        <button  type="submit" id="export_world_employee_document_btn" class="form_submit_check btn btn-success btn-sm">
                            <span class="assign_class label{{getKeyid('word_export',$data)}}" data-id="{{getKeyid('word_export',$data) }}" data-value="{{checkKey('word_export',$data) }}" >
                                {!! checkKey('word_export',$data) !!}
                            </span>
                        </button>
                    </form>
                @endif
                @if(Auth()->user()->hasPermissionTo('pdf_employee_document') || Auth::user()->all_companies == 1 )
                    <form  class="form-style"  {{pdf_view('documents')}} method="POST" action="{{ url('/user/document/export/pdf') }}">
                        <input type="hidden" id="export_pdf_employee_document" name="export_world_pdf" value="1">
                        <input type="hidden" class="employee_document_export" name="pdf_array" value="1">
                        {!! csrf_field() !!}
                        <button  type="submit" id="export_pdf_employee_document_btn"  class="form_submit_check btn btn-secondary btn-sm">
                            <span class="assign_class label{{getKeyid('pdf_export',$data)}}" data-id="{{getKeyid('pdf_export',$data) }}" data-value="{{checkKey('pdf_export',$data) }}" >
                                {!! checkKey('pdf_export',$data) !!}
                            </span>
                        </button>
                    </form>
                    @endif
                    </br>
                    <button type="button" class="btn btn-primary btn-sm">
                       <span class="assign_class label{{getKeyid('active',$data)}}" data-id="{{getKeyid('active',$data) }}" data-value="{{checkKey('active',$data) }}" >
                            {!! checkKey('active',$data) !!}
                        </span>
                        <span class="badge badge-default ml-2 active_badge">0</span>
                    </button>
                    <button type="button" class="btn btn-danger btn-sm">
                        <span class="assign_class label{{getKeyid('expiry',$data)}}" data-id="{{getKeyid('expiry',$data) }}" data-value="{{checkKey('expiry',$data) }}" >
                            {!! checkKey('expiry',$data) !!}
                        </span>
                        <span class="badge badge-default ml-2 expiry_badge">0</span>
                    </button>
                    <button type="button" class="btn btn-warning btn-sm">
                        {{isset($user->positionData->name)? $user->positionData->name : ''}}
                    </button>
                    <div id="table" class="table-editable table-responsive">
                        <table id="employee_document_table" class=" table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th class="no-sort all_checkboxes_style">
                                    @if(Auth()->user()->hasPermissionTo('employee_document_checkbox') || Auth::user()->all_companies == 1 )
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input" id="employee_document_checkbox_all">
                                            <label class="form-check-label" for="employee_document_checkbox_all">
                                                    <span class="assign_class label{{getKeyid('all',$data)}}" data-id="{{getKeyid('all',$data) }}" data-value="{{checkKey('all',$data) }}" >
                                                        {!! checkKey('all',$data) !!}
                                                    </span>
                                            </label>
                                        </div>
                                    @endif
                                </th>
                                <th>
                                    @if(Auth()->user()->hasPermissionTo('employee_document_name') || Auth::user()->all_companies == 1 )
                                        <span class="assign_class label{{getKeyid('user_document_document_name',$data)}}" data-id="{{getKeyid('name',$data) }}" data-value="{{checkKey('name',$data) }}" >
                                                {!! checkKey('name',$data) !!}
                                            </span>
                                    @endif
                                </th>
                                <th>
                                    @if(Auth()->user()->hasPermissionTo('employee_document_file') || Auth::user()->all_companies == 1 )
                                        <span class="assign_class label{{getKeyid('file',$data)}}" data-id="{{getKeyid('file',$data) }}" data-value="{{checkKey('file',$data) }}" >
                                                {!! checkKey('file',$data) !!}
                                            </span>
                                    @endif
                                </th>
                                <th>
                                    <span class="assign_class label{{getKeyid('doc_type',$data)}}" data-id="{{getKeyid('doc_type',$data) }}" data-value="{{checkKey('doc_type',$data) }}" >
                                        {!! checkKey('doc_type',$data) !!}
                                    </span>
                                </th>
                                <th>
                                    @if(Auth()->user()->hasPermissionTo('employee_document_expiry_date') || Auth::user()->all_companies == 1 )
                                        <span class="assign_class label{{getKeyid('expiry_date',$data)}}" data-id="{{getKeyid('expiry_date',$data) }}" data-value="{{checkKey('expiry_date',$data) }}" >
                                                {!! checkKey('expiry_date',$data) !!}
                                            </span>
                                    @endif
                                </th>
                                <th class="no-sort all_document_action_style"></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
            </div>
        </div>
    </div>
    @include('backend.label.input_label')
    @include('backend.employee_document.model.create')
    @include('backend.employee_document.model.edit')
    @if(Auth()->user()->all_companies == 1)
        <input type="hidden" id="checkbox_permission" value="1">
        <input type="hidden" id="name_permission" value="1">
        <input type="hidden" id="file_permission" value="1">
        <input type="hidden" id="expiry_permission" value="1">
    @else
        <input type="hidden" id="checkbox_permission" value="{{Auth()->user()->hasPermissionTo('employee_document_checkbox')}}">
        <input type="hidden" id="name_permission" value="{{Auth()->user()->hasPermissionTo('employee_document_name')}}">
        <input type="hidden" id="file_permission" value="{{Auth()->user()->hasPermissionTo('employee_document_file')}}">
        <input type="hidden" id="expiry_permission" value="{{Auth()->user()->hasPermissionTo('employee_document_expiry_date')}}">
    @endif

    <script type="text/javascript" src="{{ asset('custom/js/employee_document.js') }}" ></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.file_upload_create').file_upload();
            $('.datepicker').pickadate({selectYears:250,});
            $(function () {
                var url='/user/document/getall';
                employee_document_data(url);
            });
            $('.modelemployeedocument').click(function() {
                $('.body_notes').summernote('code', '');
                $('.file-upload>.btn,.btn-sm .btn-danger')[0].click();
                $("#employee_document_form_create").trigger("reset");
            });
            $('#employee_document_checkbox_all').click(function() {
                if ($(this).is(':checked')) {
                    $('.employee_document_checked').attr('checked', true);
                } else {
                    $('.employee_document_checked').attr('checked', false);
                }
            });
        });
        function employee_document_data(url)
        {
            $.ajax({
                type: 'GET',
                url: url,
                success: function(response)
                {
                    $('#employee_document_table').DataTable().clear().destroy();
                    var table = $('#employee_document_table').dataTable({
                        processing: true,
                        bInfo:false,
                        language: {
                            'lengthMenu': '<span class="assign_class label'+$('#show_label').attr('data-id')+'" data-id="'+$('#show_label').attr('data-id')+'" data-value="'+$('#show_label').val()+'" ></span>  _MENU_ <span class="assign_class label'+$('#entries_label').attr('data-id')+'" data-id="'+$('#entries_label').attr('data-id')+'" data-value="'+$('#entries_label').val()+'" ></span>',
                            'search':'<span class="assign_class label'+$('#search_id').attr('data-id')+'" data-id="'+$('#search_id').attr('data-id')+'" data-value="'+$('#search_id').val()+'" >'+$('#search_id').val()+'</span>',
                            'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                            'info': '<span class="assign_class label'+$('#showing_page_label').attr('data-id')+'" data-id="'+$('#showing_page_label').attr('data-id')+'" data-value="'+$('#showing_page_label').val()+'" >'+$('#showing_page_label').val()+'</span> _PAGE_ <span class="assign_class label'+$('#of_label').attr('data-id')+'" data-id="'+$('#of_label').attr('data-id')+'" data-value="'+$('#of_label').val()+'" >'+$('#of_label').val()+'</span> _PAGES_',
                            'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                            'paginate': {
                                'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                                'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                            },
                            'infoFiltered': " "
                        },
                        "ajax": {
                            "url": url,
                            "type": 'get',
                        },
                        "createdRow": function( row, data, dataIndex,columns )
                        {
                            $('.active_badge').html(response.count_document.count_active);
                            $('.expiry_badge').html(response.count_document.count_expiry);
                            var checkbox='<div class="form-check"><input type="checkbox" class="form-check-input employee_document_checked " id="employee_document'+data.id+'"><label class="form-check-label" for="employee_document'+data.id+'""></label></div>';
                            $(columns[0]).html(checkbox);
                            $(row).attr('id', 'employee_document_tr'+data['id']);
                            $(row).attr('data-id',data['id']);
                            if(data['enable'] == 1)
                            {
                                $(row).attr('class', 'selectedrowemployee_document expired');
                            }
                            else
                            {
                                $(row).attr('class', 'selectedrowemployee_document');
                            }
                        },
                        columns: [
                            {data: 'checkbox', name: 'checkbox',visible:$('#checkbox_permission').val()},
                            {data: 'name', name: 'name',visible:$('#name_permission').val()},
                            {data: 'file', name: 'file',visible:$('#file_permission').val()},
                            {data: 'doc_type', name: 'doc_type'},
                            {data: 'expiry', name: 'expiry',visible:$('#expiry_permission').val()},
                            {data: 'actions', name: 'actions'},
                        ],

                    });
                },
                error: function (error) {
                    console.log(error);
                }
            });
            // (function ($) {
            //     var active ='<span class="assign_class label'+$('#breadcrumb_user_document').attr('data-id')+'" data-id="'+$('#breadcrumb_user_document').attr('data-id')+'" data-value="'+$('#breadcrumb_user_document').val()+'" >'+$('#breadcrumb_user_document').val()+'</span>';
            //     $('.breadcrumb-item.active').html(active);
            //     var parent ='<span class="assign_class label'+$('#dashboard_label').attr('data-id')+'" data-id="'+$('#dashboard_label').attr('data-id')+'" data-value="'+$('#dashboard_label').val()+'" >'+$('#dashboard_label').val()+'</span>';
            //     $('.breadcrumb-item a').html(parent);
            // }(jQuery));
            if ($(":checkbox").prop('checked',true)){
                $(":checkbox").prop('checked',false);
            }
        }
    </script>
@endsection

