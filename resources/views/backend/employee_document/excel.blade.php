<table>
    <thead>
      <tr>
        @if(Auth()->user()->hasPermissionTo('employee_document_name_excel_export') || Auth::user()->all_companies == 1 )
          <th>Name</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('employee_document_file_excel_export') || Auth::user()->all_companies == 1 )
          <th>File</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('employee_document_expiry_date_excel_export') || Auth::user()->all_companies == 1 )
          <th>Expiry</th>
        @endif
      </tr>
    </thead>
    <tbody>
    @foreach($data as $row)
      <tr>
        @if(Auth()->user()->hasPermissionTo('employee_document_name_excel_export') || Auth::user()->all_companies == 1 )
          <td>{{ $row->name }}</td>
        @endif
        @if(Auth()->user()->hasPermissionTo('employee_document_file_excel_export') || Auth::user()->all_companies == 1 )     
          <td>{{ $row->file }}</td>
        @endif
        @if(Auth()->user()->hasPermissionTo('employee_document_expiry_date_excel_export') || Auth::user()->all_companies == 1 )
          <td>{{ $row->expiry }}</td>
        @endif   
      </tr>
    @endforeach
    </tbody>
</table>