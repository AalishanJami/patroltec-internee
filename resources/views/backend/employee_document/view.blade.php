@extends('backend.layouts.user')
@section('content')
@php
$data=localization();
@endphp
<style type="text/css">
  .white-color
  {
    color:#fff;
  }
  #pdf-viewer-user
  {
    height: 700px;
  }
</style>
@include('backend.layouts.user_sidebar')
<div class="custom_clear padding-left-custom remove-padding">
   {{ Breadcrumbs::render('document') }}
   <div class="custom_clear locations-form container-fluid form_body">
        <div class="custom_clear card">
          <h5 class="custom_clear card-header  t py-4 white-color">
            <strong>
              <span class="assign_class label{{getKeyid('view',$data)}}" data-id="{{getKeyid('view',$data) }}" data-value="{{checkKey('view',$data) }}" >
                  {!! checkKey('view',$data) !!} 
              </span>
            </strong>
          </h5>  
          <div id="pdf-viewer-user"></div>
        </div>
   </div>
</div>
@include('backend.label.input_label')
<script src="{{asset('js/pdfview.js') }}"></script>
<script type="text/javascript">
  var options = {
      pdfOpenParams: {toolbar: '0'}
  };
  PDFObject.embed("{{ url('/user/document/view-pdf', ['id' => $id]) }}", "#pdf-viewer-user",options);
  (function ($) {
      var active ='<span class="assign_class label'+$('#breadcrumb_user_document').attr('data-id')+'" data-id="'+$('#breadcrumb_user_document').attr('data-id')+'" data-value="'+$('#breadcrumb_user_document').val()+'" >'+$('#breadcrumb_user_document').val()+'</span>';
      $('.breadcrumb-item.active').html(active);
      var parent ='<span class="assign_class label'+$('#dashboard_label').attr('data-id')+'" data-id="'+$('#dashboard_label').attr('data-id')+'" data-value="'+$('#dashboard_label').val()+'" >'+$('#dashboard_label').val()+'</span>';
      $('.breadcrumb-item a').html(parent);
  }(jQuery));
</script>

@endsection
