 @php
        $data=localization();
@endphp


<div class="modal fade" id="modalEditForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Address Edit</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
              <form id="registernewcompany" method="POST" >
                @csrf
                <div class="modal-body mx-3">
                    <div class="md-form mb-4">
                        <div class="row">
                            <div class="col-md-12">
                                <select searchable="Search here.."  id="companies_selected" class="mdb-select md-form"   >
                                    <option  disabled selected>Address</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option> 
                                </select>
                                <label data-error="wrong" data-success="right" for="name"  class="active">
                            Address
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                            data-content="tool tip for Address"></i>
                        </label>
                            </div>
                        </div>
                    </div> 
                    

                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="Telephone Number" required autocomplete="name" autofocus>
                        <label data-error="wrong" data-success="right" for="name" class="active">
                            Telephone Number
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                            data-content="tool tip for Telephone Number"></i>
                        </label>
                    </div>
                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="Email Address" required autocomplete="name" autofocus>
                        <label data-error="wrong" data-success="right" for="name" class="active">
                            Email Address
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                            data-content="tool tip for Email Address"></i>
                        </label>
                    </div>
                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="Building Name" required autocomplete="name" autofocus>
                        <label data-error="wrong" data-success="right" for="name" class="active">
                            Building Name
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                            data-content="tool tip for Building Name"></i>
                        </label>
                    </div>
                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="Street" required autocomplete="name" autofocus>
                        <label data-error="wrong" data-success="right" for="name" class="active">
                            Street
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                            data-content="tool tip for Street"></i>
                        </label>
                    </div>
                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="Town" required autocomplete="name" autofocus>
                        <label data-error="wrong" data-success="right" for="name" class="active">
                            Town
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                            data-content="tool tip for Town"></i>
                        </label>
                    </div>
                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="State" required autocomplete="name" autofocus>
                        <label data-error="wrong" data-success="right" for="name" class="active">
                            State
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                            data-content="tool tip for State"></i>
                        </label>
                    </div>

                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="Country" required autocomplete="name" autofocus>
                        <label data-error="wrong" data-success="right" for="name" class="active">
                            Country
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                            data-content="tool tip for Country"></i>
                        </label>
                    </div> 
                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="Post Code" required autocomplete="name" autofocus>
                        <label data-error="wrong" data-success="right" for="name" class="active">
                            Post Code
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                            data-content="tool tip for Post Code"></i>
                        </label>
                    </div>

                </div>


                <div class="modal-footer d-flex justify-content-center">
                    <button class="btn btn-primary btn-sm" type="submit"><span class="assign_class label{{getKeyid('save',$data)}}" data-id="{{getKeyid('save',$data) }}" data-value="{{checkKey('save',$data) }}" >{!! checkKey('save',$data) !!} </span></button>
                </div>
            </form>

            <div class="modal-body mx-3">

           
            </div>
        </div>
    </div>
</div>
