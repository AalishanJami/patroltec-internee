@include('backend.layouts.pdf_start')
    <thead>
    <tr>
        @if(Auth()->user()->hasPermissionTo('group_name_pdf_export') || Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout"> Name</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('group_customer_pdf_export') || Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout"> Customer</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('group_division_pdf_export') || Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout"> Division</th>
        @endif
    </tr>
    </thead>
    <tbody>
    @foreach($data as $value)
        <tr>
            @if(Auth()->user()->hasPermissionTo('group_name_pdf_export') || Auth::user()->all_companies == 1 )
                <td class="pdf_table_layout"><p class="col_text_style">{{ $value->name }}</p></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('group_customer_pdf_export') || Auth::user()->all_companies == 1 )
                <td class="pdf_table_layout"><p class="col_text_style">{{ isset($value->customer->name) ? $value->customer->name : '' }}</p></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('group_division_pdf_export') || Auth::user()->all_companies == 1 )
                <td class="pdf_table_layout"><p class="col_text_style">{{ isset($value->division->name) ? $value->division->name : '' }}</p></td>
            @endif
        </tr>
    @endforeach
    </tbody>
@include('backend.layouts.pdf_end')
