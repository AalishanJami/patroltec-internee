 @php
        $data=localization();
@endphp
<div class="modal fade" id="modalSIteGroupEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Site Groups Edit</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
              <form  method="POST" >
                @csrf
                 <div class="modal-body mx-3">
                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="Name" required autocomplete="name" autofocus>
                        <label data-error="wrong" data-success="right" for="name" class="active" >
                            Name
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                            data-content="tool tip for Name"></i>
                        </label>
                    </div>
                   
                </div>

                <div class="modal-footer d-flex justify-content-center">
                    <button class="btn btn-primary btn-sm" type="submit"><span class="assign_class label{{getKeyid('save',$data)}}" data-id="{{getKeyid('save',$data) }}" data-value="{{checkKey('save',$data) }}" >{!! checkKey('save',$data) !!} </span></button>
                </div>
            </form>

            <div class="modal-body mx-3">

           
            </div>
        </div>
    </div>
</div>
