@php
    $data=localization();
@endphp
<div class="modal fade" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static" id="modalSIteGroupIndex" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">
                    <span class="assign_class label{{getKeyid('group',$data)}}" data-id="{{getKeyid('group',$data) }}" data-value="{{checkKey('group',$data) }}" >
                        {!! checkKey('group',$data) !!}
                    </span>
                    @if(Auth()->user()->hasPermissionTo('view_cms') || Auth::user()->all_companies == 1 )
                        <button data-class="edit-disable" class="edit_cms_enable edit_cms_switch btn btn-primary btn-sm waves-effect waves-light" style="display: none;" >
                            Edit cms
                        </button>
                        <button  data-class="edit-enable" class="edit_cms_disable edit_cms_switch btn btn-danger btn-sm waves-effect waves-light">
                            Edit cms
                        </button>
                    @endif
                </h4>
                <button type="button" class="close group_listing_close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="card">
                <div class="card-body">
                    <div id="table-sitegroup" class="table-editable">
                        @if(Auth()->user()->hasPermissionTo('create_group') || Auth::user()->all_companies == 1 )
                            <span class="add-sitegroup table-add float-right mb-3 mr-2">
                                <a class="text-success"><i class="fas fa-plus fa-2x" aria-hidden="true"></i></a>
                            </span>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_delete_group') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-danger btn-sm" id="deleteselectedsitegroup">
                                 <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                                    {!! checkKey('selected_delete',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_delete_group') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-danger btn-sm" style="display: none;" id="deleteselectedsitegroupSoft">
                                 <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                                    {!! checkKey('selected_delete',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_active_group') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-success btn-sm" style="display: none;" id="selectedactivebuttonsitegroup">
                                <span class="assign_class label{{getKeyid('selected_active',$data)}}" data-id="{{getKeyid('selected_active',$data) }}" data-value="{{checkKey('selected_active',$data) }}" >
                                    {!! checkKey('selected_active',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('restore_delete_group') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-primary btn-sm" id="restorebuttonsitegroup">
                                <span class="assign_class label{{getKeyid('restore',$data)}}" data-id="{{getKeyid('restore',$data) }}" data-value="{{checkKey('restore',$data) }}" >
                                    {!! checkKey('restore',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('active_group') || Auth::user()->all_companies == 1 )
                            <button  style="display: none;" class="btn btn-primary btn-sm" id="activebuttonsitegroup">
                                <span class="assign_class label{{getKeyid('show_active',$data)}}" data-id="{{getKeyid('show_active',$data) }}" data-value="{{checkKey('show_active',$data) }}" >
                                    {!! checkKey('show_active',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('csv_group') || Auth::user()->all_companies == 1 )
                        <form class="form-style" id="export_excel_sitegroup" method="POST" action="{{ url('sitegroup/export/excel') }}">
                            {!! csrf_field() !!}
                            <input type="hidden" class="sitegroup_export" name="excel_array" value="1">
                            <button  type="submit" class="form_submit_check btn btn-warning btn-sm">
                                <span class="assign_class label{{getKeyid('excel_export',$data)}}" data-id="{{getKeyid('excel_export',$data) }}" data-value="{{checkKey('excel_export',$data) }}" >
                                    {!! checkKey('excel_export',$data) !!}
                                </span>
                            </button>
                        </form>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('word_group') || Auth::user()->all_companies == 1 )
                         <form class="form-style" id="export_world_sitegroup" method="POST" action="{{ url('sitegroup/export/world') }}">
                            {!! csrf_field() !!}
                            <input type="hidden" class="sitegroup_export" name="word_array" value="1">
                            <button  type="submit" class="form_submit_check btn btn-success btn-sm">
                               <span class="assign_class label{{getKeyid('word_export',$data)}}" data-id="{{getKeyid('word_export',$data) }}" data-value="{{checkKey('word_export',$data) }}" >
                                    {!! checkKey('word_export',$data) !!}
                                </span>
                            </button>
                        </form>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('pdf_group') || Auth::user()->all_companies == 1 )
                        <form  class="form-style" {{pdf_view('site_groups')}}  id="export_pdf_sitegroup" method="POST" action="{{ url('sitegroup/export/pdf') }}">
                            {!! csrf_field() !!}
                            <input type="hidden" class="sitegroup_export" name="pdf_array" value="1">
                            <button  type="submit" class="form_submit_check btn btn-secondary btn-sm">
                                <span class="assign_class label{{getKeyid('pdf_export',$data)}}" data-id="{{getKeyid('pdf_export',$data) }}" data-value="{{checkKey('pdf_export',$data) }}" >
                                    {!! checkKey('pdf_export',$data) !!}
                                </span>
                            </button>
                        </form>
                        @endif
                        <div id="table" class="table-editable">
                            <table id="site_group_table" class="table table-bordered table-responsive-md table-striped">
                                <thead>
                                    <tr>
                                        @if(Auth()->user()->hasPermissionTo('group_checkbox') || Auth::user()->all_companies == 1 )
                                            <th class="no-sort all_checkboxes_style">
                                                <div class="form-check">
                                                    <input type="checkbox" class="form-check-input site_group_checked" id="site_group_checkbox_all">
                                                    <label class="form-check-label" for="site_group_checkbox_all">
                                                        <span class="assign_class label{{getKeyid('all',$data)}}" data-id="{{getKeyid('all',$data) }}" data-value="{{checkKey('all',$data) }}" >
                                                            {!! checkKey('all',$data) !!}
                                                        </span>
                                                    </label>
                                                </div>
                                            </th>
                                        @endif
                                        @if(Auth()->user()->hasPermissionTo('group_name') || Auth::user()->all_companies == 1 )
                                            <th class="all_name_style">
                                                <span class="assign_class label{{getKeyid('name',$data)}}" data-id="{{getKeyid('name',$data) }}" data-value="{{checkKey('name',$data) }}" >
                                                    {!! checkKey('name',$data) !!}
                                                </span>
                                            </th>
                                        @endif
                                        <!-- @if(Auth()->user()->hasPermissionTo('group_division') || Auth::user()->all_companies == 1 )
                                            <th> Devision</th>
                                        @endif
                                        @if(Auth()->user()->hasPermissionTo('group_customer') || Auth::user()->all_companies == 1 )
                                            <th> Customer</th>
                                        @endif -->
                                        @if(Auth()->user()->hasPermissionTo('edit_group') || Auth::user()->all_companies == 1 )
                                            <th class="no-sort all_action_btn" id="action"></th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        @if(Auth()->user()->all_companies == 1)
                            <input type="hidden" id="group_checkbox" value="1">
                            <input type="hidden" id="edit_group" value="1">
                            <input type="hidden" id="delete_group" value="1">
                            <input type="hidden" id="hard_delete_group" value="1">
                            <input type="hidden" id="group_name" value="1">
                            <input type="hidden" id="group_division" value="1">
                            <input type="hidden" id="group_customer" value="1">
                            <input type="hidden" id="group_name_edit" value="1">
                            <input type="hidden" id="group_division_edit" value="1">
                            <input type="hidden" id="group_customer_edit" value="1">
                            <input type="hidden" id="group_name_create" value="1">
                            <input type="hidden" id="group_division_create" value="1">
                            <input type="hidden" id="group_customer_create" value="1">
                        @else
                            <input type="hidden" id="group_checkbox" value="{{Auth()->user()->hasPermissionTo('group_checkbox')}}">
                            <input type="hidden" id="edit_group" value="{{Auth()->user()->hasPermissionTo('edit_group')}}">
                            <input type="hidden" id="delete_group" value="{{Auth()->user()->hasPermissionTo('delete_group')}}">
                            <input type="hidden" id="hard_delete_customer" value="{{Auth()->user()->hasPermissionTo('hard_delete_customer')}}">
                            <input type="hidden" id="group_name" value="{{Auth()->user()->hasPermissionTo('group_name')}}">
                            <input type="hidden" id="group_division" value="{{Auth()->user()->hasPermissionTo('group_division')}}">
                            <input type="hidden" id="group_customer" value="{{Auth()->user()->hasPermissionTo('group_customer')}}">

                            <input type="hidden" id="group_name_edit" value="{{Auth()->user()->hasPermissionTo('group_name_edit')}}">
                            <input type="hidden" id="group_division_edit" value="{{Auth()->user()->hasPermissionTo('group_division_edit')}}">
                            <input type="hidden" id="group_customer_edit" value="{{Auth()->user()->hasPermissionTo('group_customer_edit')}}">

                            <input type="hidden" id="group_name_create" value="{{Auth()->user()->hasPermissionTo('group_name_create')}}">
                            <input type="hidden" id="group_division_create" value="{{Auth()->user()->hasPermissionTo('group_division_create')}}">
                            <input type="hidden" id="group_customer_create" value="{{Auth()->user()->hasPermissionTo('group_customer_create')}}">
                        @endif
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<script type="text/javascript">

    function checkSelected(value,checkValue)
    {
        if(value == checkValue)
        {
            return 'selected';
        }
        else
        {
            return "";
        }

    }
    function sitegroup_data(url)
    {
        var division_id=$('#division_id').children("option:selected").val();
        var customer_id=$('#customer_id').children("option:selected").val();
        var table = $('#site_group_table').dataTable(
                    {
                        processing: true,
                        language: {
                            'lengthMenu': '  _MENU_ ',
                            'search':'<span class="assign_class label'+$('#search_id').attr('data-id')+'" data-id="'+$('#search_id').attr('data-id')+'" data-value="'+$('#search_id').val()+'" >'+$('#search_id').val()+'</span>',
                            'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                            'info':'',
                            'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                            'paginate': {
                              'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                            'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                          },
                          'infoFiltered': "(filtered from _MAX_ total records)"
                        },
                        "ajax": {
                            "url": url,
                            "type": 'get',
                            data: {
                                'division_id': division_id,
                                'customer_id': customer_id,
                            },
                        },
                        "createdRow": function( row, data,dataIndex,columns )
                        {
                            var checkbox_permission=$('#checkbox_permission').val();
                            var checkbox='';
                            checkbox+='<div class="form-check"><input type="checkbox" class="selectedrowsitegroup form-check-input" id="sitegroup'+data.id+'"><label class="form-check-label" for="sitegroup'+data.id+'""></label></div>';
                            var submit='';
                            submit+='<a type="button" class="btn btn-primary btn-xs my-0 waves-effect all_action_btn_margin waves-light savesitegroupdata sitegroup_show_button_'+data.id+' show_tick_btn'+data.id+'" style="display: none;" id="'+data.id+'"><i class="fas fa-check"></i></a>';
                            $(columns[0]).html(checkbox);
                            $(columns[1]).attr('id', 'nameS'+data['id']);
                            if($("#group_name_edit").val()==1){
                                $(columns[1]).attr('Contenteditable', 'true');
                            }
                            $(columns[1]).attr('class','edit_inline_sitegroup');
                            $(columns[1]).attr('data-id',data['id']);
                            $(columns[1]).attr('onkeydown', 'editSelectedRow(sitegroup_tr'+data['id']+')');
                            $(columns[2]).append(submit);
                            $(row).attr('id', 'sitegroup_tr'+data['id']);
                            //$(row).attr('class', 'selectedrowsitegroup');
                            $(row).attr('data-id', data['id']);
                            var temp=data['id'];
                        },
                        columns:
                            [
                                {data: 'checkbox', name: 'checkbox',visible:$('#group_checkbox').val()},
                                {data: 'name', name: 'name',visible:$('#group_name').val()},
                                // {data: 'submit', name: 'submit',visible:$('#edit_group').val()},
                                // {data:'division_id',name:'division_id',visible:$('#group_division').val()},
                                // {data:'customer_id',name:'customer_id',visible:$('#group_customer').val()},
                                {data: 'actions', name: 'actions',visible:$('#delete_group').val()},
                            ],

                    }
                );
        if ($(":checkbox").prop('checked',true)){
            $(":checkbox").prop('checked',false);
        }
    }


    $(document).ready(function () {
        var url='/sitegroup/getall';
        sitegroup_data(url);
        $('.group_listing_close').click(function(){
            console.log('dd');
            var division_id=$('#division_id').children("option:selected").val();
            var customer_id=$('#customer_id').children("option:selected").val();
            $.ajax({
                type: "GET",
                url: '/sitegroup/getall/getAllbyCustAndDivi',
                data: {
                    'division_id': division_id,
                    'customer_id': customer_id,
                },
                success: function(response)
                {
                    var html='';
                    if(response.data.length<=0){
                        html+='<option value=" ">No Groups Found ....</option>';
                    }
                    var site_group_id=$('#site_group_id').children("option:selected").val();
                    for (var i = response.data.length - 1; i >= 0; i--) {
                        var selected='';
                        if(response.project.site_group_id == response.data[i].id)
                        {
                            selected='selected';
                        }
                        html+='<option '+selected+' value="'+response.data[i].id+'">'+response.data[i].name+'</option>';
                    }

                    if (site_group_id==''){
                        html+='<option selected disabled value="">---Please Select---</option>';
                    }else{
                        html+='<option disabled value="">---Please Select---</option>';
                    }
                    $('#site_group_id').html(html);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        });

    });

    $("body").on('change','#division_id',function () {
        var division_id=$('#division_id').children("option:selected").val();
        var customer_id=$('#customer_id').children("option:selected").val();
        $.ajax({
            type: "GET",
            url: '/sitegroup/getall/getAllbyCustAndDivi',
            data: {
                'division_id': division_id,
                'customer_id': customer_id,
            },
            success: function(response)
            {
                var html='';
                html+='<option selected disabled value="">---Please Select---</option>';
                if(response.data.length<=0){
                    html+='<option value=" ">No Groups Found ....</option>';
                }
                for (var i = response.data.length - 1; i >= 0; i--) {
                    html+='<option value="'+response.data[i].id+'">'+response.data[i].name+'</option>';
                }

                $('#site_group_id').html(html);
            },
            error: function (error) {
                console.log(error);
            }
        });
    });

    $("body").on('change','#customer_id',function () {
        var division_id=$('#division_id').children("option:selected").val();
        var customer_id=$('#customer_id').children("option:selected").val();
        $.ajax({
            type: "GET",
            url: '/sitegroup/getall/getAllbyCustAndDivi',
            data: {
                'division_id': division_id,
                'customer_id': customer_id,
            },
            success: function(response)
            {
                var html='';
                if(response.data.length<=0){
                    html+='<option value=" ">No Groups Found ....</option>';
                }
                for (var i = response.data.length - 1; i >= 0; i--) {
                    html+='<option value="'+response.data[i].id+'">'+response.data[i].name+'</option>';
                }
                $('#site_group_id').html(html);
            },
            error: function (error) {
                console.log(error);
            }
        });
    });


    $('body').on('change','.get_customer_data',function(){
        var customer_id = $(this).attr('data-id');
        var division_id=$(this).children("option:selected").val();
        $.ajax({
            type: "GET",
            url: '/customer/getall/bydivision',
            data: {
                'division_id': division_id
            },
            success: function(response)
            {
                var html='';
                if(response.data.length<=0){
                    html+='<option value="">No Customer Found ....</option>';
                }
                for (var i = response.data.length - 1; i >= 0; i--) {
                    html+='<option value="'+response.data[i].id+'">'+response.data[i].name+'</option>';
                }
                $('#customer_id'+customer_id).html(html);
            },
            error: function (error) {
                console.log(error);
            }
        });

    });
</script>
