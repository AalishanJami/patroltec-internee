@extends('backend.layouts.backend')
<style type="text/css">

  td{

  color:white !important;
  }
  .progress, .progress .progress-bar {
    height: 6px!important;
}

</style>
@section('content')




      <!-- Section: Intro -->
      <section class="mt-md-4 pt-md-2 mb-5 pb-4">

        <!-- Grid row -->
        <div class="row">

          <!-- Grid column -->
          <div class="col-xl-3 col-md-6 mb-xl-0 mb-4">

            <!-- Card -->
            <div class="card card-cascade cascading-admin-card">

              <!-- Card Data -->
              <div class="admin-up">
                <i class="far fa-money-bill-alt primary-color mr-3 z-depth-2"></i>
                <div class="data">
                  <p class="text-uppercase">INDUCTIONS</p>
                  <h4 class="font-weight-bold dark-grey-text">2000</h4>
                </div>
              </div>

              <!-- Card content -->
              <div class="card-body card-body-cascade">
                <div class="progress mb-3">
                  <div class="progress-bar bg-primary" role="progressbar" style="width: 25%" aria-valuenow="25"
                    aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <p class="card-text">Better than last week (25%)</p>
              </div>

            </div>
            <!-- Card -->

          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-xl-3 col-md-6 mb-xl-0 mb-4">

            <!-- Card -->
            <div class="card card-cascade cascading-admin-card">

              <!-- Card Data -->
              <div class="admin-up">
                <i class="fas fa-chart-line warning-color mr-3 z-depth-2"></i>
                <div class="data">
                  <p class="text-uppercase">HOURS WORKED</p>
                  <h4 class="font-weight-bold dark-grey-text">200</h4>
                </div>
              </div>

              <!-- Card content -->
              <div class="card-body card-body-cascade">
                <div class="progress mb-3">
                  <div class="progress-bar red accent-2" role="progressbar" style="width: 25%" aria-valuenow="25"
                    aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <p class="card-text">Worse than last week (25%)</p>
              </div>

            </div>
            <!-- Card -->

          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-xl-3 col-md-6 mb-md-0 mb-4">

            <!-- Card -->
            <div class="card card-cascade cascading-admin-card">

              <!-- Card Data -->
              <div class="admin-up">
                <i class="fas fa-chart-pie light-blue lighten-1 mr-3 z-depth-2"></i>
                <div class="data">
                  <p class="text-uppercase">DELIVERIES</p>
                  <h4 class="font-weight-bold dark-grey-text">20000</h4>
                </div>
              </div>

              <!-- Card content -->
              <div class="card-body card-body-cascade">
                <div class="progress mb-3">
                  <div class="progress-bar red accent-2" role="progressbar" style="width: 75%" aria-valuenow="75"
                    aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <p class="card-text">Worse than last week (75%)</p>
              </div>

            </div>
            <!-- Card -->

          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-xl-3 col-md-6 mb-0">

            <!-- Card -->
            <div class="card card-cascade cascading-admin-card">

              <!-- Card Data -->
              <div class="admin-up">
                <i class="fas fa-chart-bar red accent-2 mr-3 z-depth-2"></i>
                <div class="data">
                  <p class="text-uppercase">INCIDENTS</p>
                  <h4 class="font-weight-bold dark-grey-text">2000</h4>
                </div>
              </div>

              <!-- Card content -->
              <div class="card-body card-body-cascade">
                <div class="progress mb-3">
                  <div class="progress-bar bg-primary" role="progressbar" style="width: 25%" aria-valuenow="25"
                    aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <p class="card-text">Better than last week (25%)</p>
              </div>

            </div>
            <!-- Card -->

          </div>
          <!-- Grid column -->

        </div>
        <!-- Grid row -->

      </section>
      <!-- Section: Intro -->

      <!-- Section: Main panel -->
      <section class="mb-5">

        <!-- Card -->
        <div class="card card-cascade narrower">

          <!-- Section: Chart -->
          <section>

            <!-- Grid row -->
            <div class="row">

              <!-- Grid column -->
              <div class="col-xl-5 col-lg-12 mr-0 pb-2">

                <!-- Card image -->
                <div class="view view-cascade gradient-card-header light-blue lighten-1">
                  <h2 class="h2-responsive mb-0 font-weight-500">Score</h2>
                </div>

                <!-- Card content -->
                <div class="card-body card-body-cascade pb-0">

                  <!-- Panel data -->
                  <div class="row py-3 pl-4">

                    <!-- First column -->
                    <div class="col-md-6">

                      <!-- Date select -->
                      <p class="lead"><span class="badge info-color p-2">Site List</span></p>
                   <select searchable="Search here.."  class="mdb-select colorful-select dropdown-info md-form"  onchange="javascript:handleSelect(this)">
                        <option value="" disabled>Please Select</option>
                        <option value="dashboard" >All Projects</a>
                       <option value="lily" >Lily Square</option>
                        <option value="king" selected>King Cross</option>
                        <option value="chelsea">Chelsea Barracks</option>

                      </select>

                      <!-- Date pickers -->
                      <p class="lead pt-3 pb-4"><span class="badge info-color p-2">Chose Date </span></p>
                      <div class="md-form">
                        <input placeholder="Selected date" type="text" id="from" class="form-control datepicker">
                        <label for="date-picker-example" class="active">From</label>
                      </div>
                      <div class="md-form">
                        <input placeholder="Selected date" type="text" id="to" class="form-control datepicker">
                        <label for="date-picker-example" class="active">To</label>
                      </div>

                    </div>
                    <!-- First column -->

                    <!-- Second column -->
                    <div class="col-md-6 text-center pl-xl-2 my-md-0 my-3">

                      <!-- Summary -->
                      <p>Top Score: <strong>2000</strong>
                        <button type="button" class="btn btn-info btn-sm p-2" data-toggle="tooltip" data-placement="top"
                          title="Total scores in the given period"><i class="fas fa-question"></i></button>
                      </p>
                      <p>Average Score: <strong>100</strong>
                        <button type="button" class="btn btn-info btn-sm p-2 mr-0" data-toggle="tooltip"
                          data-placement="top" title="Average daily scores in the given period"><i
                            class="fas fa-question"></i></button>
                      </p>

                      <!-- Change chart -->
                      <span class="min-chart my-4" id="chart-sales" data-percent="76"><span
                          class="percent"></span></span>
                      <h5>
                        <span class="badge red accent-2 p-2">Change <i class="fas fa-arrow-circle-up ml-1"></i></span>
                        <button type="button" class="btn btn-info btn-sm p-2" data-toggle="tooltip" data-placement="top"
                          title="Percentage change compared to the same period in the past"><i
                            class="fas fa-question"></i>
                        </button>
                      </h5>

                    </div>
                    <!-- Second column -->

                  </div>
                  <!-- Panel data -->

                </div>
                <!-- Card content -->

              </div>
              <!-- Grid column -->

              <!-- Grid column -->
              <div class="col-xl-7 col-lg-12 mb-4 pb-2">

                <!-- Chart -->
                <div class="view view-cascade gradient-card-header blue-gradient">

                  <canvas id="lineChart" height="175"></canvas>

                </div>

              </div>
              <!-- Grid column -->

            </div>
            <!-- Grid row -->

          </section>
          <!-- Section: Chart -->

          <!-- Section: Table -->
          <section>

            <!-- Top Table UI -->
            <div class="table-ui p-2 mb-3 mx-4 mb-5">

              <!-- Grid row -->
              <div class="row">

                <!-- Grid column -->
                <div class="col-xl-3 col-md-6">

                  <!-- Name -->
                  <select searchable="Search here.."  class="mdb-select colorful-select dropdown-info mx-2 md-form mt-3 md-dropdown">
                    <option value="" disabled selected>Bulk actions</option>

                    <option value="2">Export</option>

                  </select>

                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-xl-3 col-md-6">

                  <!-- Blue select -->


                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-xl-3 col-md-6">

                  <!-- Blue select -->


                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-xl-3 col-md-6">

                  <form class="form-inline mt-2 ml-2">
                    <div class="form-group md-form mt-2" >
                      <input class="form-control w-100" type="text" placeholder="Search">
                    </div>
                    <button class="btn btn-sm btn-primary ml-2 mr-0 mb-md-0 mb-4"><i
                        class="fas fa-search"></i></button>
                  </form>

                </div>
                <!-- Grid column -->

              </div>
              <!-- Grid row -->

            </div>
            <!-- Top Table UI -->

            <div class="card card-cascade narrower z-depth-0">

              <div
                class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">

                <div>
                  <button type="button" class="btn btn-outline-white btn-rounded btn-sm px-2"><i
                      class="fas fa-th-large mt-0"></i></button>
                  <button type="button" class="btn btn-outline-white btn-rounded btn-sm px-2"><i
                      class="fas fa-columns mt-0"></i></button>
                </div>

                <a href="" class="white-text mx-3">Forms Completed</a>

                <div>
                  <button type="button" class="btn btn-outline-white btn-rounded btn-sm px-2"><i
                      class="fas fa-pencil-alt mt-0"></i></button>
                  <button type="button" class="btn btn-outline-white btn-rounded btn-sm px-2"><i
                      class="fas fa-eraser mt-0"></i></button>
                  <button type="button" class="btn btn-outline-white btn-rounded btn-sm px-2"><i
                      class="fas fa-info-circle mt-0"></i></button>
                </div>

              </div>

              <div class="px-4">

                <div class="table-responsive">

                  <!--Table-->
                  <table class="table table-hover mb-0">

                    <!-- Table head -->
                    <thead>
                      <tr>
                        <th>
                          <input class="form-check-input" type="checkbox" id="checkbox">
                          <label for="checkbox" class="form-check-label mr-2 label-table"></label>
                        </th>
                        <th class="th-lg"><a>Form Name <i class="fas fa-sort ml-1"></i></a></th>
                        <th class="th-lg"><a>Site Name<i class="fas fa-sort ml-1"></i></a></th>
                        <th class="th-lg"><a>Managers Name<i class="fas fa-sort ml-1"></i></a></th>
                        <th class="th-lg"><a>Score<i class="fas fa-sort ml-1"></i></a></th>

                      </tr>
                    </thead>
                    <!-- Table head -->

                    <!-- Table body -->
                    <tbody style="color: white">
                      <tr style="background-color: green">
                        <th scope="row">
                          <input class="form-check-input" type="checkbox" id="checkbox1">
                          <label for="checkbox1" class="label-table"></label>
                        </th>
                        <td>Mark</td>
                        <td>Otto</td>
                        <td>@mdo</td>
                        <td>86</td>

                      </tr>
                      <tr style="background-color: green">
                        <th scope="row">
                          <input class="form-check-input" type="checkbox" id="checkbox2">
                          <label for="checkbox2" class="label-table"></label>
                        </th>
                        <td>Jacob</td>
                        <td>Thornton</td>
                        <td>@fat</td>
                        <td>81</td>

                      </tr>
                      <tr style="background-color: orange ">
                        <th scope="row">
                          <input class="form-check-input" type="checkbox" id="checkbox3">
                          <label for="checkbox3" class="label-table"></label>
                        </th>
                        <td>Larry</td>
                        <td>the Bird</td>
                        <td>@twitter</td>
                        <td>74</td>

                      </tr>
                      <tr style="background-color: orange ">
                        <th scope="row">
                          <input class="form-check-input" type="checkbox" id="checkbox4">
                          <label for="checkbox4" class="label-table"></label>
                        </th>
                        <td>Paul</td>
                        <td>Topolski</td>
                        <td>@P_Topolski</td>
                        <td>69</td>

                      </tr>
                      <tr style="background-color:  red">
                        <th scope="row">
                          <input class="form-check-input" type="checkbox" id="checkbox5">
                          <label for="checkbox5" class="label-table"></label>
                        </th>
                        <td>Anna</td>
                        <td>Doe</td>
                        <td>@andy</td>
                        <td>56</td>

                      </tr>
                    </tbody>
                    <!-- Table body -->

                  </table>
                  <!-- Table -->

                </div>

                <hr class="my-0">

                <!-- Bottom Table UI -->
                <div class="d-flex justify-content-between">

                  <!-- Name -->
                  <select searchable="Search here.."  class="mdb-select colorful-select dropdown-primary md-form hidden-md-down">
                    <option value="" disabled>Rows number</option>
                    <option value="1" selected>5 rows</option>
                    <option value="2">25 rows</option>
                    <option value="3">50 rows</option>
                    <option value="4">100 rows</option>
                  </select>

                  <!-- Pagination -->
                  <nav class="my-4">
                    <ul class="pagination pagination-circle pg-blue mb-0">

                      <!--First-->
                      <li class="page-item disabled clearfix d-none d-md-block"><a class="page-link">First</a></li>

                      <!-- Arrow left -->
                      <li class="page-item disabled">
                        <a class="page-link" aria-label="Previous">
                          <span aria-hidden="true">&laquo;</span>
                          <span class="sr-only">Previous</span>
                        </a>
                      </li>

                      <!-- Numbers -->
                      <li class="page-item active"><a class="page-link">1</a></li>
                      <li class="page-item"><a class="page-link">2</a></li>
                      <li class="page-item"><a class="page-link">3</a></li>
                      <li class="page-item"><a class="page-link">4</a></li>
                      <li class="page-item"><a class="page-link">5</a></li>

                      <!-- Arrow right -->
                      <li class="page-item">
                        <a class="page-link" aria-label="Next">
                          <span aria-hidden="true">&raquo;</span>
                          <span class="sr-only">Next</span>
                        </a>
                      </li>

                      <!-- First -->
                      <li class="page-item clearfix d-none d-md-block"><a class="page-link">Last</a></li>

                    </ul>
                  </nav>
                  <!-- Pagination -->

                </div>
                <!-- Bottom Table UI -->

              </div>

            </div>

          </section>
          <!--Section: Table-->

        </div>
        <!-- Card -->

      </section>
      <!-- Section: Main panel -->

      <!-- Section: Cascading panels -->
      <section class="mb-5">

        <!-- Grid row -->
        <div class="row">

          <!-- Grid column -->
          <div class="col-lg-6 col-md-6">

            <!-- Panel -->
            <div class="card">

              <div class="card-header white-text primary-color">
                Counts Per Week
              </div>


               <canvas id="cpw"></canvas>


            </div>
            <!-- Panel -->

          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-lg-6 col-md-6">

            <!-- Panel -->
            <div class="card">

              <div class="card-header white-text primary-color">
                Counts Per Month
              </div>


               <canvas id="cpm"></canvas>


            </div>
            <!-- Panel -->

          </div>
          <!-- Grid column -->

          <!-- Grid column -->

          <!-- Grid column -->

        </div>



        <!-- Grid row -->
</section>

 <section class="mb-5">
    <div class="row">


            <div class="col-lg-6 col-md-6">

            <!-- Panel -->
            <div class="card">

              <div class="card-header white-text primary-color">
              Counts Per Day
              </div>


                <canvas id="cpd"></canvas>


            </div>
            <!-- Panel -->

          </div>
          <!-- Grid column -->
          <div class="col-lg-6 col-md-6">

            <!-- Panel -->
            <div class="card">

              <div class="card-header white-text primary-color">
                Observations
              </div>


               <canvas id="ob"></canvas>


            </div>
            <!-- Panel -->

          </div>
          <!-- Grid column -->

          <!-- Grid column -->

          <!-- Grid column -->

        </div>

</section>



 <section class="mb-5">
    <div class="row">

          <!-- Grid column -->

          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-lg-6 col-md-6">

            <!-- Panel -->
            <div class="card">

              <div class="card-header white-text primary-color">
                Completed Late
              </div>


               <canvas id="cl"></canvas>


            </div>
            <!-- Panel -->

          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-lg-6 col-md-6">

            <!-- Panel -->
            <div class="card">

              <div class="card-header white-text primary-color">
              Outstanding Jobs
              </div>


                <canvas id="oj"></canvas>


            </div>
            <!-- Panel -->

          </div>
          <!-- Grid column -->

        </div>

</section>


<section class="mb-5">

  <div class="row align-items-center">
       <div class="col-lg-12 col-md-12 mb-lg-0 mb-4 ">

            <!-- Panel -->
            <div class="card">

             <canvas id="pieChart"></canvas>


            </div>
            <!-- Panel -->

          </div>

  </div>


</section>






      <!--Section: Cascading panels-->

      <!-- Section: Classic admin cards -->
      <section class="pb-3">

        <!-- Grid row -->
        <div class="row">

          <!-- Grid column -->
          <div class="col-xl-3 col-md-6 mb-xl-0 mb-4">

            <!-- Card Primary -->
            <div class="card classic-admin-card primary-color">
              <div class="card-body">
                <div class="pull-right">
                  <i class="far fa-money-bill-alt"></i>
                </div>
                <p class="white-text">GREEN OBSERVATIONS</p>
                <h4 class="check">2000</h4>
              </div>
              <div class="progress">
                <div class="progress-bar bg grey darken-3" role="progressbar" style="width: 25%" aria-valuenow="25"
                  aria-valuemin="0" aria-valuemax="100"></div>
              </div>
              <div class="card-body">
                <p>Better than last week (25%)</p>
              </div>
            </div>
            <!-- Card Primary -->

          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-xl-3 col-md-6 mb-xl-0 mb-4">

            <!-- Card Yellow -->
            <div class="card classic-admin-card warning-color">
              <div class="card-body">
                <div class="pull-right">
                  <i class="fas fa-chart-line"></i>
                </div>
                <p>RED OBSERVATION</p>
                <h4 class="check">200</h4>
              </div>
              <div class="progress">
                <div class="progress-bar bg grey darken-3" role="progressbar" style="width: 25%" aria-valuenow="25"
                  aria-valuemin="0" aria-valuemax="100"></div>
              </div>
              <div class="card-body">
                <p>Worse than last week (25%)</p>
              </div>
            </div>
            <!-- Card Yellow -->

          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-xl-3 col-md-6 mb-md-0 mb-4">

            <!-- Card Blue -->
            <div class="card classic-admin-card light-blue lighten-1">
              <div class="card-body">
                <div class="pull-right">
                  <i class="fas fa-chart-pie"></i>
                </div>
                <p>OUTSTANDING ACTIONS</p>
                <h4 class="check">20000</h4>
              </div>
              <div class="progress">
                <div class="progress-bar bg grey darken-4" role="progressbar" style="width: 75%" aria-valuenow="75"
                  aria-valuemin="0" aria-valuemax="100"></div>
              </div>
              <div class="card-body">
                <p>Better than last week (75%)</p>
              </div>
            </div>
            <!-- Card Blue -->

          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-xl-3 col-md-6 mb-0">

            <!-- Card Red -->
            <div class="card classic-admin-card red accent-2">
              <div class="card-body">
                <div class="pull-right">
                  <i class="fas fa-chart-bar"></i>
                </div>
                <p>PRIORITY FORMS</p>
                <h4 class="check">2000</h4>
              </div>
              <div class="progress">
                <div class="progress-bar bg grey darken-4" role="progressbar" style="width: 25%" aria-valuenow="25"
                  aria-valuemin="0" aria-valuemax="100"></div>
              </div>
              <div class="card-body">
                <p>Better than last week (25%)</p>
              </div>
            </div>
            <!-- Card Red -->

          </div>
          <!-- Grid column -->

        </div>
        <!-- Grid row -->

      </section>
      <!-- Section: Classic admin cards -->

<script type="text/javascript">
 $('.datepicker').pickadate({selectYears:250,});

    // Material Select Initialization
    $(document).ready(function () {
      $('.mdb-select').material_select();
    });

    // Tooltips Initialization
    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    })

  </script>

  <!-- Charts -->
  <script>
    // Small chart
    $(function () {
      $('.min-chart#chart-sales').easyPieChart({
        barColor: "#FF5252",
        onStep: function (from, to, percent) {
          $(this.el).find('.percent').text(Math.round(percent));
        }
      });
    });

    // Main chart
    var ctxL = document.getElementById("lineChart").getContext('2d');
    var myLineChart = new Chart(ctxL, {
      type: 'line',
      data: {
labels: ["40%", "50%", "60%", "70%", "80%", "90%"],
datasets: [{
label: "Kings Cross",
data: [650, 590, 800, 80, 560, 50, 900],
backgroundColor: [
'rgba(255, 206, 86, 0.2)',
],
borderColor: [
'rgba(255, 206, 86, 1)',
],
borderWidth: 2
},
]
},
      options: {
        legend: {
          labels: {
            fontColor: "#fff",
          }
        },
        scales: {
          xAxes: [{
            gridLines: {
              display: true,
              color: "rgba(255,255,255,.25)"
            },
            ticks: {
              fontColor: "#fff",
            },
          }],
          yAxes: [{
            display: true,
            gridLines: {
              display: true,
              color: "rgba(255,255,255,.25)"
            },
            ticks: {
              fontColor: "#fff",
            },
          }],
        }
      }
    });
</script>

<script type="text/javascript">
  //line
var ctxL = document.getElementById("cpw").getContext('2d');
var myLineChart = new Chart(ctxL, {
type: 'line',
      data: {
labels: ["40%", "50%", "60%", "70%", "80%", "90%"],
datasets: [{
label: "Kings Cross",
data: [650, 590, 800, 80, 560, 50, 900],
backgroundColor: [
'rgba(255, 206, 86, 0.2)',
],
borderColor: [
'rgba(255, 206, 86, 1)',
],
borderWidth: 2
},

]
},
options: {
responsive: true
}
});

</script>
<script type="text/javascript">
  //line
var ctxL = document.getElementById("cpm").getContext('2d');
var myLineChart = new Chart(ctxL, {
type: 'line',
      data: {
labels: ["40%", "50%", "60%", "70%", "80%", "90%"],
datasets: [{
label: "Kings Cross",
data: [650, 590, 800, 80, 560, 50, 900],
backgroundColor: [
'rgba(255, 206, 86, 0.2)',
],
borderColor: [
'rgba(255, 206, 86, 1)',
],
borderWidth: 2
},
]
},
options: {
responsive: true
}
});

</script>
<script type="text/javascript">
  //line
//bar
var ctxB = document.getElementById("ob").getContext('2d');
var myBarChart = new Chart(ctxB, {
type: 'bar',
data: {
labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
datasets: [{
label: '# of Votes',
data: [12, 19, 3, 5, 2, 3],
backgroundColor: [
'rgba(255, 99, 132, 0.2)',
'rgba(54, 162, 235, 0.2)',
'rgba(255, 206, 86, 0.2)',
'rgba(75, 192, 192, 0.2)',
'rgba(153, 102, 255, 0.2)',
'rgba(255, 159, 64, 0.2)'
],
borderColor: [
'rgba(255,99,132,1)',
'rgba(54, 162, 235, 1)',
'rgba(255, 206, 86, 1)',
'rgba(75, 192, 192, 1)',
'rgba(153, 102, 255, 1)',
'rgba(255, 159, 64, 1)'
],
borderWidth: 1
},

]
},
options: {
scales: {
yAxes: [{
ticks: {
beginAtZero: true
}
}]
}
}
});

</script>
<script type="text/javascript">
  new Chart(document.getElementById("cpd"), {
"type": "horizontalBar",
"data": {
"labels": ["Red", "Orange", "Yellow", "Green", "Blue", "Purple", "Grey"],
"datasets": [{
"label": "My First Dataset",
"data": [22, 33, 55, 12, 86, 23, 14],
"fill": false,
"backgroundColor": ["rgba(255, 99, 132, 0.2)", "rgba(255, 159, 64, 0.2)",
"rgba(255, 205, 86, 0.2)", "rgba(75, 192, 192, 0.2)", "rgba(54, 162, 235, 0.2)",
"rgba(153, 102, 255, 0.2)", "rgba(201, 203, 207, 0.2)"
],
"borderColor": ["rgb(255, 99, 132)", "rgb(255, 159, 64)", "rgb(255, 205, 86)",
"rgb(75, 192, 192)", "rgb(54, 162, 235)", "rgb(153, 102, 255)", "rgb(201, 203, 207)"
],
"borderWidth": 1
}]
},
"options": {
"scales": {
"xAxes": [{
"ticks": {
"beginAtZero": true
}
}]
}
}
});
</script>

<script type="text/javascript">

  var ctxL = document.getElementById("cl").getContext('2d');
var myLineChart = new Chart(ctxL, {
type: 'line',
      data: {
labels: ["40%", "50%", "60%", "70%", "80%", "90%"],
datasets: [{
label: "Kings Cross",
data: [650, 590, 800, 80, 560, 50, 900],
backgroundColor: [
'rgba(255, 206, 86, 0.2)',
],
borderColor: [
'rgba(255, 206, 86, 1)',
],
borderWidth: 2
},
]
},
options: {
responsive: true
}
});



</script>

<script type="text/javascript">
  var ctxL = document.getElementById("oj").getContext('2d');
var myLineChart = new Chart(ctxL, {
type: 'line',
      data: {
labels: ["40%", "50%", "60%", "70%", "80%", "90%"],
datasets: [{
label: "Kings Cross",
data: [650, 590, 800, 80, 560, 50, 900],
backgroundColor: [
'rgba(255, 206, 86, 0.2)',
],
borderColor: [
'rgba(255, 206, 86, 1)',
],
borderWidth: 2
},
]
},
options: {
responsive: true
}
});
</script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.7.3/dist/Chart.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@0.7.0"></script>
<script type="text/javascript">
  //pie
var ctxP = document.getElementById("pieChart").getContext('2d');
var myPieChart = new Chart(ctxP, {
  plugins: [ChartDataLabels],
  type: 'pie',
  data: {
    labels: ["Red", "Green", "Yellow", "Grey", "Dark Grey"],
    datasets: [{
      data: [210, 130, 120, 160, 120],
      backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360"],
      hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5", "#616774"]
    }]
  },
  options: {
    responsive: true,
    legend: {
      position: 'right',
      labels: {
        padding: 20,
        boxWidth: 10
      }
    },
    plugins: {
      datalabels: {
        formatter: (value, ctx) => {
          let sum = 0;
          let dataArr = ctx.chart.data.datasets[0].data;
          dataArr.map(data => {
            sum += data;
          });
          let percentage = (value * 100 / sum).toFixed(2) + "%";
          return percentage;
        },
        color: 'white',
        labels: {
          title: {
            font: {
              size: '16'
            }
          }
        }
      }
    }
  }
});
</script>
<script type="text/javascript">
function handleSelect(elm)
{
window.location = elm.value;
}
</script>

@endsection
