@extends('backend.layouts.backend')
@section('title', 'Support')
@section('content')
    @php
        $data=localization();
    @endphp
    <link rel="stylesheet" href="{{ asset('custom/css/custom_style.css') }}">
    {{ Breadcrumbs::render('support') }}
    <div class="card">
        <div class="card-body">
            <input type="hidden" id="supportflag" value="1">
            <div id="table" class="table-editable table-responsive">
                @if(Auth()->user()->hasPermissionTo('create_emailSupport') || Auth::user()->all_companies == 1 )
                    <span class="table-add  support_create float-right mb-3 mr-2">
                        <a class="text-success "  >
                            <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
                        </a>
                    </span>
                @endif
                @if(Auth()->user()->hasPermissionTo('delete_selected_emailSupport') || Auth::user()->all_companies == 1 )
                    <button class="btn btn-danger btn-sm" id="deleteselectedsupport">
                       <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                            {!! checkKey('selected_delete',$data) !!}
                        </span>
                    </button>
                @endif
                @if(Auth()->user()->hasPermissionTo('selected_restore_emailSupport') || Auth::user()->all_companies == 1 )
                    <button class="btn btn-primary btn-sm" id="restorebuttonsupport">
                        <span class="assign_class label{{getKeyid('restore',$data)}}" data-id="{{getKeyid('restore',$data) }}" data-value="{{checkKey('restore',$data) }}" >
                            {!! checkKey('restore',$data) !!}
                        </span>
                    </button>
                @endif
                @if(Auth()->user()->hasPermissionTo('select_active_emailSupport') || Auth::user()->all_companies == 1 )
                    <button class="btn btn-success btn-sm" style="display: none;" id="activeselectedbuttonsupport">
                        <span class="assign_class label{{getKeyid('selected_active',$data)}}" data-id="{{getKeyid('selected_active',$data) }}" data-value="{{checkKey('selected_active',$data) }}" >
                            {!! checkKey('selected_active',$data) !!}
                        </span>
                    </button>
                @endif
                @if(Auth()->user()->hasPermissionTo('active_emailSupport') || Auth::user()->all_companies == 1 )
                    <button class="btn btn-primary btn-sm"  style="display: none;" id="showactivebuttonsupport">
                        <span class="assign_class label{{getKeyid('show_active',$data)}}" data-id="{{getKeyid('show_active',$data) }}" data-value="{{checkKey('show_active',$data) }}" >
                            {!! checkKey('show_active',$data) !!}
                        </span>
                    </button>
                @endif
                @if(Auth()->user()->hasPermissionTo('csv_emailSupport') || Auth::user()->all_companies == 1 )
                    <form class="form-style support_export" id="export_excel_project" method="POST" action="{{ url('/support/export/excel') }}">
                        {!! csrf_field() !!}
                        <input type="hidden" class="export_array" name="excel_array" value="1">
                        <button  type="submit" class="form_submit_check btn btn-warning btn-sm">
                            <span class="assign_class label{{getKeyid('excel_export',$data)}}" data-id="{{getKeyid('excel_export',$data) }}" data-value="{{checkKey('excel_export',$data) }}" >
                                {!! checkKey('excel_export',$data) !!}
                            </span>
                        </button>
                    </form>
                @endif
                @if(Auth()->user()->hasPermissionTo('word_emailSupport') || Auth::user()->all_companies == 1 )
                    <form class="form-style support_export" method="POST" id="export_world_project" action="{{ url('/support/export/word') }}">
                        {!! csrf_field() !!}
                        <input type="hidden" class="export_array" name="word_array" value="1">
                        <button  type="submit" class="form_submit_check btn btn-success btn-sm">
                            <span class="assign_class label{{getKeyid('word_export',$data)}}" data-id="{{getKeyid('word_export',$data) }}" data-value="{{checkKey('word_export',$data) }}" >
                                {!! checkKey('word_export',$data) !!}
                            </span>
                        </button>
                    </form>
                @endif
                @if(Auth()->user()->hasPermissionTo('pdf_emailSupport') || Auth::user()->all_companies == 1 )
                    <form  class="form-style support_export" {{pdf_view('supports')}} method="POST" id="export_pdf_project" action="{{ url('/support/export/pdf') }}">
                        {!! csrf_field() !!}
                        <input type="hidden" class="export_array" name="pdf_array" value="1">
                        <button  type="submit" class="form_submit_check btn btn-secondary btn-sm">
                            <span class="assign_class label{{getKeyid('pdf_export',$data)}}" data-id="{{getKeyid('pdf_export',$data) }}" data-value="{{checkKey('pdf_export',$data) }}" >
                                {!! checkKey('pdf_export',$data) !!}
                            </span>
                        </button>
                    </form>
                @endif
                <table id="support_data" class=" table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="no-sort all_checkboxes_style">
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input support_checked" id="support_checkbox_all">
                                    <label class="form-check-label" for="support_checkbox_all">
                                        <span class="assign_class label{{getKeyid('all',$data)}}" data-id="{{getKeyid('all',$data) }}" data-value="{{checkKey('all',$data) }}" >
                                            {!! checkKey('all',$data) !!}
                                        </span>
                                    </label>
                                </div>
                            </th>
                            <th>
                                <span class="assign_class label{{getKeyid('ticket_id',$data)}}" data-id="{{getKeyid('ticket_id',$data) }}" data-value="{{checkKey('ticket_id',$data) }}" >
                                    {!! checkKey('ticket_id',$data) !!}
                                </span>
                            </th>
                            <th>
                                <span class="assign_class label{{getKeyid('user_name',$data)}}" data-id="{{getKeyid('user_name',$data) }}" data-value="{{checkKey('user_name',$data) }}" >
                                    {!! checkKey('user_name',$data) !!}
                                </span>
                            </th>
                            <th>
                                <span class="assign_class label{{getKeyid('title',$data)}}" data-id="{{getKeyid('title',$data) }}" data-value="{{checkKey('title',$data) }}" >
                                    {!! checkKey('title',$data) !!}
                                </span>
                            </th>
                            <th>
                                <span class="assign_class label{{getKeyid('message',$data)}}" data-id="{{getKeyid('message',$data) }}" data-value="{{checkKey('message',$data) }}" >
                                    {!! checkKey('message',$data) !!}
                                </span>
                            </th>
                            <th>
                                <span class="assign_class label{{getKeyid('send_date',$data)}}" data-id="{{getKeyid('send_date',$data) }}" data-value="{{checkKey('send_date',$data) }}" >
                                    {!! checkKey('send_date',$data) !!}
                                </span>
                            </th>
                            <th id="action" class="no-sort all_action_btn"></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            @if(Auth()->user()->all_companies == 1)
                <input type="hidden" id="emailSupport_checkbox" value="1">
                <input type="hidden" id="emailSupport_ticketid" value="1">
                <input type="hidden" id="emailSupport_username" value="1">
                <input type="hidden" id="emailSupport_title" value="1">
                <input type="hidden" id="emailSupport_message" value="1">
                <input type="hidden" id="emailSupport_senddate" value="1">
            @else
                <input type="hidden" id="emailSupport_checkbox" value="{{Auth()->user()->hasPermissionTo('emailSupport_checkbox')}}">
                <input type="hidden" id="emailSupport_ticketid" value="{{Auth()->user()->hasPermissionTo('emailSupport_ticketid')}}">
                <input type="hidden" id="emailSupport_username" value="{{Auth()->user()->hasPermissionTo('emailSupport_username')}}">
                <input type="hidden" id="emailSupport_title" value="{{Auth()->user()->hasPermissionTo('emailSupport_title')}}">
                <input type="hidden" id="emailSupport_message" value="{{Auth()->user()->hasPermissionTo('emailSupport_message')}}">
                <input type="hidden" id="emailSupport_senddate" value="{{Auth()->user()->hasPermissionTo('emailSupport_senddate')}}">
            @endif
        </div>
    </div>
    <input type="hidden" class="get_current_path" value="{{Request::path()}}">
    {{--    Edit Modal--}}
    @component('backend.support.model.edit')
    @endcomponent
    {{--    END Edit Modal--}}

    {{--    Register Modal--}}
    @component('backend.support.model.create')
    @endcomponent
    {{--end modal register--}}
    @include('backend.label.input_label')
    <script type="text/javascript">
        $(document).ready(function() {
            var current_path=$('.get_current_path').val();
            localStorage.setItem('current_path',current_path);
            $('.support_create').click(function(){
                var company_id=$('#companies_selected_nav').children("option:selected").val();
                $('#modalSupport').modal('show');
                $('#company_id').val(company_id);
            });
            var url='/support/getall';
            support_data(url);
            (function ($) {
                var active ='<span class="assign_class label'+$('#breadcrumb_support').attr('data-id')+'" data-id="'+$('#breadcrumb_support').attr('data-id')+'" data-value="'+$('#breadcrumb_support').val()+'" >'+$('#breadcrumb_support').val()+'</span>';
                $('.breadcrumb-item.active').html(active);
                var parent ='<span class="assign_class label'+$('#dashboard_label').attr('data-id')+'" data-id="'+$('#dashboard_label').attr('data-id')+'" data-value="'+$('#dashboard_label').val()+'" >'+$('#dashboard_label').val()+'</span>';
                $('.breadcrumb-item a').html(parent);
            }(jQuery));
        });

        function support_data(url) {
            var company_id=$('.companies_selected').children("option:selected").val();
            var table = $('#support_data').dataTable(
                {
                    processing: true,
                    serverSide:true,
                    responsive:true,
                    language: {
                        'lengthMenu': '  _MENU_ ',
                        'search':'<span class="assign_class label'+$('#search_id').attr('data-id')+'" data-id="'+$('#search_id').attr('data-id')+'" data-value="'+$('#search_id').val()+'" >'+$('#search_id').val()+'</span>',
                        'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                        'info':'',
                        'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                        'paginate': {
                            'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                            'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                        },
                        'infoFiltered': "(filtered from _MAX_ total records)"
                    },
                    "ajax": {
                        "url": url,
                        "type": 'get',
                        "data": {'id':company_id}
                    },
                    "createdRow": function( row, data, dataIndex,columns )
                    {
                        var checkbox_permission=$('#checkbox_permission').val();
                        var checkbox='';
                        checkbox+='<div class="form-check"><input type="checkbox" class="form-check-input selectedrowsupport" id="support'+data.id+'"><label class="form-check-label" for="support'+data.id+'""></label></div>';

                        // console.log(data['user']['name']);
                        $(columns[0]).html(checkbox);
                        $(columns[2]).html(data['user']['name']);
                        $(row).attr('data-id',data['id']);
                        $(row).attr('id', 'support_tr'+data['id']);
                        $(row).attr('class', 'selectedrowsupport');
                        var temp=data['id'];
                    },
                    columns:
                        [
                            // {data: 'checkbox', name: 'checkbox',},
                            {data: 'checkbox', name: 'checkbox',visible:$('#emailSupport_checkbox').val()},
                            {data: 'id', name: 'id',visible:$('#emailSupport_ticketid').val()},
                            {data: 'user_id', name: 'user_id',visible:$('#emailSupport_username').val()},
                            {data: 'title', name: 'title',visible:$('#emailSupport_title').val()},
                            {data: 'message', name: 'message',visible:$('#emailSupport_message').val()},
                            {data: 'created_at', name: 'created_at',visible:$('#emailSupport_senddate').val()},
                            {data: 'actions', name: 'actions'},
                        ],

                    "columnDefs": [ {
                        "targets": 0,
                        "orderable": false
                    } ],
                }
            );
            if ($(":checkbox").prop('checked',true)){
                $(":checkbox").prop('checked',false);
            }
        }
    </script>
    <script src="{{ asset('custom/js/support.js') }}"></script>

@endsection
