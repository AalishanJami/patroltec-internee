@php
    $data=localization();
@endphp
<style type="text/css">
    @media (min-width: 576px) {
        .modal-dialog {
            max-width: 75% !important;
        }
    }
</style>
<div class="modal fade" id="modalEditAnswer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">
                    <span class="assign_class label{{getKeyid('support',$data)}}" data-id="{{getKeyid('support',$data) }}" data-value="{{checkKey('support',$data) }}" >
                        {!! checkKey('support',$data) !!}
                    </span>
                     @if(Auth()->user()->hasPermissionTo('view_cms') || Auth::user()->all_companies == 1 )
                        <button data-class="edit-disable" class="edit_cms_enable edit_cms_switch btn btn-primary btn-sm waves-effect waves-light" style="display: none;" >
                            Edit cms
                        </button>
                        <button  data-class="edit-enable" class="edit_cms_disable edit_cms_switch btn btn-danger btn-sm waves-effect waves-light">
                            Edit cms
                        </button>
                    @endif
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="container contact-form">
                <div class="contact-image">
                    <img src="https://image.ibb.co/kUagtU/rocket_contact.png" alt="rocket_contact"/>
                </div>
                <form >
                    <h3>
                        <span class="assign_class label{{getKeyid('drop_us_a_message',$data)}}" data-id="{{getKeyid('drop_us_a_message',$data) }}" data-value="{{checkKey('drop_us_a_message',$data) }}" >
                            {!! checkKey('drop_us_a_message',$data) !!}
                        </span>
                    </h3>
                    <div class="row">
                        <div class="col-md-12">
                            @if(Auth()->user()->hasPermissionTo('emailSupport_title_edit') || Auth::user()->all_companies == 1 )
                                <div class="form-group">
                                    <label  for="name" class="ml-2 active">
                                        <span class="assign_class label{{getKeyid('title',$data)}}" data-id="{{getKeyid('title',$data) }}" data-value="{{checkKey('title',$data) }}" >
                                            {!! checkKey('title',$data) !!}
                                        </span>
                                    </label>
                                    <input readonly type="text" name="txtName" class="form-control" id="title_support" value="Need developers" />
                                </div>
                            @endif
                            @if(Auth()->user()->hasPermissionTo('emailSupport_message_edit') || Auth::user()->all_companies == 1 )
                                <div class="form-group">
                                    <label  for="name" class="ml-2 active">
                                        <span class="assign_class label{{getKeyid('description',$data)}}" data-id="{{getKeyid('description',$data) }}" data-value="{{checkKey('description',$data) }}" >
                                            {!! checkKey('description',$data) !!}
                                        </span>
                                    </label>
                                    <textarea name="txtMsg" id="message_support" readonly class="form-control" style="width: 100%; height: 150px;">Its not working</textarea>
                                </div>
                            @endif
                            <div class="form-group">
                                <span class="btnContact assign_class label{{getKeyid('send',$data)}}" data-id="{{getKeyid('send',$data) }}" data-value="{{checkKey('send',$data) }}" >
                                    {!! checkKey('send',$data) !!}
                                </span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

