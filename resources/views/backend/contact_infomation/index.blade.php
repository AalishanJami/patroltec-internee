@extends('backend.layouts.user')
@section('title', 'Contact Information')
@section('content')
    @php
       $data=localization();
    @endphp
    @include('backend.layouts.user_sidebar')
    @include('backend.label.input_label')
    <div class="padding-left-custom remove-padding">
        {!! breadcrumInner('contact_information',Session::get('employee_id'),'users','/employee/detail/'.Session::get('employee_id'),'employees','/employee') !!}
        <div class="card">
            <div class="card-body">
                <h5 class="card-header info-color white-text  py-4">
                    <strong>
                        <span class="assign_class label{{getKeyid('contact_information',$data)}}" data-id="{{getKeyid('contact_information',$data) }}" data-value="{{checkKey('contact_information',$data) }}" >
                            {!! checkKey('contact_information',$data) !!}
                        </span>
                    </strong>
                </h5>

                <div class="card-body px-lg-5 pt-0">
                    <form  action="{{url('contact_infomation/update')}}" method="post">
                        @csrf
                        <div class="form-row">
                            @if(Auth()->user()->hasPermissionTo('employeeContact_email_edit') || Auth::user()->all_companies == 1 )
                                <div class="col">
                                    <div class="md-form">
                                        <input type="text" class="form-control" name="email" value="{{ $user->email }}" autocomplete="email" autofocus>
                                        <label   for="email" class="active">
                                            <span class="assign_class label{{getKeyid('email',$data)}}" data-id="{{getKeyid('email',$data) }}" data-value="{{checkKey('email',$data) }}" >
                                                {!! checkKey('email',$data) !!}
                                            </span>
                                        </label>
                                        @if ($errors->has('email'))
                                            <small class="text-danger" style="left: 0% !important;" id="form_name_required_message">{{ $errors->first('email') }}</small>
                                        @endif
                                    </div>
                                </div>
                            @endif
                            @if(Auth()->user()->hasPermissionTo('employeeContact_phone_edit') || Auth::user()->all_companies == 1 )
                                <div class="col">
                                    <div class="md-form" >
                                        <input type="text" class="form-control" name="phone" value="{{ $user->phone }}" autocomplete="phone" autofocus>
                                        <label   for="phone" class="active">
                                            <span class="assign_class label{{getKeyid('phone',$data)}}" data-id="{{getKeyid('phone',$data) }}" data-value="{{checkKey('phone',$data) }}" >
                                                {!! checkKey('phone',$data) !!}
                                            </span>
                                        </label>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="form-row">
                            @if(Auth()->user()->hasPermissionTo('employeeContact_mobile_edit') || Auth::user()->all_companies == 1 )
                                <div class="col">
                                    <div class="md-form">
                                        <input type="text" class="form-control " name="mobile" value="{{ $user->mobile }}" autocomplete="mobile" autofocus>
                                        <label   for="mobile" class="active">
                                            <span class="assign_class label{{getKeyid('mobile_number',$data)}}" data-id="{{getKeyid('mobile_number',$data) }}" data-value="{{checkKey('mobile_number',$data) }}" >
                                                {!! checkKey('mobile_number',$data) !!}
                                            </span>
                                        </label>
                                    </div>
                                </div>
                            @endif
                            @if(Auth()->user()->hasPermissionTo('employeeContact_address_edit') || Auth::user()->all_companies == 1 )
                                <div class="col">
                                    <div class="md-form">
                                        <input type="text" class="form-control " name="address" value="{{ $user->address }}" autocomplete="address" autofocus>
                                        <label   for="address" class="active">
                                            <span class="assign_class label{{getKeyid('address',$data)}}" data-id="{{getKeyid('address',$data) }}" data-value="{{checkKey('address',$data) }}" >
                                                {!! checkKey('address',$data) !!}
                                            </span>
                                        </label>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="form-row">
                            @if(Auth()->user()->hasPermissionTo('employeeContact_house_name_edit') || Auth::user()->all_companies == 1 )
                                <div class="col">
                                    <div class="md-form">
                                        <input type="text" class="form-control " name="house_name" value="{{ $user->house_name }}" autocomplete="house_name" autofocus>
                                        <label   for="house_name" class="active">
                                            <span class="assign_class label{{getKeyid('house_name',$data)}}" data-id="{{getKeyid('house_name',$data) }}" data-value="{{checkKey('house_name',$data) }}" >
                                                {!! checkKey('house_name',$data) !!}
                                            </span>
                                        </label>
                                    </div>
                                </div>
                            @endif
                            @if(Auth()->user()->hasPermissionTo('employeeContact_street_edit') || Auth::user()->all_companies == 1 )
                                <div class="col">
                                    <div class="md-form">
                                        <input type="text" class="form-control " name="street" value="{{ $user->street }}" autocomplete="street" autofocus>
                                        <label   for="street" class="active">
                                            <span class="assign_class label{{getKeyid('street',$data)}}" data-id="{{getKeyid('street',$data) }}" data-value="{{checkKey('street',$data) }}" >
                                                {!! checkKey('street',$data) !!}
                                            </span>
                                        </label>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="form-row">
                            @if(Auth()->user()->hasPermissionTo('employeeContact_town_edit') || Auth::user()->all_companies == 1 )
                                <div class="col">
                                    <div class="md-form">
                                        <input type="text" class="form-control " name="town" value="{{ $user->town }}" autocomplete="town" autofocus>
                                        <label   for="town" class="active">
                                            <span class="assign_class label{{getKeyid('town',$data)}}" data-id="{{getKeyid('town',$data) }}" data-value="{{checkKey('town',$data) }}" >
                                                {!! checkKey('town',$data) !!}
                                            </span>
                                        </label>
                                    </div>
                                </div>
                            @endif
                            @if(Auth()->user()->hasPermissionTo('employeeContact_country_edit') || Auth::user()->all_companies == 1 )
                                <div class="col">
                                    <div class="md-form">
                                        <input type="text" class="form-control " name="county" value="{{ $user->county }}" autocomplete="country" autofocus>
                                        <label   for="county" class="active">
                                            <span class="assign_class label{{getKeyid('country',$data)}}" data-id="{{getKeyid('country',$data) }}" data-value="{{checkKey('country',$data) }}" >
                                                {!! checkKey('country',$data) !!}
                                            </span>
                                        </label>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="form-row">
                            @if(Auth()->user()->hasPermissionTo('employeeContact_post_code_edit') || Auth::user()->all_companies == 1 )
                                <div class="col">
                                    <div class="md-form">
                                        <input type="text" class="form-control " name="postcode" value="{{ $user->postcode }}" autocomplete="postcode" autofocus>
                                        <label  for="postcode" class="active">
                                            <span class="assign_class label{{getKeyid('post_code',$data)}}" data-id="{{getKeyid('post_code',$data) }}" data-value="{{checkKey('post_code',$data) }}" >
                                                {!! checkKey('post_code',$data) !!}
                                            </span>
                                        </label>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <button class="form_submit_check btn btn-outline-info btn-rounded btn-block my-4 waves-effect z-depth-0" type="submit">
                            <span class="assign_class label{{getKeyid('update',$data)}}" data-id="{{getKeyid('update',$data) }}" data-value="{{checkKey('update',$data) }}" >
                                {!! checkKey('update',$data) !!}
                            </span>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

