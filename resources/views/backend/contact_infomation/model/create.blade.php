@php
        $data=localization();
@endphp
<div class="modal fade" id="modelcontactinfromation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Contact</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form >
                 <div class="modal-body">

                    <!--Card content-->
                    <div class="card-body px-lg-5 pt-0">
                        <!-- Form -->
                        <form style="color: #757575;" action="#!">
                            <!-- Name -->
                            <div class="form-row">
                                <div class="col">
                                    <!-- First name -->
                                    <div class="md-form">
                                         <i class="fas fa-user prefix grey-text"></i>
                                        <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                        <label data-error="wrong" data-success="right" for="name">
                                            Email
                                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                                            data-content="tool tip for Directions"></i>
                                        </label>
                                    </div>
                                </div>
                                <div class="col">
                                 <div class="md-form" >
                                    <i class="fas fa-user prefix grey-text"></i>
                                        <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                        <label data-error="wrong" data-success="right" for="name">
                                            phone
                                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                                            data-content="tool tip for phone"></i>
                                        </label>
                                    </div>
                                </div>
                          
                            </div>
                            <div class="form-row">
                                <div class="col">
                                 <div class="md-form">
                                    <i class="fas fa-user prefix grey-text"></i>
                                        <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                        <label data-error="wrong" data-success="right" for="name">
                                            mobile
                                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                                            data-content="tool tip for mobile"></i>
                                        </label>
                                    </div>
                                </div>
                          
                                <div class="col">
                                 <div class="md-form">
                                        <i class="fas fa-user prefix grey-text"></i>
                                        <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                        <label data-error="wrong" data-success="right" for="name">
                                            Address
                                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                                            data-content="tool tip for Address"></i>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col">
                                 <div class="md-form">
                                    <i class="fas fa-user prefix grey-text"></i>
                                        <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                        <label data-error="wrong" data-success="right" for="name">
                                             House Name
                                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                                            data-content="tool tip for  House Name"></i>
                                        </label>
                                    </div>
                                </div>
                          
                                <div class="col">
                                 <div class="md-form">
                                        <i class="fas fa-user prefix grey-text"></i>
                                        <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                        <label data-error="wrong" data-success="right" for="name">
                                            street
                                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                                            data-content="tool tip for street"></i>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col">
                                 <div class="md-form">
                                    <i class="fas fa-user prefix grey-text"></i>
                                        <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                        <label data-error="wrong" data-success="right" for="name">
                                            town
                                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                                            data-content="tool tip for town"></i>
                                        </label>
                                    </div>
                                </div>
                          
                                <div class="col">
                                 <div class="md-form">
                                        <i class="fas fa-user prefix grey-text"></i>
                                        <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                        <label data-error="wrong" data-success="right" for="name">
                                            Country
                                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                                            data-content="tool tip for Country"></i>
                                        </label>
                                    </div>
                                </div>
                            </div>
                               <div class="form-row">
                                <div class="col">
                                 <div class="md-form">
                                    <i class="fas fa-user prefix grey-text"></i>
                                        <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                        <label data-error="wrong" data-success="right" for="name">
                                            postcode
                                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                                            data-content="tool tip for postcode"></i>
                                        </label>
                                    </div>
                                </div>
                          
                                
                            
                            </div>
                        </form>
                        <!-- Form -->
                    </div>

                </div>


                <div class="modal-footer d-flex justify-content-center">
                    <button class="btn btn-primary btn-sm" type="submit"><span class="assign_class label{{getKeyid('save',$data)}}" data-id="{{getKeyid('save',$data) }}" data-value="{{checkKey('save',$data) }}" >{!! checkKey('save',$data) !!} </span></button>
                </div>
            </form>
        </div>
    </div>
</div>
