@php
    $data=localization();
@endphp
<div class="modal fade" id="addethnicModal" tabindex="-1" data-keyboard="false" data-backdrop="static" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">
                    <span class="assign_class label{{getKeyid('ethnic',$data)}}" data-id="{{getKeyid('ethnic',$data) }}" data-value="{{checkKey('ethnic',$data) }}" >
                        {!! checkKey('ethnic',$data) !!}
                    </span>
                    @if(Auth()->user()->hasPermissionTo('view_cms') || Auth::user()->all_companies == 1 )
                        <button data-class="edit-disable" class="edit_cms_enable edit_cms_switch btn btn-primary btn-sm waves-effect waves-light" style="display: none;" >
                            Edit cms
                        </button>
                        <button  data-class="edit-enable" class="edit_cms_disable edit_cms_switch btn btn-danger btn-sm waves-effect waves-light">
                            Edit cms
                        </button>
                    @endif
                </h4>
                <button type="button" class="close ethinic_listing_close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="card">
                <div class="card-body">
                    <div id="table_ethinic" class="table-editable">
                        @if(Auth()->user()->hasPermissionTo('create_employeeEthnic') || Auth::user()->all_companies == 1 )
                        <span class="addethnicModal table-add float-right mb-3 mr-2">
                            <a class="text-success">
                                <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
                            </a>
                        </span>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_delete_employeeEthnic') || Auth::user()->all_companies == 1 )
                        <button id="deleteselectedethinic" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" class=" assign_class btn btn-danger btn-sm label{{getKeyid('selected_delete',$data)}}">
                            {!! checkKey('selected_delete',$data) !!}
                        </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_delete_employeeEthnic') || Auth::user()->all_companies == 1 )
                        <button style="display: none;" id="deleteselectedethinicSoft" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" class=" assign_class btn btn-danger btn-sm label{{getKeyid('selected_delete',$data)}}">
                            {!! checkKey('selected_delete',$data) !!}
                        </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_active_employeeEthnic') || Auth::user()->all_companies == 1 )
                        <button  style="display: none;" id="selectedactivebuttonethinic" class="btn btn-success btn-sm assign_class label{{getKeyid('selected_active',$data)}}"  data-id="{{getKeyid('selected_active',$data) }}" data-value="{{checkKey('selected_active',$data) }}" >
                            {!! checkKey('selected_active',$data) !!}
                        </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_restore_delete_employeeEthnic') || Auth::user()->all_companies == 1 )
                        <button id="restorebuttonethinic" data-id="{{getKeyid('restore',$data) }}" data-value="{{checkKey('restore',$data) }}" class="btn btn-primary btn-sm assign_class label{{getKeyid('restore',$data)}}">
                            {!! checkKey('restore',$data) !!}
                        </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('active_employeeEthnic') || Auth::user()->all_companies == 1 )
                        <button  style="display: none;" class="btn btn-primary btn-sm assign_class label{{getKeyid('show_active',$data)}}" id="activebuttonethinic" data-id="{{getKeyid('show_active',$data) }}" data-value="{{checkKey('show_active',$data) }}" >
                            {!! checkKey('show_active',$data) !!}
                        </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('csv_employeeEthnic') || Auth::user()->all_companies == 1 )
                        <form class="form-style" id="export_excel_ethinic_btn" method="POST" action="{{ url('employee/ethinic/exportExcel') }}">
                            {!! csrf_field() !!}
                            <input type="hidden" class="ethinic_export" name="excel_array" value="1">
                            <button  type="submit" class="form_submit_check btn btn-warning btn-sm">
                                <span class="assign_class label{{getKeyid('excel_export',$data)}}" data-id="{{getKeyid('excel_export',$data) }}" data-value="{{checkKey('excel_export',$data) }}" >
                                    {!! checkKey('excel_export',$data) !!}
                                </span>
                            </button>
                        </form>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('word_employeeEthnic') || Auth::user()->all_companies == 1 )
                        <form class="form-style" id="export_world_ethinic_btn" method="POST" action="{{ url('employee/ethinic/exportWord') }}">
                            {!! csrf_field() !!}
                            <input type="hidden" class="ethinic_export" name="word_array" value="1">
                            <button  type="submit" class="form_submit_check btn btn-success btn-sm">
                                <span class="assign_class label{{getKeyid('word_export',$data)}}" data-id="{{getKeyid('word_export',$data) }}" data-value="{{checkKey('word_export',$data) }}" >
                                    {!! checkKey('word_export',$data) !!}
                                </span>
                            </button>
                        </form>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('pdf_employeeEthnic') || Auth::user()->all_companies == 1 )
                        <form  class="form-style" {{pdf_view('ethinic_origins')}} id="export_pdf_ethinic_btn" method="POST" action="{{ url('employee/ethinic/exportPdf') }}">
                            {!! csrf_field() !!}
                            <input type="hidden" class="ethinic_export" name="pdf_array" value="1">
                            <button  type="submit" class="form_submit_check btn btn-secondary btn-sm">
                                <span class="assign_class label{{getKeyid('pdf_export',$data)}}" data-id="{{getKeyid('pdf_export',$data) }}" data-value="{{checkKey('pdf_export',$data) }}" >
                                    {!! checkKey('pdf_export',$data) !!}
                                </span>
                            </button>
                        </form>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('view_employeeEthnic') || Auth::user()->all_companies == 1 )
                                <div id="table" class="table-editable">
                                    <table id="ethnic_table" style="width: 100%;" class="ethnic_table table table-bordered table-responsive-md table-striped">
                                        <thead>
                                        <tr>
                                            <th class="no-sort all_checkboxes_style">
                                                <div class="form-check">
                                                    <input type="checkbox" class="form-check-input ethnic_checked" id="ethnic_checkbox_all">
                                                    <label class="form-check-label" for="ethnic_checkbox_all">
                                                        <span class="assign_class label{{getKeyid('all',$data)}}" data-id="{{getKeyid('all',$data) }}" data-value="{{checkKey('all',$data) }}" >
                                                            {!! checkKey('all',$data) !!}
                                                        </span>
                                                    </label>
                                                </div>
                                            </th>
                                            <th class="all_name_style">
                                                <span class="assign_class label{{getKeyid('name',$data)}}" data-id="{{getKeyid('name',$data) }}" data-value="{{checkKey('name',$data) }}" >
                                                    {!! checkKey('name',$data) !!}
                                                </span>
                                            </th>
                                            <th class="no-sort all_action_btn" id="action"></th>
                                        </tr>
                                        </thead>
                                        <tbody ></tbody>
                                    </table>
                                </div>
                        @endif
                        @if(Auth()->user()->all_companies == 1)
                            <input type="hidden" id="employeeEthnic_checkbox" value="1">
                            <input type="hidden" id="edit_employeeEthnic" value="1">
                            <input type="hidden" id="create_employeeEthnic" value="1">
                            <input type="hidden" id="delete_employeeEthnic" value="1">
                            <input type="hidden" id="employeeEthnic_name" value="1">
                            <input type="hidden" id="employeeEthnic_name_create" value="1">
                            <input type="hidden" id="employeeEthnic_name_edit" value="1">
                        @else
                            <input type="hidden" id="employeeEthnic_checkbox" value="{{Auth()->user()->hasPermissionTo('employeeEthnic_checkbox')}}">
                            <input type="hidden" id="edit_employeeEthnic" value="{{Auth()->user()->hasPermissionTo('edit_employeeEthnic')}}">
                            <input type="hidden" id="create_employeeEthnic" value="{{Auth()->user()->hasPermissionTo('create_employeeEthnic')}}">
                            <input type="hidden" id="delete_employeeEthnic" value="{{Auth()->user()->hasPermissionTo('delete_employeeEthnic')}}">
                            <input type="hidden" id="employeeEthnic_name" value="{{Auth()->user()->hasPermissionTo('employeeEthnic_name')}}">
                            <input type="hidden" id="employeeEthnic_name_create" value="{{Auth()->user()->hasPermissionTo('employeeEthnic_name_create')}}">
                            <input type="hidden" id="employeeEthnic_name_edit" value="{{Auth()->user()->hasPermissionTo('employeeEthnic_name_edit')}}">
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function ethinic_data(url) {
            var table = $('#ethnic_table').dataTable(
                {
                    processing: true,
                    columnDefs: [
                        { "orderable": false, "targets": 0 }
                    ],
                    language: {
                        'lengthMenu': '  _MENU_ ',
                        'search':'<span class="assign_class label'+$('#search_id').attr('data-id')+'" data-id="'+$('#search_id').attr('data-id')+'" data-value="'+$('#search_id').val()+'" >'+$('#search_id').val()+'</span>',
                        'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                        'info':'',
                        'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                        'paginate': {
                            'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                            'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                        },
                        'infoFiltered': "(filtered from _MAX_ total records)"
                    },
                    "ajax": {
                        "url": url,
                        "type": 'get',
                    },
                    "createdRow": function( row, data, dataIndex,columns )
                    {
                        var checkbox_permission=$('#checkbox_permission').val();
                        var checkbox='';
                        // checkbox+='<div class="form-check"><input type="checkbox" class="form-check-input" id="ethinic'+data.id+'"><label class="form-check-label" for="ethinic'+data.id+'""></label></div>';
                        checkbox+='<div class="form-check"><input type="checkbox" class="form-check-input ethinic_checked selectedrowethinic" id="ethinic_checked'+data.id+'"><label class="form-check-label" for="ethinic_checked'+data.id+'""></label></div>';

                        var submit='';
                        submit+='<a type="button" class="btn btn-primary btn-xs all_action_btn_margin my-0 waves-effect waves-light saveethinicdata ethinic_show_button_'+data.id+'" style="display: none;" id="'+data.id+'"><i class="fas fa-check"></i></a>';
                        if ($("#employeeEthnic_checkbox").val()==1){
                            $(columns[0]).html(checkbox);
                        }
                        $(columns[1]).attr('id', 'ethinic_name'+data['id']);
                        $(columns[1]).attr('data-id', data['id']);
                        if ($("#edit_employeeEthnic").val()==1 && $("#employeeEthnic_name_edit").val()==1){
                            $(columns[1]).attr('Contenteditable', 'true');
                        }
                        $(columns[1]).attr('onkeydown', 'editSelectedRow(ethinic_tr'+data['id']+')');
                        $(columns[1]).attr('class','edit_inline_ethinic');
                        if ($("#edit_employeeEthnic").val()==1){
                            $(columns[2]).append(submit);
                        }
                        $(row).attr('id', 'ethinic_tr'+data['id']);
                        $(row).attr('class', 'selectedrowethinic'+data['id']);
                        var temp=data['id'];
                        $(row).attr('data-id',data['id']);
                    },
                    columns:
                        [
                            {data: 'checkbox', name: 'checkbox',},
                            {data: 'name', name: 'name',visible:$('#employeeEthnic_name').val()},
                            {data: 'actions', name: 'actions'},
                        ],
                }
                );

            if ($(":checkbox").prop('checked',true)){
                $(":checkbox").prop('checked',false);
            }
        }
        var ethinic_url='/employee/ethinic/getall';
        $('.ethinic_listing_close').click(function(){
            $.ajax({
                type: "GET",
                url: ethinic_url,
                success: function(response)
                {
                    var html='';
                    var ethinic_origin_id=$("#ethinic_origin_id").children('option:selected').val();
                    for (var i = response.data.length - 1; i >= 0; i--) {
                        var selected='';
                        if (ethinic_origin_id==response.data[i].id){
                            selected='selected';
                        }
                        html+='<option '+selected+' value="'+response.data[i].id+'">'+response.data[i].name+'</option>';
                    }
                    if (ethinic_origin_id==''){
                        html+='<option selected value="" disabled>Please Select</option>';
                    }else{
                        html+='<option value="" disabled>Please Select</option>';
                    }
                    $('#ethinic_origin_id').html(html);

                },
                error: function (error) {
                    console.log(error);
                }
            });
        });
        $(document).ready(function () {
            var ethinic_url='/employee/ethinic/getall';
            ethinic_data(ethinic_url);
            $("body").on('keyup','.edit_inline_ethinic',function () {
                $(".ethinic_show_button_"+$(this).attr('data-id')).show();
                $('.deleteethinicdata'+$(this).attr('data-id')).hide();
            });
            $.ajax({
                type: "GET",
                url: ethinic_url,
                success: function(response)
                {
                    var html='';
                    var ethinic_origin_id='';
                    @if(!empty($employee->ethinic_origin_id))
                      ethinic_origin_id='{{$employee->ethinic_origin_id}}';
                    @endif
                    for (var i = response.data.length - 1; i >= 0; i--) {
                        var selected='';
                        if (ethinic_origin_id==response.data[i].id){
                            selected='selected';
                        }
                        html+='<option '+selected+' value="'+response.data[i].id+'">'+response.data[i].name+'</option>';
                    }
                    if (ethinic_origin_id==''){
                        html+='<option selected disabled value="">Please Select</option>';
                    }else{
                        html+='<option disabled value="">Please Select</option>';
                    }
                    $('#ethinic_origin_id').html(html);

                },
                error: function (error) {
                    console.log(error);
                }
            });
        });
    </script>
