@php
        $data=localization();
@endphp
<div class="modal fade" id="modalRegisterCompany" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Employee</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form >
                <div class="modal-body mx-3"> 
                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                        <label data-error="wrong" data-success="right" for="name">
                            Name
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                            data-content="tool tip for Name"></i>
                        </label>
                    </div>
                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                        <label data-error="wrong" data-success="right" for="name">
                            Payroll Number
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                            data-content="tool tip for Payroll Number"></i>
                        </label>
                    </div>
                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                        <label data-error="wrong" data-success="right" for="name">
                            Client
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                            data-content="tool tip for Client"></i>
                        </label>
                    </div>
                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                        <label data-error="wrong" data-success="right" for="name">
                            Manager
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                            data-content="tool tip for Manager"></i>
                        </label>
                    </div>
                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                        <label data-error="wrong" data-success="right" for="name">
                            Project
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                            data-content="tool tip for Project"></i>
                        </label>
                    </div>
                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                        <label data-error="wrong" data-success="right" for="name">
                            Address
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                            data-content="tool tip for address"></i>
                        </label>
                    </div>

                </div>


                <div class="modal-footer d-flex justify-content-center">
                    <button class="btn btn-primary btn-sm" type="submit"><span class="assign_class label{{getKeyid('save',$data)}}" data-id="{{getKeyid('save',$data) }}" data-value="{{checkKey('save',$data) }}" >{!! checkKey('save',$data) !!} </span></button>
                </div>
            </form>
        </div>
    </div>
</div>
