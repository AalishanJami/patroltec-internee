@php
    $data=localization();
@endphp
<div class="modal fade" id="modalReligionIndex" tabindex="-1" data-keyboard="false" data-backdrop="static" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">
                    <span class="assign_class label{{getKeyid('religion',$data)}}" data-id="{{getKeyid('religion',$data) }}" data-value="{{checkKey('religion',$data) }}" >
                        {!! checkKey('religion',$data) !!}
                    </span>
                    @if(Auth()->user()->hasPermissionTo('view_cms') || Auth::user()->all_companies == 1 )
                        <button data-class="edit-disable" class="edit_cms_enable edit_cms_switch btn btn-primary btn-sm waves-effect waves-light" style="display: none;" >
                            Edit cms
                        </button>
                        <button  data-class="edit-enable" class="edit_cms_disable edit_cms_switch btn btn-danger btn-sm waves-effect waves-light">
                            Edit cms
                        </button>
                    @endif
                </h4>
                <button type="button" class="close religion_listing_close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="card">
                <div class="card-body">
                    <div id="table_religion" class="table-editable">
                        @if(Auth()->user()->hasPermissionTo('create_employeeReligion') || Auth::user()->all_companies == 1 )
                        <span class="add_religion table-add float-right mb-3 mr-2">
                            <a class="text-success addreligionModal">
                                <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
                            </a>
                        </span>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_delete_employeeReligion') || Auth::user()->all_companies == 1 )
                        <button id="deleteselectedreligion" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" class=" assign_class btn btn-danger btn-sm label{{getKeyid('selected_delete',$data)}}">
                            {!! checkKey('selected_delete',$data) !!}
                        </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_delete_employeeReligion') || Auth::user()->all_companies == 1 )
                        <button style="display: none;" id="deleteselectedreligionSoft" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" class=" assign_class btn btn-danger btn-sm label{{getKeyid('selected_delete',$data)}}">
                            {!! checkKey('selected_delete',$data) !!}
                        </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_active_employeeReligion') || Auth::user()->all_companies == 1 )
                        <button  style="display: none;" id="selectedactivebuttonreligion" class="btn btn-success btn-sm assign_class label{{getKeyid('selected_active',$data)}}"  data-id="{{getKeyid('selected_active',$data) }}" data-value="{{checkKey('selected_active',$data) }}" >
                            {!! checkKey('selected_active',$data) !!}
                        </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_restore_delete_employeeReligion') || Auth::user()->all_companies == 1 )
                        <button id="restorebuttonreligion" data-id="{{getKeyid('restore',$data) }}" data-value="{{checkKey('restore',$data) }}" class="btn btn-primary btn-sm assign_class label{{getKeyid('restore',$data)}}">
                            {!! checkKey('restore',$data) !!}
                        </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('active_employeeReligion') || Auth::user()->all_companies == 1 )
                        <button  style="display: none;" class="btn btn-primary btn-sm assign_class label{{getKeyid('show_active',$data)}}" id="activebuttonreligion" data-id="{{getKeyid('show_active',$data) }}" data-value="{{checkKey('show_active',$data) }}" >
                            {!! checkKey('show_active',$data) !!}
                        </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('csv_employeeReligion') || Auth::user()->all_companies == 1 )
                        <form class="form-style" id="export_excel_religion" method="POST" action="{{ url('employee/religion/exportExcel') }}">
                            {!! csrf_field() !!}
                            <input type="hidden" class="religion_export" name="excel_array" value="1">
                            <button  type="submit" class="form_submit_check btn btn-warning btn-sm">
                                <span class="assign_class label{{getKeyid('excel_export',$data)}}" data-id="{{getKeyid('excel_export',$data) }}" data-value="{{checkKey('excel_export',$data) }}" >
                                    {!! checkKey('excel_export',$data) !!}
                                </span>
                            </button>
                        </form>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('word_employeeReligion') || Auth::user()->all_companies == 1 )
                        <form class="form-style" id="export_world_religion" method="POST" action="{{ url('employee/religion/exportWord') }}">
                            {!! csrf_field() !!}
                            <input type="hidden" class="religion_export" name="word_array" value="1">
                            <button  type="submit" class="form_submit_check btn btn-success btn-sm">
                                <span class="assign_class label{{getKeyid('word_export',$data)}}" data-id="{{getKeyid('word_export',$data) }}" data-value="{{checkKey('word_export',$data) }}" >
                                    {!! checkKey('word_export',$data) !!}
                                </span>
                            </button>
                        </form>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('pdf_employeeReligion') || Auth::user()->all_companies == 1 )
                        <form  class="form-style" {{pdf_view('religions')}} id="export_pdf_religion" method="POST" action="{{ url('employee/religion/exportPdf') }}">
                            {!! csrf_field() !!}
                            <input type="hidden" class="religion_export" name="pdf_array" value="1">
                            <button  type="submit" class="form_submit_check btn btn-secondary btn-sm">
                                <span class="assign_class label{{getKeyid('pdf_export',$data)}}" data-id="{{getKeyid('pdf_export',$data) }}" data-value="{{checkKey('pdf_export',$data) }}" >
                                    {!! checkKey('pdf_export',$data) !!}
                                </span>
                            </button>
                        </form>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('view_employeeReligion') || Auth::user()->all_companies == 1 )
                        <div id="table" class="table-editable">
                            <table id="religion_table" style="width: 100%;" class="religion_table table table-bordered table-responsive-md table-striped">
                                <thead>
                                <tr>
                                    <th class="no-sort all_checkboxes_style">
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input religion_checked" id="religion_checkbox_all">
                                            <label class="form-check-label" for="religion_checkbox_all">
                                                <span class="assign_class label{{getKeyid('all',$data)}}" data-id="{{getKeyid('all',$data) }}" data-value="{{checkKey('all',$data) }}" >
                                                    {!! checkKey('all',$data) !!}
                                                </span>
                                            </label>
                                        </div>
                                    </th>
                                    <th class="all_name_style">
                                        <span class="assign_class label{{getKeyid('name',$data)}}" data-id="{{getKeyid('name',$data) }}" data-value="{{checkKey('name',$data) }}" >
                                            {!! checkKey('name',$data) !!}
                                        </span>
                                    </th>
                                    <th class="no-sort all_action_btn" id="action"></th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                        @endif
                        @if(Auth()->user()->all_companies == 1)
                            <input type="hidden" id="employeeReligion_checkbox" value="1">
                            <input type="hidden" id="edit_employeeReligion" value="1">
                            <input type="hidden" id="create_employeeReligion" value="1">
                            <input type="hidden" id="delete_employeeReligion" value="1">
                            <input type="hidden" id="employeeReligion_name" value="1">
                            <input type="hidden" id="employeeReligion_name_create" value="1">
                            <input type="hidden" id="employeeReligion_name_edit" value="1">
                        @else
                            <input type="hidden" id="employeeReligion_checkbox" value="{{Auth()->user()->hasPermissionTo('employeeReligion_checkbox')}}">
                            <input type="hidden" id="edit_employeeReligion" value="{{Auth()->user()->hasPermissionTo('edit_employeeReligion')}}">
                            <input type="hidden" id="create_employeeReligion" value="{{Auth()->user()->hasPermissionTo('create_employeeReligion')}}">
                            <input type="hidden" id="delete_employeeReligion" value="{{Auth()->user()->hasPermissionTo('delete_employeeReligion')}}">
                            <input type="hidden" id="employeeReligion_name" value="{{Auth()->user()->hasPermissionTo('employeeReligion_name')}}">
                            <input type="hidden" id="employeeReligion_name_create" value="{{Auth()->user()->hasPermissionTo('employeeReligion_name_create')}}">
                            <input type="hidden" id="employeeReligion_name_edit" value="{{Auth()->user()->hasPermissionTo('employeeReligion_name_edit')}}">
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function religion_data(url) {
        var table = $('#religion_table').dataTable(
            {
                processing: true,
                columnDefs: [
                    { "orderable": false, "targets": 0 }
                ],
                language: {
                    'lengthMenu': '  _MENU_ ',
                    'search':'<span class="assign_class label'+$('#search_id').attr('data-id')+'" data-id="'+$('#search_id').attr('data-id')+'" data-value="'+$('#search_id').val()+'" >'+$('#search_id').val()+'</span>',
                    'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                    'info':'',
                    'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                    'paginate': {
                        'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                        'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                    },
                    'infoFiltered': "(filtered from _MAX_ total records)"
                },
                "ajax": {
                    "url": url,
                    "type": 'get',
                },
                "createdRow": function( row, data, dataIndex,columns )
                {
                    var checkbox_permission=$('#checkbox_permission').val();
                    var checkbox='';
                    // checkbox+='<div class="form-check"><input type="checkbox" class="form-check-input" id="religion'+data.id+'"><label class="form-check-label" for="religion'+data.id+'""></label></div>';
                    checkbox+='<div class="form-check"><input type="checkbox" class="form-check-input religion_checked selectedrowreligion" id="religion_checked'+data.id+'"><label class="form-check-label" for="religion_checked'+data.id+'""></label></div>';
                    var submit='';
                    submit+='<a type="button" class="btn btn-primary btn-xs  my-0 waves-effect waves-light  savereligiondata all_action_btn_margin religion_show_button_'+data.id+'" style="display:none;" id="'+data.id+'"><i class="fas fa-check"></i></a>';
                    if ($("#employeeReligion_checkbox").val() ==1){
                        $(columns[0]).html(checkbox);
                    }
                    $(columns[1]).attr('id', 'religion_name'+data['id']);
                    $(columns[1]).attr('data-id', data['id']);
                    if ($("#employeeReligion_name_edit").val() ==1 && $("#edit_employeeReligion").val() ==1){
                        $(columns[1]).attr('Contenteditable', 'true');
                    }
                    $(columns[1]).attr('onkeydown', 'editSelectedRow(religion_tr'+data['id']+')');

                    $(columns[1]).attr('class','edit_inline_religion');
                    if ($("#edit_employeeReligion").val() ==1){
                        $(columns[2]).append(submit);
                    }
                    $(row).attr('id', 'religion_tr'+data['id']);
                    $(row).attr('class', 'selectedrowreligion'+data['id']);
                    $(row).attr('data-id',data['id']);
                    var temp=data['id'];
                },
                columns:
                    [
                        {data: 'checkbox', name: 'checkbox',},
                        {data: 'name', name: 'name',visible:$('#employeeReligion_name').val()},
                        {data: 'actions', name: 'actions'},
                    ],
            }
        );
        if ($(":checkbox").prop('checked',true)){
            $(":checkbox").prop('checked',false);
        }
    }
    $(document).ready(function () {
        $("body").on('keyup','.edit_inline_religion',function () {
            $(".religion_show_button_"+$(this).attr('data-id')).show();
            $('.deletereligiondata'+$(this).attr('data-id')).hide();
        });


        $('.religion_listing_close').click(function(){
            $.ajax({
                type: "GET",
                url: religion_url,
                success: function(response)
                {
                    var html='';
                    var religion_id=$("#religion_id").children('option:selected').val();
                    for (var i = response.data.length - 1; i >= 0; i--) {
                        var selected='';
                        if (religion_id==response.data[i].id){
                            selected='selected';
                        }
                        html+='<option '+selected+' value="'+response.data[i].id+'">'+response.data[i].name+'</option>';
                    }
                    if (religion_id==''){
                        html+='<option disabled selected value="">Please Select</option>';
                    }else{
                        html+='<option disabled value="">Please Select</option>';
                    }
                    $('#religion_id').empty();
                    console.log(html, "html");
                    $('#religion_id').html(html);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        });
        var religion_url='/employee/religion/getall';
        religion_data(religion_url);
        $.ajax({
            type: "GET",
            url: religion_url,
            success: function(response)
            {
                var html='';
                var religion_id='';
                @if(!empty($employee->religion_id))
                    religion_id='{{$employee->religion_id}}';
                @endif
                for (var i = response.data.length - 1; i >= 0; i--) {
                    var selected='';
                    if (religion_id==response.data[i].id){
                        selected='selected';
                    }
                    html+='<option '+selected+' value="'+response.data[i].id+'">'+response.data[i].name+'</option>';
                }
                if (religion_id==''){
                    html+='<option disabled selected value="">Please Select</option>';
                }else{
                    html+='<option disabled value="">Please Select</option>';
                }
                $('#religion_id').html(html);
            },
            error: function (error) {
                console.log(error);
            }
        });
    });
</script>
