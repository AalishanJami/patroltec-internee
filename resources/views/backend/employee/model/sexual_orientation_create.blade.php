@php
    $data=localization();
@endphp
<div class="modal fade" id="SexualOrientationModal" tabindex="-1" data-keyboard="false" data-backdrop="static" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">
                    <span class="assign_class label{{getKeyid('sexual_orientation',$data)}}" data-id="{{getKeyid('sexual_orientation',$data) }}" data-value="{{checkKey('sexual_orientation',$data) }}" >
                        {!! checkKey('sexual_orientation',$data) !!}
                    </span>
                    @if(Auth()->user()->hasPermissionTo('view_cms') || Auth::user()->all_companies == 1 )
                        <button data-class="edit-disable" class="edit_cms_enable edit_cms_switch btn btn-primary btn-sm waves-effect waves-light" style="display: none;" >
                            Edit cms
                        </button>
                        <button  data-class="edit-enable" class="edit_cms_disable edit_cms_switch btn btn-danger btn-sm waves-effect waves-light">
                            Edit cms
                        </button>
                    @endif
                </h4>
                <button type="button" class="close sexual_listing_close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="card">
                <div class="card-body">
                    <div id="table_sexualorientation" class="table-editable">
                        @if(Auth()->user()->hasPermissionTo('create_employeeSexualoren') || Auth::user()->all_companies == 1 )
                        <span class="addSexualOrientationModal table-add float-right mb-3 mr-2">
                            <a class="text-success"><i class="fas fa-plus fa-2x" aria-hidden="true"></i></a>
                        </span>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_delete_employeeSexualoren') || Auth::user()->all_companies == 1 )
                        <button id="deleteselectedsexualorientation" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" class=" assign_class btn btn-danger btn-sm label{{getKeyid('selected_delete',$data)}}">
                            {!! checkKey('selected_delete',$data) !!}
                        </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_delete_employeeSexualoren') || Auth::user()->all_companies == 1 )
                        <button style="display: none;" id="deleteselectedsexualorientationSoft" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" class=" assign_class btn btn-danger btn-sm label{{getKeyid('selected_delete',$data)}}">
                            {!! checkKey('selected_delete',$data) !!}
                        </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_active_employeeSexualoren') || Auth::user()->all_companies == 1 )
                        <button  style="display: none;" id="selectedactivebuttonsexualorientation" class="btn btn-success btn-sm assign_class label{{getKeyid('selected_active',$data)}}"  data-id="{{getKeyid('selected_active',$data) }}" data-value="{{checkKey('selected_active',$data) }}" >
                            {!! checkKey('selected_active',$data) !!}
                        </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_restore_delete_employeeSexualoren') || Auth::user()->all_companies == 1 )
                        <button id="restorebuttonsexualorientation" data-id="{{getKeyid('restore',$data) }}" data-value="{{checkKey('restore',$data) }}" class="btn btn-primary btn-sm assign_class label{{getKeyid('restore',$data)}}">
                            {!! checkKey('restore',$data) !!}
                        </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('active_employeeSexualoren') || Auth::user()->all_companies == 1 )
                        <button  style="display: none;" class="btn btn-primary btn-sm assign_class label{{getKeyid('show_active',$data)}}" id="activebuttonsexualorientation" data-id="{{getKeyid('show_active',$data) }}" data-value="{{checkKey('show_active',$data) }}" >
                            {!! checkKey('show_active',$data) !!}
                        </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('csv_employeeSexualoren') || Auth::user()->all_companies == 1 )
                        <form class="form-style" id="export_excel_sexualorientation_btn" method="POST" action="{{ url('employee/sexualorientation/exportExcel') }}">
                            {!! csrf_field() !!}
                            <input type="hidden" class="sexualorientation_export" name="excel_array" value="1">
                            <button  type="submit" class="form_submit_check btn btn-warning btn-sm">
                                <span class="assign_class label{{getKeyid('excel_export',$data)}}" data-id="{{getKeyid('excel_export',$data) }}" data-value="{{checkKey('excel_export',$data) }}" >
                                    {!! checkKey('excel_export',$data) !!}
                                </span>
                            </button>
                        </form>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('word_employeeSexualoren') || Auth::user()->all_companies == 1 )
                        <form class="form-style" id="export_world_sexualorientation_btn" method="POST" action="{{ url('employee/sexualorientation/exportWord') }}">
                            {!! csrf_field() !!}
                            <input type="hidden" class="sexualorientation_export" name="word_array" value="1">
                            <button  type="submit" class="form_submit_check btn btn-success btn-sm">
                                <span class="assign_class label{{getKeyid('word_export',$data)}}" data-id="{{getKeyid('word_export',$data) }}" data-value="{{checkKey('word_export',$data) }}" >
                                    {!! checkKey('word_export',$data) !!}
                                </span>
                            </button>
                        </form>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('pdf_employeeSexualoren') || Auth::user()->all_companies == 1 )
                        <form  class="form-style" {{pdf_view('sexual_orientations')}} id="export_pdf_sexualorientation_btn" method="POST" action="{{ url('employee/sexualorientation/exportPdf') }}">
                            {!! csrf_field() !!}
                            <input type="hidden" class="sexualorientation_export" name="pdf_array" value="1">
                            <button  type="submit" class="form_submit_check btn btn-secondary btn-sm">
                                <span class="assign_class label{{getKeyid('pdf_export',$data)}}" data-id="{{getKeyid('pdf_export',$data) }}" data-value="{{checkKey('pdf_export',$data) }}" >
                                    {!! checkKey('pdf_export',$data) !!}
                                </span>
                            </button>
                        </form>
                        @endif
                            @if(Auth()->user()->hasPermissionTo('view_employeeSexualoren') || Auth::user()->all_companies == 1 )
                                <div id="table" class="table-editable">
                                    <table id="sexual_orientation_table" style="width: 100%;" class="sexual_orientation_table table table-bordered table-responsive-md table-striped">
                                        <thead>
                                        <tr>
                                            <th class="no-sort all_checkboxes_style">
                                                <div class="form-check">
                                                    <input type="checkbox" class="form-check-input sexualorientation_checked" id="sexualorientation_checkbox_all">
                                                    <label class="form-check-label" for="sexualorientation_checkbox_all">
                                                        <span class="assign_class label{{getKeyid('all',$data)}}" data-id="{{getKeyid('all',$data) }}" data-value="{{checkKey('all',$data) }}" >
                                                            {!! checkKey('all',$data) !!}
                                                        </span>
                                                    </label>
                                                </div>
                                            </th>
                                            <th class="all_name_style">
                                                <span class="assign_class label{{getKeyid('name',$data)}}" data-id="{{getKeyid('name',$data) }}" data-value="{{checkKey('name',$data) }}" >
                                                    {!! checkKey('name',$data) !!}
                                                </span>
                                            </th>
                                            <th class="no-sort all_action_btn" id="action"></th>
                                        </tr>
                                        </thead>
                                        <tbody ></tbody>
                                    </table>
                                </div>
                            @endif
                        @if(Auth()->user()->all_companies == 1)
                            <input type="hidden" id="employeeSexualoren_checkbox" value="1">
                            <input type="hidden" id="edit_employeeSexualoren" value="1">
                            <input type="hidden" id="create_employeeSexualoren" value="1">
                            <input type="hidden" id="delete_employeeSexualoren" value="1">
                            <input type="hidden" id="employeeSexualoren_name" value="1">
                            <input type="hidden" id="employeeSexualoren_name_create" value="1">
                            <input type="hidden" id="employeeSexualoren_name_edit" value="1">
                        @else
                            <input type="hidden" id="employeeSexualoren_checkbox" value="{{Auth()->user()->hasPermissionTo('employeeSexualoren_checkbox')}}">
                            <input type="hidden" id="edit_employeeSexualoren" value="{{Auth()->user()->hasPermissionTo('edit_employeeSexualoren')}}">
                            <input type="hidden" id="create_employeeSexualoren" value="{{Auth()->user()->hasPermissionTo('create_employeeSexualoren')}}">
                            <input type="hidden" id="delete_employeeSexualoren" value="{{Auth()->user()->hasPermissionTo('delete_employeeSexualoren')}}">
                            <input type="hidden" id="employeeSexualoren_name" value="{{Auth()->user()->hasPermissionTo('employeeSexualoren_name')}}">
                            <input type="hidden" id="employeeSexualoren_name_create" value="{{Auth()->user()->hasPermissionTo('employeeSexualoren_name_create')}}">
                            <input type="hidden" id="employeeSexualoren_name_edit" value="{{Auth()->user()->hasPermissionTo('employeeSexualoren_name_edit')}}">
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function sexual_orientation_data(url) {
        var table = $('#sexual_orientation_table').dataTable(
            {
                processing: true,
                columnDefs: [
                    { "orderable": false, "targets": 0 }
                ],
                language: {
                    'lengthMenu': '  _MENU_ ',
                    'search':'<span class="assign_class label'+$('#search_id').attr('data-id')+'" data-id="'+$('#search_id').attr('data-id')+'" data-value="'+$('#search_id').val()+'" >'+$('#search_id').val()+'</span>',
                    'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                    'info':'',
                    'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                    'paginate': {
                        'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                        'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                    },
                    'infoFiltered': "(filtered from _MAX_ total records)"
                },
                "ajax": {
                    "url": url,
                    "type": 'get',
                },
                "createdRow": function( row, data, dataIndex,columns )
                {
                    var checkbox_permission=$('#checkbox_permission').val();
                    var checkbox='';
                    // checkbox+='<div class="form-check"><input type="checkbox" class="form-check-input" id="sexualorientation'+data.id+'"><label class="form-check-label" for="sexualorientation'+data.id+'""></label></div>';
                    checkbox+='<div class="form-check"><input type="checkbox" class="form-check-input sexualorientation_checked selectedrowsexualorientation" id="sexualorientation_checked'+data.id+'"><label class="form-check-label" for="sexualorientation_checked'+data.id+'""></label></div>';
                    var submit='';
                    submit+='<a type="button" class="btn btn-primary btn-xs my-0 waves-effect all_action_btn_margin waves-light saveSexualorientationdata sexual_show_button_'+data.id+'" style="display: none;" id="'+data.id+'"><i class="fas fa-check"></i></a>';
                    if ($("#employeeSexualoren_checkbox").val()==1){
                        $(columns[0]).html(checkbox);
                    }
                    $(columns[1]).attr('id', 'sexualorientation_name'+data['id']);
                    $(columns[1]).attr('data-id', data['id']);
                    if ($("#employeeSexualoren_name_edit").val()==1 && $("#edit_employeeSexualoren").val()==1){
                        $(columns[1]).attr('Contenteditable', 'true');
                    }
                    $(columns[1]).attr('onkeydown', 'editSelectedRow(sexualorientation_tr'+data['id']+')');
                    $(columns[1]).attr('class','edit_inline_sexual');
                    if ($("#employeeSexualoren_name_edit").val()==1){
                        $(columns[2]).append(submit);
                    }
                    $(row).attr('id', 'sexualorientation_tr'+data['id']);
                    $(row).attr('data-id',data['id']);
                    $(row).attr('class', 'selectedrowsexualorientation'+data['id']);
                    var temp=data['id'];
                },
                columns:
                    [
                        {data: 'checkbox', name: 'checkbox',},
                        {data: 'name', name: 'name',visible:$('#employeeSexualoren_name').val()},
                        {data: 'actions', name: 'actions'},
                    ],
            }
        );
        if ($(":checkbox").prop('checked',true)){
            $(":checkbox").prop('checked',false);
        }
    }
    var orientation_id = 1;
    var orientation_url='/employee/sexualorientation/getall';
    $('.sexual_listing_close').click(function(){
        $.ajax({
            type: "GET",
            url: orientation_url,
            success: function(response)
            {
                var html='';
                var sexual_orientation_id=$('#sexual_orientation_id').children('option:selected').val();
                for (var i = response.data.length - 1; i >= 0; i--) {
                    var selected='';
                    if(response.data[i].id == sexual_orientation_id)
                    {
                        selected='selected';
                    }
                    html+='<option '+selected+' value="'+response.data[i].id+'">'+response.data[i].name+'</option>';
                }
                if (sexual_orientation_id==''){
                    html+='<option selected disabled value="">Please Select</option>';
                }else{
                    html+='<option disabled value="">Please Select</option>';
                }
                $('#sexual_orientation_id').empty();
                $('#sexual_orientation_id').html(html);
            },
            error: function (error) {
                console.log(error);
            }
        });
    });
    $(document).ready(function () {

        var orientation_url='/employee/sexualorientation/getall';
        sexual_orientation_data(orientation_url);
        $("body").on('keyup','.edit_inline_sexual',function () {
            $(".sexual_show_button_"+$(this).attr('data-id')).show();
            $('.deletesexualorientationdata'+$(this).attr('data-id')).hide();
        });

        $.ajax({
            type: "GET",
            url: orientation_url,
            success: function(response)
            {
                var html='';
                console.log("Sex Data : "+JSON.stringify(response.data));
                var sexual_orientation='';
                @if(!empty($employee->sexual_orientation_id))
                    sexual_orientation='{{$employee->sexual_orientation_id}}';
                @endif

                if (sexual_orientation==null){
                    html+='<option selected disabled value="">Please Select</option>';
                }else{
                    html+='<option disabled value="">Please Select</option>';
                }
                for (var i = response.data.length - 1; i >= 0; i--) {
                    var selected='';
                    if(response.data[i].id == sexual_orientation )
                    {
                        selected='selected';
                    }
                    html+='<option '+selected+' value="'+response.data[i].id+'">'+response.data[i].name+'</option>';
                }
                $('#sexual_orientation_id').empty();
                $('#sexual_orientation_id').html(html);
            },
            error: function (error) {
                console.log(error);
            }
        });
    });

</script>
