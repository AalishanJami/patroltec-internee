@include('backend.layouts.pdf_start')
    <thead>
    <tr>
        @if(Auth()->user()->hasPermissionTo('employee_firstname_pdf_export') || Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout"> Name</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('employee_payrolnumber_pdf_export') || Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout"> Payroll Number</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('employee_client_pdf_export') || Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout"> Client</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('employee_managerrole_pdf_export') || Auth::user()->all_companies == 1 )
            <th style="vertical-align: text-top;" class="pdf_table_layout"> Manager</th>
        @endif
    </tr>
    </thead>
    <tbody>
    @foreach($data as $row)
        <tr>
            @if(Auth()->user()->hasPermissionTo('employee_firstname_pdf_export') || Auth::user()->all_companies == 1 )
                <td class="pdf_table_layout"><p class="col_text_style">{{ $row->first_name }}</p></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('employee_payrolnumber_pdf_export') || Auth::user()->all_companies == 1 )
                <td class="pdf_table_layout"><p class="col_text_style">{{ $row->payroll_number }}</p></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('employee_client_pdf_export') || Auth::user()->all_companies == 1 )
                <td class="pdf_table_layout"><p class="col_text_style">{{ isset($row->client->surname) ? $row->client->surname : ''  }}</p></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('employee_managerrole_pdf_export') || Auth::user()->all_companies == 1 )
                <td class="pdf_table_layout"><p class="col_text_style">{{ isset($row->locationUserManager->name) ? $row->locationUserManager->name : ''  }}</p></td>
            @endif
        </tr>
    @endforeach
    </tbody>
@include('backend.layouts.pdf_end')
