@include('backend.layouts.pdf_start')
  <thead>
  <tr>
      @if(Auth()->user()->hasPermissionTo('client_firstname_pdf_export') || Auth::user()->all_companies == 1 )
        <th style="vertical-align: text-top;" class="pdf_table_layout"> First Name</th>
      @endif
      @if(Auth()->user()->hasPermissionTo('client_surname_pdf_export') || Auth::user()->all_companies == 1 )
        <th style="vertical-align: text-top;" class="pdf_table_layout"> Surname</th>
      @endif
      @if(Auth()->user()->hasPermissionTo('client_phone_pdf_export') || Auth::user()->all_companies == 1 )
        <th style="vertical-align: text-top;" class="pdf_table_layout"> phone</th>
      @endif
      @if(Auth()->user()->hasPermissionTo('client_email_pdf_export') || Auth::user()->all_companies == 1 )
        <th style="vertical-align: text-top;" class="pdf_table_layout"> Email</th>
      @endif
      @if(Auth()->user()->hasPermissionTo('client_address_pdf_export') || Auth::user()->all_companies == 1 )
        <th style="vertical-align: text-top;" class="pdf_table_layout"> Address</th>
      @endif
  </tr>
  </thead>
  <tbody>
  @foreach($data as $value)
      <tr>
          @if(Auth()->user()->hasPermissionTo('client_firstname_pdf_export') || Auth::user()->all_companies == 1 )
              <td class="pdf_table_layout"><p class="col_text_style">{{ $value->first_name }}</p></td>
          @endif
          @if(Auth()->user()->hasPermissionTo('client_surname_pdf_export') || Auth::user()->all_companies == 1 )
              <td class="pdf_table_layout"><p class="col_text_style">{{ $value->surname }}</p></td>
          @endif
          @if(Auth()->user()->hasPermissionTo('client_phone_pdf_export') || Auth::user()->all_companies == 1 )
              <td class="pdf_table_layout"><p class="col_text_style">{{ $value->phone_number }}</p></td>
          @endif
          @if(Auth()->user()->hasPermissionTo('client_email_pdf_export') || Auth::user()->all_companies == 1 )
              <td class="pdf_table_layout"><p class="col_text_style">{{ $value->email }}</p></td>
          @endif
          @if(Auth()->user()->hasPermissionTo('client_address_pdf_export') || Auth::user()->all_companies == 1 )
              <td class="pdf_table_layout"><p class="col_text_style">{{ $value->address }}</p></td>
          @endif
      </tr>
  @endforeach
  </tbody>
@include('backend.layouts.pdf_end')
