  <head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  </head>
  <body>

<table class="table table-striped table-bordered">
  <thead>
  <tr>
      @if(Auth()->user()->hasPermissionTo('client_firstname_excel_export') || Auth::user()->all_companies == 1 )
          <th> First Name</th>
      @endif
      @if(Auth()->user()->hasPermissionTo('client_surname_excel_export') || Auth::user()->all_companies == 1 )
          <th> Surname</th>
      @endif
      @if(Auth()->user()->hasPermissionTo('client_phone_excel_export') || Auth::user()->all_companies == 1 )
          <th> phone</th>
      @endif
      @if(Auth()->user()->hasPermissionTo('client_email_excel_export') || Auth::user()->all_companies == 1 )
          <th> Email</th>
      @endif
      @if(Auth()->user()->hasPermissionTo('client_address_excel_export') || Auth::user()->all_companies == 1 )
          <th> Address</th>
      @endif
  </tr>
  </thead>
  <tbody>
  @foreach($data as $value)
      <tr>
          @if(Auth()->user()->hasPermissionTo('client_firstname_excel_export') || Auth::user()->all_companies == 1 )
              <td>{{ $value->first_name }}</td>
          @endif
          @if(Auth()->user()->hasPermissionTo('client_surname_excel_export') || Auth::user()->all_companies == 1 )
              <td>{{ $value->surname }}</td>
          @endif
          @if(Auth()->user()->hasPermissionTo('client_phone_excel_export') || Auth::user()->all_companies == 1 )
              <td>{{ $value->phone_number }}</td>
          @endif
          @if(Auth()->user()->hasPermissionTo('client_email_excel_export') || Auth::user()->all_companies == 1 )
              <td>{{ $value->email }}</td>
          @endif
          @if(Auth()->user()->hasPermissionTo('client_address_excel_export') || Auth::user()->all_companies == 1 )
              <td>{{ $value->address }}</td>
          @endif
      </tr>
  @endforeach
  </tbody>
</table>
</body>
