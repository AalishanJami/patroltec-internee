@include('backend.layouts.pdf_start')
  <thead>
  <tr>
      @if(Auth()->user()->hasPermissionTo('employeePosition_name_pdf_export') || Auth::user()->all_companies == 1 )
          <th style="vertical-align: text-top;" class="pdf_table_layout">Name</th>
      @else
          <th style="vertical-align: text-top;" class="pdf_table_layout">No Permission</th>
      @endif
  </tr>
  </thead>
  <tbody>
  @if(Auth()->user()->hasPermissionTo('employeePosition_name_pdf_export') || Auth::user()->all_companies == 1 )
  @foreach($data as $value)
      <tr>
          @if(Auth()->user()->hasPermissionTo('employeePosition_name_pdf_export') || Auth::user()->all_companies == 1 )
              <td class="pdf_table_layout"><p class="col_text_style">{{ $value->name }}</p></td>
          @endif
      </tr>
  @endforeach
  @endif
  </tbody>
@include('backend.layouts.pdf_end')