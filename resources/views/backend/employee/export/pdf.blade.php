  <head>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  </head>
  <body>
  <table class="table table-striped table-bordered">
  <thead>
    <tr>
      <th> Name</th>
    </tr>
  </thead>
  <tbody>
    @foreach($data as $value)
      <tr>
        <td>{{ $value->name }}</td>
      </tr>
    @endforeach
  </tbody>
</table>
</body>
