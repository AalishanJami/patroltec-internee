<table>
    <thead>
    <tr>
        @if(Auth()->user()->hasPermissionTo('employee_firstname_excel_export') || Auth::user()->all_companies == 1 )
            <th>Name</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('employee_payrolnumber_excel_export') || Auth::user()->all_companies == 1 )
            <th>Payroll Number</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('employee_client_excel_export') || Auth::user()->all_companies == 1 )
            <th>Client</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('employee_managerrole_excel_export') || Auth::user()->all_companies == 1 )
            <th>Manager</th>
        @endif
    </tr>
    </thead>
    <tbody>
    @foreach($data as $row)
        <tr>
            @if(Auth()->user()->hasPermissionTo('employee_firstname_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{ $row->first_name }}</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('employee_payrolnumber_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{ $row->payroll_number }}</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('employee_client_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{ isset($row->client->surname) ? $row->client->surname : ''  }}</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('employee_managerrole_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{ isset($row->locationUserManager->name) ? $row->locationUserManager->name : ''  }}</td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>
