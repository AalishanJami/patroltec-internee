 @php
        $data=localization();
@endphp

<div class="modal fade" id="modalEditForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold"><span class="assign_class label{{getKeyid('edit_client',$data)}}" data-id="{{getKeyid('edit_client',$data) }}" data-value="{{checkKey('edit_client',$data) }}" >{!! checkKey('edit_client',$data) !!}edit_client </span></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="edit_client">
                @csrf
                <input type="hidden" id="client_id">
                 <div class="modal-body mx-3">
                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input id="title" type="text" class="form-control validate @error('title') is-invalid @enderror" name="title" value="{{ old('title') }}"  autocomplete="title" autofocus>
                        <label data-error="wrong" class="active" data-success="right" id="title_id" for="title"><span class="assign_class label{{getKeyid('title',$data)}}" data-id="{{getKeyid('title',$data) }}" data-value="{{checkKey('title',$data) }}" >{!! checkKey('title',$data) !!} title</span></label>
                    </div>
                    <div class="md-form mb-5">
                        <i class="fas fa-envelope prefix grey-text"></i>
                        <input id="newuser_email2" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"  autocomplete="email">
                        <label data-error="wrong" class="active" data-success="right" id="newuser_email2_id"  for="orangeForm-email"><span class="assign_class label{{getKeyid('your_email',$data)}}" data-id="{{getKeyid('your_email',$data) }}" data-value="{{checkKey('your_email',$data) }}" >{!! checkKey('your_email',$data) !!} </span></label>
                    </div>
                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input id="first_name" type="text" class="form-control validate @error('first_name') is-invalid @enderror" name="first_name" value="{{ old('first_name') }}"  autocomplete="first_name" autofocus>
                        <label data-error="wrong" class="active" data-success="right" id="first_name_id" for="first_name"><span class="assign_class label{{getKeyid('first_name',$data)}}" data-id="{{getKeyid('first_name',$data) }}" data-value="{{checkKey('first_name',$data) }}" >{!! checkKey('first_name',$data) !!} first_name</span></label>
                    </div>
                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input id="surname" type="text" class="form-control validate @error('surname') is-invalid @enderror" name="surname" value="{{ old('surname') }}"  autocomplete="surname" autofocus>
                        <label data-error="wrong" class="active" data-success="right" id="surname_id" for="title"><span class="assign_class label{{getKeyid('surname',$data)}}" data-id="{{getKeyid('surname',$data) }}" data-value="{{checkKey('surname',$data) }}" >{!! checkKey('surname',$data) !!} surname</span></label>
                    </div>
                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input id="phone_number" type="text" class="form-control validate @error('phone_number') is-invalid @enderror" name="phone_number" value="{{ old('phone_number') }}"  autocomplete="phone_number" autofocus>
                        <label data-error="wrong" class="active" data-success="right" id="phone_number_id" for="phone_number"><span class="assign_class label{{getKeyid('phone_number',$data)}}" data-id="{{getKeyid('phone_number',$data) }}" data-value="{{checkKey('phone_number',$data) }}" >{!! checkKey('phone_number',$data) !!} phone_number</span></label>
                    </div>
                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input id="address" type="text" class="form-control validate @error('address') is-invalid @enderror" name="address" value="{{ old('address') }}"  autocomplete="address" autofocus>
                        <label data-error="wrong" class="active" data-success="right" id="address_id" for="address"><span class="assign_class label{{getKeyid('address',$data)}}" data-id="{{getKeyid('address',$data) }}" data-value="{{checkKey('address',$data) }}" >{!! checkKey('address',$data) !!} address</span></label>
                    </div>
                    <div id="clientEditModel"></div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button class="btn btn-primary btn-sm" type="submit"><span class="assign_class label{{getKeyid('update',$data)}}" data-id="{{getKeyid('update',$data) }}" data-value="{{checkKey('update',$data) }}" >{!! checkKey('update',$data) !!} </span></button>
                </div>
            </form>

         
        </div>
    </div>
</div>
