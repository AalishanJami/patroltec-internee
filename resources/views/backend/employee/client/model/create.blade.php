 @php
        $data=localization();
@endphp
<style type="text/css">
   .form-check-input {
    position: absolute !important;
    pointer-events: none !important;
    opacity: 0!important;
}
</style>
<div class="modal fade" id="modalClientForm2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold"><span class="assign_class label{{getKeyid('new_client',$data)}}" data-id="{{getKeyid('new_client',$data) }}" data-value="{{checkKey('new_client',$data) }}" ><!-- {!! checkKey('new_client',$data) !!} --> new_client </span></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="new_client_add" method="POST">
                @csrf
                <div class="modal-body mx-3">
                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input id="title_new" type="text" class="form-control validate @error('title_new') is-invalid @enderror" name="title_new" value="{{ old('title_new') }}"  autocomplete="title_new" autofocus>
                        <label data-error="wrong" data-success="right" id="title_id_new" for="title_new"><span class="assign_class label{{getKeyid('title',$data)}}" data-id="{{getKeyid('title',$data) }}" data-value="{{checkKey('title',$data) }}" >{!! checkKey('title',$data) !!} title</span></label>
                    </div>
                    <div class="md-form mb-5">
                        <i class="fas fa-envelope prefix grey-text"></i>
                        <input id="newuser_email2_new" type="email" class="form-control @error('email') is-invalid @enderror" name="email_new" value="{{ old('email_new') }}"  autocomplete="email_new">
                        <label data-error="wrong" data-success="right" id="newuser_email2_id_new"  for="orangeForm-email"><span class="assign_class label{{getKeyid('your_email',$data)}}" data-id="{{getKeyid('your_email',$data) }}" data-value="{{checkKey('your_email',$data) }}" >{!! checkKey('your_email',$data) !!} </span></label>
                    </div>
                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input id="first_name_new" type="text" class="form-control validate @error('first_name_new') is-invalid @enderror" name="first_name_new" value="{{ old('first_name_new') }}"  autocomplete="first_name_new" autofocus>
                        <label data-error="wrong" data-success="right" id="first_name_id_new" for="first_name_new"><span class="assign_class label{{getKeyid('first_name',$data)}}" data-id="{{getKeyid('first_name',$data) }}" data-value="{{checkKey('first_name',$data) }}" >{!! checkKey('first_name',$data) !!} first_name</span></label>
                    </div>
                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input id="surname_new" type="text" class="form-control validate @error('surname_new') is-invalid @enderror" name="surname_new" value="{{ old('surname_new') }}"  autocomplete="surname_new" autofocus>
                        <label data-error="wrong" data-success="right" id="surname_id_new" for="surname_new"><span class="assign_class label{{getKeyid('surname',$data)}}" data-id="{{getKeyid('surname',$data) }}" data-value="{{checkKey('surname',$data) }}" >{!! checkKey('surname',$data) !!} surname</span></label>
                    </div>
                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input id="phone_number_new" type="text" class="form-control validate @error('phone_number_new') is-invalid @enderror" name="phone_number_new" value="{{ old('phone_number_new') }}"  autocomplete="phone_number_new" autofocus>
                        <label data-error="wrong" data-success="right" id="phone_number_id_new" for="phone_number_new"><span class="assign_class label{{getKeyid('phone_number',$data)}}" data-id="{{getKeyid('phone_number',$data) }}" data-value="{{checkKey('phone_number',$data) }}" >{!! checkKey('phone_number',$data) !!} phone_number</span></label>
                    </div>
                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input id="address_new" type="text" class="form-control validate @error('address_new') is-invalid @enderror" name="address_new" value="{{ old('address_new') }}"  autocomplete="address_new" autofocus>
                        <label data-error="wrong" data-success="right" id="address_id_new" for="address_new"><span class="assign_class label{{getKeyid('address',$data)}}" data-id="{{getKeyid('address',$data) }}" data-value="{{checkKey('address',$data) }}" >{!! checkKey('address',$data) !!} address</span></label>
                    </div>
                    <div id="companyCreateModel"></div>
                </div>


                <div class="modal-footer d-flex justify-content-center">
                    <button class="btn btn-primary btn-sm" type="submit"><span class="assign_class label{{getKeyid('save',$data)}}" data-id="{{getKeyid('save',$data) }}" data-value="{{checkKey('save',$data) }}" >{!! checkKey('save',$data) !!} </span></button>
                </div>
            </form>
        </div>
    </div>
</div>
