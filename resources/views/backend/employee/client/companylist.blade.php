 <div class="md-form mb-4">
    <div class="row">
        <div class="col-md-12">
            <select searchable="Search here.."  id="companies_selected" class="mdb-select md-form"   >
                <option  disabled selected><span class="assign_class label{{getKeyid('Choose_company',$data)}}" data-id="{{getKeyid('Choose_company',$data) }}" data-value="{{checkKey('Choose_company',$data) }}" >{!! checkKey('Choose_company',$data) !!} </span></option>
                @foreach($companies as $company)
                            <option value="{{$company->id}}">
                                {{$company->name}}
                            </option>
                @endforeach
            </select>
            <label class="mdb-main-label"><span class="assign_class label{{getKeyid('select_company',$data)}}" data-id="{{getKeyid('select_company',$data) }}" data-value="{{checkKey('select_company',$data) }}" >{!! checkKey('select_company',$data) !!} </span></label>
        </div>
    </div>
</div>
 