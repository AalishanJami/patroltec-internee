@extends('backend.layouts.backend')
@section('title', 'Employee')
@section('headscript')
    <link rel="stylesheet" href="{{ asset('custom/css/custom_style.css') }}">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.13.1/css/all.css" integrity="sha384-B9BoFFAuBaCfqw6lxWBZrhg/z4NkwqdBci+E+Sc2XlK/Rz25RYn8Fetb+Aw5irxa" crossorigin="anonymous">
@stop
@section('content')
@php

    $data=localization();
@endphp
{{ Breadcrumbs::render('employee') }}
<div class="card">
  <div class="card-body">
      <ul  class="nav nav-tabs" id="myTab" role="tablist">
        @if(permisionCheck('view_employee'))
            <li class="nav-item">
              <a class="nav-link active" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">
                <span class="assign_class label{{getKeyid('employees',$data)}}" data-id="{{getKeyid('employees',$data) }}" data-value="{{checkKey('employees',$data) }}" >
                    {!! checkKey('employees',$data) !!}
                </span>
              </a>
            </li>
        @endif
        @if(permisionCheck('view_training_matrix'))
        <li class="nav-item">
          <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact"aria-selected="false">
            <span class="assign_class label{{getKeyid('training_matrix',$data)}}" data-id="{{getKeyid('training_matrix',$data) }}" data-value="{{checkKey('training_matrix',$data) }}" >
                {!! checkKey('training_matrix',$data) !!}
            </span>
          </a>
        </li>
        @endif
      </ul>
    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
            <div id="table" class="table-editable">
                @if(permisionCheck('create_employee'))
                <span class="add-form float-right mb-3 mr-2">
                    <a class="text-success employee_add" href="{{url('employee/createDetail')}}" >
                        <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
                     </a>
                </span>
                @endif
                @if(permisionCheck('delete_selected_employee'))
                    <button id="deleteselectedemployee" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" class=" assign_class btn btn-danger btn-sm label{{getKeyid('selected_delete',$data)}}">
                        {!! checkKey('selected_delete',$data) !!}
                    </button>
                    <button style="display: none;" id="deleteselectedemployeeSoft" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" class=" assign_class btn btn-danger btn-sm label{{getKeyid('selected_delete',$data)}}">
                        {!! checkKey('selected_delete',$data) !!}
                    </button>
                @endif
                @if(permisionCheck('selected_active_employee'))
                    <button  style="display: none;" id="selectedactivebuttonemployee" class="btn btn-success btn-sm assign_class label{{getKeyid('selected_active',$data)}}"  data-id="{{getKeyid('selected_active',$data) }}" data-value="{{checkKey('selected_active',$data) }}" >
                        {!! checkKey('selected_active',$data) !!}
                    </button>
                @endif
                @if(permisionCheck('selected_restore_employee'))
                    <button id="restorebuttonemployee" data-id="{{getKeyid('restore',$data) }}" data-value="{{checkKey('restore',$data) }}" class="btn btn-primary btn-sm assign_class label{{getKeyid('restore',$data)}}">
                        {!! checkKey('restore',$data) !!}
                    </button>
                @endif
                @if(permisionCheck('select_active_employee'))
                    <button  style="display: none;" class="btn btn-primary btn-sm assign_class label{{getKeyid('show_active',$data)}}" id="activebuttonemployee" data-id="{{getKeyid('show_active',$data) }}" data-value="{{checkKey('show_active',$data) }}" >
                        {!! checkKey('show_active',$data) !!}
                    </button>
                @endif
                @if(permisionCheck('csv_employee'))
                    <form class="form-style" method="POST" action="{{ url('employee/export/excel') }}">
                        <input type="hidden" id="export_excel_employee" name="export_excel_employee" value="1">
                        <input type="hidden" class="employee_export" name="excel_array" value="1">
                        {!! csrf_field() !!}
                        <button  type="submit" id="export_excel_employee_btn" data-id="{{getKeyid('excel_export',$data) }}" data-value="{{checkKey('excel_export',$data) }}"  class="form_submit_check btn btn-warning btn-sm assign_class label{{getKeyid('excel_export',$data)}}">
                         {!! checkKey('excel_export',$data) !!}
                        </button>
                    </form>
                @endif
                @if(permisionCheck('word_employee'))
                    <form class="form-style" method="POST" action="{{ url('employee/export/world') }}">
                        <input type="hidden" id="export_world_employee" name="export_world_employee" value="1">
                        <input type="hidden" class="employee_export" name="word_array" value="1">
                        {!! csrf_field() !!}
                        <button  type="submit" id="export_world_employee_btn" class="btn btn-success btn-sm form_submit_check assign_class label{{getKeyid('word_export',$data)}}" data-id="{{getKeyid('word_export',$data) }}" data-value="{{checkKey('word_export',$data) }}" >
                        {!! checkKey('word_export',$data) !!}
                        </button>
                    </form>
                @endif
                @if(permisionCheck('pdf_employee'))
                    <form  class="form-style" {{pdf_view("users")}}  method="POST" action="{{ url('employee/export/pdf') }}">
                        <input type="hidden" id="export_pdf_employee" name="export_world_pdf" value="1">
                        <input type="hidden" class="employee_export" name="pdf_array" value="1">
                        {!! csrf_field() !!}
                        <button  type="submit" id="export_pdf_employee_btn"  class="btn btn-secondary btn-sm form_submit_check assign_class label{{getKeyid('pdf_export',$data)}}"  data-id="{{getKeyid('pdf_export',$data) }}" data-value="{{checkKey('pdf_export',$data) }}">
                            {!! checkKey('pdf_export',$data) !!}
                        </button>
                    </form>
                @endif
                @include('backend.label.input_label')
                <table id="employee_table"  class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th id="action" class="no-sort all_checkboxes_style">
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="employee_checkbox_all">
                                    <label class="form-check-label" for="employee_checkbox_all">
                                        <span class="assign_class label{{getKeyid('all',$data)}}" data-id="{{getKeyid('all',$data) }}" data-value="{{ checkKey('all',$data) }}" >
                                            {!!checkKey('all',$data) !!}
                                        </span>
                                    </label>
                                </div>
                            </th>
                            <th class="assign_class label{{getKeyid('first_name',$data)}}" data-id="{{getKeyid('first_name',$data) }}" data-value="{{ checkKey('first_name',$data) }}" >
                                {!!checkKey('first_name',$data) !!}
                            </th>
                            <th class="assign_class label{{getKeyid('surname',$data)}}" data-id="{{getKeyid('surname',$data) }}" data-value="{{ checkKey('surname',$data) }}" >
                                {!!checkKey('surname',$data) !!}
                            </th>
                            <th class="assign_class label{{getKeyid('payroll_number',$data)}}" data-id="{{getKeyid('payroll_number',$data) }}" data-value="{{ checkKey('payroll_number',$data) }}" >
                                {!!checkKey('payroll_number',$data) !!}
                            </th>
                            <th class="assign_class label{{getKeyid('client',$data)}}" data-id="{{getKeyid('client',$data) }}" data-value="{{ checkKey('client',$data) }}" >
                                {!!checkKey('client',$data) !!}
                            </th>
                            <th class="assign_class label{{getKeyid('manager',$data)}}" data-id="{{getKeyid('manager',$data) }}" data-value="{{ checkKey('manager',$data) }}" >
                                {!!checkKey('manager',$data) !!}
                            </th>
                            <th class="no-sort all_action_btn"></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>

                @if(Auth()->user()->all_companies == 1)
                    <input type="hidden" id="employee_checkbox" value="1">
                    <input type="hidden" id="employee_firstname" value="1">
                    <input type="hidden" id="employee_surname" value="1">
                    <input type="hidden" id="employee_payrolnumber" value="1">
                    <input type="hidden" id="employee_client" value="1">
                    <input type="hidden" id="employee_managename" value="1">
                @else
                    <input type="hidden" id="employee_checkbox" value="{{permisionCheck('employee_checkbox')}}">
                    <input type="hidden" id="employee_firstname" value="{{permisionCheck('employee_firstname')}}">
                    <input type="hidden" id="employee_surname" value="{{permisionCheck('employee_surname')}}">
                    <input type="hidden" id="employee_payrolnumber" value="{{permisionCheck('employee_payrolnumber')}}">
                    <input type="hidden" id="employee_client" value="{{permisionCheck('employee_client')}}">
                    <input type="hidden" id="employee_managename" value="{{permisionCheck('employee_managename')}}">
                @endif
            </div>
        </div>
        <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
            <div id="table_traning" class="table-editable">
                <div class="col-md-3">
                    <select searchable="Search here.."  id="select_position_id" class="mdb-select"  >
                        <option value="all">
                            Please Select Position
                        </option>
                        @foreach($positions as $key => $value)
                            <option value="{{$value->id}}">
                                {{$value->name}}
                            </option>
                        @endforeach
                    </select>
                </div>
                <form  class="form-style" {{pdf_view("users")}}  method="POST" action="{{ url('traning/export/pdf') }}">
                        <input type="hidden" class="matrix_export" name="position" value="all">
                        <input type="hidden" class="matrix_export_pdf" name="pdf_array" value="1">
                        {!! csrf_field() !!}
                        <button  type="submit" class="btn btn-secondary btn-sm form_submit_check assign_class label{{getKeyid('pdf_export',$data)}}"  data-id="{{getKeyid('pdf_export',$data) }}" data-value="{{checkKey('pdf_export',$data) }}">
                            {!! checkKey('pdf_export',$data) !!}
                        </button>
                    </form>
                <div id="table" class="table-editable table-responsive">
                    <table id="traning_table" style="width: 100%;" class="traning_table table table-bordered table-responsive-md table-striped">
                        <thead>
                            <tr>
                                <th class="no-sort all_checkboxes_style">
                                    <div class="form-check"><input type="checkbox" class="form-check-input trainingmatrix_checked" id="trainingmatrix_checkbox_all"><label class="form-check-label" for="trainingmatrix_checkbox_all"></label></div>
                                </th>
                                <th class="no-sort all_action_btn" id="action"></th>
                                <th>
                                    <span class="assign_class label{{getKeyid('first_name',$data)}}" data-id="{{getKeyid('first_name',$data) }}" data-value="{{checkKey('first_name',$data) }}" >
                                        {!! checkKey('first_name',$data) !!}
                                    </span>
                                </th>
                                <th>
                                    <span class="assign_class label{{getKeyid('surname',$data)}}" data-id="{{getKeyid('surname',$data) }}" data-value="{{checkKey('surname',$data) }}" >
                                        {!! checkKey('surname',$data) !!}
                                    </span>
                                </th>
                                @foreach($doc_type as $value)
                                <th style="width: 100px;">
                                    <p class="col_text_style" datatoggle="tooltip" title="{{$value->name}}">{{$value->name}}</p>
                                </th>
                                @endforeach()
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" class="get_current_path" value="{{Request::path()}}">
    <script type="text/javascript" src="{{ asset('custom/js/employee.js') }}" ></script>

<script type="text/javascript">
    $(document).ready(function() {
        var current_path=$('.get_current_path').val();
        localStorage.setItem('current_path',current_path);
        $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
                localStorage.setItem('activeTab', $(e.target).attr('href'));
            });
        var activeTab = localStorage.getItem('activeTab');
        if(activeTab){
            $('#myTab a[href="' + activeTab + '"]').tab('show');
        }
        // $('#employee_checkbox_all').click(function() {
        //     if ($(this).is(':checked')) {
        //         $('.employee_checked').attr('checked', true);
        //     } else {
        //         $('.employee_checked').attr('checked', false);
        //     }
        // });
        $('#select_position_id').change(function(){
            var url='/traning/getall';
            var position=$( "#select_position_id option:selected").val();
            $('.matrix_export').val(position);
            $('#traning_table').DataTable().clear().destroy();
            traning_data(url,position);
        });
        $(function () {
            var url='/employee/getall';
            employee_data(url);
            var url='/traning/getall';
            var position='all';
            traning_data(url,position);
        });
        (function ($) {
            var active ='<span class="assign_class label'+$('#breadcrumb_employee').attr('data-id')+'" data-id="'+$('#breadcrumb_employee').attr('data-id')+'" data-value="'+$('#breadcrumb_employee').val()+'" >'+$('#breadcrumb_employee').val()+'</span>';
            $('.breadcrumb-item.active').html(active);
            var parent ='<span class="assign_class label'+$('#dashboard_label').attr('data-id')+'" data-id="'+$('#dashboard_label').attr('data-id')+'" data-value="'+$('#dashboard_label').val()+'" >'+$('#dashboard_label').val()+'</span>';
            $('.breadcrumb-item a').html(parent);
        }(jQuery));
    });

    function employee_data(url)
    {
        var company_id=$('.companies_selected').children("option:selected").val();
        var table = $('#employee_table').dataTable({
           processing: true,
            bInfo:false,
            language: {
                'lengthMenu': '<span class="assign_class label'+$('#show_label').attr('data-id')+'" data-id="'+$('#show_label').attr('data-id')+'" data-value="'+$('#show_label').val()+'" ></span>  _MENU_ <span class="assign_class label'+$('#entries_label').attr('data-id')+'" data-id="'+$('#entries_label').attr('data-id')+'" data-value="'+$('#entries_label').val()+'" ></span>',
                'search':'<span class="assign_class label'+$('#search_id').attr('data-id')+'" data-id="'+$('#search_id').attr('data-id')+'" data-value="'+$('#search_id').val()+'" >'+$('#search_id').val()+'</span>',
                'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                'info': '<span class="assign_class label'+$('#showing_page_label').attr('data-id')+'" data-id="'+$('#showing_page_label').attr('data-id')+'" data-value="'+$('#showing_page_label').val()+'" >'+$('#showing_page_label').val()+'</span> _PAGE_ <span class="assign_class label'+$('#of_label').attr('data-id')+'" data-id="'+$('#of_label').attr('data-id')+'" data-value="'+$('#of_label').val()+'" >'+$('#of_label').val()+'</span> _PAGES_',
                'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                'paginate': {
                    'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                    'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                },
                'infoFiltered': " "
            },
            "ajax": {
                "url": url,
                "type": 'get',
                "data": {'id':company_id}
            },
            "createdRow": function( row, data, dataIndex,columns )
            {
                var checkbox='<div class="form-check"><input type="checkbox" class="form-check-input selectedrowemployee employee_checked" id="employee_check'+data.id+'"><label class="form-check-label" for="employee_check'+data.id+'"></label></div>';
                $(columns[0]).html(checkbox);
                $(row).attr('id', 'employee_tr'+data['id']);
                $(row).attr('class', 'selectedrowemployee');
                $(row).attr('data-id',data['id']);
            },
            columns: [
                {data:'checkbox', name:'checkbox',visible:$('#employee_checkbox').val()},
                {data:'first_name', name:'first_name',visible:$('#employee_firstname').val()},
                {data:'surname', name:'surname',visible:$('#employee_surname').val()},
                {data:'payroll_number', name:'payroll_number',visible:$('#employee_payrolnumber').val()},
                {data:'client_id', name:'client_id',visible:$('#employee_client').val()},
                {data:'manager_id', name:'manager_id',visible:$('#employee_managename').val()},
                {data:'actions', name:'actions'},
            ],
        });
        if ($(":checkbox").prop('checked',true)){
            $(":checkbox").prop('checked',false);
        }
    }
    function traning_data(url,position)
    {
        var dataObject=[];
        $.ajax({
            type: 'GET',
            url: url,
            data: {'position':position },
            success: function(response)
            {
                var visible_true=false;
                $(<?php echo json_encode($doc_type); ?>).each(function(index,value) {
                    visible_true=false;
                    $(response.doc_types_colum).each(function(index_1,doc_type)
                    {
                        if(doc_type.doc_type && doc_type.doc_type.name == value['name'] )
                        {
                            visible_true=true;

                        }
                    });
                    dataObject.push(
                        { 'data' : value['name'], 'name' : value['name'] ,visible:visible_true}
                    );
                    }
                );
                var table = $('#traning_table').dataTable({
                   processing: true,
                    bInfo:false,
                    language: {
                        'lengthMenu': '<span class="assign_class label'+$('#show_label').attr('data-id')+'" data-id="'+$('#show_label').attr('data-id')+'" data-value="'+$('#show_label').val()+'" ></span>  _MENU_ <span class="assign_class label'+$('#entries_label').attr('data-id')+'" data-id="'+$('#entries_label').attr('data-id')+'" data-value="'+$('#entries_label').val()+'" ></span>',
                        'search':'<span class="assign_class label'+$('#search_id').attr('data-id')+'" data-id="'+$('#search_id').attr('data-id')+'" data-value="'+$('#search_id').val()+'" >'+$('#search_id').val()+'</span>',
                        'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                        'info': '<span class="assign_class label'+$('#showing_page_label').attr('data-id')+'" data-id="'+$('#showing_page_label').attr('data-id')+'" data-value="'+$('#showing_page_label').val()+'" >'+$('#showing_page_label').val()+'</span> _PAGE_ <span class="assign_class label'+$('#of_label').attr('data-id')+'" data-id="'+$('#of_label').attr('data-id')+'" data-value="'+$('#of_label').val()+'" >'+$('#of_label').val()+'</span> _PAGES_',
                        'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                        'paginate': {
                            'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                            'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                        },
                        'infoFiltered': " "
                    },
                    "ajax": {
                        "url": url,
                        "type": 'get',
                        "data": {'position':position },
                    },
                    "createdRow": function( row, data, dataIndex,columns )
                    {
                        var checkbox='<div class="form-check"><input type="checkbox" class="form-check-input selectedrowtraning traning_checked" id="traning_checked'+data.id+'"><label class="form-check-label" for="traning_checked'+data.id+'"></label></div>';
                        $(columns[0]).html(checkbox);
                        $(columns[1]).attr('class','align-middle');
                        $(columns[2]).attr('class','align-middle');
                        $(columns[3]).attr('class','align-middle');

                        for (var i = 0 ; i < response.doc_type.length; i++)
                        {
                            column_index=i+4;
                            column_index=parseInt(column_index);
                            if(data[response.doc_type[i].name])
                            {
                                if(data[response.doc_type[i].name] == 'red')
                                {
                                    $(columns[parseInt(column_index)]).html('<span class="red_color_m traning_font"><i class="fal fa-times"></i></span>');
                                    // $(columns[parseInt(column_index)]).attr('class', 'red_color text-center');
                                }
                                else if(data[response.doc_type[i].name]!=1)
                                {
                                    $(columns[parseInt(column_index)]).html('<span class="'+data[response.doc_type[i].name]+'_color traning_font"><i class="far fa-check"></i></span>');
                                    // $(columns[parseInt(column_index)]).attr('class', data[response.doc_type[i].name]+'_color text-center');
                                }
                                else if(data[response.doc_type[i].name] == 1)
                                {
                                    $(columns[parseInt(column_index)]).html(' ');
                                }
                            }

                        }
                        $(row).attr('id', 'traning_tr'+data['id']);
                        // $(row).attr('class', 'selectedrowtraning');
                        $(row).attr('data-id',data['id']);
                    },
                   columns:dataObject,
                });
            },
            error: function (error) {
                console.log(error);
            }
        });
        dataObject.push(
            {data:"checkbox", name:"checkbox"},
            {data:"actions", name:"actions"},
            {data:'first_name', name:'first_name',visible:$('#employee_firstname').val()},
            {data:'surname', name:'surname',visible:$('#employee_surname').val()},
        );

    }
</script>
@endsection
