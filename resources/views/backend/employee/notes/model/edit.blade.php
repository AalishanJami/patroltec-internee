@php
  $data=localization();
@endphp
<style type="text/css">
  @media (min-width: 576px) {
      .modal-dialog {
        max-width: 75% !important;
      }
    }
</style>
<div class="modal" id="modalNoteEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">
                  <span class="assign_class label{{getKeyid('employee_notes_edit_header',$data)}}" data-id="{{getKeyid('employee_notes_edit_header',$data) }}" data-value="{{checkKey('employee_notes_edit_header',$data) }}" >
                    {!! checkKey('employee_notes_edit_header',$data) !!} 
                  </span>
                  @if(Auth()->user()->hasPermissionTo('view_cms') || Auth::user()->all_companies == 1 )
                    <button data-class="edit-disable" class="edit_cms_enable edit_cms_switch btn btn-primary btn-sm waves-effect waves-light" style="display: none;" >
                        Edit cms
                    </button>
                    <button  data-class="edit-enable" class="edit_cms_disable edit_cms_switch btn btn-danger btn-sm waves-effect waves-light">
                        Edit cms
                    </button>
                  @endif
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="employeeNoteFormEdit" >
              <input type="hidden" name="id" id="edit_id">
                <div class="modal-body mx-3">
                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input type="text" name="title" id="edit_title" class="form-control validate @error('name') is-invalid @enderror" name="name" value="" required autocomplete="name" autofocus>
                        <label class="active" data-error="wrong" data-success="right" for="name"  >
                          <span class="assign_class label{{getKeyid('employee_notes_edit_title',$data)}}" data-id="{{getKeyid('employee_notes_edit_title',$data) }}" data-value="{{checkKey('employee_notes_edit_title',$data) }}" >
                            {!! checkKey('employee_notes_edit_title',$data) !!} 
                          </span>
                          <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                          data-content="tool tip for Title"></i>
                        </label>
                         <strong class="title_required_message text-danger" id="title_required_message"></strong>
                    </div>
                    <input type="hidden" name="note" id="notes_edit">
                    <div class="md-form mb-5" id="notes_edit_div">
                      <div class="summernote_inner body_notes notes_value" ></div>
                    </div>
                </div>

                <div class="modal-footer d-flex justify-content-center">
                  <button class="btn btn-primary btn-sm" id="employee_notes_edit" type="submit">
                    <span class="assign_class label{{getKeyid('employee_notes_edit_update',$data)}}" data-id="{{getKeyid('employee_notes_edit_update',$data) }}" data-value="{{checkKey('employee_notes_edit_update',$data) }}" >
                      {!! checkKey('employee_notes_edit_update',$data) !!} 
                    </span>
                  </button>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="{{asset('editor/sprite.svg.js')}}"></script>
<script type="text/javascript">
 $("#employee_notes_edit").click(function(e){
    e.preventDefault();
    var notes=$('#notes_edit_div').find('.note-editable, .card-block p' ).html();
    $('#notes_edit').val(notes);
    $("#employeeNoteFormEdit").submit();
  });
  $("#employeeNoteFormEdit").on('submit',function (event) {
      if($("#edit_title").val() ==""){
          $("#title_required_message").html('Title field required');
          return false;
      }
      event.preventDefault();
      var formData = $("#employeeNoteFormEdit").serializeObject();
      $.ajax({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          type: "POST",
          url: '{{url('note/update')}}',
          data:new FormData(this),
          contentType:false,
          cache:false,
          processData:false,
          success: function(respone)
          {
            toastr["success"](respone.message);
            $('#clientNoteTable').DataTable().clear().destroy();
            $(".title_required_message").html();
            var url='/note/getall';
            noteAppend(url);
            $('#modalNoteEdit').modal('hide');
          },
          error: function (error) {
              console.log(error);
          }
      });

  });


</script>
