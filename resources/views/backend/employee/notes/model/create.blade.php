@php
  $data=localization();
@endphp
<style type="text/css">
  @media (min-width: 576px) {
      .modal-dialog {
        max-width: 75% !important;
      }
  }
</style>
<div class="modal fade" id="modalNotes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">
                  <span class="assign_class label{{getKeyid('employee_notes_create_header',$data)}}" data-id="{{getKeyid('employee_notes_create_header',$data) }}" data-value="{{checkKey('employee_notes_create_header',$data) }}" >
                    {!! checkKey('employee_notes_create_header',$data) !!}
                  </span>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="employeeNoteForm" >
              <input type="hidden" name="flag" value="{{$flag ?? ''}}">
                <div class="modal-body mx-3">
                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input type="text" name="title" id="title" class="form-control validate @error('name') is-invalid @enderror" name="name" value="" required autocomplete="name" autofocus>
                        <label data-error="wrong" data-success="right" for="name"  >
                          <span class="assign_class label{{getKeyid('employee_notes_create_title',$data)}}" data-id="{{getKeyid('employee_notes_create_title',$data) }}" data-value="{{checkKey('employee_notes_create_title',$data) }}" >
                            {!! checkKey('employee_notes_create_title',$data) !!}
                          </span>
                          <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                          data-content="tool tip for Title"></i>
                        </label>
                    </div>
                    <input type="hidden" name="note" id="notes">
                    <label data-error="wrong" data-success="right" for="name" class="active">
                      <span class="assign_class label{{getKeyid('employee_notes_create_notes',$data)}}" data-id="{{getKeyid('employee_notes_create_notes',$data) }}" data-value="{{checkKey('employee_notes_create_notes',$data) }}" >
                        {!! checkKey('employee_notes_create_notes',$data) !!}
                      </span>
                      <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                      data-content="tool tip for Notes"></i>
                    </label>
                    <div class="md-form mb-5" id="note">
                      <div class="summernote_inner body_notes" ></div>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button class="form_submit_check btn btn-primary btn-sm" id="employee_form_create" type="submit">
                      <span class="assign_class label{{getKeyid('employee_notes_create_save',$data)}}" data-id="{{getKeyid('employee_notes_create_save',$data) }}" data-value="{{checkKey('employee_notes_create_save',$data) }}" >
                        {!! checkKey('employee_notes_create_save',$data) !!}
                      </span>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="{{asset('editor/sprite.svg.js')}}"></script>
<script type="text/javascript">
  $("#employee_form_create").click(function(e){
    e.preventDefault();
    var notes=$('#note').find('.note-editable, .card-block p' ).html();
    $('#notes').val(notes);
    $("#employeeNoteForm").submit();
  });
  $("#employeeNoteForm").on('submit',function (event) {
      if($("#title").val() ==""){
        $("#title_required_message").html('Title field required');
        return false;
      }
      event.preventDefault();
      var formData = $("#employeeNoteForm").serializeObject();
      $.ajax({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          type: "POST",
          url: '{{url('note/store')}}',
          data:new FormData(this),
          // dataType:'JSON',
          contentType:false,
          cache:false,
          processData:false,
          success: function(respone)
          {
            toastr["success"](respone.message);
            $('#clientNoteTable').DataTable().clear().destroy();
            $(".title_required_message").html();
            var url='/note/getall';
            noteAppend(url);
            $('#modalNotes').modal('hide');
          },
          error: function (error) {
              console.log(error);
          }
      });

  });

</script>
