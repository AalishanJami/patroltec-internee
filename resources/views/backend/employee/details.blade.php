@extends('backend.layouts.user')
@section('title', 'Employee Detail')
@section('headscript')
    <link rel="stylesheet" href="{{ asset('custom/css/custom_style.css') }}">
@stop
@section('content')
    @php
        $data=localization();
    @endphp
    @include('backend.layouts.user_sidebar')
    <div class="padding-left-custom remove-padding">
        <ol class="breadcrumb">
            <!-- <li class="breadcrumb-item">
                <a href="{{url('dashboard')}}">
                    <span class="assign_class label{{getKeyid('dashboard',$data)}}" data-id="{{getKeyid('dashboard',$data) }}" data-value="{{checkKey('dashboard',$data) }}" >
                        {!! checkKey('dashboard',$data) !!}
                    </span>
                </a>
            </li> -->
            <li class="breadcrumb-item ">
                <a href="{{url('employee')}}">
                    <span class="assign_class label{{getKeyid('employees',$data)}}" data-id="{{getKeyid('employees',$data) }}" data-value="{{checkKey('employees',$data) }}" >
                        {!! checkKey('employees',$data) !!}
                    </span>
                </a>
            </li>
            <li class="breadcrumb-item active_custom ">
                <span class="assign_class label{{getKeyid('detail',$data)}}" data-id="{{getKeyid('detail',$data) }}" data-value="{{checkKey('detail',$data) }}" >
                    {!! checkKey('detail',$data) !!}
                </span>
            </li>
            <li class="breadcrumb-item active_custom ">{{$employee->first_name}}</li>
        </ol>
        <div class="locations-form container-fluid form_body">
            <div class="card">
                <h5 class="card-header {{ isset($employee->warning) ? 'red_color':'info-color' }} white-text">
                    <strong>
                      <span class="assign_class label{{getKeyid('general',$data)}}" data-id="{{getKeyid('general',$data) }}" data-value="{{checkKey('general',$data) }}" >
                        {!! checkKey('general',$data) !!}
                      </span>
                    </strong>
                </h5>
                <div class="card-body px-lg-5 pt-0">
                    <form method="POST" id="employee_form" action="{{url('/employee/updateDetail')}}">
                        @csrf
                        <input type="hidden" name="company_id" class="company_id">
                        <input type="hidden"  name="id" @if(session()->has('employee_id')) value="{{session('employee_id')}}" @endif>
                        <div class="row">
                            <div class="col-sm">
                                @if(Auth()->user()->hasPermissionTo('employee_firstname_edit') || Auth::user()->all_companies == 1 )
                                    <div class="md-form">
                                        <input type="text" name="first_name" id="materialRegisterFormFirstName" class="form-control" @if(!empty($employee))  value="{{$employee->first_name}}" @endif>
                                        <label class="active label_class" for="materialRegisterFormFirstName">
                                            <span class="assign_class label{{getKeyid('first_name',$data)}}" data-id="{{getKeyid('first_name',$data) }}" data-value="{{checkKey('first_name',$data) }}" >
                                              {!! checkKey('first_name',$data) !!}
                                            </span>
                                        </label>
                                        @if ($errors->has('first_name'))
                                            <small class="text-danger" style="left: 0% !important;" id="form_name_required_message">{{ $errors->first('first_name') }}</small>
                                        @endif
                                    </div>
                                @endif
                            </div>
                            <div class="col-sm">
                                @if(Auth()->user()->hasPermissionTo('employee_surname_edit') || Auth::user()->all_companies == 1 )
                                    <div class="md-form">
                                        <input type="text" name="surname" id="surname" class="form-control" @if(!empty($employee))  value="{{$employee->surname}}" @endif>
                                        <label class="active label_class" for="surname" >
                                        <span class="assign_class label{{getKeyid('surname',$data)}}" data-id="{{getKeyid('surname',$data) }}" data-value="{{checkKey('surname',$data) }}" >
                                            {!! checkKey('surname',$data) !!}
                                        </span>
                                        </label>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm">
                                @if(Auth()->user()->hasPermissionTo('employee_managerrole_edit') || Auth::user()->all_companies == 1 )
                                    <div class="md-form">
                                        <select searchable="Search here.." id="employee_type" name="position" class="mdb-select">
                                            <option value="">Choose option</option>
                                            <option value="1" {{ ($employee->position == 1 )?'selected':''}} >Staff</option>
                                            <option value="2" {{ ($employee->position == 2 )?'selected':''}} >Manager</option>
                                            <option value="3" {{ ($employee->position == 3 )?'selected':''}} >Contract Manager</option>
                                        </select>
                                        <label class="active label_class" for="employee_type" >
                                        <span class="assign_class label{{getKeyid('employee_type',$data)}}" data-id="{{getKeyid('employee_type',$data) }}" data-value="{{checkKey('employee_type',$data) }}" >
                                          {!!   checkKey('employee_type',$data) !!}
                                        </span>
                                        </label>
                                    </div>
                                @endif
                            </div>
                            <div class="col">
                                @if(Auth()->user()->hasPermissionTo('employee_managename_edit') || Auth::user()->all_companies == 1 )
                                    <div class="md-form">
                                        <label class="active label_class" for="materialRegisterFormFirstName">
                                        <span class="assign_class label{{getKeyid('manager',$data)}}" data-id="{{getKeyid('manager',$data) }}" data-value="{{checkKey('manager',$data) }}" >
                                          {!!   checkKey('manager',$data) !!}
                                        </span>
                                        </label>
                                        <select searchable="Search here.." name="manager_id"   class="mdb-select">
                                            <option value="" >Choose option</option>
                                            @foreach($managers as $key =>$value)
                                                <option value="{{$value->id}}" {{ ($employee->manager_id == $value->id )?'selected':''}} >{{$value->first_name." ".$value->surname}}</option>
                                            @endforeach()
                                        </select>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm">
                                @if(Auth()->user()->hasPermissionTo('employee_position_edit') || Auth::user()->all_companies == 1 )
                                    <a class="text-success modal_position">
                                        <button type="button" class="btn btn-sm btn-outline-primary waves-effect form-custom-boutton form-custom-boutton" style="float: left;">
                                        <span class="form_submit_check assign_class label{{getKeyid('position',$data)}}" data-id="{{getKeyid('position',$data) }}" data-value="{{checkKey('position',$data) }}" >
                                          {!!   checkKey('position',$data) !!}
                                        </span>
                                        </button>
                                    </a>
                                    <div class="md-form">
                                        <select searchable="Search here.." name="position_id" id="position_id"  class="mdb-select">
                                            <option value="" selected disabled>Please Select</option>
                                        </select>
                                        @if ($errors->has('position'))
                                            <small class="text-danger" style="left: 0% !important;" id="form_name_required_message">{{ $errors->first('position') }}</small>
                                        @endif
                                    </div>
                                @endif
                            </div>
                            <div class="col-sm">
                                @if(Auth()->user()->hasPermissionTo('employee_sex_edit') || Auth::user()->all_companies == 1 )
                                    <div class="md-form mt-4-5">
                                        <select searchable="Search here.." id="sex" name="sex" class="mdb-select">
                                            <option value="" >Choose option</option>
                                            <option value="2" selected>Male</option>
                                            <option value="3">Female</option>
                                        </select>
                                        <label class="active label_class" for="sex" >
                                        <span class="assign_class label{{getKeyid('sex',$data)}}" data-id="{{getKeyid('sex',$data) }}" data-value="{{checkKey('sex',$data) }}" >
                                          {!!   checkKey('sex',$data) !!}
                                        </span>
                                        </label>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm">
                                @if(Auth()->user()->hasPermissionTo('employee_dofbirth_edit') || Auth::user()->all_companies == 1 )
                                    <div class="md-form">
                                        <input type="text" id="date_of_birth" name="dob" class="form-control datepicker"
                                               value="{{Carbon\Carbon::parse($employee->dob)->format('jS M Y')}}">
                                        <label class="active label_class" for="date_of_birth">
                                        <span class="assign_class label{{getKeyid('dob',$data)}}" data-id="{{getKeyid('dob',$data) }}" data-value="{{checkKey('dob',$data) }}" >
                                            {!!   checkKey('dob',$data) !!}
                                        </span>
                                        </label>
                                    </div>
                                @endif
                            </div>
                            <div class="col-sm">
                                @if(Auth()->user()->hasPermissionTo('employee_visaexdate_edit') || Auth::user()->all_companies == 1 )
                                    <div class="md-form">
                                        <input type="text" id="visa_expiry_date" name="visa_expiry_date" class="form-control datepicker" value="{{Carbon\Carbon::parse($employee->visa_expiry_date)->format('jS M Y')}}">
                                        <label class="active label_class" for="visa_expiry_date">
                                        <span class="assign_class label{{getKeyid('ved',$data)}}" data-id="{{getKeyid('ved',$data) }}" data-value="{{checkKey('ved',$data) }}" >
                                          {!!   checkKey('ved',$data) !!}
                                        </span>
                                        </label>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm">
                                @if(Auth()->user()->hasPermissionTo('employee_nationinsiranceno_edit') || Auth::user()->all_companies == 1 )
                                    <div class="md-form">
                                        <input type="text" id="national_Insurance_number" name="national_Insurance_number" class="form-control" @if(!empty($employee)) value="{{$employee->national_Insurance_number}}" @endif>
                                        <label class="active label_class" for="national_Insurance_number">
                                        <span class="assign_class label{{getKeyid('national_Insurance_number',$data)}}" data-id="{{getKeyid('national_Insurance_number',$data) }}" data-value="{{checkKey('national_Insurance_number',$data) }}" >
                                          {!!   checkKey('national_Insurance_number',$data) !!}
                                        </span>
                                        </label>
                                    </div>
                                @endif
                            </div>
                            <div class="col-sm">
                                @if(Auth()->user()->hasPermissionTo('employee_payrolnumber_edit') || Auth::user()->all_companies == 1 )
                                    <div class="md-form">
                                        <input type="text" id="payroll_number" name="payroll_number" class="form-control" @if(!empty($employee)) value="{{$employee->payroll_number}}" @endif>
                                        <label class="active label_class" for="payroll_number">
                                        <span class="assign_class label{{getKeyid('payroll_number',$data)}}" data-id="{{getKeyid('payroll_number',$data) }}" data-value="{{checkKey('payroll_number',$data) }}" >
                                          {!!   checkKey('payroll_number',$data) !!}
                                        </span>
                                        </label>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm">
                                @if(Auth()->user()->hasPermissionTo('employee_cs_card_edit') || Auth::user()->all_companies == 1 )
                                    <div class="md-form">
                                        <input type="text" id="cscs_card_detail" name="cscs_Card" class="form-control" @if(!empty($employee)) value="{{$employee->cscs_Card}}" @endif>
                                        <label class="active label_class" for="cscs_card_detail">
                                        <span class="assign_class label{{getKeyid('cscs_card',$data)}}" data-id="{{getKeyid('cscs_card',$data) }}" data-value="{{checkKey('cscs_card',$data) }}" >
                                          {!!   checkKey('cscs_card',$data) !!}
                                        </span>
                                        </label>
                                    </div>
                                @endif
                            </div>
                            <div class="col-sm">
                                @if(Auth()->user()->hasPermissionTo('employee_cs_card_ex_date_edit') || Auth::user()->all_companies == 1 )
                                    <div class="md-form">
                                        <input type="date" id="cscs_card_expiry_date" name="cscs_Card_Expiry_Date" class="form-control datepicker" value="{{Carbon\Carbon::parse($employee->cscs_Card_Expiry_Date)->format('jS M Y')}}" >
                                        <label class="active label_class" for="cscs_card_expiry_date">
                                        <span class="assign_class label{{getKeyid('cscs_card_expiry_date',$data)}}" data-id="{{getKeyid('cscs_card_expiry_date',$data) }}" data-value="{{checkKey('cscs_card_expiry_date',$data) }}" >
                                          {!!   checkKey('cscs_card_expiry_date',$data) !!}
                                        </span>
                                        </label>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                @if(Auth()->user()->hasPermissionTo('employee_sexualorientation_edit') || Auth::user()->all_companies == 1 )
                                    <a class="text-success modal_sexual">
                                        <button type="button" class="btn btn-sm btn-outline-primary waves-effect form-custom-boutton form-custom-boutton" style="float: left;">
                                        <span class="assign_class label{{getKeyid('sexual_orientation',$data)}}" data-id="{{getKeyid('sexual_orientation',$data) }}" data-value="{{checkKey('sexual_orientation',$data) }}" >
                                          {!!   checkKey('sexual_orientation',$data) !!}
                                        </span>
                                        </button>
                                    </a>
                                    <div class="md-form">
                                        <select searchable="Search here.." name="sexual_orientation_id" id="sexual_orientation_id"  class="mdb-select"></select>
                                        @if ($errors->has('sexual_orientation_id'))
                                            <small class="text-danger" style="left: 0% !important;" id="form_name_required_message">Sexual orientation field required</small>
                                        @endif
                                    </div>
                                    @endif
                            </div>
                            <div class="col">
                                @if(Auth()->user()->hasPermissionTo('employee_ethnic_edit') || Auth::user()->all_companies == 1 )
                                    <a class="text-success modal_ethnic">
                                        <button type="button" class="btn btn-sm btn-outline-primary waves-effect form-custom-boutton form-custom-boutton" style="float: left;">
                                        <span class="assign_class label{{getKeyid('ethnic',$data)}}" data-id="{{getKeyid('ethnic',$data) }}" data-value="{{checkKey('ethnic',$data) }}" >
                                          {!!   checkKey('ethnic',$data) !!}
                                        </span>
                                        </button>
                                    </a>
                                    <div class="md-form">
                                        <select searchable="Search here.." name="ethinic_origin_id" id="ethinic_origin_id"  class="mdb-select">
                                            <option value="" >Choose option</option>
                                            <option value="1" selected>Logistics</option>
                                            <option value="2">Scaffolding</option>
                                        </select>
                                        @if ($errors->has('ethinic_origin_id'))
                                            <small class="text-danger" style="left: 0% !important;" id="form_name_required_message">Ethnic field required</small>
                                        @endif
                                    </div>
                                    @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                @if(Auth()->user()->hasPermissionTo('employee_religion_edit') || Auth::user()->all_companies == 1 )
                                    <a class="text-success modal_religion">
                                        <button type="button" class="btn btn-sm btn-outline-primary waves-effect form-custom-boutton form-custom-boutton" style="float: left;">
                                            <span class="assign_class label{{getKeyid('religion',$data)}}" data-id="{{getKeyid('religion',$data) }}" data-value="{{checkKey('religion',$data) }}" >
                                                {!!   checkKey('religion',$data) !!}
                                            </span>
                                        </button>
                                    </a>
                                    <div class="md-form">
                                        <select searchable="Search here.." name="religion_id" id="religion_id"  class="mdb-select">
                                            <option value="" >Choose option</option>
                                            <option value="2" selected>Logistics</option>
                                            <option value="3">Scaffolding</option>
                                        </select>
                                        @if ($errors->has('religion_id'))
                                            <small class="text-danger" style="left: 0% !important;" id="form_name_required_message">Religion field required</small>
                                        @endif
                                    </div>
                                @endif
                            </div>
                            <div class="col">
                                @if(Auth()->user()->hasPermissionTo('employee_client_edit') || Auth::user()->all_companies == 1 )
                                    <a class="text-success modal_client">
                                        <button type="button" class="btn btn-sm btn-outline-primary waves-effect form-custom-boutton" style="float: left;">
                                            <span class="assign_class label{{getKeyid('client',$data)}}" data-id="{{getKeyid('client',$data) }}" data-value="{{checkKey('client',$data) }}" >
                                                {!! checkKey('client',$data) !!}
                                            </span>
                                        </button>
                                    </a>
                                    <div class="md-form">
                                        <select searchable="Search here.." name="client_id" id="client_id" class="mdb-select">
                                            <option value="" >Choose option</option>
                                        </select>
                                        @if ($errors->has('client_id'))
                                            <small class="text-danger" style="left: 0% !important;" id="form_name_required_message">Client field required</small>
                                        @endif
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                @if(Auth()->user()->hasPermissionTo('employee_warning_edit') || Auth::user()->all_companies == 1 )
                                    <div class="md-form">
                                        <input type="text" name="warning" id="warning" class="form-control" value="{{$employee->warning}}">
                                        <label class="active label_class" for="warning">
                                            <span class="assign_class label{{getKeyid('warnings',$data)}}" data-id="{{getKeyid('warnings',$data) }}" data-value="{{checkKey('warnings',$data) }}" >
                                                {!!  checkKey('warnings',$data) !!}
                                            </span>
                                        </label>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                @if(Auth()->user()->hasPermissionTo('employee_note_edit') || Auth::user()->all_companies == 1 )
                                    <div class="md-form">
                                        <textarea name="notes" id="employee_notes" class="form-control md-textarea" cols="30" rows="10">@if(!empty($employee)) {{$employee->notes}} @endif</textarea>
                                        <label class="active label_class"  for="employee_notes">
                                            <span class="assign_class label{{getKeyid('notes',$data)}}" data-id="{{getKeyid('notes',$data) }}" data-value="{{checkKey('notes',$data) }}" >
                                                {!!  checkKey('notes',$data) !!}
                                            </span>
                                        </label>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm">
                                <div class="md-form">
                                    <button class="form_submit_check btn btn-outline-info btn-rounded btn-block my-4 waves-effect z-depth-0" id="employee_save" type="submit">
                                        <span class="assign_class label{{getKeyid('update',$data)}}" data-id="{{getKeyid('update',$data) }}" data-value="{{checkKey('update',$data) }}" >
                                            {!!   checkKey('update',$data) !!}
                                        </span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('backend.company_list.model.index')
    @include('backend.employee.model.sexual_orientation_create')
    @include('backend.client.model.index')
    @include('backend.employee.model.religion')
    @include('backend.employee.model.position')
    @include('backend.employee.model.ethnic')

    <script type="text/javascript">
         $("#employee_save").click(function(e){
            if( $('.edit_cms_disable').css('display') == 'none' ) {
                return 0;
            }
            e.preventDefault();
            // document.details
            var company_id=$('#companies_selected_nav').children("option:selected").val();
            $('.company_id').val(company_id);
            $("#employee_form").submit();
        });
        $.ajax({
            type: "GET",
            url: '/client/getall',
            success: function(response)
            {
                var html='';
                var client_id='';
                console.log();
                @if(!empty($employee->client_id))
                    client_id='{{$employee->client_id}}';
                @endif
                for (var i = response.data.length - 1; i >= 0; i--) {
                    var selected='';
                    if (client_id==response.data[i].id){
                        selected='selected';
                    }
                    html+='<option '+selected+' value="'+response.data[i].id+'">'+response.data[i].first_name+'</option>';
                }
                if (client_id==''){
                    html+='<option disabled selected value="">Please Select</option>';
                }else{
                    html+='<option disabled value="">Please Select</option>';
                }
                $('#client_id').html(html);

            },
            error: function (error) {
                console.log(error);
            }
        });
        $(document).ready(function () {
            $('.datepicker').pickadate({selectYears:250,});
            $('.modal_position').click(function(e) {
                if ($('.edit_cms_disable').css('display') == 'none') {
                    // console.log('Editer mode On Please change your mode');
                    return 0;
                }
                $('#addPositionModal').modal('show');
            });
            $('.modal_sexual').click(function(e) {
                if ($('.edit_cms_disable').css('display') == 'none') {
                    // console.log('Editer mode On Please change your mode');
                    return 0;
                }
                $('#SexualOrientationModal').modal('show');
            });
            $('.modal_ethnic').click(function(e) {
                if ($('.edit_cms_disable').css('display') == 'none') {
                    // console.log('Editer mode On Please change your mode');
                    return 0;
                }
                $('#addethnicModal').modal('show');
            });
            $('.modal_religion').click(function(e) {
                if ($('.edit_cms_disable').css('display') == 'none') {
                    // console.log('Editer mode On Please change your mode');
                    return 0;
                }
                $('#modalReligionIndex').modal('show');
            });
            $('.modal_client').click(function(e) {
                if ($('.edit_cms_disable').css('display') == 'none') {
                    // console.log('Editer mode On Please change your mode');
                    return 0;
                }
                $('#modalClientIndex').modal('show');
            });
            var position_url='/employee/position/getall';
        });
        $.ajax({
            type: "GET",
            url: position_url,
            success: function(response)
            {
                var html='';
                for (var i = response.data.length - 1; i >= 0; i--) {
                    var selected='';
                    if(response.data[i].id == response.user.position_id )
                    {
                        selected='selected';
                    }
                    html+='<option '+selected+' value="'+response.data[i].id+'">'+response.data[i].name+'</option>';
                }
                $('#position_id').append(html);
            },
            error: function (error) {
                console.log(error);
            }
        });
    </script>
    <script src="{{ asset('custom/js/client.js') }}"></script>
    <script src="{{ asset('custom/js/sexualorientation.js') }}"></script>
    <script src="{{ asset('custom/js/ethinic.js') }}"></script>
    <script src="{{ asset('custom/js/religion.js') }}"></script>
    <script src="{{ asset('custom/js/position.js') }}"></script>
@endsection
