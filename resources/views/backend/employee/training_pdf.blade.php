@include('backend.layouts.pdf_start')
    <thead>
        <tr>
            <th style="vertical-align: text-top;" class="pdf_table_layout">
                <p class="col_text_style">First name</p>
            </th>
            <th style="vertical-align: text-top;" class="pdf_table_layout">
                <p class="col_text_style">Surname</p>
            </th>
            @foreach($doc_type as $value)
            <th style="vertical-align: text-top;" class="pdf_table_layout">
                <p class="col_text_style">{{ucfirst($value->name)}}</p>
            </th>
            @endforeach()
        </tr>
    </thead>
    <tbody>
        @foreach($data as $index)
        <tr>
            <td class="pdf_table_layout" style="vertical-align: middle">
                <p class="col_text_style">
                    {{$index->first_name}}
                </p>
            </td>
            <td class="pdf_table_layout" style="vertical-align: middle">
                <p class="col_text_style">
                    {{$index->surname}}
                </p>
            </td>
            @foreach($doc_type as $value)
                <td class="pdf_table_layout">
                    {!! position($value,$index) !!}
                </td>
            @endforeach()
        </tr>
        @endforeach()
    </tbody>
@include('backend.layouts.pdf_end')

