@php
        $data=localization();
@endphp
<div class="modal fade" id="modalRegisterCompany" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Plant & Equipment</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>   
            <form id="registernewcompany" method="POST" >
                <div class="modal-body">
                <div class="row">
                    <div class="col">
                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="" required autocomplete="name" autofocus>
                        <label data-error="wrong" data-success="right" for="name" >
                            Name
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                            data-content="tool tip for Name"></i>
                        </label>
                    </div>
                </div>
                    <div class="col">

                    <div class="md-form mb-4">
                        
                            
                                <select searchable="Search here.."  class="mdb-select md-form"   >
                                    <option  disabled selected>Equipment</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option> 
                                </select>
                      
                            
                        
                    </div> 
                </div>

                </div>

                <div class="row">
                    <div class="col">


                    <div class="md-form mb-5">
                       <i class="fas fa-sort prefix grey-text"></i>
                        <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="" required autocomplete="name" autofocus>
                        <label data-error="wrong" data-success="right" for="name" >
                            Serial Number
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                            data-content="tool tip for Serial Number"></i>
                        </label>
                    </div>
                </div>
                    <div class="col">

                    <div class="md-form mb-5">
                        <i class="fas fa-gavel prefix grey-text"></i>
                        <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="" required autocomplete="name" autofocus>
                        <label data-error="wrong" data-success="right" for="name" >
                            Make
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                            data-content="tool tip for Make"></i>
                        </label>
                    </div>
                </div>
</div>
                <div class="row">
                    <div class="col">
                    
                    <div class="md-form mb-5">
                        <i class="fas fa-code-branch prefix grey-text"></i>
                        <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="" required autocomplete="name" autofocus>
                        <label data-error="wrong" data-success="right" for="name" >
                            Model
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                            data-content="tool tip for Model"></i>
                        </label>
                    </div>
                </div>
                    <div class="col">

                    <div class="md-form mb-5">
                        <i class="fas fa-calendar-alt prefix grey-text"></i>
                        <input type="date" class="form-control validate @error('name') is-invalid @enderror" name="name" value="" required autocomplete="name" autofocus>
                        <label data-error="wrong" data-success="right" for="name" >
                            Last Service Date
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                            data-content="tool tip for Last Service Date"></i>
                        </label>
                    </div>
                </div>

                </div>
                <div class="row">
                    <div class="col">

                    <div class="md-form mb-5">
                        <i class="fas fa-calendar-alt prefix grey-text"></i>
                        <input type="date" class="form-control validate @error('name') is-invalid @enderror" name="name" value="" required autocomplete="name" autofocus>
                        <label data-error="wrong" data-success="right" for="name" >
                            Next service Date
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                            data-content="tool tip for Next service Date"></i>
                        </label>
                    </div>
                </div>
                    <div class="col">

                    <div class="md-form mb-5">
                        <i class="fas fa-bullhorn prefix grey-text"></i>
                        <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="" required autocomplete="name" autofocus>
                        <label data-error="wrong" data-success="right" for="name" >
                            Condition
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                            data-content="tool tip for Condition"></i>
                        </label>
                    </div>
                </div>

                </div>

                 <div class="row">
                    <div class="col">
                    <div class="md-form mb-5">
                        <i class="far fa-sticky-note prefix grey-text"></i>
                        <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="" required autocomplete="name" autofocus>
                        <label data-error="wrong" data-success="right" for="name" >
                            Notes
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                            data-content="tool tip for Other Notes"></i>
                        </label>
                    </div>
                </div>
            </div>
                   

                </div>


                <div class="modal-footer d-flex justify-content-center">
                    <button class="btn btn-primary btn-sm" type="submit"><span class="assign_class label{{getKeyid('save',$data)}}" data-id="{{getKeyid('save',$data) }}" data-value="{{checkKey('save',$data) }}" >{!! checkKey('save',$data) !!} </span></button>
                </div>
            </form>
        </div>
    </div>
</div>
