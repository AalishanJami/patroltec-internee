@php
    $data=localization();
@endphp
<div class="modal fade" id="modalplantandequipment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Plant & Equipment</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="editplantAndEquipmentForm" action="javascript:;" method="POST" >
                <div class="modal-body">
                    @csrf
                    <input type="hidden" id="location_breakdown_id">
                    <div class="row">
                        <div class="col">
                            <div class="md-form mb-5">
                                <i class="fas fa-user prefix grey-text"></i>
                                <span class="edit_form_labels">Name</span>
                                <input type="text" class="form-control" id="location_breakdown_name_edit" name="name">

                            </div>
                        </div>
                        <div class="col">
                            <div class="md-form edit_form_labels_selectdropdown mb-5">
                                <i class="fas fa-gavel prefix grey-text"></i>
                                <span class="edit_form_labels">Equipment</span>
                                <select searchable="Search here.." id="location_breakdown_edit"  class="mdb-select">
                                    <option  disabled selected>Equipment</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="md-form mb-5">
                                <i class="fas fa-sort prefix grey-text"></i>
                                <span class="edit_form_labels">Serial Number</span>
                                <input type="text" class="form-control" id="serial_number_edit" name="serial_number">
                            </div>
                        </div>
                        <div class="col">
                            <div class="md-form mb-5">
                                <i class="fas fa-gavel prefix grey-text"></i>
                                <span class="edit_form_labels">Make</span>
                                <input type="text" class="form-control" id="make_edit" name="make" value="" required autocomplete="name">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="md-form mb-5">
                                <i class="fas fa-code-branch prefix grey-text"></i>
                                <span class="edit_form_labels">
                                    Model
                                </span>
                                <input type="text" class="form-control" id="model_edit" name="model">
                            </div>
                        </div>
                        <div class="col">
                            <div class="md-form mb-5">
                                <i class="fas fa-calendar-alt prefix grey-text"></i>
                                <span class="edit_form_labels">
                                    Last Service Date
                                </span>
                                <input type="text" class="form-control datepicker" name="last_service_date" id="last_service_date_edit">

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="md-form mb-5">
                                <i class="fas fa-calendar-alt prefix grey-text"></i>
                                <span class="edit_form_labels">
                                    Next service Date
                                </span>
                                <input type="text" class="form-control datepicker" name="next_service_date" id="next_service_date_edit">
                            </div>
                        </div>
                        <div class="col">
                            <div class="md-form mb-5">
                                <i class="fas fa-bullhorn prefix grey-text"></i>
                                <span class="edit_form_labels">
                                    Condition
                                </span>
                                <input type="text" class="form-control" id="condition_edit" name="condition">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button class="btn btn-primary btn-sm" id="update_plantandequipment" type="submit"><span class="assign_class label{{getKeyid('save',$data)}}" data-id="{{getKeyid('save',$data) }}" data-value="{{checkKey('save',$data) }}" >{!! checkKey('save',$data) !!} </span></button>
                </div>
            </form>
        </div>
    </div>
</div>
