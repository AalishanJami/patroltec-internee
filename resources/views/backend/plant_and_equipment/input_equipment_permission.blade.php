@if(Auth()->user()->all_companies == 1)
    <input type="hidden" id="equipment_checkbox" value="1">
    <input type="hidden" id="delete_equipment" value="1">
    <input type="hidden" id="equipment_name" value="1">
    <!-------edit------->
    <input type="hidden" id="equipment_name_edit" value="1">
    <!-------create------->
    <input type="hidden" id="equipment_name_create" value="1">
    <!-- hard delete -->
    <input type="hidden" id="hard_delete_equipment" value="1">
    <input type="hidden" id="edit_equipment" value="1">
@else
    <input type="hidden" id="equipment_checkbox" value="{{Auth()->user()->hasPermissionTo('equipment_checkbox')}}">
    <input type="hidden" id="delete_equipment" value="{{Auth()->user()->hasPermissionTo('delete_equipment')}}">
    <input type="hidden" id="edit_equipment" value="{{Auth()->user()->hasPermissionTo('edit_equipment')}}">
    <input type="hidden" id="equipment_name" value="{{Auth()->user()->hasPermissionTo('equipment_name')}}">
    <!-------edit------->
    <input type="hidden" id="equipment_name_edit" value="{{Auth()->user()->hasPermissionTo('equipment_name_edit')}}">
    <!-------create------->
    <input type="hidden" id="equipment_name_create" value="{{Auth()->user()->hasPermissionTo('equipment_name_create')}}">
    <!-- hard delete -->
    <input type="hidden" id="hard_delete_equipment" value="{{Auth()->user()->hasPermissionTo('hard_delete_equipment')}}">
@endif
