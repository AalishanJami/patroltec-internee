@if(Auth()->user()->all_companies == 1)
    <input type="hidden" id="equipmentGroup_checkbox" value="1">
    <input type="hidden" id="equipmentGroup_qr" value="1">
    <input type="hidden" id="equipmentGroup_name" value="1">
    <input type="hidden" id="equipmentGroup_equipement" value="1">
    <input type="hidden" id="equipmentGroup_serialNumber" value="1">
    <input type="hidden" id="equipmentGroup_make" value="1">
    <input type="hidden" id="equipmentGroup_model" value="1">
    <input type="hidden" id="equipmentGroup_lastserviceDate" value="1">
    <input type="hidden" id="equipmentGroup_nextserviceDate" value="1">
    <input type="hidden" id="equipmentGroup_condition" value="1">
    <input type="hidden" id="edit_equipmentGroup" value="1">
    <input type="hidden" id="delete_equipmentGroup" value="1">

    <!----------create------------->
    <input type="hidden" id="equipmentGroup_qr_create" value="1">
    <input type="hidden" id="equipmentGroup_name_create" value="1">
    <input type="hidden" id="equipmentGroup_equipement_create" value="1">
    <input type="hidden" id="equipmentGroup_serialNumber_create" value="1">
    <input type="hidden" id="equipmentGroup_make_create" value="1">
    <input type="hidden" id="equipmentGroup_model_create" value="1">
    <input type="hidden" id="equipmentGroup_lastserviceDate_create" value="1">
    <input type="hidden" id="equipmentGroup_nextserviceDate_create" value="1">
    <input type="hidden" id="equipmentGroup_condition_create" value="1">

    <!----------edit------------->
    <input type="hidden" id="equipmentGroup_qr_edit" value="1">
    <input type="hidden" id="equipmentGroup_name_edit" value="1">
    <input type="hidden" id="equipmentGroup_equipement_edit" value="1">
    <input type="hidden" id="equipmentGroup_serialNumber_edit" value="1">
    <input type="hidden" id="equipmentGroup_make_edit" value="1">
    <input type="hidden" id="equipmentGroup_model_edit" value="1">
    <input type="hidden" id="equipmentGroup_lastserviceDate_edit" value="1">
    <input type="hidden" id="equipmentGroup_nextserviceDate_edit" value="1">
    <input type="hidden" id="equipmentGroup_condition_edit" value="1">
@else
    <input type="hidden" id="equipmentGroup_checkbox" value="{{Auth()->user()->hasPermissionTo('equipmentGroup_qr')}}">
    <input type="hidden" id="equipmentGroup_qr" value="{{Auth()->user()->hasPermissionTo('equipmentGroup_qr')}}">
    <input type="hidden" id="equipmentGroup_name" value="{{Auth()->user()->hasPermissionTo('equipmentGroup_name')}}">
    <input type="hidden" id="equipmentGroup_equipement" value="{{Auth()->user()->hasPermissionTo('equipmentGroup_equipement')}}">
    <input type="hidden" id="equipmentGroup_serialNumber" value="{{Auth()->user()->hasPermissionTo('equipmentGroup_serialNumber')}}">
    <input type="hidden" id="equipmentGroup_make" value="{{Auth()->user()->hasPermissionTo('equipmentGroup_make')}}">
    <input type="hidden" id="equipmentGroup_model" value="{{Auth()->user()->hasPermissionTo('equipmentGroup_model')}}">
    <input type="hidden" id="equipmentGroup_lastserviceDate" value="{{Auth()->user()->hasPermissionTo('equipmentGroup_lastserviceDate')}}">
    <input type="hidden" id="equipmentGroup_nextserviceDate" value="{{Auth()->user()->hasPermissionTo('equipmentGroup_nextserviceDate')}}">
    <input type="hidden" id="equipmentGroup_condition" value="{{Auth()->user()->hasPermissionTo('equipmentGroup_condition')}}">
    <input type="hidden" id="edit_equipmentGroup" value="{{Auth()->user()->hasPermissionTo('edit_equipmentGroup')}}">
    <input type="hidden" id="delete_equipmentGroup" value="{{Auth()->user()->hasPermissionTo('delete_equipmentGroup')}}">

    <!----------create------------->
    <input type="hidden" id="equipmentGroup_qr_create" value="{{Auth()->user()->hasPermissionTo('equipmentGroup_qr_create')}}">
    <input type="hidden" id="equipmentGroup_name_create" value="{{Auth()->user()->hasPermissionTo('equipmentGroup_name_create')}}">
    <input type="hidden" id="equipmentGroup_equipement_create" value="{{Auth()->user()->hasPermissionTo('equipmentGroup_equipement_create')}}">
    <input type="hidden" id="equipmentGroup_serialNumber_create" value="{{Auth()->user()->hasPermissionTo('equipmentGroup_serialNumber_create')}}">
    <input type="hidden" id="equipmentGroup_make_create" value="{{Auth()->user()->hasPermissionTo('equipmentGroup_make_create')}}">
    <input type="hidden" id="equipmentGroup_model_create" value="{{Auth()->user()->hasPermissionTo('equipmentGroup_model_create')}}">
    <input type="hidden" id="equipmentGroup_lastserviceDate_create" value="{{Auth()->user()->hasPermissionTo('equipmentGroup_lastserviceDate_create')}}">
    <input type="hidden" id="equipmentGroup_nextserviceDate_create" value="{{Auth()->user()->hasPermissionTo('equipmentGroup_nextserviceDate_create')}}">
    <input type="hidden" id="equipmentGroup_condition_create" value="{{Auth()->user()->hasPermissionTo('equipmentGroup_condition_create')}}">

    <!----------edit------------->
    <input type="hidden" id="equipmentGroup_qr_edit" value="{{Auth()->user()->hasPermissionTo('equipmentGroup_qr_edit')}}">
    <input type="hidden" id="equipmentGroup_name_edit" value="{{Auth()->user()->hasPermissionTo('equipmentGroup_name_edit')}}">
    <input type="hidden" id="equipmentGroup_equipement_edit" value="{{Auth()->user()->hasPermissionTo('equipmentGroup_equipement_edit')}}">
    <input type="hidden" id="equipmentGroup_serialNumber_edit" value="{{Auth()->user()->hasPermissionTo('equipmentGroup_serialNumber_edit')}}">
    <input type="hidden" id="equipmentGroup_make_edit" value="{{Auth()->user()->hasPermissionTo('equipmentGroup_make_edit')}}">
    <input type="hidden" id="equipmentGroup_model_edit" value="{{Auth()->user()->hasPermissionTo('equipmentGroup_model_edit')}}">
    <input type="hidden" id="equipmentGroup_lastserviceDate_edit" value="{{Auth()->user()->hasPermissionTo('equipmentGroup_lastserviceDate_edit')}}">
    <input type="hidden" id="equipmentGroup_nextserviceDate_edit" value="{{Auth()->user()->hasPermissionTo('equipmentGroup_nextserviceDate_edit')}}">
    <input type="hidden" id="equipmentGroup_condition_edit" value="{{Auth()->user()->hasPermissionTo('equipmentGroup_condition_edit')}}">
@endif
