<table>
    <thead>
    <tr>
        @if(Auth()->user()->hasPermissionTo('equipmentGroup_name_excel_export') || Auth::user()->all_companies == 1 )
            <th> Name</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('equipmentGroup_equipement_excel_export') || Auth::user()->all_companies == 1 )
            <th> Equipment</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('equipmentGroup_serialNumber_excel_export') || Auth::user()->all_companies == 1 )
            <th> Serial Number</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('equipmentGroup_make_excel_export') || Auth::user()->all_companies == 1 )
            <th> Make</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('equipmentGroup_model_excel_export') || Auth::user()->all_companies == 1 )
            <th> Model</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('equipmentGroup_lastserviceDate_excel_export') || Auth::user()->all_companies == 1 )
            <th> Last Service Date</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('equipmentGroup_nextserviceDate_excel_export') || Auth::user()->all_companies == 1 )
            <th> Next service Date</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('equipmentGroup_condition_excel_export') || Auth::user()->all_companies == 1 )
            <th> Condition</th>
        @endif
        {{--          <th>Notes</th>--}}
    </tr>
    </thead>
    <tbody>
    @foreach($data as $row)
        <tr>
            @if(Auth()->user()->hasPermissionTo('equipmentGroup_name_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{ $row->name }}</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('equipmentGroup_equipement_excel_export') || Auth::user()->all_companies == 1 )
                @if(!empty($row->equipment))
                    <td>{{ $row->equipment->name }}</td>
                @endif
            @endif
            @if(Auth()->user()->hasPermissionTo('equipmentGroup_serialNumber_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{ $row->serial_number }}</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('equipmentGroup_make_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{ $row->make }}</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('equipmentGroup_model_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{ $row->model }}</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('equipmentGroup_lastserviceDate_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{ $row->last_service_date }}</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('equipmentGroup_nextserviceDate_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{ $row->next_service_date }}</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('equipmentGroup_condition_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{ $row->condition }}</td>
            @endif
            {{--              <td>{{ $row->notes }}</td>--}}
        </tr>
    @endforeach
    </tbody>
</table>
