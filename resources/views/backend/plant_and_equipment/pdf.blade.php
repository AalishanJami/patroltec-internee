@include('backend.layouts.pdf_start')
  <thead>
    <tr>
      @if( Auth::user()->all_companies == 1 )
      <th style="vertical-align: text-top;" class="pdf_table_layout"> QR</th>
      @endif
      @if(Auth::user()->all_companies == 1 )
      <th style="vertical-align: text-top;" class="pdf_table_layout"> Name</th>
      @endif
      @if(Auth::user()->all_companies == 1 )
           <th style="vertical-align: text-top;" class="pdf_table_layout">Equipment</th>
        @else
            <td class="pdf_table_layout"><p class="col_text_style">
            </p></td>
        @endif
        @if( Auth::user()->all_companies == 1 )
           <th style="vertical-align: text-top;" class="pdf_table_layout">Serial Number</th>
        @else
            <td class="pdf_table_layout"><p class="col_text_style">
            </p></td>
        @endif
        @if(Auth()->user()->hasPermissionTo('equipmentGroup_make_pdf_export') || Auth::user()->all_companies == 1 )
           <th style="vertical-align: text-top;" class="pdf_table_layout">Make</th>
        @else
            <td class="pdf_table_layout"><p class="col_text_style">
            </p></td>
        @endif
        @if( Auth::user()->all_companies == 1 )
           <th style="vertical-align: text-top;" class="pdf_table_layout">Model</th>
        @else
            <td class="pdf_table_layout"><p class="col_text_style">
            </p></td>
        @endif
        @if(Auth::user()->all_companies == 1 )
           <th style="vertical-align: text-top;" class="pdf_table_layout">Last Service Date</th>
        @else
            <td class="pdf_table_layout"><p class="col_text_style">
            </p></td>
        @endif
        @if( Auth::user()->all_companies == 1 )
           <th style="vertical-align: text-top;" class="pdf_table_layout">Next Service Date</th>
        @else
            <td class="pdf_table_layout"><p class="col_text_style">
            </p></td>
        @endif
        @if( Auth::user()->all_companies == 1 )
           <th style="vertical-align: text-top;" class="pdf_table_layout">Condition</th>
        @else
            <td class="pdf_table_layout"><p class="col_text_style">
            </p></td>
        @endif
    </tr>
  </thead>
  <tbody>
  @foreach($data as $value)
    <tr>
      @if( Auth::user()->all_companies == 1 )
          <td class="pdf_table_layout"><p class="col_text_style"><img src="{{public_path('qr/'.$value->name.'.png')}}"></p></td>
      @endif
      @if(Auth::user()->all_companies == 1 )
          <td class="pdf_table_layout"><p class="col_text_style">{{ $value->name }}</p></td>
      @endif
       @if( Auth::user()->all_companies == 1 )
            <td class="pdf_table_layout"><p class="col_text_style">{{$value->serial_number}}
            </p></td>
        @else
            <td class="pdf_table_layout"><p class="col_text_style">
            </p></td>
        @endif
        <td class="pdf_table_layout"><p class="col_text_style">
            @if (!empty($value->equipment->name))
                {{$value->equipment->name}}
            @endif

        </p></td>
                @if(Auth()->user()->hasPermissionTo('equipmentGroup_make_pdf_export') || Auth::user()->all_companies == 1 )
                    <td class="pdf_table_layout"><p class="col_text_style">{{$value->make}}
                    </p></td>
                @else
                    <td class="pdf_table_layout"><p class="col_text_style">
                    </p></td>
                @endif
                @if( Auth::user()->all_companies == 1 )
                    <td class="pdf_table_layout"><p class="col_text_style">{{$value->model}}
                    </p></td>
                @else
                    <td class="pdf_table_layout"><p class="col_text_style">
                    </p></td>
                @endif

                @if(Auth::user()->all_companies == 1 )
                    @if(isset($value->last_service_date))
                        <td class="pdf_table_layout"><p class="col_text_style">{{date('jS M Y',strtotime($value->last_service_date))}}
                        </p></td>
                    @else
                        <td class="pdf_table_layout"><p class="col_text_style">
                            </p></td>
                    @endif
                @else
                    <td class="pdf_table_layout"><p class="col_text_style">
                    </p></td>
                @endif

                @if( Auth::user()->all_companies == 1 )
                    @if(isset($value->next_service_date))
                       <td class="pdf_table_layout"><p class="col_text_style">{{date('jS M Y',strtotime($value->next_service_date))}}
                        </p></td>
                    @else
                        <td class="pdf_table_layout"><p class="col_text_style">
                            </p></td>

                    @endif
                @else
                    <td class="pdf_table_layout"><p class="col_text_style">
                    </p></td>
                @endif
                @if( Auth::user()->all_companies == 1 )
                    <td class="pdf_table_layout"><p class="col_text_style">{{$value->condition}}
                    </p></td>
                @else
                    <td class="pdf_table_layout"><p class="col_text_style">
                    </p></td>
                @endif
    </tr>
  @endforeach
  </tbody>
@include('backend.layouts.pdf_end')
