@extends('backend.layouts.backend')
@section('content')
    @php
       $data=localization();
    @endphp
        {{ Breadcrumbs::render('plant_and_equipment') }}
        <div class="card">

            <div class="card-body">
                <div id="table" class="table-editable table-responsive">
        <span class="table-add float-right mb-3 mr-2">
            <a class="text-success"  data-toggle="modal" data-target="#modalRegisterCompany">
                <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
            </a>
        </span>
        <button class="btn btn-danger btn-sm">
            Delete Selected Plant & Equipment
        </button>
        <button class="btn btn-primary btn-sm">
          Restore Deleted Plant & Equipment
        </button>

        <button  style="display: none;">
            Show Active Plant & Equipment
        </button>
        <button class="btn btn-warning btn-sm">
           Excel Export
        </button>
        <button class="btn btn-success btn-sm">
            Word Export
        </button>
    <table  class=" company_table_static table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th id="action">
            </th>
            <th>QR</th>
            <th> Name
            </th>
            <th> Equipment
            </th>
            <th> Serial Number
            </th>
            <th> Make
            </th>
            <th> Model
            </th>
            <th> Last Service Date
            </th>
            <th> Next service Date
            </th>
            <th> Condition
            </th>
            <th> Notes
            </th>
            
        </tr>
        </thead>
            <tr>
                <td class="pt-3-half" contenteditable="true">
                    <a  data-toggle="modal" data-target="#modalEditForm" class="btn btn-primary btn-xs"><i class="fas fa-pencil-alt"></i></a>
                    <a type="button"class="btn btn-danger btn-xs my-0" onclick="deleteplant_and_equipmentdata()"><i class="fas fa-trash"></i></a>

                </td>
                   <td class="pt-3-half" contenteditable="true"><img src="qr.png" alt="" style="width:50px; height:50px"></td>
                <td class="pt-3-half" contenteditable="true">Laser Plant</td>
                <td class="pt-3-half" contenteditable="true">Laser</td>
                <td class="pt-3-half" contenteditable="true">007</td>
                <td class="pt-3-half" contenteditable="true">2005</td>
                <td class="pt-3-half" contenteditable="true">2010</td>
                <td class="pt-3-half" contenteditable="true">28 Jan 2020</td>
                <td class="pt-3-half" contenteditable="true">29 Feb 2020</td>
                <td class="pt-3-half" contenteditable="true">Excellent</td>
                <td class="pt-3-half" contenteditable="true">We use best products</td>
                
            </tr>
        <tbody>
        </tbody>
    </table>
</div>
</div>
</div>
    @component('backend.plant_and_equipment.model.create')
    @endcomponent

     {{--    Edit Modal--}}
    @component('backend.plant_and_equipment.model.edit')
    @endcomponent
    {{--    END Edit Modal--}}



@endsection
<script type="text/javascript">
function deleteplant_and_equipmentdata()
{

    swal({
        title:'Are you sure?',
        text: "Delete Record.",
        type: "error",
        showCloseButton: true,
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, Delete!",
        cancelButtonText: "Cancel!",
        closeOnConfirm: false,
        customClass: "confirm_class",
        closeOnCancel: false,
    },
    function(isConfirm){
        swal.close();
 });


}

</script>
