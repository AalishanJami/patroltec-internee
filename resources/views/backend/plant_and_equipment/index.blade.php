@extends('backend.layouts.detail')
@section('title', 'Plant and Equipment')
@section('headscript')
    <script src="{{ asset('custom/js/equipment.js') }}"></script>
    <script src="{{ asset('custom/js/plant_equipment.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('custom/css/custom_style.css') }}">
@stop

@section('content')
    @php
        $data=localization();
    @endphp
    @include('backend.layouts.detail_sidebar')
    <div class="padding-left-custom remove-padding">
        {!! breadcrumInner('plant_and_equipment',Session::get('project_id'),'locations','/project/detail/'.Session::get('project_id'),'project','/project') !!}
        <div class="card">
            <div class="card-body">
                <ul  class="nav nav-tabs" id="myTab" role="tablist">
                    @if(Auth()->user()->hasPermissionTo('view_equipmentGroup') || Auth::user()->all_companies == 1 )
                        <li class="nav-item">
                            <a class="nav-link active equipment_group_refresh" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile"
                               aria-selected="false">
                               <span class="assign_class label{{getKeyid('equipmentGroup',$data)}}" data-id="{{getKeyid('equipmentGroup',$data) }}" data-value="{{checkKey('equipmentGroup',$data) }}" >
                                    {!! checkKey('equipmentGroup',$data) !!}
                                </span>
                            </a>
                        </li>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('view_equipment') || Auth::user()->all_companies == 1 )
                        <li class="nav-item">
                            <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact"
                               aria-selected="false">
                           <span class="assign_class label{{getKeyid('equipment',$data)}}" data-id="{{getKeyid('equipment',$data) }}" data-value="{{checkKey('equipment',$data) }}" >
                                {!! checkKey('equipment',$data) !!}
                            </span>
                            </a>
                        </li>
                    @endif
                </ul>
                <br>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        @if(Auth()->user()->hasPermissionTo('create_equipmentGroup') || Auth::user()->all_companies == 1 )
                            <span class="add-equipment float-right mb-3 mr-2">
                                <a  class="text-success">
                                    <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
                                </a>
                            </span>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_delete_equipmentGroup') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-danger btn-sm" id="deleteselectedequipment">
                                <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                                    {!! checkKey('selected_delete',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('hard_delete_equipmentGroup') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-danger btn-sm" style="display: none;" id="deleteselectedequipmentSoft">
                                <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                                    {!! checkKey('selected_delete',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_active_button_equipmentGroup') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-success btn-sm" style="display: none;" id="selectedactivebuttonequipment">
                                <span class="assign_class label{{getKeyid('selected_active',$data)}}" data-id="{{getKeyid('selected_active',$data) }}" data-value="{{checkKey('selected_active',$data) }}" >
                                    {!! checkKey('selected_active',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('restore_delete_equipmentGroup') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-primary btn-sm" id="restorebuttonequipment">
                                <span class="assign_class label{{getKeyid('restore',$data)}}" data-id="{{getKeyid('restore',$data) }}" data-value="{{checkKey('restore',$data) }}" >
                                    {!! checkKey('restore',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('active_equipmentGroup') || Auth::user()->all_companies == 1 )
                            <button  style="display: none;" class="btn btn-primary btn-sm" id="activebuttonequipment">
                                <span class="assign_class label{{getKeyid('show_active',$data)}}" data-id="{{getKeyid('show_active',$data) }}" data-value="{{checkKey('show_active',$data) }}" >
                                    {!! checkKey('show_active',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('csv_equipmentGroup') || Auth::user()->all_companies == 1 )
                            <form class="form-style" method="POST" action="{{ url('plant_and_equipment/export/excel') }}">
                                <input type="hidden" id="export_excel_equipment" name="export_excel_equipment" value="1">
                                <input type="hidden" class="plant_equipment_export" name="excel_array" value="1">
                                {!! csrf_field() !!}
                                <button  type="submit" id="export_excel_equipment_btn" class="form_submit_check btn btn-warning btn-sm">
                                    <span class="assign_class label{{getKeyid('excel_export',$data)}}" data-id="{{getKeyid('excel_export',$data) }}" data-value="{{checkKey('excel_export',$data) }}" >
                                      {!! checkKey('excel_export',$data) !!}
                                    </span>
                                </button>
                            </form>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('word_equipmentGroup') || Auth::user()->all_companies == 1 )
                            <form class="form-style" method="POST" action="{{ url('plant_and_equipment/export/world') }}">
                                <input type="hidden" id="export_world_equipment" name="export_world_equipment" value="1">
                                <input type="hidden" class="plant_equipment_export" name="word_array" value="1">
                                {!! csrf_field() !!}
                                <button  type="submit" id="export_world_equipment_btn" class="form_submit_check btn btn-success btn-sm">
                                    <span class="assign_class label{{getKeyid('word_export',$data)}}" data-id="{{getKeyid('word_export',$data) }}" data-value="{{checkKey('word_export',$data) }}" >
                                        {!! checkKey('word_export',$data) !!}
                                    </span>
                                </button>
                            </form>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('pdf_equipmentGroup') || Auth::user()->all_companies == 1 )
                            <form  class="form-style" {{pdf_view('location_breakdowns')}}  method="POST" action="{{ url('plant_and_equipment/export/pdf') }}">
                                <input type="hidden" id="export_world_pdf" name="export_world_pdf" value="1">
                                <input type="hidden" class="plant_equipment_export" name="pdf_array" value="1">
                                {!! csrf_field() !!}
                                <button  type="submit" id="export_world_pdf_btn"  class="form_submit_check btn btn-secondary btn-sm">
                                    <span class="assign_class label{{getKeyid('pdf_export',$data)}}" data-id="{{getKeyid('pdf_export',$data) }}" data-value="{{checkKey('pdf_export',$data) }}" >
                                        {!! checkKey('pdf_export',$data) !!}
                                    </span>
                                </button>
                            </form>
                        @endif
                        <div id="table-equipment" class=" table-editable table-responsive">
                            <table id="plant_and_equipment_table" class=" table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th class="no-sort all_checkboxes_style">
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input equipmentGroup_checked" id="equipmentGroup_checkbox_all">
                                            <label class="form-check-label" for="equipmentGroup_checkbox_all">
                                                <span class="assign_class label{{getKeyid('all',$data)}}" data-id="{{getKeyid('all',$data) }}" data-value="{{checkKey('all',$data) }}" >
                                                    {!! checkKey('all',$data) !!}
                                                </span>
                                            </label>
                                        </div>
                                    </th>
                                    <th>
                                        <span class="assign_class label{{getKeyid('qr',$data)}}" data-id="{{getKeyid('qr',$data) }}" data-value="{{checkKey('qr',$data) }}" >
                                            {!! checkKey('qr',$data) !!}
                                        </span>
                                    </th>
                                    <th>
                                        <span class="assign_class label{{getKeyid('name',$data)}}" data-id="{{getKeyid('name',$data) }}" data-value="{{checkKey('name',$data) }}" >
                                            {!! checkKey('name',$data) !!}
                                        </span>
                                    </th>
                                    <th>
                                        <span class="assign_class label{{getKeyid('equipment',$data)}}" data-id="{{getKeyid('equipment',$data) }}" data-value="{{checkKey('equipment',$data) }}" >
                                            {!! checkKey('equipment',$data) !!}
                                        </span>
                                    </th>
                                    <th>
                                        <span class="assign_class label{{getKeyid('serial_number',$data)}}" data-id="{{getKeyid('serial_number',$data) }}" data-value="{{checkKey('serial_number',$data) }}" >
                                            {!! checkKey('serial_number',$data) !!}
                                        </span>
                                    </th>
                                    <th>
                                        <span class="assign_class label{{getKeyid('make',$data)}}" data-id="{{getKeyid('make',$data) }}" data-value="{{checkKey('make',$data) }}" >
                                            {!! checkKey('make',$data) !!}
                                        </span>
                                    </th>
                                    <th>
                                        <span class="assign_class label{{getKeyid('model',$data)}}" data-id="{{getKeyid('model',$data) }}" data-value="{{checkKey('model',$data) }}" >
                                            {!! checkKey('model',$data) !!}
                                        </span>
                                    </th>
                                    <th>
                                        <span class="assign_class label{{getKeyid('last_service_date',$data)}}" data-id="{{getKeyid('last_service_date',$data) }}" data-value="{{checkKey('last_service_date',$data) }}" >
                                            {!! checkKey('last_service_date',$data) !!}
                                        </span>
                                    </th>
                                    <th>
                                        <span class="assign_class label{{getKeyid('next_service_date',$data)}}" data-id="{{getKeyid('next_service_date',$data) }}" data-value="{{checkKey('next_service_date',$data) }}" >
                                            {!! checkKey('next_service_date',$data) !!}
                                        </span>
                                    </th>
                                    <th>
                                        <span class="assign_class label{{getKeyid('condition',$data)}}" data-id="{{getKeyid('condition',$data) }}" data-value="{{checkKey('condition',$data) }}" >
                                            {!! checkKey('condition',$data) !!}
                                        </span>
                                    </th>
                                    <th class="no-sort" id="action"></th>
                                    {{--                                    <th class="no-sort"></th>--}}
                                </tr>
                                </thead>
                                <tbody id="equipment_data"></tbody>
                            </table>
                        </div>
                        <!---------equipment group column permission------------>
                        @include('backend.plant_and_equipment.input_equipmentgroup_permission')
                    </div>
                    <div class="tab-pane fade " id="contact" role="tabpanel" aria-labelledby="contact-tab">
                        @if(Auth()->user()->hasPermissionTo('create_equipment') || Auth::user()->all_companies == 1 )
                            <span class="add-equipment2 float-right mb-3 mr-2">
                                <a  class="text-success">
                                    <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
                                </a>
                            </span>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('selected_delete_equipment') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-danger btn-sm" id="deleteselectedequipment2">
                                <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                                    {!! checkKey('selected_delete',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('hard_delete_equipment') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-danger btn-sm" style="display: none;" id="deleteselectedequipment2Soft">
                                <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                                    {!! checkKey('selected_delete',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('active_equipment') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-success btn-sm" style="display: none;" id="selectedactivebuttonequipment2">
                                <span class="assign_class label{{getKeyid('selected_active',$data)}}" data-id="{{getKeyid('selected_active',$data) }}" data-value="{{checkKey('selected_active',$data) }}" >
                                    {!! checkKey('selected_active',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('restore_delete_equipment') || Auth::user()->all_companies == 1 )
                            <button class="btn btn-primary btn-sm" id="restorebuttonequipment2">
                                <span class="assign_class label{{getKeyid('restore',$data)}}" data-id="{{getKeyid('restore',$data) }}" data-value="{{checkKey('restore',$data) }}" >
                                    {!! checkKey('restore',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('active_equipment') || Auth::user()->all_companies == 1 )
                            <button  style="display: none;" class="btn btn-primary btn-sm" id="activebuttonequipment2">
                                <span class="assign_class label{{getKeyid('show_active',$data)}}" data-id="{{getKeyid('show_active',$data) }}" data-value="{{checkKey('show_active',$data) }}" >
                                    {!! checkKey('show_active',$data) !!}
                                </span>
                            </button>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('csv_equipment') || Auth::user()->all_companies == 1 )
                            <form class="form-style" method="POST" action="{{ url('equipment/export/excel') }}">
                                <input type="hidden" id="export_excel_equipment2" name="export_excel_equipment2" value="1">
                                {!! csrf_field() !!}
                                <input type="hidden" class="equipment_export" name="excel_array" value="1">
                                <button  type="submit" id="export_excel_equipment2_btn" class="form_submit_check btn btn-warning btn-sm">
                                    <span class="assign_class label{{getKeyid('excel_export',$data)}}" data-id="{{getKeyid('excel_export',$data) }}" data-value="{{checkKey('excel_export',$data) }}" >
                                        {!! checkKey('excel_export',$data) !!}
                                    </span>
                                </button>
                            </form>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('word_equipment') || Auth::user()->all_companies == 1 )
                            <form class="form-style" method="POST" action="{{ url('equipment/export/world') }}">
                                <input type="hidden" id="export_world_equipment2" name="export_world_equipment2" value="1">
                                {!! csrf_field() !!}
                                <input type="hidden" class="equipment_export" name="word_array" value="1">
                                <button  type="submit" id="export_world_equipment2_btn" class="form_submit_check btn btn-success btn-sm">
                                    <span class="assign_class label{{getKeyid('word_export',$data)}}" data-id="{{getKeyid('word_export',$data) }}" data-value="{{checkKey('word_export',$data) }}" >
                                        {!! checkKey('word_export',$data) !!}
                                    </span>
                                </button>
                            </form>
                        @endif
                        @if(Auth()->user()->hasPermissionTo('pdf_equipment') || Auth::user()->all_companies == 1 )
                            <form  class="form-style" {{pdf_view('equipment')}} method="POST" action="{{ url('equipment/export/pdf') }}">
                                <input type="hidden" id="export_pdf_equipment2" name="export_pdf_equipment2" value="1">
                                {!! csrf_field() !!}
                                <input type="hidden" class="equipment_export" name="pdf_array" value="1">
                                <button  type="submit" id="export_pdf_equipment2_btn"  class="form_submit_check btn btn-secondary btn-sm">
                                    <span class="assign_class label{{getKeyid('pdf_export',$data)}}" data-id="{{getKeyid('pdf_export',$data) }}" data-value="{{checkKey('pdf_export',$data) }}" >
                                        {!! checkKey('pdf_export',$data) !!}
                                    </span>
                                </button>
                            </form>
                        @endif
                        <div id="table-equipment2" class="table-editable table-responsive">
                            <table id="plant_and_equipment2_table" class=" table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th style="width: 20%;" class="no-sort all_checkboxes_style">
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input equipment_checkbox_all" id="equipment_checkbox_all">
                                            <label class="form-check-label" for="equipment_checkbox_all">
                                                <span class="assign_class label{{getKeyid('all',$data)}}" data-id="{{getKeyid('all',$data) }}" data-value="{{checkKey('all',$data) }}" >
                                                    {!! checkKey('all',$data) !!}
                                                </span>
                                            </label>
                                        </div>
                                    </th>
                                    <th class="all_name_style">
                                        <span class="assign_class label{{getKeyid('name',$data)}}" data-id="{{getKeyid('name',$data) }}" data-value="{{checkKey('name',$data) }}" >
                                            {!! checkKey('name',$data) !!}
                                        </span>
                                    </th>
                                    <th class="no-sort all_action_btn" id="action"></th>
                                </tr>
                                </thead>
                                <tbody id="equipment2_data"></tbody>
                            </table>
                        </div>
                        <!---------equipment group column permission------------>
                        @include('backend.plant_and_equipment.input_equipment_permission')
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backend.label.input_label')
    @include('backend.plant_and_equipment.model.edit')
    <script type="text/javascript">
        function checkSelected(value,checkValue)
        {
            if(value == checkValue)
            {
                return 'selected';
            }
            else
            {
                return "";
            }

        }
        function equipment_data(url) {
            $.ajax({
                type: 'GET',
                url: url,
                success: function(response)
                {
                    $('#plant_and_equipment_table').DataTable().clear().destroy();
                    var table = $('#plant_and_equipment_table').dataTable({
                            processing: true,
                            "columnDefs": [
                                {"targets": [0], "orderable": false},
                            ],
                            bAutoWidth: false,
                            language: {
                                'lengthMenu': '  _MENU_ ',
                                'search':'<span class="assign_class label'+$('#search_id').attr('data-id')+'" data-id="'+$('#search_id').attr('data-id')+'" data-value="'+$('#search_id').val()+'" >'+$('#search_id').val()+'</span>',
                                'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                                'info':'',
                                'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                                'paginate': {
                                    'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                                    'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                                },
                                'infoFiltered': "(filtered from _MAX_ total records)"
                            },
                            "ajax": {
                                "url": url,
                                "type": 'get',
                            },
                            "createdRow": function( row, data, dataIndex,columns )
                            {
                                var checkbox_permission=$('#checkbox_permission').val();
                                var checkbox='';
                                checkbox+='<div class="form-check"><input type="checkbox" class="form-check-input selectedrowequipment" id="plant_and_equipments'+data.id+'"><label class="form-check-label" for="plant_and_equipments'+data.id+'""></label></div>';
                                var submit='';
                                var edit='';
                                submit+='<a type="button" class="btn btn-primary btn-xs my-0 waves-effect waves-light savepedata equipment_show_button_'+data.id+' show_tick_btn'+data.id+'" style="display: none;" id="'+data.id+'"><i class="fas fa-check"></i></a>';
                                edit+='<a type="button" class="btn btn-primary btn-xs my-0 waves-effect waves-light all_action_btn_margin equipment_edit_button_'+data.id+' " onclick="getplantandequipmentdata(this.id)"  id="'+data.id+'"><i class="fas fa-pencil-alt"></i></a>';
                                var equipment='';
                                if ($("#equipmentGroup_equipement_edit").val()=='1' && $('#edit_equipmentGroup').val() ){
                                    var e_disable='';
                                }else{
                                    var e_disable='disabled';
                                }
                                equipment+='<select onchange="editSelectedRow('+"equipment_tr"+data.id+')" '+e_disable+' searchable="Search here.."  id="equipment_edit'+data.id+'"  style="background-color: inherit;border: 0px">';
                                for (var j = response.equipments.length - 1; j >= 0; j--) {
                                    equipment+='<option '+e_disable+' value="'+response.equipments[j].id+'" '+checkSelected(data.equipment_id,response.equipments[j].id)+'  >'+response.equipments[j].name+'</option>';
                                }
                                equipment+='</select>';
                                var last_date='';
                                if ($("#equipmentGroup_lastserviceDate_edit").val()=='1' && $('#edit_equipmentGroup').val() ){
                                    var l_disable='';
                                }else{
                                    var l_disable='disabled';
                                }
                                last_date+='<input onchange="editSelectedRow('+"equipment_tr"+data.id+')" '+l_disable+' placeholder="Selected date" value="'+data.last_service_date+'" type="date" id="last_service_date'+data.id+'"  class="form-control datepicker">';
                                if ($("#equipmentGroup_nextserviceDate_edit").val()=='1' && $('#edit_equipmentGroup').val() ){
                                    var n_disable='';
                                }else{
                                    var n_disable='disabled';
                                }
                                var next_date='';
                                next_date+='<input onchange="editSelectedRow('+"equipment_tr"+data.id+')" '+n_disable+' placeholder="Selected date" value="'+data.next_service_date+'" type="date" id="next_service_date'+data.id+'"  class="form-control datepicker">';
                                var qr='';
                                qr+= '<img src="{{asset("/qr")}}/'+data.name+'.png"  width="50" height="50">';

                                $(columns[0]).html(checkbox);
                                $(columns[1]).html(qr);
                                $(columns[1]).attr('class','qr_image'+data['id']);

                                $(columns[2]).attr('id', 'name'+data['id']);
                                if ($("#equipmentGroup_name_edit").val()=='1' && $('#edit_equipmentGroup').val() ){
                                    $(columns[2]).attr('Contenteditable', 'true');
                                }
                                $(columns[2]).attr('onkeydown', 'editSelectedRow(equipment_tr'+data['id']+')');
                                $(columns[2]).attr('class','edit_inlinepe');
                                $(columns[2]).attr('data-id',data['id']);

                                $(columns[3]).attr('id','equipment'+data['id']);
                                $(columns[3]).attr('class','edit_inlinepe');
                                $(columns[3]).attr('data-id',data['id']);
                                $(columns[3]).html(equipment);
                                if ($("#equipmentGroup_serialNumber_edit").val()=='1' && $('#edit_equipmentGroup').val() ){
                                    $(columns[4]).attr('Contenteditable', 'true');
                                }
                                $(columns[4]).attr('onkeydown', 'editSelectedRow(equipment_tr'+data['id']+')');
                                $(columns[4]).attr('class','edit_inlinepe');
                                $(columns[4]).attr('data-id',data['id']);
                                $(columns[4]).attr('id','serial'+data['id']);

                                $(columns[5]).attr('id', 'make'+data['id']);
                                $(columns[5]).attr('class','edit_inlinepe');
                                $(columns[5]).attr('data-id',data['id']);
                                $(columns[5]).attr('onkeydown', 'editSelectedRow(equipment_tr'+data['id']+')');
                                if ($("#equipmentGroup_make_edit").val()=='1' && $('#edit_equipmentGroup').val() ){
                                    $(columns[5]).attr('Contenteditable', 'true');
                                }

                                $(columns[6]).attr('id', 'model'+data['id']);
                                $(columns[6]).attr('class','edit_inlinepe');
                                $(columns[6]).attr('data-id',data['id']);
                                if ($("#equipmentGroup_model_edit").val()=='1' && $('#edit_equipmentGroup').val() ){
                                    $(columns[6]).attr('Contenteditable', 'true');
                                }
                                $(columns[6]).attr('onkeydown', 'editSelectedRow(equipment_tr'+data['id']+')');
                                $(columns[7]).attr('id', 'last_service_date'+data['id']);
                                $(columns[7]).attr('class','edit_inlinepe');
                                $(columns[7]).attr('onkeydown', 'editSelectedRow(equipment_tr'+data['id']+')');
                                $(columns[7]).attr('data-id',data['id']);
                                //$(columns[7]).html(last_date);
                                $(columns[8]).attr('id', 'next_service_date'+data['id']);
                                $(columns[8]).attr('class','edit_inlinepe');
                                $(columns[8]).attr('data-id',data['id']);
                                //$(columns[8]).html(next_date);

                                $(columns[9]).attr('id', 'condition'+data['id']);
                                $(columns[9]).attr('class','edit_inlinepe');
                                $(columns[9]).attr('data-id',data['id']);
                                if ($("#equipmentGroup_condition_edit").val()=='1' && $('#edit_equipmentGroup').val() ){
                                    $(columns[9]).attr('Contenteditable', 'true');
                                }
                                $(columns[9]).attr('onkeydown', 'editSelectedRow(equipment_tr'+data['id']+')');
                                $(columns[10]).append(submit);
                                $(columns[10]).append(edit);
                                $(columns[10]).attr('class','equipment_data_action');
                                $(row).attr('id', 'equipment_tr'+data['id']);
                                // $(row).attr('class', 'selectedrowequipment');
                                var temp=data['id'];
                                $(row).attr('data-id', data['id']);
                            },
                            columns:[
                                {data: 'checkbox', name: 'checkbox',visible:$('#equipmentGroup_checkbox').val()},
                                {data: 'qr', name: 'qr',visible:$('#equipmentGroup_qr').val()},
                                {data: 'name', name: 'name',visible:$('#equipmentGroup_name').val()},
                                {data: 'equipment_id', name: 'equipment_id',visible:$('#equipmentGroup_equipement').val()},
                                {data: 'serial_number', name: 'serial_number',visible:$('#equipmentGroup_serialNumber').val()},
                                {data: 'make', name: 'make',visible:$('#equipmentGroup_make').val()},
                                {data: 'model', name: 'model',visible:$('#equipmentGroup_model').val()},
                                {data: 'last_service_date', name: 'last_service_date',visible:$('#equipmentGroup_lastserviceDate').val()},
                                {data: 'next_service_date', name: 'next_service_date',visible:$('#equipmentGroup_nextserviceDate').val()},
                                {data: 'condition', name: 'condition',visible:$('#equipmentGroup_condition').val()},
                                // {data: 'submit', name: 'submit',visible:$('#edit_equipmentGroup').val()},
                                {data: 'actions', name: 'actions'},
                            ],
                        }
                    );},
                error: function (error) {
                    console.log(error);
                }
            });
            // (function ($) {
            //     var active ='<span class="assign_class label'+$('#breadcrumb_equipmentGroup').attr('data-id')+'" data-id="'+$('#breadcrumb_equipmentGroup').attr('data-id')+'" data-value="'+$('#breadcrumb_equipmentGroup').val()+'" >'+$('#breadcrumb_equipmentGroup').val()+'</span>';
            //     $('.breadcrumb-item.active').html(active);
            //     var parent ='<span class="assign_class label'+$('#dashboard_label').attr('data-id')+'" data-id="'+$('#dashboard_label').attr('data-id')+'" data-value="'+$('#dashboard_label').val()+'" >'+$('#dashboard_label').val()+'</span>';
            //     $('.breadcrumb-item a').html(parent);
            // }(jQuery));
            if ($(":checkbox").prop('checked',true)){
                $(":checkbox").prop('checked',false);
            }

        }

        $(document).ready(function () {
            var url1='/plant_and_equipment/getall';
            equipment_data(url1);

            var url2='/equipment/getall';
            equipment2_data(url2);

            $("body").on('click','.equipment_group_refresh',function () {
                var url='/plant_and_equipment/getall';
                equipment_data(url);
            });
        });

        // tab 2
        function equipment2_data(url) {
            var table = $('#plant_and_equipment2_table').dataTable({
                    processing: true,
                    language: {
                        'lengthMenu': '  _MENU_ ',
                        'search':'<span class="assign_class label'+$('#search_id').attr('data-id')+'" data-id="'+$('#search_id').attr('data-id')+'" data-value="'+$('#search_id').val()+'" >'+$('#search_id').val()+'</span>',
                        'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                        'info':'',
                        'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                        'paginate': {
                            'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                            'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                        },
                        'infoFiltered': "(filtered from _MAX_ total records)"
                    },
                    "ajax": {
                        "url": url,
                        "type": 'get',
                    },
                    "createdRow": function( row, data, dataIndex,columns )
                    {
                        var checkbox_permission=$('#checkbox_permission').val();
                        var checkbox='';
                        checkbox+='<div class="form-check"><input type="checkbox" class="form-check-input selectedrowequipment2" id="equipment2s' + data.id + '"><label class="form-check-label" for="equipment2s' + data.id + '"></label></div>';
                        var submit='';
                        submit+='<a type="button" class="btn btn-primary btn-xs my-0 all_action_btn_margin waves-effect waves-light saveedit_inline_equipment equipment_data_show_button_'+data.id+' show_tick_btn'+data.id+'" style="display: none;" id="'+data.id+'"><i class="fas fa-check"></i></a>';
                        $(columns[0]).html(checkbox);
                        $(columns[1]).attr('id', 'name'+data['id']);
                        $(columns[1]).attr('class', 'edit_inline_equipment');
                        if ($("#equipment_name_edit").val()=='1' && $('#edit_equipment').val() ){
                            $(columns[1]).attr('Contenteditable', 'true');
                        }
                        $(columns[1]).attr('onkeydown', 'editSelectedRow(equipment2_tr'+data['id']+')');
                        $(columns[2]).append(submit);
                        $(row).attr('id', 'equipment2_tr'+data['id']);
                        //$(row).attr('class', 'selectedrowequipment2');
                        $(row).attr('data-id', data['id']);
                    },
                    columns:[
                        {data: 'checkbox', name: 'checkbox',visible:$('#equipment_checkbox').val()},
                        {data: 'name', name: 'name',visible:$('#equipment_name').val()},
                        {data: 'actions', name: 'actions'},
                    ],
                }
            );
            if ($(":checkbox").prop('checked',true)){
                $(":checkbox").prop('checked',false);
            }
        }

        $('body').on('change', '.datepicker', function() {
            var id=$(this).attr('id');
            var value=$(this).val();
            if(value)
            {
                $('#'+id).val(value);
            }
        });

    </script>
@endsection
