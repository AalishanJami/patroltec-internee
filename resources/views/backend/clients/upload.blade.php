@extends('backend.layouts.client')
@section('title', 'Upload')
@section('content')
    @php
        $data=localization();
    @endphp
    @include('backend.layouts.client_sidebar')
    <link rel="stylesheet" href="{{ asset('custom/css/custom_style.css') }}">
    <div class="padding-left-custom remove-padding">
        @php
            $data=localization();
        @endphp
        {!! breadcrumInner('upload',Session::get('client_id'),'clients','/client/detail/'.Session::get('client_id'),'client','/client') !!}
        <div class="card">
            <div class="card-body">
                <div id="table" class="table-editable table-responsive">
                    @if(Auth()->user()->hasPermissionTo('create_clientUpload') || Auth::user()->all_companies == 1 )
                        <span class="table-add float-right mb-3 mr-2">
                            <a class="text-success"  data-toggle="modal" data-target="#modalUpload">
                                <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
                            </a>
                        </span>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('selected_delete_clientUpload') || Auth::user()->all_companies == 1 )
                        <button id="deleteSelectedupload" class="btn btn-danger btn-sm">
                            <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                                {!! checkKey('selected_delete',$data) !!}
                            </span>
                        </button>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('restore_delete_clientUpload') || Auth::user()->all_companies == 1 )
                        <button id="restore_button_upload" class="btn btn-primary btn-sm">
                            <span class="assign_class label{{getKeyid('restore',$data)}}" data-id="{{getKeyid('restore',$data) }}" data-value="{{checkKey('restore',$data) }}" >
                                {!! checkKey('restore',$data) !!}
                            </span>
                        </button>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('selected_active_clientUpload') || Auth::user()->all_companies == 1 )
                        <button style="display: none;" id="selectedactivebuttonupload" class="btn btn-success btn-sm waves-effect waves-light">
                            <span class="assign_class label{{getKeyid('selected_active',$data)}}" data-id="{{getKeyid('selected_active',$data) }}" data-value="{{checkKey('selected_active',$data) }}" >
                                {!! checkKey('selected_active',$data) !!}
                            </span>
                        </button>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('active_clientUpload') || Auth::user()->all_companies == 1 )
                        <button id="show_active_button_upload" class="btn btn-primary btn-sm" style="display: none;">
                            <span class="assign_class label{{getKeyid('show_active',$data)}}" data-id="{{getKeyid('show_active',$data) }}" data-value="{{checkKey('show_active',$data) }}" >
                                {!! checkKey('show_active',$data) !!}
                            </span>
                        </button>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('csv_clientUpload') || Auth::user()->all_companies == 1 )
                        <form class="form-style export_excel_upload" method="POST" action="{{ url('/upload/exportExcel') }}">
                            <input type="hidden" id="export_excel_upload" name="export_excel_upload" value="1">
                            <input type="hidden"  name="link_id" value="1">
                            <input type="hidden"  name="link_name" value="client">
                            <input type="hidden" class="upload_export" name="excel_array" value="1">
                            {!! csrf_field() !!}
                            <button  type="submit" class="form_submit_check btn btn-warning btn-sm">
                            <span class="assign_class label{{getKeyid('excel_export',$data)}}" data-id="{{getKeyid('excel_export',$data) }}" data-value="{{checkKey('excel_export',$data) }}" >
                                {!! checkKey('excel_export',$data) !!}
                            </span>
                            </button>
                        </form>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('word_clientUpload') || Auth::user()->all_companies == 1 )
                        <form class="form-style export_word_upload" method="POST" action="{{ url('/upload/exportWorld') }}">
                            <input type="hidden" id="export_word_upload" name="export_word_upload" value="1">
                            <input type="hidden"  name="link_id" value="1">
                            <input type="hidden"  name="link_name" value="client">
                            <input type="hidden" class="upload_export" name="word_array" value="1">
                            {!! csrf_field() !!}
                            <button  type="submit" class="form_submit_check btn btn-success btn-sm">
                           <span class="assign_class label{{getKeyid('word_export',$data)}}" data-id="{{getKeyid('word_export',$data) }}" data-value="{{checkKey('word_export',$data) }}" >
                                {!! checkKey('word_export',$data) !!}
                            </span>
                            </button>
                        </form>
                    @endif
                    @if(Auth()->user()->hasPermissionTo('pdf_clientUpload') || Auth::user()->all_companies == 1 )
                        <form  class="form-style export_pdf_upload" {{pdf_view('uploads','1')}}  method="POST" action="{{ url('upload/exportPdf') }}">
                            <input type="hidden" id="export_pdf_upload" name="export_pdf_upload" value="1">
                            <input type="hidden"  name="link_id" value="1">
                            <input type="hidden"  name="link_name" value="client">
                            <input type="hidden" class="upload_export" name="pdf_array" value="1">
                            {!! csrf_field() !!}
                            <button  type="submit" class="form_submit_check btn btn-secondary btn-sm">
                           <span class="assign_class label{{getKeyid('pdf_export',$data)}}" data-id="{{getKeyid('pdf_export',$data) }}" data-value="{{checkKey('pdf_export',$data) }}" >
                                {!! checkKey('pdf_export',$data) !!}
                            </span>
                            </button>
                        </form>
                    @endif
                    @include('backend.label.input_label')
                    <input type="hidden" name="flag" id="flagAddress" value="1">
                    <input type="hidden" name="flagShip" id="flagShip" value="{{$flag}}">
                    <table id="upload_table"  class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th class="no-sort all_checkboxes_style">
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input upload_checked" id="upload_checkbox_all">
                                    <label class="form-check-label" for="upload_checkbox_all">
                                        <span class="assign_class label{{getKeyid('all',$data)}}" data-id="{{getKeyid('all',$data) }}" data-value="{{checkKey('all',$data) }}" >
                                            {!! checkKey('all',$data) !!}
                                        </span>
                                    </label>
                                </div>
                            </th>
                            <th>
                                <span class="assign_class label{{getKeyid('title',$data)}}" data-id="{{getKeyid('title',$data) }}" data-value="{{checkKey('title',$data) }}" >
                                    {!! checkKey('title',$data) !!}
                                </span>
                            </th>
                            <th>
                                <span class="assign_class label{{getKeyid('notes',$data)}}" data-id="{{getKeyid('notes',$data) }}" data-value="{{checkKey('notes',$data) }}" >
                                    {!! checkKey('notes',$data) !!}
                                </span>
                            </th>
                            <th class="no-sort all_action_btn_extra" id="action"></th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                @if(Auth()->user()->all_companies == 1)
                    <input type="hidden" id="clientUpload_checkbox" value="1">
                    <input type="hidden" id="clientUpload_notes" value="1">
                    <input type="hidden" id="clientUpload_title" value="1">
                    <input type="hidden" id="clientUpload_upload" value="1">
                    <input type="hidden" id="clientUpload_status" value="1">
                @else
                    <input type="hidden" id="clientUpload_checkbox" value="{{Auth()->user()->hasPermissionTo('clientUpload_checkbox')}}">
                    <input type="hidden" id="clientUpload_notes" value="{{Auth()->user()->hasPermissionTo('clientUpload_notes')}}">
                    <input type="hidden" id="clientUpload_title" value="{{Auth()->user()->hasPermissionTo('clientUpload_title')}}">
                    <input type="hidden" id="clientUpload_upload" value="{{Auth()->user()->hasPermissionTo('clientUpload_upload')}}">
                    <input type="hidden" id="clientUpload_status" value="{{Auth()->user()->hasPermissionTo('clientUpload_status')}}">
                @endif
            </div>
        </div>
        @include('backend.clients.upload.model.create')
        {{--    Edit Modal--}}
        @include('backend.clients.upload.model.edit')
        {{--    END Edit Modal--}}
    </div>
    <script type="text/javascript" src="{{ asset('custom/js/upload.js') }}" ></script>
    <script type="text/javascript">
        $('.file-upload').file_upload();
        $(document).ready(function() {
            $(function () {
                uploadAppend();
            });
            $("body").on('click')
            // (function ($) {
            //     var active ='<span class="assign_class label'+$('#breadcrumb_upload').attr('data-id')+'" data-id="'+$('#breadcrumb_upload').attr('data-id')+'" data-value="'+$('#breadcrumb_upload').val()+'" >'+$('#breadcrumb_upload').val()+'</span>';
            //     $('.breadcrumb-item.active').html(active);
            //     var parent ='<span class="assign_class label'+$('#dashboard_label').attr('data-id')+'" data-id="'+$('#dashboard_label').attr('data-id')+'" data-value="'+$('#dashboard_label').val()+'" >'+$('#dashboard_label').val()+'</span>';
            //     $('.breadcrumb-item a').html(parent);
            // }(jQuery));
        });
        function uploadAppend()
        {
            var table = $('#upload_table').dataTable({
                processing: true,
                binfo: false,
                language: {
                    'lengthMenu': '_MENU_',
                    'search': '<span class="assign_class label' + $('#search_label').attr('data-id') + '" data-id="' + $('#search_label').attr('data-id') + '" data-value="' + $('#search_label').val() + '" >' + $('#search_label').val() + '</span>',
                    'zeroRecords': ' <span class="assign_class label' + $('#nothing_found_sorry_label').attr('data-id') + '" data-id="' + $('#nothing_found_sorry_label').attr('data-id') + '" data-value="' + $('#nothing_found_sorry_label').val() + '" >' + $('#nothing_found_sorry_label').val() + '</span>',
                    'info': '<span class="assign_class label' + $('#showing_page_label').attr('data-id') + '" data-id="' + $('#showing_page_label').attr('data-id') + '" data-value="' + $('#showing_page_label').val() + '" >' + $('#showing_page_label').val() + '</span> _PAGE_ <span class="assign_class label' + $('#of_label').attr('data-id') + '" data-id="' + $('#of_label').attr('data-id') + '" data-value="' + $('#of_label').val() + '" >' + $('#of_label').val() + '</span> _PAGES_',
                    'infoEmpty': '<span class="assign_class label' + $('#no_records_available_label').attr('data-id') + '" data-id="' + $('#no_records_available_label').attr('data-id') + '" data-value="' + $('#no_records_available_label').val() + '" >' + $('#no_records_available_label').val() + '</span>',
                    'paginate': {
                        'previous': '<span class="assign_class label' + $('#previous_label').attr('data-id') + '" data-id="' + $('#previous_label').attr('data-id') + '" data-value="' + $('#previous_label').val() + '" >' + $('#previous_label').val() + '</span>',
                        'next': '<span class="assign_class label' + $('#next_label').attr('data-id') + '" data-id="' + $('#next_label').attr('data-id') + '" data-value="' + $('#next_label').val() + '" >' + $('#next_label').val() + '</span>',
                    },
                    'infoFiltered': "(filtered from _MAX_ total records)"
                },
                "ajax": {
                    "headers": {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    "url": '/upload/getAll',
                    "type": 'post',
                    "data": {'flag': '{{$flag}}'},
                },
                "createdRow": function (row, data, dataIndex,columns) {
                    console.log(data);
                    $(row).attr('id', data['id']);
                    var checkbox='';
                    checkbox+='<div class="form-check upload_check_section"><input type="checkbox" class="form-check-input upload_check selectedRowUpload" id="upload_check'+data.id+'"><label class="form-check-label" for="upload_check'+data.id+'""></label></div>';
                    $(columns[0]).html(checkbox);
                    var temp = data['id'];
                    $(row).attr('data-id',data['id']);
                    $(row).attr('id', 'upload_tr'+data['id']);
                    //$(row).attr('onclick', "selectedRowUpload(this.id)");
                },
                columns: [
                    {data: 'checkbox', name: 'checkbox', visible: $('#clientUpload_checkbox').val()},
                    {data: 'name', name: 'name', visible: $('#clientUpload_title').val()},
                    {data: 'notes', name: 'notes', visible: $('#clientUpload_notes').val()},
                    {data: 'actions', name: 'actions'},
                ],
            });
            if ($(":checkbox").prop('checked',true)){
                $(":checkbox").prop('checked',false);
            }
        }

        function uploadSoftAppend()
        {
            var table = $('#upload_table').dataTable({
                processing: true,
                language: {
                    'lengthMenu': '_MENU_',
                    'search':'<span class="assign_class label'+$('#search_label').attr('data-id')+'" data-id="'+$('#search_label').attr('data-id')+'" data-value="'+$('#search_label').val()+'" >'+$('#search_label').val()+'</span>',
                    'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                    'info': '<span class="assign_class label'+$('#showing_page_label').attr('data-id')+'" data-id="'+$('#showing_page_label').attr('data-id')+'" data-value="'+$('#showing_page_label').val()+'" >'+$('#showing_page_label').val()+'</span> _PAGE_ <span class="assign_class label'+$('#of_label').attr('data-id')+'" data-id="'+$('#of_label').attr('data-id')+'" data-value="'+$('#of_label').val()+'" >'+$('#of_label').val()+'</span> _PAGES_',
                    'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                    'paginate': {
                        'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                        'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                    },
                    'infoFiltered': "(filtered from _MAX_ total records)"
                },
                "ajax": {
                    "headers": {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    "url": '/upload/getAllSoft',
                    "type": 'post',
                    "data":  {'flag':$('#flagShip').val()},
                },
                "createdRow": function (row, data, dataIndex,columns) {
                    console.log(data);
                    $(row).attr('id', data['id']);
                    var checkbox='';
                    checkbox+='<div class="form-check upload_check_section"><input type="checkbox" class="form-check-input upload_check selectedRowUpload" id="upload_check'+data.id+'"><label class="form-check-label" for="upload_check'+data.id+'"></label></div>';
                    $(columns[0]).html(checkbox);
                    var temp = data['id'];
                    $(row).attr('id', 'upload_tr'+data['id']);
                    //$(row).attr('onclick', "selectedRowUpload(this.id)");
                },
                columns: [
                    {data: 'checkbox', name: 'checkbox', visible: $('#clientUpload_checkbox').val()},
                    {data: 'name', name: 'name', visible: $('#clientUpload_title').val()},
                    {data: 'notes', name: 'notes', visible: $('#clientUpload_notes').val()},
                    {data: 'actions', name: 'actions'},
                ],
            });
            //if ($(":checkbox").prop('checked',true)){
                $(":checkbox").prop('checked',false);
           // }
        }
        $(document).on('focusout','.note-editable',function(){
            var notes = $('.note-editing-area').text();
            $("#notes_value").val(notes);
            $("#notes_value_edit").val(notes);
        });
    </script>
@endsection
