@extends('backend.layouts.backend')
@section('title', 'Client')
@section('content')
    @php
        $data=localization();
    @endphp
    {{ Breadcrumbs::render('client') }}
    <link rel="stylesheet" href="{{ asset('custom/css/custom_style.css') }}">

    <div class="card">
        <div class="card-body">
            <div id="table" class="table-editable">
                <span class="table-add float-right mb-3 mr-2">
                    @if(Auth()->user()->hasPermissionTo('create_client') || Auth::user()->all_companies == 1 )
                        <a class="text-success" href="{{url('client/createDetail')}}" >
                            <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
                        </a>
                    @endif
                </span>
                @if(Auth()->user()->hasPermissionTo('selected_delete_client') || Auth::user()->all_companies == 1 )
                <button id="deleteselectedclient" class="btn btn-danger btn-sm">
                    <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                        {!! checkKey('selected_delete',$data) !!}
                    </span>
                </button>
                @endif
                <input type="hidden" id="flagClient" value="1">
                @if(Auth()->user()->hasPermissionTo('restore_delete_client') || Auth::user()->all_companies == 1 )
                <button id="restore_button_client" class="btn btn-primary btn-sm">
                   <span class="assign_class label{{getKeyid('restore',$data)}}" data-id="{{getKeyid('restore',$data) }}" data-value="{{checkKey('restore',$data) }}" >
                        {!! checkKey('restore',$data) !!}
                    </span>
                </button>
                @endif
                @if(Auth()->user()->hasPermissionTo('slected_active_client') || Auth::user()->all_companies == 1 )
                <button id="selectedactivebuttonclient" class="btn btn-success btn-sm">
                    <span class="assign_class label{{getKeyid('selected_active',$data)}}" data-id="{{getKeyid('selected_active',$data) }}" data-value="{{checkKey('selected_active',$data) }}" >
                        {!! checkKey('selected_active',$data) !!}
                    </span>
                </button>
                @endif
                @if(Auth()->user()->hasPermissionTo('active_client') || Auth::user()->all_companies == 1 )
                <button id="show_active_button_client" class="btn btn-primary btn-sm" style="display: none;">
                    <span class="assign_class label{{getKeyid('show_active',$data)}}" data-id="{{getKeyid('show_active',$data) }}" data-value="{{checkKey('show_active',$data) }}" >
                        {!! checkKey('show_active',$data) !!}
                    </span>
                </button>
                @endif
                @if(Auth()->user()->hasPermissionTo('csv_client') || Auth::user()->all_companies == 1 )
                <form class="form-style export_excel_client" method="POST" action="{{ url('/client/exportExcel') }}">
                    <input type="hidden" class="client_export"  id="export_excel_client" name="excel_array" value="1">
                    {!! csrf_field() !!}
                    <button  type="submit" data-id="{{getKeyid('excel_export',$data) }}" data-value="{{checkKey('excel_export',$data) }}"  class="form_submit_check btn btn-warning btn-sm assign_class label{{getKeyid('excel_export',$data)}}">
                        {!! checkKey('excel_export',$data) !!}
                    </button>
                </form>
                @endif
                @if(Auth()->user()->hasPermissionTo('word_client') || Auth::user()->all_companies == 1 )
                <form class="form-style export_word_client" method="POST" action="{{ url('/client/exportWord') }}">
                    <input type="hidden" class="client_export" id="export_word_client" name="word_array" value="1">
                    {!! csrf_field() !!}
                    <button  type="submit" class="form_submit_check btn btn-success btn-sm">
                       <span class="assign_class label{{getKeyid('word_export',$data)}}" data-id="{{getKeyid('word_export',$data) }}" data-value="{{checkKey('word_export',$data) }}" >
                            {!! checkKey('word_export',$data) !!}
                        </span>
                    </button>
                </form>
                @endif
                @if(Auth()->user()->hasPermissionTo('pdf_client') || Auth::user()->all_companies == 1 )
                <form  class="form-style export_pdf_client" {{pdf_view('clients')}}   method="POST" action="{{ url('client/exportPdf') }}">
                    <input type="hidden" class="client_export" id="export_pdf_client" name="pdf_array" value="1">
                    {!! csrf_field() !!}
                    <button  type="submit" class="form_submit_check btn btn-secondary btn-sm">
                        <span class="assign_class label{{getKeyid('pdf_export',$data)}}" data-id="{{getKeyid('pdf_export',$data) }}" data-value="{{checkKey('pdf_export',$data) }}" >
                            {!! checkKey('pdf_export',$data) !!}
                        </span>
                    </button>
                </form>
                @endif
                @include('backend.label.input_label')
                <table  id="client_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="no-sort all_checkboxes_style" id="action">
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input client_checked" id="client_checkbox_all">
                                <label class="form-check-label" for="client_checkbox_all">
                                    <span class="assign_class label{{getKeyid('all',$data)}}" data-id="{{getKeyid('all',$data) }}" data-value="{{checkKey('all',$data) }}" >
                                        {!! checkKey('all',$data) !!}
                                    </span>
                                </label>
                            </div>
                        </th>
                        <th>
                            <span class="assign_class label{{getKeyid('name',$data)}}" data-id="{{getKeyid('name',$data) }}" data-value="{{checkKey('name',$data) }}" >
                                {!! checkKey('name',$data) !!}
                            </span>
                        </th>
                        <th>
                            <span class="assign_class label{{getKeyid('number',$data)}}" data-id="{{getKeyid('number',$data) }}" data-value="{{checkKey('number',$data) }}" >
                                {!! checkKey('number',$data) !!}
                            </span>
                        </th>
                        <th>
                            <span class="assign_class label{{getKeyid('email',$data)}}" data-id="{{getKeyid('email',$data) }}" data-value="{{checkKey('email',$data) }}" >
                                {!! checkKey('email',$data) !!}
                            </span>
                        </th>
                        <th>
                            <span class="assign_class label{{getKeyid('address',$data)}}" data-id="{{getKeyid('address',$data) }}" data-value="{{checkKey('address',$data) }}" >
                                {!! checkKey('address',$data) !!}
                            </span>
                        </th>
                        <th class="no-sort all_action_btn"></th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            @if(Auth()->user()->all_companies == 1)
                <input type="hidden" id="client_checkbox" value="1">
                <input type="hidden" id="client_name" value="1">
                <input type="hidden" id="client_firstname" value="1">
                <input type="hidden" id="client_surname" value="1">
                <input type="hidden" id="client_phone" value="1">
                <input type="hidden" id="client_email" value="1">
                <input type="hidden" id="client_address" value="1">
                <input type="hidden" id="edit_client" value="1">
                <input type="hidden" id="delete_client" value="1">
            @else
                <input type="hidden" id="client_checkbox" value="{{Auth()->user()->hasPermissionTo('client_checkbox')}}">
                <input type="hidden" id="client_name" value="{{Auth()->user()->hasPermissionTo('client_name')}}">
                <input type="hidden" id="client_firstname" value="{{Auth()->user()->hasPermissionTo('client_firstname')}}">
                <input type="hidden" id="client_surname" value="{{Auth()->user()->hasPermissionTo('client_surname')}}">
                <input type="hidden" id="client_phone" value="{{Auth()->user()->hasPermissionTo('client_phone')}}">
                <input type="hidden" id="client_email" value="{{Auth()->user()->hasPermissionTo('client_email')}}">
                <input type="hidden" id="client_address" value="{{Auth()->user()->hasPermissionTo('client_email')}}">
                <input type="hidden" id="edit_client" value="{{Auth()->user()->hasPermissionTo('edit_client')}}">
                <input type="hidden" id="delete_client" value="{{Auth()->user()->hasPermissionTo('delete_client')}}">
            @endif
            <input type="hidden" id="getAllClientMain" value="1">
        </div>
    </div>
    <input type="hidden" id="main_client" value="1">
    <input type="hidden" class="get_current_path" value="{{Request::path()}}">
    @component('backend.clients.model.create')
    @endcomponent
    {{--    Edit Modal--}}
    @component('backend.clients.model.edit')
    @endcomponent
    {{--    END Edit Modal--}}
    <script type="text/javascript" src="{{asset('custom/js/client.js')}}" ></script>

    <script type="text/javascript">

        $(document).ready(function() {
            var current_path=$('.get_current_path').val();
            localStorage.setItem('current_path',current_path);
            $('#client_checkbox_all').click(function() {
                if ($(this).is(':checked')) {
                    $('.client_checked').attr('checked', true);
                    $('tr').addClass('selected');
                } else {
                    $('.client_checked').attr('checked', false);
                    $('tr').removeClass('selected');
                }
            });

            $(function () {
                client_data('/client/getAllClientMain');
                (function ($) {
                    var active ='<span class="assign_class label'+$('#breadcrumb_client').attr('data-id')+'" data-id="'+$('#breadcrumb_client').attr('data-id')+'" data-value="'+$('#breadcrumb_client').val()+'" >'+$('#breadcrumb_client').val()+'</span>';
                    $('.breadcrumb-item.active').html(active);
                    var parent ='<span class="assign_class label'+$('#dashboard_label').attr('data-id')+'" data-id="'+$('#dashboard_label').attr('data-id')+'" data-value="'+$('#dashboard_label').val()+'" >'+$('#dashboard_label').val()+'</span>';
                    $('.breadcrumb-item a').html(parent);
                }(jQuery));
            });
        });
        function client_data(url)
        {
            var table = $('#client_table').dataTable({
                processing: true,
                binfo:false,
                language: {
                    'lengthMenu': '  _MENU_ ',
                    'search':'<span class="assign_class label'+$('#search_label').attr('data-id')+'" data-id="'+$('#search_label').attr('data-id')+'" data-value="'+$('#search_label').val()+'" >'+$('#search_label').val()+'</span>',
                    'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                    'info': ' _PAGE_ <span class="assign_class label'+$('#of_label').attr('data-id')+'" data-id="'+$('#of_label').attr('data-id')+'" data-value="'+$('#of_label').val()+'" >'+$('#of_label').val()+'</span> _PAGES_',
                    'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                    'paginate': {
                        'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                        'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                    },
                    'infoFiltered': "(filtered from _MAX_ total records)"
                },
                "ajax": {
                    "url": url,
                    "type": 'get',
                },
                "createdRow": function( row, data, dataIndex,columns ) {
                    var i=0;
                    var checkbox='<div class="form-check"><input type="checkbox" class="form-check-input selectedrowclient client_checked" id="client_checked'+data.id+'"><label class="form-check-label" for="client_checked'+data.id+'"></label></div>';
                    $(columns[0]).html(checkbox);
                    var checkbox_permission=$('#checkbox_permission').val();
                    $(row).attr('id', "client_tr"+data['id']);
                    var table = $('#client_table').DataTable();
                    // $(row).attr('data-id', table.row('tr').index());
                    var temp=data['id'];
                    $(row).attr('data-id',data['id']);
                    // $(row).attr('onclick',"selectedRowClient(this.id)");
                    //$(row).attr('class',"selectedrowclient");
                },
                columns: [
                    {data: 'checkbox', name: 'checkbox',visible:$('#client_checkbox').val()},
                    {data: 'title', name: 'title',visible:$('#client_name').val()},
                    {data: 'phone_number', name: 'phone_number',visible:$('#client_phone').val()},
                    {data: 'email', name: 'email',visible:$('#client_email').val()},
                    {data: 'address', name: 'address',visible:$('#client_address').val()},
                    {data: 'actions', name: 'actions'},
                ],
                columnDefs: [ {
                    'targets': [0,1], /* column index */
                    'orderable': false, /* true or false */
                }],
            });
            if ($(":checkbox").prop('checked',true)){
                $(":checkbox").prop('checked',false);
            }
        }
    </script>
@endsection
