@php
    $data=localization();
@endphp
<style type="text/css">
    @media (min-width: 576px) {
        .modal-dialog {
            max-width: 75% !important;
        }
    }

    #bulk_upload_img_loader_edit{
        position: absolute;
        z-index: 1000;
        top: 19%;
        left: 41%;
        width: 150px;
        height: 150px;
        display: none;
    }
</style>
<div class="modal fade" id="modalUploadEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">
                    <span class="assign_class label{{getKeyid('upload',$data)}}" data-id="{{getKeyid('upload',$data) }}" data-value="{{checkKey('upload',$data) }}" >
                        {!! checkKey('upload',$data) !!}
                    </span>
                    @if(Auth()->user()->hasPermissionTo('view_cms') || Auth::user()->all_companies == 1 )
                        <button data-class="edit-disable" class="edit_cms_enable edit_cms_switch btn btn-primary btn-sm waves-effect waves-light" style="display: none;" >
                            Edit cms
                        </button>
                        <button  data-class="edit-enable" class="edit_cms_disable edit_cms_switch btn btn-danger btn-sm waves-effect waves-light">
                            Edit cms
                        </button>
                    @endif
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="uploadFormEdit" method="POST" action="javascript:;" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="id" id="edit_id">
                <input type="hidden" id="base_url" value="{{storage_path('app')}}">
                <div class="modal-body mx-3">
                    <div class="md-form mb-5">
                        @if(Auth()->user()->hasPermissionTo('clientUpload_title_edit') || Auth::user()->all_companies == 1 )
                            <i class="fas fa-user prefix grey-text"></i>
                            <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="title" id="edit_title" value="Title" required autocomplete="name" autofocus>
                            <label data-error="wrong" data-success="right" for="name" class="active" >
                                <span class="assign_class label{{getKeyid('title',$data)}}" data-id="{{getKeyid('title',$data) }}" data-value="{{checkKey('title',$data) }}" >
                                    {!! checkKey('title',$data) !!}
                                </span>
                            </label>
                        @endif
                    </div>
                    @if(Auth()->user()->hasPermissionTo('clientUpload_upload_edit') || Auth::user()->all_companies == 1 )
                        <div class="file-upload-wrapper">
                            <div class="spinner-border text-success" id="bulk_upload_img_loader_edit"></div>
                            <input type="file" id="edit_input-file-now" name="file" class="file-upload" />
                        </div>
                    @endif
                    <br>
                    @if(Auth()->user()->hasPermissionTo('clientUpload_notes_edit') || Auth::user()->all_companies == 1 )
                        <label data-error="wrong" data-success="right" for="name" class="active">
                                <span class="assign_class label{{getKeyid('notes',$data)}}" data-id="{{getKeyid('notes',$data) }}" data-value="{{checkKey('notes',$data) }}" >
                                    {!! checkKey('notes',$data) !!}
                                </span>
                        </label>
                        <div class="md-form mb-5" id="upload_note_edit">
                            <div class="summernote_inner body_notes" ></div>
                        </div>
                        <input type="hidden" name="notes_edit" class="empty_input" id="notes_value_edit">
                    @endif
                </div>
                <input type="hidden" name="notes" id="notes_value">
                <div class="modal-footer d-flex justify-content-center">
                    <button class="btn btn-primary btn-sm" type="submit">
                        <span class="assign_class label{{getKeyid('update',$data)}}" data-id="{{getKeyid('update',$data) }}" data-value="{{checkKey('update',$data) }}" >
                            {!! checkKey('update',$data) !!}
                        </span>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<script !src="">
    $("#uploadFormEdit").on('submit',function (event) {
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        $("#bulk_upload_img_loader_edit").show();
        $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: '{{route('client_updateUpload')}}',
                data:new FormData(this),
                // dataType:'JSON',
                contentType:false,
                cache:false,
                processData:false,
                success: function(data)
                {
                    setTimeout(function(){
                        toastr["success"]('uploaded successfully');
                        $('#upload_table').DataTable().clear().destroy();
                        uploadAppend();
                        $("#bulk_upload_img_loader_edit").hide();
                        $('#modalUploadEdit').modal('hide');
                    },1000);
                },
                error: function (error) {
                    console.log(error);
                }
            });

    });
</script>
