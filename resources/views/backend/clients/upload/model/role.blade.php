 @php
        $data=localization();
@endphp
<style type="text/css">
   [type=checkbox]:checked, [type=checkbox]:not(:checked) {
    position: unset;
    pointer-events: unset;
     opacity: unset;
}
</style>
<!-- Modal -->
<div class="modal fade" id="RoleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><span class="assign_class label{{getKeyid('select_groups',$data)}}" data-id="{{getKeyid('select_groups',$data) }}" data-value="{{checkKey('select_groups',$data) }}" >{!! checkKey('select_groups',$data) !!} </span></h5>

                <input type="hidden" id="user_id_hidden">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{url('/user/role/update')}}" method="POST">
            @csrf
                <div class="modal-body" style="text-align: center">
                    <div id="user_role_assign"></div>
                
                </div>
                <div class="modal-footer">
                    <button type="button" id="close_model" class="btn btn-secondary btn-sm"><span class="assign_class label{{getKeyid('close',$data)}}" data-id="{{getKeyid('close',$data) }}" data-value="{{checkKey('close',$data) }}" >{!! checkKey('close',$data) !!} </span></button>
                    <button type="submit" id="save_role" class="btn btn-primary btn-sm"><span class="assign_class label{{getKeyid('save',$data)}}" data-id="{{getKeyid('save',$data) }}" data-value="{{checkKey('save',$data) }}" >{!! checkKey('save',$data) !!} </span></button>
                </div>
            </form>
        </div>
    </div>
</div>