@php
    $data=localization();
@endphp
<style type="text/css">
    @media (min-width: 576px) {
        .modal-dialog {
            max-width: 75% !important;
        }
    }
    #bulk_upload_img_loader{
        position: absolute;
        z-index: 1000;
        top: 19%;
        left: 41%;
        width: 150px;
        height: 150px;
        display: none;
    }
    .title_required_filed,.file_required_filed{
        position: absolute;
        top: 92%;
        left: 3.5%;
    }
</style>
<div class="modal fade" id="modalUpload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">
                    <span class="assign_class label{{getKeyid('upload',$data)}}" data-id="{{getKeyid('upload',$data) }}" data-value="{{checkKey('upload',$data) }}" >
                        {!! checkKey('upload',$data) !!}
                    </span>
                    @if(Auth()->user()->hasPermissionTo('view_cms') || Auth::user()->all_companies == 1 )
                        <button data-class="edit-disable" class="edit_cms_enable edit_cms_switch btn btn-primary btn-sm waves-effect waves-light" style="display: none;" >
                            Edit cms
                        </button>
                        <button  data-class="edit-enable" class="edit_cms_disable edit_cms_switch btn btn-danger btn-sm waves-effect waves-light">
                            Edit cms
                        </button>
                      @endif
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @if(Auth()->user()->hasPermissionTo('create_clientUpload') || Auth::user()->all_companies == 1 )
                <form id="uploadForm" method="POST" action="{{url('client/storeUpload')}}" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="flag" value="{{$flag ?? ''}}">
                    <div class="modal-body mx-3">
                        <div class="md-form mb-5">
                            @if(Auth()->user()->hasPermissionTo('clientUpload_title_create') || Auth::user()->all_companies == 1 )
                                <i class="fas fa-user prefix grey-text"></i>
                                <input type="text" class="form-control empty_input" name="title" id="title" placeholder="Title"  autocomplete="name" autofocus>
                                <label data-error="wrong" data-success="right" for="name" class="active" >
                                    <span class="assign_class label{{getKeyid('title',$data)}}" data-id="{{getKeyid('title',$data) }}" data-value="{{checkKey('title',$data) }}" >
                                        {!! checkKey('title',$data) !!}
                                    </span>
                                </label>
                                <small class="title_required_filed text-danger"></small>
                            @endif
                        </div>
                        <div class="md-form mb-5">
                            @if(Auth()->user()->hasPermissionTo('clientUpload_status_create') || Auth::user()->all_companies == 1 )
                                <div class="row">
                                    <div class="col-md-1" style="max-width: 4.333333%;">
                                        <i class="fas fa-check prefix grey-text"></i>
                                    </div>
                                    <div class="col-md-11">
                                        <select name="status" id="status" class="empty_input mdb-select form-check-label">
                                            <option value="">Choose</option>
                                            <option value="extracted">Extracted </option>
                                            <option value="pending_extraction">Pending Extraction</option>
                                        </select>
                                        <label data-error="wrong" data-success="right" for="status" class="active" >
                                            <span class="assign_class label{{getKeyid('status',$data)}}" data-id="{{getKeyid('status',$data) }}" data-value="{{checkKey('status',$data) }}" >
                                                {!! checkKey('status',$data) !!}
                                            </span>
                                        </label>
                                    </div>
                                </div>
                            @endif
                        </div>
                        @if(Auth()->user()->hasPermissionTo('clientUpload_upload_create') || Auth::user()->all_companies == 1 )
                            <div class="file-upload-wrapper">
                                <div class="spinner-border text-success" id="bulk_upload_img_loader"></div>
                                <input type="file" id="input-file-now" name="file" class="file-upload empty_input" />
                                <small class="file_required_filed text-danger"></small>
                            </div>
                        @endif
                        <br>
                        @if(Auth()->user()->hasPermissionTo('clientUpload_notes_create') || Auth::user()->all_companies == 1 )
                            <label data-error="wrong" data-success="right" for="name" class="active">
                                <span class="assign_class label{{getKeyid('notes',$data)}}" data-id="{{getKeyid('notes',$data) }}" data-value="{{checkKey('notes',$data) }}" >
                                    {!! checkKey('notes',$data) !!}
                                </span>
                            </label>
                            <div class="md-form mb-5" id="note">
                                <div class="summernote_inner body_notes" ></div>
                            </div>
                            <input type="hidden" name="notes" class="empty_input" id="notes_value">
                        @endif
                    </div>
                    <div class="modal-footer d-flex justify-content-center">
                        <button class="form_submit_check btn btn-primary btn-sm" type="submit">
                            <span class="assign_class label{{getKeyid('save',$data)}}" data-id="{{getKeyid('save',$data) }}" data-value="{{checkKey('save',$data) }}" >
                                {!! checkKey('save',$data) !!}
                            </span>
                        </button>
                    </div>
                </form>
            @endif
        </div>
    </div>
</div>

<script type="text/javascript">
    $("#uploadForm").on('submit',function (event) {
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        event.preventDefault();
        if($("#title").val() ==""){
            $(".title_required_filed").html('Title fileld required');
            return 0;
        }
        $("#bulk_upload_img_loader").show();
        var formData = $("#uploadForm").serializeObject();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            url: '{{route('client_storeUpload')}}',
            data:new FormData(this),
            // dataType:'JSON',
            contentType:false,
            cache:false,
            processData:false,
            success: function(data)
            {
                if (data.status =="validation"){
                    $("#bulk_upload_img_loader").hide();
                    $(".file_required_filed").html(data.error);
                    toastr["error"](data.error);
                    return 0;
                }
                toastr["success"]('uploaded successfully');
                $("#bulk_upload_img_loader").hide();
                $('#upload_table').DataTable().clear().destroy();
                uploadAppend();
                $('#modalUpload').modal('hide');
                $("#title").val('');
                $("#status").children('option:selected').val('');
                $("#note_value").val('');
                $(".file-upload-preview").attr('style','display:none;');
                $(".file-upload>button").attr('style','display:none;');
                $('.note-editable').html('');
            },
            error: function (error) {
                console.log(error);
            }
        });
    });

    $("body").on("change",'#input-file-now',function () {
        if ($(".has-preview>.file-upload-preview>.file-upload-render").html() !==""){
            $(".file-upload>button").attr('style','display:block;');
        }
    });
</script>
