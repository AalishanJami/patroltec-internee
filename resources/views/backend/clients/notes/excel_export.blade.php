<table>
    <thead>
    <tr>
        @if(Auth()->user()->hasPermissionTo('clientNotes_subject_excel_export') || Auth::user()->all_companies == 1 )
            <th>Title</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('clientNotes_notes_excel_export') || Auth::user()->all_companies == 1 )
            <th>Note</th>
        @endif
    </tr>
    </thead>
    <tbody>
    @foreach($data as $list)
        <tr>
            @if(Auth()->user()->hasPermissionTo('clientNotes_subject_excel_export') || Auth::user()->all_companies == 1 )
                <td style="width: 25% !important;">{{ $list->title }}</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('clientNotes_notes_excel_export') || Auth::user()->all_companies == 1 )
                <td style="width: 75% !important;">{{ $list->note }}</td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>
