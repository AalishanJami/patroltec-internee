@php
    $data=localization();
@endphp
<style type="text/css">
    @media (min-width: 576px) {
        .modal-dialog {
            max-width: 75% !important;
        }
    }
    .error_message{
        position: absolute;
        top: 98%;
        text-transform: initial !important;
        left: 3.7% !important;
    }
</style>
<div class="modal fade" id="modalNotes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Notes</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="clientNoteForm" >
                <input type="hidden" name="flag" value="{{$flag ?? ''}}">
                <div class="modal-body mx-3">
                    <div class="md-form mb-5">
                        @if(Auth()->user()->hasPermissionTo('clientNotes_subject_create') || Auth::user()->all_companies == 1 )
                            <i class="fas fa-user prefix grey-text"></i>
                            <input type="text" name="title" id="title" class="form-control validate @error('name') is-invalid @enderror" name="name" value="" required autocomplete="name" autofocus>
                            <label for="name"  >
                                Title
                                <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                                   data-content="tool tip for Title"></i>
                            </label>
                            <small id="title_message" class="text-danger error_message"></small>
                        @endif
                    </div>
                    {{--                    <div class="md-form mb-5">--}}
                    {{--                        <textarea  form="clientNoteForm" id="document-full" name="note" class="ql-scroll-y" style="height: 300px; width: 100%"></textarea>--}}
                    {{--                    </div>--}}
                    @if(Auth()->user()->hasPermissionTo('clientNotes_notes_create') || Auth::user()->all_companies == 1 )
                        <div class="md-form mb-5" id="note">
                            <div class="summernote_inner body_notes" ></div>
                        </div>
                        <input type="hidden" name="note" id="note_value">
                    @endif
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button class="btn btn-primary btn-sm" type="button" onclick="save()">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="{{asset('editor/sprite.svg.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("body").on('focusout','.note-editable',function () {
            $("#note_value").val($(this).html());
        });
    });

    // var toolbarOptions = [
    //     ['bold', 'italic', 'underline'], // toggled buttons
    //     ['blockquote'],
    //
    //    // custom button values
    //     [{
    //       'list': 'ordered'
    //     }, {
    //       'list': 'bullet'
    //     }],
    //     [{
    //       'indent': '-1'
    //     }, {
    //       'indent': '+1'
    //     }], // outdent/indent
    //
    //
    //      // dropdown with defaults from theme
    //
    //     [{
    //       'align': []
    //     }],
    //    // remove formatting button
    //   ];
    // var quillFull = new Quill('#document-full', {
    //     modules: {
    //         toolbar: toolbarOptions,
    //         autoformat: true
    //     },
    //       theme: 'snow',
    //       placeholder: "Write something..."
    //   });


</script>
