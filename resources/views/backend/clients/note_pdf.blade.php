<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
</head>
<body>
<table class="table table-striped table-bordered">
    <thead>
    <tr>
        @if(Auth()->user()->hasPermissionTo('clientNotes_subject_pdf_export') || Auth::user()->all_companies == 1 )
            <th> Title</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('clientNotes_notes_pdf_export') || Auth::user()->all_companies == 1 )
            <th> Note</th>
        @endif
    </tr>
    </thead>
    <tbody>
    @foreach($data as $value)
        <tr>
            @if(Auth()->user()->hasPermissionTo('clientNotes_subject_pdf_export') || Auth::user()->all_companies == 1 )
                <td>{{ $value->title }}</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('clientNotes_notes_pdf_export') || Auth::user()->all_companies == 1 )
                <td>{{ $value->note }}</td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>
</body>
