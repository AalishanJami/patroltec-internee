@extends('backend.layouts.detail')
@section('title', 'Client Details')
@section('content')
    @php
        $data=localization();
    @endphp
    <style>
        .error_message{
            position: absolute;
            top: 98%;
            text-transform: initial !important;
            color:#e3342f;
            display:block;
        }
    </style>
    @include('backend.layouts.client_sidebar')
    <div class="padding-left-custom remove-padding">
        <ol class="breadcrumb">
            <li class="breadcrumb-item ">
                <a href="{{url('client')}}">
                    <span class="assign_class label{{getKeyid('client',$data)}}" data-id="{{getKeyid('client',$data) }}" data-value="{{checkKey('client',$data) }}" >
                        {!! checkKey('client',$data) !!}
                    </span>
                </a>
            </li>
            <li class="breadcrumb-item active_custom ">
                <span class="assign_class label{{getKeyid('detail',$data)}}" data-id="{{getKeyid('detail',$data) }}" data-value="{{checkKey('detail',$data) }}" >
                    {!! checkKey('detail',$data) !!}
                </span>
            </li>
            <li class="breadcrumb-item active_custom ">{{$client->title}}</li>
        </ol>
        <div class="locations-form container-fluid form_body" >
            <div class="card">
                <h5 class="card-header info-color white-text py-4">
                    <strong>
                        <span class="assign_class label{{getKeyid('general',$data)}}" data-id="{{getKeyid('general',$data) }}" data-value="{{checkKey('general',$data) }}" >
                            {!! checkKey('general',$data) !!}
                        </span>
                    </strong>
                </h5>
                <div class="card-body px-lg-5 pt-0">
                    @if(!empty($errors))
                        @if($errors->any())
                            <ul class="alert alert-danger" style="list-style-type: none">
                                @foreach($errors->all() as $error)
                                    <li>{!! $error !!}</li>
                                @endforeach
                            </ul>
                        @endif
                    @endif
                    <form class="text-center" id="update_client" style="color: #757575;" method="POST" action="{{url('client/updateDetail')}}">
                        @csrf
                        <div class="form-row">
                            <div class="col">
                                @if(Auth()->user()->hasPermissionTo('client_name_edit') || Auth::user()->all_companies == 1 )
                                <div class="md-form">
                                    <input type="text" id="update_client_title" name="title" class="form-control" @if(!empty($client->title))value="{{$client->title}}" @endif>
{{--                                    {!! Form::text('title',$client->title, ['class' => 'form-control']) !!}--}}
                                    <label class="active" for="materialRegisterFormFirstName">
                                        <span class="assign_class label{{getKeyid('name',$data)}}" data-id="{{getKeyid('name',$data) }}" data-value="{{checkKey('name',$data) }}" >
                                            {!! checkKey('name',$data) !!}
                                        </span>
                                    </label>
                                    <small id="project_name" class="text-danger error_message"></small>
                                </div>
                                @endif
                            </div>
                            <div class="col">
                                @if(Auth()->user()->hasPermissionTo('client_firstname_edit') || Auth::user()->all_companies == 1 )
                                <div class="md-form">
                                    {!! Form::text('first_name',$client->first_name, ['class' => 'form-control']) !!}
                                    <label class="active" for="materialRegisterFormLastName">
                                        <span class="assign_class label{{getKeyid('first_name',$data)}}" data-id="{{getKeyid('first_name',$data) }}" data-value="{{checkKey('first_name',$data) }}" >
                                            {!! checkKey('first_name',$data) !!}
                                        </span>
                                    </label>
                                </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                @if(Auth()->user()->hasPermissionTo('client_phone_edit') || Auth::user()->all_companies == 1 )
                                <div class="md-form">
                                    {!! Form::text('phone_number',$client->phone_number, ['class' => 'form-control']) !!}
                                    <label class="active" for="materialRegisterFormFirstName">
                                        <span class="assign_class label{{getKeyid('number',$data)}}" data-id="{{getKeyid('number',$data) }}" data-value="{{checkKey('number',$data) }}" >
                                            {!! checkKey('number',$data) !!}
                                        </span>
                                    </label>
                                </div>
                                @endif
                            </div>
                            <div class="col">
                                @if(Auth()->user()->hasPermissionTo('client_email_edit') || Auth::user()->all_companies == 1 )
                                <div class="md-form">
                                    {!! Form::email('email',$client->email, ['class' => 'form-control']) !!}
                                    <label class="active" for="materialRegisterFormFirstName">
                                        <span class="assign_class label{{getKeyid('email',$data)}}" data-id="{{getKeyid('email',$data) }}" data-value="{{checkKey('email',$data) }}" >
                                            {!! checkKey('email',$data) !!}
                                        </span>
                                    </label>
                                </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                <!-- First name -->
                                @if(Auth()->user()->hasPermissionTo('client_address_edit') || Auth::user()->all_companies == 1 )
                                <div class="md-form">
                                    {!! Form::text('address',$client->address, ['class' => 'form-control']) !!}
                                    <label class="active" for="materialRegisterFormFirstName">
                                        <span class="assign_class label{{getKeyid('address',$data)}}" data-id="{{getKeyid('address',$data) }}" data-value="{{checkKey('address',$data) }}" >
                                            {!! checkKey('address',$data) !!}
                                        </span>
                                    </label>
                                </div>
                                @endif
                            </div>

                        </div>
                        <input type="hidden" name="id" value="{{$client->id}}">
                        <button class="form_submit_check btn btn-outline-info btn-rounded btn-block my-4 waves-effect z-depth-0" type="submit">
                            <span class="assign_class label{{getKeyid('update',$data)}}" data-id="{{getKeyid('update',$data) }}" data-value="{{checkKey('update',$data) }}" >
                                {!! checkKey('update',$data) !!}
                            </span>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @component('backend.client.model.index')
    @endcomponent
    @component('backend.contractor.model.index')
    @endcomponent
    @component('backend.divisions.model.index')
    @endcomponent
    @component('backend.customers.model.index')
    @endcomponent
    @component('backend.site_groups.model.index')
    @endcomponent
    <script !src="">
        $("body").on("submit","#update_client",function(event){
            if ($("#update_client_title").val() ==""){
                $("#update_client_title").attr('style','border-bottom:1px solid #e64c48');
                $(".error_message").html('Title field required');
                event.preventDefault();
                return 0;
            }
            return true;
        });
    </script>
@endsection
