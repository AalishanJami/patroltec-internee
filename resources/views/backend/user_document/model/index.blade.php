@extends('backend.layouts.user')
@section('content')
    @php
       $data=localization();
    @endphp
    @include('backend.layouts.user_sidebar')
    <div class="padding-left-custom remove-padding">
        {{ Breadcrumbs::render('document') }}
           <div class="card">

        <div class="card-body">
            <div id="table" class="table-editable table-responsive">
        <span class="table-add float-right mb-3 mr-2">
            <a class="text-success"  data-toggle="modal" data-target="#modelcontactinfromation">
                <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
            </a>
        </span>
        <button class="btn btn-danger btn-sm">
            Delete Selected Contact
        </button>
        <button class="btn btn-primary btn-sm">
          Restore Deleted Contact
        </button>

        <button  style="display: none;">
            Show Active Contact
        </button>
        <button class="btn btn-warning btn-sm">
           Excel Export
        </button>
        <button class="btn btn-success btn-sm">
            Word Export
        </button>
        <br><br>
        <table  class="company_table_static table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th id="action" colspan="3">
                </th>

                <th>Document Name
                </th>
                <th> File
                </th>
                <th> Expiry
                </th>



            </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="3">
                        <a class="btn btn-warning btn-xs"><i class="fa fa-download"></i></a>
                         <a class="btn btn-success btn-xs"><i class="fas fa-eye"></i></a>
                        <a  data-toggle="modal" data-target="#modelcontactinfromation" class="btn btn-primary btn-xs"><i class="fas fa-pencil-alt fa-xs"></i></a>
                        <a type="button"class="btn btn-danger btn-xs" onclick="deleteContactInformationdata()"><i class="fas fa-trash fa-xs"></i></a>

                    </td>

                    <td  class="pt-3-half" >xyz</td>
                    <td class="pt-3-half" >090078601</td>
                    <td class="pt-3-half" >29/10/2020</td>

                </tr>

            </tbody>
        </table>
               </div>
        </div>
    </div>
        @component('backend.user_document.model.model.create')
        @endcomponent

         {{--    Edit Modal--}}
        @component('backend.user_document.model.model.edit')
        @endcomponent
        {{--    END Edit Modal--}}
    </div>
@endsection


<script type="text/javascript">
      window.onload = function() {
    document.getElementById("action").classList.remove('sorting_asc');
};
function deleteContactInformationdata()
{

    swal({
        title:'Are you sure?',
        text: "Delete Record.",
        type: "error",
        showCloseButton: true,
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, Delete!",
        cancelButtonText: "Cancel!",
        closeOnConfirm: false,
        customClass: "confirm_class",
        closeOnCancel: false,
    },
    function(isConfirm){
        swal.close();
 });


}

</script>
