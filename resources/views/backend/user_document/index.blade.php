

            @extends('backend.layouts.backend')
@section('content')
    @php
       $data=localization();
    @endphp
        {{ Breadcrumbs::render('completed/Forms') }}

        <div class="card">
            <div class="card-body">
                <div id="table" class="table-editable">
                    <span class="table-add float-right mb-3 mr-2">
                        <!-- <a class="text-success"  data-toggle="modal" data-target="#modelaccount">
                            <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
                        </a> -->
                    </span>
                    <button class="btn btn-danger btn-sm">
                        Delete Selected Completed Forms
                    </button>
                    <button class="btn btn-primary btn-sm">
                      Restore Deleted Completed Forms
                    </button>

                    <button  style="display: none;">
                        Show Active Account
                    </button>
                    <button class="btn btn-warning btn-sm">
                       Excel Export
                    </button>
                    <button class="btn btn-success btn-sm">
                        Word Export
                    </button>
                    <table  class="company_table_static table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>       
                    <th> Form Name
                    </th>
                    <th> Location Name
                    </th>
                    <th>Created By
                    </th>
                    <th>Date Time
                    </th>
                    <th>Report Date
                    </th>
                    <th id="action">
                    </th>
                </tr>
                </thead>
                    <tr>  
                        <td class="pt-3-half" contenteditable="true"> Welfare Checklist </td>
                        <td class="pt-3-half" contenteditable="true">Lillie Square (Block C1 (CP 07)) </td>
                        <td class="pt-3-half" contenteditable="true">Richard online Test</td>
                        <td class="pt-3-half" contenteditable="true">Perris</td>
                         <td class="pt-3-half" contenteditable="true">10 Dec 2019 12:07</td>
                     


                        <td>
                            <a  data-toggle="modal" data-target="#modalEditcompelete" class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i></a>
                            <button type="button"class="btn btn-danger btn-sm my-0" onclick="deletecomplete_formsdata()"><i class="fas fa-trash"></i></button>

                        </td>
                    </tr>
                <tbody>
                </tbody>
            </table>
                </div>
            </div>
        </div>
        @component('backend.complete_list.model.edit')
        @endcomponent

        

@endsection
<script type="text/javascript">
function complete_list()
{

    swal({
        title:'Are you sure?',
        text: "Delete Record.",
        type: "error",
        showCloseButton: true,
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, Delete!",
        cancelButtonText: "Cancel!",
        closeOnConfirm: false,
        customClass: "confirm_class",
        closeOnCancel: false,
    },
    function(isConfirm){
        swal.close();
 });


}

</script>
