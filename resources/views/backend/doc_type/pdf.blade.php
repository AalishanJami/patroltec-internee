@include('backend.layouts.pdf_start')
  <thead>
    <tr>
      <th style="vertical-align: text-top;" class="pdf_table_layout">Name</th>
    </tr>
  </thead>
  <tbody>
  @foreach($data as $value)
    <tr>
      <td class="pdf_table_layout"><p class="col_text_style">{{ $value->name }}</p></td>
    </tr>
  @endforeach
  </tbody>
@include('backend.layouts.pdf_end')