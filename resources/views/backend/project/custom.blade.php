@extends('backend.layouts.detail')
@section('content')
    @php
       $data=localization();
    @endphp
    <div class="row">
      @include('backend.layouts.detail_sidebar')
      <div class="padding-left-custom remove-padding">
        {{ Breadcrumbs::render('detail') }}
        <div class="locations-form container-fluid form_body" >
           <form>
              <input type="hidden" name="_csrf-backend" value="o3P9KIBFoQ0d-4_FrL8468LQikg2gb613QiHzsBGuVtyUJ3ZAoEBV_tyUYNK48rkPsGGeTY4XBPmNdHB19OcsQ==">
              <div id="w1">
              </div>
              <div class="row">
                 <div class="col-lg-11">
                    <div class="row">
                       <!-- Left Block -->
                       <div class="col-lg-6">
                          <div class="col-lg-12 edit_sub_header">General</div>
                          <div class="col-lg-12">
                             <div class="form-group field-locations-name required">
                                <label class="control-label" for="locations-name">Project Name</label>
                                <input type="text" id="locations-name" class="form-control" name="Locations[name]" value="Lower Thames Crossing" maxlength="50" aria-required="true">
                                <div class="help-block"></div>
                             </div>
                          </div>
                          <div class="col-lg-6">
                             <div class="form-group field-locations-ref">
                                <label class="control-label" for="locations-ref">Ref</label>
                                <input type="text" id="locations-ref" value="11009888" class="form-control" name="Locations[ref]" value="">
                                <div class="help-block"></div>
                             </div>
                          </div>
                          <div class="col-lg-12">
                             <div class="form-group field-locations-client_name">
                                <label class="control-label" for="locations-client_name">Managers Name</label>
                                <input type="text" id="locations-client_name" class="form-control" name="Locations[client_name]" value="Billy Jones" maxlength="100">
                                <div class="help-block"></div>
                             </div>
                          </div>
                        
                            <div class="col-lg-12">
                             <div class="form-group field-locations-office required">
                                <label class="control-label" for="locations-office">Client</label>
                                 <a class="text-success"  data-toggle="modal" data-target="#modalClientIndex">
                                    <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
                                </a>

                                <select searchable="Search here.."  id="locations-office" class="form-control select2-hidden-accessible" name="Locations[office]" aria-required="true" data-s2-options="s2options_d6851687" data-krajee-select2="select2_6a081501"  tabindex="-1" aria-hidden="true">
                                   <option value="">Please Select ...</option>
                                   <option value="1" selected="">Logistics</option>
                                   <option value="2">Scaffolding</option>
                                </select>
                                
                                <div class="help-block"></div>
                             </div>
                          </div>
                          <div class="col-lg-12">
                             <div class="form-group field-locations-office required">
                                <label class="control-label" for="locations-office">Division
                                   <a class="text-success"  data-toggle="modal" data-target="#modalDevisionsIndex">
                                    <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
                                </a>
                                </label>
                                <select searchable="Search here.."  id="locations-office" class="form-control select2-hidden-accessible" name="Locations[office]" aria-required="true" data-s2-options="s2options_d6851687" data-krajee-select2="select2_6a081501"  tabindex="-1" aria-hidden="true">
                                   <option value="">Please Select ...</option>
                                   <option value="1" selected="">Logistics</option>
                                   <option value="2">Scaffolding</option>
                                </select>
                                
                                <div class="help-block"></div>
                             </div>
                          </div>
                           <div class="col-lg-12">
                             <div class="form-group field-locations-subcontractor">
                                <label class="control-label" for="locations-subcontractor">Customer
                                  <a class="text-success"  data-toggle="modal" data-target="#modalcustomerIndex">
                                    <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
                                  </a>

                                </label>
                                <select searchable="Search here.."  id="locations-subcontractor" class="form-control select2-hidden-accessible" name="Locations[subcontractor]" data-s2-options="s2options_d6851687" data-krajee-select2="select2_6a081501"  tabindex="-1" aria-hidden="true">
                                   <option value="">Please Select ...</option>
                                   <option value="755" selected>Aecom</option>
                                   <option value="748">Argent</option>
                                   <option value="753">Ballymore</option>
                                   <option value="756">Buckingham</option>
                                   <option value="749">Canary Wharf Contractors</option>
                                   <option value="757">Derwent</option>
                                   <option value="754">ISG</option>
                                   <option value="747">Kier</option>
                                   <option value="746">Mace</option>
                                   <option value="750">Quintain</option>
                                   <option value="752">Sir Robert McAlpine</option>
                                   <option value="8">Wates</option>
                                   <option value="751">Westfield</option>
                                </select>
                                <div class="help-block"></div>
                             </div>
                          </div>
                           <div class="col-lg-12">
                             <div class="form-group field-locations-subcontractor">
                                <label class="control-label" for="locations-subcontractor">Site Group
                                  <a class="text-success"  data-toggle="modal" data-target="#modalSIteGroupIndex">
                                    <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
                                  </a>
                                </label>
                                <select searchable="Search here.."  id="locations-subcontractor" class="form-control select2-hidden-accessible" name="Locations[subcontractor]" data-s2-options="s2options_d6851687" data-krajee-select2="select2_6a081501"  tabindex="-1" aria-hidden="true">
                                   <option value="">Please Select ...</option>
                                   <option value="755">Aecom</option>
                                   <option value="748">Argent</option>
                                   <option value="753">Ballymore</option>
                                   <option value="756">Buckingham</option>
                                   <option value="749">Canary Wharf Contractors</option>
                                   <option value="757">Derwent</option>
                                   <option value="754">ISG</option>
                                   <option value="747" selected>Kier</option>
                                   <option value="746">Mace</option>
                                   <option value="750">Quintain</option>
                                   <option value="752">Sir Robert McAlpine</option>
                                   <option value="8">Wates</option>
                                   <option value="751">Westfield</option>
                                </select>
                                <div class="help-block"></div>
                             </div>
                          </div>
                           <div class="col-lg-12">
                                <div class="form-group field-locations-terminationreason">
                                   <label class="control-label" for="locations-terminationreason">Termination Reason</label>
                                   <div class="redactor-box" role="application">
                                      <textarea id="locations-terminationreason" name="Locations[terminationReason]" dir="ltr" >Not follow Deadline</textarea>
                                   </div>
                                   <div class="help-block"></div>
                                </div>
                            </div>
                    
                       </div>
                       <!-- Right Block -->
                       <div class="col-lg-6">
                          <div class="col-lg-12 edit_sub_header">Notes</div>
                          <div class="col-lg-4">
                             <div class="form-group field-locations-travel_time">
                                <label class="control-label" for="locations-travel_time"></label>
                                <textarea>we want all thing before time</textarea>
                                <div class="help-block"></div>
                             </div>
                          </div>
                           <div class="col-lg-6">
                             <div class="form-group field-locations-account_number">
                                <label class="control-label" for="locations-account_number">Project ID</label>
                                <input type="text" id="locations-account_number" class="form-control" name="Locations[account_number]" value="L4387" maxlength="10">
                                <div class="help-block"></div>
                             </div>
                          </div>
                          <div class="col-lg-6">
                             <div class="form-group field-locations-account_number">
                                <label class="control-label" for="locations-account_number">Accout Number</label>
                                <input type="text" id="locations-account_number" class="form-control" name="Locations[account_number]" value="L4387" maxlength="10">
                                <div class="help-block"></div>
                             </div>
                          </div>
            
                            <div class="col-lg-12">
                                <div class="form-group field-locations-user_id">
                                    <label class="control-label" for="locations-user_id">Contract Manager</label>
                                    <select searchable="Search here.."  id="locations-user_id" class="form-control select2-hidden-accessible" name="Locations[user_id]" data-s2-options="s2options_d6851687" data-krajee-select2="select2_6a081501"  tabindex="-1" aria-hidden="true">
                                       <option value="">Please Select ...</option>
                                       <option value="4214">Alandale Demo</option>
                                       <option value="10683">Billy Allison</option>
                                       <option value="10781">Bob Marriott</option>
                                       <option value="138">CIBI WALK</option>
                                       <option value="10417">Ciprian Murgus</option>
                                       <option value="10418">Dan  Balta</option>
                                       <option value="10420">Daniel  Balta.</option>
                                       <option value="137">Dave Bloggs</option>
                                       <option value="10681" selected="">David Brookes</option>
                                       <option value="10688">David Bulman</option>
                                       <option value="10421">Florin  Nitu</option>
                                       <option value="10422">Iqbal  Shahid</option>
                                       <option value="10626">Jannie van der Sandt</option>
                                       <option value="10682">Jason Hallam</option>
                                       <option value="10651">Luana Neacsu</option>
                                       <option value="10423">Marcelus  Ofomata</option>
                                       <option value="10424">Mehari  Isaak</option>
                                       <option value="10425">Muhhamad  Tahir</option>
                                       <option value="10803">Peter Hammes</option>
                                       <option value="10419">Ruth Trant</option>
                                       <option value="10680">Tom Berrington</option>
                                    </select>
                                   
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-lg-12 edit_sub_header">Contract
                              <a class="text-success"  data-toggle="modal" data-target="#modalContractIndex">
                                    <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
                                </a>
                            </div>
                             <div class="col-lg-12">
                                <div class="form-group field-locations-renewal required">
                                   <select searchable="Search here.."  id="locations-renewal" class="form-control select2-hidden-accessible" name="Locations[renewal]" aria-required="true" data-s2-options="s2options_d6851687" data-krajee-select2="select2_6a081501"  tabindex="-1" aria-hidden="true">
                                      <option value="">Please Select ...</option>
                                      <option value="12" selected="">Annual</option>
                                      <option value="6">Six Monthly</option>
                                      <option value="3">Quarterly</option>
                                      <option value="1">Monthly</option>
                                   </select>
                                   <div class="help-block"></div>
                                </div>
                             </div>
                            <div class="col-lg-6">
                                   <label class="control-label form_date_field_lable"><label for="locations-contract_start">Start Date</label></label>
                                   <div id="locations-contract_start-kvdate" class="input-group date"><input type="text" id="locations-contract_start" class="krajee-datepicker form-control" name="Locations[contract_start]" value="01 Oct, 2019" data-datepicker-source="locations-contract_start-kvdate" data-datepicker-type="3" data-krajee-kvdatepicker="kvDatepicker_6ccdcf97"><span class="input-group-addon kv-date-remove" title="Clear field"><i class="glyphicon glyphicon-remove"></i></span><span class="input-group-addon kv-date-calendar" title="Select date"><i class="glyphicon glyphicon-calendar"></i></span></div>
                             </div>
                             <div class="col-lg-6">
                                   <label class="control-label form_date_field_lable"><label for="locations-date_end">End date</label></label>
                                   <div id="locations-date_end-kvdate" class="input-group date"><input type="text" id="locations-date_end" value="08 Oct, 2019" class="krajee-datepicker form-control" name="Locations[date_end]" data-datepicker-source="locations-date_end-kvdate" data-datepicker-type="3" data-krajee-kvdatepicker="kvDatepicker_6ccdcf97"><span class="input-group-addon kv-date-remove" title="Clear field"><i class="glyphicon glyphicon-remove"></i></span><span class="input-group-addon kv-date-calendar" title="Select date"><i class="glyphicon glyphicon-calendar"></i></span></div>
                             </div>
                             <div class="col-lg-12">
                                <div class="custom_error"></div>
                                <div class="custom_error"></div>
                             </div>
                       </div>
                    </div>
                 </div>
              </div>
           </form>
        </div>
      </div>
    </div>


 
  @component('backend.client.model.index')
  @endcomponent
    
  @component('backend.contractor.model.index')
  @endcomponent
  @component('backend.divisions.model.index')
  @endcomponent
  @component('backend.customers.model.index')
  @endcomponent
  @component('backend.site_groups.model.index')
  @endcomponent




  

@endsection