<table >
    <thead>
    <tr>
        @if(Auth()->user()->hasPermissionTo('project_name_excel_export') || Auth::user()->all_companies == 1 )
        <th> Name</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('project_devision_excel_export') || Auth::user()->all_companies == 1 )
        <th> Division</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('project_customer_excel_export') || Auth::user()->all_companies == 1 )
        <th> Customer</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('project_group_excel_export') || Auth::user()->all_companies == 1 )
        <th> Group</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('project_manager_excel_export') || Auth::user()->all_companies == 1 )
        <th> Managers</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('project_contract_manager_excel_export') || Auth::user()->all_companies == 1 )
        <th>Contract Manager</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('project_client_excel_export') || Auth::user()->all_companies == 1 )
        <th>Client Name</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('project_start_date_excel_export') || Auth::user()->all_companies == 1 )
        <th>Start Date</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('project_expiry_date_excel_export') || Auth::user()->all_companies == 1 )
        <th>Expiry Date</th>
        @endif
        
    </tr>
    </thead>
    <tbody>
    @foreach($data as $value)
        <tr>
            @if(Auth()->user()->hasPermissionTo('project_name_excel_export') || Auth::user()->all_companies == 1 )
            <td>{{ $value->name }}</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('project_devision_excel_export') || Auth::user()->all_companies == 1 )
            <td>@if(!empty($value->devisions)){{$value->devisions->name}}@endif</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('project_customer_excel_export') || Auth::user()->all_companies == 1 )
            <td>@if(!empty($value->customer)){{ $value->customer->name}}@endif</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('project_group_excel_export') || Auth::user()->all_companies == 1 )
                <td>@if(!empty($value->siteGroup)){{ $value->siteGroup->name }}@endif</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('project_manager_excel_export') || Auth::user()->all_companies == 1 )
                <td>@if(!empty($value->userManager)){{ $value->userManager->name }}@endif</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('project_contract_manager_excel_export') || Auth::user()->all_companies == 1 )
                <td>@if(!empty($value->userContractManager)){{ $value->userContractManager->name }}@endif</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('project_client_excel_export') || Auth::user()->all_companies == 1 )
                <td>@if(!empty($value->clients)){{ $value->clients->first_name }} @endif</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('project_start_date_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{ $value->contract_start }}</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('project_expiry_date_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{ $value->contract_end }}</td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>