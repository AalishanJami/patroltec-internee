@extends('backend.layouts.backend')
@section('title', 'Client')
@section('content')
    @php
        $data=localization();
    @endphp
        {{Breadcrumbs::render('clients')}}
        @if(Auth()->user()->hasPermissionTo('create_users') || Auth::user()->all_companies == 1 )
        <span class="table-add float-right mb-3 mr-2">
            <a class="text-success" id="create_client" data-toggle="modal" data-target="#modalClientForm2">
                <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
            </a>
        </span>
        @endif
        @if(Auth()->user()->hasPermissionTo('selected_delete_users') || Auth::user()->all_companies == 1 )
         <button id="deleteSelectedClient"  data-id="{{getKeyid('Delete_Selected_client',$data) }}" data-value="{{checkKey('Delete_Selected_client',$data) }}" class=" assign_class btn btn-danger btn-sm label{{getKeyid('Delete_Selected_client',$data)}}">
             <!-- {!! checkKey('Delete_Selected_client',$data) !!} -->Delete_Selected_client
         </button>
        @endif
        @if(Auth()->user()->hasPermissionTo('selected_restore_delete_users') || Auth::user()->all_companies == 1 )
        <button id="restoreButtonClient" data-id="{{getKeyid('Restore_Deleted_client',$data) }}" data-value="{{checkKey('Restore_Deleted_client',$data) }}"   class="btn btn-primary btn-sm assign_class label{{getKeyid('Restore_Deleted_client',$data)}}">
           <!--  {!! checkKey('Restore_Deleted_client',$data) !!} -->Restore_Deleted_client
        </button>
        @endif
        @if(Auth()->user()->hasPermissionTo('active_users') || Auth::user()->all_companies == 1 )
        <button id="showactivebuttonClient" data-id="{{getKeyid('show_active_users',$data) }}" data-value="{{checkKey('show_active_users',$data) }}"  class="btn btn-primary btn-sm assign_class label{{getKeyid('show_active_users',$data)}}" style="display: none;">
            {!! checkKey('show_active_users',$data) !!}
        </button>
        @endif
        @if(Auth()->user()->hasPermissionTo('csv_users') || Auth::user()->all_companies == 1 )
<!-- 
        <form class="form-style" method="POST" action="{{ url('export_excel_user') }}">
                <input type="hidden" id="export_excel_user" name="export_excel_user" value="1">
                {!! csrf_field() !!}
                 <button  type="submit" data-id="{{getKeyid('export_excel_user',$data) }}" data-value="{{checkKey('export_excel_user',$data) }}"  class="btn btn-warning btn-sm assign_class label{{getKeyid('export_excel_user',$data)}}">
                {!! checkKey('export_excel_user',$data) !!}
            </button>
        </form> -->

       
        @endif
        @if(Auth()->user()->hasPermissionTo('word_users') || Auth::user()->all_companies == 1 )
        <!--  <form  class="form-style"  method="POST" action="{{ url('export_word_user') }}">
                <input type="hidden" id="export_word_user" name="export_word_user" value="1">
                {!! csrf_field() !!}
                 <button  type="submit" data-id="{{getKeyid('export_word_user',$data) }}" data-value="{{checkKey('export_word_user',$data) }}"  class="btn btn-success btn-sm assign_class label{{getKeyid('export_word_user',$data)}}">
                    {!! checkKey('export_word_user',$data) !!}
            </button>
        </form> -->
        @endif
    @include('backend.label.input_label')

    <table id="clienttable" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th class="assign_class label{{getKeyid('checkbox',$data)}}" data-id="{{getKeyid('checkbox',$data) }}" data-value="{{checkKey('checkbox',$data) }}" >{!! checkKey('checkbox',$data) !!}</th>
            <th class="th-sm assign_class label{{getKeyid('title',$data)}}" data-id="{{getKeyid('title',$data) }}" data-value="{{ checkKey('title',$data) }}" ><!--{ !! checkKey('title',$data) !!} -->title
            </th>
            <th class="th-sm assign_class label{{getKeyid('email',$data)}}" data-id="{{getKeyid('email',$data) }}" data-value="{{ checkKey('email',$data) }}" >{!!checkKey('email',$data) !!}
            </th>
            <th class="th-sm assign_class label{{getKeyid('first_name',$data)}}" data-id="{{getKeyid('first_name',$data) }}" data-value="{{ checkKey('first_name',$data) }}" ><!-- {!!checkKey('first_name',$data) !!} -->first_name
            </th>
            <th class="th-sm assign_class label{{getKeyid('surname',$data)}}" data-id="{{getKeyid('surname',$data) }}" data-value="{{ checkKey('surname',$data) }}" ><!-- {!!checkKey('surname',$data) !!} -->surname
            </th>
            <th class="th-sm assign_class label{{getKeyid('phone_number',$data)}}" data-id="{{getKeyid('phone_number',$data) }}" data-value="{{ checkKey('phone_number',$data) }}" ><!-- {!!checkKey('phone_number',$data) !!} -->phone_number
            </th>
            <th class="th-sm assign_class label{{getKeyid('address',$data)}}" data-id="{{getKeyid('address',$data) }}" data-value="{{ checkKey('address',$data) }}" ><!-- {!!checkKey('address',$data) !!} -->address
            </th>
            <th class="th-sm assign_class label{{getKeyid('actions',$data)}}" data-id="{{getKeyid('actions',$data) }}" data-value="{{ checkKey('actions',$data) }}" >{!!checkKey('actions',$data) !!}
            </th>
        </tr>
        </thead>

        <tbody>
        </tbody>
    </table>
    @if(Auth()->user()->all_companies == 1)
     <input type="hidden" id="checkbox_permission" value="1">
    <input type="hidden" id="name_permission" value="1">
     <input type="hidden" id="email_permission" value="1">
    @else
    <input type="hidden" id="checkbox_permission" value="{{Auth()->user()->hasPermissionTo('user_checkbox')}}">
    <input type="hidden" id="name_permission" value="{{Auth()->user()->hasPermissionTo('user_name')}}">
    <input type="hidden" id="email_permission" value="{{Auth()->user()->hasPermissionTo('user_email')}}">
    @endif
     {{--    Edit Modal--}}
    @component('backend.client.model.edit', ['companies' => $companies])
    @endcomponent
    {{--    END Edit Modal--}}

    {{--    Register Modal--}}
    @component('backend.client.model.create')
    @endcomponent
    {{--end modal register--}}

    {{--    Register Modal--}}
    @component('backend.user.model.role')
    @endcomponent
    {{--end modal register--}}

    <script type="text/javascript">
        var company_id = 1;
        var editor;
        $(document).ready(function() {

            $(function () {
                clientAppend();
            }); 
            (function ($) {
                var active ='<span class="assign_class label'+$('#manage_users_label').attr('data-id')+'" data-id="'+$('#manage_users_label').attr('data-id')+'" data-value="'+$('#manage_users_label').val()+'" >'+$('#manage_users_label').val()+'</span>';
                $('.breadcrumb-item.active').html(active);
                var parent ='<span class="assign_class label'+$('#dashboard_label').attr('data-id')+'" data-id="'+$('#dashboard_label').attr('data-id')+'" data-value="'+$('#dashboard_label').val()+'" >'+$('#dashboard_label').val()+'</span>';
                $('.breadcrumb-item a').html(parent);
            }(jQuery));
          
      
        }); 
        function clientAppend()
        {
              

            var table = $('#clienttable').dataTable({
               processing: true,
                language: {
                    'lengthMenu': '<span class="assign_class label'+$('#show_label').attr('data-id')+'" data-id="'+$('#show_label').attr('data-id')+'" data-value="'+$('#show_label').val()+'" >'+$('#show_label').val()+'</span>  _MENU_ <span class="assign_class label'+$('#entries_label').attr('data-id')+'" data-id="'+$('#entries_label').attr('data-id')+'" data-value="'+$('#entries_label').val()+'" >'+$('#entries_label').val()+'</span>',
                    'search':'<span class="assign_class label'+$('#search_label').attr('data-id')+'" data-id="'+$('#search_label').attr('data-id')+'" data-value="'+$('#search_label').val()+'" >'+$('#search_label').val()+'</span>',
                    'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                    'info': '<span class="assign_class label'+$('#showing_page_label').attr('data-id')+'" data-id="'+$('#showing_page_label').attr('data-id')+'" data-value="'+$('#showing_page_label').val()+'" >'+$('#showing_page_label').val()+'</span> _PAGE_ <span class="assign_class label'+$('#of_label').attr('data-id')+'" data-id="'+$('#of_label').attr('data-id')+'" data-value="'+$('#of_label').val()+'" >'+$('#of_label').val()+'</span> _PAGES_',
                    'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                    'paginate': {
                        'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                        'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                    },
                    'infoFiltered': "(filtered from _MAX_ total records)"
                },
                "ajax": {
                    "url": 'getallclients',
                    "type": 'get',
                },
              "createdRow": function( row, data, dataIndex ) {
                
                    var checkbox_permission=$('#checkbox_permission').val();
                    console.log(checkbox_permission);
                    console.log(data);
                    $(row).attr('id', data['id']);
                    var temp=data['id'];
                    $(row).attr('onclick',"selectedrow(this.id)");
                },
                columns: [
                {data: 'checkbox', name: 'checkbox',visible:$('#checkbox_permission').val(),},
                    {data: 'title', name: 'title',visible:$('#name_permission').val(),},
                    {data: 'email', name: 'email',visible:$('#email_permission').val(),},
                    {data: 'first_name', name: 'first_name'},
                    {data: 'surname', name: 'surname'},
                    {data: 'phone_number', name: 'phone_number'},
                    {data: 'address', name: 'address'},
                    {data: 'actions', name: 'actions'},

                ],
                columnDefs: [{
                orderable: false,
                className: 'select-checkbox',
                targets: 0
                }],
            
                select: {
                style: 'os',
                selector: 'td:first-child'
                },

            });
        }
    </script>
@endsection
