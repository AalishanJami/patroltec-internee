@include('backend.layouts.pdf_start')
<thead>
<tr>
    @if(Auth()->user()->hasPermissionTo('project_name_pdf_export') || Auth::user()->all_companies == 1 )
        <th style="vertical-align: text-top;" class="pdf_table_layout"> Name</th>
    @endif
    @if(Auth()->user()->hasPermissionTo('project_devision_pdf_export') || Auth::user()->all_companies == 1 )
        <th style="vertical-align: text-top;" class="pdf_table_layout"> Division</th>
    @endif
    @if(Auth()->user()->hasPermissionTo('project_customer_pdf_export') || Auth::user()->all_companies == 1 )
        <th style="vertical-align: text-top;" class="pdf_table_layout"> Customer</th>
    @endif
    @if(Auth()->user()->hasPermissionTo('project_group_pdf_export') || Auth::user()->all_companies == 1 )
        <th style="vertical-align: text-top;" class="pdf_table_layout"> Group</th>
    @endif
    @if(Auth()->user()->hasPermissionTo('project_manager_pdf_export') || Auth::user()->all_companies == 1 )
        <th style="vertical-align: text-top;" class="pdf_table_layout"> Managers</th>
    @endif
    @if(Auth()->user()->hasPermissionTo('project_contract_manager_pdf_export') || Auth::user()->all_companies == 1 )
        <th style="vertical-align: text-top;" class="pdf_table_layout">Contract Manager</th>
    @endif
    @if(Auth()->user()->hasPermissionTo('project_client_pdf_export') || Auth::user()->all_companies == 1 )
        <th style="vertical-align: text-top;" class="pdf_table_layout">Client Name</th>
    @endif
    @if(Auth()->user()->hasPermissionTo('project_start_date_pdf_export') || Auth::user()->all_companies == 1 )
        <th style="vertical-align: text-top;" class="pdf_table_layout">Start Date</th>
    @endif
    @if(Auth()->user()->hasPermissionTo('project_expiry_date_pdf_export') || Auth::user()->all_companies == 1 )
        <th style="vertical-align: text-top;" class="pdf_table_layout">Expiry Date</th>
    @endif
</tr>
</thead>
<tbody>
@foreach($data as $value)
    @if(!empty($value->name))
        <tr>
            @if(Auth()->user()->hasPermissionTo('project_name_pdf_export') || Auth::user()->all_companies == 1 )
                <td class="pdf_table_layout"><p class="col_text_style">{{ $value->name }}</p></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('project_devision_pdf_export') || Auth::user()->all_companies == 1 )
                    <td class="pdf_table_layout"><p class="col_text_style">@if(!empty($value->devisions)){{$value->devisions->name}}@endif</p></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('project_customer_pdf_export') || Auth::user()->all_companies == 1 )
                    <td class="pdf_table_layout"><p class="col_text_style">@if(!empty($value->customer)){{ $value->customer->name}}@endif</p></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('project_group_pdf_export') || Auth::user()->all_companies == 1 )
                    <td class="pdf_table_layout"><p class="col_text_style">@if(!empty($value->siteGroup)){{ $value->siteGroup->name }}@endif</p></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('project_manager_pdf_export') || Auth::user()->all_companies == 1 )
                    <td class="pdf_table_layout"><p class="col_text_style">@if(!empty($value->userManager)){{ $value->userManager->name }}@endif</p></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('project_contract_manager_pdf_export') || Auth::user()->all_companies == 1 )
                    <td class="pdf_table_layout"><p class="col_text_style">@if(!empty($value->userContractManager)){{ $value->userContractManager->name }}@endif</p></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('project_client_pdf_export') || Auth::user()->all_companies == 1 )
                    <td class="pdf_table_layout"><p class="col_text_style">@if(!empty($value->clients)){{ $value->clients->first_name }} @endif</p></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('project_start_date_pdf_export') || Auth::user()->all_companies == 1 )
                    <td class="pdf_table_layout"><p class="col_text_style">{{ $value->contract_start }}</p></td>
            @endif
            @if(Auth()->user()->hasPermissionTo('project_expiry_date_pdf_export') || Auth::user()->all_companies == 1 )
                    <td class="pdf_table_layout"><p class="col_text_style">{{ $value->contract_end }}</p></td>
            @endif
        </tr>
    @endif
@endforeach
</tbody>
@include('backend.layouts.pdf_end')

