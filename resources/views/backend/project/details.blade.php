@extends('backend.layouts.detail')
@section('title', 'Project Detail')
@section('headscript')
    <script src="{{ asset('custom/js/contractor.js') }}"></script>
    <script src="{{ asset('custom/js/devision.js') }}"></script>
    <script src="{{ asset('custom/js/customer.js') }}"></script>
    <script src="{{ asset('custom/js/client.js') }}"></script>
    <script src="{{ asset('custom/js/site_group.js') }}"></script>
    <script src="{{ asset('custom/js/manager.js') }}"></script>
    <script src="{{ asset('custom/js/contractor_manager.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('custom/css/custom_style.css') }}">
@stop
@section('content')
    @php
       $data=localization();
    @endphp
    @include('backend.layouts.detail_sidebar')
    <div class="padding-left-custom remove-padding">
        <ol class="breadcrumb">
           <!--  <li class="breadcrumb-item">
                <a href="{{url('dashboard')}}">
                    <span class="assign_class label{{getKeyid('dashboard',$data)}}" data-id="{{getKeyid('dashboard',$data) }}" data-value="{{checkKey('dashboard',$data) }}" >
                        {!! checkKey('dashboard',$data) !!}
                    </span>
                </a>
            </li> -->
            <li class="breadcrumb-item ">
                <a href="{{url('project')}}">
                    <span class="assign_class label{{getKeyid('project',$data)}}" data-id="{{getKeyid('project',$data) }}" data-value="{{checkKey('project',$data) }}" >
                        {!! checkKey('project',$data) !!}
                    </span>
                </a>
            </li>
            <li class="breadcrumb-item active_custom ">
                <span class="assign_class label{{getKeyid('detail',$data)}}" data-id="{{getKeyid('detail',$data) }}" data-value="{{checkKey('detail',$data) }}" >
                    {!! checkKey('detail',$data) !!}
                </span>
            </li>
            <li class="breadcrumb-item active_custom ">{{$project->name}}</li>
        </ol>
        <div class="locations-form container-fluid form_body" >
            <div class="card">
                <h5 class="card-header info-color white-text py-4">
                    <strong>
                        <span class="assign_class label{{getKeyid('general',$data)}}" data-id="{{getKeyid('general',$data) }}" data-value="{{checkKey('general',$data) }}" >
                            {!! checkKey('general',$data) !!}
                        </span>
                    </strong>
                </h5>
                <div class="card-body px-lg-5 pt-0">
                    <form class="text-center" style="color: #757575;" action="{{url('/project/updateDetail')}}" method="post">
                        @csrf
                        <div class="form-row">
                            @if(Auth()->user()->hasPermissionTo('project_name_edit') || Auth::user()->all_companies == 1 )
                                <div class="col">
                                    <div class="md-form">
                                        @if ($errors->has('name'))
                                            @php $style ="border-bottom:1px solid #e3342f"; @endphp
                                        @else
                                            @php $style =""; @endphp
                                        @endif
                                        {!! Form::text('name',$project->name, ['class' => 'form-control','id'=>'project_name','style'=>$style]) !!}
                                        <label class="active" for="materialRegisterFormFirstName">
                                            <span class="assign_class label{{getKeyid('name',$data)}}" data-id="{{getKeyid('name',$data) }}" data-value="{{checkKey('name',$data) }}" >
                                                {!! checkKey('name',$data) !!}
                                            </span>
                                        </label>
                                        @if ($errors->has('name'))
                                            <small class="text-danger" style="left: 0% !important;" id="form_name_required_message">{{ $errors->first('name') }}</small>
                                        @endif
                                    </div>
                                </div>
                            @endif
                            @if(Auth()->user()->hasPermissionTo('project_ref_edit') || Auth::user()->all_companies == 1 )
                                <div class="col">
                                    <div class="md-form">
                                        {!! Form::text('ref',$project->ref, ['class' => 'form-control']) !!}
                                        <label class="active" for="materialRegisterFormLastName">
                                            <span class="assign_class label{{getKeyid('ref',$data)}}" data-id="{{getKeyid('ref',$data) }}" data-value="{{checkKey('ref',$data) }}" >
                                                {!! checkKey('ref',$data) !!}
                                            </span>
                                        </label>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="form-row">
                            @if(Auth()->user()->hasPermissionTo('project_account_number_edit') || Auth::user()->all_companies == 1 )
                                <div class="col-md-6">
                                    <div class="md-form">
                                        {!! Form::text('account_number',$project->account_number, ['class' => 'form-control']) !!}
                                        <label class="active" for="materialRegisterFormLastName">
                                            <span class="assign_class label{{getKeyid('account_number',$data)}}" data-id="{{getKeyid('account_number',$data) }}" data-value="{{checkKey('account_number',$data) }}" >
                                                {!! checkKey('account_number',$data) !!}
                                            </span>
                                        </label>
                                    </div>
                                </div>
                            @endif
                            {!! Form::hidden('id',$project->id, ['class' => 'form-control','readonly']) !!}
                        </div>
                        <div class="form-row">
                            @if(Auth()->user()->hasPermissionTo('project_contract_edit') || Auth::user()->all_companies == 1 )
                                <div class="col">
                                    <a class="text-success modal_contrator">
                                        <button type="button" class="btn btn-sm btn-outline-primary waves-effect form-custom-boutton" style="float: left;">
                                            <span class="assign_class label{{getKeyid('contractor',$data)}}" data-id="{{getKeyid('contractor',$data) }}" data-value="{{checkKey('contractor',$data) }}" >
                                                {!! checkKey('contractor',$data) !!}
                                            </span>
                                        </button>
                                    </a>
                                    <div class="md-form contractor_listin">
                                        {!! Form::select('contractor_id',$contractors, $project->contractor_id, ['class' =>'mdb-select','placeholder'=>'---Please Select----','id'=>'contractor_id']) !!}
                                    </div>
                                </div>
                            @endif
                            @if(Auth()->user()->hasPermissionTo('project_client_edit') || Auth::user()->all_companies == 1 )
                                <div class="col">
                                    <a class="text-success modal_client">
                                        <button type="button" class="btn btn-sm btn-outline-primary waves-effect form-custom-boutton" style="float: left;">
                                            <span class="assign_class label{{getKeyid('client',$data)}}" data-id="{{getKeyid('client',$data) }}" data-value="{{checkKey('client',$data) }}" >
                                                {!! checkKey('client',$data) !!}
                                            </span>
                                        </button>
                                    </a>
                                    <div class="md-form">
                                        {!! Form::select('client_id',$clients, $project->client_id, ['class' =>'mdb-select','placeholder'=>'---Please Select----','id'=>'client_id']) !!}
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="form-row">
                            @if(Auth()->user()->hasPermissionTo('project_manager_edit') || Auth::user()->all_companies == 1 )
                                <div class="col">
                                    <a class="text-success modal_stafflist">
                                        <button type="button" class="btn btn-sm btn-outline-primary waves-effect form-custom-boutton" style="float: left;">
                                            <span class="assign_class label{{getKeyid('manager',$data)}}" data-id="{{getKeyid('manager',$data) }}" data-value="{{checkKey('manager',$data) }}" >
                                                {!! checkKey('manager',$data) !!}
                                            </span>
                                        </button>
                                    </a>
                                    <div class="md-form">
                                        {!! Form::select('manager_id',$managers, $project->manager_id, ['class' =>'mdb-select','placeholder'=>'---Please Select----','id'=>'manager_id']) !!}
                                    </div>
                                </div>
                            @endif
                            @if(Auth()->user()->hasPermissionTo('project_contract_manager_edit') || Auth::user()->all_companies == 1 )
                                <div class="col">
                                    <div class="md-form mt-4-5">
                                        <label class="active" >
                                            <span class="assign_class label{{getKeyid('contract_manager',$data)}}" data-id="{{getKeyid('contract_manager',$data) }}" data-value="{{checkKey('contract_manager',$data) }}" >
                                                {!! checkKey('contract_manager',$data) !!}
                                            </span>
                                        </label>
                                        {!! Form::select('contract_manager_id',$contractManagers, $project->contract_manager_id, ['class' =>'mdb-select','placeholder'=>'---Please Select----','id'=>'contract_manager_id']) !!}
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="form-row">
                            @if(Auth()->user()->hasPermissionTo('project_devision_edit') || Auth::user()->all_companies == 1 )
                                <div class="col">
                                    <a class="text-success modal_division">
                                        <button type="button" class="btn btn-sm btn-outline-primary waves-effect form-custom-boutton" style="float: left;">
                                            <span class="assign_class label{{getKeyid('devision',$data)}}" data-id="{{getKeyid('devision',$data) }}" data-value="{{checkKey('devision',$data) }}" >
                                                {!! checkKey('devision',$data) !!}
                                            </span>
                                        </button>
                                    </a>
                                    <div class="md-form">
                                        {!! Form::select('division_id',$divisions, $project->division_id, ['class' =>'mdb-select division_id','placeholder'=>'---Please Select----','id'=>'division_id']) !!}
                                    </div>
                                </div>
                            @endif
                            @if(Auth()->user()->hasPermissionTo('project_customer_edit') || Auth::user()->all_companies == 1 )
                                <div class="col">
                                    <a class="text-success add-customer-button modal_customer">
                                        <button type="button" class="btn btn-sm btn-outline-primary waves-effect form-custom-boutton add_customer_popup_btn" style="float: left;">
                                            <span class="assign_class label{{getKeyid('customer',$data)}}" data-id="{{getKeyid('customer',$data) }}" data-value="{{checkKey('customer',$data) }}" >
                                                {!! checkKey('customer',$data) !!}
                                            </span>
                                        </button>
                                    </a>
                                    <div class="md-form">
                                        {!! Form::select('customer_id',$customers, $project->customer_id, ['class' =>'mdb-select customer_id','placeholder'=>'---Please Select----','id'=>'customer_id']) !!}
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="form-row">
                            @if(Auth()->user()->hasPermissionTo('project_group_edit') || Auth::user()->all_companies == 1 )
                                <div class="col-md-6">
                                    <a class="text-success modal_group">
                                        <button type="button" class="btn btn-sm btn-outline-primary waves-effect form-custom-boutton" style="float: left;">
                                            <span class="assign_class label{{getKeyid('group',$data)}}" data-id="{{getKeyid('group',$data) }}" data-value="{{checkKey('group',$data) }}" >
                                                {!! checkKey('group',$data) !!}
                                            </span>
                                        </button>
                                    </a>
                                    <div class="md-form" id="site_groups_div">
                                        <select searchable=" "  class="mdb-select" name="site_group_id" id="site_group_id" >
                                            <option value="">---Please Select----</option>
                                            @foreach($groups as $value)
                                                <option value="{{$value->id}}" {{$project->site_group_id == $value->id ? 'selected':'' }}>{{$value->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            @endif
                        </div>
                        @if(Auth()->user()->hasPermissionTo('project_notes_edit') || Auth::user()->all_companies == 1 )
                        <div class="md-form">
                            {!! Form::textArea('notes',$project->notes, ['class' => 'form-control md-textarea']) !!}
                            <label class="active" for="materialContactFormMessage">
                                <span class="assign_class label{{getKeyid('notes',$data)}}" data-id="{{getKeyid('notes',$data) }}" data-value="{{checkKey('notes',$data) }}" >
                                    {!! checkKey('notes',$data) !!}
                                </span>
                            </label>
                        </div>
                        @endif
                        <div class="form-row">
                            @if(Auth()->user()->hasPermissionTo('project_start_date_edit') || Auth::user()->all_companies == 1 )
                                <div class="col">
                                    <div class="md-form">
                                        <div class="md-form">
                                            {!! Form::text('contract_start',Carbon\Carbon::parse($project->contract_start)->format('jS M Y'), ['class' => 'form-control datepicker']) !!}
                                            <label class="active" for="date-picker-example">
                                                <span class="assign_class label{{getKeyid('start_date',$data)}}" data-id="{{getKeyid('start_date',$data) }}" data-value="{{checkKey('start_date',$data) }}" >
                                                    {!! checkKey('start_date',$data) !!}
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            @if(Auth()->user()->hasPermissionTo('project_expiry_date_edit') || Auth::user()->all_companies == 1 )
                                <div class="col">
                                    <div class="md-form">
                                        <div class="md-form">
                                            {!! Form::text('contract_end',Carbon\Carbon::parse($project->contract_end)->format('jS M Y'), ['class' => 'form-control datepicker']) !!}
                                            <label class="active" for="date-picker-example">
                                                <span class="assign_class label{{getKeyid('expiry_date',$data)}}" data-id="{{getKeyid('expiry_date',$data) }}" data-value="{{checkKey('expiry_date',$data) }}" >
                                                    {!! checkKey('expiry_date',$data) !!}
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                        @if(Auth()->user()->hasPermissionTo('project_termination_reason_edit') || Auth::user()->all_companies == 1 )
                        <div class="md-form">
                            {!! Form::textArea('termination_reason',$project->termination_reason, ['class' => 'form-control md-textarea']) !!}
                            <label class="active" for="materialContactFormMessage">
                                <span class="assign_class label{{getKeyid('termination_reason',$data)}}" data-id="{{getKeyid('termination_reason',$data) }}" data-value="{{checkKey('termination_reason',$data) }}" >
                                    {!! checkKey('termination_reason',$data) !!}
                                </span>
                            </label>
                        </div>
                        @endif
                        <button class="form_submit_check btn btn-outline-info btn-rounded btn-block my-4 waves-effect z-depth-0" type="submit">
                            <span class="assign_class label{{getKeyid('update',$data)}}" data-id="{{getKeyid('update',$data) }}" data-value="{{checkKey('update',$data) }}" >
                                {!! checkKey('update',$data) !!}
                            </span>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('backend.label.input_label')
    @component('backend.user.model.manager')
    @endcomponent
    @component('backend.user.model.contrator_manager')
    @endcomponent
    @component('backend.client.model.index')
    @endcomponent
    @component('backend.contractor.model.index')
    @endcomponent
    @component('backend.divisions.model.index')
    @endcomponent
    @component('backend.customers.model.index')
    @endcomponent
    @component('backend.site_groups.model.index')
    @endcomponent
    @component('backend.company.model.index')
    @endcomponent
    <script type="text/javascript">
        $(document).ready(function () {
            $('input[type="search"]').addClass('export_search');
        });
        $('.modal_contrator').click(function(e) {
            if ($('.edit_cms_disable').css('display') == 'none') {
                return 0;
            }
            $('#modalContractIndex').modal('show');
        });
        $('.modal_client').click(function(e) {
            if ($('.edit_cms_disable').css('display') == 'none') {
                return 0;
            }
            $('#modalClientIndex').modal('show');
        });
        $('.modal_stafflist').click(function(e) {
            if ($('.edit_cms_disable').css('display') == 'none') {
                return 0;
            }
            $('#modalmanagerIndex').modal('show');
        });
        $('.modal_division').click(function(e) {
            if ($('.edit_cms_disable').css('display') == 'none') {
                return 0;
            }
            $('#modalDevisionsIndex').modal('show');
        });
        $('.modal_customer').click(function(e) {
            if ($('.edit_cms_disable').css('display') == 'none') {
                return 0;
            }
            $('#modalcustomerIndex').modal('show');
        });
        $('.modal_group').click(function(e) {
            if ($('.edit_cms_disable').css('display') == 'none') {
                return 0;
            }
            $('#site_group_table').DataTable().clear().destroy();
            var url='/sitegroup/getall';
            sitegroup_data(url);
            $('#modalSIteGroupIndex').modal('show');
        });
        $('.datepicker').pickadate({
            selectYears:250,
        });
    </script>
@endsection

