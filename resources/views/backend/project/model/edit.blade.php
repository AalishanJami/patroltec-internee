 @php
        $data=localization();
@endphp
<div class="modal fade" id="modalEditForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold"><span class="assign_class label{{getKeyid('edit_company',$data)}}" data-id="{{getKeyid('edit_company',$data) }}" data-value="{{checkKey('edit_company',$data) }}" >{!! checkKey('edit_company',$data) !!} </span></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="edit_company_name" method="POST">
                @csrf
                <input type="hidden" id="user_id_hidden" >
                <div class="modal-body mx-3">
                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>

                        <input id="edit_user_name" type="text" class="form-control" name="name">
                        <label data-error="wrong" data-success="right" for="name" class="active"><span class="assign_class label{{getKeyid('company_name',$data)}}" data-id="{{getKeyid('company_name',$data) }}" data-value="{{checkKey('company_name',$data) }}" >{!! checkKey('company_name',$data) !!} </span>
                            @php
                                    $id=getKeyid('company.model.edit.name.msg',$data);
                                    $value=checkKey('company.model.edit.name.msg',$data);
                                    $title=$id.'---'.$value;  
                                @endphp
                                <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                                data-content="{{$title}}"></i>
                        </label>


                    </div>

                </div>

                <div class="modal-footer d-flex justify-content-center">
                     <button class="btn btn-primary btn-sm" type="submit"><span class="assign_class label{{getKeyid('update',$data)}}" data-id="{{getKeyid('update',$data) }}" data-value="{{checkKey('update',$data) }}" >{!! checkKey('update',$data) !!} </span></button>
                </div>
            </form>

            <div class="modal-body mx-3">

           
            </div>
        </div>
    </div>
</div>
