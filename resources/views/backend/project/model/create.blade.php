
<style type="text/css">
   .ml-4-5 {
   margin-left: 2.5rem !important;
   }
    .form-custom-boutton{
      margin-left: unset!important;
      padding: 0.3rem!important;
   }
</style>
<div class="modal fade" id="modalCreateProject" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
   aria-hidden="true">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header text-center">
            <h4 class="modal-title w-100 font-weight-bold">Project</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <form id="projectForm" >
            <div class="modal-body">
               <div class="md-form mb-5">
                  <div class="row">
                     <div class="col">
                        <div class="md-form ml-4-5">
                           <label class="active mdb-select-label">Division</label>
                         {!! Form::select('division_id',$divisions,null, ['class' =>'mdb-select','placeholder'=>'---Please Select----','id'=>'division_id']) !!}
                        </div>
                     </div>
                     <div class="col">
                        <div class="md-form mb-5">
                           <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" id="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                           <label data-error="wrong" data-success="right" for="name">Name
                           <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                              data-content="tool tip for Telephone Number"></i>
                           </label>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col">
                        <div class="md-form ml-4-5">
                           <label class="active mdb-select-label">Customer</label>
                         {!! Form::select('customer_id',$customers,null, ['class' =>'mdb-select','placeholder'=>'---Please Select----','id'=>'customer_id']) !!}
                        </div>
                     </div>
                     <div class="col">
                        <div class="md-form ml-4-5">
                        <label class="active mdb-select-label">Site Groups</label>
                         {!! Form::select('site_group_id',$groups,null, ['class' =>'mdb-select','placeholder'=>'---Please Select----','id'=>'site_group_id']) !!}
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col">
                        <div class="md-form ml-4-5">
                      <label class="active mdb-select-label">Manager</label>
            {!! Form::select('manager_id',$managers,1,['class' =>'mdb-select','placeholder'=>'---Please Select----','id'=>'manager_id']) !!}
                        </div>
                     </div>
                     <div class="col">

                        <div class="md-form ml-4-5">

                           <label class="active mdb-select-label">Contract Manager</label>
                         {!! Form::select('contract_manager_id',$contractManagers,1, ['class' =>'mdb-select','placeholder'=>'---Please Select----','id'=>'contract_manager_id']) !!}
                        </div>
                     </div>
                  </div>
                  <div class="row">
                    <div class="col">
                        <div class="md-form ml-4-5">
                           <label class="active mdb-select-label">Client</label>
                         {!! Form::select('client_id',$clients,null, ['class' =>'mdb-select','placeholder'=>'---Please Select----','id'=>'client_id']) !!}
                        </div>
                    </div>
                     <div class="col">
                        <div class="md-form mb-5">
                           <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="post_code" id="post_code" value="{{ old('name') }}" required autocomplete="name" autofocus>
                           <label data-error="wrong" data-success="right" for="name">
                            Post Code
                           <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                              data-content="tool tip for Country"></i>
                           </label>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col">
                        <div class="md-form mb-5">
                           <input type="date" class="form-control validate @error('name') is-invalid @enderror" name="contract_start" id="contract_start" value="{{ old('name') }}" required autocomplete="name" autofocus>
                           <label data-error="wrong" data-success="right" for="name">
                            Start Date
                           <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                              data-content="tool tip for Post Code"></i>
                           </label>
                        </div>
                     </div>
                        <div class="col">
                        <div class="md-form mb-5">
                           <input type="date" class="form-control validate @error('name') is-invalid @enderror" name="contract_end" id="contract_end" value="{{ old('name') }}" required autocomplete="name" autofocus>
                           <label data-error="wrong" data-success="right" for="name">
                           Expiry Date
                           </label>
                        </div>
                     </div>
                  </div>
                      <div class="row justify-content-center">
                      <button class="btn btn-primary btn-sm" type="button" onclick="save()">Save</button>
                  </div>
               </div>
            </div>

         </form>
      </div>
   </div>

</div>
