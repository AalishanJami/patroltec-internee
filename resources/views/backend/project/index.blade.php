@extends('backend.layouts.backend')
@section('title', 'Project')
@section('content')
    @php
        $data=localization();
    @endphp
    <link rel="stylesheet" href="{{ asset('custom/css/custom_style.css') }}">
    {{ Breadcrumbs::render('project') }}
    <div class="card">
        <div class="card-body">
            <div id="table" class="table-editable table-responsive">
                @if(Auth()->user()->hasPermissionTo('create_project') || Auth::user()->all_companies == 1 )
                    <span class="table-add float-right mb-3 mr-2">
                        <a class="text-success" href="javascript:;" onclick="save()">
                            <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
                        </a>
                    </span>
                @endif
                @if(Auth()->user()->hasPermissionTo('selected_delete_project') || Auth::user()->all_companies == 1 )
                    <button id="deleteSelectedProject" class="btn btn-danger btn-sm">
                        <span class="assign_class label{{getKeyid('selected_delete',$data)}}" data-id="{{getKeyid('selected_delete',$data) }}" data-value="{{checkKey('selected_delete',$data) }}" >
                            {!! checkKey('selected_delete',$data) !!}
                        </span>
                    </button>
                @endif
                @if(Auth()->user()->hasPermissionTo('selected_restore_delete_project') || Auth::user()->all_companies == 1 )
                    <button id="restore_button_project" class="btn btn-primary btn-sm">
                        <span class="assign_class label{{getKeyid('restore',$data)}}" data-id="{{getKeyid('restore',$data) }}" data-value="{{checkKey('restore',$data) }}" >
                            {!! checkKey('restore',$data) !!}
                        </span>
                    </button>
                @endif
                @if(Auth()->user()->hasPermissionTo('selected_active_button_project') || Auth::user()->all_companies == 1 )
                    <button  style="display: none;" id="selectedactivebuttonproject" class="btn btn-success btn-sm assign_class label{{getKeyid('selected_active',$data)}}"  data-id="{{getKeyid('selected_active',$data) }}" data-value="{{checkKey('selected_active',$data) }}" >
                        {!! checkKey('selected_active',$data) !!}
                    </button>
                @endif
                @if(Auth()->user()->hasPermissionTo('active_project') || Auth::user()->all_companies == 1 )
                    <button id="show_active_button_project" class="btn btn-primary btn-sm" style="display: none;">
                        <span class="assign_class label{{getKeyid('show_active',$data)}}" data-id="{{getKeyid('show_active',$data) }}" data-value="{{checkKey('show_active',$data) }}" >
                            {!! checkKey('show_active',$data) !!}
                        </span>
                    </button>
                @endif
                @if(Auth()->user()->hasPermissionTo('csv_project') || Auth::user()->all_companies == 1 )
                    <form class="form-style" id="export_excel_project" method="POST" action="{{ url('/project/exportExcel') }}">
                        {!! csrf_field() !!}
                        <input type="hidden" class="project_export" name="excel_array" value="1">
                        <button  type="submit" class="form_submit_check btn btn-warning btn-sm">
                            <span class="assign_class label{{getKeyid('excel_export',$data)}}" data-id="{{getKeyid('excel_export',$data) }}" data-value="{{checkKey('excel_export',$data) }}" >
                                {!! checkKey('excel_export',$data) !!}
                            </span>
                        </button>
                    </form>
                @endif
                @if(Auth()->user()->hasPermissionTo('word_project') || Auth::user()->all_companies == 1 )
                    <form class="form-style" method="POST" id="export_world_project" action="{{ url('project/exportWorld') }}">
                        {!! csrf_field() !!}
                        <input type="hidden" class="project_export" name="word_array" value="1">
                        <button  type="submit" class="form_submit_check btn btn-success btn-sm">
                            <span class="assign_class label{{getKeyid('word_export',$data)}}" data-id="{{getKeyid('word_export',$data) }}" data-value="{{checkKey('word_export',$data) }}" >
                                {!! checkKey('word_export',$data) !!}
                            </span>
                        </button>
                    </form>
                @endif
                @if(Auth()->user()->hasPermissionTo('pdf_project') || Auth::user()->all_companies == 1 )
                    <form  class="form-style" {{pdf_view("locations")}} method="POST" id="export_pdf_project" action="{{ url('project/exportPdf') }}">
                        {!! csrf_field() !!}
                        <input type="hidden" class="project_export" name="pdf_array" value="1">
                        <button  type="submit" class="form_submit_check btn btn-secondary btn-sm">
                        <span class="assign_class label{{getKeyid('pdf_export',$data)}}" data-id="{{getKeyid('pdf_export',$data) }}" data-value="{{checkKey('pdf_export',$data) }}" >
                            {!! checkKey('pdf_export',$data) !!}
                        </span>
                        </button>
                    </form>
                @endif
                @include('backend.label.input_label')
                <input type="hidden" name="flag" id="flagProject" value="1">
                <table  id="project_table" class=" table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="no-sort all_checkboxes_style align-top">
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input project_checked" id="project_checkbox_all">
                                <label class="form-check-label" for="project_checkbox_all">
                                    <span class="assign_class label{{getKeyid('all',$data)}}" data-id="{{getKeyid('all',$data) }}" data-value="{{checkKey('all',$data) }}" >
                                        {!! checkKey('all',$data) !!}
                                    </span>
                                </label>
                            </div>
                        </th>
                        <th class="align-top">
                            <span class="assign_class label{{getKeyid('name',$data)}}" data-id="{{getKeyid('name',$data) }}" data-value="{{checkKey('name',$data) }}" >
                                {!! checkKey('name',$data) !!}
                            </span>
                        </th>
                        <th class="align-top">
                            <span class="assign_class label{{getKeyid('devision',$data)}}" data-id="{{getKeyid('devision',$data) }}" data-value="{{checkKey('devision',$data) }}" >
                                {!! checkKey('devision',$data) !!}
                            </span>
                        </th>
                        <th class="align-top">
                            <span class="assign_class label{{getKeyid('customer',$data)}}" data-id="{{getKeyid('customer',$data) }}" data-value="{{checkKey('customer',$data) }}" >
                                {!! checkKey('customer',$data) !!}
                            </span>
                        </th>
                        <th class="align-top">
                            <span class="assign_class label{{getKeyid('group',$data)}}" data-id="{{getKeyid('group',$data) }}" data-value="{{checkKey('group',$data) }}" >
                                {!! checkKey('group',$data) !!}
                            </span>
                        </th>
                        <th class="align-top">
                            <span class="assign_class label{{getKeyid('manager',$data)}}" data-id="{{getKeyid('manager',$data) }}" data-value="{{checkKey('manager',$data) }}" >
                                {!! checkKey('manager',$data) !!}
                            </span>
                        </th>
                        <th class="align-top">
                            <span class="assign_class label{{getKeyid('contract_manager',$data)}}" data-id="{{getKeyid('contract_manager',$data) }}" data-value="{{checkKey('contract_manager',$data) }}" >
                                {!! checkKey('contract_manager',$data) !!}
                            </span>
                        </th>
                        <th class="align-top">
                            <span class="assign_class label{{getKeyid('client',$data)}}" data-id="{{getKeyid('client',$data) }}" data-value="{{checkKey('client',$data) }}" >
                                {!! checkKey('client',$data) !!}
                            </span>
                        </th>
                        <th class="align-top">
                            <span class="assign_class label{{getKeyid('start_date',$data)}}" data-id="{{getKeyid('start_date',$data) }}" data-value="{{checkKey('start_date',$data) }}" >
                                {!! checkKey('start_date',$data) !!}
                            </span>
                        </th>
                        <th class="align-top">
                            <span class="assign_class label{{getKeyid('expiry_date',$data)}}" data-id="{{getKeyid('expiry_date',$data) }}" data-value="{{checkKey('expiry_date',$data) }}" >
                                {!! checkKey('expiry_date',$data) !!}
                            </span>
                        </th>
                        <th id="action" class="no-sort all_action_btn"></th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    @if(Auth()->user()->all_companies == 1)
        <input type="hidden" id="checkbox_permission" value="1">
        <input type="hidden" id="name_permission" value="1">
        <input type="hidden" id="devision_permission" value="1">
        <input type="hidden" id="customer_permission" value="1">
        <input type="hidden" id="group_permission" value="1">
        <input type="hidden" id="manager_permission" value="1">
        <input type="hidden" id="contract_manager_permission" value="1">
        <input type="hidden" id="client_permision" value="1">
        <input type="hidden" id="start_date_permision" value="1">
        <input type="hidden" id="expiry_date_permision" value="1">
    @else
        <input type="hidden" id="checkbox_permission" value="{{Auth()->user()->hasPermissionTo('project_checkbox')}}">
        <input type="hidden" id="name_permission" value="{{Auth()->user()->hasPermissionTo('project_name')}}">
        <input type="hidden" id="devision_permission" value="{{Auth()->user()->hasPermissionTo('project_devision')}}">
        <input type="hidden" id="customer_permission" value="{{Auth()->user()->hasPermissionTo('project_customer')}}">
        <input type="hidden" id="group_permission" value="{{Auth()->user()->hasPermissionTo('project_group')}}">
        <input type="hidden" id="manager_permission" value="{{Auth()->user()->hasPermissionTo('project_manager')}}">
        <input type="hidden" id="contract_manager_permission" value="{{Auth()->user()->hasPermissionTo('project_contract_manager')}}">
        <input type="hidden" id="client_permision" value="{{Auth()->user()->hasPermissionTo('project_client')}}">
        <input type="hidden" id="start_date_permision" value="{{Auth()->user()->hasPermissionTo('project_start_date')}}">
        <input type="hidden" id="expiry_date_permision" value="{{Auth()->user()->hasPermissionTo('project_expiry_date')}}">
    @endif

    <input type="hidden" class="get_current_path" value="{{Request::path()}}">
    @include('backend.project.model.create')
    <script type="text/javascript" src="{{ asset('custom/js/project_main.js') }}" ></script>
    <script type="text/javascript">

        $(document).ready(function() {
            var current_path=$('.get_current_path').val();
            localStorage.setItem('current_path',current_path);
            $(function () {
                var url='/project/getAll';
                projectAppend(url);
            });
            (function ($) {
                var active ='<span class="assign_class label'+$('#breadcrumb_project').attr('data-id')+'" data-id="'+$('#breadcrumb_project').attr('data-id')+'" data-value="'+$('#breadcrumb_project').val()+'" >'+$('#breadcrumb_project').val()+'</span>';
                $('.breadcrumb-item.active').html(active);
                var parent ='<span class="assign_class label'+$('#dashboard_label').attr('data-id')+'" data-id="'+$('#dashboard_label').attr('data-id')+'" data-value="'+$('#dashboard_label').val()+'" >'+$('#dashboard_label').val()+'</span>';
                $('.breadcrumb-item a').html(parent);
            }(jQuery));

        });
        function projectAppend(url)
        {
            var company_id=$('.companies_selected').children("option:selected").val();
            var table = $('#project_table').dataTable({
                "columnDefs": [
                    { "orderable": false, "targets": [0,10]},
                ],
                processing: true,
                bInfo:false,
                language: {
                    'lengthMenu': '<span class="assign_class label'+$('#show_label').attr('data-id')+'" data-id="'+$('#show_label').attr('data-id')+'" data-value="'+$('#show_label').val()+'" ></span>  _MENU_ <span class="assign_class label'+$('#entries_label').attr('data-id')+'" data-id="'+$('#entries_label').attr('data-id')+'" data-value="'+$('#entries_label').val()+'" ></span>',
                    'search':'<span class="assign_class label'+$('#search_id').attr('data-id')+'" data-id="'+$('#search_id').attr('data-id')+'" data-value="'+$('#search_id').val()+'" >'+$('#search_id').val()+'</span>',
                    'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                    'info': '<span class="assign_class label'+$('#showing_page_label').attr('data-id')+'" data-id="'+$('#showing_page_label').attr('data-id')+'" data-value="'+$('#showing_page_label').val()+'" >'+$('#showing_page_label').val()+'</span> _PAGE_ <span class="assign_class label'+$('#of_label').attr('data-id')+'" data-id="'+$('#of_label').attr('data-id')+'" data-value="'+$('#of_label').val()+'" >'+$('#of_label').val()+'</span> _PAGES_',
                    'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                    'paginate': {
                        'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                        'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                    },
                    'infoFiltered': " "
                },
                "ajax": {
                    "url": url,
                    "type": 'get',
                    "data": {'id':company_id}
                },
                "createdRow": function( row, data, dataIndex,columns )
                {
                    var checkbox='<div class="form-check"><input type="checkbox" class="form-check-input selectedrowproject project_checked" id="project'+data.id+'"><label class="form-check-label" for="project'+data.id+'"></label></div>';
                    $(columns[0]).html(checkbox);
                    $(row).attr('id', 'project_tr'+data['id']);
                    $(row).attr('data-id',data['id']);
                    // $(row).attr('class', 'selectedrowproject');
                    var current = new Date();
                    var check = new Date(data['contract_end']);
                    var one_day=1000*60*60*24;
                    var diff =check.getTime() - current.getTime();
                    var days = diff/one_day;
                    if(days>=7)
                    {
                        $(row).attr('style',"background-color: #00c851");
                        var contract_end='<input type="hidden" class="form-check-input contract_end'+data.id+'" value="#00c851">';
                    }
                    else if(days>=0 && days<=7)
                    {
                        $(row).attr('style',"background-color: #ffb347");
                        var contract_end='<input type="hidden" class="form-check-input contract_end'+data.id+'" value="#ffb347">';
                    }
                    else if(days<=0)
                    {
                        $(row).attr('style',"background-color: #fc685f");
                        var contract_end='<input type="hidden" class="form-check-input contract_end'+data.id+'" value="#fc685f">';
                    }
                    $(columns[9]).append(contract_end);
                },
                columns: [
                    {data: 'checkbox', name: 'checkbox',visible:$('#checkbox_permission').val()},
                    {data: 'name', name: 'name',visible:$('#name_permission').val()},
                    {data: 'divisions', name: 'divisions',visible:$('#devision_permission').val()},
                    {data: 'customer_id', name: 'customer_id',visible:$('#customer_permission').val()},
                    {data: 'site_group_id', name: 'site_group_id',visible:$('#group_permission').val()},
                    {data: 'manager_id', name: 'manager_id',visible:$('#manager_permission').val()},
                    {data: 'contract_manager_id', name: 'contract_manager_id',visible:$('#contract_manager_permission').val()},
                    {data: 'client_id', name: 'client_id',visible:$('#client_permision').val()},
                    {data: 'contract_start', name: 'contract_start',visible:$('#start_date_permision').val()},
                    {data: 'contract_end', name: 'contract_end',visible:$('#expiry_date_permision').val()},
                    {data: 'actions', name: 'actions'},
                ],
            });
            if ($(":checkbox").prop('checked',true)){
                $(":checkbox").prop('checked',false);
            }
        }
    </script>
@endsection
