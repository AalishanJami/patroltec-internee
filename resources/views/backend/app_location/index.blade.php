@extends('backend.layouts.detail')
@section('content')
    @php
       $data=localization();
    @endphp
            {{ Breadcrumbs::render('app/location') }}
            <!-- <span class="table-add float-right mb-3 mr-2">
                <a class="text-success"  data-toggle="modal" data-target="#modalRegisterCompany">
                    <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
                </a>
            </span> -->


            <div class="card">

            <div class="card-body">
                <div id="table" class="table-editable table-responsive">

            <button class="btn btn-danger btn-sm">
                Delete Selected App Location
            </button>
            <button class="btn btn-primary btn-sm">
              Restore Deleted App Location
            </button>

            <button  style="display: none;">
                Show Active App Location
            </button>
            <button class="btn btn-warning btn-sm">
               Excel Export
            </button>
            <button class="btn btn-success btn-sm">
                Word Export
            </button>
            <table class="company_table_static table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th> User Name
                    </th>
                    <th> App Version Number
                    </th>
                    <th> Logitude
                    </th>
                    <th> Latitude
                    </th>
                    <th> Date Time
                    </th>
                </tr>
                </thead>
                    <tr>
                        <td  class="pt-3-half">Alandale</td>
                        <td  class="pt-3-half">v 4.4</td>
                        <td  class="pt-3-half">888.444</td>
                        <td class="pt-3-half">111.22</td>
                        <td class="pt-3-half">20 marach 2020 : 4:00 pm</td>

                       <!--  <td>
                            <a  data-toggle="modal" data-target="#modalEditForm" class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i></a>
                            <button type="button"class="btn btn-danger btn-sm my-0" onclick="deleteAddressdata()"><i class="fas fa-trash"></i></button>

                        </td> -->
                    </tr>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
            <!--  {{--    Edit Modal--}}
            @component('backend.address.model.edit')
            @endcomponent
            {{--    END Edit Modal--}}

            {{--    Register Modal--}}
            @component('backend.address.model.create')
            @endcomponent
            {{--end modal register--}} -->
        </div>
    </div>

@endsection
<script type="text/javascript">
function deleteAddressdata()
{

    swal({
        title:'Are you sure?',
        text: "Delete Record.",
        type: "error",
        showCloseButton: true,
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, Delete!",
        cancelButtonText: "Cancel!",
        closeOnConfirm: false,
        customClass: "confirm_class",
        closeOnCancel: false,
    },
    function(isConfirm){
        swal.close();
 });


}

</script>
