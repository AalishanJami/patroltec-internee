 @php
        $data=localization();
@endphp
<style type="text/css">
   .form-check-input {
    position: absolute !important;
    pointer-events: none !important;
    opacity: 0!important;
}
 @media (min-width: 576px) {
      .modal-dialog {
        max-width: 75% !important;
      }
    }
</style>
<div class="modal fade" id="modalManagerIndex" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold"><span > Manager </span></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <div class="col-md-12">
            <span class="table-add float-right mb-3 mr-2">
            <a class="text-success"  data-toggle="modal" data-target="#managerCreate">
                <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
            </a>
            </span>
            <button class="btn btn-danger btn-sm">
                Delete Selected Manager User
            </button>
            <button class="btn btn-primary btn-sm">
              Restore Deleted Manager User
            </button>

            <button  style="display: none;">
                Show Active Manager User
            </button>
            <button class="btn btn-warning btn-sm">
               Excel Export
            </button>
            <button class="btn btn-success btn-sm">
                Word Export
            </button>

        </div>

   <table id="company_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th> ID
            </th>
            <th> Name
            </th>
            <th> Email
            </th>
            <th id="action">
            </th>
        </tr>
        </thead>
            <tr>
                <td>1</td>
                <td>Admin</td>
                <td>admin@gmail.com</td>
                <td>
                    <a  data-toggle="modal" data-target="#modalEditEmployee" class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i></a>
                    <button type="button"class="btn btn-danger btn-sm my-0" onclick="deleteEmployeedata()"><i class="fas fa-trash"></i></button>

                </td>
            </tr>
        <tbody>
        </tbody>
    </table>
</div>
    </div>
</div>

    @component('backend.manager.model.create')
    @endcomponent

     {{--    Edit Modal--}}
    @component('backend.manager.model.edit')
    @endcomponent
    {{--    END Edit Modal--}}
        <script type="text/javascript">
            function deleteEmployeedata()
            {

                swal({
                    title:'Are you sure?',
                    text: "Delete Record.",
                    type: "error",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, Delete!",
                    cancelButtonText: "Cancel!",
                    closeOnConfirm: false,
                    customClass: "confirm_class",
                    closeOnCancel: false,
                },
                function(isConfirm){
                    swal.close();
             });


            }

        </script>
