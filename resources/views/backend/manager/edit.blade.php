<div class="col-md-9 col-sm-9 col-xs-12">
  <input type="hidden" name="user_id" value="{{$user->id}}">
    @foreach ($roles as $key=> $role)
    <div class="checkbox">
       <label>
            @if($user->roles->count()>0)
             @foreach ($user->roles as $data)
               @if($data->id == $role->id)
                  <input type="checkbox" name="roles[]" checked="checked" value="{{$role->id}}">
               @else
                 <input type="checkbox" name="roles[]"  value="{{$role->id}}">
               @endif
             @endforeach
             @else
              <input type="checkbox" name="roles[]" value="{{$role->id}}">
            @endif
              
            {{ Form::label($role->name, ucfirst($role->name), array('class' => 'flat' )) }}

       </label>
    </div>
    @endforeach
 </div>