@extends('backend.layouts.backend')
@section('title', 'Permissions')
@section('content')
@php
    $data=localization();
@endphp
<style type="text/css">

  .list-group > .list-group {
  display: none;
  margin-bottom: 0;
}
.list-group-item:focus-within + .list-group {
  display: block;
}

.list-group > .list-group-item {
  border-radius: 0;
  border-width: 1px 0 0 0;
}

.list-group > .list-group-item:first-child {
  border-top-width: 0;
}

.list-group  > .list-group > .list-group-item {
  padding-left: 2.5rem;
}
</style>
 {{Breadcrumbs::render('Permissions')}}
<div class="card">
  <div class="card-body">
    <div class="container body">
      <div class="main_container">
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>
                  <span class="assign_class label{{getKeyid('permissions',$data)}}" data-id="{{getKeyid('permissions',$data) }}" data-value="{{checkKey('permissions',$data) }}" >
                    {{checkKey('permissions',$data) }}
                  </span>
                </h3>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="x_panel">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <div class="clearfix"></div>
                  </div>
                  <div class="row ">
                    <div class=" table-responsive">
                      @include('errors.errorDisplay')
                      <div class="accordion md-accordion accordion-1" id="accordionEx23" role="tablist">
                        <div class="card">
                          @foreach ($permissions as $key=> $permission)
                            <div class="card-header blue lighten-3 z-depth-1" role="tab" id="heading{{str_slug($key , '-')}}" >
                              <h5 class="text-uppercase mb-0 py-1">
                                <a class="white-text " data-toggle="collapse" href="#collapse{{str_slug($key , '-')}}" aria-expanded="true"
                                  aria-controls="collapse{{str_slug($key , '-')}}">
                                  {{$key}} 
                                </a>
                              </h5>
                            </div>
                            <div id="collapse{{str_slug($key , '-')}}" class="collapse " role="tabpanel" aria-labelledby="heading{{str_slug($key , '-')}}"
                              data-parent="#accordionEx23">
                              <div class="card-body">
                                <div class="row my-4">
                                  <div class="col-md-8">
                                    <ul>
                                      @foreach ($permission as $value)
                                      <li>{{ $value->permission_name }}</li>
                                      @endforeach
                                    </ul>
                                  </div>
                                </div>
                              </div>
                            </div>
                          @endforeach
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
  </div>
</div>
  <input type="hidden" class="get_current_path" value="{{Request::path()}}">
    <script type="text/javascript">
        $(document).ready(function() {
            var current_path=$('.get_current_path').val();
            localStorage.setItem('current_path',current_path);
        });
    </script>
@endsection
