@extends('layouts.app')

@section('title', '| Edit Permission')

@section('content')
    <section class="content-header">
        <h1>
            Permission Edit
        </h1>
    </section>
    <div class="content">
        @include('errors.errorDisplay')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class='col-lg-4 col-lg-offset-4'>

                        <h3><i class='fa fa-key'></i> Edit {{$permission->name}}</h3>
                        <br>
                         @include('errors.errorDisplay')
                        {{ Form::model($permission, array('url' => array('permissions/update', $permission->id), 'method' => 'POST')) }}{{-- Form model binding to automatically populate our fields with permission data --}}

                        <div class="form-group">
                            {{ Form::label('name', 'Permission Name') }}
                            {{ Form::text('name', null, array('class' => 'form-control')) }}
                        </div>
                        <br>
                        {{ Form::submit('Edit', array('class' => 'btn btn-primary')) }}

                        {{ Form::close() }}

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection