@extends('backend.layouts.user')
@section('content')
    @php
       $data=localization();
    @endphp
@include('backend.layouts.user_sidebar')
<div class="padding-left-custom remove-padding">
    {{ Breadcrumbs::render('relationship') }}
    <div class="locations-form container-fluid form_body">
        <div class=" text-right mb-3 mr-2">
          <a  href="{{url('/relationship')}}"  class="btn btn-outline-success">Relationship</a>
        </div>
        <div class="card">   
          <div class="card-body">
            <div id="table_relationship" class="table-editable">
                <button class="btn btn-danger btn-sm" id="deleteselectedrelationship">
                    Delete Selected Relationship
                </button>
                <button class="btn btn-danger btn-sm" style="display: none;" id="deleteselectedrelationshipSoft">
                    Delete Selected Relationship
                </button>
                <button class="btn btn-success btn-sm" style="display: none;" id="selectedactivebuttonrelationship">
                    Active Selected Relationship
                </button>
                <button class="btn btn-primary btn-sm" id="restorebuttonrelationship">
                  Restore Deleted Relationship
                </button>
                <button  style="display: none;" class="btn btn-primary btn-sm" id="activebuttonrelationship">
                    Show Active Relationship
                </button>
                <form class="form-style" method="POST" id="export_excel_relationship" action="{{ url('relationship/export/excel') }}">
                    <input type="hidden"  name="export_excel_relationship" value="1">
                    <input type="hidden" class="relationship_export" name="excel_array" value="1">
                    {!! csrf_field() !!}
                    <button  type="submit" id="export_excel_relationship_btn" class="btn btn-warning btn-sm">
                        Excel Export
                    </button>
                </form>
                <form class="form-style" method="POST" id="export_world_relationship" action="{{ url('relationship/export/world') }}">
                    <input type="hidden"  name="export_world_relationship" value="1">
                    <input type="hidden" class="relationship_export" name="word_array" value="1">
                    {!! csrf_field() !!}
                    <button  type="submit" id="export_world_relationship_btn" class="btn btn-success btn-sm">
                       Word Export
                    </button>
                </form>
                <form  class="form-style" id="export_pdf_relationship" method="POST" action="{{ url('relationship/export/pdf') }}">
                    <input type="hidden" name="export_world_pdf" value="1">
                    <input type="hidden" class="relationship_export" name="pdf_array" value="1">
                    {!! csrf_field() !!}
                    <button  type="submit" id="export_pdf_relationship_btn"  class="btn btn-secondary btn-sm">
                        Pdf Export
                    </button>
                </form>
                <span class="add_relation float-right mb-3 mr-2">
                  <a class="text-success">
                    <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
                  </a>
                </span>
              <table id="relationship_table" class="table table-bordered table-responsive-md table-striped text-center">
                <thead>
                  <tr>
                    <th class="text-center"></th>
                    <th class="text-center"></th>
                    <th class="text-center">Relationship</th>
                    <th class="text-center"></th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
<!-- Editable table -->
         {{--    Edit Modal--}}
        @component('backend.relationship.model.relationship')
        @endcomponent
        {{--    END Edit Modal--}}

        
        {{--end modal register--}}
    </div>
</div>
<script type="text/javascript" src="{{ asset('custom/js/relationship.js') }}" ></script>
<script type="text/javascript">      
  function relationship_data(url) {
    $.ajax({
        type: 'GET',
        url: url,
        success: function(response)
        {
          var table = $('#relationship_table').dataTable(
          {
            processing: true,
            language: {
            'lengthMenu': '_MENU_',
            'info': ' ',
            },
            "ajax": {
                "url": url,
                "type": 'get',
            },
            "createdRow": function( row, data,dataIndex,columns )
            {
                var checkbox_permission=$('#checkbox_permission').val();
                var checkbox='';
                checkbox+='<div class="form-check"><input type="checkbox" class="form-check-input" id="relationship'+data.id+'"><label class="form-check-label" for="relationship'+data.id+'""></label></div>';
                var submit='';
                submit+='<a type="button" class="btn btn-primary btn-xs my-0 waves-effect waves-light saverelationshipdata " id="'+data.id+'"><i class="fas fa-check"></i></a>';
              
                $(columns[0]).html(checkbox);
                $(columns[2]).attr('id', 'name'+data['id']);
                $(columns[2]).attr('Contenteditable', 'true');
                $(columns[2]).attr('class','edit_inline_relationship');
                $(columns[3]).html(submit);
                $(row).attr('id', 'relationship_tr'+data['id']);
                $(row).attr('class', 'selectedrowrelationship');
                var temp=data['id'];
            },
            columns:
            [
              {data: 'checkbox', name: 'checkbox',},
              {data: 'actions', name: 'actions'},
              {data: 'name', name: 'name',},
              {data: 'submit', name: 'submit',},
            ],

          }
          );
        },
        error: function (error) {
          console.log(error);
        }
    });
  }
  var url='/relationship/getall';
  relationship_data(url);
</script>

@endsection