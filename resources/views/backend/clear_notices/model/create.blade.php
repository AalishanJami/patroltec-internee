@php
        $data=localization();
@endphp
<div class="modal fade" id="modalRegisterNotices" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Clear Up Notice</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form >
                <div class="modal-body mx-3"> 
                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                        <label data-error="wrong" data-success="right" for="name">
                            Location Name
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                            data-content="tool tip for Name"></i>
                        </label>
                    </div>
                   <div class="md-form mb-4">
                        <div class="row">
                            <div class="col-md-12">
                                <select searchable="Search here.."  id="companies_selected" class="mdb-select md-form"   >
                                    <option  disabled selected>Form Type</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option> 
                                </select>
                                <label data-error="wrong" data-success="right" for="name"  class="active">
                            Form Type
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                            data-content="tool tip for Address"></i>
                        </label>
                            </div>
                        </div>
                    </div> 
                      <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input type="date" class="form-control validate @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                        <label data-error="wrong" data-success="right" for="name">
                            Date
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                            data-content="tool tip for Name"></i>
                        </label>
                    </div>
                        <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input type="time" class="form-control validate @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                        <label data-error="wrong" data-success="right" for="name">
                            Time
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                            data-content="tool tip for Name"></i>
                        </label>
                    </div>
                    <div class="md-form mb-4">
                        <div class="row">
                            <div class="col-md-12">
                                <select searchable="Search here.."  id="companies_selected" class="mdb-select md-form"   >
                                    <option  disabled selected>Users</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option> 
                                </select>
                                <label data-error="wrong" data-success="right" for="name"  class="active">
                            Users
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                            data-content="tool tip for Address"></i>
                        </label>
                            </div>
                        </div>
                    </div> 
                        <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <TEXTAREA class="form-control validate @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus></TEXTAREA>
                        
                        <label data-error="wrong" data-success="right" for="name">
                            Instructions
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                            data-content="tool tip for Name"></i>
                        </label>
                    </div>
                       <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input type="number" class="form-control validate @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                        <label data-error="wrong" data-success="right" for="name">
                            Billing Total Cost
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                            data-content="tool tip for Name"></i>
                        </label>
                    </div>
                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input type="text" class="form-control validate @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                        <label data-error="wrong" data-success="right" for="name">
                            Billing Info
                            <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" data-placement="right"
                            data-content="tool tip for Name"></i>
                        </label>
                    </div>
                   
                     <div class="file-upload-wrapper">
                        <input type="file" id="input-file-now2" class="file-upload" />
                    </div>
                </div>


                <div class="modal-footer d-flex justify-content-center">
                    <button class="btn btn-primary btn-sm" type="submit"><span class="assign_class label{{getKeyid('save',$data)}}" data-id="{{getKeyid('save',$data) }}" data-value="{{checkKey('save',$data) }}" >{!! checkKey('save',$data) !!} </span></button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
  
    $('#input-file-now2').file_upload();

</script>
