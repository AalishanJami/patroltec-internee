@extends('backend.layouts.backend')
@section('content')

@php
$data=localization();
@endphp
{{ Breadcrumbs::render('clear_notices') }}

 <div class="card">
          
            <div class="card-body">
                <div id="table" class="table-editable table-responsive" >

                    <span class="table-add float-right mb-3 mr-2">
                        <a class="text-success"  data-toggle="modal" data-target="#modalJob">
                            <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
                        </a>
                    </span>
                    <button class="btn btn-danger btn-sm">
                        Delete Selected Job
                    </button>
                    <button class="btn btn-primary btn-sm">
                      Restore Deleted Job
                    </button>
                   
                    <button  style="display: none;">
                        Show Active Job
                    </button>
                    <button class="btn btn-warning btn-sm">
                       Excel Export
                    </button>
                    <button class="btn btn-success btn-sm">
                        Word Export
                    </button>
                    <table class="company_table_static table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th id="action">
                            </th>
                            <th> Title
                            </th>
                            <th> Employee Name
                            </th>
                            <th> Project Name
                            </th>
                            <th> Date and Time
                            </th>
                            <th> Form Name
                            </th>
                            
                        </tr>
                        </thead>
                            <tr>
                                 <td>
                                    <a href="{{url('job/complete')}}" class="btn btn-primary btn-xs"><i class="far fa-eye"></i></a>
                                  <!--   <a  data-toggle="modal" data-target="#modalEditJob" class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i></a>
                                    <button type="button"class="btn btn-danger btn-sm my-0" onclick="deleteJobdata()"><i class="fas fa-trash"></i></button> -->

                                </td>
                                <td class="pt-3-half" contenteditable="true" >Job Title</td>
                                <td class="pt-3-half" contenteditable="true">Alandale Demo</td>
                                <td class="pt-3-half" contenteditable="true">Lillie Square (Block C1 (CP 07)) </td>
                                <td class="pt-3-half" contenteditable="true">29 Feb 2020 7:00 pm</td>
                                <td class="pt-3-half" contenteditable="true">Welfare Checklist</td>
                               
                            </tr>
                        <tbody>
                        </tbody>
                    </table>
                     {{--    Edit Modal--}}
                    @component('backend.job.model.edit')
                    @endcomponent
                    {{--    END Edit Modal--}}

                    {{--    Register Modal--}}
                    @component('backend.job.model.create')
                    @endcomponent
                    {{--end modal register--}}
                </div>
            </div>
        </div>
@component('backend.clear_notices.model.create')
@endcomponent

@component('backend.clear_notices.model.edit')
@endcomponent





<script type="text/javascript">
      window.onload = function() {
    document.getElementById("action").classList.remove('sorting_asc');
};
    function deleteempooyeedata()
    {

        swal({
            title:'Are you sure?',
            text: "Delete Record.",
            type: "error",
            showCloseButton: true,
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Delete!",
            cancelButtonText: "Cancel!",
            closeOnConfirm: false,
            customClass: "confirm_class",
            closeOnCancel: false,
        },
        function(isConfirm){
            swal.close();
        });


    }



</script>
@endsection
