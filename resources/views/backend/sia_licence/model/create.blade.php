@php $data=localization(); @endphp
<div class="modal fade" id="modalRegisterLicence" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">SIA LICENCE</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="sia_licence_form">
                <div class="modal-body">
                    <div class="card-body px-lg-5 pt-0">
                        <div class="form-row">
                            <div class="col">
                                @if(Auth()->user()->hasPermissionTo('licence_firstname_create') || Auth::user()->all_companies == 1 )
                                <div class="md-form">
                                    <i class="fas fa-user prefix grey-text"></i>
                                    <input type="text" class="form-control validate @error('first_name') is-invalid @enderror" name="first_name" value="{{ old('first_name') }}" required autocomplete="first_name" autofocus>
                                    <label data-error="wrong" data-success="right" for="first_name">
                                        First Name
                                        <i class="fa fa-question-circle" aria-hidden="true" data-toggle="popover" data-placement="right" data-content="tool tip for Directions"></i>
                                    </label>
                                </div>
                                @endif
                            </div>
                            <div class="col">
                                @if(Auth()->user()->hasPermissionTo('licence_surname_create') || Auth::user()->all_companies == 1 )
                                <div class="md-form">
                                    <i class="fas fa-user prefix grey-text"></i>
                                    <input type="text" class="form-control validate @error('surname') is-invalid @enderror" name="surname" value="{{ old('surname') }}" required autocomplete="surname" autofocus>
                                    <label data-error="wrong" data-success="right" for="surname">
                                        Sur Name
                                        <i class="fa fa-question-circle" aria-hidden="true" data-toggle="popover" data-placement="right" data-content="tool tip for Directions"></i>
                                    </label>
                                </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                @if(Auth()->user()->hasPermissionTo('licence_status_create') || Auth::user()->all_companies == 1 )
                                <div class="md-form">
                                    <div class="row">
                                        <div class="col">
                                            <i class="fas fa-thermometer prefix grey-text"></i>
                                            <label data-error="wrong" data-success="right" for="name">
                                                Status
                                            </label>
                                        </div>
                                    </div>
                                    <div class="md-form tex-left">
                                        <div class="row">
                                            <div class="form-check col-md-3">
                                                <input type="radio" class="form-check-input" id="status_active" value="1" name="status">
                                                <label class="form-check-label" for="status_active">Active</label>
                                            </div>
                                            <div class="form-check col-md-3">
                                                <input type="radio" class="form-check-input" id="status_deactive" value="0" name="status" checked>
                                                <label class="form-check-label" for="status_deactive">Deactive</label>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col">
                                @if(Auth()->user()->hasPermissionTo('licence_licence_create') || Auth::user()->all_companies == 1 )
                                <div class="md-form">
                                    <i class="fas fa-id-card prefix grey-text"></i>
                                    <input type="text" class="form-control validate @error('licence') is-invalid @enderror" name="licence" value="{{ old('licence') }}" required autocomplete="licence" autofocus>
                                    <label data-error="wrong" data-success="right" for="licence">
                                        Licence
                                        <i class="fa fa-question-circle" aria-hidden="true" data-toggle="popover" data-placement="right" data-content="tool tip for Directions"></i>
                                    </label>
                                </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                @if(Auth()->user()->hasPermissionTo('licence_end_date_create') || Auth::user()->all_companies == 1 )
                                <div class="md-form">
                                    <i class="fas fa-calendar prefix grey-text"></i>
                                     {!! Form::text('expiry',Carbon\Carbon::parse()->format('jS M Y'), ['class' => 'form-control datepicker']) !!}
                                   <label class="active" for="date-picker-example">End Date</label>
                                </div>
                                @endif
                            </div>
                            <div class="col">
                                @if(Auth()->user()->hasPermissionTo('licence_role_create') || Auth::user()->all_companies == 1 )
                                <div class="row">
                                    <div class="col-0 ml-3">
                                        <i class="fas fa-user prefix grey-text fa-2x" style="margin-top:32px;font-size: 28px;"></i>
                                    </div>
                                    <div class="col">
                                        <div class="md-form">
                                            <label data-error="wrong" data-success="right" for="role" class="active">Role</label>
                                            <input type="text" class="form-control validate @error('role') is-invalid @enderror" name="role" value="{{ old('role') }}" required autocomplete="role" autofocus>
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                @if(Auth()->user()->hasPermissionTo('licence_notes_create') || Auth::user()->all_companies == 1 )
                                <div class="md-form">
                                    <i class="fas fa-sticky-note grey-text prefix"></i>
                                    <input type="text" class="form-control validate @error('notes') is-invalid @enderror" name="notes" value="{{ old('notes') }}" required autocomplete="notes" autofocus>
                                    <label data-error="wrong" data-success="right" for="notes">
                                        Notes
                                        <i class="fa fa-question-circle" aria-hidden="true" data-toggle="popover" data-placement="right" data-content="tool tip for Directions"></i>
                                    </label>
                                </div>
                                @endif
                            </div>

                            <div class="col">
                                @if(Auth()->user()->hasPermissionTo('licence_licence_no_create') || Auth::user()->all_companies == 1 )
                                <div class="md-form">
                                    <i class="far fa-id-card prefix grey-text"></i>
                                    <input type="text" class="form-control validate @error('licence_number') is-invalid @enderror" name="licence_number" value="{{ old('licence_number') }}" required autocomplete="licence_number" autofocus>
                                    <label data-error="wrong" data-success="right" for="licence_number">
                                        Licence Number
                                        <i class="fa fa-question-circle" aria-hidden="true" data-toggle="popover" data-placement="right" data-content="tool tip for Directions"></i>
                                    </label>
                                </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                @if(Auth()->user()->hasPermissionTo('licence_licence_sector_create') || Auth::user()->all_companies == 1 )
                                <div class="md-form">
                                    <i class="fas fa-address-card prefix grey-text"></i>
                                    <input type="text" class="form-control validate @error('licence_sector') is-invalid @enderror" name="licence_sector" value="{{ old('licence_sector') }}" required autocomplete="licence_sector" autofocus>
                                    <label data-error="wrong" data-success="right" for="licence_sector">
                                        License Sector
                                        <i class="fa fa-question-circle" aria-hidden="true" data-toggle="popover" data-placement="right" data-content="tool tip for Directions"></i>
                                    </label>
                                </div>
                                @endif
                            </div>
                            <div class="col">
                                @if(Auth()->user()->hasPermissionTo('licence_licence_contact_create') || Auth::user()->all_companies == 1 )
                                <div class="row">
                                    <div class="col-0 ml-2">
                                        <i class="fas fa-phone prefix grey-text fa-2x" style="margin-top:32px;font-size: 28px;"></i>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="md-form">
                                            <select searchable="Search here.." class="mdb-select" name="contact">
                                                <option value="" disabled>Please Select</option>
                                                <option value="1">+92</option>
                                                <option value="2">+56</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="md-form">
                                            <input type="text" class="form-control validate @error('licence_sector') is-invalid @enderror" name="licence_sector" value="{{ old('licence_sector') }}" required autocomplete="licence_sector" autofocus>
                                            <label data-error="wrong" data-success="right" for="licence_sector">
                                                Contact
                                                <i class="fa fa-question-circle" aria-hidden="true" data-toggle="popover" data-placement="right" data-content="tool tip for Directions"></i>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                @if(Auth()->user()->hasPermissionTo('licence_licence_ldn_ref_no_create') || Auth::user()->all_companies == 1 )
                                <div class="md-form">
                                    <i class="fas fa-retweet prefix grey-text"></i>
                                    <input type="text" class="form-control validate @error('reference_number') is-invalid @enderror" name="reference_number" value="{{ old('reference_number') }}" required autocomplete="reference_number" autofocus>
                                    <label data-error="wrong" data-success="right" for="reference_number">
                                        LDN Reference Number
                                        <i class="fa fa-question-circle" aria-hidden="true" data-toggle="popover" data-placement="right" data-content="tool tip for Directions"></i>
                                    </label>
                                </div>
                                @endif
                            </div>
                            <div class="col">
                                @if(Auth()->user()->hasPermissionTo('licence_tracking_number_create') || Auth::user()->all_companies == 1 )
                                <div class="md-form">
                                    <i class="fas fa-sign-out-alt prefix grey-text"></i>
                                    <input type="text" class="form-control validate @error('tracking_number') is-invalid @enderror" name="tracking_number" value="{{ old('tracking_number') }}" required autocomplete="tracking_number" autofocus>
                                    <label data-error="wrong" data-success="right" for="tracking_number">
                                        Tracking Number
                                        <i class="fa fa-question-circle" aria-hidden="true" data-toggle="popover" data-placement="right" data-content="tool tip for Directions"></i>
                                    </label>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button class="btn btn-primary btn-sm" id="create_sia_licence" type="submit"><span class="assign_class label{{getKeyid('save',$data)}}" data-id="{{getKeyid('save',$data) }}" data-value="{{checkKey('save',$data) }}">{!! checkKey('save',$data) !!} </span></button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('.datepicker').pickadate({selectYears:250,});
</script>
