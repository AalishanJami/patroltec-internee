@extends('backend.layouts.user')
@section('headscript')
  <style>
      tr>td:nth-child(1){
          width: 10%;
      }
      tr>td:nth-child(2){
          width: 12%;
          text-align: center;
      }
      tr>td:nth-child(2)>a{
          margin: 3px!important;
      }
  </style>
@stop
@section('content')
    @php
       $data=localization();
    @endphp
    @include('backend.layouts.user_sidebar')
    <div class="padding-left-custom remove-padding">
        {{ Breadcrumbs::render('sia') }}
        <div class="card">
            <div class="card-body">
                @if(Auth()->user()->hasPermissionTo('create_licence') || Auth::user()->all_companies == 1 )
                    <span class="sia_licence_table float-right">
                        <a class="text-success"  data-toggle="modal" data-target="#modalRegisterLicence">
                            <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
                        </a>
                    </span>
                @endif
                @if(Auth()->user()->hasPermissionTo('delete_selected_licence') || Auth::user()->all_companies == 1 )
                    <button class="btn btn-danger btn-sm" id="deleteselectedsia_licence">
                        <span class="assign_class label{{getKeyid('sia_licence_delete_selected',$data)}}" data-id="{{getKeyid('sia_licence_delete_selected',$data) }}" data-value="{{checkKey('sia_licence_delete_selected',$data) }}" >
                            {!! checkKey('sia_licence_delete_selected',$data) !!}
                        </span>
                    </button>
                @endif
                @if(Auth()->user()->hasPermissionTo('delete_selected_licence') || Auth::user()->all_companies == 1 )
                    <button class="btn btn-danger btn-sm" style="display: none;" id="deleteselectedsia_licenceSoft">
                        <span class="assign_class label{{getKeyid('sia_licence_delete_selected',$data)}}" data-id="{{getKeyid('sia_licence_delete_selected',$data) }}" data-value="{{checkKey('sia_licence_delete_selected',$data) }}" >
                            {!! checkKey('sia_licence_delete_selected',$data) !!}
                        </span>
                    </button>
                @endif
                @if(Auth()->user()->hasPermissionTo('select_active_licence') || Auth::user()->all_companies == 1 )
                    <button class="btn btn-success btn-sm" style="display: none;" id="selectedactivebuttonsia_licence">
                      <span class="assign_class label{{getKeyid('sia_licence_active_selected',$data)}}" data-id="{{getKeyid('sia_licence_active_selected',$data) }}" data-value="{{checkKey('sia_licence_active_selected',$data) }}" >
                        {!! checkKey('sia_licence_active_selected',$data) !!}
                      </span>
                    </button>
                @endif
                @if(Auth()->user()->hasPermissionTo('selected_restore_licence') || Auth::user()->all_companies == 1 )
                    <button class="btn btn-primary btn-sm" id="restorebuttonsia_licence">
                        <span class="assign_class label{{getKeyid('sia_licence_restore_delete',$data)}}" data-id="{{getKeyid('sia_licence_restore_delete',$data) }}" data-value="{{checkKey('sia_licence_restore_delete',$data) }}" >
                            {!! checkKey('sia_licence_restore_delete',$data) !!}
                        </span>
                    </button>
                @endif
                @if(Auth()->user()->hasPermissionTo('select_active_licence') || Auth::user()->all_companies == 1 )
                    <button  style="display: none;" class="btn btn-primary btn-sm" id="activebuttonsia_licence">
                        <span class="assign_class label{{getKeyid('sia_licence_show_active',$data)}}" data-id="{{getKeyid('sia_licence_show_active',$data) }}" data-value="{{checkKey('sia_licence_show_active',$data) }}" >
                            {!! checkKey('sia_licence_show_active',$data) !!}
                        </span>
                    </button>
                @endif
                @if(Auth()->user()->hasPermissionTo('csv_licence') || Auth::user()->all_companies == 1 )
                    <form class="form-style" id="export_excel_sia_licence" method="POST" action="{{ url('licence/export/excel') }}">
                        <input type="hidden"  name="export_excel_sia_licence" value="1">
                        <input type="hidden" class="sia_licence_export" name="excel_array" value="1">
                        {!! csrf_field() !!}
                        <button  type="submit" id="export_excel_sia_licence_btn" class="form_submit_check btn btn-warning btn-sm">
                          <span class="assign_class label{{getKeyid('sia_licence_excel_export',$data)}}" data-id="{{getKeyid('sia_licence_excel_export',$data) }}" data-value="{{checkKey('sia_licence_excel_export',$data) }}" >
                              {!! checkKey('sia_licence_excel_export',$data) !!}
                          </span>
                        </button>
                    </form>
                @endif
                @if(Auth()->user()->hasPermissionTo('word_licence') || Auth::user()->all_companies == 1 )
                    <form class="form-style" id="export_world_sia_licence" method="POST" action="{{ url('licence/export/world') }}">
                        <input type="hidden"  name="export_world_sia_licence" value="1">
                        <input type="hidden" class="sia_licence_export" name="word_array" value="1">
                        {!! csrf_field() !!}
                        <button  type="submit" id="export_world_sia_licence_btn" class="form_submit_check btn btn-success btn-sm">
                            <span class="assign_class label{{getKeyid('sia_licence_word_export',$data)}}" data-id="{{getKeyid('sia_licence_word_export',$data) }}" data-value="{{checkKey('sia_licence_word_export',$data) }}" >
                                {!! checkKey('sia_licence_word_export',$data) !!}
                            </span>
                        </button>
                    </form>
                @endif
                @if(Auth()->user()->hasPermissionTo('pdf_licence') || Auth::user()->all_companies == 1 )
                    <form  class="form-style" id="export_pdf_sia_licence"  method="POST" action="{{ url('licence/export/pdf') }}">
                        <input type="hidden"  name="export_world_pdf" value="1">
                        <input type="hidden" class="sia_licence_export" name="pdf_array" value="1">
                        {!! csrf_field() !!}
                        <button  type="submit" id="export_pdf_sia_licence_btn"  class="form_submit_check btn btn-secondary btn-sm">
                          <span class="assign_class label{{getKeyid('sia_licence_pdf_export',$data)}}" data-id="{{getKeyid('sia_licence_pdf_export',$data) }}" data-value="{{checkKey('sia_licence_pdf_export',$data) }}" >
                            {!! checkKey('sia_licence_pdf_export',$data) !!}
                          </span>
                        </button>
                    </form>
                @endif
                <div class="table-responsive">
                    <table  id="sia_licence_table" class=" table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            @if(Auth()->user()->hasPermissionTo('licence_checkbox') || Auth::user()->all_companies == 1 )
                            <th id="action" class="no-sort">
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="licence_checkbox_all">
                                    <label class="form-check-label" for="licence_checkbox_all">
                                      <span class="assign_class label{{getKeyid('sia_licence_all',$data)}}" data-id="{{getKeyid('sia_licence_all',$data) }}" data-value="{{checkKey('sia_licence_all',$data) }}" >
                                        {!! checkKey('sia_licence_all',$data) !!}
                                      </span>
                                    </label>
                                </div>
                            </th>
                            @endif
                            <th id="action" class="no-sort"></th>
                            @if(Auth()->user()->hasPermissionTo('licence_firstname') || Auth::user()->all_companies == 1 )
                            <th>
                              <span class="assign_class label{{getKeyid('sia_licence_name',$data)}}" data-id="{{getKeyid('sia_licence_name',$data) }}" data-value="{{checkKey('sia_licence_name',$data) }}" >
                                {!! checkKey('sia_licence_name',$data) !!}
                              </span>
                            </th>
                            @endif
                            @if(Auth()->user()->hasPermissionTo('licence_licence_ldn_ref_no') || Auth::user()->all_companies == 1 )
                            <th>
                              <span class="assign_class label{{getKeyid('sia_licence_service',$data)}}" data-id="{{getKeyid('sia_licence_service',$data) }}" data-value="{{checkKey('sia_licence_service',$data) }}" >
                                {!! checkKey('sia_licence_service',$data) !!}
                              </span>
                            </th>
                            @endif
                            @if(Auth()->user()->hasPermissionTo('licence_status') || Auth::user()->all_companies == 1 )
                            <th>
                              <span class="assign_class label{{getKeyid('sia_licence_status',$data)}}" data-id="{{getKeyid('sia_licence_status',$data) }}" data-value="{{checkKey('sia_licence_status',$data) }}" >
                                {!! checkKey('sia_licence_status',$data) !!}
                              </span>
                            </th>
                            @endif
                            @if(Auth()->user()->hasPermissionTo('licence_licence') || Auth::user()->all_companies == 1 )
                            <th>
                              <span class="assign_class label{{getKeyid('sia_licence_licence',$data)}}" data-id="{{getKeyid('sia_licence_licence',$data) }}" data-value="{{checkKey('sia_licence_licence',$data) }}" >
                                {!! checkKey('sia_licence_licence',$data) !!}
                              </span>
                            </th>
                            @endif
                            @if(Auth()->user()->hasPermissionTo('licence_end_date') || Auth::user()->all_companies == 1 )
                            <th>
                              <span class="assign_class label{{getKeyid('sia_licence_expiry',$data)}}" data-id="{{getKeyid('sia_licence_expiry',$data) }}" data-value="{{checkKey('sia_licence_expiry',$data) }}" >
                                {!! checkKey('sia_licence_expiry',$data) !!}
                              </span>
                            </th>
                            @endif
                            @if(Auth()->user()->hasPermissionTo('licence_licence_no') || Auth::user()->all_companies == 1 )
                            <th>
                              <span class="assign_class label{{getKeyid('sia_licence_licence_number',$data)}}" data-id="{{getKeyid('sia_licence_licence_number',$data) }}" data-value="{{checkKey('sia_licence_licence_number',$data) }}" >
                                {!! checkKey('sia_licence_licence_number',$data) !!}
                              </span>
                            </th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                    @if(Auth()->user()->all_companies == 1)
                        <input type="hidden" id="licence_checkbox" value="1">
                        <input type="hidden" id="licence_firstname" value="1">
                        <input type="hidden" id="licence_licence_ldn_ref_no" value="1">
                        <input type="hidden" id="licence_status" value="1">
                        <input type="hidden" id="licence_licence" value="1">
                        <input type="hidden" id="licence_end_date" value="1">
                        <input type="hidden" id="licence_licence_no" value="1">
                    @else
                        <input type="hidden" id="licence_checkbox" value="{{Auth()->user()->hasPermissionTo('licence_checkbox')}}">
                        <input type="hidden" id="licence_firstname" value="{{Auth()->user()->hasPermissionTo('licence_firstname')}}">
                        <input type="hidden" id="licence_licence_ldn_ref_no" value="{{Auth()->user()->hasPermissionTo('licence_licence_ldn_ref_no')}}">
                        <input type="hidden" id="licence_status" value="{{Auth()->user()->hasPermissionTo('licence_status')}}">
                        <input type="hidden" id="licence_licence" value="{{Auth()->user()->hasPermissionTo('licence_licence')}}">
                        <input type="hidden" id="licence_end_date" value="{{Auth()->user()->hasPermissionTo('licence_end_date')}}">
                        <input type="hidden" id="licence_licence_no" value="{{Auth()->user()->hasPermissionTo('licence_licence_no')}}">
                    @endif
            </div>
        </div>
    </div>
    @include('backend.label.input_label')
    @component('backend.sia_licence.model.create')
    @endcomponent
    @component('backend.sia_licence.model.edit')
    @endcomponent
<script type="text/javascript" src="{{ asset('custom/js/sia_licence.js') }}" ></script>
<script type="text/javascript">
    $(document).ready(function() {
        var url='/licence/getall';
        sia_licenceAppend(url);

        $('#licence_checkbox_all').click(function() {
            if ($(this).is(':checked')) {
                $('.licence_checked').attr('checked', true);
            } else {
                $('.licence_checked').attr('checked', false);
            }
        });
    });
    function sia_licenceAppend(url)
    {
          var table = $('#sia_licence_table').dataTable({
             processing: true,
              language: {
                  'lengthMenu': '  _MENU_ ',
                  'search':'<span class="assign_class label'+$('#sia_licence_search_id').attr('data-id')+'" data-id="'+$('#sia_licence_search_id').attr('data-id')+'" data-value="'+$('#sia_licence_search_id').val()+'" >'+$('#sia_licence_search_id').val()+'</span>',
                  'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                  'info':'',
                  'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                  'paginate': {
                      'previous': '<span class="assign_class label'+$('#sia_licence_previous_label').attr('data-id')+'" data-id="'+$('#sia_licence_previous_label').attr('data-id')+'" data-value="'+$('#sia_licence_previous_label').val()+'" >'+$('#sia_licence_previous_label').val()+'</span>',
                      'next': '<span class="assign_class label'+$('#sia_licence_next_label').attr('data-id')+'" data-id="'+$('#sia_licence_next_label').attr('data-id')+'" data-value="'+$('#sia_licence_next_label').val()+'" >'+$('#sia_licence_next_label').val()+'</span>',
                  },
                  'infoFiltered': "(filtered from _MAX_ total records)"
              },
              "ajax": {
                  "url": url,
                  "type": 'get',
              },
            "createdRow": function( row, data, dataIndex,columns ) {
                var checkbox='';
                checkbox+='<div class="form-check"><input type="checkbox" class="form-check-input licence_checked" id="sia_licence'+data.id+'"><label class="form-check-label" for="sia_licence'+data.id+'""></label></div>';
                $(columns[0]).html(checkbox);
                $(row).attr('id', 'sia_licence_tr'+data['id']);
                $(row).attr('class', 'selectedrowsia_licence');
              },
              columns: [
              {data: 'checkbox', name: 'checkbox',visible:$('#licence_checkbox').val()},
              {data: 'actions', name: 'actions'},
              {data: 'first_name', name: 'first_name',visible:$('#licence_firstname').val()},
              {data: 'licence_loc', name: 'licence_loc',visible:$('#licence_licence_ldn_ref_no').val()},
              {data: 'type', name: 'type',visible:$('#licence_status').val()},
              {data: 'sia_reference', name: 'sia_reference',visible:$('#licence_licence').val()},
              {data: 'end', name: 'end',visible:$('#licence_end_date').val()},
              {data: 'sia_number', name: 'sia_number',visible:$('#licence_licence_no').val()},

              ],

          });
          (function ($) {
            var active ='<span class="assign_class label'+$('#breadcrumb_sia_licence').attr('data-id')+'" data-id="'+$('#breadcrumb_sia_licence').attr('data-id')+'" data-value="'+$('#breadcrumb_sia_licence').val()+'" >'+$('#breadcrumb_sia_licence').val()+'</span>';
            $('.breadcrumb-item.active').html(active);
            var parent ='<span class="assign_class label'+$('#dashboard_label').attr('data-id')+'" data-id="'+$('#dashboard_label').attr('data-id')+'" data-value="'+$('#dashboard_label').val()+'" >'+$('#dashboard_label').val()+'</span>';
            $('.breadcrumb-item a').html(parent);
          }(jQuery));

    }
</script>
@endsection

