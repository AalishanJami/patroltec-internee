<input type="hidden" name="id" value="{{$sia_licence->id}}">
<div class="modal-body">
    <div class="card-body px-lg-5 pt-0">
        <div class="form-row">
            <div class="col">
                @if(Auth()->user()->hasPermissionTo('licence_firstname_edit') || Auth::user()->all_companies == 1 )
                <div class="md-form">
                    <i class="fas fa-user prefix grey-text"></i>
                    <input type="text" class="form-control validate @error('first_name') is-invalid @enderror" name="first_name" value="{{ $sia_licence->first_name  }}" required autocomplete="first_name" autofocus>
                    <label data-error="wrong" data-success="right" for="first_name" class="active">
                        First Name
                        <i class="fa fa-question-circle" aria-hidden="true" data-toggle="popover" data-placement="right" data-content="tool tip for Directions"></i>
                    </label>
                </div>
                @endif
            </div>
            <div class="col">
                @if(Auth()->user()->hasPermissionTo('licence_surname_edit') || Auth::user()->all_companies == 1 )
                <div class="md-form">
                    <i class="fas fa-user prefix grey-text"></i>
                    <input type="text" class="form-control validate @error('surname') is-invalid @enderror" name="surname" value="{{$sia_licence->surname}}" required autocomplete="surname" autofocus>
                    <label data-error="wrong" data-success="right" for="surname" class="active">
                        Sur Name
                        <i class="fa fa-question-circle" aria-hidden="true" data-toggle="popover" data-placement="right" data-content="tool tip for Directions"></i>
                    </label>
                </div>
                @endif
            </div>
        </div>
        <div class="form-row">
            <div class="col">
                @if(Auth()->user()->hasPermissionTo('licence_status_edit') || Auth::user()->all_companies == 1 )
                <div class="md-form">
                    <div class="row">
                        <div class="col">
                            <i class="fas fa-thermometer prefix grey-text"></i>
                            <label data-error="wrong" data-success="right" for="name">
                                Status
                            </label>
                        </div>
                    </div>
                    <div class="md-form tex-left">
                        <div class="row">
                            <div class="form-check col-md-3">
                                <input type="radio" {{ $sia_licence->status == 1 ? 'checked':'' }} class="form-check-input" id="status_active{{ $sia_licence->id}}" value="1" name="status">
                                <label class="form-check-label" for="status_active{{ $sia_licence->id}}">Active</label>
                            </div>
                            <div class="form-check col-md-3">
                                <input type="radio" {{ $sia_licence->status == 0 ? 'checked':'' }} class="form-check-input" id="status_deactive{{ $sia_licence->id}}" value="0" name="status" checked>
                                <label class="form-check-label" for="status_deactive{{ $sia_licence->id}}">Deactive</label>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
            <div class="col">
                @if(Auth()->user()->hasPermissionTo('licence_licence_edit') || Auth::user()->all_companies == 1 )
                <div class="md-form">
                    <i class="fas fa-id-card prefix grey-text"></i>
                    <input type="text" class="form-control validate @error('licence') is-invalid @enderror" name="licence" value="{{ $sia_licence->licence_loc }} " required autocomplete="licence" autofocus>
                    <label data-error="wrong" data-success="right" for="licence" class="active">
                        Licence
                        <i class="fa fa-question-circle" aria-hidden="true" data-toggle="popover" data-placement="right" data-content="tool tip for Directions"></i>
                    </label>
                </div>
                @endif
            </div>
        </div>
        <div class="form-row">
            <div class="col">
                @if(Auth()->user()->hasPermissionTo('licence_end_date_edit') || Auth::user()->all_companies == 1 )
                <div class="md-form">
                    <i class="fas fa-calendar prefix grey-text"></i>
                     {!! Form::text('expiry',Carbon\Carbon::parse($sia_licence->end)->format('jS M Y'), ['class' => 'form-control datepicker']) !!}
                   <label class="active" for="date-picker-example">End Date</label>
                </div>
                @endif
            </div>
            <div class="col">
                @if(Auth()->user()->hasPermissionTo('licence_role_edit') || Auth::user()->all_companies == 1 )
                <div class="row">
                    <div class="col-0 ml-3">
                        <i class="fas fa-user prefix grey-text fa-2x" style="margin-top:32px;font-size: 28px;"></i>
                    </div>
                    <div class="col">
                        <div class="md-form">
                            <label data-error="wrong" data-success="right" for="role" class="active">Role</label>
                            <input type="text" class="form-control validate @error('role') is-invalid @enderror" name="role" value="{{ $sia_licence->role }} " required autocomplete="role" autofocus>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
        <div class="form-row">
            <div class="col">
                @if(Auth()->user()->hasPermissionTo('licence_notes_edit') || Auth::user()->all_companies == 1 )
                <div class="md-form">
                    <i class="fas fa-sticky-note grey-text prefix"></i>
                    <input type="text" class="form-control validate @error('notes') is-invalid @enderror" name="notes" value="{{ $sia_licence->notes  }}" required autocomplete="notes" autofocus>
                    <label data-error="wrong" data-success="right" for="notes" class="active">
                        Notes
                        <i class="fa fa-question-circle" aria-hidden="true" data-toggle="popover" data-placement="right" data-content="tool tip for Directions"></i>
                    </label>
                </div>
                @endif
            </div>
            <div class="col">
                @if(Auth()->user()->hasPermissionTo('licence_licence_no_edit') || Auth::user()->all_companies == 1 )
                <div class="md-form">
                    <i class="far fa-id-card prefix grey-text"></i>
                    <input type="text" class="form-control validate @error('licence_number') is-invalid @enderror" name="licence_number" value="{{ $sia_licence->sia_number  }}" required autocomplete="licence_number" autofocus>
                    <label data-error="wrong" data-success="right" for="licence_number" class="active">
                        Licence Number
                        <i class="fa fa-question-circle" aria-hidden="true" data-toggle="popover" data-placement="right" data-content="tool tip for Directions"></i>
                    </label>
                </div>
                @endif
            </div>
        </div>
        <div class="form-row">
            <div class="col">
                @if(Auth()->user()->hasPermissionTo('licence_licence_sector_edit') || Auth::user()->all_companies == 1 )
                <div class="md-form">
                    <i class="fas fa-address-card prefix grey-text"></i>
                    <input type="text" class="form-control validate @error('licence_sector') is-invalid @enderror" name="licence_sector" value="{{ $sia_licence->sector  }}" required autocomplete="licence_sector" autofocus>
                    <label data-error="wrong" data-success="right" for="licence_sector" class="active">
                        License Sector
                        <i class="fa fa-question-circle" aria-hidden="true" data-toggle="popover" data-placement="right" data-content="tool tip for Directions"></i>
                    </label>
                </div>
                @endif
            </div>


            <div class="col">
                @if(Auth()->user()->hasPermissionTo('licence_licence_contact_edit') || Auth::user()->all_companies == 1 )
                <div class="row">
                    <div class="col-0 ml-2">
                        <i class="fas fa-phone prefix grey-text fa-2x" style="margin-top:32px;font-size: 28px;"></i>
                    </div>
                    <div class="col-md-2">
                        <div class="md-form">
                            <select searchable="Search here.." class="mdb_select_sia_licence" name="contact">
                                <option value="" disabled>Please Select</option>
                                <option value="1" {{ $sia_licence->contact == 1 ? 'selected':'' }}>+92</option>
                                <option value="2" {{ $sia_licence->contact == 2 ? 'selected':'' }} >+56</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="md-form">
                            <input type="text" class="form-control validate @error('licence_sector') is-invalid @enderror" name="licence_sector" value="{{ old('licence_sector') }}" required autocomplete="licence_sector" autofocus>
                            <label data-error="wrong" data-success="right" for="licence_sector">
                                Contact
                                <i class="fa fa-question-circle" aria-hidden="true" data-toggle="popover" data-placement="right" data-content="tool tip for Directions"></i>
                            </label>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
        <div class="form-row">
            <div class="col">
                @if(Auth()->user()->hasPermissionTo('licence_licence_ldn_ref_no_edit') || Auth::user()->all_companies == 1 )
                <div class="md-form">
                    <i class="fas fa-retweet prefix grey-text"></i>
                    <input type="text" class="form-control validate @error('reference_number') is-invalid @enderror" name="reference_number" value="{{$sia_licence->sia_reference}}" required autocomplete="reference_number" autofocus>
                    <label data-error="wrong" data-success="right" for="reference_number" class="active">
                        LDN Reference Number
                        <i class="fa fa-question-circle" aria-hidden="true" data-toggle="popover" data-placement="right" data-content="tool tip for Directions"></i>
                    </label>
                </div>
                @endif
            </div>
            <div class="col">
                @if(Auth()->user()->hasPermissionTo('licence_tracking_number_edit') || Auth::user()->all_companies == 1 )
                <div class="md-form">
                    <i class="fas fa-sign-out-alt prefix grey-text"></i>
                    <input type="text" class="form-control validate @error('tracking_number') is-invalid @enderror" name="tracking_number" value="{{ $sia_licence->tracking_number}}" required autocomplete="tracking_number" autofocus>
                    <label data-error="wrong" data-success="right" for="tracking_number" class="active">
                        Tracking Number
                        <i class="fa fa-question-circle" aria-hidden="true" data-toggle="popover" data-placement="right" data-content="tool tip for Directions"></i>
                    </label>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="modal-footer d-flex justify-content-center">
    <button class="btn btn-primary btn-sm" id="edit_sia_licence" type="submit"><span class="assign_class">Update </span></button>
</div>
