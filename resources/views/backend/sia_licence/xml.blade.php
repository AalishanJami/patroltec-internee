<table>
    <thead>
    <tr>
        @if(Auth()->user()->hasPermissionTo('licence_firstname_excel_export') || Auth::user()->all_companies == 1 )
            <th>Name</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('licence_licence_ldn_ref_no_excel_export') || Auth::user()->all_companies == 1 )
            <th> Service</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('licence_status_excel_export') || Auth::user()->all_companies == 1 )
            <th> Status</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('licence_licence_excel_export') || Auth::user()->all_companies == 1 )
            <th> Licence</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('licence_end_date_excel_export') || Auth::user()->all_companies == 1 )
            <th> Expiry</th>
        @endif
        @if(Auth()->user()->hasPermissionTo('licence_licence_no_excel_export') || Auth::user()->all_companies == 1 )
            <th> Licence Number</th>
        @endif
    </tr>
    </thead>
    <tbody>
    @foreach($data as $row)
        <tr>
            @if(Auth()->user()->hasPermissionTo('licence_firstname_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{ $row->name }}</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('licence_licence_ldn_ref_no_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{ $row->licence_loc }}</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('licence_status_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{ $row->type }}</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('licence_licence_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{ $row->sia_reference }}</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('licence_end_date_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{ Carbon\Carbon::parse($row->end)->format('jS M Y') }}</td>
            @endif
            @if(Auth()->user()->hasPermissionTo('licence_licence_no_excel_export') || Auth::user()->all_companies == 1 )
                <td>{{ $row->sia_number }}</td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>
