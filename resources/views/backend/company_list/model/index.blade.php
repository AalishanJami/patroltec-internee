 @php
        $data=localization();
@endphp
<style type="text/css">
   .form-check-input {
    position: absolute !important;
    pointer-events: none !important;
    opacity: 0!important;
}
</style>
<div class="modal fade" id="modalCompanyIndex" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold"><span > Company Listing </span></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <div class="col-md-12">
            <span class="table-add float-right mb-3 mr-2">
            <a class="text-success"  data-toggle="modal" data-target="#companyCreate">
                <i class="fas fa-plus fa-2x" aria-hidden="true"></i>
            </a>
            </span>
            <button class="btn btn-danger btn-sm">
                Delete Selected Manager Company
            </button>
            <button class="btn btn-primary btn-sm">
              Restore Deleted Manager Company
            </button>
           
            <button  style="display: none;">
                Show Active Manager Company
            </button>
            <button class="btn btn-warning btn-sm">
               Excel Export
            </button>
            <button class="btn btn-success btn-sm">
                Word Export
            </button>
           
        </div>
     
   <table id="company_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th> ID
            </th>
            <th> Name
            </th>        
            <th id="action">
            </th>
        </tr>
        </thead>
            <tr>
                <td>1</td>
                <td>Jonny</td>
                <td>
                    <a  data-toggle="modal" data-target="#modalEditCompany" class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i></a>
                    <button type="button"class="btn btn-danger btn-sm my-0" onclick="deleteCOmpanydata()"><i class="fas fa-trash"></i></button>

                </td>
            </tr>
        <tbody>
        </tbody>
    </table>
</div>
    </div>
</div>

    @component('backend.company_list.model.create')
    @endcomponent

     {{--    Edit Modal--}}
    @component('backend.company_list.model.edit')
    @endcomponent
    {{--    END Edit Modal--}}
        <script type="text/javascript">
            function deleteCOmpanydata()
            {

                swal({
                    title:'Are you sure?',
                    text: "Delete Record.",
                    type: "error",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, Delete!",
                    cancelButtonText: "Cancel!",
                    closeOnConfirm: false,
                    customClass: "confirm_class",
                    closeOnCancel: false,
                },
                function(isConfirm){
                    swal.close();
             });

                
            }

        </script>
 