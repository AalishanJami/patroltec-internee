<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Customer Support</title>
<style>
.email_table {
  color: #333;
  font-family: sans-serif;
  font-size: 15px;
  font-weight: 300;
  text-align: center;
  border-collapse: separate;
  border-spacing: 0;
  width: 99%;
  margin: 6px auto;
  box-shadow:none;
}
table {
  color: #333;
  font-family: sans-serif;
  font-size: 15px;
  font-weight: 300;
  text-align: center;
  border-collapse: separate;
  border-spacing: 0;
  width: 99%;
  margin: 20px auto;
  box-shadow: 0 4px 8px 0 rgba(0,0,0,.16);
}

th {font-weight: bold; padding:10px; border-bottom:2px solid #000;}

tbody td {border-bottom: 1px solid #ddd; padding:10px;}



.email_main_div{width:700px; margin:auto; background-color:#EEEEEE; min-height:500px; border:2px groove #ddd;}
strong{font-weight:bold;}
.item_table{text-align:left;}
</style>
</head>

<body>
<div class="email_main_div">
<table class="email_table">
	<tr>
		<td width="100px"  style="text-align:left; padding:10px;">
			<img src="https://patroltec.net/backend/web/uploads/_180705101111.png" width="100" />
		</td>
	</tr>
</table>

<table class="email_table" style="margin-top:14px;">
	<tr>
		<td width="350px;" style="text-align:left; padding:10px;">
    Dear {{$name ?? ""}}, <br/>
		<br />
		Please find the below notification of works required,<br />
		</td>
	</tr>
</table>

<table class="item_table">
  <thead>
  </thead>
  <tbody>
    <tr>
      <td>Job ID</td>
      <td>{{$id ?? ""}}</td>

    </tr>
    <tr>
      <td>Project Name</td>
	  <td>{{$project_name ?? ""}}</td>

    </tr>
    <tr>
      <td>Location</td>
	  <td>{{$locationbreaks ?? ""}}</td>
   
    </tr> 
    <tr>
      <td>Date</td>
    <td>{{$date ?? ""}}</td>
  
    </tr>
    <tr>
      <td>Cost</td>
    <td>£{{$cost?? ""}}.00</td>
 
    </tr>
    <tr>
      <td>Instructions</td>
    <td>{{$notes ?? ""}}</td>
 
    </tr>

    <tr>
      <td>Billin Notes </td>
    <td>{{$billing_info ?? ""}}</td>
 
    </tr>
	<tr>
	 
	</tr>
  </tbody>
</table>
</div>
</body>
</html>
