@extends('backend.layouts.app')

@section('content')
    @php
        $data=localization();
    @endphp
    <div class="col-md-6 col-xl-5 offset-xl-1">
    @include('errors.errorDisplay')
    <!-- Form -->
        <div class="card wow fadeInRight" data-wow-delay="0.3s" style="visibility: visible; animation-name: fadeInRight; animation-delay: 0.3s;">
            <div class="card-body">
                <!-- Header -->
                <div class="text-center">
                    <h3 class="white-text">
                        <i class="fas fa-user white-text"></i>
                        <span data-id="{{getKeyid('login',$data) }}" data-value="{{checkKey('login',$data) }}" class=" assign_class  label{{getKeyid('login',$data)}}">
                            {!! checkKey('login',$data) !!}
                        </span></h3>
                    <hr class="hr-light">
                </div>
                <!-- Body -->
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="md-form">
                        <i class="fas fa-envelope prefix white-text"></i>
                        <input id="email" id="form2" type="email" style="color: white;" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                        @if ($errors->has('email'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                        <label for="form2" class="white-text">
                            <span data-id="{{getKeyid('email',$data) }}" data-value="{{checkKey('email',$data) }}" class=" assign_class  label{{getKeyid('email',$data)}}">
                                {!! checkKey('email',$data) !!}
                            </span>
                        </label>
                    </div>
                    <div class="md-form">
                        <i class="fas fa-lock prefix white-text"></i>
                        <input id="password" id="form4" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" style="color: #fff" required>
                        @if ($errors->has('password'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                        <label for="form4" class="white-text">
                            <span data-id="{{getKeyid('password',$data) }}" data-value="{{checkKey('password',$data) }}" class=" assign_class  label{{getKeyid('password',$data)}}">
                                {!! checkKey('password',$data) !!}
                            </span>
                        </label>
                    </div>
                    <div class="text-center mt-4">
                        <button class="btn btn-light-blue btn-rounded waves-effect waves-light">
                            <span data-id="{{getKeyid('sign_up',$data) }}" data-value="{{checkKey('sign_up',$data) }}" class=" assign_class  label{{getKeyid('sign_up',$data)}}">
                                {!! checkKey('sign_up',$data) !!}
                            </span>
                        </button>
                        <hr class="hr-light mb-3 mt-4">
                        <a class="btn btn-light-blue" href="{{ route('password.request') }}">
                            <span data-id="{{getKeyid('forget_password',$data) }}" data-value="{{checkKey('forget_password',$data) }}" class=" assign_class  label{{getKeyid('forget_password',$data)}}">
                                {!! checkKey('forget_password',$data) !!}
                            </span>
                        </a>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.Form -->
    </div>
@endsection
