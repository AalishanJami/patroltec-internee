@extends('backend.layouts.app')

@section('content')
<div class="col-md-6 col-xl-5 offset-xl-1">
  @include('errors.errorDisplay')
 <!-- Form -->
    <div class="card wow fadeInRight" data-wow-delay="0.3s" style="visibility: visible; animation-name: fadeInRight; animation-delay: 0.3s;">
        <div class="card-body">
           <!-- Header -->
           <div class="text-center">
              <h3 class="white-text"><i class="fas fa-user white-text"></i> {{ __('Reset Password') }}</h3>
              <hr class="hr-light">
           </div>
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
           <!-- Body -->
            <form method="POST" action="{{ route('password.email') }}">
                @csrf
              <div class="md-form">
                  <i class="fas fa-envelope prefix white-text"></i>
                   <input id="email" type="email" id="form2" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror                 
                  <label for="form2" class="white-text">Your email</label>
              </div>
              <div class="text-center mt-4">
                  <button class="btn btn-light-blue btn-rounded waves-effect waves-light"> {{ __('Send Password Reset Link') }}</button> 

                  
                  <hr class="hr-light mb-3 mt-4">
                  <a class="btn btn-light-blue" href="{{url('login')}}"> Back to Login</a> 

              </div>

           </form>
        </div>
    </div>
 <!-- /.Form -->
</div>
@endsection
