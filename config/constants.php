<?php

return [
	'store' => 'add',
	'update' => 'update',
    'delete' => 'delete',
    'restore' => 'restore',
    'soft_delete' => 'soft_delete',
    'selected_delete' => 'Selected delete',
    'selected_soft_delete' => 'Selected soft delete',
    'selected_active' => 'Selected Active',
];
