$(document).ready(function() {
    var selectedrows = [];
    $('body').on('click', '#support_checkbox_all', function () {
        $('input:checkbox').not(this).prop('checked', this.checked);
        $('input[type="checkbox"]').each(function(key,value) {
            var id=$(this).attr('id');
            console.log(id);
            if(id)
            {
                id=id.split('support');
                row_id='#support_tr'+id[1];
                if($(this).is(":checked"))
                {
                    if(key == 0)
                    {
                        selectedrows=[];
                    }
                    if(id[1] != '_checkbox_all')
                    {
                        selectedrows.push(id[1]);
                    }
                    $('.export_array').val(JSON.stringify(selectedrows));
                    $(row_id).addClass('selected');
                }
                else
                {
                    $('.export_array').val(1);
                    selectedrows=[];
                    $(row_id).removeClass('selected');
                }
            }
        });
    });
    $('body').on('click', '.selectedrowsupport', function () {
        var id=$(this).attr('id');
        id=id.split('support');
        row_id='#support_tr'+id[1];
        if($(this).is(":checked"))
        {
            selectedrows.push(id[1]);
            $('.export_array').val(JSON.stringify(selectedrows));
            $(row_id).addClass('selected');
        }else
        {
            if(selectedrows.includes(id[1]))
            {
                selectedrows.splice( selectedrows.indexOf(id[1]),1);
                $('.export_array').val(JSON.stringify(selectedrows));
                $(row_id).removeClass('selected');
            }
        }
    });

    $('#restorebuttonsupport').click(function () {
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        selectedrows=[];
        $('#restorebuttonsupport').hide();
        $('#showactivebuttonsupport').show();
        $('#activeselectedbuttonsupport').show();
        $('.support_export').hide();
        $('#supportflag').val('0');
        $('#support_data').DataTable().clear().destroy();
        var url='/support/getAllSoft';
        support_data(url);
    });
    $('#showactivebuttonsupport').click(function () {
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        selectedrows=[];
        $('#restorebuttonsupport').show();
        $('#showactivebuttonsupport').hide();
        $('#activeselectedbuttonsupport').hide();
        $('.support_export').show();
        $('#supportflag').val('1');
        $('#support_data').DataTable().clear().destroy();
        var url='/support/getall';
        support_data(url);
    });
    $('#deleteselectedsupport').click(function(){
        if ($('#supportflag').val()=='0'){
            var url='/support/deleteMultipleSOft';
        }else{
            var url='/support/deleteMultiple';
        }
        delete_mutilpleDataSupport(url,selectedrows);
    });
    $('#activeselectedbuttonsupport').click(function(){
        restore_mutilpleDataSupport(selectedrows);
    });
});

function delete_mutilpleDataSupport(url,selectedrows)
{
    if( $('.edit_cms_disable').css('display') == 'none' ) {
        console.log('Editer mode On Please change your mode');
        return 0;
    }
    if (selectedrows && selectedrows.length) {
        // not empty
        swal({
                title: "Are you sure?",
                text: "Selected option will be inactive and can be Active again by the admin only!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:url,
                        data: {'ids':selectedrows},
                        success: function(response)
                        {

                            toastr["success"](response.message);
                            if(response.flag==1)
                            {
                                var url='/support/getall';
                            }
                            else
                            {
                                var url='/support/getAllSoft';
                            }
                            $('#support_data').DataTable().clear().destroy();
                            support_data(url);
                            swal.close();

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                    swal("Cancelled", "Your Record is safe", "error");

                }
            });
    }
    else
    {
        // empty
        return toastr["error"]("Select at least one record to delete!");
    }
}

function restore_mutilpleDataSupport(selectedrows)
{
    if( $('.edit_cms_disable').css('display') == 'none' ) {
        console.log('Editer mode On Please change your mode');
        return 0;
    }
    if (selectedrows && selectedrows.length) {
        // not empty
        swal({
                title: "Are you sure?",
                text: "Selected option will be inactive and can be Active again by the admin only!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:'/support/restore/mutilple',
                        data: {'ids':selectedrows},
                        success: function(response)
                        {

                            toastr["success"](response.message);
                            var url='/support/getAllSoft';
                            $('#support_data').DataTable().clear().destroy();
                            support_data(url);
                            swal.close();

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                    swal("Cancelled", "Your Record is safe", "error");

                }
            });
    }
    else
    {
        // empty
        return toastr["error"]("Select at least one record to delete!");
    }
}
function getSupportdata(id)
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type : 'post',
        url : '/support/edit',
        data:{'id':id},
        success:function(data)
        {

            console.log(data);
            $('#modalEditAnswer').modal('show');
            $('#title_support').val(data.title);
            $('#message_support').val(data.message);


        }
    });


}
function deleteSupportdata(id)
{
    var custom_class='';
    if( $('.edit_cms_disable').css('display') == 'none' ) {
        custom_class='assign_class get_text';
    }
    else
    {
        custom_class='assign_class';
    }
    console.log(custom_class);
    swal({
            title:'Are you sure?',
            text: "Delete Record.",
            type: "error",
            showCloseButton: true,
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Delete!",
            cancelButtonText: "Cancel!",
            closeOnConfirm: false,
            customClass: "confirm_class",
            closeOnCancel: false,
        },
        function(isConfirm){
            console.log( isConfirm);
            if( $('.edit_cms_disable').css('display') != 'none' ) {
                if (isConfirm) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type : 'post',
                        url : '/support/delete',
                        data:{'id':id},
                        success:function(data)
                        {

                            setTimeout(function () {
                                swal({
                                        title: "",
                                        text: "Delete Sucessfully!",
                                        type: "success",
                                        confirmButtonText: "OK"
                                    },

                                    function(isConfirm){

                                        if (isConfirm) {
                                            $('#support_data').DataTable().clear().destroy();
                                            var url='/support/getall';
                                            support_data(url);
                                        }
                                    }); }, 500);
                        }
                    });

                }
                else
                {
                    swal.close();
                }
            }
        });
}
function harddeleteSupportdata(id)
{
    var custom_class='';
    if( $('.edit_cms_disable').css('display') == 'none' ) {
        custom_class='assign_class get_text';
    }
    else
    {
        custom_class='assign_class';
    }
    console.log(custom_class);
    swal({
            title:'Are you sure?',
            text: "Delete Record.",
            type: "error",
            showCloseButton: true,
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Delete!",
            cancelButtonText: "Cancel!",
            closeOnConfirm: false,
            customClass: "confirm_class",
            closeOnCancel: false,
        },
        function(isConfirm){
            console.log( isConfirm);
            if( $('.edit_cms_disable').css('display') != 'none' ) {
                if (isConfirm) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type : 'post',
                        url : '/support/harddelete',
                        data:{'id':id},
                        success:function(data)
                        {
                            setTimeout(function () {
                                swal({
                                        title: "",
                                        text: "Delete Sucessfully!",
                                        type: "success",
                                        confirmButtonText: "OK"
                                    },
                                    function(isConfirm){
                                        if (isConfirm) {
                                            $('#support_data').DataTable().clear().destroy();
                                            var url='/support/getAllSoft';
                                            support_data(url);
                                        }
                                    }); }, 500);
                        }
                    });

                }
                else
                {
                    swal.close();
                }
            }
        });
}

function restoreSupportData(id)
{
    var custom_class='';
    if( $('.edit_cms_disable').css('display') == 'none' ) {
        custom_class='assign_class get_text';
    }
    else
    {
        custom_class='assign_class';
    }
    console.log(custom_class);
    swal({
            title:'Are you sure?',
            text: "Delete Record.",
            type: "error",
            showCloseButton: true,
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Delete!",
            cancelButtonText: "Cancel!",
            closeOnConfirm: false,
            customClass: "confirm_class",
            closeOnCancel: false,
        },
        function(isConfirm){
            console.log( isConfirm);
            if( $('.edit_cms_disable').css('display') != 'none' ) {
                if (isConfirm) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type : 'post',
                        url : '/support/restore',
                        data:{'id':id},
                        success:function(data)
                        {
                            setTimeout(function () {
                                swal({
                                        title: "",
                                        text: "Delete Sucessfully!",
                                        type: "success",
                                        confirmButtonText: "OK"
                                    },
                                    function(isConfirm){
                                        if (isConfirm) {
                                            $('#support_data').DataTable().clear().destroy();
                                            var url='/support/getAllSoft';
                                            support_data(url);
                                        }
                                    }); }, 500);
                        }
                    });

                }
                else
                {
                    swal.close();
                }
            }
        });
}
