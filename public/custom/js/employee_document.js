 $(document).ready(function() {
     var selectedrows=[];
     $('body').on('click','#employee_document_checkbox_all',function() {
         $('input:checkbox').not(this).prop('checked', this.checked);
         $('input[type="checkbox"]').each(function(key,value) {
             var id=$(this).attr('id');
             console.log(id);
             if(id)
             {
                 id=id.split('employee_document');
                 row_id='#employee_document_tr'+id[1];
                 if($(this).is(":checked"))
                 {
                     if(key == 0)
                     {
                         selectedrows=[];
                     }
                     if(id[1] != '_checkbox_all')
                     {
                         selectedrows.push(id[1]);
                     }
                     $('.employee_document_export').val(JSON.stringify(selectedrows));
                     $(row_id).addClass('selected');
                 }
                 else
                 {
                     $('.employee_document_export').val(1);
                     selectedrows=[];
                     $(row_id).removeClass('selected');
                 }
             }
         });
     });


    $('body').on('click', '.employee_document_checked', function() {
        var id=$(this).attr('id');
        id=id.split('employee_document');
        row_id='#employee_document_tr'+id[1];
        if($(this).is(":checked"))
        {
            selectedrows.push(id[1]);
            $('.employee_document_export').val(JSON.stringify(selectedrows));
            $(row_id).addClass('selected');
        }else
        {
            if(selectedrows.includes(id[1]))
            {
                selectedrows.splice( selectedrows.indexOf(id[1]),1);
                $('.employee_document_export').val(JSON.stringify(selectedrows));
                $(row_id).removeClass('selected');
            }
        }
    });
    $('#selectedactivebuttonemployee_document').click(function(){
        var url='/user/document/active/multiple';
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        if (selectedrows && selectedrows.length) {
       // not empty
            swal({
                title: "Are you sure?",
                text: "Selected option will be active  again ..!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
             },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:url,
                        data: {'ids':selectedrows},
                        success: function(response)
                        {

                            toastr["success"](response.message);
                            var url='/user/document/getallSoft';
                            $('#employee_document_table').DataTable().clear().destroy();
                            employee_document_data(url);
                            swal.close();

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                  swal("Cancelled", "Your Record is safe", "error");

                }
            });
         }
        else
        {
       // empty
            return toastr["error"]("Select at least one record to delete!");
        }
    });
    $('#deleteselectedemployee_document').click(function(){
       var url='/user/document/delete/multiple';
       delete_mutilpleData(url,selectedrows);
    });
    $('#deleteselectedemployee_documentSoft').click(function(){
       var url='/user/document/delete/soft/multiple';
       delete_mutilpleData(url,selectedrows);
    });
    $('#restorebuttonemployee_document').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        $('#export_excel_employee_document_btn').hide();
        $('#export_world_employee_document_btn').hide();
        $('#export_pdf_employee_document_btn').hide();
        $('.active_badge').html('0');
        $('.expiry_badge').html('0');
        $('#restorebuttonemployee_document').hide();
        $('#deleteselectedemployee_document').hide();
        $('#selectedactivebuttonemployee_document').show();
        $('#deleteselectedemployee_documentSoft').show();
        $('#export_excel_employee_document').val(2);
        $('#export_world_employee_document').val(2);
        $('#export_world_pdf').val(2);
        $('.employee_document_add').hide();
        $('#activebuttonemployee_document').show();
        var url='/user/document/getallSoft';
        $('#employee_document_table').DataTable().clear().destroy();
        employee_document_data(url);
    });
    $('#activebuttonemployee_document').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        $('#export_excel_employee_document_btn').show();
        $('#export_world_employee_document_btn').show();
        $('#export_pdf_employee_document_btn').show();
        $('.active_badge').html('0');
        $('.expiry_badge').html('0');
        $('#export_excel_employee_document').val(1);
        $('#export_world_employee_document').val(1);
        $('#export_world_pdf').val(1);
        $('.employee_document_add').show();
        $('#selectedactivebuttonemployee_document').hide();
        $('#deleteselectedemployee_document').show();
        $('#deleteselectedemployee_documentSoft').hide();
        $('#restorebuttonemployee_document').show();
        $('#activebuttonemployee_document').hide();
        var url='/user/document/getall';
        $('#employee_document_table').DataTable().clear().destroy();
        employee_document_data(url);
    });
});
function getemployee_documentdata(id)
{
    $("#form_name_required_message_edit").html('');
    $("#form_doctype_required_message_edit").html('');
    $("#form_file_required_message_edit").html('');
    $("input").attr('style','');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type : 'POST',
        url : '/user/document/edit',
        data:{'id':id},
        success:function(response)
        {
            $('#id_employee_document').val(response.document.id);
            $('#name_employee_document').val(response.document.name);
            var filurl=$("#edit_base_url").val()+'/public/employee_documents/'+response.document.file;
            var fil_html='<div class="spinner-border text-success" id="bulk_upload_img_loader"></div><input type="file" name="file" id="input-file-now" data-default-file="'+filurl+'" class="file_upload_create" />';
            $('.file_upload_wrapper_edit').html(fil_html);
            setTimeout(function (){
                $('.file_upload_create').file_upload();
            },1000);


            var type_array=[{'id':'d','name':'Document'},{'id':'p','name':'Photo'}];
            var html='';
            for (var i = type_array.length - 1; i >= 0; i--) {
                if(type_array[i].id ==  response.document.type )
                {
                    html+='<option selected value="'+type_array[i].id+'">'+type_array[i].name+'</option>';
                }
                else
                {
                    html+='<option value="'+type_array[i].id+'">'+type_array[i].name+'</option>';
                }
            }
            $('#type_employee_document').empty();
            $('#type_employee_document').html(html)
            html='';
            console.log("Document : "+response.document.doc_type_id);
            for (var i = response.doc_types.length - 1; i >= 0; i--) {
                            console.log(response.doc_types[i].doc_type);
                console.log("Doc ID : "+JSON.stringify(response.doc_types[i].doc_type));
                if (response.doc_types[i].doc_type!==null){
                    if(jQuery.inArray(response.doc_types[i].id, response.user_doc_type) !== -1)
                    {
                        var match='(required)';
                    }else{
                        var match='';
                    }
                    if(response.doc_types[i].id ==  response.document.doc_type_id )
                    {
                        html+='<option selected  value="'+response.doc_types[i].id+'">'+response.doc_types[i].name+' '+match+'</option>';
                    }
                    else
                    {
                        html+='<option value="'+response.doc_types[i].id+'">'+response.doc_types[i].name+' '+match+'</option>';
                    }
                }
            }
            $('#doc_type_id_employee_document').empty();
            $('#doc_type_id_employee_document').html(html)
            $('.body_notes_edit').summernote('code',response.document.notes);
            $('.issue_date').val(response.document.issue);
            $('.expiry_date').val(response.document.expiry);
            $('#modelemployeedocumentEdit').modal('show');

        }
    });


}
function restoreemployee_documentdata(id)
{
    var url='/user/document/active';
    swal({
        title: "Are you sure?",
        text: "Selected option with be active ",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
     },
     function(isConfirm){

       if (isConfirm){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
             $.ajax({
                        type: "POST",
                        url: url,
                        data: {'id':id},
                        success: function(response)
                        {
                            if(response.flag==1)
                            {
                                var url='/user/document/getall';
                            }
                            else
                            {
                                var url='/user/document/getallSoft';
                            }
                            $('#employee_document_table').DataTable().clear().destroy();
                            employee_document_data(url);
                            swal.close();
                            toastr["success"](response.message);
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
        } else {
          swal("Cancelled", "Your Record is safe", "error");

        }
     });
 }
 function delete_mutilpleData(url,selectedrows)
 {
     if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
         console.log(selectedrows);

        if (selectedrows && selectedrows.length) {
       // not empty
            swal({
                title: "Are you sure?",
                text: "Selected option will be inactive and can be Active again by the admin only!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
             },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:url,
                        data: {'ids':selectedrows},
                        success: function(response)
                        {

                            toastr["success"](response.message);
                            if(response.flag==1)
                            {
                                var url='/user/document/getall';
                            }
                            else
                            {
                                var url='/user/document/getallSoft';
                            }
                            $('#employee_document_table').DataTable().clear().destroy();
                            employee_document_data(url);
                            swal.close();

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                  swal("Cancelled", "Your Record is safe", "error");

                }
            });
         }
        else
        {
       // empty
            return toastr["error"]("Select at least one record to delete!");
        }
 }
 function delete_data_employee_document(id,url)
 {
    swal({
        title: "Are you sure?",
        text: "Selected option with be Deleted ",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
     },
     function(isConfirm){

       if (isConfirm){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
             $.ajax({
                        type: "POST",
                        url: url,
                        data: {'id':id},
                        success: function(response)
                        {
                            if(response.flag==1)
                            {
                                var url='/user/document/getall';
                            }
                            else
                            {
                                var url='/user/document/getallSoft';
                            }
                            $('#employee_document_table').DataTable().clear().destroy();
                            employee_document_data(url);
                            swal.close();
                            toastr["success"](response.message);
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
        } else {
          swal("Cancelled", "Your Record is safe", "error");

        }
     });
 }
function deletesoftemployee_documentdata(id)
{
    var url='/user/document/softdelete';
    delete_data_employee_document(id,url);

}
function deleteemployee_documentdata(id)
 {
    var url='/user/document/delete';
    delete_data_employee_document(id,url);
 }
