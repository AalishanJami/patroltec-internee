var selectedrows=[];
$(document).ready(function() {
    $('#selectedactivebuttonclient').hide();
    $('body').on('click','#client_checkbox_all',function() {
        $('.selectedrowclient').not(this).prop('checked', this.checked);
        $('.selectedrowclient').each(function(key,value) {
            var id=$(this).attr('id');
            if(id)
            {
                id=id.split('client_checked');
                row_id='#client_tr'+id[1];
                if($(this).is(":checked"))
                {
                    if(key == 0)
                    {
                        selectedrows=[];
                    }
                    if(id[1] != '_checkbox_all')
                    {
                        selectedrows.push(id[1]);
                    }
                    $('.client_export').val(JSON.stringify(selectedrows));
                    $(row_id).addClass('selected');
                }
                else
                {
                    $('.client_export').val(1);
                    selectedrows=[];
                    $(row_id).removeClass('selected');
                }
            }
        });

    });

    $('body').on('click', '.selectedrowclient', function() {
        var id=$(this).attr('id');
        id=id.split('client_checked');
        row_id='#client_tr'+id[1];
        if($(this).is(":checked"))
        {
            selectedrows.push(id[1]);
            $('.client_export').val(JSON.stringify(selectedrows));
            $(row_id).addClass('selected');
        }else
        {
            if(selectedrows.includes(id[1]))
            {
                selectedrows.splice( selectedrows.indexOf(id[1]),1);
                $('.client_export').val(JSON.stringify(selectedrows));
                $(row_id).removeClass('selected');
            }
        }
    });

    $('#selectedactivebuttonclient').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        var url='/client/active/multiple';
        if (selectedrows && selectedrows.length) {
            // not empty
            swal({
                    title: "Are you sure?",
                    text: "Selected option will be active  again ..!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Yes, I am sure!',
                    cancelButtonText: "No, cancel it!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm){

                    if (isConfirm){
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $.ajax({
                            type: "POST",
                            url:url,
                            data: {'ids':selectedrows},
                            success: function(response)
                            {
                                console.log(response);
                                $('#client_table').DataTable().clear().destroy();
                                toastr["success"](response.message);
                                var url='/client/getallSoft';
                                client_data(url);
                                swal.close();

                            },
                            error: function (error) {
                                console.log(error);
                            }
                        });

                    } else {
                        swal("Cancelled", "Your Record is safe", "error");

                    }
                });
        }
        else
        {
            // empty
            return toastr["error"]("Select at least one record to delete!");
        }
    });
    $('#deleteselectedclient').click(function(){
        var url='/client/deleteMultiple';
        delete_mutilpleData_client(url,selectedrows);
    });
    $('#deleteselectedclientSoft').click(function(){
        var url='/client/deleteMultipleSOft';
        delete_mutilpleData_client(url,selectedrows);
    });

    $('.add-client').on('click', 'i', () => {
        $.ajax({
            type: 'GET',
            url: '/client/store',
            success: function(response)
            {
                var html='';
                html+= '<tr id="client_tr'+response.data.id+'" role="row" class="hide odd">';
                html+='<td>';
                if (($("#client_checkbox").val() =='1')){
                    html+='<div class="form-check"><input type="checkbox" class="form-check-input selectedrowclient" id="client_checked'+response.data.id+'"><label class="form-check-label" for="client_checked'+response.data.id+'""></label></div>';
                }
                html+='</td>';
                //html+='<td class="pt-3-half edit_inline_client" id="title'+response.data.id+'"   contenteditable="true"></td>';
                if (($("#client_firstname").val() =='1')){
                    var f_edit=($("#client_firstname_create").val() =='1' && $("#create_client").val() =='1') ? 'true':'false';
                    html+='<td class="pt-3-half edit_inline_client" onkeypress="editSelectedRow('+"client_tr"+response.data.id+')" data-id="'+response.data.id+'" id="first_name'+response.data.id+'"   contenteditable="'+f_edit+'"></td>';
                }
                if (($("#client_surname").val() =='1')){
                    var s_edit=($("#client_surname_create").val() =='1' && $("#create_client").val() =='1') ? 'true':'false';
                    html+='<td class="pt-3-half edit_inline_client" data-id="'+response.data.id+'" onkeypress="editSelectedRow('+"client_tr"+response.data.id+')" id="surname'+response.data.id+'"   contenteditable="'+s_edit+'"></td>';
                }
                if (($("#client_phone").val() =='1')){
                    var ph_edit=($("#client_phone_create").val() =='1' && $("#create_client").val() =='1') ? 'true':'false';
                    html+='<td class="pt-3-half edit_inline_client" data-id="'+response.data.id+'" onkeypress="editSelectedRow('+"client_tr"+response.data.id+')" id="phone_number'+response.data.id+'"   contenteditable="'+ph_edit+'"></td>';
                }
                if (($("#client_email").val() =='1')){
                    var e_edit=($("#client_email_create").val() =='1' && $("#create_client").val() =='1') ? 'true':'false';
                    html+='<td class="pt-3-half edit_inline_client" data-id="'+response.data.id+'" onkeypress="editSelectedRow('+"client_tr"+response.data.id+')" id="email'+response.data.id+'"   contenteditable="'+e_edit+'"></td>';
                }
                if (($("#client_address").val() =='1')){
                    var a_edit=($("#client_address_create").val() =='1' && $("#create_client").val() =='1') ? 'true':'false';
                    html+='<td class="pt-3-half edit_inline_client" data-id="'+response.data.id+'" onkeypress="editSelectedRow('+"client_tr"+response.data.id+')" id="address'+response.data.id+'"   contenteditable="'+a_edit+'"></td>';
                }
                html+='<td>';
                if (($("#delete_client").val() =='1')){
                    html+='<a type="button" class="btn deleteclientdata'+response.data.id+' btn-danger btn-xs my-0 waves-effect waves-light delete-btn'+response.data.id+'" onclick="deleteclientmodaldata('+response.data.id+')"><i class="fas fa-trash"></i></a>';
                }
                if (($("#edit_client").val() =='1')){
                    html+='<a type="button" class="btn btn-primary btn-xs all_action_btn_margin my-0 waves-effect waves-light saveclientdata client_show_button_'+response.data.id+' show_tick_btn'+response.data.id+'" style="display: none;" id="'+response.data.id+'"><i class="fas fa-check"></i></a>';
                }
                html+='</td>';
                html+='</tr>';
                $('#table-client').find('table').prepend(html);
            },
            error: function (error) {
                console.log(error);
            }
        });


    });
    $('body').on('click', '.saveclientdata', function() {
        var company_id=$('#companies_selected_nav').children("option:selected").val();
        var tr_id=$(this).attr('id');
        var data={
            'id':tr_id,
            'title':$('#title'+tr_id).html(),
            'first_name':$('#first_name'+tr_id).html(),
            'surname':$('#surname'+tr_id).html(),
            'phone_number':$('#phone_number'+tr_id).html(),
            'email':$('#email'+tr_id).html(),
            'address':$('#address'+tr_id).html(),
            'company_id':company_id,

        };
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url:'/client/update',
            data:data,
            success: function(response)
            {
                if(response.error)
                {
                    toastr["error"](response.error);
                    $("#client_tr"+tr_id).attr('style','background-color:#fc685f !important');

                }
                else
                {
                    console.log(response);
                    toastr["success"](response.message);
                    save_and_delete_btn_toggle(tr_id);
                    if(response.flag==1)
                    {
                        $("#client_tr"+tr_id).attr('style','background-color:#90ee90 !important');
                        // var url='/client/getall';
                    }
                    else
                    {
                        $("#client_tr"+tr_id).attr('style','background-color:#ff3547 !important');

                        // var url='/client/getallsoft';
                    }
                    $(".client_show_button_"+tr_id).hide();
                    $('.deleteclientdata'+tr_id).show();
                    // $('#client_table').DataTable().clear().destroy();
                    // client_data(url);

                }
            }

        });

        // console.log(data);

    });

    $('#restorebuttonclient').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        $('#restorebuttonclient').hide();
        $('#deleteselectedclient').hide();
        $('#selectedactivebuttonclient').show();
        $('#deleteselectedclientSoft').show();
        $('#export_excel_client').hide();
        $('#export_world_client').hide();
        selectedrows=[];
        $('#export_pdf_client').hide();
        $('.add-client').hide();
        $('#activebuttonclient').show();
        var url='/client/getallSoft';
        $('#client_table').DataTable().clear().destroy();
        client_data(url);
    });
    $('#activebuttonclient').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        $('#export_excel_client').show();
        $('#export_world_client').show();
        $('#export_pdf_client').show();
        $('.add-client').show();
        $('#selectedactivebuttonclient').hide();
        selectedrows=[];
        $('#deleteselectedclient').show();
        $('#deleteselectedclientSoft').hide();
        $('#restorebuttonclient').show();
        $('#activebuttonclient').hide();
        var url='/client/getall';
        $('#client_table').DataTable().clear().destroy();
        client_data(url);
    });



});
function restoreclientdata(id)
{
    var url='/client/active';
    swal({
            title: "Are you sure?",
            text: "Selected option with be active ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){

            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {'id':id},
                    success: function(response)
                    {

                        var url='/client/getallSoft';

                        $('#client_table').DataTable().clear().destroy();
                        client_data(url);
                        swal.close();
                        toastr["success"](response.message);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            } else {
                swal("Cancelled", "Your Record is safe", "error");

            }
        });
}
function delete_mutilpleData_client(url,selectedrows)
{
    if( $('.edit_cms_disable').css('display') == 'none' ) {
        console.log('Editer mode On Please change your mode');
        return 0;
    }
    if (selectedrows && selectedrows.length) {
        // not empty
        swal({
                title: "Are you sure?",
                text: "Selected option will be inactive and can be Active again by the admin only!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:url,
                        data: {'ids':selectedrows,'flag':$('#flagClient').val()},
                        success: function(response)
                        {
                            console.log(response);
                            toastr["success"](response.message);
                            if($('#main_client').val() == 1)
                            if(response.flag==1)
                            {

                                if($('#main_client').val() == 1)
                                {

                                    var url='/client/getAllClientMain';
                                }
                                else
                                {
                                    var url='/client/getall';
                                }
                            }

                            else
                            {
                                var url='/client/getallSoft';
                            }
                            $('#client_table').DataTable().clear().destroy();
                            client_data(url);
                            swal.close();

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                    swal("Cancelled", "Your Record is safe", "error");

                }
            });
    }
    else
    {
        // empty
        return toastr["error"]("Select at least one record to delete!");
    }
}
function delete_data_client(id,url)
{
    swal({
            title: "Are you sure?",
            text: "Selected option with be Deleted ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){

            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {'id':id},
                    success: function(response)
                    {
                        if(response.flag==1)
                        {

                            if($('#main_client').val() == 1)
                            {

                                var url='/client/getAllClientMain';
                            }
                            else
                            {
                                var url='/client/getall';
                            }
                        }
                        else
                        {
                            var url='/client/getallSoft';
                        }
                        $('#client_table').DataTable().clear().destroy();
                        client_data(url);
                        swal.close();
                        toastr["success"](response.message);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            } else {
                swal("Cancelled", "Your Record is safe", "error");

            }
        });
}
function deletesoftclientdata(id)
{
    var url='/client/softdelete';
    delete_data_client(id,url);

}
function deleteclientdata(id)
{
    var url='/client/delete';
    delete_data_client(id,url);
}

function deleteclientmodaldata(id)
{
    var url='/client/delete';
    delete_data_client(id,url);
}

function selectedRowClient(id)
{
    console.log(id);
    if(selectedrows.includes(id))
    {
        selectedrows.splice( selectedrows.indexOf(id), 1 );

        $('#client_main_table tr#'+id).removeClass('selected');
    }
    else
    {
        selectedrows.push(id);
        $('#client_main_table tr#'+id).addClass('selected');
    }

}

$(document).ready(function(){
    $('#restore_button_client').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        $('#restore_button_client').hide();
        $('#export_excel_client').val(2);
        $('#export_word_client').val(2);
        $('#flagClient').val(2);
        $('#show_active_button_client').show();
        selectedrows=[];
        $('#selectedactivebuttonclient').show();
        $('.export_excel_client').hide();
        $('.export_word_client').hide();
        $('.export_pdf_client').hide();
        $('.add-client').hide();
        $('#activebuttonclient').show();

        $('#client_table').DataTable().clear().destroy();
        var url='/client/getallSoft';
        client_data(url);
    });
    $('#show_active_button_client').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        $('#show_active_button_client').hide();
        $('#export_excel_client').val(2);
        $('#export_pdf_client').val(2);
        $('#flagClient').val(1);
        $('#restore_button_client').show();
        $('.export_excel_client').show();
        $('.export_word_client').show();
        $('.export_pdf_client').show();
        $('.add-client').show();
        $('#selectedactivebuttonclient').hide();
        selectedrows=[];
        if($('#main_client').val() == 1)
        {

            var url='/client/getAllClientMain';
        }
        else
        {
            var url='/client/getall';
        }
        $('#client_table').DataTable().clear().destroy();
        // var url='/client/getall';
        client_data(url);
    });
});

function restoreClient(id)
{
    swal({
            title: "Are you sure?",
            text: "Selected Location will be Active again!",
            type: "warning",
            showCloseButton: true,
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            customClass: "confirm_class",
            closeOnCancel: false
        },
        function(isConfirm){
            if( $('.edit_cms_disable').css('display') != 'none' ) {
                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:'/client/restore',
                        data: {'id':id},
                        success: function(response)
                        {
                            $('#client_table').DataTable().ajax.reload();
                            $('#client_main_table').DataTable().ajax.reload();
                            swal.close()
                            toastr["success"]("Location Activated Successfully!");

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
                }
                else {
                    swal.close();
                    // swal("Cancelled", "Your Record is safe", "error");
                    // e.preventDefault();
                }
            }
        });
}



