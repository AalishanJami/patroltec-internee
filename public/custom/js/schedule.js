$(document).ready(function() {
    // schedule_form

    $('body').on('click','.modalSchedule_btn',function() {
        setTimeout(function(){
            // $("#location_breakdown_id_create").prepend('<option value="" disabled selected>Please Select</option>');
            $("#schedule_form").trigger("reset");
            document.getElementById("schedule_form").reset();

            var schdule_project=$("#schdule_project").children('option:selected').val();
            if (schdule_project=='please'){
                $("#schdule_project").children('option:selected').attr('disabled',true);
            }

            var schedule_user=$("#schedule_user_id").children('option:selected').val();
            if (schedule_user=='please'){
                $("#schedule_user_id").children('option:selected').attr('disabled',true);
            }
            $(".error_message").html('');
            $("input").attr('style','');

        },100);
    });

    var selectedrows=[];
    $('body').on('click','#schedule_checkbox_all',function() {
        $('input:checkbox').not(this).prop('checked', this.checked);
        $('input[type="checkbox"]').each(function(key,value) {
            var id=$(this).attr('id');
            console.log(id);
            if(id)
            {
                id=id.split('schedule');
                row_id='#schedule_tr'+id[1];
                if($(this).is(":checked"))
                {
                    if(key == 0)
                    {
                        selectedrows=[];
                    }
                    if(id[1] != '_checkbox_all')
                    {
                        selectedrows.push(id[1]);
                    }
                    $('.schedule_export').val(JSON.stringify(selectedrows));
                    $(row_id).addClass('selected');
                }
                else
                {
                    $('.schedule_export').val(1);
                    selectedrows=[];
                    $(row_id).removeClass('selected');
                }
            }
        });
    });

    $('body').on('click', '.selectedrowschedule', function() {
        var id=$(this).attr('id');
        id=id.split('schedule');
        row_id='#schedule_tr'+id[1];
        if($(this).is(":checked"))
        {
            selectedrows.push(id[1]);
            $('.scoring_export').val(JSON.stringify(selectedrows));
            $(row_id).addClass('selected');
        }else
        {
            if(selectedrows.includes(id[1]))
            {
                selectedrows.splice( selectedrows.indexOf(id[1]),1);
                $('.scoring_export').val(JSON.stringify(selectedrows));
                $(row_id).removeClass('selected');
            }
        }
    });

    $('body').on('click', '.task_listing_close', function() {
        $.ajax({
            type: "GET",
            url: '/schedule/tasktype/listing/',
            success: function(response)
            {
                console.log(response.task_type);
                // console.log(response.location);
                var html='';

                html+='<select class="mdb-select-custom-tasktye" name="task_type_id" >';
                html+='<option value="" selected disabled>Please Select</option>';
                for (var i = response.task_type.length - 1; i >= 0; i--) {
                    response.task_type[i]
                    html+='<option value="'+response.task_type[i].id+'">'+response.task_type[i].name+'</option>';
                }

                html+='</select> ';
                $('.task_type_listing').empty();
                console.log(html, "html");
                $('.task_type_listing').html(html);
                $('.mdb-select-custom-tasktye').materialSelect();
                $('select.mdb-select-custom-tasktye').hide();

            },
            error: function (error) {
                console.log(error);
            }
        });
    });

    $('body').on('change','.location-dropdown',function() {

        var schdule_project=$(this).val();
        console.log(schdule_project);
        if(schdule_project!='') {

            $.ajax({
                type: "GET",
                url: '/schedule/project/' + schdule_project,
                success: function (response) {

                    console.log(response.location);
                    var html = '';
                    html += '<select name="location_breakdown_id[]" class="mdb-select-custom-location colorful-select dropdown-primary md-form" multiple searchable="Search here..">';

                    html += '<option value="" disabled selected>Please Select</option>';
                    for (var i = response.location.length - 1; i >= 0; i--) {
                        response.location[i]
                        html += '<option value="' + response.location[i].id + '">' + response.location[i].name + '</option>';
                    }

                    html += '</select> ';
                    if (response.form_type == "t")
                        html += '<label class="mdb-main-label">QR List</label>';
                    if (response.form_type == "e")
                        html += '<label class="mdb-main-label">Equipment List</label>';

                    $('.schedule_qr_id').empty();
                    console.log(html, "html");
                    $('.schedule_qr_id').html(html);
                    $('.mdb-select-custom-location').materialSelect();
                    $('select.mdb-select-custom-location').hide();

                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
    });

    $('#deleteselectedschedule').click(function(){
        var url='/schedule/delete/multiple';
        console.log(selectedrows);
        delete_mutilpleDataschedule(url,selectedrows);
    });

    $('#deleteselectedscheduleSoft').click(function(){
        var url='/schedule/delete/soft/multiple';
        delete_mutilpleDataschedule(url,selectedrows);
    });
    $('body').on('click', '#create_schedule', function(event)
    {
        var company_id=$('#companies_selected_nav').children("option:selected").val();
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        $('.error_message').html('');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        event.preventDefault();
        var data=$('#schedule_form').serializeArray();
        var obj={'name':'company_id',
            'value':company_id,
        };
        data.push(obj);
        console.log(data);
        $.ajax({
            type: 'POST',
            url: '/schedule/store',
            data: data,
            success: function(response)
            {
                if (response.status =='validation'){

                    console.log("Errors : "+JSON.stringify(response.error));

                    $("#schdule_project_message").html(response.error.location_id);
                    if(response.error.location_id)
                    {
                        $("[data-activates=select-options-schdule_project]").attr('style','border-bottom:1px solid #e3342f');
                    }
                    $("#location_breakdown_id").html(response.error.location_breakdown_id);
                    if(response.error.location_breakdown_id)
                    {
                        $("[data-activates=select-options-location_breakdown_id_create]").attr('style','border-bottom:1px solid #e3342f');
                    }
                    $("#start_date_er").html(response.error.start_date);
                    if(response.error.start_date)
                    {
                        $("#start_date_create").attr('style','border-bottom:1px solid #e3342f');
                    }
                    $("#time_er").html(response.error.time);
                    if(response.error.time)
                    {
                        $("#input_starttime_create").attr('style','border-bottom:1px solid #e3342f');
                    }
                    $("#end_date_er").html(response.error.end_date);
                    if(response.error.end_date)
                    {
                        $("#end_date_create").attr('style','border-bottom:1px solid #e3342f');
                    }
                    $("#pre_time_mins_er").html(response.error.pre_time_mins);
                    if(response.error.pre_time_mins)
                    {
                        $(".pre_time_mins").attr('style','border-bottom:1px solid #e3342f');
                    }
                    $("#post_time_mins_er").html(response.error.post_time_mins);
                    if(response.error.post_time_mins)
                    {
                        $(".post_time_mins").attr('style','border-bottom:1px solid #e3342f');
                    }
                    $("#input_starttime_er").html(response.error.input_starttime);
                    if(response.error.input_starttime)
                    {
                        $("#input_starttime").attr('style','border-bottom:1px solid #e3342f');
                    }
                    $("#frequency_error_msg").html(response.error.frequency);
                    return 0;
                }
                toastr["success"](response.message);
                console.log(response);
                $('#schedule_table_static').DataTable().clear().destroy();
                ScheduleAppend();
                $('#modalSchedule').modal('hide');
            },
            error: function (error) {
                console.log(error);
            }
        });

    });

    $('body').on('click', '#edit_schedule', function()
    {
        var company_id=$('#companies_selected_nav').children("option:selected").val();
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        event.preventDefault();
        var data=$('#schedule_form_edit').serializeArray();
        var obj={'name':'company_id',
            'value':company_id,
        };
        data.push(obj);
        console.log(data);
        $.ajax({
            type: 'POST',
            url: '/schedule/update',
            data: data,
            success: function(response)
            {

                if (response.status =='validation'){
                    console.log(response.error);
                    if (response.error.location_id){
                        $("[data-activates=select-options-schdule_project_edit]").attr('style','border-bottom:1px solid #e3342f !important');
                    }
                    if(response.error.location_breakdown_id)
                    {
                        console.log(response.error.location_breakdown_id);
                        $("#location_breakdown_id_edit_msg").html(response.error.location_breakdown_id);
                        // $("[data-activates=select-options-schdule_project]").attr('style','border-bottom:1px solid #e3342f');
                    }

                    if (response.error.start_date){
                        $("#date-picker-example").attr('style','border-bottom:1px solid #e3342f !important');
                    }

                    if (response.error.time){
                        $("#input_starttime").attr('style','border-bottom:1px solid #e3342f !important');
                    }

                    if (response.error.end_date){
                        $("#date-picker-example2").attr('style','border-bottom:1px solid #e3342f !important');
                    }

                    if (response.error.pre_time_mins){
                        $(".pre_time_mins").attr('style','border-bottom:1px solid #e3342f !important');
                    }

                    if (response.error.post_time_mins){
                        $(".post_time_mins").attr('style','border-bottom:1px solid #e3342f !important');
                    }
                    if (response.error.post_time_mins){
                        $(".post_time_mins").attr('style','border-bottom:1px solid #e3342f !important');
                    }
                    if (response.error.input_starttime){
                        $(".inputStarttime").attr('style','border-bottom:1px solid #e3342f !important');
                    }
                    console.log(response.error.location_breakdown_id);
                    $("#location_breakdown_id_edit_msg").html(response.error.location_breakdown_id);
                    $("#project_name_edit").html(response.error.location_id);
                    $("#start_date_edit").html(response.error.start_date);
                    $("#time_edit").html(response.error.time);
                    $("#end_date_edit").html(response.error.end_date);
                    $("#pre_time_mins_edit").html(response.error.pre_time_mins);
                    $("#post_time_mins_edit").html(response.error.post_time_mins);
                    $("#frequency_error_msg_edit").html(response.error.frequency);
                    return 0;
                }

                toastr["success"](response.message);
                $('#schedule_table_static').DataTable().clear().destroy();
                ScheduleAppend();
                $('#modalEditSchedule').modal('hide');
            },
            error: function (error) {
                console.log(error);
            }
        });

    });

    $('#restorebuttonschedule').click(function(){

        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        $('#restorebuttonschedule').hide();
        $('#deleteselectedschedule').hide();
        $('#selectedactivebuttonschedule').show();
        $('#deleteselectedscheduleSoft').show();
        $('#export_excel_schedule').hide();
        $('#export_world_schedule').hide();
        $('#export_pdf_schedule').hide();
        selectedrows=[];
        $('.schedule-table').hide();
        $('#activebuttonschedule').show();
        $('#schedule_table_static').DataTable().clear().destroy();
        ScheduleSoftAppend();
    });
    $('#activebuttonschedule').click(function(){
        $('#export_excel_schedule').show();
        $('#export_world_schedule').show();
        $('#export_pdf_schedule').show();
        $('.schedule-table').show();
        selectedrows=[];
        $('#selectedactivebuttonschedule').hide();
        $('#deleteselectedschedule').show();
        $('#deleteselectedscheduleSoft').hide();
        $('#restorebuttonschedule').show();
        $('#activebuttonschedule').hide();
        var url='/schedule/getall';
        $('#schedule_table_static').DataTable().clear().destroy();
        ScheduleAppend();
    });
});
function getscheduledata(id)
{
    $(".error_message").html('');
    $("input").attr('style','');
    console.log(id);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type : 'POST',
        url : '/schedule/edit',
        data:{'id':id},
        success:function(data)
        {

            console.log("Data : "+JSON.stringify(data.data));
            $('#schedule_id').val(data.data.id);
            var selected_tags_arr = new Array();
            var selected_tags = data.data.user_id;
            if(selected_tags !==null){
                selected_tags_arr = selected_tags.split(",");
                $('#schedule_user_id_edit option').each(function (){
                    var option_val = this.value;
                    for (var i in selected_tags_arr) {
                        var selected_tags_val=selected_tags_arr[i].replace('"','');
                        selected_tags_val=selected_tags_val.replace('"','');
                        selected_tags_val=selected_tags_val.replace('[','');
                        selected_tags_val=selected_tags_val.replace(']','');
                        if(selected_tags_val == option_val){
                            $("#schedule_user_id_edit option[value='" + this.value + "']").attr("selected", true);
                        }
                    }
                });
            }

            $('.pre_time_mins_edit').val(data.data.pre_time_mins);
            $('.post_time_mins_edit').val(data.data.post_time_mins);
            $('#repeat').val(data.data.repeat);
            $('#repeat_min_gap').val(data.data.repeat_min_gap);
            $('#signature').val(data.data.signature);
            var options = {year: 'numeric', month: 'long', day: 'numeric' };
            var start_date_format  = new Date(data.start_date);
            var start_date=start_date_format.toLocaleDateString("en-US", options);

            $('.start_date_Edit').val(start_date);

            $('.time_Edit').val(data.data.time);

            var end_date_format  = new Date(data.data.end_date);
            var end_date=end_date_format.toLocaleDateString("en-US", options);
            $('.end_date_Edit').val(end_date);
            today  = new Date(data.data.recycle_jobs_after);
            var recycle_jobs_after=today.toLocaleDateString("en-US", options);
            $('.recycle_jobs_after_Edit').attr('placeholder',recycle_jobs_after);

            $('.max_display_edit').val(data.data.max_display);

            var pre_time_select="";
            if (data.data.pre_time_select =='1'){
                pre_time_select+="<option selected value='1'>Min</option>";
            }else{
                pre_time_select+="<option value='1'>Min</option>";
            }
            if (data.data.pre_time_select =='2'){
                pre_time_select+="<option selected value='2'>Hrs</option>";
            }else{
                pre_time_select+="<option value='2'>Hrs</option>";
            }
            if (data.data.pre_time_select =='3'){
                pre_time_select+="<option selected value='3'>Days</option>";
            }else{
                pre_time_select+="<option value='3'>Days</option>";
            }
            if (data.data.pre_time_select =='4'){
                pre_time_select+="<option selected value='4'>Week</option>";
            }else{
                pre_time_select+="<option value='4'>Week</option>";
            }

            $("#pre_time_select_edit").html(pre_time_select);


            var post_time_select="";
            if (data.data.post_time_select =='1'){
                post_time_select+="<option selected value='1'>Min</option>";
            }else{
                post_time_select+="<option value='1'>Min</option>";
            }
            if (data.data.post_time_select =='2'){
                post_time_select+="<option selected value='2'>Hrs</option>";
            }else{
                post_time_select+="<option value='2'>Hrs</option>";
            }
            if (data.data.post_time_select =='3'){
                post_time_select+="<option selected value='3'>Days</option>";
            }else{
                post_time_select+="<option value='3'>Days</option>";
            }
            if (data.data.post_time_select =='4'){
                post_time_select+="<option selected value='4'>Week</option>";
            }else{
                post_time_select+="<option value='4'>Week</option>";
            }

            $("#post_time_select_edit").html(post_time_select);

            var frequency_limit="";
            var frequency_type="<option value=''>Please Select</option>";
            if (data.data.week_limit!==null){
                frequency_type+="<option selected value='4'>Week</option>";
                frequency_limit+=data.data.week_limit;
            }else{
                frequency_type+="<option value='4'>Week</option>";
            }
            if (data.data.month_limit!==null){
                frequency_type+="<option selected value='5'>Month</option>";
                frequency_limit+=data.data.month_limit;
            }else{
                frequency_type+="<option value='5'>Month</option>";
            }
            if (data.data.year_limit!==null){
                frequency_type+="<option selected value='6'>Year</option>";
                frequency_limit+=data.data.year_limit;
            }else{
                frequency_type+="<option value='6'>Year</option>";
            }

            $("#frequency_type").html(frequency_type);
            $("#frequency_limit_edit").val(frequency_limit);

            if(data.data.do_not_schedule == 1)
            {
                $('#Schedule1').prop('checked',true);
            }
            if(data.data.mon == 1)
            {
                $('#monedit1').prop('checked',true);
            }
            if(data.data.tue == 1)
            {
                $('#tueedit3').prop('checked',true);
            }
            if(data.data.wed == 1)
            {
                $('#wededit1').prop('checked',true);
            }
            if(data.data.thu == 1)
            {
                $('#thuedit1').prop('checked',true);
            }
            if(data.data.fri == 1)
            {
                $('#friedit1').prop('checked',true);
            }
            if(data.data.sat == 1)
            {
                $('#satedit1').prop('checked',true);
            }
            if(data.data.sun == 1)
            {
                $('#sunedit1').prop('checked',true);
            }
            if(data.data.bank == 1)
            {
                $('#bankedit1').prop('checked',true);
            }
            var task_type_html='';
            task_type_html+='<select class="mdb-select-edit location-dropdown" name="task_type_id">';
            task_type_html+='<option value="" disabled>Please Select</option>';
            for (var i = data.task_type.length - 1; i >= 0; i--) {
                if(data.task_type[i].id == data.data.task_type_id)
                {
                    task_type_html+='<option selected value="'+data.task_type[i].id+'">'+data.task_type[i].name+'</option>';
                }
                else
                {
                    task_type_html+='<option value="'+data.task_type[i].id+'">'+data.task_type[i].name+'</option>';

                }
            }
            task_type_html+='</select>';
            $('#task_type_id_edit').html(task_type_html);

            console.log(data.data);

            var location_html='';
            location_html+=' <span>Project Name</span>';
            location_html+='<select class="mdb-select-edit location-dropdown" id="schdule_project_edit" name="location_id">';
            // location_html+='<option value="" disabled>Please Select</option>';
            for (var i = data.locations.length - 1; i >= 0; i--) {
                if (data.data.schedule_task_location.location_id !==null){
                    if(data.locations[i].id == data.data.schedule_task_location.location_id)
                    {
                        location_html+='<option selected value="'+data.locations[i].id+'">'+data.locations[i].name+'</option>';
                    }
                    else
                    {
                        location_html+='<option value="'+data.locations[i].id+'">'+data.locations[i].name+'</option>';
                    }
                }else{
                    location_html+='<option value="'+data.locations[i].id+'">'+data.locations[i].name+'</option>';
                }

            }
            location_html+='</select>';
            location_html+='<small id="project_name_edit" class="text-danger error_message"></small>';
            $('#location_id_edit').html(location_html);

            var location_breakdown_html='';
            if(data.form_type=="t")
            {

                location_breakdown_html+=' <span>QR List</span>';
            }
            else if(data.form_type=="e")
            {
                location_breakdown_html+=' <span>Equipment List</span>';
            }


            location_breakdown_html+='<select class="mdb-select-edit colorful-select dropdown-primary md-form schedule_qr_id" multiple searchable="Search here.." name="location_breakdown_id[]" id="schedule_qr_id" >';
            location_breakdown_html+='<option value="" disabled>Please Select</option>';

            for (var i = data.location_breakdowns.length - 1; i >= 0; i--) {


                if(data.location_breakdowns[i].id == data.data.schedule_task_location.location_breakdown_id)
                {
                    location_breakdown_html+='<option selected value="'+data.location_breakdowns[i].id+'">'+data.location_breakdowns[i].name+'</option>';
                }
                else
                {
                    location_breakdown_html+='<option value="'+data.location_breakdowns[i].id+'">'+data.location_breakdowns[i].name+'</option>';

                }
            }
            location_breakdown_html+='</select>';
            $('#location_breakdown_id_edit').html(location_breakdown_html);

            setTimeout(function(){
                var selected_tags_arr_task = data.scheduletasklocation;
                $('#schedule_qr_id option').each(function (){
                    var option_val_task = this.value;
                    for (var i in selected_tags_arr_task) {
                        var selected_tags_val_task=selected_tags_arr_task[i].location_breakdown_id;
                        if(selected_tags_val_task == option_val_task){
                            $("#schedule_qr_id option[value='" + selected_tags_val_task + "']").attr("selected", true);
                        }
                    }
                });
            },2000);

            var tag_swipe="";
            if (data.data.tag_swipe =='1'){
                tag_swipe+="<option value='1' selected>Yes</option>";
            }else{
                tag_swipe+="<option value='1'>Yes</option>";
            }
            if (data.data.tag_swipe =='2'){
                tag_swipe+="<option value='2' selected>No</option>";
            }else{
                tag_swipe+="<option value='2'>No</option>";
            }

            $("#tag_swipe_edit").html(tag_swipe);

            $('#modalEditSchedule').modal('show');
            setTimeout(function(){
                $('.mdb-select-edit').materialSelect();
                $('select .mdb-select-edit ,.initialized').hide();
            },1000);
        }
    });
}
function selectedrowschedule(id)
{
    var tr_id=id;
    var flag=$('#schedule'+tr_id).prop('checked');
    console.log(flag);
    if(flag)
    {
        console.log(tr_id);
        console.log(selectedrows);
        if(selectedrows.includes(tr_id))
        {
            selectedrows.splice( selectedrows.indexOf(tr_id), 1 );
            $('#schedule'+tr_id).attr('checked', false );
            $('#schedule_table_static tr#'+tr_id).removeClass('selected');
        }
        else
        {
            selectedrows.push(tr_id);
            console.log(selectedrows);
            $('#schedule'+tr_id).attr('checked', true );
            $('#schedule_table_static tr#'+tr_id).addClass('selected');

        }
    }

}
function restorescheduledata(id)
{
    var url='/schedule/active';
    swal({
            title: "Are you sure?",
            text: "Selected option with be active ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){

            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {'id':id},
                    success: function(response)
                    {
                        $('#schedule_table_static').DataTable().clear().destroy();

                        if(response.flag==1)
                        {
                            ScheduleAppend();
                        }
                        else
                        {
                            ScheduleSoftAppend();
                        }
                        swal.close();
                        toastr["success"](response.message);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            } else {
                swal("Cancelled", "Your Record is safe", "error");

            }
        });
}
function delete_mutilpleDataschedule(url,selectedrows)
{
    if( $('.edit_cms_disable').css('display') == 'none' ) {
        console.log('Editer mode On Please change your mode');
        return 0;
    }
    if (selectedrows && selectedrows.length) {
        // not empty
        swal({
                title: "Are you sure?",
                text: "Selected option will be inactive and can be Active again by the admin only!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:url,
                        data: {'ids':selectedrows},
                        success: function(response)
                        {

                            toastr["success"](response.message);
                            $('#schedule_table_static').DataTable().clear().destroy();
                            if(response.flag==1)
                            {
                                ScheduleAppend();
                            }
                            else
                            {
                                ScheduleSoftAppend();
                            }
                            swal.close();

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                    swal("Cancelled", "Your Record is safe", "error");

                }
            });
    }
    else
    {
        // empty
        return toastr["error"]("Select at least one record to delete!");
    }
}
function delete_data_schedule(id,url)
{
    swal({
            title: "Are you sure?",
            text: "Selected option with be Deleted ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){

            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {'id':id},
                    success: function(response)
                    {
                        $('#schedule_table_static').DataTable().clear().destroy();
                        if(response.flag==1)
                        {
                            ScheduleAppend();
                        }
                        else
                        {
                            ScheduleSoftAppend();
                        }


                        swal.close();
                        toastr["success"](response.message);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            } else {
                swal("Cancelled", "Your Record is safe", "error");

            }
        });
}
function deletesoftscheduledata(id)
{
    selectedrows=[];
    var url='/schedule/softdelete';
    delete_data_schedule(id,url);

}
function deletescheduledata(id)
{
    selectedrows=[];
    console.log(selectedrows);
    var url='/schedule/delete';
    delete_data_schedule(id,url);
}

