$(document).ready(function() {
    var selectedrows=[];
    $('body').on('click','#document_checkbox_all',function() {
        $('input:checkbox').not(this).prop('checked', this.checked);
        $('input[type="checkbox"]').each(function(key,value) {
            var id=$(this).attr('id');
            console.log(id);
            if(id)
            {
                id=id.split('form_file');
                row_id='#form_tr'+id[1];
                if($(this).is(":checked"))
                {
                    if(key == 0)
                    {
                        selectedrows=[];
                    }
                    if(id[1] != '_checkbox_all')
                    {
                        selectedrows.push(id[1]);
                    }
                    $('.form_export').val(JSON.stringify(selectedrows));
                    $(row_id).addClass('selected');
                }
                else
                {
                    $('.form_export').val(1);
                    selectedrows=[];
                    $(row_id).removeClass('selected');
                }
            }
        });
    });

    $('body').on('click', '.selectedrowform', function() {
        var id=$(this).attr('id');
        id=id.split('form_file');
        row_id='#form_tr'+id[1];
        if($(this).is(":checked"))
        {
            selectedrows.push(id[1]);
            $('.form_export').val(JSON.stringify(selectedrows));
            $(row_id).addClass('selected');
        }else
        {
            if(selectedrows.includes(id[1]))
            {
                selectedrows.splice( selectedrows.indexOf(id[1]),1);
                $('.form_export').val(JSON.stringify(selectedrows));
                $(row_id).removeClass('selected');
            }
        }
    });

    $('#selectedactivebuttonform').click(function(){
        var url='/form/active/multiple';
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        if (selectedrows && selectedrows.length) {
            // not empty
            swal({
                    title: "Are you sure?",
                    text: "Selected option will be active  again ..!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Yes, I am sure!',
                    cancelButtonText: "No, cancel it!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm){

                    if (isConfirm){
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $.ajax({
                            type: "POST",
                            url:url,
                            data: {'ids':selectedrows},
                            success: function(response)
                            {

                                toastr["success"](response.message);
                                var url='/form/getallSoft';
                                $('#document').DataTable().clear().destroy();
                                form_data(url);
                                swal.close();

                            },
                            error: function (error) {
                                console.log(error);
                            }
                        });

                    } else {
                        swal("Cancelled", "Your Record is safe", "error");

                    }
                });
        }
        else
        {
            // empty
            return toastr["error"]("Select at least one record to delete!");
        }
    });

    $('#deleteselectedform').click(function(){
        var url='/form/delete/multiple';
        delete_mutilpleDataform(url,selectedrows);
    });

    $('#deleteselectedformSoft').click(function(){
        var url='/form/delete/soft/multiple';
        delete_mutilpleDataform(url,selectedrows);
    });

    /*    $('.add-form').on('click', 'i', () => {
          $.ajax({
                type: 'GET',
                url: '/form/store',
                success: function(response)
                {
                    var html='';
                    html+= '<tr id="form_tr'+response.data.id+'">';
                    html+= '<td class="pt-3-half  edit_inline_form" contenteditable="true" id="ref'+response.data.id+'"></td>';
                    html+= '<td class="pt-3-half  edit_inline_form" contenteditable="true" id="nam'+response.data.id+'"></td>';

                    html+='<td class="pt-3-half edit_inline_form" id="valid_to'+response.data.id+'"   contenteditable="true" >';
                    html+='<input placeholder="Selected date" value="'+response.data.start+'" type="date" id="valid_to'+response.data.id+'"  class="form-control datepicker">';
                    html+'</td>';
                    html+= '<td id="'+response.data.id+'" >';
                    html+='<a href="/document/detail/'+response.data.id+'" class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i></a>';
                    html+='<a href="/version/log/edit" class="btn btn-success btn-sm"><i class="fas fa-eye"></i></a>';
                    html+='<a  data-toggle="modal" data-target="#modelviewpopup" class="btn btn-default btn-sm"><i class="fas fa-angle-double-up"></i></a>';
                    html+='<a href="/view_register" class="btn btn-warning  btn-sm"><i class="fas fa-copy"></i></a>';
                    html+='<a type="button"class="btn btn-danger btn-sm my-0" onclick="deleteDocumentdata('+response.data.id+')"><i class="fas fa-trash"></i></a>';
                    html+='</td>';
                    html+='</tr>';
                    console.log(html);
                    $('#table-form').find('table').append(html);

                },
                error: function (error) {
                    console.log(error);
                }
            });


        });*/

    $('body').on('focusout', '.edit_inline_form', function() {
        var company_id=$('#companies_selected_nav').children("option:selected").val();
        var tr_id=$(this).parent('tr').attr('id');
        tr_id=tr_id.split('form');
        tr_id=tr_id[1];
        var data={
            'id':tr_id,
            'ref':$('#ref'+tr_id).html(),
            'name':$('#name'+tr_id).html(),
            'valid_to':$('#valid_to'+tr_id).val(),
            'company_id':company_id
        };
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url:'/form/update',
            data:data,
            success: function(response)
            {
                if(response.error)
                {
                    toastr["error"](response.error);
                    $("#form"+tr_id).attr('style','background-color:#ff3547 !important');
                }
                else
                {
                    toastr["success"](response.message);
                    if(response.flag==1)
                    {
                        var url='/form/getall';
                        $("#form"+tr_id).attr('style','background-color:#90ee90 !important');
                    }
                    else
                    {
                        var url='/form/getallsoft';
                    }
                    // $('#document').DataTable().clear().destroy();
                    // form_data(url);
                }

            }

        });
    });

    $('#restorebuttonform').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        selectedrows=[];
        $("#export_excel_form_btn").hide();
        $("#export_world_form_btn").hide();
        $("#export_pdf_form_btn").hide();
        $('#restorebuttonform').hide();
        $('#deleteselectedform').hide();
        $('#selectedactivebuttonform').show();
        $('#deleteselectedformSoft').show();
        $('.add-bonus').hide();
        $('#activebuttonform').show();
        var url='/form/getallSoft';
        $('#document').DataTable().clear().destroy();
        form_data(url);
    });
    $('#activebuttonform').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        selectedrows=[];
        $("#export_excel_form_btn").show();
        $("#export_world_form_btn").show();
        $("#export_pdf_form_btn").show();
        $('.add-bonus').show();
        $('#selectedactivebuttonform').hide();
        $('#deleteselectedform').show();
        $('#deleteselectedformSoft').hide();
        $('#restorebuttonform').show();
        $('#activebuttonform').hide();
        var url='/form/getall';
        $('#document').DataTable().clear().destroy();
        form_data(url);
    });



});

function restoreformdata(id)
{
    var url='/form/active';
    swal({
            title: "Are you sure?",
            text: "Selected option with be active ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){

            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {'id':id},
                    success: function(response)
                    {
                        if(response.flag==1)
                        {
                            var url='/form/getall';
                        }
                        else
                        {
                            var url='/form/getallSoft';
                        }
                        $('#document').DataTable().clear().destroy();
                        form_data(url);
                        swal.close();
                        toastr["success"](response.message);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            } else {
                swal("Cancelled", "Your Record is safe", "error");

            }
        });
}
function delete_mutilpleDataform(url,selectedrows)
{
    if( $('.edit_cms_disable').css('display') == 'none' ) {
        console.log('Editer mode On Please change your mode');
        return 0;
    }
    if (selectedrows && selectedrows.length) {
        // not empty
        swal({
                title: "Are you sure?",
                text: "Selected option will be inactive and can be Active again by the admin only!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:url,
                        data: {'ids':selectedrows},
                        success: function(response)
                        {

                            toastr["success"](response.message);
                            if(response.flag==1)
                            {
                                var url='/form/getall';
                            }
                            else
                            {
                                var url='/form/getallSoft';
                            }
                            $('#document').DataTable().clear().destroy();
                            form_data(url);
                            swal.close();

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                    swal("Cancelled", "Your Record is safe", "error");

                }
            });
    }
    else
    {
        // empty
        return toastr["error"]("Select at least one record to delete!");
    }
}
function delete_data_form(id,url,formgroup_id)
{
    swal({
            title: "Are you sure?",
            text: "Selected option with be Deleted ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){

            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {'id':id},
                    success: function(response)
                    {
                        if(response.flag==1)
                        {
                            if(formgroup_id== 0)
                            {
                                var url='/form/getall';

                            }
                            else
                            {
                                var url='/form/get/' + formgroup_id;

                            }
                        }
                        else
                        {
                            var url='/form/getallSoft';
                        }
                        $('#document').DataTable().clear().destroy();
                        form_data(url);
                        swal.close();
                        toastr["success"](response.message);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            } else {
                swal("Cancelled", "Your Record is safe", "error");

            }
        });
}
function deletesoftformdata(id)
{
    selectedrows=[];
    var url='/form/softdelete';
    delete_data_form(id,url,formgroup_id);
}
function viewform_groupdatadelete(id)
{
    var url='/form_group/delete';
    swal({
            title: "Are you sure?",
            text: "Selected option with be Deleted ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){

            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {'id':id},
                    success: function(response)
                    {
                        console.log(response.form_id);
                        if(response.form_id !==null)
                        {
                            var url='/form_group/getall/' + response.form_id;
                        }
                        else
                        {
                            var formgroup_id = 0;
                            var url='/form_group/getall/'+formgroup_id;
                        }
                        $("#document_folder").DataTable().clear().destroy();
                        form_group_data(url);
                        swal.close();
                        toastr["success"](response.message);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            } else {
                swal("Cancelled", "Your Record is safe", "error");

            }
        });
}
function deleteDocumentdata(id)
{
    selectedrows=[];
    var url='/form/delete';
    delete_data_form(id,url,formgroup_id);
}
