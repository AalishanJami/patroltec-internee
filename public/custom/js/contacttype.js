var selectedrows=[];
$(document).ready(function() {
    $('body').on('click','#contacttype_checkbox_all',function() {
        var x = document.getElementsByClassName("selectedrowcontacttype");
        var i;
        for (i = 0; i < x.length;i++) {
            var tr_id=x[i].id;
            tr_id=tr_id.split('contacttype');
            tr_id=tr_id[1];
            if(selectedrows.includes(tr_id))
            {
                selectedrows.splice( selectedrows.indexOf(tr_id), 1 );
                if (selectedrows.length >0){
                    $('.contacttype_export').val(JSON.stringify(selectedrows));
                }else{
                    $('.contacttype_export').val('1');
                }
                $('#contacttype_tr'+tr_id).removeClass('selected');
                $('#contacttype'+tr_id).attr('checked',false);
            }
            else
            {
                selectedrows.push(tr_id);
                $('.contacttype_export').val(JSON.stringify(selectedrows));
                $('#contacttype_tr'+tr_id).addClass('selected');
                $('#contacttype'+tr_id).attr('checked',true);
            }
        }

    });


    $('body').on('click', '.selectedrowcontacttype', function() {
        var tr_id=$(this).attr('id');
        tr_id=tr_id.split('contacttype');
        tr_id=tr_id[1];
            if(selectedrows.includes(tr_id))
            {
                selectedrows.splice( selectedrows.indexOf(tr_id), 1 );
                if (selectedrows.length >0){
                    $('.contacttype_export').val(JSON.stringify(selectedrows));
                }else{
                    $('.contacttype_export').val('1');
                }
                $('#contacttype_tr'+tr_id).removeClass('selected');
            }
            else
            {
                selectedrows.push(tr_id);
                $('.contacttype_export').val(JSON.stringify(selectedrows));
                $('#contacttype_tr'+tr_id).addClass('selected');
            }
    });
    $('#selectedactivebuttoncontacttype').click(function(){
        var url='/contacttype/active/multiple';
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        if (selectedrows && selectedrows.length) {
       // not empty
            swal({
                title: "Are you sure?",
                text: "Selected option will be active  again ..!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
             },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:url,
                        data: {'ids':selectedrows},
                        success: function(response)
                        {

                            toastr["success"](response.message);
                            var url='/contacttype/getallSoft';
                            $('.contacttype_table').DataTable().clear().destroy();
                            contacttype_data(url);
                            swal.close();

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                  swal("Cancelled", "Your Record is safe", "error");

                }
            });
         }
        else
        {
       // empty
            return toastr["error"]("Select at least one record to delete!");
        }
    });
    $('#deleteselectedcontacttype').click(function(){
       var url='/contacttype/delete/multiple';
       delete_mutilpleDatacontactType(url,selectedrows);
    });
    $('#deleteselectedcontacttypeSoft').click(function(){
       var url='/contacttype/delete/soft/multiple';
       delete_mutilpleDatacontactType(url,selectedrows);
    });

    $('.add-contacttype').on('click', 'i', () => {
      $.ajax({
            type: 'GET',
            url: '/contacttype/store',
            success: function(response)
            {
                var html='';
                html+= '<tr id="contacttype_tr'+response.data.id+'" role="row" class="hide odd">';
                html+=' <td><div class="form-check"><input type="checkbox" class="form-check-input contacttype_checked selectedrowcontacttype" id="contacttype'+response.data.id+'"><label class="form-check-label" for="contacttype'+response.data.id+'"></label></div></td>';
                html+='<td  onkeypress="editSelectedRow('+"contacttype_tr"+response.data.id+')" class="pt-3-half edit_inline_contacttype" data-id="'+response.data.id+'" id="nameD'+response.data.id+'" ';
                if($('#contactType_name_create').val())
                {
                }
                html+='contenteditable="true" ';
                html+='></td>';
                html+='<td>';
                html+='<a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light delete-btn'+response.data.id+'" onclick="deletecontacttypedata()"><i class="fas fa-trash"></i></a>';
                if($('#contactType_name_create').val())
                {
                }
                html+='<a type="button" class="btn btn-primary btn-xs my-0 waves-effect waves-light  savecontacttypedata all_action_btn_margin show_tick_btn'+response.data.id+'" style="display: none;"   id="'+response.data.id+'"><i class="fas fa-check"></i></a>';
                html+='</td>';
                html+='</tr>';
                $('#table-contacttype').find('table').prepend(html);

            },
            error: function (error) {
                console.log(error);
            }
        });


    });
     $('body').on('click', '.savecontacttypedata', function() {
        var company_id=$('#companies_selected_nav').children("option:selected").val();
        var tr_id=$(this).attr('id');
        var data={
            'id':tr_id,
            'name':$('#nameD'+tr_id).html(),
            'company_id':company_id,

        };
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url:'/contacttype/update',
            data:data,
            success: function(response)
            {
                if(response.error)
                {
                    toastr["error"](response.error);
                    $("#contacttype_tr"+tr_id).attr('style','background-color:#ff3547 !important');
                }
                else {
                    toastr["success"](response.message);
                    if (response.flag == 1) {
                        $("#contacttype_tr" + tr_id).attr('style', 'background-color:#90ee90 !important');
                        // var url='/contacttype/getall';
                    }
                    else {
                        $("#contacttype_tr" + tr_id).attr('style', 'background-color:#ff3547 !important');
                        // var url='/contacttype/getallsoft';
                    }

                    save_and_delete_btn_toggle(tr_id);

                    //  $('.contacttype_table').DataTable().clear().destroy();
                    // contacttype_data(url);
                }



            }

        });

        // console.log(data);

    });

    $('#restorebuttoncontacttype').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        selectedrows=[];
        $('#restorebuttoncontacttype').hide();
        $('#deleteselectedcontacttype').hide();
        $('#selectedactivebuttoncontacttype').show();
        $('#deleteselectedcontacttypeSoft').show();
        $('#export_excel_contacttype').hide();
        $('#export_world_contacttype').hide();
        $('#export_pdf_contacttype').hide();
        $('.add-contacttype').hide();
        $('#activebuttoncontacttype').show();
        var url='/contacttype/getallSoft';
        $('.contacttype_table').DataTable().clear().destroy();
        contacttype_data(url);
    });
    $('#activebuttoncontacttype').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        selectedrows=[];
        $('#export_excel_contacttype').show();
        $('#export_world_contacttype').show();
        $('#export_pdf_contacttype').show();
        $('.add-contacttype').show();
        $('#selectedactivebuttoncontacttype').hide();
        $('#deleteselectedcontacttype').show();
        $('#deleteselectedcontacttypeSoft').hide();
        $('#restorebuttoncontacttype').show();
        $('#activebuttoncontacttype').hide();
        var url='/contacttype/getall';
        $('.contacttype_table').DataTable().clear().destroy();
        contacttype_data(url);
    });



});
 function restorecontacttypedata(id)
 {
    var url='/contacttype/active';
    swal({
        title: "Are you sure?",
        text: "Selected option with be active ",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
     },
     function(isConfirm){

       if (isConfirm){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: url,
                data: {'id':id},
                success: function(response)
                {
                    if(response.flag==1)
                    {
                        var url='/contacttype/getall';
                    }
                    else
                    {
                        var url='/contacttype/getallSoft';
                    }
                     $('.contacttype_table').DataTable().clear().destroy();
                    contacttype_data(url);
                    swal.close();
                    toastr["success"](response.message);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        } else {
          swal("Cancelled", "Your Record is safe", "error");

        }
     });
 }
 function delete_mutilpleDatacontactType(url,selectedrows)
 {
     if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        if (selectedrows && selectedrows.length) {
       // not empty
            swal({
                title: "Are you sure?",
                text: "Selected option will be inactive and can be Active again by the admin only!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
             },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:url,
                        data: {'ids':selectedrows},
                        success: function(response)
                        {

                            toastr["success"](response.message);
                            if(response.flag==1)
                            {
                                var url='/contacttype/getall';
                            }
                            else
                            {
                                var url='/contacttype/getallSoft';
                            }
                             $('.contacttype_table').DataTable().clear().destroy();
                            contacttype_data(url);
                            swal.close();

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                  swal("Cancelled", "Your Record is safe", "error");

                }
            });
         }
        else
        {
       // empty
            return toastr["error"]("Select at least one record to delete!");
        }
 }
 function delete_data_contactType(id,url)
 {
    swal({
        title: "Are you sure?",
        text: "Selected option with be Deleted ",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
     },
     function(isConfirm){

       if (isConfirm){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
             $.ajax({
                    type: "POST",
                    url: url,
                    data: {'id':id},
                    success: function(response)
                    {
                        if(response.flag==1)
                        {
                            var url='/contacttype/getall';
                        }
                        else
                        {
                            var url='/contacttype/getallSoft';
                        }
                         $('.contacttype_table').DataTable().clear().destroy();
                        contacttype_data(url);
                        swal.close();
                        toastr["success"](response.message);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
        } else {
          swal("Cancelled", "Your Record is safe", "error");

        }
     });
 }
function deletesoftcontacttypedata(id)
{
    var url='/contacttype/softdelete';
    delete_data_contactType(id,url);

}
function deletecontacttypedata(id)
 {
    var url='/contacttype/delete';
    delete_data_contactType(id,url);
 }
