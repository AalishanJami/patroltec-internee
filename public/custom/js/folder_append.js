function form_group_data(url)
{
    var table = $('#document_folder').dataTable({
        "ordering": false,
        processing: true,
        "bStateSave": true,
        language: {
            'lengthMenu': '<span class="assign_class label'+$('#show_label').attr('data-id')+'" data-id="'+$('#show_label').attr('data-id')+'" data-value="'+$('#show_label').val()+'" ></span>  _MENU_ <span class="assign_class label'+$('#entries_label').attr('data-id')+'" data-id="'+$('#entries_label').attr('data-id')+'" data-value="'+$('#entries_label').val()+'" ></span>',
            'search':'<span class="assign_class label'+$('#search_id').attr('data-id')+'" data-id="'+$('#search_id').attr('data-id')+'" data-value="'+$('#search_id').val()+'" >'+$('#search_id').val()+'</span>',
            'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
            'info': '<span class="assign_class label'+$('#showing_page_label').attr('data-id')+'" data-id="'+$('#showing_page_label').attr('data-id')+'" data-value="'+$('#showing_page_label').val()+'" >'+$('#showing_page_label').val()+'</span> _PAGE_ <span class="assign_class label'+$('#of_label').attr('data-id')+'" data-id="'+$('#of_label').attr('data-id')+'" data-value="'+$('#of_label').val()+'" >'+$('#of_label').val()+'</span> _PAGES_',
            'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
            'paginate': {
                'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
            },
            'infoFiltered': " "
        },
        "ajax": {
            "url": url,
            "type": 'get',
        },
        "createdRow": function( row, data, dataIndex,columns )
        {
            var folder_name=data['name'];
            $(columns[0]).html(folder_name);
            $(columns[0]).attr('class', 'text-left edit_inline_form_group white_color');
            $(columns[0]).attr('id', 'folder_name'+data['id']);
            $(columns[0]).attr('data-id',data['order']+'-'+data['id']);
            if ($("#documentfolder_name_create").val()==1){
                $(columns[0]).attr('Contenteditable', 'true');
            }
            $(columns[1]).attr('class', 'text-nowrap text-left');
            $(row).attr('id', 'form_group'+data['id']);
            $(row).attr('class', 'folder_class selectedrowform_group form_group_background');
        },
        columns: [
            {data:'name', name:'name',visible:$('#documentfolder_name').val()},
            {data:'actions', name:'actions'},
        ],
    });
}
$(document).ready(function () {
    var cut_id='';
    $('body').on('click','.copy_paste',function(){
        console.log($(this));
        var key=$(this).attr('data-key');
        if(key == 'cut')
        {
            cut_id=$(this).attr('data-value');

            $('.folder_class').removeClass('purple');
            $('.folder_class').addClass('form_group_background');

            setTimeout(function(){
                $('#form_group'+cut_id).removeClass('selected');
                $('#form_group'+cut_id).addClass('purple');
            },100);
            $('#form_group'+cut_id).attr('style','');
            $('#form_group'+cut_id).removeClass('form_group_background');
            $('#form_group'+cut_id).removeClass('selected');
            $('.enable_paste').each(function(index,value) {
                if( cut_id != $(this).attr('data-value'))
                {
                    $(this).removeClass('disable_href');
                }
            });
        }
    });
    $('body').on('click','.enable_paste',function(){
        var paste_id=$(this).attr('data-value');
        if(paste_id == 0)
        {
            cut_id=$(this).attr('data-key');
        }
        sendRequest(paste_id,cut_id);

    });
    $('body').on('click','.enable_paste_parent',function(){
        var paste_id=$(this).attr('data-key');
        cut_id=$(this).attr('data-value');
        sendRequest(paste_id,cut_id);
    });
    function sendRequest(paste_id,cut_id)
    {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url: '/form_group/paste',
            data: {
                "paste_id" : paste_id,
                "cut_id" : cut_id,
            },
            success: function(response)
            {
                toastr["success"](response.message);
                $('#form_group'+cut_id).hide();
                // $('#document_folder').DataTable().clear().destroy();
                // var url='/form_group/getall/'+formgroup_id;
                // form_group_data(url);
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    $('.form_group_data').sortable();
    $('body').on('mouseleave', '.edit_inline_form_group ', function() {
        setTimeout(reOrder, 2000);
    });
    $('body').on('blur', '.edit_inline_form_group ', function() {
        $(this).parent().find('tr').removeClass('selected');
    });
});
function reOrder()
{
    var orderArray=[];
    $('.edit_inline_form_group ').each(function(i, obj) {
        var temp=$(this).attr('data-id');
        if(temp)
        {
            temp=temp.split('-');
            var obj={
                'id':temp[1],
                'order':temp[0],
            };
            orderArray.push(obj);
        }
    });
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'POST',
        url: '/form_group/reorder',
        data: {
            "array" : orderArray
        },
        success: function(response)
        {
            console.log("success")
        },
        error: function (error) {
            console.log(error);
        }
    });

}
