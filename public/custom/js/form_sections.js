var selectedrows=[];

$(document).ready(function() {
    var active_tab = localStorage.getItem('activeTab');
    if($('#profile-tab').hasClass('active') && active_tab == '#profile')
    {
        $('.active_plus_btn').show();
    }
    $('body').on('click', '.nav-link', function() {
        if($(this).attr('id') == 'profile-tab' || $(this).attr('id') == 'job-tab' )
        {
            $('.active_plus_btn').show();
        }
        else
        {
            $('.active_plus_btn').hide();
        }
    });
    $("body").on('focusout','.note-editable',function () {
        $("#cmnt").val($(this).html());
    });
    $('.model_create_form_section_popup').click(function(){
        $('.form_section_show_hide_column').hide();
        $('#add_new_form_section_form').trigger("reset");
        $("#valid_width_number").children('option:selected').val('');
        $("#form_section_column").children('option:selected').val('');
        setTimeout(function(){
            $("#set_height_create_select").children('option:selected').val('');
            $('select.create_mdb-select-setheight option:first').attr('disabled', true);
            $('select.create_mdb-select-setheight option:first').attr('selected', true);
            // $("#set_height_create_select").prepend('<option value="" selected disabled>Set Height</option>');
        },5000);
        $("small").html('');
        $("input").attr('style','');

    });
    $('body').on('change','.show_hide_section_column',function(){
        var check=$(this).children("option:selected").val();
        if(parseInt(check))
        {
            if (check == 12){
                $('.form_section_show_hide_column').show();
            }
            else{
                $('.form_section_show_hide_column').hide();
            }

        }
    });
    $('body').on('change', '.answer_type_id', function() {
        answer_type_text=$(this).children("option:selected").text();
        answer_type_text=answer_type_text.trim();
        console.log(answer_type_text);
        $('#answer_group_create').children("option:selected").val('');
        $('#answer_group_create').children("option:selected").text('Chose an answer');
        if(answer_type_text)
        {
            if(answer_type_text == 'date' || answer_type_text == 'time'|| answer_type_text == 'datetime' || answer_type_text == 'text' || answer_type_text == 'richtext' || answer_type_text == 'clocs') {
                $('.answer_group_div').hide();
                $('.layout_group_div').hide();
                $('.column_size').hide();
                $('.comment_summer_note').hide();
                $('.si_text').hide();
                $('.si_user').hide();
                $('.si_select').hide();
                $('.import').show();


                $(".answer_group_div").addClass('form_type_text_field_margin');
                $(".answer_group_div>.form_type_text_field").addClass('form_type_text_field_margin');
                $(".question_field_align").addClass('form_type_text_field_margin_bottom');
                $(".import").addClass('form_type_text_field_margin_bottom');
            }else if(answer_type_text == 'comment') {
                $('.comment_summer_note').show();
                $('.answer_group_div').hide();
                $('.layout_group_div').hide();
                $('.column_size').hide();
                $('.si_text').hide();
                $('.si_user').hide();
                $('.si_select').hide();
                $('.import').hide();

                $(".answer_group_div").addClass('form_type_text_field_margin');
                $(".answer_group_div>.form_type_text_field").addClass('form_type_text_field_margin');
                $(".question_field_align").addClass('form_type_text_field_margin_bottom');
                $(".import").addClass('form_type_text_field_margin_bottom');
            } else if(answer_type_text == 'radio' || answer_type_text == 'checkbox') {
                $('.layout_group_div').show();
                $('.column_size').show();
                $('.answer_group_div').show();
                $('.comment_summer_note').hide();
                $('.si_text').hide();
                $('.si_user').hide();
                $('.si_select').hide();
                $('.import').hide();

                $(".answer_group_div").removeClass('form_type_text_field_margin');
                $(".answer_group_div>.form_type_text_field").removeClass('form_type_text_field_margin');
                $(".question_field_align").removeClass('form_type_text_field_margin_bottom');
                $(".import").removeClass('form_type_text_field_margin_bottom');
            }else if(answer_type_text == 'signature text') {
                $('.layout_group_div').hide();
                $('.column_size').hide();
                $('.answer_group_div').hide();
                $('.comment_summer_note').hide();
                $('.si_text').show();
                $('.si_user').hide();
                $('.si_select').hide();
                $('.import').hide();

                $(".answer_group_div").addClass('form_type_text_field_margin');
                $(".answer_group_div>.form_type_text_field").addClass('form_type_text_field_margin');
                $(".question_field_align").addClass('form_type_text_field_margin_bottom');
                $(".import").addClass('form_type_text_field_margin_bottom');
            }else if(answer_type_text == 'signature user') {
                $('.layout_group_div').hide();
                $('.column_size').hide();
                $('.answer_group_div').hide();
                $('.comment_summer_note').hide();
                $('.si_text').hide();
                $('.si_user').show();
                $('.si_select').hide();
                $('.import').hide();

                $(".answer_group_div").addClass('form_type_text_field_margin');
                $(".answer_group_div>.form_type_text_field").addClass('form_type_text_field_margin');
                $(".question_field_align").addClass('form_type_text_field_margin_bottom');
                $(".import").addClass('form_type_text_field_margin_bottom');
            } else if(answer_type_text == 'signature select') {
                $('.layout_group_div').hide();
                $('.column_size').hide();
                $('.answer_group_div').hide();
                $('.comment_summer_note').hide();
                $('.si_text').hide();
                $('.si_user').hide();
                $('.si_select').show();
                $('.import').hide();

                $(".answer_group_div").addClass('form_type_text_field_margin');
                $(".answer_group_div>.form_type_text_field").addClass('form_type_text_field_margin');
                $(".question_field_align").addClass('form_type_text_field_margin_bottom');
                $(".import").addClass('form_type_text_field_margin_bottom');
            } else {
                $('.answer_group_div').show();
                $('.column_size').hide();
                $('.layout_group_div').hide();
                $('.comment_summer_note').hide();
                $('.si_text').hide();
                $('.si_user').hide();
                $('.si_select').hide();
                $('.import').hide();

                $(".answer_group_div").addClass('form_type_text_field_margin');
                $(".answer_group_div>.form_type_text_field").addClass('form_type_text_field_margin');
                $(".question_field_align").addClass('form_type_text_field_margin_bottom');
                $(".import").addClass('form_type_text_field_margin_bottom');

            }
        }

    });

    $('body').on('click', '.open_popup', function() {
        $('.si_text').hide();
        $('.import').hide();
        $('.si_user').hide();
        $('.si_select').hide();
        $('.answer_group_div').hide();
        $('.layout_group_div').hide();
        $('.column_size').hide();
        $('.comment_summer_note').hide();
        $('.quesion_add_part').summernote('code', ' ');
        $('.quesion_add_mandatory').summernote('code', ' ');
        $('.comment_section_remove').summernote('code', ' ');
        $('#question_Form').trigger("reset");
        $('.question_create_iconsize').html(icon_size_function());
        var id=$(this).attr('id');

        $('.error_message_text').html('');

        $("select").siblings('input').attr('style','');
        $("input").attr('style','');

        $('#question_section_id').val(id);
        $('#answer_group').children("option:selected").val('');
        $('#answer_group').children("option:selected").text('Chose an Answer');


        $('#columnsize_selected_nav').children("option:selected").val('');
        $('#columnsize_selected_nav').children("option:selected").text('Please Select');
        $('#columnsize_selected_nav').children("option:selected").attr('disabled',true);

        $.ajax({
            type: "GET",
            url: ' /document/forms/answer',
            success: function(response)
            {
                var html='';
                html+='<option value="" selected>Answer Group</option>';
                for (var i = response.answer_groups.length - 1; i >= 0; i--) {
                    html+='<option value="'+response.answer_groups[i].id+'">'+response.answer_groups[i].name+'</option>';
                }
                $('#answer_group_create').html(html);
            },
            error: function (error) {
                console.log(error);
            }
        });
        $('#modelAddDynamicForm').modal('show');
    });


    $( '#schdule_project' ).change(function() {
        schdule_project=$('#schdule_project').children("option:selected").val();
        console.log(schdule_project);
        $.ajax({
            type: "GET",
            url: ' /document/forms/project/'+schdule_project,
            success: function(response)
            {

                console.log(response.location);
                var html='';
                html+='<select class="mdb-select-custom-location colorful-select dropdown-primary md-form" multiple searchable="Search here..">';

                for (var i = response.location.length - 1; i >= 0; i--) {
                    response.location[i]
                    html+='<option value="'+response.location[i].id+'">'+response.location[i].name+'</option>';
                }
                html+='</select> ';
                html+='<label class="mdb-main-label">QR List</label>';
                $('#form_section_qr_id').empty();
                $('#form_section_qr_id').html(html);
                $('.mdb-select-custom-location').materialSelect();
                $('select.mdb-select-custom-location').hide();

            },
            error: function (error) {
                console.log(error);
            }
        });
    });
    // $('body').on('click', '.selectedrowform_section', function() {
    //     var tr_id=$(this).attr('id');
    //     tr_id=tr_id.split('form_section_tr');
    //     tr_id=tr_id[1];
    //     var flag=$('#form_section'+tr_id).prop('checked');
    //     console.log(flag);
    //     if(!flag)
    //     {
    //         console.log(tr_id);
    //         console.log(selectedrows);
    //         if(selectedrows.includes(tr_id))
    //         {

    //             selectedrows.splice( selectedrows.indexOf(tr_id), 1 );
    //             $('#form_section'+tr_id).attr('checked', false );
    //             $('#form_section_tr'+tr_id).removeClass('selected');

    //         }
    //         else
    //         {
    //             selectedrows.push(tr_id);
    //             $('#form_section'+tr_id).attr('checked', true );
    //             $('#form_section_tr'+tr_id).addClass('selected');

    //         }
    //     }
    // });
    $('#selectedactivebuttonform_section').click(function(){
        var url=' /document/forms/active/multiple';
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        if (selectedrows && selectedrows.length) {
            // not empty
            swal({
                    title: "Are you sure?",
                    text: "Selected option will be active  again ..!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Yes, I am sure!',
                    cancelButtonText: "No, cancel it!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm){

                    if (isConfirm){
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $.ajax({
                            type: "POST",
                            url:url,
                            data: {'ids':selectedrows},
                            success: function(response)
                            {
                                toastr["success"](response.message);
                                $('#form_section_table_static').DataTable().clear().destroy();
                                form_sectionSoftAppend();
                                swal.close();

                            },
                            error: function (error) {
                                console.log(error);
                            }
                        });

                    } else {
                        swal("Cancelled", "Your Record is safe", "error");

                    }
                });
        }
        else
        {
            // empty
            return toastr["error"]("Select at least one record to delete!");
        }
    });
    $('#deleteselectedform_section').click(function(){
        var url=' /document/forms/delete/multiple';
        console.log(selectedrows);
        delete_mutilpleDataform_section(url,selectedrows);
    });
    $('#deleteselectedform_sectionSoft').click(function(){
        var url=' /document/forms/delete/soft/multiple';
        delete_mutilpleDataform_section(url,selectedrows);
    });

    $('body').on('click', '.question_submit', function()
    {
        $('.form_name_required_message').html('');
        var company_id=$('#companies_selected_nav').children("option:selected").val();
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }

        var validation = true;
        var question_name_create=$('.question_name_create').find( '.note-editable, .card-block p' ).html();
        if($.trim(question_name_create).length == 0){
            $('#question_name_create').html('Field Required');
            validation = false;
        }

         var question_mandatory_create=$('.question_mandatory_create').find( '.note-editable, .card-block p' ).html();
        // if($.trim(question_mandatory_create).length == 0){
        //     $('#question_mandatory_create').html('Field Required');
        //     validation = false;
        // }

        if($.trim($('.numberBox-create').val()).length == 0){
            $("#numberBox-create").attr('style','border-bottom:1px solid;border-color: #e3342f');
            $('#numberBox-create-msg').html('Field Required');
            validation = false;
        }

        // if($('#answer_number').val() == ""){
        //     $('#answer_number_msg').html('Field Required');
        //     validation = false;
        // }
        if($('#answer_type_id').children("option:selected").val() == ""){
            $("[data-activates=select-options-answer_type_id]").attr('style','border-bottom:1px solid;border-color: #e3342f');
            $('#answer_number_msg').html('Field Required');
            validation = false;
        }
        if(validation == false){
            return false;

        }else{

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            event.preventDefault();
            var data = $('#question_Form').serializeArray();
            console.log(data);

            var comment_section = $('.comment_section_create').find('.note-editable, .card-block p').html();
            var obj = {
                'name': 'company_id',
                'value': company_id,
            };
            data.push(obj);
            obj = {
                'name': 'question_name',
                'value': question_name_create,
            };
            data.push(obj);
            obj = {
                'name' : 'display_picture',
                'value' : $('#layout2').is(':checked') ? "1" : "0",
            }
            data.push(obj);
            obj = {
                'name': 'mandatory_message',
                'value': question_mandatory_create,
            };
            data.push(obj);
            obj = {
                'name': 'comment',
                'value': comment_section,
            };
            data.push(obj);
            console.log('#cmnt', data);
            $.ajax({
                type: 'POST',
                url: '/question/store',
                data: data,
                success: function (response) {
                    if (response.error) {
                        toastr["error"](response.error);
                    }
                    else {
                        console.log('dd');
                        $(".question_name_create").attr('style', '');
                        $(".form_name_required_message").html('');
                        toastr["success"](response.message);
                        $('#modelAddDynamicForm').modal('hide');
                        getAllFormsQuestion();
                        getAllForms();
                    }

                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
    });
    $('body').on('click', '.question_edit_submit', function()
    {
        var company_id=$('#companies_selected_nav').children("option:selected").val();
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }

        $('.form_name_required_message').html('');
        var validation = true;
        var question_name = $('.question_name_edit').find('.note-editable, .card-block p').html();
        var question_mandatory = $('.question_mandatory_edit').find('.note-editable, .card-block p').html();
        if($.trim(question_name) == ""){
            $('#question_name_edit_msg').html('Field Required');
            validation = false;
        }

        // if($.trim(question_mandatory) == ""){
        //     $('#question_mandatory_edit_msg').html('Field Required');
        //     validation = false;
        // }

        if($.trim($('.question_order_value').val()).length == 0){
            $('.question_order_value').attr('style','border-bottom:1px solid;border-color: #e3342f');
            $('#numberBox-edit-msg').html('Field Required');
            validation = false;
        }
        // console.log($('#answer_type_id_edit').children("option:selected").val());
        if($('#answer_type_id_edit').children("option:selected").val() == ""){
            $("[data-activates=select-options-answer_type_id]").attr('style','border-bottom:1px solid;border-color: #e3342f');
            $('#answer_type_id_msg_edit').html('Field Required');
            validation = false;
        }

        if(validation == false){
            return false;

        }else {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            event.preventDefault();
            var data = $('#question_Form_edit').serializeArray();

            var comment_section = $('.comment_section').find('.note-editable, .card-block p').html();

            var obj = {
                'name': 'company_id',
                'value': company_id,
            };
            data.push(obj);
            obj = {
                'name': 'question_name',
                'value': question_name,
            };
            data.push(obj);
            obj = {
                'name': 'question_mandatory',
                'value': question_mandatory,
            };
            data.push(obj);
            obj = {
                'name': 'comment',
                'value': comment_section,
            };
            data.push(obj);
            // console.log(data);
            $.ajax({
                type: 'POST',
                url: '/question/update',
                data: data,
                success: function (response) {
                    if (response.error) {
                        toastr["error"](response.error);
                    }
                    else {
                        toastr["success"](response.message);
                        $('#modelAddDynamicFormEdit').modal('hide');
                        getAllFormsQuestion();
                        getAllForms();
                    }
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
    });

    $('body').on('submit', '#add_new_form_section_form', function(event)
    {
        var company_id=$('#companies_selected_nav').children("option:selected").val();
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return false;
        }
        var valid_width_number = $("#valid_width_number").children("option:selected").val();
        var form_name_required=$("#form_name_required").val();
        if (valid_width_number=='' || form_name_required==''){
            if (form_name_required ==""){
                $("#form_name_required").attr('style','border-bottom:1px solid #eb6e6b !important');
                $(".form_name_required_message").html('Field required');
            }
            if (valid_width_number ==""){
                $("[data-activates=select-options-valid_width_number]").attr('style','border-bottom:1px solid #eb6e6b !important;');
                $(".valid_width_number_message").html('Field required');
            }
            return false;
        }


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        event.preventDefault();
        var data=$('#add_new_form_section_form').serializeArray();
        var obj={'name':'company_id',
            'value':company_id,
        };
        data.push(obj);
        console.log(data);
        $.ajax({
            type: 'POST',
            url: '/document/forms/store',
            data: data,
            success: function(response)
            {
                if(response.error)
                {
                    toastr["error"](response.error);
                }
                else
                {
                    $(".valid_width_number_message").html('');
                    $(".form_name_required_message").html('');
                    $('#model_create_form_section').modal('hide');
                    getAllFormsQuestion();
                    getAllForms();
                }
            },
            error: function (error) {
                console.log(error);
            }
        });
    });

    $('body').on('click', '#button_edit_form_id', function(event)
    {
        $(".valid_width_number_message").html('');
        var company_id=$('#companies_selected_nav').children("option:selected").val();
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return false;
        }

        if ($("#name_edit").val() ==""){
            $("#name_edit").attr('style','border-bottom:1px solid #eb6e6b !important');
            $(".form_name_required_message").html('field required');
            return false;
        }
        var valid_width_number = $("#width_edit").children("option:selected").val();

        if (valid_width_number ==""){
            $(".select-dropdown").attr('style','border-bottom:1px solid #eb6e6b !important;');
            $(".valid_width_number_message").html('Field required');
            return false;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        event.preventDefault();

        var data = $('#edit_form_section_form').serializeArray();
        var obj = {
            'name': 'company_id',
            'value': company_id,
        };
        data.push(obj);
        console.log('Data Update : ' + JSON.stringify(data));
        $.ajax({
            type: 'POST',
            url: '/document/forms/update',
            data: data,
            success: function (response) {
                if (response.error) {
                    toastr["error"](response.error);
                }
                else {
                    toastr["success"](response.message);
                    $(".valid_width_number_message").html('');
                    $(".form_name_required_message").html('');
                    $('#modelEditDynamicForm').modal('hide');

                    getAllFormsQuestion();
                    getAllForms();
                    setFormSectionHeight();
                }
            },
            error: function (error) {
                console.log(error);
            }
        });
    });



    $('#restorebuttonform_section').click(function(){

        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        $('#restorebuttonform_section').hide();
        $('#deleteselectedform_section').hide();
        $('#selectedactivebuttonform_section').show();
        $('#deleteselectedform_sectionSoft').show();
        $('#export_excel_form_section').val(2);
        $('#export_world_form_section').val(2);
        $('#export_world_pdf').val(2);
        $('.form_section-table').hide();
        $('#activebuttonform_section').show();
        $('#form_section_table_static').DataTable().clear().destroy();
        form_sectionSoftAppend();
    });
    $('#activebuttonform_section').click(function(){
        $('#export_excel_form_section').val(1);
        $('#export_world_form_section').val(1);
        $('#export_world_pdf').val(1);
        $('.form_section-table').show();
        $('#selectedactivebuttonform_section').hide();
        $('#deleteselectedform_section').show();
        $('#deleteselectedform_sectionSoft').hide();
        $('#restorebuttonform_section').show();
        $('#activebuttonform_section').hide();
        var url=' /document/forms/getall';
        $('#form_section_table_static').DataTable().clear().destroy();
        form_sectionAppend();
    });


    $('body').on('click', '.remove_div', function () {
        var id=$(this).attr('id');
        id=id.split('form_section');
        id=id[1];
        console.log(id);
        var url='/document/forms/delete';
        delete_data_form_section(id,url);
        // $(this).parents('.sortable-card').detach();
    });
    $('body').on('click', '.form_section_clone', function() {
        var id=$(this).attr('id');
        id=id.split('form_section_clone');
        id=id[1];
        var url=' /document/forms/clone';
        form_sectiondata(id,url);

        // var ele = $(this).closest('.sortable-card ').clone(true);
        // console.log(ele);
        // $(this).closest('.sortable-card ').after(ele);
    })



});
function icon_size_function(value=null,flag=null)
{
    console.log(value);
    // var icon_size_Array='<select searchable="Search here.."   class="icon_size browser-default custom-select" name="icon_size">';
    if(flag == 1)
    {

        var icon_size_Array='<select searchable="Search here.."   class="show_hide_section_column mdb-select_form_width colorful-select dropdown-primary" style="margin-left: auto !important;width:96%!important;"  name="width" id="width_edit">';
    }
    else{
        var icon_size_Array='<select searchable="Search here.."   class="icon_size browser-default custom-select" name="icon_size">';
        icon_size_Array+='<option value="" disabled>Icon Size</option>';
    }
    if(!flag)
    {
        var selected=' ';
        if(value == 1)
        {
            selected='selected';
        }
        icon_size_Array+='<option value="1" '+selected+'>';
        if(flag == 1)
        {
            icon_size_Array+='1';
        }
        else
        {
            icon_size_Array+='1%';
        }
        icon_size_Array+='</option>';
        selected=' ';
        if(value == 2)
        {
            selected='selected';
        }
        icon_size_Array+='<option value="2" '+selected+'>';
        if(flag == 1)
        {
            icon_size_Array+='2';
        }
        else
        {
            icon_size_Array+='2%';
        }
        icon_size_Array+='</option>';
    }
    selected=' ';
    if(value == 3)
    {
        selected='selected';
    }
    icon_size_Array+='<option value="3" '+selected+'>';
    if(flag == 1)
    {
        icon_size_Array+='3';
    }
    else
    {
        icon_size_Array+='3%';
    }
    icon_size_Array+='</option>';
    selected=' ';
    if(value == 4)
    {
        selected='selected';
    }
    icon_size_Array+='<option value="4" '+selected+'>';
    if(flag == 1)
    {
        icon_size_Array+='4';
    }
    else
    {
        icon_size_Array+='4%';
    }
    icon_size_Array+='</option>';
    selected=' ';
    if(value == 5)
    {
        selected='selected';
    }
    icon_size_Array+='<option value="5" '+selected+'>';
    if(flag == 1)
    {
        icon_size_Array+='5';
    }
    else
    {
        icon_size_Array+='5%';
    }
    icon_size_Array+='</option>';
    selected=' ';
    if(value == 6)
    {
        selected='selected';
    }
    icon_size_Array+='<option value="6" '+selected+'>';
    if(flag == 1)
    {
        icon_size_Array+='6';
    }
    else
    {
        icon_size_Array+='6%';
    }
    icon_size_Array+='</option>';
    selected=' ';
    if(value == 7)
    {
        selected='selected';
    }
    icon_size_Array+='<option value="7" '+selected+'>';
    if(flag == 1)
    {
        icon_size_Array+='7';
    }
    else
    {
        icon_size_Array+='7%';
    }
    icon_size_Array+='</option>';
    selected=' ';
    if(value == 8)
    {
        selected='selected';
    }
    icon_size_Array+='<option value="8" '+selected+'>';
    if(flag == 1)
    {
        icon_size_Array+='8';
    }
    else
    {
        icon_size_Array+='8%';
    }
    icon_size_Array+='</option>';
    selected=' ';
    if(value == 9)
    {
        selected='selected';
    }
    icon_size_Array+='<option value="9" '+selected+'>';
    if(flag == 1)
    {
        icon_size_Array+='9';
    }
    else
    {
        icon_size_Array+='9%';
    }
    icon_size_Array+='</option>';
    selected=' ';
    if(value == 10)
    {
        selected='selected';
    }
    icon_size_Array+='<option value="10" '+selected+'>';
    if(flag == 1)
    {
        icon_size_Array+='10';
    }
    else
    {
        icon_size_Array+='10%';
    }
    icon_size_Array+='</option>';
    selected=' ';
    if(value == 11)
    {
        selected='selected';
    }
    icon_size_Array+='<option value="11" '+selected+'>';
    if(flag == 1)
    {
        icon_size_Array+='11';
    }
    else
    {
        icon_size_Array+='11%';
    }
    icon_size_Array+='</option>';
    selected=' ';
    if(value == 12)
    {
        selected='selected';
    }
    icon_size_Array+='<option value="12" '+selected+'>';
    if(flag == 1)
    {
        icon_size_Array+='12';
    }
    else
    {
        icon_size_Array+='12%';
    }
    icon_size_Array+='</option>';
    return icon_size_Array;
}
function share(id){
    var url='/document/forms/share';
    swal({
            title: "Are you sure?",
            text: "Selected option with be shared ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){

            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "GET",
                    url: url,
                    data: {'id':id},
                    success: function(response)
                    {
                        getAllFormsActive();
                        getAllFormsQuestion();
                        getAllForms();
                        // if(response.flag==1)
                        // {
                        //     getAllForms();
                        //     getAllFormsQuestion();
                        //     // var url='/form/getall';
                        // }
                        // else
                        // {
                        //     // var url='/form/getallSoft';
                        //     getAllFormsActive();
                        // }
                        swal.close();
                        toastr["success"](response.message);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            } else {
                swal("Cancelled", "The Section was not shared", "error");

            }
        });
}


$("body").on('click','#profile-tab',function () {
    getAllFormsQuestion();
    getAllForms();
});

function delete_sectionquestiondata(id){
    var url='/question/delete';
    swal({
            title: "Are you sure?",
            text: "Want to delete this question ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){

            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {'id':id},
                    success: function(response)
                    {
                        swal.close();
                        toastr["success"](response.message);
                        getAllFormsQuestion();
                        getAllForms();
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            } else {
                swal("Cancelled", "The Section was not shared", "error");

            }
        });
}

function getform_sectioncompletedata(id)
{
    var company_id=$('#companies_selected_nav').children("option:selected").val();
    var form_sections = document.getElementsByClassName("form_section");
    var form_section_ids = [];
    for(var i=0;i<form_sections.length;i++)
    {

        form_section_ids.push(form_sections[i].value);

    }

    console.log(form_section_ids);

    var url='/document/forms/complete';
    swal({
            title: "Are you sure?",
            text: "Selected option with be active ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){

            if (isConfirm){
                $('#Loading_gif').show();
                // $('#profile-tab').removeClass('active');
                // $('#profile').removeClass('active show');

                localStorage.setItem('activeTab', $('#contact-tab').attr('href'));
                var activeTab = localStorage.getItem('activeTab');
                if(activeTab) {

                    $('#myTab a[href="' + activeTab + '"]').tab('show');

                }
                swal.close();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {'id':id,'company_id': company_id,'form_section_ids':form_section_ids},
                    success: function(response)
                    {
                        getAllFormsActive();
                        getAllForms();
                        $('#Loading_gif').hide();
                        toastr["success"](response.message);
                    },
                    error: function (error) {
                        $('#Loading_gif').hide();
                        console.log(error);
                    }
                });
            } else {
                swal("Cancelled", "Your Record is safe", "error");

            }
        });
}

function getform_sectionquestiondata(id)
{
    var url='/question/edit/'+id;
    $.ajax({
        type: "GET",
        url: url,
        success: function(response)
        {
            console.log(response.data);
            $('.question_section_id_edit').val(response.data.section_id);
            // signature_user_edit
            $('.answer_group_div').hide();
            $('.column_size').hide();
            $('.layout_group_div').hide();
            $('.comment_summer_note').hide();
            $('.si_text').hide();
            $('.si_user').hide();
            $('.si_select').hide();
            $('.import').hide();

            $('.error_message_text').html('');

            $("select").siblings('input').attr('style','');
            $("input").attr('style','');

            var answer_type_text=response.data.answer_type.name;
            if(answer_type_text == 'date' || answer_type_text == 'time'|| answer_type_text == 'datetime' || answer_type_text == 'text' || answer_type_text == 'richtext' || answer_type_text == 'clocs')
            {
                $('.import').show();
            }
            $('#signature_user_edit').val(response.data.import_name)
            var check_drop_down='';
            $('#question_name_edit').summernote('code',response.data.question);
            $('#question_mandatory_edit').summernote('code',response.data.mandatory_message);
            $('.question_number_edit').val(response.data.order);
            $('.icon_size_edit').empty();
            $('.icon_size_edit').html(icon_size_function(response.data.icon_size));
            $('#sc').summernote('code',response.data.comment);

            var column_size_html='<select searchable="Search here.."   class="icon_size_function mdb-select-custom-question" name="column_size">';
            column_size_html+='<option value="" disabled>Column Size</option>';
            var selected='';
            if(response.data.column_size == '1')
            {
                selected='selected';
            }
            column_size_html+='<option '+selected+' value="1">1</option>';
            selected='';
            if(response.data.column_size == '2')
            {
                selected='selected';
            }
            column_size_html+='<option '+selected+' value="2">2</option>';
            selected='';
            if(response.data.column_size == '3')
            {
                selected='selected';
            }
            column_size_html+='<option '+selected+' value="3">3</option>';
            selected='';
            if(response.data.column_size == '4')
            {
                selected='selected';
            }
            column_size_html+='<option '+selected+' value="4">4</option>';
            selected='';
            if(response.data.column_size == '5')
            {
                selected='selected';
            }
            column_size_html+='<option '+selected+' value="5">5</option>';
            selected='';
            if(response.data.column_size == '6')
            {
                selected='selected';
            }
            column_size_html+='<option '+selected+' value="6">6</option>';
            column_size_html+='</select>';
            $('.column_size_edit').html(column_size_html);

            var html='';
            html+='<select searchable="Search here.." id="answer_type_id"  class="answer_type_id browser-default custom-select" name="answer_number">';
            for (var i = response.answer_types.length - 1; i >= 0; i--) {
                // response.contactsType[i]
                if(response.data.answer_type_id == response.answer_types[i].id)
                {
                    check_drop_down=response.answer_types[i].name;
                    html+='<option selected value="'+response.answer_types[i].id+'">'+response.answer_types[i].name+'</option>';
                }
                else
                {
                    html+='<option  value="'+response.answer_types[i].id+'">'+response.answer_types[i].name+'</option>';
                }
            }
            html+='</select> ';

            $('.answer_type_id_edit').empty();
            $('.answer_type_id_edit').html(html);

            html='';

            html+='<select searchable="Search here.." class="mdb-select-custom-question" name="answer_group">';
            for (var i = response.answer_groups.length - 1; i >= 0; i--) {
                // response.answer_groups[i]

                if(response.data.answer_groups_id == response.answer_groups[i].id)
                {
                    html+='<option selected value="'+response.answer_groups[i].id+'">'+response.answer_groups[i].name+'</option>';
                }
                else
                {
                    html+='<option value="'+response.answer_groups[i].id+'">'+response.answer_groups[i].name+'</option>';
                }
            }
            html+='</select> ';
            console.log(response.data.layout);
            if(response.data.layout == 1)
            {
                $('.layout_vertical').attr('checked',true);
            }
            else if(response.data.layout == 2)
            {
                $('.layout_horizontal').attr('checked',true);
            }
            $('.answer_group_id_edit').empty();
            $('.answer_group_id_edit').html(html);


            var answer_type_text=$('.answer_type_id').children("option:selected").val();
            answer_type_text=answer_type_text.trim();
            console.log("answer_type : "+answer_type_text);
            if(answer_type_text == '5' || answer_type_text == '6' || answer_type_text == '7' || answer_type_text == '8')
            {
                $('.answer_group_div').hide();
                $('.layout_group_div').hide();
            }
            else if(answer_type_text == '1' || answer_type_text == '3')
            {
                $('.layout_group_div').show();
                $('.answer_group_div').show();
            }else if(answer_type_text == '12' || answer_type_text == '11' || answer_type_text == '10' || answer_type_text == '9'){
                $('.answer_group_div').hide();
            }else{
                $('.answer_group_div').show();
                $('.layout_group_div').hide();
            }

            setFormSectionHeight();

            // $('.answer_group_div').hide();
            // $('.column_size').hide();
            // $('.layout_group_div').hide();
            // $('.comment_summer_note').hide();
            // $('.si_text').hide();
            // $('.si_user').hide();
            // $('.si_select').hide();
            //$('.import').hide();
            switch(check_drop_down) {
                case 'radio':
                    $('.answer_group_div').show();
                    $('.layout_group_div').show();
                    $('.column_size').show();
                    break;
                case 'checkbox':
                    $('.answer_group_div').show();
                    $('.layout_group_div').show();
                    $('.column_size').show();
                    break;
                case 'comment':
                    $('.comment_summer_note').show();
                    break;
                case 'signature text':
                    $('.si_text').show();
                    $('.signature_text').val(response.data.signature_required_app_text_field);
                    break;
                case 'signature user':
                    $('.si_user').show();
                    break;
                case 'signature select':
                    $('.signature_user').val(response.data.signature_required_app_select_menu);
                    $('.si_select').show();
                    break;
            }
            console.log(response.data.answer_groups_id);
            console.log(response.sig_select);
            var sig_html='';
            sig_html+='<select searchable="Search here.." class="mdb-select-custom-question sig_select" name="sig_select">';
            for (var i = response.sig_select.length - 1; i >= 0; i--) {
                console.log(response.sig_select[i].id);

                if(response.data.answer_groups_id == response.sig_select[i].id)
                {
                    sig_html+='<option selected value="'+response.sig_select[i].id+'">'+response.sig_select[i].name+'</option>';
                }
                else
                {
                    sig_html+='<option value="'+response.sig_select[i].id+'">'+response.sig_select[i].name+'</option>';
                }
            }
            sig_html+='</select> ';
            $('.user_sig_edit').html(sig_html);
            $('.mdb-select-custom-question').materialSelect();
            $('select.mdb-select-custom-question').hide();
            $('#question_id').val(response.data.id);
            $('#modelAddDynamicFormEdit').modal('show');

            // $('#question_name_edit').val(response.data.question);
            // $('#question_name_edit').val(response.data.question);
            // getAllForms();
            // getAllFormsActive();
            // $('#Loading_gif').hide();
            // toastr["success"](response.message);
        },
        error: function (error) {
            // $('#Loading_gif').hide();
            // console.log(error);
        }
    });

}
function getform_sectiondata(id)
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type : 'POST',
        url : ' /document/forms/edit',
        data:{'id':id},
        success:function(data)
        {
            console.log(data.data);
            $("input").siblings('.error_message_text').html('');
            $("input").attr('style','');
            $("input").siblings('input').attr('style','');
            show_header
            if(data.data.width == 12)
            {
                $('.form_section_show_hide_column').show();
            }
            else
            {
                $('.form_section_show_hide_column').hide();
            }
            $('#name_edit').val(data.data.name);
            $('#header_edit').val(data.data.header);
            $('#footer_edit').val(data.data.footer);
            if (data.data.footer_height !==null){
                $('#footer_height_edit').val(data.data.footer_height);
            }else{
                $('#footer_height_edit').val('50');
            }
            if (data.data.header_height !==null){
                $('#header_height_edit').val(data.data.header_height);
            }else{
                $('#header_height_edit').val('50');
            }
            $('#set_formsection_height').val(data.data.form_sections_height);
            $('#signature_edit').val(data.data.signature);
            // $('#width_edit').val(data.data.width);
            // $('#width_edit').val(data.data.width).attr("selected");
            // $('#width_edit').children("option:selected").val(data.data.width);
            $('.width_edit').empty();
            $('.width_edit').html(icon_size_function(data.data.width,1));
            var selected_option='<select searchable="Search here.."   class="mdb-select_form_width colorful-select dropdown-primary"   name="form_section_column" >';
            selected_option +='<option value="" selected disabled>Column Size</option>';
            if (data.data.form_section_column==1){
                selected_option += '<option selected value="1">1</option>';
            }else{
                selected_option += '<option value="1">1</option>';
            }
            if (data.data.form_section_column==2){
                selected_option += '<option selected value="2">2</option>';
            }else{
                selected_option += '<option value="2">2</option>';
            }
            if (data.data.form_section_column==3){
                selected_option += '<option selected value="3">3</option>';
            }else{
                selected_option += '<option value="3">3</option>';
            }
            if (data.data.form_section_column==4){
                selected_option += '<option selected value="4">4</option>';
            }else{
                selected_option += '<option value="4">4</option>';
            }
            if (data.data.form_section_column==5){
                selected_option += '<option selected value="5">5</option>';
            }else{
                selected_option += '<option value="5">5</option>';
            }
            if (data.data.form_section_column==6){
                selected_option += '<option selected value="6">6</option>';
            }else{
                selected_option += '<option value="6">6</option>';
            }
            selected_option += '</select>';
            $('#form_section_column_edit').html(selected_option);

            $('.mdb-select_form_width').materialSelect();
            $('select .mdb-select_form_width, .initialized').hide();
            $('.form_section_column').materialSelect();
            $('select .form_section_column, .initialized').hide();
            $('#id_edit').val(data.data.id);
            $('#border_style_edit').val(data.data.border_style);
            if(data.data.show_on_dashboard == 1)
            {
                $('#dashboardedit1').attr('checked',true);
            }
            else
            {
                $('#dashboardedit0').attr('checked',true);
            }
            $('#modelEditDynamicForm').modal('show');

            if (data.data.show_header =="1"){
                $('#show_header_edit').prop('checked',true);
                $('#show_header_edit').val('1');
            }else{
                $('#show_header_edit').val('0');
                $('#show_header_edit').prop('checked',false);
            }

            if (data.data.show_footer =="1"){
                $('#show_footer_edit').val('1');
                $("#show_footer_edit").prop('checked',true);
            }else{
                $('#show_footer_edit').val('0');
                $("#show_footer_edit").prop('checked',false);
            }
            if (data.data.is_repeatable =="1"){
                $('#is_repeatable_edit').val('1');
                $("#is_repeatable_edit").prop('checked',true);
            }else{
                $('#is_repeatable_edit').val('0');
                $("#is_repeatable_edit").prop('checked',false);
            }
            if (data.data.is_take_pic =="1"){
                $('#is_take_pic_edit').val('1');
                $("#is_take_pic_edit").prop('checked',true);
            }else{
                $('#is_take_pic_edit').val('0');
                $("#is_take_pic_edit").prop('checked',false);
            }
            if (data.data.is_select_pic =="1"){
                $('#is_select_pic_edit').val('1');
                $("#is_select_pic_edit").prop('checked',true);
            }else{
                $('#is_select_pic_edit').val('0');
                $("#is_select_pic_edit").prop('checked',false);
            }
            if (data.data.is_select_doc =="1"){
                $('#is_select_doc_edit').val('1');
                $("#is_select_doc_edit").prop('checked',true);
            }else{
                $('#is_select_doc_edit').val('0');
                $("#is_select_doc_edit").prop('checked',false);
            }
            if (data.data.is_notes =="1"){
                $('#is_notes_edit').val('1');
                $("#is_notes_edit").prop('checked',true);
            }else{
                $('#is_notes_edit').val('0');
                $("#is_notes_edit").prop('checked',false);
            }

            setFormSectionHeight(null,id);
        }
    });
}
function selectedrowform_section(id)
{

    var tr_id=id;
    var flag=$('#form_section'+tr_id).prop('checked');
    console.log(flag);
    if(flag)
    {
        console.log(tr_id);
        console.log(selectedrows);
        if(selectedrows.includes(tr_id))
        {
            selectedrows.splice( selectedrows.indexOf(tr_id), 1 );
            $('#form_section'+tr_id).attr('checked', false );
            $('#form_section_table_static tr#'+tr_id).removeClass('selected');

        }
        else
        {
            selectedrows.push(tr_id);
            console.log(selectedrows);
            $('#form_section'+tr_id).attr('checked', true );
            $('#form_section_table_static tr#'+tr_id).addClass('selected');

        }
    }

}
function form_sectiondata(id,url)
{
    swal({
            title: "Are you sure?",
            text: "Selected option with be clone",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){

            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {'id':id},
                    success: function(response)
                    {
                        getAllFormsActive();
                        getAllFormsQuestion();
                        getAllForms();
                        swal.close();
                        toastr["success"](response.message);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            } else {
                swal("Cancelled", "Your Record is safe", "error");

            }
        });
}
function delete_mutilpleDataform_section(url,selectedrows)
{
    if( $('.edit_cms_disable').css('display') == 'none' ) {
        console.log('Editer mode On Please change your mode');
        return 0;
    }
    if (selectedrows && selectedrows.length) {
        // not empty
        swal({
                title: "Are you sure?",
                text: "Selected option will be inactive and can be Active again by the admin only!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:url,
                        data: {'ids':selectedrows},
                        success: function(response)
                        {

                            toastr["success"](response.message);
                            $('#form_section_table_static').DataTable().clear().destroy();
                            if(response.flag==1)
                            {
                                form_sectionAppend();
                            }
                            else
                            {
                                form_sectionSoftAppend();
                            }
                            swal.close();

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                    swal("Cancelled", "Your Record is safe", "error");

                }
            });
    }
    else
    {
        // empty
        return toastr["error"]("Select at least one record to delete!");
    }
}
function delete_data_form_section(id,url)
{
    swal({
            title: "Are you sure?",
            text: "Selected option with be Deleted ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){

            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {'id':id},
                    success: function(response)
                    {
                        // if(response.flag==1)
                        // {
                        //     getAllForms();
                        // }
                        // else
                        // {
                        //     form_sectionSoftAppend();
                        // }
                        getAllFormsActive();
                        getAllFormsQuestion();
                        getAllForms();


                        swal.close();
                        toastr["success"](response.message);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            } else {
                swal("Cancelled", "Your Record is safe", "error");

            }
        });
}
function deletesoftform_sectiondata(id)
{
    selectedrows=[];
    var url=' /document/forms/softdelete';
    delete_data_form_section(id,url);

}
function deleteform_sectiondata(id)
{
    selectedrows=[];
    console.log(selectedrows);
    var url=' /document/forms/delete';
    delete_data_form_section(id,url);
}
