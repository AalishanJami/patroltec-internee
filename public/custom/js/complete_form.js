function selectedJob(job_id,location_id,break_id)
{
  console.log("Job Selected",job_id,location_id,break_id);
  $('#job_group_id').val(job_id);
  $('#location_id').val(location_id);
  $('#location_breakdown_id').val(break_id);
} 


 $(document).ready(function() {

 	  $( '#form_type' ).change(function() {
        form_type=$('#form_type').children("option:selected").val();
        console.log(form_type);
        $.ajax({
            type: "GET",
            url: '/job/getForm/'+form_type,
            success: function(response)
            {

              console.log(response.form);
              var html='';
              html+='<select name="form_id" class="mdb-select mdb-select-custom-form" id="form_id">';

                for (var i = response.form.length - 1; i >= 0; i--) {
                      
                        html+='<option value="'+response.form[i].id+'">'+response.form[i].name+'</option>';
                }

                html+='</select> ';
              
                $('#form_div').empty();
                console.log(html, "html");
                $('#form_div').html(html);
                $('#form_id').materialSelect();
               $('select.mdb-select-custom-form').hide();

            },
            error: function (error) {
                console.log(error);
            }
        });
    });

 	});


 function signed()
 {
     var url='/completed_forms/getAllSignedDocument';
       $('#complete_table').DataTable().clear().destroy();
  complete_data(url);
 }
 function signedReport()
 {
  
      if(id==0)
     var url='/completed_forms/getAllSignedReport';
      else
     var url='/completed_forms/getAllSignedReport/'+id;

       $('#complete_table').DataTable().clear().destroy();
  complete_data(url);
 }

  function filterReport(fl)
 {

      if(id==0)
     var url='/completed_forms/getAllFilterReport/null/'+fl;
      else
     var url='/completed_forms/getAllFilterReport/'+id+'/'+fl;

       $('#complete_table').DataTable().clear().destroy();
  complete_data(url);
 }
function filterProject(fl)
{
  console.log(fl);
  console.log(id);
  if(id==0)
  {
    var url='/completed_forms/getAllFilterProject/null/'+fl;
  }
  else
  {
    var url='/completed_forms/getAllFilterProject/'+id+'/'+fl;
  }
  $('#complete_table').DataTable().clear().destroy();
  complete_data(url);
}
   function filterDocument(fl)
 {


     var url='/completed_forms/getAllFilterDocument/null/'+fl;


       $('#complete_table').DataTable().clear().destroy();
  complete_data(url);
 }

    function filterUser(fl)
 {


     var url='/completed_forms/getAllFilterUser/null/'+fl;


       $('#complete_table').DataTable().clear().destroy();
  complete_data(url);
 }
function signedProject()
{
  if(id==0)
  {
  var url='/completed_forms/getAllSignedProject';
  }
  else
  {
    var url='/completed_forms/getAllSignedProject/'+id;
  }
  $('#complete_table').DataTable().clear().destroy();
  complete_data(url);
}
  function signedEmployee()
 {
     var url='/completed_forms/getAllSignedUser';
       $('#complete_table').DataTable().clear().destroy();
  complete_data(url);
 }

  function unSigned()
 {
       
     var url='/completed_forms/getAllReport';
       $('#complete_table').DataTable().clear().destroy();
  complete_data(url);
 }
   function unSignedReport()
 {
     /*$("#signed").attr("style", "display:block");
      $("#un_signed").attr("style", "display:none");*/
     var url='/completed_forms/getAllReport';
       $('#complete_table').DataTable().clear().destroy();
  complete_data(url);
 }
function unSignedProject()
{  
  var url='/completed_forms/getAllProject';
  $('#complete_table').DataTable().clear().destroy();
  complete_data(url);
}
  function unSignedEmployee()
 {
    
     var url='/completed_forms/getAllUser';
       $('#complete_table').DataTable().clear().destroy();
  complete_data(url);
 }