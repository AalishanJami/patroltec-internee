 $(document).ready(function() {
    var selectedrows=[];

     $('body').on('click','#nextofkin_checkbox_all',function() {
         $('.selectedrownext_kin').not(this).prop('checked', this.checked);
         $('.selectedrownext_kin').each(function(key,value) {
             var id=$(this).attr('id');
             console.log(id);
             if(id)
             {
                 id=id.split('next_kin');
                 row_id='#next_kin_tr'+id[1];
                 if($(this).is(":checked"))
                 {
                     if(key == 0)
                     {
                         selectedrows=[];
                     }
                     if(id[1] != '_checkbox_all')
                     {
                         selectedrows.push(id[1]);
                     }
                     $('.next_kin_export').val(JSON.stringify(selectedrows));
                     $(row_id).addClass('selected');
                 }
                 else
                 {
                     $('.next_kin_export').val(1);
                     selectedrows=[];
                     $(row_id).removeClass('selected');
                 }
             }
         });
     });



     $('body').on('click', '.selectedrownext_kin', function() {
         var id=$(this).attr('id');
         id=id.split('next_kin');
         row_id='#next_kin_tr'+id[1];
         if($(this).is(":checked"))
         {
             selectedrows.push(id[1]);
             $('.next_kin_export').val(JSON.stringify(selectedrows));
             $(row_id).addClass('selected');
         }else
         {
             if(selectedrows.includes(id[1]))
             {
                 selectedrows.splice( selectedrows.indexOf(id[1]),1);
                 $('.next_kin_export').val(JSON.stringify(selectedrows));
                 $(row_id).removeClass('selected');
             }
         }
    });
    $('#selectedactivebuttonnext_kin').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        var url='/next/kin/active/multiple';
        if (selectedrows && selectedrows.length) {
       // not empty
            swal({
                title: "Are you sure?",
                text: "Selected option will be active  again ..!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
             },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:url,
                        data: {'ids':selectedrows},
                        success: function(response)
                        {

                            toastr["success"](response.message);
                            var url='/next/kin/getallSoft';
                            $('#next_kin_table').DataTable().clear().destroy();
                            next_kin_data(url);
                            swal.close();

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
                } else {
                  swal("Cancelled", "Your Record is safe", "error");
                }
            });
         }
        else
        {
       // empty
            return toastr["error"]("Select at least one record to delete!");
        }
    });
    $('#deleteselectednext_kin').click(function(){
       var url='/next/kin/delete/multiple';
       delete_mutilpleData_kin(url,selectedrows);
    });
    $('#deleteselectednext_kinSoft').click(function(){
       var url='/next/kin/delete/soft/multiple';
       delete_mutilpleData_kin(url,selectedrows);
    });
    $('.add_next_kin').on('click', 'i', () => {
      $.ajax({
            type: 'GET',
            url: '/next/kin/store',
            success: function(response)
            {
                var html='';
                html+= '<tr id="next_kin_tr'+response.data.id+'" class="selectedrownext_kin'+response.data.id+'" role="row" class="hide odd">';
                html+='<td>';
                if($('#nextofkin_checkbox').val()==1){
                    html+='<div class="form-check"><input type="checkbox" class="form-check-input notes_checked selectedrownext_kin" id="next_kin'+response.data.id+'"><label class="form-check-label" for="next_kin'+response.data.id+'""></label></div>';
                }
                html+='</td>';
                if($('#nextofkin_title').val()==1){
                    var t_edit=($('#nextofkin_title_create').val()==1 && $('#create_nextofkin').val()==1) ? "true":"false";
                    html+='<td data-id="'+response.data.id+'" onkeypress="editSelectedRow('+"next_kin_tr"+response.data.id+')" class="pt-3-half edit_inline_next_kin"  id="title'+response.data.id+'"   contenteditable="'+t_edit+'"></td>';
                }
                if($('#nextofkin_firstname').val()==1){
                    var f_edit=($('#nextofkin_firstname_create').val()==1 && $('#create_nextofkin').val()==1) ? "true":"false";
                    html+='<td data-id="'+response.data.id+'" onkeypress="editSelectedRow('+"next_kin_tr"+response.data.id+')" class="pt-3-half edit_inline_next_kin" id="first_name'+response.data.id+'"   contenteditable="'+f_edit+'"></td>';
                }
                if($('#nextofkin_surname').val()==1){
                    var u_edit=($('#nextofkin_surname_create').val()==1 && $('#create_nextofkin').val()==1) ? "true":"false";
                    html+='<td data-id="'+response.data.id+'" onkeypress="editSelectedRow('+"next_kin_tr"+response.data.id+')" class="pt-3-half edit_inline_next_kin" id="surname'+response.data.id+'"   contenteditable="'+u_edit+'"></td>';
                }
                if($('#nextofkin_email').val()==1){
                    var e_edit=($('#nextofkin_email_create').val()==1 && $('#create_nextofkin').val()==1) ? "true":"false";
                    html+='<td data-id="'+response.data.id+'" onkeypress="editSelectedRow('+"next_kin_tr"+response.data.id+')" class="pt-3-half edit_inline_next_kin" id="email'+response.data.id+'"   contenteditable="'+e_edit+'"></td>';
                }
                if($('#nextofkin_relationship').val()==1){
                    html+='<td data-id="'+response.data.id+'" class="pt-3-half edit_inline_next_kin" >';
                    var relationship=($('#nextofkin_relationship_create').val()==1) ? "":"disabled";
                    html+='<select id="relation_ship_id'+response.data.id+'" onchange="editSelectedRow('+"next_kin_tr"+response.data.id+')"  style="background-color: inherit;border: 0px">';
                    for (var j = response.relationships.length - 1; j >= 0; j--) {
                        html+='<option '+relationship+' value="'+response.relationships[j].id+'"  >'+response.relationships[j].name+'</option>';
                    }
                    html+='</select></td>';
                }
                if($('#nextofkin_phone').val()==1){
                    var ph_edit=($('#nextofkin_phone_create').val()==1 && $('#create_nextofkin').val()==1) ? "true":"false";
                    html+='<td data-id="'+response.data.id+'" onkeypress="editSelectedRow('+"next_kin_tr"+response.data.id+')" class="pt-3-half edit_inline_next_kin" id="phone_number'+response.data.id+'"   contenteditable="'+ph_edit+'"></td>';
                }
                html+='<td>';
                if($('#delete_nextofkin').val()==1){
                    html+='<a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light deletenext_kindata'+response.data.id+'" onclick="deletenext_kindata()"><i class="fas fa-trash"></i></a>';
                }
                if($('#edit_nextofkin').val()==1){
                    html+='<a type="button" class="btn btn-primary all_action_btn_margin btn-xs my-0 waves-effect waves-light savenext_kindata savenext_kindata_show_'+response.data.id+'" style="display: none;" id="'+response.data.id+'"><i class="fas fa-check"></i></a>';
                }
                html+='</td>';
                html+='</tr>';
                console.log(html);
                $('#table_next_kin').find('table').prepend(html);

            },
            error: function (error) {
                console.log(error);
            }
        });
    });
    $('body').on('click', '.savenext_kindata', function() {
        var company_id=$('#companies_selected_nav').children("option:selected").val();
        var tr_id=$(this).attr('id');
        var relation_ship_id = $('#relation_ship_id'+tr_id).children("option:selected").val();
        var data={
            'id':tr_id,
            'title':$('#title'+tr_id).html(),
            'first_name':$('#first_name'+tr_id).html(),
            'surname':$('#surname'+tr_id).html(),
            'email':$('#email'+tr_id).html(),
            'relation_ship_id':relation_ship_id,
            'phone_number':$('#phone_number'+tr_id).html(),
            'company_id':company_id,
        };
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url:'/next/kin/update',
            data:data,
            success: function(response)
            {
                if(response.error)
                {
                    toastr["error"](response.error);
                    $("#next_kin_tr"+tr_id).attr('style',"background-color: #fc685f");
                }
                else
                {
                    toastr["success"](response.message);
                    $("#next_kin_tr"+tr_id).attr('style',"background-color: lightgreen !important;");
                    $('.deletenext_kindata'+tr_id).show();
                    $('.savenext_kindata_show_'+tr_id).hide();
                }
            }

        });

    });

    $('#restorebuttonnext_kin').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        selectedrows=[];
        $('#restorebuttonnext_kin').hide();
        $('#deleteselectednext_kin').hide();
        $('#selectedactivebuttonnext_kin').show();
        $('#deleteselectednext_kinSoft').show();
        $('#export_excel_next_kin').hide();
        $('#export_world_next_kin').hide();
        $('#export_pdf_next_kin').hide();
        $('.add_next_kin').hide();
        $('#activebuttonnext_kin').show();
        var url='/next/kin/getallSoft';
        $('#next_kin_table').DataTable().clear().destroy();
        next_kin_data(url);
    });
    $('#activebuttonnext_kin').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        selectedrows=[];
        $('#export_excel_next_kin').show();
        $('#export_world_next_kin').show();
        $('#export_pdf_next_kin').show();
        $('.add_next_kin').show();
        $('#selectedactivebuttonnext_kin').hide();
        $('#deleteselectednext_kin').show();
        $('#deleteselectednext_kinSoft').hide();
        $('#restorebuttonnext_kin').show();
        $('#activebuttonnext_kin').hide();
        var url='/next/kin/getall';
        $('#next_kin_table').DataTable().clear().destroy();
        next_kin_data(url);
    });



});
function restorenext_kindata(id)
{
    var url='/next/kin/active';
    swal({
        title: "Are you sure?",
        text: "Selected option with be active ",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
     },
    function(isConfirm){
        if (isConfirm){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: url,
                data: {'id':id},
                success: function(response)
                {
                    if(response.flag==1)
                    {
                        var url='/next/kin/getall';
                    }
                    else
                    {
                        var url='/next/kin/getallSoft';
                    }
                    $('#next_kin_table').DataTable().clear().destroy();
                    next_kin_data(url);
                    swal.close();
                    toastr["success"](response.message);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        } else {
          swal("Cancelled", "Your Record is safe", "error");

        }
    });
 }
 function delete_mutilpleData_kin(url,selectedrows)
 {
    if( $('.edit_cms_disable').css('display') == 'none' ) {
        console.log('Editer mode On Please change your mode');
        return 0;
    }
    if (selectedrows && selectedrows.length) {
   // not empty
        swal({
            title: "Are you sure?",
            text: "Selected option will be inactive and can be Active again by the admin only!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
         },
        function(isConfirm){

            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url:url,
                    data: {'ids':selectedrows},
                    success: function(response)
                    {
                        console.log(response);
                        toastr["success"](response.message);
                        if(response.flag==1)
                        {
                            var url='/next/kin/getall';
                        }
                        else
                        {
                            var url='/next/kin/getallSoft';
                        }
                        $('#next_kin_table').DataTable().clear().destroy();
                        next_kin_data(url);
                        swal.close();

                    },
                    error: function (error) {
                        console.log(error);
                    }
                });

            } else {
              swal("Cancelled", "Your Record is safe", "error");

            }
        });
     }
    else
    {
   // empty
        return toastr["error"]("Select at least one record to delete!");
    }
 }
 function delete_data_next_kin(id,url)
 {
    swal({
        title: "Are you sure?",
        text: "Selected option with be Deleted ",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
     },
     function(isConfirm){

        if (isConfirm){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: url,
                data: {'id':id},
                success: function(response)
                {
                    if(response.flag==1)
                    {
                        var url='/next/kin/getall';
                    }
                    else
                    {
                        var url='/next/kin/getallSoft';
                    }
                    $('#next_kin_table').DataTable().clear().destroy();
                    next_kin_data(url);
                    swal.close();
                    toastr["success"](response.message);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        } else {
          swal("Cancelled", "Your Record is safe", "error");

        }
     });
 }
function deletesoftnext_kindata(id)
{
    var url='/next/kin/softdelete';
    delete_data_next_kin(id,url);

}
function deletenext_kindata(id)
 {
    var url='/next/kin/delete';
    delete_data_next_kin(id,url);
 }
