 $(document).ready(function() {
    var selectedrows=[];
    $('body').on('click', '.selectedrowcontratormanager', function() {
        var tr_id=$(this).attr('id');
        tr_id=tr_id.split('contratormanager_tr');
        tr_id=tr_id[1];
        var flag=$('#contratormanager'+tr_id).prop('checked');
        if(!flag)
        {
            if(selectedrows.includes(tr_id))
            {

                selectedrows.splice( selectedrows.indexOf(tr_id), 1 );
                $('#contratormanager_tr'+tr_id).removeClass('selected');
                $('.contratormanager_export').val(JSON.stringify(selectedrows));
                $('#contratormanager'+tr_id).attr('checked',false);

            }
            else
            {
                selectedrows.push(tr_id);
                $('#contratormanager_tr'+tr_id).addClass('selected');
                $('.contratormanager_export').val(JSON.stringify(selectedrows));
                $('#contratormanager'+tr_id).attr('checked',true);

            }
        }
    });
    $('#selectedactivebuttoncontratormanager').click(function(){
        var url='/contratormanager/active/multiple';
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        if (selectedrows && selectedrows.length) {
       // not empty
            swal({
                title: "Are you sure?",
                text: "Selected option will be active  again ..!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
             },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:url,
                        data: {'ids':selectedrows},
                        success: function(response)
                        {

                            toastr["success"](response.message);
                            var url='/contratormanager/getallSoft';
                            $('#contratc_manager_table').DataTable().clear().destroy();
                            contratormanager_data(url);
                            swal.close();

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                  swal("Cancelled", "Your Record is safe", "error");

                }
            });
         }
        else
        {
       // empty
            return toastr["error"]("Select at least one record to delete!");
        }
    });
    $('#deleteselectedcontratormanager').click(function(){
       var url='/contratormanager/delete/multiple';
       delete_mutilpleData(url,selectedrows);
    });
    $('#deleteselectedcontratormanagerSoft').click(function(){
       var url='/contratormanager/delete/soft/multiple';
       delete_mutilpleData(url,selectedrows);
    });

    $('.add-contratormanager').on('click', 'i', () => {
      $.ajax({
            type: 'GET',
            url: '/contratormanager/store',
            success: function(response)
            {
                var contractmanagerName_checkbox = $("#contractmanagerName_checkbox").val();
                var contractmanagerName_name = $("#contractmanagerName_name").val();
                var contractmanagerName_position = $("#contractmanagerName_position").val();
                var edit_contractmanagerName = $("#edit_contractmanagerName").val();
                var delete_contractmanagerName = $("#delete_contractmanagerName").val();
                var html='';
                    console.log(response.data.id);
                    html+= '<tr id="contratormanager_tr'+response.data.id+'" class="selectedrowcontratormanager"  role="row" class="odd" >';
                    if (contractmanagerName_checkbox==1){
                        html+='<td class="pt-3-half selectedrowmanager" id="manager_tr'+response.data.id+'"><div class="form-check"><input type="checkbox" class="form-check-input" id="manager'+response.data.id+'"><label class="form-check-label" for="manager'+response.data.id+'""></label></div></td>';
                    }
                    if (delete_contractmanagerName==1){
                        html+='<td class="pt-3-half" id="delete'+response.data.id+'"><a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light" onclick="deletecontratormanagerdata('+response.data.id+')"><i class="fas fa-trash"></i></a></td>';
                    }
                    if (contractmanagerName_name==1){
                        var n_creat =($("#contractmanagerName_name_create").val()==1) ? "true":"false";
                        html+='<td class="pt-3-half edit_inline_contratormanager" data-id="'+response.data.id+'" id="nameCM'+response.data.id+'"   contenteditable="'+n_creat+'">'+response.data.name+'</td>';
                    }
                    if (contractmanagerName_position==1) {
                        var p_create =($("#contractmanagerName_name_create").val()==1) ? "":"disabled";
                        html += '<td class="pt-3-half edit_inline_contratormanager" data-id="' + response.data.id + '" id="position' + response.data.id + '"  >';
                        html += '<div class="form-check float-left">';
                        html += ' <input type="radio" '+p_create+' value="C" ' + checkSelectedContratorManger(response.data.position, 'C') + ' name="cmposition' + response.data.id + '"  class="position' + response.data.id + ' form-check-input" id="cm_yes_position' + response.data.id + '">';
                        html += '<label class="form-check-label" for="cm_yes_position' + response.data.id + '">Yes</label>';
                        html += '</div>';
                        html += '<div class="form-check float-left">';
                        html += ' <input type="radio" '+p_create+' value="0" ' + checkSelectedContratorManger(response.data.position, 0) + '  name="cmposition' + response.data.id + '"  class="position' + response.data.id + ' form-check-input" id="cm_no_position' + response.data.id + '">';
                        html += '<label class="form-check-label" for="cm_no_position' + response.data.id + '">No</label>';
                        html += '</div>';
                        html += '</td>';
                    }
                    if (edit_contractmanagerName==1){
                        html+='<td class="pt-3-half">';
                        html+='<a type="button" class="btn btn-primary btn-xs my-0 waves-effect waves-light save_contractor_manager_data contratormanager_show_button_'+response.data.id+'" style="display: none;" id="'+response.data.id+'"><i class="fas fa-check"></i></a>';
                        html+='</td>';
                    }
                    html+='</tr>';
                $('#contratormanager_data').prepend(html);
            },
            error: function (error) {
                console.log(error);
            }
        });
    });
    $('body').on('click', '.save_contractor_manager_data', function() {
        var company_id=$('#companies_selected_nav').children("option:selected").val();
        var tr_id=$(this).attr('id');
        var data={
            'id':tr_id,
            'name':$('#nameCM'+tr_id).html(),
            'position':$("input[name='cmposition"+tr_id+"']:checked").val(),
            'company_id':company_id,
        };
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url:'/contratormanager/update',
            data:data,
            success: function(response)
            {
                if(response.error)
                {
                    toastr["error"](response.error);
                }
                else
                {
                    toastr["success"](response.message);
                    if(response.flag==1)
                    {
                        $("#contratormanager_tr"+tr_id).attr('style','background-color:#90ee90 !important');

                        // var url='/contratormanager/getall';
                    }
                    else
                    {
                        $("#contratormanager_tr"+tr_id).attr('style','background-color:#ff3547 !important');
                        // var url='/contratormanager/getallsoft';
                    }

                    // $('#contratc_manager_table').DataTable().clear().destroy();//
                    // contratormanager_data(url);

                }

            }

        });

        // console.log(data);

    });

    $('#restorebuttoncontratormanager').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        $('#restorebuttoncontratormanager').hide();
        $('#deleteselectedcontratormanager').hide();
        $('#selectedactivebuttoncontratormanager').show();
        $('#deleteselectedcontratormanagerSoft').show();
        $('#export_excel_contratormanager').hide();
        $('#export_world_contratormanager').hide();
        $('#export_pdf_contratormanager').hide();
        $('.add-contratormanager').hide();
        $('#activebuttoncontratormanager').show();
        var url='/contratormanager/getallSoft';
        $('#contratc_manager_table').DataTable().clear().destroy();
        contratormanager_data(url);
    });
    $('#activebuttoncontratormanager').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        $('#export_excel_contratormanager').show();
        $('#export_world_contratormanager').show();
        $('#export_pdf_contratormanager').show();
        $('.add-contratormanager').show();
        $('#selectedactivebuttoncontratormanager').hide();
        $('#deleteselectedcontratormanager').show();
        $('#deleteselectedcontratormanagerSoft').hide();
        $('#restorebuttoncontratormanager').show();
        $('#activebuttoncontratormanager').hide();
        var url='/contratormanager/getall';
        $('#contratc_manager_table').DataTable().clear().destroy();
        contratormanager_data(url);
    });



});
 function restorecontratormanagerdata(id)
 {
    var url='/contratormanager/active';
    swal({
        title: "Are you sure?",
        text: "Selected option with be active ",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
     },
     function(isConfirm){

       if (isConfirm){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
             $.ajax({
                        type: "POST",
                        url: url,
                        data: {'id':id},
                        success: function(response)
                        {
                            if(response.flag==1)
                            {
                                var url='/contratormanager/getall';
                            }
                            else
                            {
                                var url='/contratormanager/getallSoft';
                            }
                            $('#contratc_manager_table').DataTable().clear().destroy();
                            contratormanager_data(url);
                            swal.close();
                            toastr["success"](response.message);
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
        } else {
          swal("Cancelled", "Your Record is safe", "error");

        }
     });
 }
 function delete_mutilpleData(url,selectedrows)
 {
     if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        if (selectedrows && selectedrows.length) {
       // not empty
            swal({
                title: "Are you sure?",
                text: "Selected option will be inactive and can be Active again by the admin only!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
             },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:url,
                        data: {'ids':selectedrows},
                        success: function(response)
                        {

                            toastr["success"](response.message);
                            if(response.flag==1)
                            {
                                var url='/contratormanager/getall';
                            }
                            else
                            {
                                var url='/contratormanager/getallSoft';
                            }
                            $('#contratc_manager_table').DataTable().clear().destroy();
                            contratormanager_data(url);
                            swal.close();

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                  swal("Cancelled", "Your Record is safe", "error");

                }
            });
         }
        else
        {
       // empty
            return toastr["error"]("Select at least one record to delete!");
        }
 }
 function delete_contractor_manager_data(id,url)
 {
    swal({
        title: "Are you sure?",
        text: "Selected option with be Deleted ",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
     },
     function(isConfirm){

       if (isConfirm){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
             $.ajax({
                        type: "POST",
                        url: url,
                        data: {'id':id},
                        success: function(response)
                        {
                            if(response.flag==1)
                            {
                                var contractor_manager_url='/contratormanager/getall';
                            }
                            else
                            {
                                var contractor_manager_url='/contratormanager/getallSoft';
                            }
                            $('#contratc_manager_table').DataTable().clear().destroy();
                            contratormanager_data(contractor_manager_url);
                            swal.close();
                            toastr["success"](response.message);
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
        } else {
          swal("Cancelled", "Your Record is safe", "error");

        }
     });
 }
function deletesoftcontratormanagerdata(id)
{
    var url='/contratormanager/softdelete';
    delete_contractor_manager_data(id,url);

}
function deletecontratormanagerdata(id)
 {
    var url='/contratormanager/delete';
    delete_contractor_manager_data(id,url);
 }
