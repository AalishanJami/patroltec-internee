$(document).ready(function() {
  var selectedrows=[];
  $('body').on('click','#site_group_checkbox_all',function() {
      $('.selectedrowsitegroup').not(this).prop('checked', this.checked);
      $('.selectedrowsitegroup').each(function(key,value) {
          var id=$(this).attr('id');
          console.log(id);
          if(id)
          {
              id=id.split('sitegroup');
              row_id='#sitegroup_tr'+id[1];
              if($(this).is(":checked"))
              {
                  if(key == 0)
                  {
                      selectedrows=[];
                  }
                  if(id[1] != '_checkbox_all')
                  {
                      selectedrows.push(id[1]);
                  }
                  $('.sitegroup_export').val(JSON.stringify(selectedrows));
                  $(row_id).addClass('selected');
              }
              else
              {
                  $('.sitegroup_export').val(1);
                  selectedrows=[];
                  $(row_id).removeClass('selected');
              }
          }
      });
  });
  $('body').on('click', '.selectedrowsitegroup', function() {
      var id=$(this).attr('id');
      id=id.split('sitegroup');
      row_id='#sitegroup_tr'+id[1];
      if($(this).is(":checked"))
      {
          selectedrows.push(id[1]);
          $('.sitegroup_export').val(JSON.stringify(selectedrows));
          $(row_id).addClass('selected');
      }else
      {
          if(selectedrows.includes(id[1]))
          {
              selectedrows.splice( selectedrows.indexOf(id[1]),1);
              $('.sitegroup_export').val(JSON.stringify(selectedrows));
              $(row_id).removeClass('selected');
          }
      }
  });
  $('#selectedactivebuttonsitegroup').click(function(){
      var url='/sitegroup/active/multiple';
      if( $('.edit_cms_disable').css('display') == 'none' ) {
          console.log('Editer mode On Please change your mode');
          return 0;
      }
      if(selectedrows && selectedrows.length) {
        swal({
            title: "Are you sure?",
            text: "Selected option will be active  again ..!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
         },
        function(isConfirm){

            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url:url,
                    data: {'ids':selectedrows},
                    success: function(response)
                    {
                        // customer_table
                        toastr["success"](response.message);
                        var url='/sitegroup/getallSoft';
                        $('#site_group_table').DataTable().clear().destroy();
                        sitegroup_data(url);
                        swal.close();

                    },
                    error: function (error) {
                        console.log(error);
                    }
                });

            } else {
              swal("Cancelled", "Your Record is safe", "error");

            }
        });
       }
      else
      {
        return toastr["error"]("Select at least one record to delete!");
      }
  });
  $('#deleteselectedsitegroup').click(function(){
     var url='/sitegroup/delete/multiple';
     delete_mutilpleData_site_group(url,selectedrows);
  });
  $('#deleteselectedsitegroupSoft').click(function(){
    var url='/sitegroup/delete/soft/multiple';
    delete_mutilpleData_site_group(url,selectedrows);
  });

  $('.add-sitegroup').on('click', 'i', () => {
    $.ajax({
        type: 'GET',
        url: '/sitegroup/store',
        success: function(response)
        {
            var html='';
            var n_create = ($("#group_name_create").val()==1) ? "true":"false";
            var c_create=($("#group_customer_create").val()==1)? "true":"false";
            var c_disable=($("#group_customer_create").val()==1)? "":"disabled";
            var d_create=($("#group_division_create").val()==1)? "":"disabled";
            var d_diable=($("#group_division_create").val()==1)? "true":"false";
            html+= '<tr id="sitegroup_tr'+response.data.id+'" role="row" class="hide odd">';
            html+=' <td><div class="form-check"><input type="checkbox" class="form-check-input" id="sitegroup'+response.data.id+'"><label class="form-check-label" for="sitegroup'+response.data.id+'"></label></div></td>';
            html+='<td class="pt-3-half edit_inline_sitegroup" data-id="'+response.data.id+'" onkeypress="editSelectedRow('+"sitegroup_tr"+response.data.id+')" id="nameS'+response.data.id+'"   contenteditable="'+n_create+'"></td>';

            // html+='<td class="pt-3-half edit_inline_sitegroup" data-id="'+response.data.id+'"  contenteditable="'+d_diable+'">';
            // html+='<select id="division_id_site'+response.data.id+'"  style="background-color: inherit;border: 0px">';
            // for (var j = response.divisions.length - 1; j >= 0; j--) {
            //     html+='<option '+d_create+' value="'+response.divisions[j].id+'"  >'+response.divisions[j].name+'</option>';
            // }
            // html+='</select></td>';
            // html+='<td class="pt-3-half edit_inline_sitegroup" data-id="'+response.data.id+'"   contenteditable="'+c_create+'">';
            // html+='<select id="customer_id_site'+response.data.id+'"  style="background-color: inherit;border: 0px">';
            // for (var j = response.customers.length - 1; j >= 0; j--) {
            //     html+='<option '+c_disable+' value="'+response.customers[j].id+'"  >'+response.customers[j].name+'</option>';
            // }
            // html+='</select></td>';

            html+='<td><a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light delete-btn'+response.data.id+'" onclick="deletesitegroupdata('+response.data.id+')"><i class="fas fa-trash"></i></a><a type="button" class="btn btn-primary all_action_btn_margin btn-xs my-0 waves-effect waves-light savesitegroupdata sitegroup_show_button_'+response.data.id+' show_tick_btn'+response.data.id+'" style="display: none;" id="'+response.data.id+'"><i class="fas fa-check"></i></a></td>'
            html+='</tr>';
            $('#table-sitegroup').find('table').prepend(html);

        },
        error: function (error) {
            console.log(error);
        }
    });
  });
  $('body').on('click', '.savesitegroupdata', function() {
        var company_id=$('#companies_selected_nav').children("option:selected").val();
        var tr_id=$(this).attr('id');
        var division_id_site = $('#division_id').children("option:selected").val();
        var customer_id_site = $('#customer_id').children("option:selected").val();
        var n_create = $("#group_name_create").val();
        var c_create=$("#group_customer_create").val();
        var d_create=$("#group_division_create").val();
        var data={
            'id':tr_id,
            'name':$('#nameS'+tr_id).html(),
            'division_id':division_id_site,
            'customer_id':customer_id_site,
            'company_id':company_id,

        };
        if (c_create=='1' || d_create=="1"){
            if (division_id_site =="" && customer_id_site==""){
                toastr["error"](['Please first add division and customer']);
                return 0;
            }else{
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type:"POST",
                    url:'/sitegroup/update',
                    data:data,
                    success: function(response)
                    {
                        if(response.error)
                        {
                            toastr["error"](response.error);
                        } else {
                            toastr["success"](response.message);
                            save_and_delete_btn_toggle(tr_id);
                            if(response.flag==1)
                            {
                                $("#sitegroup_tr"+tr_id).attr('style','background-color:#90ee90 !important');
                                // var url='/sitegroup/getall';
                            } else {
                                $("#sitegroup_tr"+tr_id).attr('style','background-color:#ff3547 !important');
                                // var url='/sitegroup/getallsoft';
                            }
                            // $('#site_group_table').DataTable().clear().destroy();
                            // sitegroup_data(url);

                        }
                    }
                });
            }
        }else if (n_create=='1'){
            if($('#nameS'+tr_id).html() ==""){
                toastr["error"](['Name field required']);
                return 0;
            }else{
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type:"POST",
                    url:'/sitegroup/update',
                    data:data,
                    success: function(response)
                    {
                        if(response.error)
                        {
                            toastr["error"](response.error);
                        } else {
                            toastr["success"](response.message);
                            if(response.flag==1) {
                                $("#sitegroup_tr"+tr_id).attr('style','background-color:#90ee90 !important');
                                // var url='/sitegroup/getall';
                            } else {
                                $("#sitegroup_tr"+tr_id).attr('style','background-color:#ff3547 !important');
                                // var url='/sitegroup/getallsoft';
                            }
                            // $('#site_group_table').DataTable().clear().destroy();
                            // sitegroup_data(url);

                        }
                    }
                });
            }
        }else{
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type:"POST",
                url:'/sitegroup/update',
                data:data,
                success: function(response)
                {
                    if(response.error)
                    {
                        toastr["error"](response.error);
                    }
                    else
                    {
                        toastr["success"](response.message);
                        if(response.flag==1)
                        {
                            $("#sitegroup_tr"+tr_id).attr('style','background-color:#90ee90 !important');
                            // var url='/sitegroup/getall';
                        }
                        else
                        {
                            $("#sitegroup_tr"+tr_id).attr('style','background-color:#ff3547 !important');
                            // var url='/sitegroup/getallsoft';
                        }
                        // $('#site_group_table').DataTable().clear().destroy();
                        // sitegroup_data(url);

                    }

                }

            });
        }

  });
  $('#restorebuttonsitegroup').click(function(){
    if( $('.edit_cms_disable').css('display') == 'none' ) {
      console.log('Editer mode On Please change your mode');
      return 0;
    }
    selectedrows=[];
    $('#restorebuttonsitegroup').hide();
    $('#deleteselectedsitegroup').hide();
    $('#selectedactivebuttonsitegroup').show();
    $('#deleteselectedsitegroupSoft').show();
    $('#export_excel_sitegroup').hide();
    $('#export_world_sitegroup').hide();
    $('#export_pdf_sitegroup').hide();
    $('.add-sitegroup').hide();
    $('#activebuttonsitegroup').show();
    var url='/sitegroup/getallSoft';
    $('#site_group_table').DataTable().clear().destroy();
    sitegroup_data(url);
  });
  $('#activebuttonsitegroup').click(function(){
    selectedrows=[];
    $('#export_excel_sitegroup').show();
    $('#export_world_sitegroup').show();
    $('#export_pdf_sitegroup').show();
    $('.add-sitegroup').show();
    $('#selectedactivebuttonsitegroup').hide();
    $('#deleteselectedsitegroup').show();
    $('#deleteselectedsitegroupSoft').hide();
    $('#restorebuttonsitegroup').show();
    $('#activebuttonsitegroup').hide();
    var url='/sitegroup/getall';
    $('#site_group_table').DataTable().clear().destroy();
    sitegroup_data(url);
  });
});
function restoresitegroupdata(id)
{
  var url='/sitegroup/active';
  swal({
      title: "Are you sure?",
      text: "Selected option with be active ",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Yes, I am sure!',
      cancelButtonText: "No, cancel it!",
      closeOnConfirm: false,
      closeOnCancel: false
   },
   function(isConfirm){

     if (isConfirm){
          $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
          });
           $.ajax({
                      type: "POST",
                      url: url,
                      data: {'id':id},
                      success: function(response)
                      {
                          if(response.flag==1)
                          {
                              var url='/sitegroup/getall';
                          }
                          else
                          {
                              var url='/sitegroup/getallSoft';
                          }
                          $('#site_group_table').DataTable().clear().destroy();
                  sitegroup_data(url);
                          swal.close();
                          toastr["success"](response.message);
                      },
                      error: function (error) {
                          console.log(error);
                      }
                  });
      } else {
        swal("Cancelled", "Your Record is safe", "error");

      }
   });
}
 function delete_mutilpleData_site_group(url,selectedrows)
 {
    if( $('.edit_cms_disable').css('display') == 'none' ) {
      console.log('Editer mode On Please change your mode');
      return 0;
    }
    if (selectedrows && selectedrows.length) {
      // not empty
        swal({
            title: "Are you sure?",
            text: "Selected option will be inactive and can be Active again by the admin only!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
         },
        function(isConfirm){

            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url:url,
                    data: {'ids':selectedrows},
                    success: function(response)
                    {

                        toastr["success"](response.message);
                        if(response.flag==1)
                        {
                            var url='/sitegroup/getall';
                        }
                        else
                        {
                            var url='/sitegroup/getallSoft';
                        }
                        $('#site_group_table').DataTable().clear().destroy();
                        sitegroup_data(url);
                        swal.close();

                    },
                    error: function (error) {
                        console.log(error);
                    }
                });

            } else {
              swal("Cancelled", "Your Record is safe", "error");

            }
        });
     }
    else
    {
   // empty
        return toastr["error"]("Select at least one record to delete!");
    }
 }
 function delete_data_group(id,url)
 {
    swal({
        title: "Are you sure?",
        text: "Selected option with be Deleted ",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
     },
     function(isConfirm){

       if (isConfirm){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
             $.ajax({
                        type: "POST",
                        url: url,
                        data: {'id':id},
                        success: function(response)
                        {
                            if(response.flag==1)
                            {
                                var url='/sitegroup/getall';
                            }
                            else
                            {
                                var url='/sitegroup/getallSoft';
                            }
                            $('#site_group_table').DataTable().clear().destroy();
                    sitegroup_data(url);
                            swal.close();
                            toastr["success"](response.message);
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
        } else {
          swal("Cancelled", "Your Record is safe", "error");

        }
     });
 }
function deletesoftsitegroupdata(id)
{
    var url='/sitegroup/softdelete';
    delete_data_group(id,url);

}
function deletesitegroupdata(id)
 {
    var url='/sitegroup/delete';
    delete_data_group(id,url);
 }
