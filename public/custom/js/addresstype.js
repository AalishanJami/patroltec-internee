 $(document).ready(function() {
    var selectedrows=[];
     $('body').on('click','#addresstype_checkbox_all',function() {
         var x = document.getElementsByClassName("selectedrowaddresstype");
         var i;
         for (i = 0; i < x.length;i++) {
             var tr_id=x[i].id;
             tr_id=tr_id.split('addresstype');
             tr_id=tr_id[1];
             if(selectedrows.includes(tr_id))
             {
                 selectedrows.splice( selectedrows.indexOf(tr_id), 1 );
                 if (selectedrows.length >0){
                     $('.addresstype_export').val(JSON.stringify(selectedrows));
                 }else {
                     $('.addresstype_export').val('1');
                 }
                 $('#addresstype_tr'+tr_id).removeClass('selected');
                 $('#addresstype'+tr_id).attr('checked',false);
             }
             else
             {
                 selectedrows.push(tr_id);
                 $('.addresstype_export').val(JSON.stringify(selectedrows));
                 $('#addresstype_tr'+tr_id).addClass('selected');
                 $('#addresstype'+tr_id).attr('checked',true);
             }
         }

     });

     $('body').on('click', '.selectedrowaddresstype', function() {
        var tr_id=$(this).attr('id');
        tr_id=tr_id.split('addresstype');
        tr_id=tr_id[1];
        if(selectedrows.includes(tr_id))
        {
            selectedrows.splice( selectedrows.indexOf(tr_id), 1 );
            if (selectedrows.length >0){
                $('.addresstype_export').val(JSON.stringify(selectedrows));
            }else {
                $('.addresstype_export').val('1');
            }
            $('#addresstype_tr'+tr_id).removeClass('selected');
            $('#addresstype'+tr_id).attr('checked',false);
        }
        else
        {
            selectedrows.push(tr_id);
            $('.addresstype_export').val(JSON.stringify(selectedrows));
            $('#addresstype_tr'+tr_id).addClass('selected');
            $('#addresstype'+tr_id).attr('checked',true);
        }
    });

    $('#selectedactivebuttonaddresstype').click(function(){
        var url='/addresstype/active/multiple';
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        if (selectedrows && selectedrows.length) {
       // not empty
            swal({
                title: "Are you sure?",
                text: "Selected option will be active  again ..!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
             },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:url,
                        data: {'ids':selectedrows},
                        success: function(response)
                        {
                            toastr["success"](response.message);
                            var url='/addresstype/getallSoft';
                            $('.address_table').DataTable().clear().destroy();
                            addresstype_data(url);
                            swal.close();

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                  swal("Cancelled", "Your Record is safe", "error");

                }
            });
         }
        else
        {
       // empty
            return toastr["error"]("Select at least one record to delete!");
        }
    });
    $('#deleteselectedaddresstype').click(function(){
       var url='/addresstype/delete/multiple';
       delete_mutilpleDataAddressType(url,selectedrows);
    });
    $('#deleteselectedaddresstypeSoft').click(function(){
       var url='/addresstype/delete/soft/multiple';
       delete_mutilpleDataAddressType(url,selectedrows);
    });
    $('.add-addresstype').on('click', 'i', () => {
        $.ajax({
            type: 'GET',
            url: '/addresstype/store',
            success: function(response)
            {
                var html='';
                html+= '<tr id="addresstype_tr'+response.data.id+'"  role="row" class="hide odd">';
                html+=' <td><div class="form-check"><input type="checkbox" class="form-check-input selectedrowaddresstype addresstype_checked" id="addresstype'+response.data.id+'"><label class="form-check-label" for="addresstype'+response.data.id+'"></label></div></td>';
                html+='<td onkeypress="editSelectedRow(addresstype_tr'+response.data.id+')" class="pt-3-half edit_inline_addresstype" data-id="'+response.data.id+'" id="nameD'+response.data.id+'" ';
                if($('#addressType_name_create').val()=='1')
                {
                    html+='contenteditable="true" ';
                }
                html+='></td>';
                html+='<td>';
                if($('#delete_addressType').val()=='1')
                {
                    html+='<a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light delete-btn'+response.data.id+'" onclick="deleteaddresstypedata('+response.data.id+')"><i class="fas fa-trash"></i></a>';
                }
                if($('#addressType_name_create').val()=='1')
                {
                    html+='<a type="button" class="btn btn-primary btn-xs all_action_btn_margin waves-effect waves-light saveaddresstypedata show_tick_btn'+response.data.id+'" style="display: none;"  id="'+response.data.id+'"><i class="fas fa-check"></i></a>';
                }
                html+='</td>';
                html+='</tr>';
                $('#table-addresstype').find('table').prepend(html);
            },
            error: function (error) {
                console.log(error);
            }
        });
    });
    $('body').on('click', '.saveaddresstypedata', function() {
        var company_id=$('#companies_selected_nav').children("option:selected").val();
        var tr_id=$(this).attr('id');
        var data={
            'id':tr_id,
            'name':$('#nameD'+tr_id).html(),
            'company_id':company_id,

        };
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url:'/addresstype/update',
            data:data,
            success: function(response)
            {
                if(response.error)
                {
                    toastr["error"](response.error);
                    $("#addresstype_tr"+tr_id).attr('style','background-color:#ff3547 !important');
                }
                else
                {
                    toastr["success"](response.message);
                    if(response.flag==1)
                    {
                        $("#addresstype_tr"+tr_id).attr('style','background-color:#90ee90 !important');
                        // var url='/addresstype/getall';
                    }
                    else
                    {
                        $("#addresstype_tr"+tr_id).attr('style','background-color:#ff3547 !important');
                        // var url='/addresstype/getallsoft';
                    }

                     save_and_delete_btn_toggle(tr_id);


                    // $('.address_table').DataTable().clear().destroy();
                    //         addresstype_data(url);

                }

            }

        });

        // console.log(data);

    });
    $('#restorebuttonaddresstype').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        selectedrows=[];
        $('#restorebuttonaddresstype').hide();
        $('#deleteselectedaddresstype').hide();
        $('#selectedactivebuttonaddresstype').show();
        $('#deleteselectedaddresstypeSoft').show();
        $('#export_excel_addresstype').hide();
        $('#export_world_addresstype').hide();
        $('#export_pdf_addresstype').hide();
        $('.add-addresstype').hide();
        $('#activebuttonaddresstype').show();
        var url='/addresstype/getallSoft';
        $('.address_table').DataTable().clear().destroy();
        addresstype_data(url);
    });
    $('#activebuttonaddresstype').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        selectedrows=[];
        $('#export_excel_addresstype').show();
        $('#export_world_addresstype').show();
        $('#export_pdf_addresstype').show();
        $('.add-addresstype').show();
        $('#selectedactivebuttonaddresstype').hide();
        $('#deleteselectedaddresstype').show();
        $('#deleteselectedaddresstypeSoft').hide();
        $('#restorebuttonaddresstype').show();
        $('#activebuttonaddresstype').hide();
        var url='/addresstype/getall';
        $('.address_table').DataTable().clear().destroy();
        addresstype_data(url);
    });
});
 function restoreaddresstypedata(id)
 {
    var url='/addresstype/active';
    swal({
        title: "Are you sure?",
        text: "Selected option with be active ",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
     },
     function(isConfirm){
       if (isConfirm){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: url,
                data: {'id':id},
                success: function(response)
                {
                    if(response.flag==1)
                    {
                        var url='/addresstype/getall';
                    }
                    else
                    {
                        var url='/addresstype/getallSoft';
                    }
                    $('.address_table').DataTable().clear().destroy();
                    addresstype_data(url);
                    swal.close();
                    toastr["success"](response.message);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        } else {
          swal("Cancelled", "Your Record is safe", "error");

        }
     });
 }
 function delete_mutilpleDataAddressType(url,selectedrows)
 {
     if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        if (selectedrows && selectedrows.length) {
       // not empty
            swal({
                title: "Are you sure?",
                text: "Selected option will be inactive and can be Active again by the admin only!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
             },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:url,
                        data: {'ids':selectedrows},
                        success: function(response)
                        {

                            toastr["success"](response.message);
                            if(response.flag==1)
                            {
                                var url='/addresstype/getall';
                            }
                            else
                            {
                                var url='/addresstype/getallSoft';
                            }
                            $('.address_table').DataTable().clear().destroy();
                            addresstype_data(url);
                            swal.close();

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                  swal("Cancelled", "Your Record is safe", "error");

                }
            });
         }
        else
        {
       // empty
            return toastr["error"]("Select at least one record to delete!");
        }
 }
function delete_data_addressType(id,url)
{
    swal({
        title: "Are you sure?",
        text: "Selected option with be Deleted ",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
     },
     function(isConfirm){

       if (isConfirm){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: url,
                data: {'id':id},
                success: function(response)
                {
                    if(response.flag==1)
                    {
                        var url='/addresstype/getall';
                    }
                    else
                    {
                        var url='/addresstype/getallSoft';
                    }
                    $('.address_table').DataTable().clear().destroy();
                    addresstype_data(url);
                    swal.close();
                    toastr["success"](response.message);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        } else {
          swal("Cancelled", "Your Record is safe", "error");

        }
     });
}
function deletesoftaddresstypedata(id)
{
    var url='/addresstype/softdelete';
    delete_data_addressType(id,url);

}
function deleteaddresstypedata(id)
 {
    var url='/addresstype/delete';
    delete_data_addressType(id,url);
 }
