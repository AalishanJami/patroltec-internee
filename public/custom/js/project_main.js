var selectedrows=[];
$(document).ready(function () {
    $('body').on('click','#project_checkbox_all',function() {
        $('input:checkbox').not(this).prop('checked', this.checked);
        $('input[type="checkbox"]').each(function(key,value) {
            var id=$(this).attr('id');
            id=id.split('project');
            row_id='#project_tr'+id[1];
            contract_end_id='.contract_end'+id[1];
            if($(this).is(":checked"))
            {
                if(key == 0)
                {
                    selectedrows=[];
                }
                if(id[1] != '_checkbox_all')
                {
                    selectedrows.push(id[1]);
                }
                $('.project_export').val(JSON.stringify(selectedrows));
                $(row_id).addClass('selected');
            }
            else
            {
                $('.project_export').val(1);
                selectedrows=[];
                $(row_id).removeClass('selected');
                var contract_end=$(contract_end_id).val();
                $(row_id).attr('style','background-color:'+contract_end+';');
            }
        });

    });
    $('body').on('click', '.selectedrowproject', function() {
        var id=$(this).attr('id');
        id=id.split('project');
        row_id='#project_tr'+id[1];
        var contract_end_id='.contract_end'+id[1];
        if($(this).is(":checked"))
        {
            selectedrows.push(id[1]);
            $('.project_export').val(JSON.stringify(selectedrows));
            $(row_id).addClass('selected');
        }else {
            if(selectedrows.includes(id[1]))
            {
                selectedrows.splice( selectedrows.indexOf(id[1]),1);
                $('.project_export').val(JSON.stringify(selectedrows));
                $(row_id).removeClass('selected');
                var contract_end=$(contract_end_id).val();
                $(row_id).attr('style','background-color:'+contract_end+';');
            }
        }
    });

    $('#deleteSelectedProject').click(function(e) {
        console.log("Selected Rows : "+selectedrows);
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        if (selectedrows && selectedrows.length)
        {
            swal({
                title: "Are you sure?",
                text: "Locations will be inactive and can be Active again by the admin only!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
             },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:'/project/multipleDelete',
                        data: {'ids':selectedrows,'flag': $('#flagProject').val()},
                        success: function(response)
                        {

                            $('#project_table').DataTable().clear().destroy();
                            if($('#flagProject').val()==1)
                            {
                                var url='/project/getAll';
                            }
                            else
                            {
                                var url='/project/getAllSoft';
                            }
                            selectedrows=[];
                            projectAppend(url);
                            swal.close()
                            toastr["success"](response.message);

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                  swal("Cancelled", "Your Record is safe", "error");
                     e.preventDefault();
                }
            });
        }
        else
        {
            return toastr["error"]("Select at least one record to delete!");
        }
    });
    $('#restore_button_project').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        selectedrows=[];
        $('.project_export').val(1);
        $('#restore_button_project').hide();
        $('#export_excel_project').hide();
        $('#export_pdf_project').hide();
        $('#export_world_project').hide();
        $('#selectedactivebuttonproject').show();
        $('#flagProject').val(2);
        $('#show_active_button_project').show();
        $('#project_table').DataTable().clear().destroy();
        var url='/project/getAllSoft';
        projectAppend(url);
    });
    $('#show_active_button_project').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        selectedrows=[];
        $('#export_excel_project').show();
        $('#export_pdf_project').show();
        $('#export_world_project').show();
        $('#selectedactivebuttonproject').hide();
        $('#show_active_button_project').hide();
        $('#flagProject').val(1);
        $('#restore_button_project').show();
        $('#project_table').DataTable().clear().destroy();
        var url='/project/getAll';
        projectAppend(url);
    });
    $('#selectedactivebuttonproject').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        var url='/project/active/multiple';
        if (selectedrows && selectedrows.length) {
       // not empty
            swal({
                title: "Are you sure?",
                text: "Selected option will be active  again ..!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
             },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:url,
                        data: {'ids':selectedrows},
                        success: function(response)
                        {
                            toastr["success"](response.message);
                            $('#project_table').DataTable().clear().destroy();
                            var url='/project/getAllSoft';
                            selectedrows=[];
                            projectAppend(url);
                            swal.close();
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                  swal("Cancelled", "Your Record is safe", "error");

                }
            });
         }
        else
        {
       // empty
            return toastr["error"]("Select at least one record to delete!");
        }
    });
});
function save(){
    if( $('.edit_cms_disable').css('display') == 'none' ) {
        console.log('Editer mode On Please change your mode');
        return 0;
    }
    var formData = $("#projectForm").serializeObject();
    var company_id = $('#companies_selected_nav').children("option:selected").val();
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: '/project/store',
        data: {'company_id':company_id},
        success: function(data)
        {
            $('#project_table').DataTable().clear().destroy();
            var url='/project/getAll';
            projectAppend(url);
            toastr["success"](data.message);
            window.location='/project/detail/'+data.location_id;
            $('#modalCreateProject').modal('hide');
        },
        error: function (error) {
            console.log(error);
        }
    });
}
function deleteProject(id) {
    swal({
            title:'Are you sure?',
            text: "Delete Record.",
            type: "error",
            showCloseButton: true,
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Delete!",
            cancelButtonText: "Cancel!",
            closeOnConfirm: false,
            customClass: "confirm_class",
            closeOnCancel: false,
        },
    function(isConfirm){
        console.log( isConfirm);
        if( $('.edit_cms_disable').css('display') != 'none' ) {
            if (isConfirm) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type : 'post',
                    url : '/project/delete',
                    data:{'id':id},
                    success:function(data)
                    {

                        setTimeout(function () {
                        swal({
                                title: "",
                                text: "Delete Sucessfully!",
                                type: "success",
                                confirmButtonText: "OK"
                            },

                            function(isConfirm){
                                if (isConfirm) {
                                    $('#project_table').DataTable().clear().destroy();
                                    var url='/project/getAll';
                                    projectAppend(url);
                                }
                            }); }, 500);
                    }
                });
            }
            else
            {
               swal.close();
            }
        }
    });
}
function restoreProject(id)
{
    swal({
        title: "Are you sure?",
        text: "Selected Location will be Active again!",
        type: "warning",
        showCloseButton: true,
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        customClass: "confirm_class",
        closeOnCancel: false
    },
    function(isConfirm){
        if( $('.edit_cms_disable').css('display') != 'none' ) {
           if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url:'/project/restore',
                    data: {'id':id},
                    success: function(response)
                    {
                        $('#project_table').DataTable().ajax.reload();
                        swal.close()
                        toastr["success"](response.message);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }
            else {
                 swal.close();
            }
        }
    });

}
function hardDeleteProject(id)
{
    swal({
    title: "Are you sure?",
    text: "Selected Location with be Hard Deleted and can active from backend again!",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: '#DD6B55',
    confirmButtonText: 'Yes, I am sure!',
    cancelButtonText: "No, cancel it!",
    closeOnConfirm: false,
    closeOnCancel: false
    },
    function(isConfirm){

    if (isConfirm){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: '/project/hardDelete',
            data: {'id':id},
            success: function(response)
            {
                $('#project_table').DataTable().clear().destroy();
                var url='/project/getAll';
                projectAppend(url);
                swal.close()
                toastr["success"](response.message);
            },
            error: function (error) {
                console.log(error);
            }
        });
    } else {
      swal("Cancelled", "Your Record is safe", "error");
    }
    });
}
