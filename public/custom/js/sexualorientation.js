$(document).ready(function() {
    var selectedrows=[];
    $('body').on('click','#sexualorientation_checkbox_all',function() {
        $('.selectedrowsexualorientation').not(this).prop('checked', this.checked);
        $('.selectedrowsexualorientation').each(function(key,value) {
            var id=$(this).attr('id');
            console.log(id);
            if(id)
            {
                id=id.split('sexualorientation_checked');
                row_id='#sexualorientation_tr'+id[1];
                if($(this).is(":checked"))
                {
                    if(key == 0)
                    {
                        selectedrows=[];
                    }
                    if(id[1] != '_checkbox_all')
                    {
                        selectedrows.push(id[1]);
                    }
                    $('.sexualorientation_export').val(JSON.stringify(selectedrows));
                    $(row_id).addClass('selected');
                }
                else
                {
                    $('.sexualorientation_export').val(1);
                    selectedrows=[];
                    $(row_id).removeClass('selected');
                }
            }
        });
    });


    $('body').on('click', '.selectedrowsexualorientation', function() {
        var id=$(this).attr('id');
        id=id.split('sexualorientation_checked');
        row_id='#sexualorientation_tr'+id[1];
        if($(this).is(":checked"))
        {
            selectedrows.push(id[1]);
            $('.sexualorientation_export').val(JSON.stringify(selectedrows));
            $(row_id).addClass('selected');
        }else
        {
            if(selectedrows.includes(id[1]))
            {
                selectedrows.splice( selectedrows.indexOf(id[1]),1);
                $('.sexualorientation_export').val(JSON.stringify(selectedrows));
                $(row_id).removeClass('selected');
            }
        }
    });
    $('body').on('click', '.addSexualOrientationModal', () => {
        $.ajax({
            type: 'GET',
            url: '/employee/sexualorientation/store',
            success: function(response)
            {
               var html='';
                html+='<tr id="sexualorientation_tr'+response.data.id+'" class="selectedrowsexualorientation'+response.data.id+'" role="row" class="hide odd">';
                html+='<td>';
                if ($("#employeeSexualoren_checkbox").val()==1){
                    html+='<div class="form-check"><input type="checkbox" class="form-check-input sexualorientation_checked selectedrowsexualorientation" id="sexualorientation_checked'+response.data.id+'"><label class="form-check-label" for="sexualorientation_checked'+response.data.id+'""></label></div>';
                }
                html+='</td>';
                if ($("#employeeSexualoren_name").val()==1){
                    var n_create=($("#employeeSexualoren_name_create").val()==1 && $("#create_employeeSexualoren").val()==1) ? "true":"false";
                    html+='<td class="pt-3-half edit_inline_sexual" onkeypress="editSelectedRow('+"sexualorientation_tr"+response.data.id+')" data-id="'+response.data.id+'" id="sexualorientation_name'+response.data.id+'"   contenteditable="'+n_create+'"></td>';
                }
                html+='<td>';
                if ($("#delete_employeeSexualoren").val()==1){
                    html+='<a type="button" class="btn deletesexualorientationdata'+response.data.id+' btn-danger btn-xs my-0 waves-effect waves-light" onclick="deletesexualorientationdata( ' +response.data.id+')"><i class="fas fa-trash"></i></a>';
                }
                if ($("#employeeSexualoren_name_edit").val()==1){
                    html+='<a type="button" class="btn btn-primary btn-xs my-0 all_action_btn_margin waves-effect waves-light saveSexualorientationdata sexual_show_button_'+response.data.id+'" style="display: none;"  id="'+response.data.id+'"><i class="fas fa-check"></i></a>';
                }
                html+='</td>';
                html+='</tr>';
                $('#table_sexualorientation').find('table').prepend(html);
            },
            error: function (error) {
                console.log(error);
            }
        });


    });
    $('#selectedactivebuttonsexualorientation').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        var url='/employee/sexualorientation/active/multiple';
        if (selectedrows && selectedrows.length) {
       // not empty
            swal({
                title: "Are you sure?",
                text: "Selected option will be active  again ..!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
             },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:url,
                        data: {'ids':selectedrows},
                        success: function(response)
                        {

                            toastr["success"](response.message);
                            var url='/employee/sexualorientation/getallSoft';
                            $('#sexual_orientation_table').DataTable().clear().destroy();
                            sexual_orientation_data(url);
                            swal.close();

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                  swal("Cancelled", "Your Record is safe", "error");

                }
            });
         }
        else
        {
       // empty
            return toastr["error"]("Select at least one record to delete!");
        }
    });
    $('#deleteselectedsexualorientation').click(function(){
       var url='/employee/sexualorientation/delete/multiple';
       delete_mutilpleDatasexualorientation(url,selectedrows);
    });
    $('#deleteselectedsexualorientationSoft').click(function(){
       var url='/employee/sexualorientation/delete/soft/multiple';
       delete_mutilpleDatasexualorientation(url,selectedrows);
    });
    $('#restorebuttonsexualorientation').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        $('#export_excel_sexualorientation_btn').hide();
        $('#export_world_sexualorientation_btn').hide();
        $('#export_pdf_sexualorientation_btn').hide();
        $('#restorebuttonsexualorientation').hide();
        $('#deleteselectedsexualorientation').hide();
        selectedrows=[];
        $('#selectedactivebuttonsexualorientation').show();
        $('#deleteselectedsexualorientationSoft').show();
        $('.addSexualOrientationModal').hide();
        $('#activebuttonsexualorientation').show();
        var url='/employee/sexualorientation/getallSoft';
        $('#sexual_orientation_table').DataTable().clear().destroy();
        sexual_orientation_data(url);
    });
    $('#activebuttonsexualorientation').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        $('#export_excel_sexualorientation_btn').show();
        $('#export_world_sexualorientation_btn').show();
        $('#export_pdf_sexualorientation_btn').show();
        $('.addSexualOrientationModal').show();
        selectedrows=[];
        $('#selectedactivebuttonsexualorientation').hide();
        $('#deleteselectedsexualorientation').show();
        $('#deleteselectedsexualorientationSoft').hide();
        $('#restorebuttonsexualorientation').show();
        $('#activebuttonsexualorientation').hide();
        var url='/employee/sexualorientation/getall';
        $('#sexual_orientation_table').DataTable().clear().destroy();
        sexual_orientation_data(url);
    });
    $('body').on('click', '.saveSexualorientationdata', function() {
        var company_id=$('#companies_selected_nav').children("option:selected").val();
        var tr_id=$(this).attr('id');
        var data={
            'id':tr_id,
            'name':$('#sexualorientation_name'+tr_id).html(),
            'company_id':company_id,
        };
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url:'/employee/sexualorientation/update',
            data:data,
            success: function(response)
            {
                if(response.error)
                {
                    toastr["error"](response.error);
                    $("#sexualorientation_tr"+tr_id).attr('style','background-color: #fc685f');
                }
                else
                {
                    $("#sexualorientation_tr"+tr_id).attr('style','background-color: lightgreen !important');
                    toastr["success"](response.message);
                    $(".sexual_show_button_"+tr_id).hide();
                    $('.deletesexualorientationdata'+tr_id).show();
                }
            }
        });
        // console.log(data);
    });
});
function restoresexualorientationdata(id)
{
    var url='/employee/sexualorientation/active';
    swal({
        title: "Are you sure?",
        text: "Selected option with be active ",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
     },
     function(isConfirm){

       if (isConfirm){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
             $.ajax({
                type: "POST",
                url: url,
                data: {'id':id},
                success: function(response)
                {
                    if(response.flag==1)
                    {
                        var url='/employee/sexualorientation/getall';
                    }
                    else
                    {
                        var url='/employee/sexualorientation/getallSoft';
                    }
                    $('#sexual_orientation_table').DataTable().clear().destroy();
                    sexual_orientation_data(url);
                    swal.close();
                    toastr["success"](response.message);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        } else {
          swal("Cancelled", "Your Record is safe", "error");

        }
     });
}
function delete_data_sexualorientation(id,url)
{
    swal({
            title: "Are you sure?",
            text: "Selected option will be Deleted ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){
            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {'id':id},
                    success: function(response)
                    {
                        $('#sexual_orientation_table').DataTable().clear().destroy();
                        var url='/employee/sexualorientation/getall';
                        sexual_orientation_data(url);
                        swal.close();
                        toastr["success"](response.message);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            } else {
                swal("Cancelled", "Your Record is safe", "error");

            }
        });
}

function delete_mutilpleDatasexualorientation(url,selectedrows)
{
    if( $('.edit_cms_disable').css('display') == 'none' ) {
        console.log('Editer mode On Please change your mode');
        return 0;
    }

    if (selectedrows && selectedrows.length) {
   // not empty
        swal({
            title: "Are you sure?",
            text: "Selected option will be inactive and can be Active again by the admin only!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
         },
        function(isConfirm){

            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url:url,
                    data: {'ids':selectedrows},
                    success: function(response)
                    {

                        toastr["success"](response.message);
                        if(response.flag==1)
                        {
                            var url='/employee/sexualorientation/getall';
                        }
                        else
                        {
                            var url='/employee/sexualorientation/getallSoft';
                        }
                        $('#sexual_orientation_table').DataTable().clear().destroy();
                        sexual_orientation_data(url);
                        swal.close();

                    },
                    error: function (error) {
                        console.log(error);
                    }
                });

            } else {
              swal("Cancelled", "Your Record is safe", "error");

            }
        });
     }
    else
    {
   // empty
        return toastr["error"]("Select at least one record to delete!");
    }
 }
 function delete_data_sexualorientation(id,url)
 {
    swal({
        title: "Are you sure?",
        text: "Selected option with be Deleted ",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
     },
     function(isConfirm){

       if (isConfirm){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
             $.ajax({
                type: "POST",
                url: url,
                data: {'id':id},
                success: function(response)
                {
                    if(response.flag==1)
                    {
                        var url='/employee/sexualorientation/getall';
                    }
                    else
                    {
                        var url='/employee/sexualorientation/getallSoft';
                    }
                    $('#sexual_orientation_table').DataTable().clear().destroy();
                    sexual_orientation_data(url);
                    swal.close();
                    toastr["success"](response.message);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        } else {
          swal("Cancelled", "Your Record is safe", "error");

        }
     });
 }
function deletesoftsexualorientationdata(id)
{
    var url='/employee/sexualorientation/softdelete';
    delete_data_sexualorientation(id,url);

}
function deletesexualorientationdata(id)
{
    var url='/employee/sexualorientation/delete';
    delete_data_sexualorientation(id,url);
}
