$(document).ready(function() {
    var selectedrows=[];

    $('body').on('click','#qr_checkbox_all',function() {
        // var x = document.getElementsByClassName("selectedrowqr");
        // var i;
        // for (i = 0; i < x.length;i++) {
        //     var tr_id=x[i].id;
        //     tr_id=tr_id.split('qrs');
        //     tr_id=tr_id[1];
        //     if(selectedrows.includes(tr_id))
        //     {
        //         selectedrows.splice( selectedrows.indexOf(tr_id), 1 );
        //         if (selectedrows.length >0){
        //             $('.qr_export').val(JSON.stringify(selectedrows));
        //         }else{
        //             $('.qr_export').val('1');
        //         }
        //         $('#qr_tr'+tr_id).removeClass('selected');
        //         $("#qrs"+tr_id).attr('checked',false);
        //     }
        //     else
        //     {
        //         selectedrows.push(tr_id);
        //         $('.qr_export').val(JSON.stringify(selectedrows));
        //         $('#qr_tr'+tr_id).addClass('selected');
        //         $("#qrs"+tr_id).attr('checked',true);
        //     }

        // }
        $('input:checkbox').not(this).prop('checked', this.checked);
        $('input[type="checkbox"]').each(function(key,value) {
            var id=$(this).attr('id');
            id=id.split('qrs');
            row_id='#qr_tr'+id[1];
            if($(this).is(":checked"))
            {
                if(key == 0)
                {
                    selectedrows=[];
                }
                if(id[1] != '_checkbox_all')
                {
                    selectedrows.push(id[1]);
                }
                $('.qr_export').val(JSON.stringify(selectedrows));
                $(row_id).addClass('selected');
            }
            else
            {
                $('.qr_export').val(1);
                selectedrows=[];
                $(row_id).removeClass('selected');
            }
        });

    });

    $('body').on('click', '.selectedrowqr', function() {
        // var tr_id=$(this).attr('id');
        // tr_id=tr_id.split('qrs');
        // tr_id=tr_id[1];
        // if(selectedrows.includes(tr_id))
        // {
        //     selectedrows.splice( selectedrows.indexOf(tr_id), 1 );

        //     if (selectedrows.length >0){
        //         $('.qr_export').val(JSON.stringify(selectedrows));
        //     }else{
        //         $('.qr_export').val('1');
        //     }
        //     $('#qr_tr'+tr_id).removeClass('selected');

        // }
        // else
        // {
        //     selectedrows.push(tr_id);
        //     $('.qr_export').val(JSON.stringify(selectedrows));
        //     $('#qr_tr'+tr_id).addClass('selected');

        // }
        var id=$(this).attr('id');
        id=id.split('qrs');
        row_id='#qr_tr'+id[1];
        if($(this).is(":checked"))
        {
            selectedrows.push(id[1]);
            $('.qr_export').val(JSON.stringify(selectedrows));
            $(row_id).addClass('selected');
        }else
        {
            if(selectedrows.includes(id[1]))
            {
                selectedrows.splice( selectedrows.indexOf(id[1]),1);
                $('.qr_export').val(JSON.stringify(selectedrows));
                $(row_id).removeClass('selected');
            }
        }
    });
    $('#selectedactivebuttonqr').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        var url='/qr/active/multiple';
        if (selectedrows && selectedrows.length) {
            // not empty
            swal({
                    title: "Are you sure?",
                    text: "Selected option will be active  again ..!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Yes, I am sure!',
                    cancelButtonText: "No, cancel it!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm){

                    if (isConfirm){
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $.ajax({
                            type: "POST",
                            url:url,
                            data: {'ids':selectedrows},
                            success: function(response)
                            {

                                toastr["success"](response.message);
                                var url='/qr/getallSoft';
                                if ($("#qr_checkbox_all").prop('checked',true)){
                                    $("#qr_checkbox_all").prop('checked',false);
                                }
                                qr_data(url);
                                swal.close();

                            },
                            error: function (error) {
                                console.log(error);
                            }
                        });

                    } else {
                        swal("Cancelled", "Your Record is safe", "error");
                        e.preventDefault();
                    }
                });
        }
        else
        {
            // empty
            return toastr["error"]("Select at least one record to delete!");
        }
    });
    $('#deleteselectedqr').click(function(){
        var url='/qr/delete/multiple';
        delete_mutilpleData(url,selectedrows);
    });
    $('#deleteselectedqrSoft').click(function(){
        var url='/qr/delete/soft/multiple';
        delete_mutilpleData(url,selectedrows);
    });

    $('.add-qr').on('click', 'i', () => {
        $.ajax({
            type: 'GET',
            url: '/qr/store',
            success: function(response)
            {
                var today = new Date();
                var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
                var html='';
                html+= '<tr id="qr_tr'+response.data.id+'"  role="row" class="hide odd">';
                if ($("#projectqr_checkbox").val()=='1'){
                    html+=' <td><div class="form-check"><input type="checkbox" class="form-check-input selectedrowqr" id="qrs'+response.data.id+'"><label class="form-check-label" for="qrs'+response.data.id+'"></label></div></td>';
                }
                html+='<td class="qr_image'+response.data.id+'"></td>';
                if ($("#projectqr_name").val()=='1'){
                    var n_edit = $('#projectqr_name_create').val() ? 'true':'false';
                    html+='<td class="pt-3-half edit_inlineqr" onkeypress="editSelectedRow('+"qr_tr"+response.data.id+')"  data-id="'+response.data.id+'" id="name'+response.data.id+'"   contenteditable="'+n_edit+'"></td>';
                }

                if ($("#projectqr_directions").val()=='1'){
                    var d_edit = $('#projectqr_directions_create').val() ? 'true':'false';
                    html+='<td class="pt-3-half edit_inlineqr" onkeypress="editSelectedRow('+"qr_tr"+response.data.id+')" data-id="'+response.data.id+'" id="directions'+response.data.id+'"   contenteditable="'+d_edit+'"></td>';
                }
                if ($("#projectqr_information").val()=='1'){
                    var in_edit = $('#projectqr_information_create').val() ? 'true':'false';
                    html+='<td class="pt-3-half edit_inlineqr" onkeypress="editSelectedRow('+"qr_tr"+response.data.id+')" data-id="'+response.data.id+'" id="information'+response.data.id+'"   contenteditable="'+in_edit+'"></td>';
                }

                html+='<td class="pt-3-half edit_inlineqr">';
                if ($("#delete_projectqr").val()=='1'){
                    html+='<a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light delete-btn'+response.data.id+'" onclick="deleteqrdata('+response.data.id+')"><i class="fas fa-trash"></i></a>';
                }
                if ($("#edit_projectqr").val()=='1'){
                    html+='<a type="button" class="btn btn-primary all_action_btn_margin btn-xs my-0 waves-effect waves-light saveqrdata equipment_show_button_'+response.data.id+' show_tick_btn'+response.data.id+'" style="display: none;" id="'+response.data.id+'"><i class="fas fa-check"></i></a>';

                    // html+='<a type="button" class="btn btn-primary btn-xs my-0 waves-effect waves-light saveqrdata equipment_show_button_'+response.data.id+'" style="display: none;" id="'+response.data.id+'"><i class="fas fa-check"></i></a>';
                }

                html+='</td></tr>';
                $('#table-qr').find('table').prepend(html);

            },
            error: function (error) {
                console.log(error);
            }
        });


    });
    $('body').on('click', '.saveqrdata ', function() {
        var company_id=$('#companies_selected_nav').children("option:selected").val();
        var tr_id=$(this).attr('id');
        var data={
            'id':tr_id,
            'name':$('#name'+tr_id).html(),
            'qr':$('#qr'+tr_id).children("option:selected").val(),
            'directions':$('#directions'+tr_id).html(),
            'information':$('#information'+tr_id).html(),
            'tag':$('#notes'+tr_id).html(),
            'company_id':company_id,

        };
        if($("#projectqr_name_edit").val() =='1') {
            if ($('#name' + tr_id).html() == "") {
                toastr["error"]('name field required');
            }
            else{
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url:'/qr/update',
                    data:data,
                    success: function(response)
                    {
                        $('.qr_image'+tr_id).html(response.qr);
                        toastr["success"](response.message);
                        save_and_delete_btn_toggle(tr_id);
                        $("#qr_tr"+tr_id).attr('style','background-color:#90ee90 !important');
                        // var url='/qr/getall';
                        // qr_data(url);

                    }
                });
            }
        }
    });

    $('#restorebuttonqr').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        selectedrows=[];
        if ($("#qr_checkbox_all").prop('checked',true)){
            $("#qr_checkbox_all").prop('checked',false);
        }
        $("#export_excel_qr_btn").hide();
        $("#export_world_qr_btn").hide();
        $("#export_pdf_qr_btn").hide();
        $('#restorebuttonqr').hide();
        $('#deleteselectedqr').hide();
        $('#selectedactivebuttonqr').show();
        $('#deleteselectedqrSoft').show();
        $('#export_excel_qr').val(2);
        $('#export_world_qr').val(2);
        $('#export_pdf_qr').val(2);
        $('.add-qr').hide();
        $('#activebuttonqr').show();
        var url='/qr/getallSoft';
        qr_data(url);
    });
    $('#activebuttonqr').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        selectedrows=[];
        if ($("#qr_checkbox_all").prop('checked',true)){
            $("#qr_checkbox_all").prop('checked',false);
        }
        $("#export_excel_qr_btn").show();
        $("#export_world_qr_btn").show();
        $("#export_pdf_qr_btn").show();
        $('#export_excel_qr').val(1);
        $('#export_world_qr').val(1);
        $('#export_pdf_qr').val(1);
        $('.add-qr').show();
        $('#selectedactivebuttonqr').hide();
        $('#deleteselectedqr').show();
        $('#deleteselectedqrSoft').hide();
        $('#restorebuttonqr').show();
        $('#activebuttonqr').hide();
        var url='/qr/getall';
        qr_data(url);
    });



});

function restoreqrdata(id)
{
    var url='/qr/active';
    if( $('.edit_cms_disable').css('display') == 'none' ) {
        console.log('Editer mode On Please change your mode');
        return 0;
    }
    // not empty
    swal({
            title: "Are you sure?",
            text: "Selected option will be active !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){

            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url:url,
                    data: {'id':id},
                    success: function(response)
                    {

                        toastr["success"](response.message);
                        if(response.flag==1)
                        {
                            var url='/qr/getall';
                        }
                        else
                        {
                            var url='/qr/getallSoft';
                        }
                        if ($("#qr_checkbox_all").prop('checked',true)){
                            $("#qr_checkbox_all").prop('checked',false);
                        }
                        qr_data(url);
                        swal.close();

                    },
                    error: function (error) {
                        console.log(error);
                    }
                });

            } else {
                swal("Cancelled", "Your Record is safe", "error");
                e.preventDefault();
            }
        });

}
function delete_mutilpleData(url,selectedrows)
{
    if( $('.edit_cms_disable').css('display') == 'none' ) {
        console.log('Editer mode On Please change your mode');
        return 0;
    }
    if (selectedrows && selectedrows.length) {
        // not empty
        swal({
                title: "Are you sure?",
                text: "Selected option will be inactive and can be Active again by the admin only!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:url,
                        data: {'ids':selectedrows},
                        success: function(response)
                        {

                            toastr["success"](response.message);
                            if(response.flag==1)
                            {
                                var url='/qr/getall';
                            }
                            else
                            {
                                var url='/qr/getallSoft';
                            }
                            if ($("#qr_checkbox_all").prop('checked',true)){
                                $("#qr_checkbox_all").prop('checked',false);
                            }
                            qr_data(url);
                            swal.close();

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                    swal("Cancelled", "Your Record is safe", "error");
                    e.preventDefault();
                }
            });
    }
    else
    {
        // empty
        return toastr["error"]("Select at least one record to delete!");
    }
}
function delete_data_qr(id,url)
{
    swal({
            title: "Are you sure?",
            text: "Selected option with be Deleted ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){

            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {'id':id},
                    success: function(response)
                    {
                        if(response.flag==1)
                        {
                            var url='/qr/getall';
                        }
                        else
                        {
                            var url='/qr/getallSoft';
                        }
                        if ($("#qr_checkbox_all").prop('checked',true)){
                            $("#qr_checkbox_all").prop('checked',false);
                        }
                        qr_data(url);
                        swal.close();
                        toastr["success"](response.message);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            } else {
                swal("Cancelled", "Your Record is safe", "error");
                e.preventDefault();
            }
        });
}
function deletesoftqrdata(id)
{
    var url='/qr/softdelete';
    delete_data_qr(id,url);

}
function deleteqrdata(id)
{
    var url='/qr/delete';
    delete_data_qr(id,url);
}
