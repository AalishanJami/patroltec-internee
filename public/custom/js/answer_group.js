 $(document).ready(function() {
    var selectedrows=[];
     $('body').on('click', '.remove_answer_main', function() {
        var url='/answer_group/delete';
        delete_data_answer_group($(this).attr('id'),url);
    });

     $('body').on('click','#answer_group_checkbox_all',function() {
        $('.selectedrowanswer_group').not(this).prop('checked', this.checked);
        $('.selectedrowanswer_group').each(function(key,value) {
            var id=$(this).attr('id');
            id=id.split('answer_group');
            row_id='#answer_group_tr'+id[1];
            if($(this).is(":checked"))
            {
                if(key == 0)
                {
                    selectedrows=[];
                }
                if(id[1] != '_checkbox_all')
                {
                    selectedrows.push(id[1]);
                }
                $('.answer_group_export').val(JSON.stringify(selectedrows));
                $(row_id).addClass('selected');
            }
            else
            {
                $('.answer_group_export').val(1);
                selectedrows=[];
                $(row_id).removeClass('selected');
            }
        });


     });

    $('body').on('click', '.selectedrowanswer_group', function() {
        var id=$(this).attr('id');
        id=id.split('answer_group');
        row_id='#answer_group_tr'+id[1];
        if($(this).is(":checked"))
        {
            selectedrows.push(id[1]);
            $('.answer_group_export').val(JSON.stringify(selectedrows));
            $(row_id).addClass('selected');
        }else
        {
            if(selectedrows.includes(id[1]))
            {
                selectedrows.splice( selectedrows.indexOf(id[1]),1);
                $('.answer_group_export').val(JSON.stringify(selectedrows));
                $(row_id).removeClass('selected');
            }
        }
    });
    $('#selectedactivebuttonanswer_group').click(function(){
        var url='/answer_group/active/multiple';
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        if (selectedrows && selectedrows.length) {
       // not empty
            swal({
                title: "Are you sure?",
                text: "Selected option will be active  again ..!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
             },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:url,
                        data: {'ids':selectedrows},
                        success: function(response)
                        {

                            toastr["success"](response.message);
                            var url='/answer_group/getallSoft';
                            $('.answer_group_table').DataTable().clear().destroy();
                            answer_group_data(url);
                            swal.close();

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                  swal("Cancelled", "Your Record is safe", "error");

                }
            });
         }
        else
        {
       // empty
            return toastr["error"]("Select at least one record to delete!");
        }
    });
    $('#deleteselectedanswer_group').click(function(){
       var url='/answer_group/delete/multiple';
       delete_mutilpleDataanswer_group(url,selectedrows);
    });
    $('#deleteselectedanswer_groupSoft').click(function(){
       var url='/answer_group/delete/soft/multiple';
       delete_mutilpleDataanswer_group(url,selectedrows);
    });

    $('.add-answer_group').on('click', 'i', () => {
        var company_id=$('#companies_selected_nav').children("option:selected").val();
         var data={
            'company_id':company_id
        };
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
      $.ajax({
            type: 'POST',
            url: '/answer_group/store',
            data: data,
            success: function(response)
            {
                var html='';
                html+= '<tr id="answer_group_tr'+response.data.id+'">';
                html+= '<td>';
                html+='<div class="form-check"><input type="checkbox" class="form-check-input selectedrowanswer_group" id="answer_group'+response.data.id+'"><label class="form-check-label" for="answer_group'+response.data.id+'""></label></div>';
                html+= '</td>';
                html+= '<td style="text-transform: capitalize;" onkeypress="editSelectedRow('+"answer_group_tr"+response.data.id+')" id="answer_group_name'+response.data.id+'" data-id="'+response.data.id+'" class="pt-3-half edit_inline_answer_group"';
                if($('#document_answer_group_create').val())
                {
                    html+='contenteditable="true"';
                }
                html+= '></td>';
                html+= '<td class="edit_inline_answer_group" data-id="'+response.data.id+'">';
                html+='<div class="form-check"><input type="checkbox" class="form-check-input sig_select" value="1" id="sig_select'+response.data.id+'"><label class="form-check-label" for="sig_select'+response.data.id+'""></label></div>';
                html+= '</td>';
                html+= '<td id="answer_group_edit'+response.data.id+'" >';
                html+= '<a id="'+response.data.id+'" class="btn btn-danger btn-xs my-0 waves-effect waves-light delete_answergrp'+response.data.id+'" type="button"><i class="fas fa-trash"></i></a>';
                html+= '<a onclick="get_group_answer_id('+response.data.id+')" class="btn all_action_btn_margin btn-primary btn-xs my-0 waves-effect waves-light" type="button"><i class="fas fa-pencil-alt"></i></a>';
                html+= '<a type="button" class="btn btn-primary btn-xs my-0 all_action_btn_margin waves-effect waves-light saveanswer_groupdata answer_group_show_button_'+response.data.id+'" style="display: none;" id="'+response.data.id+'"><i class="fas fa-check"></i></a>';
                html+= '</td>';
                html+= '</tr>';
                $('#table-answer_group').find('table').prepend(html);

            },
            error: function (error) {
                console.log(error);
            }
        });
    });

    $('body').on('keyup','.edit_inline_answer_group',function () {
        var id=$(this).attr('id');
        $('#'+id).focus();
        id=id.split('answer_group_name');
        $('.answer_group_show_button_'+id[1]).show();
        $('.delete_answergrp'+id[1]).hide();
    });
    $('body').on('click', '.saveanswer_groupdata', function() {
        var company_id=$('#companies_selected_nav').children("option:selected").val();
        var tr_id=$(this).attr('id');
        var answer_group_name='';
        if($('#answer_group_name'+tr_id+" span" ).html())
        {
            answer_group_name=$('#answer_group_name'+tr_id).html();
        } else {
            answer_group_name=$('#answer_group_name'+tr_id).html();
        }
        var sig_select = $('#sig_select'+tr_id+":checked").val();
        var data={
            'id':tr_id,
            'name':answer_group_name,
             'sig_select':sig_select,
            'company_id':company_id
        };
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url:'/answer_group/update',
            data:data,
            success: function(response)
            {
                if(response.error)
                {
                    toastr["error"](response.error);
                }
                else
                {
                    toastr["success"](response.message);
                    if(response.flag==1)
                    {
                        $("#answer_group_tr"+tr_id).attr('style','background-color:#90ee90 !important');
                        // var url='/answer_group/getall';
                    }
                    else
                    {
                        $("#answer_group_tr"+tr_id).attr('style','background-color:#ff3547 !important');
                        // var url='/answer_group/getallsoft';
                    }
                    $('.answer_group_show_button_'+tr_id).hide();
                    $('.delete_answergrp'+tr_id).show();
                    // $('.answer_group_table').DataTable().clear().destroy();
                    // answer_group_data(url);

                }

            }

        });

        // console.log(data);

    });

    $('#restorebuttonanswer_group').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        selectedrows=[];
        $('#restorebuttonanswer_group').hide();
        $('#deleteselectedanswer_group').hide();
        $('#selectedactivebuttonanswer_group').show();
        $('#deleteselectedanswer_groupSoft').show();
        $('#export_excel_answer_group').hide();
        $('#export_world_answer_group').hide();
        $('#export_pdf_answer_group').hide();
        $('.add-answer_group').hide();
        $('#activebuttonanswer_group').show();
        var url='/answer_group/getallSoft';
        $('.answer_group_table').DataTable().clear().destroy();
        answer_group_data(url);
    });
    $('#activebuttonanswer_group').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        selectedrows=[];
        $('#export_excel_answer_group').show();
        $('#export_world_answer_group').show();
        $('#export_pdf_answer_group').show();
        $('.add-answer_group').show();
        $('#selectedactivebuttonanswer_group').hide();
        $('#deleteselectedanswer_group').show();
        $('#deleteselectedanswer_groupSoft').hide();
        $('#restorebuttonanswer_group').show();
        $('#activebuttonanswer_group').hide();
        var url='/answer_group/getall';
        $('.answer_group_table').DataTable().clear().destroy();
        answer_group_data(url);
    });



});
 function restoreanswer_groupdata(id)
 {
    var url='/answer_group/active';
    swal({
        title: "Are you sure?",
        text: "Selected option with be active ",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
     },
     function(isConfirm){

       if (isConfirm){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
             $.ajax({
                        type: "POST",
                        url: url,
                        data: {'id':id},
                        success: function(response)
                        {
                            if(response.flag==0)
                            {
                                var url='/answer_group/getall';
                            }
                            else
                            {
                                var url='/answer_group/getallSoft';
                            }
                            $('.answer_group_table').DataTable().clear().destroy();
                            answer_group_data(url);
                            swal.close();
                            toastr["success"](response.message);
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
        } else {
          swal("Cancelled", "Your Record is safe", "error");

        }
     });
 }
 function delete_mutilpleDataanswer_group(url,selectedrows)
 {
     if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        if (selectedrows && selectedrows.length) {
       // not empty
            swal({
                title: "Are you sure?",
                text: "Selected option will be inactive and can be Active again by the admin only!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
             },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:url,
                        data: {'ids':selectedrows},
                        success: function(response)
                        {
                            if(response.flag==2)
                            {
                                toastr["error"](response.message);
                                var url='/answer_group/getall';
                            }
                            else
                            {
                                toastr["success"](response.message);
                            }

                            if(response.flag==0)
                            {
                                var url='/answer_group/getall';
                            }
                            else if(response.flag==1)
                            {
                                var url='/answer_group/getallSoft';
                            }
                            $('.answer_group_table').DataTable().clear().destroy();
                            answer_group_data(url);
                            swal.close();

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                  swal("Cancelled", "Your Record is safe", "error");

                }
            });
         }
        else
        {
       // empty
            return toastr["error"]("Select at least one record to delete!");
        }
 }
 function delete_data_answer_group(id,url)
 {
    swal({
        title: "Are you sure?",
        text: "Selected option with be Deleted ",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
     },
     function(isConfirm){

       if (isConfirm){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
             $.ajax({
                        type: "POST",
                        url: url,
                        data: {'id':id},
                        success: function(response)
                        {
                            if(response.flag==0)
                            {
                                var url='/answer_group/getall';
                                toastr["success"](response.message);
                                $('.answer_group_table').DataTable().clear().destroy();
                                answer_group_data(url);
                                swal.close();

                            } else if(response.flag == 2){
                                toastr["error"](response.message);
                                swal.close();
                            }
                            else
                            {
                                var url='/answer_group/getallSoft';
                                $('.answer_group_table').DataTable().clear().destroy();
                                answer_group_data(url);
                                swal.close();

                            }

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
        } else {
          swal("Cancelled", "Your Record is safe", "error");

        }
     });
 }
function deletesoftanswer_groupdata(id)
{
    selectedrows=[];
    var url='/answer_group/softdelete';
    delete_data_answer_group(id,url);

}
function deleteanswer_groupdata(id)
 {
    selectedrows=[];
    console.log(selectedrows);
    var url='/answer_group/delete';
    delete_data_answer_group(id,url);
 }
