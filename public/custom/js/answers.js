 $(document).ready(function() {
    var selectedrows=[];

     $('body').on('click','#answer_checkbox_all',function() {
        $('input:checkbox').not(this).prop('checked', this.checked);
        $('input[type="checkbox"]').each(function(key,value) {
            var id=$(this).attr('id');
            if(id)
            {
                id=id.split('answer');
                row_id='#answer_tr'+id[1];
                if($(this).is(":checked"))
                {
                    if(key == 0)
                    {
                        selectedrows=[];
                    }
                    if(id[1] != '_checkbox_all')
                    {
                        selectedrows.push(id[1]);
                    }
                    $('.answer_export').val(JSON.stringify(selectedrows));
                    $(row_id).addClass('selected');
                }
                else
                {
                    $('.answer_export').val(1);
                    selectedrows=[];
                    $(row_id).removeClass('selected');
                }
            }
        });


     });

    $('body').on('click', '.table-remove_answer', function() {
        var url='/answer/delete';
        delete_data_answer($(this).attr('id'),url);
    });
    $('body').on('click', '.selectedrowanswer', function() {
        // var tr_id=$(this).attr('id');
        // tr_id=tr_id.split('answer');
        // tr_id=tr_id[1];
        // if(selectedrows.includes(tr_id))
        // {
        //     selectedrows.splice( selectedrows.indexOf(tr_id), 1 );
        //     $('.answer_export').val(JSON.stringify(selectedrows));
        //     $('#answer'+tr_id).attr('checked', false );
        //     $('#answer_tr'+tr_id).removeClass('selected');

        // }
        // else
        // {
        //     selectedrows.push(tr_id);
        //     $('.answer_export').val(JSON.stringify(selectedrows));
        //     $('#answer'+tr_id).attr('checked', true );
        //     $('#answer_tr'+tr_id).addClass('selected');

        // }
        var id=$(this).attr('id');
        id=id.split('answer');
        row_id='#answer_tr'+id[1];
        if($(this).is(":checked"))
        {
            selectedrows.push(id[1]);
            $('.answer_export').val(JSON.stringify(selectedrows));
            $(row_id).addClass('selected');
        }else
        {
            if(selectedrows.includes(id[1]))
            {
                selectedrows.splice( selectedrows.indexOf(id[1]),1);
                $('.answer_export').val(JSON.stringify(selectedrows));
                $(row_id).removeClass('selected');
            }
        }
    });
    $('#selectedactivebuttonanswer').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        var url='/answer/active/multiple';
        if (selectedrows && selectedrows.length) {
       // not empty
            swal({
                title: "Are you sure?",
                text: "Selected option will be active  again ..!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
             },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:url,
                        data: {'ids':selectedrows},
                        success: function(response)
                        {

                            toastr["success"](response.message);
                            var group_id = sessionStorage.getItem('answer_group_id');
                            var url='/answer/getallSoft/'+group_id;
                            $('.answer_table').DataTable().clear().destroy();
                            answer_data(url);
                            swal.close();

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                  swal("Cancelled", "Your Record is safe", "error");

                }
            });
         }
        else
        {
       // empty
            return toastr["error"]("Select at least one record to delete!");
        }
    });
    $('#deleteselectedanswer').click(function(){
       var url='/answer/delete/multiple';
       delete_mutilpleDataanswer(url,selectedrows);
    });
    $('#deleteselectedanswerSoft').click(function(){
       var url='/answer/delete/soft/multiple';
       delete_mutilpleDataanswer(url,selectedrows);
    });

    $('.add-answer').on('click', 'i', () => {
        var company_id=$('#companies_selected_nav').children("option:selected").val();
         var data={
            'company_id':company_id
        };
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
      $.ajax({
            type: 'POST',
            url: '/answer/store',
            data: data,
            success: function(response)
            {
                var html='';
                html+= '<tr id="answer_tr'+response.data.id+'">';
                html+= '<td>';
                html+='<div class="form-check"><input type="checkbox" class="form-check-input selectedrowanswer" id="answer'+response.data.id+'"><label class="form-check-label" for="answer'+response.data.id+'""></label></div>';
                html+= '</td>'
                if($('#document_answer_image').val())
                {
                    html+= '<td id="image_data'+response.data.id+'">';
                    html+= '';
                    html+= '</td>'
                }
                html+= '<td onkeypress="editSelectedRow('+"answer_tr"+response.data.id+')" data-id="'+response.data.id+'" class="pt-3-half edit_inline_answer"';
                if($('#document_answer_name_create').val())
                {
                    html+= 'contenteditable="true" id="answer_name'+response.data.id+'';
                }
                html+= '"></td>';
                var grade_disable='';
                if(!$('#document_answer_grade_create').val())
                {
                    grade_disable='disabled';
                }
                html+= '<td class="pt-3-half edit_inline_answer" data-id="'+response.data.id+'" contenteditable="true" id="answer_grade'+response.data.id+'"><select '+grade_disable+' searchable="Search here.." onchange="editSelectedRow('+"answer_tr"+response.data.id+')"  id="answer_grade'+response.data.id+'"  style="background-color: inherit;border: 0px"><option value="1">Pass</option><option value="0">Fail</option><option value="0">Ignore</option></td>';
                html+= '<td onkeypress="editSelectedRow('+"answer_tr"+response.data.id+')" class="pt-3-half edit_inline_answer"';
                if($('#document_answer_score_create').val())
                {
                    html+= 'contenteditable="true" id="answer_score'+response.data.id+'';
                }
                html+= '"></td>';
                html+= '<td id="answer_edit'+response.data.id+'" >';
                html+= '<div style=" display: flex;flex-direction: row; flex-wrap: nowrap; "> ';
                if($('#upload_permision_document_answer').val())
                {
                    html+= '<form  method="post" class="mr-2" id="form_upload_icon'+response.data.id+'" enctype="multipart/form-data" action="'+upload+'">'+token+'<div style=" display: flex;flex-direction: row; flex-wrap: nowrap; "><input class="edit_inline_answer" data-id="'+response.data.id+'" type="file" onchange="editSelectedRow('+"answer_tr"+response.data.id+')" name="file" style=" border: 1px solid #ccc;display: inline-block;padding: 6px 12px;cursor: pointer;" ><input type="hidden" value="'+response.data.id+'" name="answer_id"></div></form>';
                }
                html+= '</div>';
                html+= '</td>';

                html+= '<td id="answer_edit'+response.data.id+'" >';
                html+= '<div style=" display: flex;flex-direction: row; flex-wrap: nowrap; "> ';
                html+='<span style="margin-top:6px; margin-right:6px;" class="save showAnswer'+response.data.id+'" id="'+response.data.id+'" hidden><a type="button"';
                html+= 'class="btn btn-success btn-rounded btn-sm my-0"><i class="fa fa-check"></i></a>';
                html+= '</span>';
                html+= '<span class="table-remove_answer" style="margin-top:6px" id="'+response.data.id+'"><a type="button"';
                html+= 'class="btn remove_answer delete_answer'+response.data.id+' btn-danger btn-rounded btn-sm my-0"><i class="fas fa-trash"></i></a>';
                html+= '</span></div>';
                html+= '</td>';
                html+= '</tr>';
                $('#table-answer').find('table').prepend(html);

            },
            error: function (error) {
                console.log(error);
            }
        });
    });

    $('body').on('keyup', '.edit_inline_answer', function() {
        var tr_id=$(this).parent('tr').attr('id');
        tr_id=tr_id.split('answer_tr');
        tr_id=tr_id[1];
        $('.showAnswer'+tr_id).attr('hidden',false);
        $('.delete_answer'+tr_id).hide();
    });

    $('body').on('click', '.save', function() {
        var company_id=$('#companies_selected_nav').children("option:selected").val();
        var tr_id=$(this).attr('id');
        var formid = "form_upload_icon"+tr_id;
        var answer_group_id=sessionStorage.getItem('answer_group_id');
        var data={
            'id':tr_id,
            'answer':$('#answer_name'+tr_id).html(),
            'answers_type_id':$('#answer_type'+tr_id).children("option:selected").val(),
            'grade':$('#answer_grade'+tr_id).children("option:selected").val(),
            'score':$('#answer_score'+tr_id).html(),
            'company_id':company_id,
            'answer_group_id':answer_group_id,
        };
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url:'/answer/update',
            data:data,
            success: function(response)
            {
                if(response.error)
                {
                    toastr["error"](response.error);
                    $("#answer_tr"+tr_id).removeClass('selected_row');
                    $("#answer_tr"+tr_id).attr('style',"background-color: #fc685f");
                }
                else
                {
                    toastr["success"](response.message);
                  //   var group_id = sessionStorage.getItem('answer_group_id');
                  //   if(response.flag==1)
                  //   {
                  //       var url='/answer/getall/'+group_id;
                  //   }
                  //   else
                  //   {
                  //       var url='/answer/getallsoft/'+group_id;
                  //   }

                  // /*  $('.answer_table').DataTable().clear().destroy();
                  //   answer_data(url);
                  //   */
                    uploadAnswerImage(formid,tr_id);
                    $("#answer_tr"+tr_id).attr('style',"background-color: lightgreen !important;");
                    $('.showAnswer'+tr_id).attr('hidden',true);
                    $('.delete_answer'+tr_id).show();
                }

            }

        });

    });

    $('#restorebuttonanswer').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        selectedrows=[];
        $('#restorebuttonanswer').hide();
        $('#deleteselectedanswer').hide();
        $('#selectedactivebuttonanswer').show();
        $('#deleteselectedanswerSoft').show();
        $('#export_excel_answer').hide();
        $('#export_world_answer').hide();
        $('#export_pdf_answer').hide();
        $('.add-answer').hide();
        $('#activebuttonanswer').show();
        var group_id = sessionStorage.getItem('answer_group_id');
        var url='/answer/getallSoft/'+group_id;
        console.log(url);
        $('.answer_table').DataTable().clear().destroy();
        answer_data(url);
    });
    $('#activebuttonanswer').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        selectedrows=[];
        $('#export_excel_answer').show();
        $('#export_world_answer').show();
        $('#export_pdf_answer').show();
        $('.add-answer').show();
        $('#selectedactivebuttonanswer').hide();
        $('#deleteselectedanswer').show();
        $('#deleteselectedanswerSoft').hide();
        $('#restorebuttonanswer').show();
        $('#activebuttonanswer').hide();
        var group_id = sessionStorage.getItem('answer_group_id');
        var url='/answer/getall/'+group_id;
        $('.answer_table').DataTable().clear().destroy();
        answer_data(url);
    });



});
function restoreanswerdata(id)
{
    var url='/answer/active';
    swal({
        title: "Are you sure?",
        text: "Selected option with be active ",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
     },
     function(isConfirm){

       if (isConfirm){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
             $.ajax({
                type: "POST",
                url: url,
                data: {'id':id},
                success: function(response)
                {
                    var group_id = sessionStorage.getItem('answer_group_id');
                    if(response.flag==1)
                    {
                        var url='/answer/getall/'+group_id;
                    }
                    else
                    {
                        var url='/answer/getallSoft/'+group_id;
                    }
                    $('.answer_table').DataTable().clear().destroy();
                    answer_data(url);
                    swal.close();
                    toastr["success"](response.message);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        } else {
          swal("Cancelled", "Your Record is safe", "error");

        }
     });
}
function delete_mutilpleDataanswer(url,selectedrows)
{
    if( $('.edit_cms_disable').css('display') == 'none' ) {
        console.log('Editer mode On Please change your mode');
        return 0;
    }
    if (selectedrows && selectedrows.length) {
   // not empty
        swal({
            title: "Are you sure?",
            text: "Selected option will be inactive and can be Active again by the admin only!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
         },
        function(isConfirm){

            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url:url,
                    data: {'ids':selectedrows},
                    success: function(response)
                    {

                        toastr["success"](response.message);
                        var group_id = sessionStorage.getItem('answer_group_id');
                        if(response.flag==1)
                        {
                            var url='/answer/getall/'+group_id;
                        }
                        else
                        {
                            var url='/answer/getallSoft/'+group_id;
                        }
                        $('.answer_table').DataTable().clear().destroy();
                        answer_data(url);
                        swal.close();

                    },
                    error: function (error) {
                        console.log(error);
                    }
                });

            } else {
              swal("Cancelled", "Your Record is safe", "error");

            }
        });
     }
    else
    {
   // empty
        return toastr["error"]("Select at least one record to delete!");
    }
}
function delete_data_answer(id,url)
{
    swal({
        title: "Are you sure?",
        text: "Selected option with be Deleted ",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
     },
     function(isConfirm){
       if (isConfirm){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: url,
                data: {'id':id},
                success: function(response)
                {
                    toastr["success"](response.message);
                    var group_id = sessionStorage.getItem('answer_group_id');
                    if(response.flag==1)
                    {
                        var url='/answer/getall/'+group_id;

                    }
                    else
                    {
                        var url='/answer/getallSoft/'+group_id;
                    }
                    $('.answer_table').DataTable().clear().destroy();
                    answer_data(url);
                    swal.close();
                },
                error: function (error) {
                    console.log(error);
                }
            });
        } else {
          swal("Cancelled", "Your Record is safe", "error");

        }
     });
}
function deletesoftanswerdata(id)
{
    selectedrows=[];
    var url='/answer/softdelete';
    delete_data_answer(id,url);

}
function deleteanswerdata(id)
 {
    selectedrows=[];
    var url='/answer/delete';
    delete_data_answer(id,url);
 }
