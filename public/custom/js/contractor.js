$(document).ready(function() {
    var selectedrows=[];
    $('body').on('click','#contractor_checkbox_all',function() {
        $('.selectedrowcontractor').not(this).prop('checked', this.checked);
        $('.selectedrowcontractor').each(function(key,value) {
            var id=$(this).attr('id');
            console.log(id);
            if(id)
            {
                id=id.split('contractor');
                row_id='#contractor_tr'+id[1];
                if($(this).is(":checked"))
                {
                    if(key == 0)
                    {
                        selectedrows=[];
                    }
                    if(id[1] != '_checkbox_all')
                    {
                        selectedrows.push(id[1]);
                    }
                    $('.contractor_export').val(JSON.stringify(selectedrows));
                    $(row_id).addClass('selected');
                }
                else
                {
                    $('.contractor_export').val(1);
                    selectedrows=[];
                    $(row_id).removeClass('selected');
                }
            }
        });
    });

    $('body').on('click', '.selectedrowcontractor', function() {
        var id=$(this).attr('id');
        id=id.split('contractor');
        row_id='#contractor_tr'+id[1];
        if($(this).is(":checked"))
        {
            selectedrows.push(id[1]);
            $('.contractor_export').val(JSON.stringify(selectedrows));
            $(row_id).addClass('selected');
        }else
        {
            if(selectedrows.includes(id[1]))
            {
                selectedrows.splice( selectedrows.indexOf(id[1]),1);
                $('.contractor_export').val(JSON.stringify(selectedrows));
                $(row_id).removeClass('selected');
            }
        }
    });
    $('#selectedactivebuttoncontractor').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        var url='/contractor/active/multiple';
        if (selectedrows && selectedrows.length) {
            // not empty
            swal({
                    title: "Are you sure?",
                    text: "Selected option will be active  again ..!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Yes, I am sure!',
                    cancelButtonText: "No, cancel it!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm){

                    if (isConfirm){
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $.ajax({
                            type: "POST",
                            url:url,
                            data: {'ids':selectedrows},
                            success: function(response)
                            {

                                toastr["success"](response.message);
                                var url='/contractor/getallSoft';
                                if ($("#contractor_checkbox_all").prop('checked',true)){
                                    $("#contractor_checkbox_all").prop('checked',false);
                                }
                                $('#contractor_table_id').DataTable().clear().destroy();
                                contractor_data(url);

                                swal.close();

                            },
                            error: function (error) {
                                console.log(error);
                            }
                        });

                    } else {
                        swal("Cancelled", "Your Record is safe", "error");

                    }
                });
        }
        else
        {
            // empty
            return toastr["error"]("Select at least one record to delete!");
        }
    });
    $('#deleteselectedcontractor').click(function(){
        var url='/contractor/delete/multiple';
        delete_mutilpleData_contrater(url,selectedrows);
    });
    $('#deleteselectedcontractorSoft').click(function(){
        var url='/contractor/delete/soft/multiple';
        delete_mutilpleData_contrater(url,selectedrows);
    });

    $('.add-contractor').on('click', 'i', () => {
        $.ajax({
            type: 'GET',
            url: '/contractor/store',
            success: function(response)
            {
                var p_disable=($("#contractor_allowed_create").val()=='1') ? '':'disabled';
                var b_disable=($("#contractor_banned_create").val()=='1') ? '':'disabled';
                var s_disable=($("#contractor_start_date_create").val()=='1') ? '':'disabled';
                var e_disable=($("#contractor_end_date_create").val()=='1') ? '':'disabled';
                var n_edit=($("#contractor_name_create").val()=='1') ? 'true':'false';
                var html='';
                html+= '<tr id="contractor_tr'+response.data.id+'"  role="row" class="hide odd">';
                html+=' <td><div class="form-check"><input type="checkbox" class="form-check-input" id="contractors'+response.data.id+'"><label class="form-check-label" for="contractors'+response.data.id+'"></label></div></td>';
                // html+='<td class="sorting_1"><a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light" onclick="deletecontractordata()"><i class="fas fa-trash"></i></a></td>';

                if($("#contractor_name").val()==1){
                    html+='<td class="pt-3-half edit_inline_contractor" onkeypress="editSelectedRow('+"contractor_tr"+response.data.id+')" id="name'+response.data.id+'" data-id="'+response.data.id+'"   contenteditable="'+n_edit+'"></td>';
                }
                if($("#contractor_start_date").val()==1){
                    html+='<td class="pt-3-half edit_inline_contractor" id="start'+response.data.id+'"  data-id="'+response.data.id+'" " >';
                    html+='<input onchange="editSelectedRow('+"contractor_tr"+response.data.id+')" placeholder="Selected date" '+s_disable+' value="'+response.data.start+'" type="date" id="start'+response.data.id+'"  class="form-control datepicker">';
                    html+'</td>';
                }
                if($("#contractor_end_date").val()==1){
                    html+='<td class="pt-3-half edit_inline_contractor" data-id="'+response.data.id+'" id="end'+response.data.id+'" " >';
                    html+='<input onchange="editSelectedRow('+"contractor_tr"+response.data.id+')" placeholder="Selected date" '+e_disable+' value="'+response.data.end+'" type="date" id="end'+response.data.id+'"  class="form-control datepicker">';
                    html+'</td>';
                }
                // html+='<td class="pt-3-half edit_inline_contractor" id="start'+response.data.id+'"   contenteditable="true"></td>';
                // html+='<td class="pt-3-half edit_inline_contractor" id="end'+response.data.id+'"   contenteditable="true" ></td>';
                if($("#contractor_allowed").val()==1){
                    html+='<td class="pt-3-half edit_inline_contractor" data-id="'+response.data.id+'" id="preferred'+response.data.id+'"  >';
                    html+='<div class="form-check float-left">';
                    html+=' <input type="radio" '+p_disable+' onclick="editSelectedRow('+"contractor_tr"+response.data.id+')" value="1" checked  name="preferred'+response.data.id+'"  class="preferred'+response.data.id+' form-check-input" id="yes_preferred'+response.data.id+'">';
                    html+='<label class="form-check-label" for="yes_preferred'+response.data.id+'">Yes</label>';
                    html+='</div>';
                    html+='<div class="form-check float-left">';
                    html+=' <input type="radio" value="0"  '+p_disable+' onclick="editSelectedRow('+"contractor_tr"+response.data.id+')"  name="preferred'+response.data.id+'"  class="preferred'+response.data.id+' form-check-input" id="no_preferred'+response.data.id+'">';
                    html+='<label class="form-check-label" for="no_preferred'+response.data.id+'">No</label>';
                    html+='</div>';
                    html+='</td>'
                }
                if($("#contractor_banned").val()==1){
                    html+='<td class="pt-3-half edit_inline_contractor" data-id="'+response.data.id+'" id="banned'+response.data.id+'"  >';
                    html+='<div class="form-check float-left">';
                    html+=' <input type="radio"  '+b_disable+' onclick="editSelectedRow('+"contractor_tr"+response.data.id+')" value="1" name="banned'+response.data.id+'" class="banned'+response.data.id+' form-check-input" id="yes_banned'+response.data.id+'">';
                    html+='<label class="form-check-label" for="yes_banned'+response.data.id+'">Yes</label>';
                    html+='</div>';
                    html+='<div class="form-check float-left">';
                    html+=' <input type="radio" '+b_disable+' onclick="editSelectedRow('+"contractor_tr"+response.data.id+')" value="0" name="banned'+response.data.id+'"  class="banned'+response.data.id+' form-check-input" id="no_banned'+response.data.id+'">';
                    html+='<label class="form-check-label" for="no_banned'+response.data.id+'">No</label>';
                    html+='</div>';
                    html+='</td>';
                }
                html+='<td>';
                html+='<a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light delete-btn'+response.data.id+'" onclick="deletecontractordata()"><i class="fas fa-trash"></i></a>';
                html+='<a type="button" class="btn btn-primary btn-xs my-0 waves-effect all_action_btn_margin waves-light savecontractordata contractor_show_button_'+response.data.id+' show_tick_btn'+response.data.id+'" style="display:none;" id="'+response.data.id+'"><i class="fas fa-check"></i></a>';
                html+='</td>';

                html+='</tr>';
                $('#table-contractor').find('table').prepend(html);
            },
            error: function (error) {
                console.log(error);
            }
        });


    });
    $('body').on('click', '.savecontractordata', function() {
        var company_id=$('#companies_selected_nav').children("option:selected").val();
        var tr_id=$(this).attr('id');
        var data={
            'id':tr_id,
            'name':$('#name'+tr_id).html(),
            'start':$('#start'+tr_id).val(),
            'end':$('#end'+tr_id).val(),
            'preferred':$("input[name='preferred"+tr_id+"']:checked").val(),
            'banned':$("input[name='banned"+tr_id+"']:checked").val(),
            'company_id':company_id,

        };
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url:'/contractor/update',
            data:data,
            success: function(response)
            {
                if(response.error)
                {
                    toastr["error"](response.error);
                }
                else
                {
                    toastr["success"](response.message);
                    save_and_delete_btn_toggle(tr_id);
                    if(response.flag==1)
                    {
                        $('#contractor_tr'+tr_id).attr('style','background-color:#90ee90 !important');
                        // var url='/contractor/getall';

                    }
                    else
                    {
                        $('#contractor_tr'+tr_id).attr('style','background-color:#ff3547 !important');
                        // var url='/contractor/getallsoft';

                    }

                    // $('#contractor_table_id').DataTable().clear().destroy();
                    // contractor_data(url);
                    /**/

                }

            }

        });
    });

    $('#restorebuttoncontractor').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        if ($("#contractor_checkbox_all").prop('checked',true)){
            $("#contractor_checkbox_all").prop('checked',false);
        }
        selectedrows=[];
        $('#restorebuttoncontractor').hide();
        $('#deleteselectedcontractor').hide();
        $('#selectedactivebuttoncontractor').show();
        $('#deleteselectedcontractorSoft').show();
        $('#export_excel_contractor').hide();
        $('#export_world_contractor').hide();
        $('#export_pdf_contractor').hide();
        $('.add-contractor').hide();
        $('#activebuttoncontractor').show();
        var url='/contractor/getallSoft';
        $('#contractor_table_id').DataTable().clear().destroy();
        contractor_data(url);

    });
    $('#activebuttoncontractor').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        if ($("#contractor_checkbox_all").prop('checked',true)){
            $("#contractor_checkbox_all").prop('checked',false);
        }
        selectedrows=[];
        $('#export_excel_contractor').show()
        $('#export_world_contractor').show()
        $('#export_pdf_contractor').show()
        $('.add-contractor').show();
        $('#selectedactivebuttoncontractor').hide();
        $('#deleteselectedcontractor').show();
        $('#deleteselectedcontractorSoft').hide();
        $('#restorebuttoncontractor').show();
        $('#activebuttoncontractor').hide();
        var url='/contractor/getall';
        $('#contractor_table_id').DataTable().clear().destroy();
        contractor_data(url);

    });

});

function restorecontractordata(id)
{
    var url='/contractor/active';
    swal({
            title: "Are you sure?",
            text: "Selected option with be active ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){

            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {'id':id},
                    success: function(response)
                    {
                        if(response.flag==1)
                        {
                            var url='/contractor/getall';
                        }
                        else
                        {
                            var url='/contractor/getallSoft';
                        }
                        if ($("#contractor_checkbox_all").prop('checked',true)){
                            $("#contractor_checkbox_all").prop('checked',false);
                        }
                        $('#contractor_table_id').DataTable().clear().destroy();
                        contractor_data(url);

                        swal.close();
                        toastr["success"](response.message);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            } else {
                swal("Cancelled", "Your Record is safe", "error");

            }
        });
}
function delete_mutilpleData_contrater(url,selectedrows)
{
    if( $('.edit_cms_disable').css('display') == 'none' ) {
        console.log('Editer mode On Please change your mode');
        return 0;
    }
    if (selectedrows && selectedrows.length) {
        // not empty
        swal({
                title: "Are you sure?",
                text: "Selected option will be inactive and can be Active again by the admin only!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:url,
                        data: {'ids':selectedrows},
                        success: function(response)
                        {

                            toastr["success"](response.message);
                            if(response.flag==1)
                            {
                                var url='/contractor/getall';
                            }
                            else
                            {
                                var url='/contractor/getallSoft';
                            }
                            if ($("#contractor_checkbox_all").prop('checked',true)){
                                $("#contractor_checkbox_all").prop('checked',false);
                            }
                            $('#contractor_table_id').DataTable().clear().destroy();
                            contractor_data(url);

                            swal.close();

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                    swal("Cancelled", "Your Record is safe", "error");

                }
            });
    }
    else
    {
        // empty
        return toastr["error"]("Select at least one record to delete!");
    }
}
function delete_data_contractor(id,url)
{
    swal({
            title: "Are you sure?",
            text: "Selected option with be Deleted ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){

            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {'id':id},
                    success: function(response)
                    {
                        if(response.flag==1)
                        {
                            var url='/contractor/getall';
                        }
                        else
                        {
                            var url='/contractor/getallSoft';
                        }
                        $('#contractor_table_id').DataTable().clear().destroy();
                        contractor_data(url);

                        swal.close();
                        toastr["success"](response.message);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            } else {
                swal("Cancelled", "Your Record is safe", "error");

            }
        });
}
function deletesoftcontractordata(id)
{
    var url='/contractor/softdelete';
    delete_data_contractor(id,url);

}
function deletecontractordata(id)
{
    var url='/contractor/delete';
    delete_data_contractor(id,url);
}
