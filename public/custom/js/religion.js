$(document).ready(function() {
    var selectedrows=[];
    $('body').on('click','#religion_checkbox_all',function() {
        $('.selectedrowreligion').not(this).prop('checked', this.checked);
        $('.selectedrowreligion').each(function(key,value) {
            var id=$(this).attr('id');
            console.log(id);
            if(id)
            {
                id=id.split('religion_checked');
                row_id='#religion_tr'+id[1];
                if($(this).is(":checked"))
                {
                    if(key == 0)
                    {
                        selectedrows=[];
                    }
                    if(id[1] != '_checkbox_all')
                    {
                        selectedrows.push(id[1]);
                    }
                    $('.religion_export').val(JSON.stringify(selectedrows));
                    $(row_id).addClass('selected');
                }
                else
                {
                    $('.religion_export').val(1);
                    selectedrows=[];
                    $(row_id).removeClass('selected');
                }
            }
        });
    });


    $('body').on('click', '.selectedrowreligion', function() {
        var id=$(this).attr('id');
        id=id.split('religion_checked');
        row_id='#religion_tr'+id[1];
        if($(this).is(":checked"))
        {
            selectedrows.push(id[1]);
            $('.religion_export').val(JSON.stringify(selectedrows));
            $(row_id).addClass('selected');
        }else
        {
            if(selectedrows.includes(id[1]))
            {
                selectedrows.splice( selectedrows.indexOf(id[1]),1);
                $('.religion_export').val(JSON.stringify(selectedrows));
                $(row_id).removeClass('selected');
            }
        }
    });
    $('body').on('click', '.addreligionModal', () => {
        $.ajax({
            type: 'GET',
            url: '/employee/religion/store',
            success: function(response)
            {
               var html='';
                html+='<tr id="religion_tr'+response.data.id+'" class="selectedrowreligion'+response.data.id+'" role="row" class="hide odd">';
                html+='<td>';
                if ($("#employeeReligion_checkbox").val() ==1){
                    html+='<div class="form-check"><input type="checkbox" class="form-check-input religion_checked selectedrowreligion" id="religion_checked'+response.data.id+'"><label class="form-check-label" for="religion_checked'+response.data.id+'""></label></div>';
                }
                html+='</td>';
                if ($("#employeeReligion_name").val() ==1){
                    if ($("#employeeReligion_name_edit").val() ==1 && $("#edit_employeeReligion").val() ==1){
                        var n_create="true";
                    }else{
                        var n_create="false";
                    }
                    html+='<td class="pt-3-half edit_inline_religion" onkeypress="editSelectedRow('+"religion_tr"+response.data.id+')" data-id="'+response.data.id+'" id="religion_name'+response.data.id+'"   contenteditable="'+n_create+'"></td>';
                }
                html+='<td>';
                if ($("#delete_employeeReligion").val() ==1){
                    html+='<a type="button" class="btn deletereligiondata'+response.data.id+' btn-danger btn-xs my-0 waves-effect waves-light" onclick="deletereligiondata( ' +response.data.id+')"><i class="fas fa-trash"></i></a>';
                }
                if ($("#edit_employeeReligion").val() ==1){
                    html+='<a type="button" class="btn btn-primary btn-xs my-0 waves-effect waves-light savereligiondata all_action_btn_margin religion_show_button_'+response.data.id+'" style="display:none;"  id="'+response.data.id+'"><i class="fas fa-check"></i></a>';
                }
                html+='</td>';
                html+='</tr>';
                $('#table_religion').find('table').prepend(html);
            },
            error: function (error) {
                console.log(error);
            }
        });


    });
    $('#selectedactivebuttonreligion').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        var url='/employee/religion/active/multiple';
        if (selectedrows && selectedrows.length) {
       // not empty
            swal({
                title: "Are you sure?",
                text: "Selected option will be active  again ..!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
             },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:url,
                        data: {'ids':selectedrows},
                        success: function(response)
                        {

                            toastr["success"](response.message);
                            var url='/employee/religion/getallSoft';
                            $('#religion_table').DataTable().clear().destroy();
                            religion_data(url);
                            swal.close();

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                  swal("Cancelled", "Your Record is safe", "error");

                }
            });
         }
        else
        {
       // empty
            return toastr["error"]("Select at least one record to delete!");
        }
    });
    $('#deleteselectedreligion').click(function(){
       var url='/employee/religion/delete/multiple';
       delete_mutilpleDatareligion(url,selectedrows);
    });
    $('#deleteselectedreligionSoft').click(function(){
       var url='/employee/religion/delete/soft/multiple';
       delete_mutilpleDatareligion(url,selectedrows);
    });
    $('#restorebuttonreligion').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        $('#export_excel_religion').hide();
        $('#export_world_religion').hide();
        $('#export_pdf_religion').hide();
        selectedrows=[];
        $('#restorebuttonreligion').hide();
        $('#deleteselectedreligion').hide();
        $('#selectedactivebuttonreligion').show();
        $('#deleteselectedreligionSoft').show();
        $('.add_religion').hide();
        $('#activebuttonreligion').show();
        var url='/employee/religion/getallSoft';
        $('#religion_table').DataTable().clear().destroy();
        religion_data(url);
    });
    $('#activebuttonreligion').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        $('#export_excel_religion').show();
        $('#export_world_religion').show();
        $('#export_pdf_religion').show();
        $('.add_religion').show();
        $('#selectedactivebuttonreligion').hide();
        $('#deleteselectedreligion').show();
        $('#deleteselectedreligionSoft').hide();
        selectedrows=[];
        $('#restorebuttonreligion').show();
        $('#activebuttonreligion').hide();
        var url='/employee/religion/getall';
        $('#religion_table').DataTable().clear().destroy();
        religion_data(url);
    });
    $('body').on('click', '.savereligiondata', function() {
        var company_id=$('#companies_selected_nav').children("option:selected").val();
        var tr_id=$(this).attr('id');
        var data={
            'id':tr_id,
            'name':$('#religion_name'+tr_id).html(),
            'company_id':company_id,
        };
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url:'/employee/religion/update',
            data:data,
            success: function(response)
            {
                if(response.error)
                {
                    toastr["error"](response.error);
                    $("#religion_tr"+tr_id).attr('style','background-color: #fc685f');
                }
                else
                {
                    $("#religion_tr"+tr_id).attr('style','background-color: lightgreen !important');
                    toastr["success"](response.message);
                    $(".religion_show_button_"+tr_id).hide();
                    $('.deletereligiondata'+tr_id).show();

                }
            }
        });
        // console.log(data);
    });
});
function restorereligiondata(id)
{
    var url='/employee/religion/active';
    swal({
        title: "Are you sure?",
        text: "Selected option with be active ",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
     },
     function(isConfirm){

       if (isConfirm){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
             $.ajax({
                type: "POST",
                url: url,
                data: {'id':id},
                success: function(response)
                {
                    if(response.flag==1)
                    {
                        var url='/employee/religion/getall';
                    }
                    else
                    {
                        var url='/employee/religion/getallSoft';
                    }
                    $('#religion_table').DataTable().clear().destroy();
                    religion_data(url);
                    swal.close();
                    toastr["success"](response.message);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        } else {
          swal("Cancelled", "Your Record is safe", "error");

        }
     });
}
function delete_data_religion(id,url)
{
    swal({
            title: "Are you sure?",
            text: "Selected option will be Deleted ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){
            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {'id':id},
                    success: function(response)
                    {
                        $('#religion_table').DataTable().clear().destroy();
                        var url='/employee/religion/getall';
                        religion_data(url);
                        swal.close();
                        toastr["success"](response.message);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            } else {
                swal("Cancelled", "Your Record is safe", "error");

            }
        });
}

function delete_mutilpleDatareligion(url,selectedrows)
{
    if( $('.edit_cms_disable').css('display') == 'none' ) {
        console.log('Editer mode On Please change your mode');
        return 0;
    }

    if (selectedrows && selectedrows.length) {
   // not empty
        swal({
            title: "Are you sure?",
            text: "Selected option will be inactive and can be Active again by the admin only!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
         },
        function(isConfirm){

            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url:url,
                    data: {'ids':selectedrows},
                    success: function(response)
                    {

                        toastr["success"](response.message);
                        if(response.flag==1)
                        {
                            var url='/employee/religion/getall';
                        }
                        else
                        {
                            var url='/employee/religion/getallSoft';
                        }
                        $('#religion_table').DataTable().clear().destroy();
                        religion_data(url);
                        swal.close();

                    },
                    error: function (error) {
                        console.log(error);
                    }
                });

            } else {
              swal("Cancelled", "Your Record is safe", "error");

            }
        });
     }
    else
    {
   // empty
        return toastr["error"]("Select at least one record to delete!");
    }
}
 function delete_data_religion(id,url)
 {
    swal({
        title: "Are you sure?",
        text: "Selected option with be Deleted ",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
     },
     function(isConfirm){

       if (isConfirm){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
             $.ajax({
                type: "POST",
                url: url,
                data: {'id':id},
                success: function(response)
                {
                    if(response.flag==1)
                    {
                        var url='/employee/religion/getall';
                    }
                    else
                    {
                        var url='/employee/religion/getallSoft';
                    }
                    $('#religion_table').DataTable().clear().destroy();
                    religion_data(url);
                    swal.close();
                    toastr["success"](response.message);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        } else {
          swal("Cancelled", "Your Record is safe", "error");

        }
     });
 }
function deletesoftreligiondata(id)
{
    var url='/employee/religion/softdelete';
    delete_data_religion(id,url);

}
function deletereligiondata(id)
{
    var url='/employee/religion/delete';
    delete_data_religion(id,url);
}
