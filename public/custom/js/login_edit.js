$(document).ready(function() {
  $('.cms_login').click(function () {
    if( $('.edit_cms_disable').css('display') == 'none' ) {
      console.log('Editer mode On Please change your mode');
      return 0;
    }
    event.preventDefault();
    var key=$('#cms_key').val();
    var value=$('#cms_value').val();
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      type: "POST",
      url: '/login/cms/store',
      data: {'key':key,'value':value},
      success: function(response)
      {
        $('#editLoginDatatable').DataTable().ajax.reload();
        toastr["success"](response.message);
        $('#modalcmsLogin').modal('hide');

      },
      error: function (error) {
        console.log(error);
      }
    });

  });
  $('.cms_login_Edit').click(function () {
    if( $('.edit_cms_disable').css('display') == 'none' ) {
      console.log('Editer mode On Please change your mode');
      return 0;
    }
    event.preventDefault();
    var id=$('#cms_id').val();
    var key=$('#cms_key_edit').val();
    var value=$('#cms_value_edit').val();
    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
        type: "POST",
        url: '/login/cms/update',
        data: {'id':id,'key':key,'value':value},
        success: function(response)
        {
          $('#editLoginDatatable').DataTable().ajax.reload();
          toastr["success"](response.message);
          $('#modalEditloginCms').modal('hide');
        },
        error: function (error) {
            console.log(error);
        }
    });
  });
});
function cms_edit(id)
{
  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  $.ajax({
    type : 'POST',
    url : '/login/cms',
    data:{'id':id},
    success:function(data)
    {
      $('#cms_id').val(data.company_label_get_one.id);
      $('#cms_key_edit').val(data.key);
      $('#cms_value_edit').val(data.company_label_get_one.value);
      $('#modalEditloginCms').modal('show');
    }
  });
}

  
   
    