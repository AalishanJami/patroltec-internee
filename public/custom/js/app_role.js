 $(document).ready(function() {
    $('body').on('click', '.module_header_name', function() {
        var div_id=$(this).attr('id');
        var id=$(this).attr('data-value');
        $('#loader'+id).show();
        var role_id=$('#role_id').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url:'roles/edit/ajax',
            data:{'id':id,'role_id':role_id},
            success: function(response)
            {
                $('#loader'+id).hide();
                $('#module_content_name'+id).html(response);
                $('#'+div_id).removeClass('module_header_name');
            }
        });
    });

    $('body').on('click', '.appPermisionChange', function() {
        var role_id=$('#app_role_id').val();
        var permision_id=$(this).val();
        var flag='';
        if(this.checked) {
            flag=1;
        }
        else{
            flag=0;
        }
        var data={
            'app_role_id':role_id,
            'app_permission_id':permision_id,
            'flag':flag,
        };
        console.log("D : "+data);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url:'roles/update/ajax',
            data:data,
            success: function(response)
            {
                toastr["success"](response.message);
            }
        });

    });


 	$('body').on('click', '.copy_user', function() {
 		console.log($(this).attr('id'));
 		var url='/user/copy';
 		$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
 		$.ajax({
 			method: 'POST',
            type: 'json',
            url: url,
            data: {
                id: $(this).attr('id')
            },

            success: function(response)
            {
                console.log(response.user);
                $('#newuser_name2_id').addClass('active');
                $('#newuser_name2').val(response.user.name);
                $('#newuser_email2_id').addClass('active');
                $('#newuser_email2').val(response.user.email);
                $('#modalRegisterForm2').modal('show');

            },
            error: function (error) {
                console.log(error);
            }
        });
 	});

    $('#companies_selected_roles').change(function(){
        var url='/role/company/'+$( "#companies_selected_roles option:selected").val();
        $('#role_table').DataTable().clear().destroy();
        var table = $('#role_table').dataTable({
            processing: true,
            language: {
                'lengthMenu': '<span class="assign_class label'+$('#show_label').attr('data-id')+'" data-id="'+$('#show_label').attr('data-id')+'" data-value="'+$('#show_label').val()+'" >'+$('#show_label').val()+'</span>  _MENU_ <span class="assign_class label'+$('#entries_label').attr('data-id')+'" data-id="'+$('#entries_label').attr('data-id')+'" data-value="'+$('#entries_label').val()+'" >'+$('#entries_label').val()+'</span>',
                'search':'<span class="assign_class label'+$('#search_label').attr('data-id')+'" data-id="'+$('#search_label').attr('data-id')+'" data-value="'+$('#search_label').val()+'" >'+$('#search_label').val()+'</span>',
                'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                'info': '<span class="assign_class label'+$('#showing_page_label').attr('data-id')+'" data-id="'+$('#showing_page_label').attr('data-id')+'" data-value="'+$('#showing_page_label').val()+'" >'+$('#showing_page_label').val()+'</span> _PAGE_ <span class="assign_class label'+$('#of_label').attr('data-id')+'" data-id="'+$('#of_label').attr('data-id')+'" data-value="'+$('#of_label').val()+'" >'+$('#of_label').val()+'</span> _PAGES_',
                'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                'paginate': {
                    'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                    'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                },
                'infoFiltered': "(filtered from _MAX_ total records)"
            },

            "ajax": {
                "url": url,
                "type": 'get',
            },
             "createdRow": function( row, data, dataIndex ) {
            $(row).attr('id', data['id']);
            var temp=data['id'];
            $(row).attr('onclick',"selectedrow(this.id)");
            },
            columns: [
                 {data: 'checkbox', name: 'checkbox',visible:$('#checkbox_permission').val(),},
                    {data: 'name', name: 'name',visible:$('#name_permission').val(),},
                    {data: 'module_name', name: 'module_name',visible:$('#module_permission').val(),},
                    {data: 'actions', name: 'actions'},

                ],
            columnDefs: [{
            orderable: false,
            className: 'select-checkbox',
            targets: 0
            }],
            select: {
            style: 'os',
            selector: 'td:first-child'
            },

        });
        // $.ajax({
        //     method: 'GET',
        //     url: url,
        //     success: function(response)
        //     {
        //         $('#role_table').DataTable().clear().destroy();
        //         roleAppend();

        //     },
        //     error: function (error) {
        //         console.log(error);
        //     }
        // });

   });

    $('#edit_companies_selected').materialSelect();

    var company_id = 1;
    $('body').on('click', '#restorebuttonrole', function(){
        console.log('dsdsds');
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        $('#restorebuttonrole').hide();
        $('#showactivebuttonrole').show();
        $('#role_table').DataTable().clear().destroy();
        var table = $('#role_table').dataTable({
            processing: true,
            language: {
                    'lengthMenu': '<span class="assign_class label'+$('#show_label').attr('data-id')+'" data-id="'+$('#show_label').attr('data-id')+'" data-value="'+$('#show_label').val()+'" ></span>  _MENU_ <span class="assign_class label'+$('#entries_label').attr('data-id')+'" data-id="'+$('#entries_label').attr('data-id')+'" data-value="'+$('#entries_label').val()+'" ></span>',
                    'search':'<span class="assign_class label'+$('#search_id').attr('data-id')+'" data-id="'+$('#search_id').attr('data-id')+'" data-value="'+$('#search_id').val()+'" >'+$('#search_id').val()+'</span>',
                    'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                    'info': '<span class="assign_class label'+$('#showing_page_label').attr('data-id')+'" data-id="'+$('#showing_page_label').attr('data-id')+'" data-value="'+$('#showing_page_label').val()+'" >'+$('#showing_page_label').val()+'</span> _PAGE_ <span class="assign_class label'+$('#of_label').attr('data-id')+'" data-id="'+$('#of_label').attr('data-id')+'" data-value="'+$('#of_label').val()+'" >'+$('#of_label').val()+'</span> _PAGES_',
                    'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                    'paginate': {
                        'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                        'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                    },
                    'infoFiltered': " "
                },

            "ajax": {
                "url": '/roles/soft',
                "type": 'get',
            },
             "createdRow": function( row, data, dataIndex ) {
            $(row).attr('id', data['id']);
            var temp=data['id'];
            $(row).attr('onclick',"selectedrow(this.id)");
            },
            columns: [
                 {data: 'checkbox', name: 'checkbox',visible:$('#checkbox_permission').val(),},
                    {data: 'name', name: 'name',visible:$('#name_permission').val(),},
                    {data: 'module_name', name: 'module_name',visible:$('#module_permission').val(),},
                    {data: 'actions', name: 'actions'},

                ],
            columnDefs: [{
            orderable: false,
            className: 'select-checkbox',
            targets: 0
            }],
            select: {
            style: 'os',
            selector: 'td:first-child'
            },

        });
    });
    $('#showactivebuttonrole').click(function(){
        $('#restorebuttonrole').show();
        $('#showactivebuttonrole').hide();
        $('#role_table').DataTable().clear().destroy();
        roleAppend();
    });


    // buttonHolder.css({'transform' : 'translate(0px)'});


       $('#deleteselectedrole').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        if (selectedrows && selectedrows.length) {
       // not empty
            swal({
                title: "Are you sure?",
                text: "Users will be inactive and can be Active again by the admin only!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
             },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:'/role/delete/multiple',
                        data: {'ids':selectedrows},
                        success: function(response)
                        {

                            $('#role_table').DataTable().clear().destroy();
                            roleAppend();
                            swal.close()

                            toastr["success"]("User Deleted Successfully!");

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                  swal("Cancelled", "Your Record is safe", "error");
                     e.preventDefault();
                }
            });
        }
        else
        {
       // empty
            return toastr["error"]("Select at least one record to delete!");
        }
    });




});
    var selectedrows=[];
    function selectedrow(id)
    {
        alert(id.id);
        if(selectedrows.includes(id))
        {
            selectedrows.splice( selectedrows.indexOf(id), 1 );

        $('#usertable tr#'+id).removeClass('selected');
        }
        else
        {
            selectedrows.push(id);
            $('#usertable tr#'+id).addClass('selected');
        }

    }

    function getapproledata(id)
    {
        var get_current_path=$(".get_current_path").val();
        $('.loader').show();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type : 'post',
            url : 'roles/edit',
            data:{'id':id},
            success:function(data)
            {
                $('#edit_role_data').html(data);
                $('.loader').hide();
                $('[data-toggle="popover"]').popover();
            }
        });


    }
    function deleteroledata(id)
    {
        var custom_class='';
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            custom_class='assign_class get_text';
        }
        else
        {
            custom_class='assign_class';
        }
        console.log(custom_class);
        swal({
                title:'Are you sure?',
                text: "Delete Record.",
                type: "error",
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, Delete!",
                cancelButtonText: "Cancel!",
                closeOnConfirm: false,
                customClass: "confirm_class",
                closeOnCancel: false,
            },
            function(isConfirm){
                console.log( isConfirm);
                if( $('.edit_cms_disable').css('display') != 'none' ) {
                    if (isConfirm) {
                         $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $.ajax({
                            type : 'post',
                            url : '/role/delete',
                            data:{'id':id},
                            success:function(data)
                            {

                                setTimeout(function () {
                                    swal({
                                            title: "",
                                            text: "Delete Sucessfully!",
                                            type: "success",
                                            confirmButtonText: "OK"
                                        },

                                        function(isConfirm){

                                            if (isConfirm) {
                                                $('#role_table').DataTable().clear().destroy();
                                                roleAppend();

                                            }
                                        }); }, 500);


                            }
                        });

                    }
                    else
                    {
                       swal.close();
                    }
                }
            });

            var active ='<span class="'+custom_class+' label'+$('#are_you_sure_label').attr('data-id')+'" data-id="'+$('#are_you_sure_label').attr('data-id')+'" data-value="'+$('#are_you_sure_label').val()+'" >'+$('#are_you_sure_label').val()+'</span>';
            $('.confirm_class h2').html(active);
            active ='<span class="'+custom_class+' label'+$('#delete_record_label').attr('data-id')+'" data-id="'+$('#delete_record_label').attr('data-id')+'" data-value="'+$('#delete_record_label').val()+'" >'+$('#delete_record_label').val()+'</span>';
            $('.confirm_class p').html(active);
            active ='<span class="'+custom_class+' label'+$('#cancel_label').attr('data-id')+'" data-id="'+$('#cancel_label').attr('data-id')+'" data-value="'+$('#cancel_label').val()+'" >'+$('#cancel_label').val()+'</span>';
            $('.cancel').html(active);
            active ='<span class="'+custom_class+' label'+$('#delete_label').attr('data-id')+'" data-id="'+$('#delete_label').attr('data-id')+'" data-value="'+$('#delete_label').val()+'" >'+$('#delete_label').val()+'</span>';
            $('.confirm').html(active);


    }
    function restore_role(id)
    {
        var custom_class='';
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            custom_class='assign_class get_text';
        }
        else
        {
            custom_class='assign_class';
        }
        swal({
            title: "Are you sure?",
            text: "Selected user with be Active again!",
            type: "warning",
            showCloseButton: true,
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            customClass: "confirm_class",
            closeOnCancel: false
        },
        function(isConfirm){
            if( $('.edit_cms_disable').css('display') != 'none' ) {
               if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                     $.ajax({
                                type: "POST",
                                url:'/role/restore',
                                data: {'id':id},
                                success: function(response)
                                {
                                    $('#role_table').DataTable().ajax.reload();
                                         swal.close()
                                    toastr["success"]("User Activated Successfully!");
                                },
                                error: function (error) {
                                    console.log(error);
                                }
                            });
                }
                else {
                     swal.close();
                  // swal("Cancelled", "Your Record is safe", "error");
                     // e.preventDefault();
                }
            }
        });
        var active ='<span class="'+custom_class+' label'+$('#are_you_sure_label').attr('data-id')+'" data-id="'+$('#are_you_sure_label').attr('data-id')+'" data-value="'+$('#are_you_sure_label').val()+'" >'+$('#are_you_sure_label').val()+'</span>';
                    $('.confirm_class h2').html(active);
        active ='<span class="'+custom_class+' label'+$('#selected_user_with_be_active_again_label').attr('data-id')+'" data-id="'+$('#selected_user_with_be_active_again_label').attr('data-id')+'" data-value="'+$('#selected_user_with_be_active_again_label').val()+'" >'+$('#selected_user_with_be_active_again_label').val()+'</span>';
        $('.confirm_class p').html(active);
        active ='<span class="'+custom_class+' label'+$('#cancel_label').attr('data-id')+'" data-id="'+$('#cancel_label').attr('data-id')+'" data-value="'+$('#cancel_label').val()+'" >'+$('#cancel_label').val()+'</span>';
        $('.cancel').html(active);
        active ='<span class="'+custom_class+' label'+$('#delete_label').attr('data-id')+'" data-id="'+$('#delete_label').attr('data-id')+'" data-value="'+$('#delete_label').val()+'" >'+$('#delete_label').val()+'</span>';
        $('.confirm').html(active);

    }
    function hard_delete_role(id)
    {
     swal({
        title: "Are you sure?",
        text: "Selected user with be Hard Deleted and can active from backend again!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
     },
     function(isConfirm){

       if (isConfirm){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
             $.ajax({
                        type: "POST",
                        url: '/role/hard/delete',
                        data: {'id':id},
                        success: function(response)
                        {
                            $('#role_table').DataTable().ajax.reload();
                            swal.close()
                            toastr["success"]("User Hard Deleted Successfully!");
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
        } else {
          swal("Cancelled", "Your Record is safe", "error");
             e.preventDefault();
        }
     });



    }
    var selectedrows=[];
    function selectedrow(id)
    {
        if(selectedrows.includes(id))
        {
            selectedrows.splice( selectedrows.indexOf(id), 1 );

        $('#role_table tr#'+id).removeClass('selected');
        }
        else
        {
            selectedrows.push(id);
            $('#role_table tr#'+id).addClass('selected');
        }

    }
