 $(document).ready(function() {
      
    $('body').on('click','.formTypeCreate_btn',function() {
        console.log('rest');
        // $('.check_form_type').prop("checked", false);
        document.getElementById("form_type_form").reset();
        $('#form_type_id').val('');
        $('#formTypeCreate').modal('show');
      })
    var selectedrows=[];
     $('body').on('click','#form_type_checkbox_all',function() {
         // var x = document.getElementsByClassName("selectedrowform_type");
         // var i;
         // for (i = 0; i < x.length;i++) {
         //     var tr_id=x[i].id;
         //     tr_id=tr_id.split('form_type');
         //     tr_id=tr_id[1];
         //     if($(this).is(":checked",true))
         //     {
         //         selectedrows.push(tr_id);
         //         $('#form_type_tr'+tr_id).addClass('selected');
         //         $('#form_type'+tr_id).prop('checked',true);
         //         $('.form_type_export').val(JSON.stringify(selectedrows));
         //     }
         //     else
         //     {
         //         selectedrows.splice( selectedrows.indexOf(tr_id), 1 );
         //         $('#form_type_tr'+tr_id).removeClass('selected');
         //         $('#form_type'+tr_id).prop('checked',false);
         //         if (selectedrows.length >0){
         //             $('.form_type_export').val(JSON.stringify(selectedrows));
         //         }else{
         //             $('.form_type_export').val('1');
         //         }
         //     }
         // }
        $('input:checkbox').not(this).prop('checked', this.checked);
        $('input[type="checkbox"]').each(function(key,value) {
            var id=$(this).attr('id');
            console.log(id);
            if(id)
            {
              id=id.split('form_type');
              row_id='#form_type_tr'+id[1];
              if($(this).is(":checked"))
              {
                  if(key == 0)
                  {
                      selectedrows=[];
                  }
                  if(id[1] != '_checkbox_all')
                  {
                      selectedrows.push(id[1]);
                  }
                  $('.form_type_export').val(JSON.stringify(selectedrows));
                  $(row_id).addClass('selected');
              }
              else
              {
                  $('.form_type_export').val(1);
                  selectedrows=[];
                  $(row_id).removeClass('selected');
              }
            }
        });
     });
     $('body').on('click', '.selectedrowform_type', function() {
        // var tr_id=$(this).attr('id');
        // tr_id=tr_id.split('form_type');
        // tr_id=tr_id[1];
        // if(selectedrows.includes(tr_id))
        // {
        //     selectedrows.splice( selectedrows.indexOf(tr_id), 1 );
        //     $('#form_type_tr'+tr_id).removeClass('selected');
        //     $('#form_type'+tr_id).attr('checked',false);
        //     $('.form_type_export').val(JSON.stringify(selectedrows));

        // }
        // else
        // {
        //     selectedrows.push(tr_id);
        //     $('#form_type_tr'+tr_id).addClass('selected');
        //     $('#form_type'+tr_id).attr('checked',true);
        //     $('.form_type_export').val(JSON.stringify(selectedrows));

        // }
        var id=$(this).attr('id');
        id=id.split('form_type');
        row_id='#form_type_tr'+id[1];
        if($(this).is(":checked"))
        {
          selectedrows.push(id[1]);
          $('.form_type_export').val(JSON.stringify(selectedrows));
          $(row_id).addClass('selected');
        }else
        {
          if(selectedrows.includes(id[1]))
          {
            selectedrows.splice( selectedrows.indexOf(id[1]),1);
            $('.form_type_export').val(JSON.stringify(selectedrows));
            $(row_id).removeClass('selected');
          }
        }
    });
    $('#selectedactivebuttonform_type').click(function(){
        var url='/form_type/active/multiple';
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        if (selectedrows && selectedrows.length) {
       // not empty
            swal({
                title: "Are you sure?",
                text: "Selected option will be active  again ..!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
             },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:url,
                        data: {'ids':selectedrows},
                        success: function(response)
                        {
                            toastr["success"](response.message);
                            var url='/form_type/getallSoft';
                            $('#form_type_table_data').DataTable().clear().destroy();
                            form_type_data(url);
                            swal.close();

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                  swal("Cancelled", "Your Record is safe", "error");

                }
            });
         }
        else
        {
       // empty
            return toastr["error"]("Select at least one record to delete!");
        }
    });
    $('#deleteselectedform_type').click(function(){
       var url='/form_type/delete/multiple';
       delete_mutilpleData_site_group(url,selectedrows);
    });
    $('#deleteselectedform_typeSoft').click(function(){
       var url='/form_type/delete/soft/multiple';
       delete_mutilpleData_site_group(url,selectedrows);
    });

    $('.add_form_type').on('click', 'i', () => {
      $.ajax({
          type: 'GET',
          url: '/form_type/store',
          success: function(response)
          {
              var check_show_dashboard='';
              var check_count_dashboard='';
              var check_count_day='';
              var check_count_response_postive='';
              var check_count_response_negative='';
              var check_outanding_count='';
              var check_late_completion_count='';
              var check_detailed_field_stats='';
              if($('#show_dashboard_permission_create').val())
              {
                  check_show_dashboard='disabled';
              }
              if($('#count_dashboard_permission_create').val())
              {
                  check_count_dashboard='disabled';
              }
              if($('#count_day_permission_create').val())
              {
                  check_count_day='disabled';
              }
              if($('#count_response_positive_permission_create').val())
              {
                  check_count_response_postive='disabled';
              }
              if($('#count_response_negative_permission_create').val())
              {
                  check_count_response_negative='disabled';
              }
              if($('#outanding_count_permission_create').val())
              {
                  check_outanding_count='disabled';
              }
              if($('#late_completion_count_permission_create').val())
              {
                  check_late_completion_count='disabled';
              }
              if($('#detailed_field_stats_permission_create').val())
              {
                  check_detailed_field_stats='disabled';
              }
              var html='';
              html+= '<tr id="form_type_tr'+response.data.id+'" class="selectedrowform_type" role="row" class="hide odd">';
              html+=' <td><div class="form-check"><input type="checkbox" class="form-check-input" id="form_type'+response.data.id+'"><label class="form-check-label" for="form_type'+response.data.id+'""></label></div></td>';
              html+='<td class="pt-3-half">';
              html+='<a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light" onclick="deleteform_typedata('+response.data.id+')"><i class="fas fa-trash"></i></a>';
              html+='</td>';
              html+='<td class="pt-3-half edit_inline_form_type" data-id="'+response.data.id+'" id="name'+response.data.id+'" ';
              if($('#name_permission_create').val())
              {
                html+='contenteditable="true"';
              }
              html+='></td>';
              html+='<td class="pt-3-half">';
              html+='<div class="form-check float-left">';
              html+=' <input type="radio" '+check_show_dashboard+' value="1" name="show_dashboard'+response.data.id+'" class="show_dashboard'+response.data.id+' form-check-input show_button" data-id="'+response.data.id+'" id="yes_show_dashboard'+response.data.id+'">';
              html+='<label class="form-check-label" for="yes_show_dashboard'+response.data.id+'">Yes</label>';
              html+='</div>';
              html+='<div class="form-check float-left">';
              html+=' <input type="radio" '+check_show_dashboard+'  value="0" name="show_dashboard'+response.data.id+'"  class="show_dashboard'+response.data.id+' form-check-input show_button" data-id="'+response.data.id+'" id="no_show_dashboard'+response.data.id+'">';
              html+='<label class="form-check-label" for="no_show_dashboard'+response.data.id+'">No</label>';
              html+='</div>';
              html+='</td>';
              html+='<td class="pt-3-half">';
              html+='<div class="form-check float-left">';
              html+=' <input type="radio" '+check_count_dashboard+'  value="1" name="count_dashboard'+response.data.id+'" class="count_dashboard'+response.data.id+' form-check-input show_button" data-id="'+response.data.id+'" id="yes_count_dashboard'+response.data.id+'">';
              html+='<label class="form-check-label" for="yes_count_dashboard'+response.data.id+'">Yes</label>';
              html+='</div>';
              html+='<div class="form-check float-left">';
              html+=' <input type="radio" '+check_count_dashboard+'  value="0" name="count_dashboard'+response.data.id+'"  class="count_dashboard'+response.data.id+' form-check-input show_button" data-id="'+response.data.id+'" id="no_count_dashboard'+response.data.id+'">';
              html+='<label class="form-check-label" for="no_count_dashboard'+response.data.id+'">No</label>';
              html+='</div>';
              html+='</td>';
              html+='<td class="pt-3-half">';
              html+='<div class="form-check float-left">';
              html+=' <input type="radio" '+check_count_day+'  value="1" name="count_day'+response.data.id+'" class="count_day'+response.data.id+' form-check-input show_button" data-id="'+response.data.id+'" id="yes_count_day'+response.data.id+'">';
              html+='<label class="form-check-label" for="yes_count_day'+response.data.id+'">Yes</label>';
              html+='</div>';
              html+='<div class="form-check float-left">';
              html+=' <input type="radio" '+check_count_day+' value="0" name="count_day'+response.data.id+'"  class="count_day'+response.data.id+' form-check-input show_button" data-id="'+response.data.id+'" id="no_count_day'+response.data.id+'">';
              html+='<label class="form-check-label" for="no_count_day'+response.data.id+'">No</label>';
              html+='</div>';
              html+='</td>';
              html+='<td class="pt-3-half">';
              html+='<div class="form-check float-left">';
              html+=' <input type="radio" '+check_count_response_postive+' value="1" name="count_response_positive'+response.data.id+'" class="count_response_positive'+response.data.id+' form-check-input show_button" data-id="'+response.data.id+'" id="yes_count_response_positive'+response.data.id+'">';
              html+='<label class="form-check-label" for="yes_count_response_positive'+response.data.id+'">Yes</label>';
              html+='</div>';
              html+='<div class="form-check float-left">';
              html+=' <input type="radio" '+check_count_response_postive+' value="0" name="count_response_positive'+response.data.id+'"  class="count_response_positive'+response.data.id+' form-check-input show_button" data-id="'+response.data.id+'" id="no_count_response_positive'+response.data.id+'">';
              html+='<label class="form-check-label" for="no_count_response_positive'+response.data.id+'">No</label>';
              html+='</div>';
              html+='</td>';
              html+='<td class="pt-3-half">';
              html+='<div class="form-check float-left">';
              html+=' <input type="radio" '+check_count_response_negative+' value="1" name="count_response_negative'+response.data.id+'" class="count_response_negative'+response.data.id+' form-check-input show_button" data-id="'+response.data.id+'" id="yes_count_response_negative'+response.data.id+'">';
              html+='<label class="form-check-label" for="yes_count_response_negative'+response.data.id+'">Yes</label>';
              html+='</div>';
              html+='<div class="form-check float-left">';
              html+=' <input type="radio" '+check_count_response_negative+' value="0" name="count_response_negative'+response.data.id+'"  class="count_response_negative'+response.data.id+' form-check-input show_button" data-id="'+response.data.id+'" id="no_count_response_negative'+response.data.id+'">';
              html+='<label class="form-check-label" for="no_count_response_negative'+response.data.id+'">No</label>';
              html+='</div>';
              html+='</td>';
              html+='<td class="pt-3-half">';
              html+='<div class="form-check float-left">';
              html+=' <input type="radio" '+check_outanding_count+' value="1" name="outanding_count'+response.data.id+'" class="outanding_count'+response.data.id+' form-check-input show_button" data-id="'+response.data.id+'" id="yes_outanding_count'+response.data.id+'">';
              html+='<label class="form-check-label" for="yes_outanding_count'+response.data.id+'">Yes</label>';
              html+='</div>';
              html+='<div class="form-check float-left">';
              html+=' <input type="radio" '+check_outanding_count+' value="0" name="outanding_count'+response.data.id+'"  class="outanding_count'+response.data.id+' form-check-input show_button" data-id="'+response.data.id+'" id="no_outanding_count'+response.data.id+'">';
              html+='<label class="form-check-label" for="no_outanding_count'+response.data.id+'">No</label>';
              html+='</div>';
              html+='</td>';
              html+='<td class="pt-3-half">';
              html+='<div class="form-check float-left">';
              html+=' <input type="radio" '+check_late_completion_count+' value="1" name="late_completion_count'+response.data.id+'" class="late_completion_count'+response.data.id+' form-check-input show_button" data-id="'+response.data.id+'" id="yes_late_completion_count'+response.data.id+'">';
              html+='<label class="form-check-label" for="yes_late_completion_count'+response.data.id+'">Yes</label>';
              html+='</div>';
              html+='<div class="form-check float-left">';
              html+=' <input type="radio" '+check_late_completion_count+' value="0" name="late_completion_count'+response.data.id+'"  class="late_completion_count'+response.data.id+' form-check-input show_button" data-id="'+response.data.id+'" id="no_late_completion_count'+response.data.id+'">';
              html+='<label class="form-check-label" for="no_late_completion_count'+response.data.id+'">No</label>';
              html+='</div>';
              html+='</td>';
              html+='<td class="pt-3-half">';
              html+='<div class="form-check float-left">';
              html+=' <input type="radio" '+check_detailed_field_stats+' value="1" name="detailed_field_stats'+response.data.id+'" class="detailed_field_stats'+response.data.id+' form-check-input show_button" data-id="'+response.data.id+'" id="yes_detailed_field_stats'+response.data.id+'">';
              html+='<label class="form-check-label" for="yes_detailed_field_stats'+response.data.id+'">Yes</label>';
              html+='</div>';
              html+='<div class="form-check float-left">';
              html+=' <input type="radio" '+check_detailed_field_stats+'  value="0" name="detailed_field_stats'+response.data.id+'"  class="detailed_field_stats'+response.data.id+' form-check-input show_button" data-id="'+response.data.id+'" id="no_detailed_field_stats'+response.data.id+'">';
              html+='<label class="form-check-label" for="no_detailed_field_stats'+response.data.id+'">No</label>';
              html+='</div>';
              html+='</td>';
              html+='<td class="pt-3-half edit_inline_form_type" data-id="'+response.data.id+'" id="action_score'+response.data.id+'" contenteditable="true"></td>';
              html+='<td class="pt-3-half">';
              html+='<a type="button" class="btn btn-primary btn-xs my-0 waves-effect waves-light saveform_typedata " id="'+response.data.id+'"><i class="fas fa-check"></i></a>';
              html+='</td>';
              html+='</tr>';
              $('#table_form_type').find('table').prepend(html);

          },
          error: function (error) {
              console.log(error);
          }
      });
    });
      $('body').on('click', '.saveform_typedata', function() {
          var company_id=$('#companies_selected_nav').children("option:selected").val();
          var tr_id=$(this).attr('id');
          data = $('#form_type_form').serializeObject();
          data['company_id']=company_id;
          $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          });
          $.ajax({
              type:"POST",
              url:'/form_type/store',
              data:data,
              success: function(response)
              {
                  if(response.error)
                  {
                      toastr["error"](response.error);
                     // $("#form_type_tr"+tr_id).attr('style','background-color:#ff3547 !important');
                  }
                  else
                  {
                    toastr["success"](response.message);
                    var url='/form_type/getall';
                    $('#form_type_table_data').DataTable().clear().destroy();
                    form_type_data(url);
                    $('#formTypeCreate').modal('hide');
                    document.getElementById("form_type_form").reset();
                    $('#form_type_id').val('');
                  }

              }

          });

    });

    $('#restorebuttonform_type').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        $('#restorebuttonform_type').hide();
        $('#deleteselectedform_type').hide();
        $('#selectedactivebuttonform_type').show();
        $('#deleteselectedform_typeSoft').show();
        $('#export_excel_form_type').hide();
        $('#export_world_form_type').hide();
        $('#export_pdf_form_type').hide();
        $('.add_form_type').hide();
        $('#activebuttonform_type').show();
        var url='/form_type/getallSoft';
        $('#form_type_table_data').DataTable().clear().destroy();
        form_type_data(url);
    });
    $('#activebuttonform_type').click(function(){
        $('#export_excel_form_type').show();
        $('#export_world_form_type').show();
        $('#export_pdf_form_type').show();
        $('.add_form_type').show();
        $('#selectedactivebuttonform_type').hide();
        $('#deleteselectedform_type').show();
        $('#deleteselectedform_typeSoft').hide();
        $('#restorebuttonform_type').show();
        $('#activebuttonform_type').hide();
        var url='/form_type/getall';
        $('#form_type_table_data').DataTable().clear().destroy();
        form_type_data(url);
    });



});
 function restoreform_typedata(id)
 {
    var url='/form_type/active';
    swal({
        title: "Are you sure?",
        text: "Selected option with be active ",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
     },
     function(isConfirm){

       if (isConfirm){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
             $.ajax({
                        type: "POST",
                        url: url,
                        data: {'id':id},
                        success: function(response)
                        {
                            if(response.flag==1)
                            {
                                var url='/form_type/getall';
                            }
                            else
                            {
                                var url='/form_type/getallSoft';
                            }
                            $('#form_type_table_data').DataTable().clear().destroy();
                            form_type_data(url);
                            swal.close();
                            toastr["success"](response.message);
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
        } else {
          swal("Cancelled", "Your Record is safe", "error");

        }
     });
 }
 function delete_mutilpleData_site_group(url,selectedrows)
 {
     if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        if (selectedrows && selectedrows.length) {
       // not empty
            swal({
                title: "Are you sure?",
                text: "Selected option will be inactive and can be Active again by the admin only!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
             },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:url,
                        data: {'ids':selectedrows},
                        success: function(response)
                        {

                            toastr["success"](response.message);
                            if(response.flag==1)
                            {
                                var url='/form_type/getall';
                            }
                            else
                            {
                                var url='/form_type/getallSoft';
                            }
                            $('#form_type_table_data').DataTable().clear().destroy();
                            form_type_data(url);
                            swal.close();

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                  swal("Cancelled", "Your Record is safe", "error");

                }
            });
         }
        else
        {
       // empty
            return toastr["error"]("Select at least one record to delete!");
        }
 }
 function delete_data_form_type(id,url)
 {
    swal({
        title: "Are you sure?",
        text: "Selected option with be Deleted ",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
     },
     function(isConfirm){

       if (isConfirm){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
             $.ajax({
                        type: "POST",
                        url: url,
                        data: {'id':id},
                        success: function(response)
                        {
                            if(response.flag==1)
                            {
                                var url='/form_type/getall';
                            }
                            else
                            {
                                var url='/form_type/getallSoft';
                            }
                            $('#form_type_table_data').DataTable().clear().destroy();
                            form_type_data(url);
                            swal.close();
                            toastr["success"](response.message);
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
        } else {
          swal("Cancelled", "Your Record is safe", "error");

        }
     });
 }
function deletesoftform_typedata(id)
{
    var url='/form_type/softdelete';
    delete_data_form_type(id,url);

}
function deleteform_typedata(id)
 {
    var url='/form_type/delete';
    delete_data_form_type(id,url);
 }

function formTypeEdit(id)
{
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: '/form_type/edit',
        data: {id:id},
        success: function(data)
        {
            console.log(data);
            // document.getElementById("form_type_form").reset();
            $('.form_type_id').val(data.id);
            $('.form_type_name').val(data.name);
            $('.action_score').val(data.action_score);
            $('#show_dashboard').prop('checked',data._show_dashboard);
            $('#count_dashboard').prop('checked',data.count_dashboard);
            $('#count_day').prop('checked',data.count_day);
            $('#count_response_positive').prop('checked',data.count_response_postive);
            $('#count_response_negative').prop('checked',data.count_response_negative);
            $('#outanding_count').prop('checked',data.outanding_count);
            $('#late_completion_count').prop('checked',data.late_completion_count);
            $('#detailed_field_stats').prop('checked',data.detailed_field_stats);
            $('#formTypeCreate').modal('show');
        },
        error: function (error) {
            console.log(error);
        }
    });
}
