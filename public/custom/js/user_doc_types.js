 $(document).ready(function() {
    var selectedrows=[];
     $('body').on('click','#user_doc_type_table_checkbox_all',function() {
         $('.selectedrowuser_doc_type').not(this).prop('checked', this.checked);
         $('.selectedrowuser_doc_type').each(function(key,value) {
             var id=$(this).attr('id');
             id=id.split('user_doc_type');
             row_id='#user_doc_type_tr'+id[1];
             if($(this).is(":checked"))
             {
                 if(key == 0)
                 {
                     selectedrows=[];
                 }
                 if(id[1] != '_checkbox_all')
                 {
                     selectedrows.push(id[1]);
                 }
                 $('.user_doc_type_export').val(JSON.stringify(selectedrows));
                 $(row_id).addClass('selected');
             }
             else
             {
                 $('.user_doc_type_export').val(1);
                 selectedrows=[];
                 $(row_id).removeClass('selected');
             }
         });

     });
     $('body').on('click', '.selectedrowuser_doc_type', function() {
         var id=$(this).attr('id');
         id=id.split('user_doc_type');
         row_id='#user_doc_type_tr'+id[1];
         if($(this).is(":checked"))
         {
             selectedrows.push(id[1]);
             $('.user_doc_type_export').val(JSON.stringify(selectedrows));
             $(row_id).addClass('selected');
         }else
         {
             if(selectedrows.includes(id[1]))
             {
                 selectedrows.splice( selectedrows.indexOf(id[1]),1);
                 $('.user_doc_type_export').val(JSON.stringify(selectedrows));
                 $(row_id).removeClass('selected');
             }
         }
     });


     $('#selectedactivebuttonuser_doc_type').click(function(){
        var url='/user/doc_type/active/multiple';
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        if(selectedrows && selectedrows.length) {
          swal({
              title: "Are you sure?",
              text: "Selected option will be active  again ..!",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: '#DD6B55',
              confirmButtonText: 'Yes, I am sure!',
              cancelButtonText: "No, cancel it!",
              closeOnConfirm: false,
              closeOnCancel: false
           },
          function(isConfirm){

              if (isConfirm){
                  $.ajaxSetup({
                      headers: {
                          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                      }
                  });
                  $.ajax({
                      type: "POST",
                      url:url,
                      data: {'ids':selectedrows},
                      success: function(response)
                      {
                          // customer_table
                          toastr["success"](response.message);
                          var url='/user/doc_type/getallSoft';
                          $('#user_doc_type_table').DataTable().clear().destroy();
                          user_doc_type_data(url);
                          swal.close();

                      },
                      error: function (error) {
                          console.log(error);
                      }
                  });

              } else {
                swal("Cancelled", "Your Record is safe", "error");

              }
          });
         }
        else
        {
          return toastr["error"]("Select at least one record to delete!");
        }
    });
    $('#deleteselecteduser_doc_type').click(function(){
       var url='/user/doc_type/delete/multiple';
       delete_mutilpleData_user_doc_type(url,selectedrows);
    });
    $('#deleteselecteduser_doc_typeSoft').click(function(){
      var url='/user/doc_type/delete/soft/multiple';
      delete_mutilpleData_user_doc_type(url,selectedrows);
    });

    $('.add_user_doc_type').on('click', 'i', () => {
      $.ajax({
          type: 'GET',
          url: '/user/doc_type/store',
          success: function(response)
          {
              var html='';
              html+= '<tr id="user_doc_type_tr'+response.data.id+'" class="selectedrowuser_doc_type" role="row" class="hide odd">';
              html+=' <td><div class="form-check"><input type="checkbox" class="form-check-input" id="user_doc_types'+response.data.id+'"><label class="form-check-label" for="user_doc_types'+response.data.id+'"></label></div></td>';


              html+='<td class="pt-3-half edit_inline_user_doc_type" data-id="'+response.data.id+'" >';
              html+='<select onchange="editSelectedRow(user_doc_type_tr'+response.data.id+')" id="doc_type_id'+response.data.id+'"  style="background-color: inherit;border: 0px">';
              for (var j = response.doc_type.length - 1; j >= 0; j--) {
                  html+='<option  value="'+response.doc_type[j].id+'"  >'+response.doc_type[j].name+'</option>';
              }
              html+='</select></td>';
              html+='<td class="pt-3-half edit_inline_user_doc_type" data-id="'+response.data.id+'" >';
              html+='<select id="position'+response.data.id+'" onchange="editSelectedRow(user_doc_type_tr'+response.data.id+')"  style="background-color: inherit;border: 0px">';
              for (var j = response.positions.length - 1; j >= 0; j--) {
                  html+='<option  value="'+response.positions[j].id+'"  >'+response.positions[j].name+'</option>';
              }
              html+='</select></td>';
              // html+='<td class="sorting_1"></td>';
              html+='<td><a type="button" class="btn btn-danger btn-xs my-0 waves-effect  waves-light delete-btn'+response.data.id+'" onclick="deleteuser_doc_typedata('+response.data.id+')"><i class="fas fa-trash"></i></a><a type="button" class="btn btn-primary btn-xs my-0 waves-effect waves-light saveuser_doc_typedata show_tick_btn'+response.data.id+'" id="'+response.data.id+'" style="display: none;"><i class="fas fa-check"></i></a></td>'
              html+='</tr>';
              $('#table-user_doc_type').find('table').prepend(html);

          },
          error: function (error) {
              console.log(error);
          }
      });
    });
      $('body').on('click', '.saveuser_doc_typedata', function() {
          var company_id=$('#companies_selected_nav').children("option:selected").val();
          var tr_id=$(this).attr('id');
          var doc_type_id = $('#doc_type_id'+tr_id).children("option:selected").val();
          var position = $('#position'+tr_id).children("option:selected").val();
          var data={
              'id':tr_id,
              // 'name':$('#nameS'+tr_id).html(),
              'doc_type_id':doc_type_id,
              'position_id':position,
              'company_id':company_id,
          };
          $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
          });
          $.ajax({
              type:"POST",
              url:'/user/doc_type/update',
              data:data,
              success: function(response)
              {
                  if(response.error)
                  {
                      toastr["error"](response.error);
                  } else {
                      toastr["success"](response.message);
                      if(response.flag==1)
                      {
                          $("#user_doc_type_tr"+tr_id).attr('style','background-color:#90ee90 !important');
                          // var url='/user/doc_type/getall';
                      } else {
                          $("#user_doc_type_tr"+tr_id).attr('style','background-color:#ff3547 !important');
                          // var url='/user/doc_type/getallsoft';
                      }
                      // $('#user_doc_type_table').DataTable().clear().destroy();
                      // user_doc_type_data(url);

                      $(".delete-btn"+tr_id).show();
                      $(".show_tick_btn"+tr_id).hide();
                  }
              }
          });



    });

    $('#restorebuttonuser_doc_type').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        selectedrows=[];
        $('#restorebuttonuser_doc_type').hide();
        $('#deleteselecteduser_doc_type').hide();
        $('#selectedactivebuttonuser_doc_type').show();
        $('#deleteselecteduser_doc_typeSoft').show();
        $('#export_excel_user_doc_type').hide();
        $('#export_world_user_doc_type').hide();
        $('#export_pdf_user_doc_type').hide();
        $('.add-user_doc_type').hide();
        $('#activebuttonuser_doc_type').show();
        var url='/user/doc_type/getallSoft';
        $('#user_doc_type_table').DataTable().clear().destroy();
        user_doc_type_data(url);
    });
    $('#activebuttonuser_doc_type').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        selectedrows=[];
        $('#export_excel_user_doc_type').show();
        $('#export_world_user_doc_type').show();
        $('#export_pdf_user_doc_type').show();
        $('.add-user_doc_type').show();
        $('#selectedactivebuttonuser_doc_type').hide();
        $('#deleteselecteduser_doc_type').show();
        $('#deleteselecteduser_doc_typeSoft').hide();
        $('#restorebuttonuser_doc_type').show();
        $('#activebuttonuser_doc_type').hide();
        var url='/user/doc_type/getall';
        $('#user_doc_type_table').DataTable().clear().destroy();
        user_doc_type_data(url);
    });



});
 function restoreuser_doc_typedata(id)
 {
    var url='/user/doc_type/active';
    swal({
        title: "Are you sure?",
        text: "Selected option with be active ",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
     },
     function(isConfirm){

       if (isConfirm){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
             $.ajax({
                        type: "POST",
                        url: url,
                        data: {'id':id},
                        success: function(response)
                        {
                            if(response.flag==1)
                            {
                                var url='/user/doc_type/getall';
                            }
                            else
                            {
                                var url='/user/doc_type/getallSoft';
                            }
                            $('#user_doc_type_table').DataTable().clear().destroy();
                    user_doc_type_data(url);
                            swal.close();
                            toastr["success"](response.message);
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
        } else {
          swal("Cancelled", "Your Record is safe", "error");

        }
     });
 }
 function delete_mutilpleData_user_doc_type(url,selectedrows)
 {
     if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        if (selectedrows && selectedrows.length) {
       // not empty
            swal({
                title: "Are you sure?",
                text: "Selected option will be inactive and can be Active again by the admin only!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
             },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:url,
                        data: {'ids':selectedrows},
                        success: function(response)
                        {

                            toastr["success"](response.message);
                            if(response.flag==1)
                            {
                                var url='/user/doc_type/getall';
                            }
                            else
                            {
                                var url='/user/doc_type/getallSoft';
                            }
                            $('#user_doc_type_table').DataTable().clear().destroy();
                            user_doc_type_data(url);
                            swal.close();

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                  swal("Cancelled", "Your Record is safe", "error");

                }
            });
         }
        else
        {
       // empty
            return toastr["error"]("Select at least one record to delete!");
        }
 }
 function delete_data_group(id,url)
 {
    swal({
        title: "Are you sure?",
        text: "Selected option with be Deleted ",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
     },
     function(isConfirm){

       if (isConfirm){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
             $.ajax({
                        type: "POST",
                        url: url,
                        data: {'id':id},
                        success: function(response)
                        {
                            if(response.flag==1)
                            {
                                var url='/user/doc_type/getall';
                            }
                            else
                            {
                                var url='/user/doc_type/getallSoft';
                            }
                            $('#user_doc_type_table').DataTable().clear().destroy();
                            user_doc_type_data(url);
                            swal.close();
                            toastr["success"](response.message);
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
        } else {
          swal("Cancelled", "Your Record is safe", "error");

        }
     });
 }
function deletesoftuser_doc_typedata(id)
{
    var url='/user/doc_type/softdelete';
    delete_data_group(id,url);

}
function deleteuser_doc_typedata(id)
 {
    var url='/user/doc_type/delete';
    delete_data_group(id,url);
 }
