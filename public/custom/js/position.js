$(document).ready(function() {
    var selectedrows=[];

    $('body').on('click','#position_checkbox_all',function() {
        $('.selectedrowposition').not(this).prop('checked', this.checked);
        $('.selectedrowposition').each(function(key,value) {
            var id=$(this).attr('id');
            console.log(id);
            if(id)
            {
                id=id.split('position_checked');
                row_id='#position_tr'+id[1];
                if($(this).is(":checked"))
                {
                    if(key == 0)
                    {
                        selectedrows=[];
                    }
                    if(id[1] != '_checkbox_all')
                    {
                        selectedrows.push(id[1]);
                    }
                    $('.position_export').val(JSON.stringify(selectedrows));
                    $(row_id).addClass('selected');
                }
                else
                {
                    $('.position_export').val(1);
                    selectedrows=[];
                    $(row_id).removeClass('selected');
                }
            }
        });
    });

    $('body').on('click', '.selectedrowposition', function() {
        var id=$(this).attr('id');
        id=id.split('position_checked');
        row_id='#position_tr'+id[1];
        if($(this).is(":checked"))
        {
            selectedrows.push(id[1]);
            $('.position_export').val(JSON.stringify(selectedrows));
            $(row_id).addClass('selected');
        }else
        {
            if(selectedrows.includes(id[1]))
            {
                selectedrows.splice( selectedrows.indexOf(id[1]),1);
                $('.position_export').val(JSON.stringify(selectedrows));
                $(row_id).removeClass('selected');
            }
        }
    });
    $('#selectedactivebuttonposition').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        var url='/employee/position/active/multiple';
        if (selectedrows && selectedrows.length) {
       // not empty
            swal({
                title: "Are you sure?",
                text: "Selected option will be active  again ..!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
             },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:url,
                        data: {'ids':selectedrows},
                        success: function(response)
                        {

                            toastr["success"](response.message);
                            var url='/employee/position/getallSoft';
                            $('#position_table').DataTable().clear().destroy();
                            position_data(url);
                            swal.close();

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                  swal("Cancelled", "Your Record is safe", "error");

                }
            });
         }
        else
        {
       // empty
            return toastr["error"]("Select at least one record to delete!");
        }
    });
    $('#deleteselectedposition').click(function(){
       var url='/employee/position/delete/multiple';
       delete_mutilpleDataPosition(url,selectedrows);
    });
    $('#deleteselectedpositionSoft').click(function(){
       var url='/employee/position/delete/soft/multiple';
       delete_mutilpleDataPosition(url,selectedrows);
    });
    $('#restorebuttonposition').click(function(){

        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        $('#export_excel_position_btn').hide();
        $('#export_world_position_btn').hide();
        $('#export_pdf_position_btn').hide();
        $('#restorebuttonposition').hide();
        $('#deleteselectedposition').hide();
        $('#selectedactivebuttonposition').show();
        $('#deleteselectedpositionSoft').show();
        $('.addpositionModal').hide();
        $('#activebuttonposition').show();
        selectedrows=[];
        $('#position_table').DataTable().clear().destroy();
        var url='/employee/position/getallSoft';
        position_data(url);
    });
    $('#activebuttonposition').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        $('#export_excel_position_btn').show();
        $('#export_world_position_btn').show();
        $('#export_pdf_position_btn').show();
        $('.addpositionModal').show();
        selectedrows=[];
        $('#selectedactivebuttonposition').hide();
        $('#deleteselectedposition').show();
        $('#deleteselectedpositionSoft').hide();
        $('#restorebuttonposition').show();
        $('#activebuttonposition').hide();
        var url='/employee/position/getall';
        $('#position_table').DataTable().clear().destroy();
        position_data(url);
    });
    $('body').on('click', '.addpositionModal', () => {
        $.ajax({
            type: 'GET',
            url: '/employee/position/store',
            success: function(response)
            {
                var html='';
                html+='<tr id="position_tr'+response.data.id+'" role="row" class="hide odd selectedrowposition'+response.data.id+'">';
                html+='<td>';
                if ($("#employeePosition_checkbox").val()==1) {
                    html+='<div class="form-check"><input type="checkbox" class="form-check-input relationship_checked selectedrowposition" id="position_checked'+response.data.id+'"><label class="form-check-label" for="position_checked'+response.data.id+'""></label></div>';
                }
                html+='</td>';
                if ($("#employeePosition_name").val()==1) {
                    var n_disable = ($("#employeePosition_name_create").val()==1 && $("#create_employeePosition").val()==1) ? "true":"false";
                    html+='<td class="pt-3-half edit_inline_position" onkeypress="editSelectedRow('+"position_tr"+response.data.id+')" data-id="'+response.data.id+'" id="position_name'+response.data.id+'"   contenteditable="'+n_disable+'"></td>';
                }
                html+='<td>';
                if ($("#delete_employeePosition").val()==1) {
                    html += '<a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light deletepositiondata'+response.data.id+' delete-btn'+response.data.id+'" onclick="deletepositiondata( ' + response.data.id + ')"><i class="fas fa-trash"></i></a>';
                }
                if ($("#edit_employeePosition").val()==1){
                    html+='<a type="button" class="btn btn-primary all_action_btn_margin btn-xs my-0 waves-effect waves-light savepositiondata position_show_button_'+response.data.id+' show_tick_btn'+response.data.id+'" style="display: none;"  id="'+response.data.id+'"><i class="fas fa-check"></i></a>';
                }
                html+='</td>';
                html+='</tr>';
                $('#table_position').find('table').prepend(html);
            },
            error: function (error) {
                console.log(error);
            }
        });
    });
    $('body').on('click', '.savepositiondata', function() {
        var company_id=$('#companies_selected_nav').children("option:selected").val();
        var tr_id=$(this).attr('id');
        console.log($(this).attr('id'));
        var data={
            'id':tr_id,
            'name':$('#position_name'+tr_id).html(),
            'company_id':company_id,

        };
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url:'/employee/position/update',
            data:data,
            success: function(response)
            {
                if(response.error)
                {
                    toastr["error"](response.error);
                    $("#position_tr"+tr_id).attr('style','background-color: #fc685f');
                }
                else
                {
                    $("#position_tr"+tr_id).attr('style','background-color: lightgreen !important');
                    toastr["success"](response.message);
                    // save_and_delete_btn_toggle(tr_id);
                    $('.position_show_button_'+tr_id).hide();

                    $('.delete-btn'+tr_id).show();
                    $('.show_tick_btn'+tr_id).hide();
                }
            }
        });
        // console.log(data);
    });
});
function restorepositiondata(id)
{
    var url='/employee/position/active';
    swal({
        title: "Are you sure?",
        text: "Selected option with be active ",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
     },
     function(isConfirm){

       if (isConfirm){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
             $.ajax({
                        type: "POST",
                        url: url,
                        data: {'id':id},
                        success: function(response)
                        {
                            if(response.flag==1)
                            {
                                var url='/employee/position/getall';
                            }
                            else
                            {
                                var url='/employee/position/getallSoft';
                            }
                            $('#position_table').DataTable().clear().destroy();
                            position_data(url);
                            swal.close();
                            toastr["success"](response.message);
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
        } else {
          swal("Cancelled", "Your Record is safe", "error");

        }
     });
}
function delete_data_position(id,url)
{
    swal({
            title: "Are you sure?",
            text: "Selected option will be Deleted ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){
            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {'id':id},
                    success: function(response)
                    {
                        $('#position_table').DataTable().clear().destroy();
                        var url='/employee/position/getall';
                        position_data(url);
                        swal.close();
                        toastr["success"](response.message);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            } else {
                swal("Cancelled", "Your Record is safe", "error");

            }
        });
}

function delete_mutilpleDataPosition(url,selectedrows)
{
    if( $('.edit_cms_disable').css('display') == 'none' ) {
        console.log('Editer mode On Please change your mode');
        return 0;
    }

    if (selectedrows && selectedrows.length) {
   // not empty
        swal({
            title: "Are you sure?",
            text: "Selected option will be inactive and can be Active again by the admin only!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
         },
        function(isConfirm){

            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url:url,
                    data: {'ids':selectedrows},
                    success: function(response)
                    {

                        toastr["success"](response.message);
                        if(response.flag==1)
                        {
                            var url='/employee/position/getall';
                        }
                        else
                        {
                            var url='/employee/position/getallSoft';
                        }
                        $('#position_table').DataTable().clear().destroy();
                        position_data(url);
                        swal.close();

                    },
                    error: function (error) {
                        console.log(error);
                    }
                });

            } else {
              swal("Cancelled", "Your Record is safe", "error");

            }
        });
     }
    else
    {
   // empty
        return toastr["error"]("Select at least one record to delete!");
    }
 }
 function delete_data_employee(id,url)
 {
    swal({
        title: "Are you sure?",
        text: "Selected option with be Deleted ",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
     },
     function(isConfirm){

       if (isConfirm){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
             $.ajax({
                        type: "POST",
                        url: url,
                        data: {'id':id},
                        success: function(response)
                        {
                            if(response.flag==1)
                            {
                                var url='/employee/position/getall';
                            }
                            else
                            {
                                var url='/employee/position/getallSoft';
                            }
                            $('#position_table').DataTable().clear().destroy();
                            position_data(url);
                            swal.close();
                            toastr["success"](response.message);
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
        } else {
          swal("Cancelled", "Your Record is safe", "error");

        }
     });
 }
function deletesoftpositiondata(id)
{
    var url='/employee/position/softdelete';
    delete_data_employee(id,url);

}
function deletepositiondata(id)
{
    var url='/employee/position/delete';
    delete_data_employee(id,url);
}

