$(document).ready(function() {

//  $('.company_table_static').find('tr').each(function(){ $(this).find('th').eq(0).before('<th></th>'); $(this).find('td').eq(0).before('<td><div class="form-check"><input type="checkbox" class="form-check-input" id="materialUncheckedTest"><label class="form-check-label" for="materialUncheckedTest"></label></div></td>'); });



    $('body').on('click', '#create_user', function() {
        $.ajax({
            type: "GET",
            url: 'user/create/company/list',
            success: function(response)
            {
                $('#companyCreateModel').html(response);
                console.log(response);
                $('.mdb-select').materialSelect();
                $('#all_companies').change(function() {
      
                    if (this.checked) {

                         $(".company").prop("checked", true);
                        
                    } else {

                         $(".company").prop("checked", false);
                        
                    }
                });

            },
            error: function (error) {
                console.log(error);
            }
        });
        // $('#modalRegisterForm2').modal('show');   
    });
    $('#showactivebutton').click(function(){
        $('#export_excel_user').val(1);
        $('#export_word_user').val(1);
        
        $('#restorebutton').show();
        $('#showactivebutton').hide();
        $('#usertable').DataTable().clear().destroy();
        userAppend();
    });

    $('#deleteselected').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        if (selectedrows && selectedrows.length) {
       // not empty
            swal({
                title: "Are you sure?",
                text: "Users will be inactive and can be Active again by the admin only!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
             },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:'/user/delete/multiple',
                        data: {'ids':selectedrows},
                        success: function(response)
                        {

                            $('#usertable').DataTable().clear().destroy();
                            userAppend();
                            swal.close()

                            toastr["success"]("User Deleted Successfully!");
                            toastr.options = {
                  "closeButton": false,
                  "debug": false,
                  "newestOnTop": false,
                  "progressBar": false,
                  "positionClass": "toast-top-center",
                  "preventDuplicates": false,
                  "onclick": null,
                  "showDuration": "300",
                  "hideDuration": "1000",
                  "timeOut": "5000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                };

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                  swal("Cancelled", "Your Record is safe", "error");
                     e.preventDefault();
                }
            });
    } 
        else 
        {
       // empty
            return toastr["error"]("Select at least one record to delete!");
        }
    });

 	$('body').on('click', '.copy_user', function() {
 		console.log($(this).attr('id'));
 		var url='/user/copy';
 		$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

 		$.ajax({
 			method: 'POST',
            type: 'json',
            url: url,
            data: {
                id: $(this).attr('id')
            },
             
            success: function(response)
            {
                console.log(response.html);
                $('#newuser_name2_id').addClass('active');
                $('#newuser_name2').val(response.user.name);
                $('#newuser_email2_id').addClass('active');
                $('#newuser_email2').val(response.user.email);
                $('#companyCreateModel').html(response.html);
                $('.mdb-select').materialSelect();
                $('#modalRegisterForm2').modal('show');
                   $('#all_companies').change(function() {
      
                                                    if (this.checked) {

                                                         $(".company").prop("checked", true);
                                                        
                                                    } else {

                                                         $(".company").prop("checked", false);
                                                        
                                                    }
                                             });

            },
            error: function (error) {
                console.log(error);
            }
        });
 	});
   

    // $('.mdb-select').materialSelect();
    // $('#edit_companies_selected').materialSelect();

    var company_id = 1;
   

    $('#restorebutton').click(function(){

        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        $('#restorebutton').hide();
        $('#export_excel_user').val(2);
        $('#export_word_user').val(2);
        $('#showactivebutton').show();
        $('#usertable').DataTable().clear().destroy();
        var table = $('#usertable').dataTable({
            processing: true,
            language: {
                'lengthMenu': '<span class="assign_class label'+$('#show_label').attr('data-id')+'" data-id="'+$('#show_label').attr('data-id')+'" data-value="'+$('#show_label').val()+'" >'+$('#show_label').val()+'</span>  _MENU_ <span class="assign_class label'+$('#entries_label').attr('data-id')+'" data-id="'+$('#entries_label').attr('data-id')+'" data-value="'+$('#entries_label').val()+'" >'+$('#entries_label').val()+'</span>',
                'search':'<span class="assign_class label'+$('#search_label').attr('data-id')+'" data-id="'+$('#search_label').attr('data-id')+'" data-value="'+$('#search_label').val()+'" >'+$('#search_label').val()+'</span>',
                'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                'info': '<span class="assign_class label'+$('#showing_page_label').attr('data-id')+'" data-id="'+$('#showing_page_label').attr('data-id')+'" data-value="'+$('#showing_page_label').val()+'" >'+$('#showing_page_label').val()+'</span> _PAGE_ <span class="assign_class label'+$('#of_label').attr('data-id')+'" data-id="'+$('#of_label').attr('data-id')+'" data-value="'+$('#of_label').val()+'" >'+$('#of_label').val()+'</span> _PAGES_',
                'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                'paginate': {
                    'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                    'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                },
                'infoFiltered': "(filtered from _MAX_ total records)"
            },

            "ajax": {
                "url": '/user/soft',
                "type": 'get',
            },
             "createdRow": function( row, data, dataIndex ) {
              $( row ).find('td:eq(1)') .attr('contenteditable',true);
                    $( row ).find('td:eq(2)') .attr('contenteditable',true);
            $(row).attr('id', data['id']);
            var temp=data['id'];
            $(row).attr('onclick',"selectedrowuser(this.id)");
            },
            columns: [
                {data: 'checkbox', name: 'checkbox',visible:$('#checkbox_permission').val(),},
                    {data: 'name', name: 'name',visible:$('#name_permission').val(),},
                    {data: 'email', name: 'email',visible:$('#email_permission').val(),},
                    {data: 'actions', name: 'actions'},

                ],
            columnDefs: [{
            orderable: false,
            className: 'select-checkbox',
            targets: 0
            }],
            select: {
            style: 'os',
            selector: 'td:first-child'
            },

        });
    });
    $('#registernewuser2').submit(function () {
         if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        event.preventDefault();
        var name=$('#newuser_name2').val();
        var email=$('#newuser_email2').val();
        var password=$('#newuser_password2').val();
        var password_confirm=$('#newuser_password_confirm2').val();
        if( $('.companies_selected:checked').length > 0){
            //build an array of selected values
            var selected_companies = [];
            
            $('.companies_selected:checked').each(function(i, selected) {
                selected_companies[i] = $(selected).val();
            });
        }
        console.log(selected_companies);
        // selected_companies.splice(selected_companies.indexOf(""), 1);

        if(password != password_confirm)
        {
            return toastr["error"]("Password and Confirm Password not match!");
        }
        // company_ids':JSON.stringify(selected_companies)

        $.ajax({
            type: "POST",
            url: 'user/store',
            data: {'name':name,'email':email,'password':password,'confirm-password':password_confirm,'selected_companies':selected_companies},
            success: function(response)
            {
                console.log(response);

                if (response.user_exist== true)
                {
                    toastr["error"]("Email ALready Exists!!");
                    toastr.options = {
                  "closeButton": false,
                  "debug": false,
                  "newestOnTop": false,
                  "progressBar": false,
                  "positionClass": "toast-top-center",
                  "preventDuplicates": false,
                  "onclick": null,
                  "showDuration": "300",
                  "hideDuration": "1000",
                  "timeOut": "5000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                };
                }
                else {
                    $('#usertable').DataTable().clear().destroy();
                    userAppend();
                    toastr["success"]("New User Created Successfully!");
                    toastr.options = {
                  "closeButton": false,
                  "debug": false,
                  "newestOnTop": false,
                  "progressBar": false,
                  "positionClass": "toast-top-center",
                  "preventDuplicates": false,
                  "onclick": null,
                  "showDuration": "300",
                  "hideDuration": "1000",
                  "timeOut": "5000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                };
                    $('#modalRegisterForm2').modal('hide');

                }


            },
            error: function (error) {
                console.log(error);
            }
        });

    });


    $('#edit_name_email').submit(function () {
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        event.preventDefault();
        var name=$('#edit_user_name').val();
        var email=$('#edit_user_email').val();
        var id=$('#user_id_hidden').val();
        if( $('.edit_companies_selected:checked').length > 0){
            //build an array of selected values
            var selected_companies = [];
            $('.edit_companies_selected:checked').each(function(i, selected) {
                selected_companies[i] = $(selected).val();
            });
        }
        console.log(selected_companies);

         $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        // selected_companies.splice(selected_companies.indexOf(""), 1);
        // console.log(selected_companies);
        $.ajax({
            type: "POST",
            url:'user/update',
            data: {'id':id,'name':name,'email':email,'companies':JSON.stringify(selected_companies)},
            success: function(response)
            {
                console.log(response);

                $('#usertable').DataTable().ajax.reload();

                toastr["success"](" User Updated Successfully!");
                toastr.options = {
                  "closeButton": false,
                  "debug": false,
                  "newestOnTop": false,
                  "progressBar": false,
                  "positionClass": "toast-top-center",
                  "preventDuplicates": false,
                  "onclick": null,
                  "showDuration": "300",
                  "hideDuration": "1000",
                  "timeOut": "5000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                };
                $('#modalEditForm').modal('hide');

            },
            error: function (error) {
                console.log(error);
            }
        });

    });
    $('#edit_password').submit(function () {
        if( $('.edit_cms_disable').css('display') == 'none' ) {
                    console.log('Editer mode On Please change your mode');
                    return 0;
                }
        event.preventDefault();
        var password=$('#newuser_password').val();
        var password_confirm=$('#newuser_password_confirm').val();

        if(password != password_confirm)
        {
            return toastr["error"]("Password and Confirm Password not match!");
        }

        var id=$('#user_id_hidden').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "POST",
            url: 'user/update/password',
            data: {'id':id,'password':password},
            success: function(response)
            {
                console.log(response);

                $('#usertable').DataTable().ajax.reload();
                toastr["success"](" Password Updated Successfully!");
                toastr.options = {
                  "closeButton": false,
                  "debug": false,
                  "newestOnTop": false,
                  "progressBar": false,
                  "positionClass": "toast-top-center",
                  "preventDuplicates": false,
                  "onclick": null,
                  "showDuration": "300",
                  "hideDuration": "1000",
                  "timeOut": "5000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                };
                $('#modalEditForm').modal('hide');

            },
            error: function (error) {
                console.log(error);
            }
        });

    });
        

});
    var selectedrows=[];
    function selectedrowuser(id)
    {
        console.log(id);
        if(selectedrows.includes(id))
        {
            selectedrows.splice( selectedrows.indexOf(id), 1 );

        $('#usertable tr#'+id).removeClass('selected');
        }
        else
        {
            selectedrows.push(id);
            $('#usertable tr#'+id).addClass('selected');
        }

    }
    function getuserdata(id)
    {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type : 'post',
            url : '/user',
            data:{'id':id},
            success:function(data)
            {

                console.log(data);
                $('#edit_user_name').val(data.name);

                // $('#edit_user_email').empty();
                $('#edit_user_email').val(data.email);
                $('#user_id_hidden').val(data.id);
                $('#select_edit').html(data.html);
                 $('.mdb-select').materialSelect();
                // $('#edit_companies_selected').val(data['companies']).trigger('change');
                   $('#all_companies').change(function() {
      
                                                    if (this.checked) {

                                                         $(".company").prop("checked", true);
                                                        
                                                    } else {

                                                         $(".company").prop("checked", false);
                                                        
                                                    }
                                             });
            }
        });


    }

    function getuserrole(id)
    {

        $('#user_id_hidden').val(id);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type : 'post',
            url : '/user/role',
            data:{'id':id},
            success:function(data)
            {
                console.log(data);
                $('#user_role_assign').html(data);
                // $('#roles_selected').multiSelect('deselect_all');
                // $('#roles_selected').multiSelect('select',data);
            }
        });

    }
    function restore_user(id)
    {
        var custom_class='';
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            custom_class='assign_class get_text';
        }
        else
        {
            custom_class='assign_class';
        }
        swal({
            title: "Are you sure?",
            text: "Selected user with be Active again!",
            type: "warning",
            showCloseButton: true,
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            customClass: "confirm_class",
            closeOnCancel: false
        },
        function(isConfirm){
            if( $('.edit_cms_disable').css('display') != 'none' ) {
               if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                     $.ajax({
                                type: "POST",
                                url:'/user/restore',
                                data: {'id':id},
                                success: function(response)
                                {
                                    $('#usertable').DataTable().ajax.reload();
                                         swal.close()
                                    toastr["success"]("User Activated Successfully!");
                                    toastr.options = {
                  "closeButton": false,
                  "debug": false,
                  "newestOnTop": false,
                  "progressBar": false,
                  "positionClass": "toast-top-center",
                  "preventDuplicates": false,
                  "onclick": null,
                  "showDuration": "300",
                  "hideDuration": "1000",
                  "timeOut": "5000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                };
                                },
                                error: function (error) {
                                    console.log(error);
                                }
                            });
                } 
                else {
                     swal.close();
                  // swal("Cancelled", "Your Record is safe", "error");
                     // e.preventDefault();
                }
            }
        });
        var active ='<span class="'+custom_class+' label'+$('#are_you_sure_label').attr('data-id')+'" data-id="'+$('#are_you_sure_label').attr('data-id')+'" data-value="'+$('#are_you_sure_label').val()+'" >'+$('#are_you_sure_label').val()+'</span>';
                    $('.confirm_class h2').html(active);
        active ='<span class="'+custom_class+' label'+$('#selected_user_with_be_active_again_label').attr('data-id')+'" data-id="'+$('#selected_user_with_be_active_again_label').attr('data-id')+'" data-value="'+$('#selected_user_with_be_active_again_label').val()+'" >'+$('#selected_user_with_be_active_again_label').val()+'</span>';
        $('.confirm_class p').html(active);
        active ='<span class="'+custom_class+' label'+$('#cancel_label').attr('data-id')+'" data-id="'+$('#cancel_label').attr('data-id')+'" data-value="'+$('#cancel_label').val()+'" >'+$('#cancel_label').val()+'</span>';
        $('.cancel').html(active);
        active ='<span class="'+custom_class+' label'+$('#delete_label').attr('data-id')+'" data-id="'+$('#delete_label').attr('data-id')+'" data-value="'+$('#delete_label').val()+'" >'+$('#delete_label').val()+'</span>';
        $('.confirm').html(active);

    }
    function hard_delete_user(id)
    {
     swal({
        title: "Are you sure?",
        text: "Selected user with be Hard Deleted and can active from backend again!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
     },
     function(isConfirm){

       if (isConfirm){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
             $.ajax({
                        type: "POST",
                        url: '/user/hard/delete',
                        data: {'id':id},
                        success: function(response)
                        {
                            $('#usertable').DataTable().clear().destroy();
                            userAppend();
                            swal.close()
                            toastr["success"]("User Hard Deleted Successfully!");
                            toastr.options = {
                  "closeButton": false,
                  "debug": false,
                  "newestOnTop": false,
                  "progressBar": false,
                  "positionClass": "toast-top-center",
                  "preventDuplicates": false,
                  "onclick": null,
                  "showDuration": "300",
                  "hideDuration": "1000",
                  "timeOut": "5000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                };
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
        } else {
          swal("Cancelled", "Your Record is safe", "error");
             e.preventDefault();
        }
     });



    }
      function deleteuserdata(id)
        {
            var custom_class='';
            if( $('.edit_cms_disable').css('display') == 'none' ) {
                custom_class='assign_class get_text';
            }
            else
            {
                custom_class='assign_class';
            }
            console.log(custom_class);
            swal({
                    title:'Are you sure?',
                    text: "Delete Record.",
                    type: "error",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, Delete!",
                    cancelButtonText: "Cancel!",
                    closeOnConfirm: false,
                    customClass: "confirm_class",
                    closeOnCancel: false,
                },
                function(isConfirm){
                    console.log( isConfirm);
                    if( $('.edit_cms_disable').css('display') != 'none' ) {
                        if (isConfirm) {
                             $.ajaxSetup({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                }
                            });
                            $.ajax({
                                type : 'post',
                                url : '/user/delete',
                                data:{'id':id},
                                success:function(data)
                                {

                                    setTimeout(function () {
                                        swal({
                                                title: "",
                                                text: "Delete Sucessfully!",
                                                type: "success",
                                                confirmButtonText: "OK"
                                            },

                                            function(isConfirm){

                                                if (isConfirm) {
                                                    $('#usertable').DataTable().clear().destroy();
                                                    userAppend();
                                                }
                                            }); }, 500);


                                }
                            });

                        }
                        else
                        {
                           swal.close();
                        }
                    }
                });

                var active ='<span class="'+custom_class+' label'+$('#are_you_sure_label').attr('data-id')+'" data-id="'+$('#are_you_sure_label').attr('data-id')+'" data-value="'+$('#are_you_sure_label').val()+'" >'+$('#are_you_sure_label').val()+'</span>';
                $('.confirm_class h2').html(active);
                active ='<span class="'+custom_class+' label'+$('#delete_record_label').attr('data-id')+'" data-id="'+$('#delete_record_label').attr('data-id')+'" data-value="'+$('#delete_record_label').val()+'" >'+$('#delete_record_label').val()+'</span>';
                $('.confirm_class p').html(active);
                active ='<span class="'+custom_class+' label'+$('#cancel_label').attr('data-id')+'" data-id="'+$('#cancel_label').attr('data-id')+'" data-value="'+$('#cancel_label').val()+'" >'+$('#cancel_label').val()+'</span>';
                $('.cancel').html(active);
                active ='<span class="'+custom_class+' label'+$('#delete_label').attr('data-id')+'" data-id="'+$('#delete_label').attr('data-id')+'" data-value="'+$('#delete_label').val()+'" >'+$('#delete_label').val()+'</span>';
                $('.confirm').html(active);



              
          

        }







