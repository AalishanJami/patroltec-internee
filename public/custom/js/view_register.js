 $(document).ready(function() {
    $('body').on('click', '.save_btn_view_register', function() {
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        var url='/view_register/updatePermision'
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        // console.log(question_permision);
        $.ajax({
            type: "POST",
            url:url,
            data: {'ids':question_permision},
            success: function(response)
            {
                toastr["success"](response.message);
                swal.close();
            },
            error: function (error) {
                console.log(error);
            }
        });
    });
});
   