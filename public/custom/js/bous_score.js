$(document).ready(function() {
    var selectedrows=[];
    $('body').on('click','#bonusscore_checkbox_all',function() {
        // var x = document.getElementsByClassName("selectedrowbonus_score");
        // var i;
        // for (i = 0; i < x.length;i++) {
        //     var tr_id=x[i].id;
        //     tr_id=tr_id.split('bonus_score');
        //     tr_id=tr_id[1];
        //     if(selectedrows.includes(tr_id))
        //     {
        //         selectedrows.splice( selectedrows.indexOf(tr_id), 1 );
        //         if (selectedrows.length >0){
        //             $('.bonus_score_export').val(JSON.stringify(selectedrows));
        //         }else{
        //             $('.bonus_score_export').val('1');
        //         }
        //         $('#bonus_score_tr'+tr_id).removeClass('selected');
        //         $('#bonus_score'+tr_id).attr('checked',false);
        //     }
        //     else
        //     {
        //         selectedrows.push(tr_id);
        //         $('.bonus_score_export').val(JSON.stringify(selectedrows));
        //         $('#bonus_score_tr'+tr_id).addClass('selected');
        //         $('#bonus_score'+tr_id).attr('checked',true);
        //     }

        // }
        $('input:checkbox').not(this).prop('checked', this.checked);
        $('input[type="checkbox"]').each(function(key,value) {
            var id=$(this).attr('id');
            id=id.split('bonus_score');
            row_id='#bonus_score_tr'+id[1];
            if($(this).is(":checked"))
            {
                if(key == 0)
                {
                    selectedrows=[];
                }
                if(id[1] != '_checkbox_all')
                {
                    selectedrows.push(id[1]);
                }
                $('.bonus_score_export').val(JSON.stringify(selectedrows));
                $(row_id).addClass('selected');
            }
            else
            {
                $('.bonus_score_export').val(1);
                selectedrows=[];
                $(row_id).removeClass('selected');
            }
        });

    });
    $('body').on('click', '.selectedrowbonus_score', function() {
        // var tr_id=$(this).attr('id');
        // tr_id=tr_id.split('bonus_score');
        // tr_id=tr_id[1];
        // if(selectedrows.includes(tr_id))
        // {
        //     selectedrows.splice( selectedrows.indexOf(tr_id), 1 );
        //     if (selectedrows.length >0){
        //         $('.bonus_score_export').val(JSON.stringify(selectedrows));
        //     }else{
        //         $('.bonus_score_export').val('1');
        //     }
        //     $('#bonus_score_tr'+tr_id).removeClass('selected');
        //     $('#bonus_score'+tr_id).attr('checked',false);
        // }
        // else
        // {
        //     selectedrows.push(tr_id);
        //     $('.bonus_score_export').val(JSON.stringify(selectedrows));
        //     $('#bonus_score_tr'+tr_id).addClass('selected');
        //     $('#bonus_score'+tr_id).attr('checked',true);
        // }
        var id=$(this).attr('id');
        id=id.split('bonus_score');
        row_id='#bonus_score_tr'+id[1];
        if($(this).is(":checked"))
        {
            selectedrows.push(id[1]);
            $('.bonus_score_export').val(JSON.stringify(selectedrows));
            $(row_id).addClass('selected');
        }else
        {
            if(selectedrows.includes(id[1]))
            {
                selectedrows.splice( selectedrows.indexOf(id[1]),1);
                $('.bonus_score_export').val(JSON.stringify(selectedrows));
                $(row_id).removeClass('selected');
            }
        }
    });
    $('#selectedactivebuttonbonus_score').click(function(){
        var url='/bonus/score/active/multiple';
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        if (selectedrows && selectedrows.length) {
            // not empty
            swal({
                    title: "Are you sure?",
                    text: "Selected option will be active  again ..!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Yes, I am sure!',
                    cancelButtonText: "No, cancel it!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm){

                    if (isConfirm){
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $.ajax({
                            type: "POST",
                            url:url,
                            data: {'ids':selectedrows},
                            success: function(response)
                            {
                                toastr["success"](response.message);
                                var url='/bonus/score/getallSoft';
                                $('.scoring_table_static_document').DataTable().clear().destroy();
                                bonus_score_data(url);
                                swal.close();

                            },
                            error: function (error) {
                                console.log(error);
                            }
                        });

                    } else {
                        swal("Cancelled", "Your Record is safe", "error");

                    }
                });
        }
        else
        {
            // empty
            return toastr["error"]("Select at least one record to delete!");
        }
    });
    $('#deleteselectedbonus_score').click(function(){
        var url='/bonus/score/delete/multiple';
        delete_mutilpleData(url,selectedrows);
    });
    $('#deleteselectedbonus_scoreSoft').click(function(){
        var url='/bonus/score/delete/soft/multiple';
        delete_mutilpleData(url,selectedrows);
    });
    $('.add-bonus').on('click', 'i', () => {
        $.ajax({
            type: 'GET',
            url: '/bonus/score/store',
            success: function(response)
            {
                var date = new Date();
                var html='';
                html+='<tr id="bonus_score_tr'+response.data.id+'"role="row" class="hide odd">';

                if ($("#bonusScore_checkbox").val()==1) {
                    html+='<td><div class="form-check"><input type="checkbox" class="form-check-input selectedrowbonus_score" id="bonus_score'+response.data.id+'"><label class="form-check-label" for="bonus_scores'+response.data.id+'"></label></div></td>';
                }

                if ($("#bonusScore_month").val()==1) {
                    var m_edit = ($("#bonusScore_month_create").val()==1)? 'true':'false';
                    var disabled = ($("#bonusScore_month_create").val()==1)? '':'disabled';
                    html += '<td class="pt-3-half edit_inline_bonus_score" data-id="' + response.data.id + '">';
                    html += '<select onchange="editSelectedRow('+"bonus_score_tr"+response.data.id+')" '+disabled+' id="month' + response.data.id + '"  style="background-color: inherit;border: 0px"><option selected disabled value="">--Select Month--</option><option ' + disabled + ' value="1">Jan</option><option ' + disabled + ' value="2">Feb</option><option ' + disabled + ' value="3">Mar</option><option ' + disabled + ' value="4">April</option><option ' + disabled + ' value="5">May</option><option ' + disabled + ' value="6">June</option><option ' + disabled + ' value="7">July</option><option ' + disabled + ' value="8">August</option> <option ' + disabled + ' value="9">September</option><option ' + disabled + ' value="10">October</option><option ' + disabled + ' value="11">November</option><option ' + disabled + ' value="12">December</option></select>';
                    html += '</td>';
                }
                if ($("#bonusScore_year").val()==1) {
                    var y_edit = ($("#bonusScore_year_create").val()==1)? 'true':'false';
                    var year = ($("#bonusScore_year_create").val()==1)? date.getFullYear():'';
                    html+='<td class="pt-3-half edit_inline_bonus_score" onkeypress="editSelectedRow('+"bonus_score_tr"+response.data.id+')" data-id="'+response.data.id+'" id="year'+response.data.id+'" contenteditable="'+y_edit+'">'+year+'</td>';
                }

                if ($("#bonusScore_score").val()==1) {
                    var s_edit = ($("#bonusScore_score_create").val()==1)? 'true':'false';
                    html+='<td class="pt-3-half edit_inline_bonus_score" onkeypress="editSelectedRow('+"bonus_score_tr"+response.data.id+')" data-id="'+response.data.id+'" id="bonus_score_filed'+response.data.id+'" contenteditable="'+s_edit+'"></td>';
                }
                if ($("#bonusScore_notes").val()==1) {
                    var n_edit = ($("#bonusScore_notes_create").val()==1)? 'true':'false';
                    html+='<td class="pt-3-half edit_inline_bonus_score" onkeypress="editSelectedRow('+"bonus_score_tr"+response.data.id+')" data-id="'+response.data.id+'" id="notes'+response.data.id+'" contenteditable="'+n_edit+'"></td>';
                }
                html+='<td>';
                if ($("#delete_bonusScore").val()==1) {
                    html+='<a class="btn btn-danger btn-xs my-0 waves-effect waves-light delete-btn'+response.data.id+'" onclick="deletebonus_scoredata('+response.data.id+')"><i class="fas fa-trash"></i></a>';
                }
                if ($("#edit_bonusScore").val()==1) {
                    html+='<a  class="btn btn-primary btn-xs all_action_btn_margin waves-effect waves-light savebonus_scoredata scoredata_show_button_'+response.data.id+' show_tick_btn'+response.data.id+'" style="display: none;" id="'+response.data.id+'"><i class="fas fa-check"></i></a>';
                }
                html+='</td>';
                html+='</tr>';
                $('#table-bonus_score').find('table').prepend(html);

            },
            error: function (error) {
                console.log(error);
            }
        });


    });
    $('body').on('click', '.savebonus_scoredata', function() {
        var company_id=$('#companies_selected_nav').children("option:selected").val();
        var tr_id=$(this).attr('id');
        var data={
            'id':tr_id,
            'month':$('#month'+tr_id).children("option:selected").val(),
            'bonus_score':$('#bonus_score_filed'+tr_id).html(),
            'year':$('#year'+tr_id).html(),
            'notes':$('#notes'+tr_id).html(),
            'company_id':company_id,

        };
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url:'/bonus/score/update',
            data:data,
            success: function(response)
            {
                if(response.error)
                {
                    toastr["error"](response.error);
                    $("#bonus_score_tr"+tr_id).attr('style','background-color:#fc685f !important');
                }
                else
                {
                    toastr["success"](response.message);
                    save_and_delete_btn_toggle(tr_id);
                    if(response.flag==1)
                    {
                        $("#bonus_score_tr"+tr_id).attr('style','background-color:#90ee90 !important');
                        // var url='/bonus/score/getall';
                    }
                    else
                    {
                        $("#bonus_score_tr"+tr_id).attr('style','background-color:#ff3547 !important');
                        // var url='/bonus/score/getallsoft';
                    }
                    // $('.scoring_table_static_document').DataTable().clear().destroy();
                    // bonus_score_data(url);
                }
            }
        });

        // console.log(data);

    });

    $('#restorebuttonbonus_score').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        selectedrows=[];
        $('#export_excel_bonus_score_btn').hide();
        $('#export_world_bonus_score_btn').hide();
        $('#export_pdf_bonus_score_btn').hide();
        $('#restorebuttonbonus_score').hide();
        $('#deleteselectedbonus_score').hide();
        $('#selectedactivebuttonbonus_score').show();
        $('#deleteselectedbonus_scoreSoft').show();
        $('#export_excel_bonus_score').val(2);
        $('#export_world_bonus_score').val(2);
        $('#export_world_pdf').val(2);
        $('.add-bonus').hide();
        $('#activebuttonbonus_score').show();
        var url='/bonus/score/getallSoft';
        $('.scoring_table_static_document').DataTable().clear().destroy();
        bonus_score_data(url);
    });
    $('#activebuttonbonus_score').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        selectedrows=[];
        $('#export_excel_bonus_score_btn').show();
        $('#export_world_bonus_score_btn').show();
        $('#export_pdf_bonus_score_btn').show();
        $('#export_excel_bonus_score').val(1);
        $('#export_world_bonus_score').val(1);
        $('#export_world_pdf').val(1);
        $('.add-bonus').show();
        $('#selectedactivebuttonbonus_score').hide();
        $('#deleteselectedbonus_score').show();
        $('#deleteselectedbonus_scoreSoft').hide();
        $('#restorebuttonbonus_score').show();
        $('#activebuttonbonus_score').hide();
        var url='/bonus/score/getall';
        $('.scoring_table_static_document').DataTable().clear().destroy();
        bonus_score_data(url);
    });



});
function restorebonus_scoredata(id)
{
    var url='/bonus/score/active';
    swal({
            title: "Are you sure?",
            text: "Selected option with be active ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
    function(isConfirm){
        if (isConfirm){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: url,
                data: {'id':id},
                success: function(response)
                {
                    if(response.flag==1)
                    {
                        var url='/bonus/score/getall';
                    }
                    else
                    {
                        var url='/bonus/score/getallSoft';
                    }
                    $('.scoring_table_static_document').DataTable().clear().destroy();
                    bonus_score_data(url);
                    swal.close();
                    toastr["success"](response.message);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        } else {
            swal("Cancelled", "Your Record is safe", "error");
        }
    });
 }
 function delete_mutilpleData(url,selectedrows)
 {
     if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
     }
     console.log(selectedrows);
    if (selectedrows && selectedrows.length) {
        // not empty
        swal({
                title: "Are you sure?",
                text: "Selected option will be inactive and can be Active again by the admin only!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:url,
                        data: {'ids':selectedrows},
                        success: function(response)
                        {

                            toastr["success"](response.message);
                            if(response.flag==1)
                            {
                                var url='/bonus/score/getall';
                            }
                            else
                            {
                                var url='/bonus/score/getallSoft';
                            }
                            $('.scoring_table_static_document').DataTable().clear().destroy();
                            bonus_score_data(url);
                            swal.close();

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                    swal("Cancelled", "Your Record is safe", "error");

                }
            });
    }
    else {
        // empty
        return toastr["error"]("Select at least one record to delete!");
    }
}
function delete_data_bonus_score(id,url)
{
    swal({
            title: "Are you sure?",
            text: "Selected option with be Deleted ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){

            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {'id':id},
                    success: function(response)
                    {
                        if(response.flag==1)
                        {
                            var url='/bonus/score/getall';
                        }
                        else
                        {
                            var url='/bonus/score/getallSoft';
                        }
                        $('.scoring_table_static_document').DataTable().clear().destroy();
                        bonus_score_data(url);
                        swal.close();
                        toastr["success"](response.message);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            } else {
                swal("Cancelled", "Your Record is safe", "error");

            }
        });
}
function deletesoftbonus_scoredata(id)
{
    var url='/bonus/score/softdelete';
    delete_data_bonus_score(id,url);

}
function deletebonus_scoredata(id)
{
    var url='/bonus/score/delete';
    delete_data_bonus_score(id,url);
}
