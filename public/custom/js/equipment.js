 $(document).ready(function() {
    var selectedrows=[];
     $('body').on('click','#equipment_checkbox_all',function() {
        $('input:checkbox').not(this).prop('checked', this.checked);
        $('input[type="checkbox"]').each(function(key,value) {
            var id=$(this).attr('id');
            id=id.split('equipment2s');
            row_id='#equipment2_tr'+id[1];
            if($(this).is(":checked"))
            {
                if(key == 0)
                {
                    selectedrows=[];
                }
                if(id[1] != '_checkbox_all')
                {
                    selectedrows.push(id[1]);
                }
                $('.equipment_export').val(JSON.stringify(selectedrows));
                $(row_id).addClass('selected');
            }
            else
            {
                $('.equipment_export').val(1);
                selectedrows=[];
                $(row_id).removeClass('selected');
            }
        });
         // var x = document.getElementsByClassName("selectedrowequipment2");
         // var i;
         // for (i = 0; i < x.length;i++) {
         //     var tr_id=x[i].id;
         //     tr_id=tr_id.split('equipment2s');
         //     tr_id=tr_id[1];
         //     if(selectedrows.includes(tr_id))
         //     {
         //         selectedrows.splice( selectedrows.indexOf(tr_id), 1 );
         //         if (selectedrows.length >0){
         //             $('.equipment_export').val(JSON.stringify(selectedrows));
         //         }else{
         //             $('.equipment_export').val('1');
         //         }
         //         $('#equipment2_tr'+tr_id).removeClass('selected');
         //         $('#equipment2s'+tr_id).attr('checked',false);
         //     }
         //     else
         //     {
         //         selectedrows.push(tr_id);
         //         $('.equipment_export').val(JSON.stringify(selectedrows));
         //         $('#equipment2_tr'+tr_id).addClass('selected');
         //         $('#equipment2s'+tr_id).attr('checked',true);

         //     }
         // }

     });

     $('body').on('click', '.selectedrowequipment2', function() {
        // var tr_id=$(this).attr('id');
        // tr_id=tr_id.split('equipment2s');
        // tr_id=tr_id[1];
        // if(selectedrows.includes(tr_id))
        // {
        //     selectedrows.splice( selectedrows.indexOf(tr_id), 1 );

        //     if (selectedrows.length >0){
        //         $('.equipment_export').val(JSON.stringify(selectedrows));
        //     }else{
        //         $('.equipment_export').val('1');
        //     }
        //     $('#equipment2_tr'+tr_id).removeClass('selected');
        //     // $('#equipments'+tr_id).attr('checked',false);
        // }
        // else
        // {
        //     selectedrows.push(tr_id);
        //     $('.equipment_export').val(JSON.stringify(selectedrows));
        //     $('#equipment2_tr'+tr_id).addClass('selected');
        //     // $('#equipments'+tr_id).attr('checked',true);
        // }
        var id=$(this).attr('id');
        id=id.split('equipment2s');
        row_id='#equipment2_tr'+id[1];
        if($(this).is(":checked"))
        {
            selectedrows.push(id[1]);
            $('.equipment_export').val(JSON.stringify(selectedrows));
            $(row_id).addClass('selected');
        }else
        {
            if(selectedrows.includes(id[1]))
            {
                selectedrows.splice( selectedrows.indexOf(id[1]),1);
                $('.equipment_export').val(JSON.stringify(selectedrows));
                $(row_id).removeClass('selected');
            }
        }

    });



     $('#selectedactivebuttonequipment2').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        var url='/equipment/restore/multiple';
        if (selectedrows && selectedrows.length) {
       // not empty
            swal({
                title: "Are you sure?",
                text: "Selected option will be active  again ..!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
             },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:url,
                        data: {'ids':selectedrows},
                        success: function(response)
                        {

                            toastr["success"](response.message);
                            $('#plant_and_equipment2_table').DataTable().clear().destroy();
                            var url='/equipment/getallSoft';
                            equipment2_data(url);
                            swal.close();

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                  swal("Cancelled", "Your Record is safe", "error");

                }
            });
         }
        else
        {
       // empty
            return toastr["error"]("Select at least one record to delete!");
        }
    });

    $('#deleteselectedequipment2').click(function(){
       var url='/equipment/delete/multiple';
       delete_mutilpleDataEquipment(url,selectedrows);
    });

    $('#deleteselectedequipment2Soft').click(function(){
       var url='/equipment/delete/soft/multiple';
       delete_mutilpleDataEquipment(url,selectedrows);
    });

    $('.add-equipment2').on('click', 'i', () => {
      $.ajax({
            type: 'GET',
            url: '/equipment/store',
            success: function(response)
            {
                var equipment_checkbox=$("#equipment_checkbox").val();
                var delete_equipment=$("#delete_equipment").val();
                var hard_delete_equipment=$("#hard_delete_equipment").val();
                var equipment_name=$("#equipment_name").val();

                var html='';
                html+='<tr id="equipment2_tr'+response.data.id+'" role="row" class="hide odd">';
                if(equipment_checkbox == 1){
                    html+='<td><div class="form-check"><input type="checkbox" class="form-check-input selectedrowequipment2" id="equipment2s'+response.data.id+'"><label class="form-check-label" for="equipment2s'+response.data.id+'"></label></div></td>';
                }

                if (equipment_name == 1) {
                    var n_edit = ($("#equipment_name_create").val()=='1') ? 'true':'false';
                    var name = '';
                    html += '<td class="pt-3-half edit_inline_equipment" onkeypress="editSelectedRow('+"equipment2_tr"+response.data.id+')" id="name2' +response.data.id+ '"   contenteditable="'+n_edit+'">' +name+ '</td>';
                }
                if (delete_equipment == 1){
                    html += '<td><a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light delete-btn' + response.data.id + '" onclick="deleteequipment2data(' + response.data.id + ')"><i class="fas fa-trash"></i></a> <a type="button" class="btn btn-primary btn-xs my-0 waves-effect all_action_btn_margin waves-light saveedit_inline_equipment equipment_data_show_button_'+response.data.id+' show_tick_btn'+response.data.id+'" style="display: none;" id="'+response.data.id   +'"><i class="fas fa-check"></i></a></td>';
                }
                html+='</tr>';
                $('#table-equipment2').find('table').prepend(html);
                var url='/plant_and_equipment/getall';
                equipment_data(url);
            },
            error: function (error) {
                console.log(error);
            }
        });


    });
    $('body').on('click','.edit_inline_equipment',function () {
        var id=$(this).attr('id');
        id=id.split('name');
        // $('.equipment_data_show_button_'+id[1]).show();
    });

    $('body').on('click', '.saveedit_inline_equipment', function() {
        var company_id=$('#companies_selected_nav').children("option:selected").val();
        var tr_id=$(this).attr('id');

    // $('body').on('focusout', '.saveedit_inline_equipment', function() {
    //     var company_id=$('#companies_selected_nav').children("option:selected").val();
    //     var tr_id=$(this).parent('tr').attr('id');
    //     tr_id=tr_id.split('equipment2_tr');
    //     tr_id=tr_id[1];
        console.log(tr_id);
        var data={
            'id':tr_id,
            'name':$('#name2'+tr_id).html(),
            'company_id':company_id,

        };
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url:'/equipment/update',
            data:data,
            success: function(response)
            {
                if(response.error)
                {
                    toastr["error"](response.error);
                    $("#equipment2_tr"+tr_id).attr('style','background-color:#ff3547 !important');
                }
                else
                {
                    toastr["success"](response.message);
                    $("#equipment2_tr"+tr_id).attr('style','background-color:#90ee90 !important');
                    // $('#contact_table').DataTable().clear().destroy();
                    // var url='/equipment/getall';
                    // equipment2_data(url);
                    // var url='/plant_and_equipment/getall';
                    // equipment_data(url);
                    save_and_delete_btn_toggle(tr_id);

                }

            }
        });

        // console.log(data);

    });

    $('#restorebuttonequipment2').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        selectedrows=[];
        $("#export_excel_equipment2_btn").hide();
        $("#export_world_equipment2_btn").hide();
        $("#export_pdf_equipment2_btn").hide();
        $('#restorebuttonequipment2').hide();
        $('#deleteselectedequipment2').hide();
        $('#selectedactivebuttonequipment2').show();
        $('#deleteselectedequipment2Soft').show();
        $('#export_excel_equipment2').val(2);
        $('#export_world_equipment2').val(2);
        $('#export_pdf_equipment2').val(2);
        $('.add-equipment2').hide();
        $('#activebuttonequipment2').show();
        $('#plant_and_equipment2_table').DataTable().clear().destroy();
        var url='/equipment/getallSoft';
        equipment2_data(url);
    });
    $('#activebuttonequipment2').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        selectedrows=[];
        $("#export_excel_equipment2_btn").show();
        $("#export_world_equipment2_btn").show();
        $("#export_pdf_equipment2_btn").show();
        $('#export_excel_equipment2').val(1);
        $('#export_world_equipment2').val(1);
        $('#export_pdf_equipment2').val(1);
        $('.add-equipment2').show();
        $('#selectedactivebuttonequipment2').hide();
        $('#deleteselectedequipment2').show();
        $('#deleteselectedequipment2Soft').hide();
        $('#restorebuttonequipment2').show();
        $('#activebuttonequipment2').hide();
        $('#plant_and_equipment2_table').DataTable().clear().destroy();
        var url='/equipment/getall';
        equipment2_data(url);
    });

});
// function restore_eqipment(url,selectedrows)
// {
//       if( $('.edit_cms_disable').css('display') == 'none' ) {
//             console.log('Editer mode On Please change your mode');
//             return 0;
//         }
//         if (selectedrows && selectedrows.length) {
//        // not empty
//             swal({
//                 title: "Are you sure?",
//                 text: "Selected option will be inactive and can be Active again by the admin only!",
//                 type: "warning",
//                 showCancelButton: true,
//                 confirmButtonColor: '#DD6B55',
//                 confirmButtonText: 'Yes, I am sure!',
//                 cancelButtonText: "No, cancel it!",
//                 closeOnConfirm: false,
//                 closeOnCancel: false
//              },
//             function(isConfirm){

//                 if (isConfirm){
//                     $.ajaxSetup({
//                         headers: {
//                             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//                         }
//                     });
//                     $.ajax({
//                         type: "POST",
//                         url:url,
//                         data: {'ids':selectedrows},
//                         success: function(response)
//                         {

//                             swal.close();
//                             toastr["success"](response.message);
//                             if(response.flag==1)
//                             {
//                                 var url='/equipment/getall';
//                             }
//                             else
//                             {
//                                 var url='/equipment/getallSoft';
//                             }
//                             equipment2_data(url);

//                         },
//                         error: function (error) {
//                             console.log(error);
//                         }
//                     });

//                 } else {
//                   swal("Cancelled", "Your Record is safe", "error");
//
//                 }
//             });
//          }
//         else
//         {
//        // empty
//             return toastr["error"]("Select at least one record to delete!");
//         }
//  }
// function restoreequipment2data(id)
//  {
//     function restore_eqipment(url,selectedrows)
//     restore_eqipment(url,selectedrows)
//  }
 function delete_mutilpleDataEquipment(url,selectedrows)
 {
     if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        if (selectedrows && selectedrows.length) {
       // not empty
            swal({
                title: "Are you sure?",
                text: "Selected option will be inactive and can be Active again by the admin only!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
             },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:url,
                        data: {'ids':selectedrows},
                        success: function(response)
                        {

                            swal.close();
                            toastr["success"](response.message);
                            $('#plant_and_equipment2_table').DataTable().clear().destroy();
                            if(response.flag==1)
                            {
                                var url='/equipment/getall';
                            }
                            else
                            {
                                var url='/equipment/getallSoft';
                            }
                            equipment2_data(url);

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                  swal("Cancelled", "Your Record is safe", "error");

                }
            });
         }
        else
        {
       // empty
            return toastr["error"]("Select at least one record to delete!");
        }
 }
 function restoreequipment2data(id)
{
    var url='/equipment/restore';
    data_equipment_operation(id,url);

}
 function data_equipment_operation(id,url)
 {
    swal({
        title: "Are you sure?",
        text: "Selected option with be Operate ",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
     },
     function(isConfirm){

       if (isConfirm){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
             $.ajax({
                        type: "POST",
                        url: url,
                        data: {'id':id},
                        success: function(response)
                        {
                            if(response.flag==1)
                            {
                                var url='/equipment/getall';
                            }
                            else
                            {
                                var url='/equipment/getallSoft';
                            }
                            $('#plant_and_equipment2_table').DataTable().clear().destroy();
                            $('#plant_and_equipment_table').DataTable().clear().destroy();
                            equipment2_data(url);
                            var url='/plant_and_equipment/getall';
                            equipment_data(url);

                            swal.close();
                            toastr["success"](response.message);
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
        } else {
          swal("Cancelled", "Your Record is safe", "error");

        }
     });
 }
function deletesoftequipment2data(id)
{
    var url='/equipment/softdelete';
    data_equipment_operation(id,url);

}
function deleteequipment2data(id)
 {
    var url='/equipment/delete';
    data_equipment_operation(id,url);
 }
