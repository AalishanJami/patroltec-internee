 $(document).ready(function() {
     $("body").on('change','#customer_id',function () {
         var division_id = $('#division_id').children("option:selected").val();
         if (division_id ==""){
             toastr["error"](['Please first add division']);
             $('#customer_id').val('');
         }else {
             $('#customer_id').children("option:selected").val();

            var division_id = $('#division_id').children("option:selected").val();
            var customer_id = $('#customer_id').children("option:selected").val();
            $.ajax({
                type: "GET",
                url: '/customer/getall/sitegroup',
                data: {
                    'division_id': division_id,
                    'customer_id':customer_id,
                },
                success: function(response)
                {
                    var html='';
                    html+='<select searchable=" "  class="mdb-select_sitegroup" name="site_group_id" id="site_group_id" >';

                    if(response.data.length<=0){
                        html+='<option value=" ">No Site Group Found ....</option>';
                    }
                    for (var i = response.data.length - 1; i >= 0; i--) {
                        // response.data[i]
                        html+='<option value="'+response.data[i].id+'">'+response.data[i].name+'</option>';
                    }
                     html+='</select>';
                    $('#site_groups_div').html(html);
                    $('.mdb-select_sitegroup').materialSelect();
                    $('select.mdb-select_sitegroup').hide();
                },
                error: function (error) {
                    console.log(error);
                }
            });
         }
     });

    $("body").on('change','#site_group_id',function () {
        var division_id = $('#division_id').children("option:selected").val();
        var customer_id = $('#customer_id').children("option:selected").val();
        if (division_id ==""){
            toastr["error"](['Please first add division ']);
            $('#site_group_id').val('');
        }else{
            $('#site_group_id').children("option:selected").val();
        }

        if (customer_id ==""){
            toastr["error"](['Please first add customer']);
            $('#site_group_id').val('');
        }else{
            $('#site_group_id').children("option:selected").val();
        }
    });

    var selectedrows=[];

     $('body').on('click','#customer_checkbox_all',function() {
         $('.selectedrowcustomer').not(this).prop('checked', this.checked);
         $('.selectedrowcustomer').each(function(key,value) {
             var id=$(this).attr('id');
             console.log(id);
             if(id)
             {
                 id=id.split('customer');
                 row_id='#customer_tr'+id[1];
                 if($(this).is(":checked"))
                 {
                     if(key == 0)
                     {
                         selectedrows=[];
                     }
                     if(id[1] != '_checkbox_all')
                     {
                         selectedrows.push(id[1]);
                     }
                     $('.customer_export').val(JSON.stringify(selectedrows));
                     $(row_id).addClass('selected');
                 }
                 else
                 {
                     $('.customer_export').val(1);
                     selectedrows=[];
                     $(row_id).removeClass('selected');
                 }
             }
         });
     });


     $('body').on('click', '.selectedrowcustomer', function() {
         var id=$(this).attr('id');
         id=id.split('customer');
         row_id='#customer_tr'+id[1];
         if($(this).is(":checked"))
         {
             selectedrows.push(id[1]);
             $('.customer_export').val(JSON.stringify(selectedrows));
             $(row_id).addClass('selected');
         }else
         {
             if(selectedrows.includes(id[1]))
             {
                 selectedrows.splice( selectedrows.indexOf(id[1]),1);
                 $('.customer_export').val(JSON.stringify(selectedrows));
                 $(row_id).removeClass('selected');
             }
         }
    });
    $('#selectedactivebuttoncustomer').click(function(){
        var url='/customer/active/multiple';
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        if (selectedrows && selectedrows.length) {
            swal({
                title: "Are you sure?",
                text: "Selected option will be active  again ..!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
             },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:url,
                        data: {'ids':selectedrows},
                        success: function(response)
                        {

                            toastr["success"](response.message);
                            var url='/customer/getallSoft';
                            $('#customer_table').DataTable().clear().destroy();
                            customer_data(url);
                            swal.close();

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                  swal("Cancelled", "Your Record is safe", "error");

                }
            });
         }
        else
        {
       // empty
            return toastr["error"]("Select at least one record to delete!");
        }
    });
    $('#deleteselectedcustomer').click(function(){
       var url='/customer/delete/multiple';
       delete_mutilpleData_customer(url,selectedrows);
    });
    $('#deleteselectedcustomerSoft').click(function(){
       var url='/customer/delete/soft/multiple';
       delete_mutilpleData_customer(url,selectedrows);
    });

    $('.add-customer').on('click', 'i', () => {
        var division_id = $('#division_id').children("option:selected").val();
        if (division_id ==""){
            toastr["error"](['Please first add division']);
            return 0;
        }else {
            $.ajax({
                type: 'GET',
                url: '/customer/store',
                success: function(response)
                {
                    var html='';
                    var n_create = ($("#customer_name_create").val()==1)? "true":"false";
                    var d_create = ($("#customer_division_create").val()==1)? "":"disabled";
                    var d_edit = ($("#customer_division_create").val()==1)? "true":"false";
                    html+= '<tr id="customer_tr'+response.data.id+'" role="row" class="hide odd">';
                    html+=' <td><div class="form-check"><input type="checkbox" class="form-check-input selectedrowcustomer" id="customer'+response.data.id+'"><label class="form-check-label" for="customer'+response.data.id+'"></label></div></td>';
                    html+='<td class="pt-3-half edit_inline_customer" onkeypress="editSelectedRow('+"customer_tr"+response.data.id+')" data-id="'+response.data.id+'" id="nameC'+response.data.id+'"   contenteditable="'+n_create+'"></td>';
                    html+='<td class="pt-3-half edit_inline_customer" data-id="'+response.data.id+'">';
                    html+='<select id="division_id'+response.data.id+'" onchange="editSelectedRow('+"customer_tr"+response.data.id+')"  style="background-color: inherit;border: 0px">';
                    for (var j = response.divisions.length - 1; j >= 0; j--) {
                        html+='<option value="'+response.divisions[j].id+'" '+d_create+' >'+response.divisions[j].name+'</option>';
                    }
                    html+='</select></td>';
                    html+='<td><a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light" onclick="deletecustomerdata('+response.data.id+')"><i class="fas fa-trash"></i></a><a type="button" style="display: none;" class="btn btn-primary all_action_btn_margin btn-xs my-0 waves-effect waves-light savecustomerdata customer_show_button_'+response.data.id+' delete-btn'+response.data.id+'" id="'+response.data.id+'"><i class="fas fa-check"></i></a></td>'
                    html+='</tr>';
                    $('#table_customer').find('table').prepend(html);

                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
    });
     $('body').on('click', '.savecustomerdata ', function() {
         var tr_id=$(this).attr('id');
         var division_id = $('#division_id'+tr_id).children("option:selected").val();
         var company_id=$('#companies_selected_nav').children("option:selected").val();
         if (division_id ==""){
             toastr["error"](['Please first add division']);
             return 0;
         }else{
            var data={
                'id':tr_id,
                'name':$('#nameC'+tr_id).html(),
                'division_id':division_id,
                'company_id':company_id,
            };
            $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
            type: "POST",
            url:'/customer/update',
            data:data,
            success: function(response)
            {
                if(response.error)
                {
                    toastr["error"](response.error);
                }
                else
                {
                    toastr["success"](response.message);
                    save_and_delete_btn_toggle(tr_id);
                    if(response.flag==1)
                    {
                        $("#customer_tr"+tr_id).attr('style','background-color:#90ee90 !important');
                    // var url='/customer/getall';
                    }
                    else
                    {
                        $("#customer_tr"+tr_id).attr('style','background-color:#ff3547 !important');
                    // var url='/customer/getallsoft';
                    }

                    // $('#customer_table').DataTable().clear().destroy();
                    // customer_data(url);

                }

            }

            });
         }
    });

    $('#restorebuttoncustomer').click(function(){

        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        $('#restorebuttoncustomer').hide();
        $('#deleteselectedcustomer').hide();
        $('#selectedactivebuttoncustomer').show();
        $('#deleteselectedcustomerSoft').show();
        $('#export_excel_customer').hide();
        $('#export_world_customer').hide();
        selectedrows=[];
        $('#export_pdf_customer').hide();
        $('.add-customer').hide();
        $('#activebuttoncustomer').show();
        var url='/customer/getallSoft';
        $('#customer_table').DataTable().clear().destroy();
        customer_data(url);
    });
    $('#activebuttoncustomer').click(function(){
        $('#export_excel_customer').show();
        $('#export_world_customer').show();
        $('#export_pdf_customer').show();
        $('.add-customer').show();
        $('#selectedactivebuttoncustomer').hide();
        $('#deleteselectedcustomer').show();
        selectedrows=[];
        $('#deleteselectedcustomerSoft').hide();
        $('#restorebuttoncustomer').show();
        $('#activebuttoncustomer').hide();
        var url='/customer/getall';
        $('#customer_table').DataTable().clear().destroy();
        customer_data(url);
    });



});
 function restorecustomerdata(id)
 {
    var url='/customer/active';
    swal({
        title: "Are you sure?",
        text: "Selected option with be active ",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
     },
     function(isConfirm){

       if (isConfirm){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
             $.ajax({
                        type: "POST",
                        url: url,
                        data: {'id':id},
                        success: function(response)
                        {
                            if(response.flag==1)
                            {
                                var url='/customer/getall';
                            }
                            else
                            {
                                var url='/customer/getallSoft';
                            }
                            $('#customer_table').DataTable().clear().destroy();
                            customer_data(url);
                            swal.close();
                            toastr["success"](response.message);
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
        } else {
          swal("Cancelled", "Your Record is safe", "error");

        }
     });
 }
 function delete_mutilpleData_customer(url,selectedrows)
 {
     if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        if (selectedrows && selectedrows.length) {
       // not empty
            swal({
                title: "Are you sure?",
                text: "Selected option will be inactive and can be Active again by the admin only!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
             },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:url,
                        data: {'ids':selectedrows},
                        success: function(response)
                        {

                            toastr["success"](response.message);
                            if(response.flag==1)
                            {
                                var url='/customer/getall';
                            }
                            else
                            {
                                var url='/customer/getallSoft';
                            }
                            $('#customer_table').DataTable().clear().destroy();
                            customer_data(url);
                            swal.close();

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                  swal("Cancelled", "Your Record is safe", "error");

                }
            });
         }
        else
        {
       // empty
            return toastr["error"]("Select at least one record to delete!");
        }
 }
 function delete_data_customer(id,url)
 {
    swal({
        title: "Are you sure?",
        text: "Selected option with be Deleted ",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
     },
     function(isConfirm){

       if (isConfirm){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
             $.ajax({
                        type: "POST",
                        url: url,
                        data: {'id':id},
                        success: function(response)
                        {
                            if(response.flag==1)
                            {
                                var url='/customer/getall';
                            }
                            else
                            {
                                var url='/customer/getallSoft';
                            }
                             $('#customer_table').DataTable().clear().destroy();
                    customer_data(url);
                            swal.close();
                            toastr["success"](response.message);
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
        } else {
          swal("Cancelled", "Your Record is safe", "error");

        }
     });
 }
function deletesoftcustomerdata(id)
{
    var url='/customer/softdelete';
    delete_data_customer(id,url);

}
function deletecustomerdata(id)
 {
    var url='/customer/delete';
    delete_data_customer(id,url);
 }
