 $(document).ready(function() {
    var selectedrows=[];
    toastr.options = {
      "closeButton": false,
      "debug": false,
      "newestOnTop": false,
      "progressBar": false,
      "positionClass": "toast-top-center",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": "5000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    };
    $('body').on('click', '.selectedrowpopulate', function() {
        var tr_id=$(this).attr('id');
        tr_id=tr_id.split('populate_tr');
        tr_id=tr_id[1];
        var flag=$('#populate'+tr_id).prop('checked');
        console.log(flag);
        if(!flag)
        {
            console.log(selectedrows);
            if(selectedrows.includes(tr_id))
            {

                selectedrows.splice( selectedrows.indexOf(tr_id), 1 );
                $('#populate_tr'+tr_id).removeClass('selected');

            }
            else
            {
                selectedrows.push(tr_id);
                $('#populate_tr'+tr_id).addClass('selected');

            }
        }
    });

    $('.add-populate').on('click', 'i', () => {

      $.ajax({
            type: 'GET',
            url: '/document/storePopulate',
            success: function(response)
            {
                var html='';
                html+= '<tr id="populate_tr'+response.data.id+'" class="selectedrowpopulate" role="row" class="hide odd">';
                html+=' <td><div class="form-check"><input type="checkbox" class="form-check-input" id="populates'+response.data.id+'"><label class="form-check-label" for="populates'+response.data.id+'"></label></div></td><td class="sorting_1"><a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light" onclick="deletepopulatedata('+response.data.id+')"><i class="fas fa-trash"></i></a></td>';
                html+='<td class="pt-3-half edit_inline_populate" id="nameD'+response.data.id+'"   contenteditable="true"></td>';
                html+='<td class="pt-3-half"><a type="button" class="btn btn-primary btn-xs my-0 waves-effect waves-light savepopulatedata " id="'+response.data.id+'"><i class="fas fa-check"></i></a></td>';
                html+='</tr>';
                $('#table-populate').find('table').prepend(html);

            },
            error: function (error) {
                console.log(error);
            }
        });


    });


  $('body').on('click', '.savepopulatedata', function() {
        //var company_id=$('#companies_selected_nav').children("option:selected").val();
        var tr_id=$(this).attr('id');
        // tr_id=tr_id.split('populate_tr');
        // tr_id=tr_id[1];
        console.log($('#nameD'+tr_id).html());
        var data={
            'id':tr_id,
            'name':$('#nameD'+tr_id).html(),

        };
        console.log(data);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url:'/document/updatePopulate',
            data:data,
            success: function(response)
            {
                if(response.error)
                {
                    toastr["error"](response.error);
                }
                else
                {
                    toastr["success"](response.message);
                    if(response.flag==1)
                    {
                        $("#populate_tr"+tr_id).attr('style','background-color:#90ee90 !important');
                        // var url='/populate/getall';
                        console.log(Object.keys(response.pre_populate).length );

                        var html='';

                        html+='<select class="mdb-select" name="pre_populate_id" id="pre_populate_id" >';

                        for (var key in response.pre_populate) {
                             if (response.pre_populate.hasOwnProperty(key))
                                html+='<option  value="'+key+'">'+response.pre_populate[key]+'</option>';
                        }

                        html+='</select> ';
                        $('#pre_populate_listing').empty();
                        console.log(html, "html");
                        document.getElementById('pre_populate_listing').innerHTML=html;
                        $('#pre_populate_id').materialSelect();

                    }
                    else
                    {
                        $("#populate_tr"+tr_id).attr('style','background-color:#ff3547 !important');
                        // var url='/populate/getallsoft';
                    }
                    // $('#populate_table').DataTable().clear().destroy();
                    // populate_data(url);

                }

            }

        });

        // console.log(data);

    });










  });
  function delete_data_populate(id,url)
 {
    swal({
        title: "Are you sure?",
        text: "Selected option with be Deleted ",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
     },
     function(isConfirm){

       if (isConfirm){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
             $.ajax({
                        type: "POST",
                        url: url,
                        data: {'id':id},
                        success: function(response)
                        {
                            if(response.flag==1)
                            {
                                var url='/document/getAllPopulate';
                            }
                            else
                            {
                                var url='/document/getallSoft';
                            }
                            $('#populate_table').DataTable().clear().destroy();
                            swal.close();
                            populate_data(url);

                            toastr["success"](response.message);
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
        } else {
          swal("Cancelled", "Your Record is safe", "error");

        }
     });
 }

function deletepopulatedata(id)
 {
    var url='/document/deletePopulate';
    delete_data_populate(id,url);
 }
