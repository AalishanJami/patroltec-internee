$(document).ready(function() {
    var selectedrows=[];

    $('body').on('click','#tasktype_checkbox_all',function() {
            $('input:checkbox').not(this).prop('checked', this.checked);
            $('input[type="checkbox"]').each(function(key,value) {
                var id=$(this).attr('id');
                console.log(id);
                if(id)
                {
                    id=id.split('tasktype');
                    row_id='#tasktype_tr'+id[1];
                    if($(this).is(":checked"))
                    {
                        if(key == 0)
                        {
                            selectedrows=[];
                        }
                        if(id[1] != '_checkbox_all')
                        {
                            selectedrows.push(id[1]);
                        }
                        $('.task_export').val(JSON.stringify(selectedrows));
                        $(row_id).addClass('selected');
                    }
                    else
                    {
                        $('.task_export').val(1);
                        selectedrows=[];
                        $(row_id).removeClass('selected');
                    }
                }
            });
    });

    $('body').on('click', '.selectedrowtasktype', function() {
        var id=$(this).attr('id');
        id=id.split('tasktype');
        row_id='#tasktype_tr'+id[1];
        if($(this).is(":checked"))
        {
            selectedrows.push(id[1]);
            $('.task_export').val(JSON.stringify(selectedrows));
            $(row_id).addClass('selected');
        }else
        {
            if(selectedrows.includes(id[1]))
            {
                selectedrows.splice( selectedrows.indexOf(id[1]),1);
                $('.task_export').val(JSON.stringify(selectedrows));
                $(row_id).removeClass('selected');
            }
        }
    });

    $('#selectedactivebuttontasktype').click(function(){
        var url='/tasktype/active/multiple';
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        if (selectedrows && selectedrows.length) {
            // not empty
            swal({
                    title: "Are you sure?",
                    text: "Selected option will be active  again ..!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Yes, I am sure!',
                    cancelButtonText: "No, cancel it!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm){

                    if (isConfirm){
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $.ajax({
                            type: "POST",
                            url:url,
                            data: {'ids':selectedrows},
                            success: function(response)
                            {

                                var url='/tasktype/getAllSoft';
                                tasktype_data(url);
                                swal.close();
                                toastr["success"](response.message);
                            },
                            error: function (error) {
                                console.log(error);
                            }
                        });

                    } else {
                        swal("Cancelled", "Your Record is safe", "error");

                    }
                });

        }
        else
        {
            // empty
            return toastr["error"]("Select at least one record to delete!");
        }
    });
    $('#deleteselectedtasktype').click(function(){
        if ($("#taskTypeflag").val()=='1'){
            var url='/tasktype/delete/multiple';
        }else{
            var url='/tasktype/delete/soft/multiple';
        }
        delete_mutilpleDatataskType(url,selectedrows);
    });
    $('#deleteselectedtasktypeSoft').click(function(){
        var url='/tasktype/delete/soft/multiple';
        delete_mutilpleDatataskType(url,selectedrows);
    });

    $('.add-tasktype').on('click', 'i', () => {
        console.log('sdssd');
        $.ajax({
            type: 'GET',
            url: '/tasktype/store',
            success: function(response)
            {
                var html='';
                html+= '<tr id="tasktype_tr'+response.data.id+'" class="" role="row" class="hide odd">';
                if ($("#documentTaskType_checkbox").val()=='1'){
                    html+=' <td>';
                    html+='<div class="form-check"><input type="checkbox" class="form-check-input selectedrowtasktype" id="tasktype'+response.data.id+'"><label class="form-check-label" for="tasktype'+response.data.id+'"></label></div>';
                    html+='</td>';
                }
                if ($("#documentTaskType_name").val()=='1'){
                    if ($("#documentTaskType_name_create").val()=='1'){
                        var editable='true';
                    }else{
                        var editable='false';
                    }
                    html+='<td class="pt-3-half edit_inline_tasktype" onkeypress="editSelectedRow('+"tasktype_tr"+response.data.id+')" data-id="'+response.data.id+'" id="nameD'+response.data.id+'"   contenteditable="'+editable+'"></td>';
                }
                html+='<td class="sorting_1">';
                if ($("#delete_documentTaskType").val()=='1'){
                    html+='<a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light delete-btn'+response.data.id+'" onclick="deletetasktypedata()"><i class="fas fa-trash"></i></a>';
                }
                html+='<a type="button" class="btn btn-primary btn-xs my-0 all_action_btn_margin waves-effect waves-light savetasktypedata show_tick_btn'+response.data.id+'" style="display: none;" id="'+response.data.id+'"><i class="fas fa-check"></i></a>';
                html+='</td>';
                html+='</tr>';
                $('#table-tasktype').find('table').prepend(html);

            },
            error: function (error) {
                console.log(error);
            }
        });


    });

    // $('body').on('click','.edit_inline_tasktype',function () {
    //     var id=$(this).attr('id');
    //     $('#'+id).focus();
    //     id=id.split('nameD');
    //     $('.tasktype_show_button_'+id[1]).show();
    // });
    //

    $('body').on('click', '.savetasktypedata', function() {
        var company_id=$('#companies_selected_nav').children("option:selected").val();
        var tr_id=$(this).attr('id');
        console.log("name data : "+$('#nameD'+tr_id).html());
        var data={
            'id':tr_id,
            'name':$('#nameD'+tr_id).html(),

        };
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url:'/tasktype/update',
            data:data,
            success: function(response)
            {
                if(response.error)
                {
                    toastr["error"](response.error);
                    $("#tasktype_tr"+tr_id).attr('style','background-color:#e3342f !important');
                }
                else
                {
                    toastr["success"](response.message);
                    if(response.flag==1)
                    {
                        $("#tasktype_tr"+tr_id).attr('style','background-color:#8eeb8e !important');

                        // var url='/tasktype/getall';
                    }
                    else
                    {
                        $("#tasktype_tr"+tr_id).attr('style','background-color:#e3342f !important');

                        // var url='/tasktype/getallsoft';
                    }

                    $(".delete-btn"+tr_id).show();
                    $(".show_tick_btn"+tr_id).hide();
                    // tasktype_data(url);

                }

            }

        });

        // console.log(data);

    });

    $('#restorebuttontasktype').click(function(){

        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        $('#restorebuttontasktype').hide();
        $('.task_export_from').hide();
        selectedrows=[];
        $('#selectedactivebuttontasktype').show();
        $('#deleteselectedtasktypeSoft').show();
        $('#export_excel_tasktype').val(2);
        $('#export_world_tasktype').val(2);
        $('#export_world_pdf').val(2);
        $('#taskTypeflag').val(0);
        $('.add-tasktype').hide();
        $('#activebuttontasktype').show();
        var url='/tasktype/getAllSoft';
        tasktype_data(url);
    });
    $('#activebuttontasktype').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        $('#export_excel_tasktype').val(1);
        $('#export_world_tasktype').val(1);
        $('#export_world_pdf').val(1);
        $('.add-tasktype').show();
        $('#taskTypeflag').val(1);
        selectedrows=[];
        $('.task_export_from').show();
        $('#selectedactivebuttontasktype').hide();
        $('#deleteselectedtasktypeSoft').hide();
        $('#restorebuttontasktype').show();
        $('#activebuttontasktype').hide();
        var url='/tasktype/getall';
        tasktype_data(url);
    });



});
function restoretasktypedata(id)
{
    var url='/tasktype/active';
    swal({
            title: "Are you sure?",
            text: "Selected option with be active ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){

            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {'id':id},
                    success: function(response)
                    {
                        var url='/tasktype/getAllSoft';
                        tasktype_data(url);
                        swal.close();
                        toastr["success"](response.message);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            } else {
                swal("Cancelled", "Your Record is safe", "error");
            }
        });
}
function delete_mutilpleDatataskType(url,selectedrows)
{
    if( $('.edit_cms_disable').css('display') == 'none' ) {
        console.log('Editer mode On Please change your mode');
        return 0;
    }
    if (selectedrows && selectedrows.length) {
        // not empty
        swal({
                title: "Are you sure?",
                text: "Selected option will be inactive and can be Active again by the admin only!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:url,
                        data: {'ids':selectedrows},
                        success: function(response)
                        {
                            toastr["success"](response.message);
                            if(response.flag==1)
                            {
                                var url='/tasktype/getall';
                            }
                            else
                            {
                                var url='/tasktype/getAllSoft';
                            }
                            $('#tasktype_table_model').DataTable().clear().destroy();
                            tasktype_data(url);
                            swal.close();

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                    swal("Cancelled", "Your Record is safe", "error");

                }
            });
    }
    else
    {
        // empty
        return toastr["error"]("Select at least one record to delete!");
    }
}
function delete_data_taskType(id,url)
{
    swal({
            title: "Are you sure?",
            text: "Selected option with be Deleted ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){

            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {'id':id},
                    success: function(response)
                    {
                        if(response.flag==1)
                        {
                            var url='/tasktype/getall';
                        }
                        else
                        {
                            var url='/tasktype/getAllSoft';
                        }
                        $('#tasktype_table_model').DataTable().clear().destroy();
                        tasktype_data(url);
                        swal.close();
                        toastr["success"](response.message);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            } else {
                swal("Cancelled", "Your Record is safe", "error");

            }
        });
}
function deletesofttasktypedata(id)
{
    var url='/tasktype/softdelete';
    delete_data_taskType(id,url);

}
function deletetasktypedata(id)
{
    var url='/tasktype/delete';
    delete_data_taskType(id,url);
}
