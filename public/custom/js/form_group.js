 $(document).ready(function() {
    var selectedrows=[];
    $('body').on('click', '.edit_inline_form_group', function() {

        $('.selectedrowform_group').removeClass('selected');
        //$('.selectedrowform_group').removeClass('purple');
        if (!$(".selectedrowform_group").hasClass('purple')){
            $('.selectedrowform_group').addClass('form_group_background');
        }

        $('.selectedrowform_group').attr('style','');
        var tr_id=$(this).attr('id');
        tr_id=tr_id.split('folder_name');
        tr_id=tr_id[1];
        setTimeout(function (){
            $('#form_group'+tr_id).addClass('selected');
        },5);
        var flag=$('#form_group'+tr_id).prop('checked');
        if(!flag)
        {
            if(selectedrows.includes(tr_id))
            {
                selectedrows.splice( selectedrows.indexOf(tr_id), 1 );
                $('#form_group'+tr_id).attr('checked', false );
                $('#form_group'+tr_id).removeClass('selected');

            }
            else
            {
                selectedrows.push(tr_id);
                $('#form_group'+tr_id).attr('checked', true );
                $('#form_group'+tr_id).addClass('selected');

            }
        }
    });
    $('#selectedactivebuttonform_group').click(function(){
        var url='/form_group/active/multiple';
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        if (selectedrows && selectedrows.length) {
       // not empty
            swal({
                title: "Are you sure?",
                text: "Selected option will be active  again ..!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
             },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:url,
                        data: {'ids':selectedrows},
                        success: function(response)
                        {

                            toastr["success"](response.message);
                            var url='/form_group/getallSoft';
                            $('.form_group_table_static').DataTable().clear().destroy();
                            form_group_data(url);
                            swal.close();

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                  swal("Cancelled", "Your Record is safe", "error");

                }
            });
         }
        else
        {
       // empty
            return toastr["error"]("Select at least one record to delete!");
        }
    });
    $('#deleteselectedform_group').click(function(){
       var url='/form_group/delete/multiple';
       delete_mutilpleDataform_group(url,selectedrows);
    });
    $('#deleteselectedform_groupSoft').click(function(){
       var url='/form_group/delete/soft/multiple';
       delete_mutilpleDataform_group(url,selectedrows);
    });

    $('.add-form_group').on('click', 'i', () => {
      $.ajax({
            type: 'GET',
            url: '/form_group/store',
            success: function(response)
            {
                // console.log(response.form_id);
                // if(response.form_id)
                // {
                //     var url='/form_group/get/' + response.form_id;
                // }
                // else
                // {
                //     var url='/form_group/getall';
                // }

                // $('#document_folder').DataTable().clear().destroy();
                // form_group_data(url);

                var html='';
                html+= '<tr id="form_group'+response.data.id+'" class="folder_class selected">';
                if ($("#documentfolder_name_create").val()==1){
                    var contenteditable='true';
                }else{
                    var contenteditable='false';
                }
                html+= '<td class="pt-3-half  edit_inline_form_group text-left white_color" contenteditable="'+contenteditable+'" id="folder_name'+response.data.id+'"></td>';
                html+= '<td class="text-nowrap" id="'+response.data.id+'" style="width: 1.5%;text-align: center;">';
                html+='<div class="btn-group dropleft">';
                    html+='<button type="button" class="btn btn-primary dropdown-toggle px-3"';
                    html+='data-toggle="dropdown" aria-haspopup="true"';
                    html+='aria-expanded="false">';
                    html+='</button>';
                    html+='<div class="dropdown-menu dropdown_menu_width">';
                        html+='<a class="dropdown-item copy_paste" data-key="cut" data-value="'+response.data.id+'" >Cut</a>';
                        html+='<a class="dropdown-item enable_paste copy_paste disable_href" data-key="paste"  data-value="'+response.data.id+'" >Paste</a>';
                        console.log(response.form_id);
                        if(response.form_id)
                        {
                            html+=response.parent_list;
                            // foreach ($parent_list as $parent_key => $parent_name) {
                            //     if(response.form_id !=$parent_key)
                            //     {
                            //         html+='<a class="dropdown-item enable_paste_parent" data-key="'.$parent_key.'"  data-value="'+response.data.id+'" >'.$parent_name.'</a>';
                            //     }
                            // }
                        }
                    html+='</div>';
                html+='</div>';
                html+= '<span><a  href="/form_group/folder/'+response.data.id+'" type="button" class=" btn btn-outline-light btn-sm my-0" style="background-color: #fff"><i class="far fa-eye"></i></a></span>';
                if ($("#delete_documentfolder").val()=='1'){
                    html+= '<a class="btn btn-outline-light btn-sm my-0 form_groupdatadelete'+response.data.id+'" onclick="viewform_groupdatadelete('+response.data.id+')" style="background-color: red !important;border:2px solid red !important;margin-left: 2% !important;"><i class="fa fa-trash"></i></a>';
                }
                html+='<a type="button" class="btn btn-primary btn-xs all_action_btn_margin waves-effect waves-light btn-sm my-0 saveform_groupdata form_groupdata_show_button_'+response.data.id+'" style="display: none;" id="'+response.data.id+'"><i class="fas fa-check"></i></a>';
                html+= '</td>';
                html+='</tr>';
                $('#table-form_group').find('table').prepend(html);
            },
            error: function (error) {
                console.log(error);
            }
        });
    });
    $('body').on('click','.edit_inline_form_group',function () {
        var id=$(this).attr('id');
        $('#'+id).focus();
    });
    $('body').on('keyup','.edit_inline_form_group',function () {
        var id=$(this).attr('id');
        id=id.split('folder_name');
        $('.form_groupdata_show_button_'+id[1]).show();
        $('.form_groupdatadelete'+id[1]).hide();
    });
    $('body').on('click', '.saveform_groupdata', function() {
        var company_id=$('#companies_selected_nav').children("option:selected").val();
        var tr_id=$(this).attr('id');
        var folder_name='';
        if($('#folder_name'+tr_id+" span" ).html())
        {
            folder_name=$('#folder_name'+tr_id).html();
        } else {
            folder_name=$('#folder_name'+tr_id).html();
        }
        var data={
            'id':tr_id,
            'name':folder_name,
            'company_id':company_id
        };
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url:'/form_group/update',
            data:data,
            success: function(response)
            {
                if(response.error)
                {
                    toastr["error"](response.error);
                    $("#form_group"+tr_id).attr('style','background-color:#ff3547 !important');
                }
                else
                {
                    toastr["success"](response.message);
                    if(response.form_id)
                    {
                        // var url='/form_group/get/' + response.form_id;
                        $("#form_group"+tr_id).attr('style','background-color:#90ee90 !important');
                    }
                    else
                    {
                        $("#form_group"+tr_id).attr('style','background-color:#90ee90 !important');
                        // var url='/form_group/getall';
                    }
                    $('.form_groupdata_show_button_'+tr_id).hide();
                    $('.form_groupdatadelete'+tr_id).show();
                    // console.log(url);
                    // $('#document_folder').DataTable().clear().destroy();
                    // form_group_data(url);

                }

            }

        });

        // console.log(data);

    });

    $('#restorebuttonform_group').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        $('#restorebuttonform_group').hide();
        $('#deleteselectedform_group').hide();
        $('#selectedactivebuttonform_group').show();
        $('#deleteselectedform_groupSoft').show();
        $('#export_excel_form_group').val(2);
        $('#export_world_form_group').val(2);
        $('#export_world_pdf').val(2);
        $('.add-bonus').hide();
        $('#activebuttonform_group').show();
        var url='/form_group/getallSoft';
        $('.form_group_table_static').DataTable().clear().destroy();
        form_group_data(url);
    });
    $('#activebuttonform_group').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        $('#export_excel_form_group').val(1);
        $('#export_world_form_group').val(1);
        $('#export_world_pdf').val(1);
        $('.add-bonus').show();
        $('#selectedactivebuttonform_group').hide();
        $('#deleteselectedform_group').show();
        $('#deleteselectedform_groupSoft').hide();
        $('#restorebuttonform_group').show();
        $('#activebuttonform_group').hide();
        var url='/form_group/getall';
        $('.form_group_table_static').DataTable().clear().destroy();
        form_group_data(url);
    });



});
 function restoreform_groupdata(id)
 {
    var url='/form_group/active';
    swal({
        title: "Are you sure?",
        text: "Selected option with be active ",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
     },
     function(isConfirm){

       if (isConfirm){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: url,
                data: {'id':id},
                success: function(response)
                {
                    if(response.flag==1)
                    {
                        var url='/form_group/getall';
                    }
                    else
                    {
                        var url='/form_group/getallSoft';
                    }
                    $('.form_group_table_static').DataTable().clear().destroy();
                    form_group_data(url);
                    swal.close();
                    toastr["success"](response.message);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        } else {
          swal("Cancelled", "Your Record is safe", "error");

        }
     });
 }
 function delete_mutilpleDataform_group(url,selectedrows)
 {
     if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        if (selectedrows && selectedrows.length) {
       // not empty
            swal({
                title: "Are you sure?",
                text: "Selected option will be inactive and can be Active again by the admin only!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
             },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:url,
                        data: {'ids':selectedrows},
                        success: function(response)
                        {

                            toastr["success"](response.message);
                            if(response.flag==1)
                            {
                                var url='/form_group/getall';
                            }
                            else
                            {
                                var url='/form_group/getallSoft';
                            }
                            $('.form_group_table_static').DataTable().clear().destroy();
                            form_group_data(url);
                            swal.close();

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                  swal("Cancelled", "Your Record is safe", "error");

                }
            });
         }
        else
        {
       // empty
            return toastr["error"]("Select at least one record to delete!");
        }
 }
 function delete_data_form_group(id,url)
 {
    swal({
        title: "Are you sure?",
        text: "Selected option with be Deleted ",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
     },
     function(isConfirm){

       if (isConfirm){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
             $.ajax({
                        type: "POST",
                        url: url,
                        data: {'id':id},
                        success: function(response)
                        {
                            if(response.flag==1)
                            {
                                var url='/form_group/getall';
                            }
                            else
                            {
                                var url='/form_group/getallSoft';
                            }
                            $('.form_group_table_static').DataTable().clear().destroy();
                            form_group_data(url);
                            swal.close();
                            toastr["success"](response.message);
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
        } else {
          swal("Cancelled", "Your Record is safe", "error");

        }
     });
 }
function deletesoftform_groupdata(id)
{
    selectedrows=[];
    var url='/form_group/softdelete';
    delete_data_form_group(id,url);

}
function deleteform_groupdata(id)
 {
    selectedrows=[];
    console.log(selectedrows);
    var url='/form_group/delete';
    delete_data_form_group(id,url);
 }
