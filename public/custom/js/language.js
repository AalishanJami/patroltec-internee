$(document).ready(function() {
   $(function () {
        var table = $('#usertable').dataTable({
            processing: true,
            ajax: '/language/index',
            "createdRow": function (row, data, dataIndex) {
                $(row).attr('id', data['id']);
                var temp = data['id'];
                $(row).attr('onclick', "selectedrow(this.id)");
            },
            columns: [{
                    data: 'checkbox',
                    name: 'checkbox'
                },
                {
                    data: 'language',
                    name: 'language'
                },
                {
                    data: 'short_lan',
                    name: 'short_lan'
                },
                {
                    data: 'actions',
                    name: 'actions'
                },

            ],
            columnDefs: [{
                orderable: false,
                className: 'select-checkbox',
                targets: 0
            }],
            select: {
                style: 'os',
                selector: 'td:first-child'
            },

        });

    });
 });


     



