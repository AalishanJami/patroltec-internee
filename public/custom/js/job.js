var ref =null;
var selectedrows=[];
var ms =null;
var percent =null;
var ispaused =true;
function saveOv()
{
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: '/job/responseForm',
        data:new FormData(document.getElementById("ov_form")),
        // dataType:'JSON',
        contentType:false,
        cache:false,
        processData:false,
        success: function(data)
        {
            toastr["success"](data.message);

        },
        error: function (error) {
            console.log(error);
        }
    });
}
save = async(complete) =>
{


    console.log(ids);
    //toastr["info"]("please be patient form is saving");
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": 0,
        "hideDuration": 0,
        "timeOut": 0,
        "extendedTimeOut": 0,
        "positionClass": "md-toast-top-center",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
    toastr["info"]('Please wait form is saving  <div class="spinner-border  text-warning" role="status"><span class="sr-only">Loading...</span></div>');
    var st = [];
    var x=null;

    for (var i = ids.length - 1; i >= 0;i--) {
        x = response(ids[i],complete);
        st.push(x);
    }
    setTimeout(function(){toastr.clear();toastr["success"](ms,{timeOut: 3000});},3000);
}

function tst(i,size)
{
    var p = parseInt((size+1/i+1)*100);
}


function response(id,complete)
{
    ispaused =false;
    var form = "jobForm_"+id;
    var formData = $("#jobForm_"+id).serializeObject();
    $("#complete_"+id).val(complete);
    var x = document.getElementById(form);
    if(x!=null)
    {


        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            url: '/job/response',
            data:new FormData(document.getElementById(form)),
            // dataType:'JSON',
            contentType:false,
            cache:false,
            processData:false,
            success: function(data)
            {

                ms=data.message;
                if(complete)
                {
                    console.log(complete);

                    $('#complete_btn').hide();
                    $('#link_view').show();

                }

                return 1;
            },
            error: function (error) {
                console.log(error);

                return 0;
            }
        });
    }
}


function deleteJob(id,status)
{

    if(status == 0)
    {
        swal({
                title:'Are you sure?',
                text: "Delete Record.",
                type: "error",
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, Delete!",
                cancelButtonText: "Cancel!",
                closeOnConfirm: false,
                customClass: "confirm_class",
                closeOnCancel: false,
            },
            function(isConfirm){
                console.log( isConfirm);
                if( $('.edit_cms_disable').css('display') != 'none' ) {
                    if (isConfirm) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $.ajax({
                            type : 'post',
                            url : '/job/delete',
                            data:{'id':id},
                            success:function(data)
                            {

                                setTimeout(function () {
                                    swal({
                                            title: "",
                                            text: "Delete Sucessfully!",
                                            type: "success",
                                            confirmButtonText: "OK"
                                        },

                                        function(isConfirm){

                                            if (isConfirm) {
                                                $('#job_table').DataTable().clear().destroy();
                                                var url='/job/getall';
                                                job_data(url);
                                            }
                                        }); }, 500);


                            }
                        });

                    }
                    else
                    {
                        swal.close();
                    }
                }
            });
    }
    else if (status==1)
    {
        $('#deleteModal').modal('show');
        $('#job_group_id').val(id);

    }

}


$(document).ready(function() {
    $('#link_view').hide();
    $('body').on('click', '#job_checkbox_all', function () {
        $('input:checkbox').not(this).prop('checked', this.checked);
        $('input[type="checkbox"]').each(function(key,value) {
            var id=$(this).attr('id');
            console.log(id);
            if(id)
            {
                id=id.split('job');
                row_id='#job_tr'+id[1];
                if($(this).is(":checked"))
                {
                    if(key == 0)
                    {
                        selectedrows=[];
                    }
                    if(id[1] != '_checkbox_all')
                    {
                        selectedrows.push(id[1]);
                    }
                    $('.job_export').val(JSON.stringify(selectedrows));
                    $(row_id).addClass('selected');
                }
                else
                {
                    $('.job_export').val(1);
                    selectedrows=[];
                    $(row_id).removeClass('selected');
                }
            }
        });
    });

    $('body').on('click', '.selectedrowjob_checked', function() {
        var id=$(this).attr('id');
        id=id.split('job');
        row_id='#job_tr'+id[1];
        if($(this).is(":checked"))
        {
            selectedrows.push(id[1]);
            $('.job_export').val(JSON.stringify(selectedrows));
            $(row_id).addClass('selected');
        }else
        {
            if(selectedrows.includes(id[1]))
            {
                selectedrows.splice( selectedrows.indexOf(id[1]),1);
                $('.job_export').val(JSON.stringify(selectedrows));
                $(row_id).removeClass('selected');
            }
        }
    });


    $('#deleteSelectedJob').click(function() {
        console.log("Selected Rows : "+selectedrows);
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        if (selectedrows && selectedrows.length)
        {
            swal({
                    title: "Are you sure?",
                    text: "Locations will be inactive and can be Active again by the admin only!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Yes, I am sure!',
                    cancelButtonText: "No, cancel it!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm){

                    if (isConfirm){
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $.ajax({
                            type: "POST",
                            url:'/job/multipleDelete',
                            data: {'ids':selectedrows},
                            success: function(response)
                            {

                                // $('#job_table').DataTable().clear().destroy();
                                /*  if($('#flagjob').val()==1)
                                  {
                                      var url='/project/getAll';
                                  }
                                  else
                                  {
                                      var url='/project/getAllSoft';
                                  }*/
                                var url='/job/getall/0';
                                job_data(url);
                                swal.close()
                                toastr["success"](response.message);

                            },
                            error: function (error) {
                                console.log(error);
                            }
                        });

                    } else {
                        swal("Cancelled", "Your Record is safe", "error");
                        e.preventDefault();
                    }
                });
        }
        else
        {
            return toastr["error"]("Select at least one record to delete!");
        }
    });
    try
    {
        var interval = setInterval(job_data(url), 300000);
    }catch(e)
    {
        console.log(e);
    }
    var today = new Date();
    var time = today.getHours() + ":" + today.getMinutes();
    $('#input_starttime').val(time);

    $( '#form_type' ).change(function() {
        form_type=$('#form_type').children("option:selected").val();
        console.log(form_type);
        $.ajax({
            type: "GET",
            url: '/job/getForm/'+form_type,
            success: function(response)
            {

                console.log(response.form);
                var html='';
                html+='<span>Form</span>'
                html+='<select name="form_id" class="mdb-select mdb-select-custom-form" id="form_id" required placeholder="please select">';
                html+='<option value="null" selected disabled>Please Select</option>';

                for (var i = response.form.length - 1; i >= 0; i--) {

                    html+='<option value="'+response.form[i].id+'">'+response.form[i].name+'</option>';
                }

                html+='</select> ';

                $('#form_div').empty();
                console.log(html, "html");
                $('#form_div').html(html);
                $('#form_id').materialSelect();
                $('select.mdb-select-custom-form').hide();

            },
            error: function (error) {
                console.log(error);
            }
        });
    });


    $( '#form_type2' ).change(function() {
        form_type=$('#form_type2').children("option:selected").val();
        console.log(form_type);
        $.ajax({
            type: "GET",
            url: '/job/getForm/'+form_type,
            success: function(response)
            {

                console.log(response.form);
                var html='';
                html+='<span>Form</span>'
                html+='<select name="form_id2" class="mdb-select mdb-select-custom-form" id="form_id2" required placeholder="please select">';
                html+='<option value="0" disabled>please Select</option>';

                for (var i = response.form.length - 1; i >= 0; i--) {

                    html+='<option value="'+response.form[i].id+'">'+response.form[i].name+'</option>';
                }

                html+='</select> ';

                $('#form_div2').empty();
                console.log(html, "html");
                $('#form_div2').html(html);
                $('#form_id2').materialSelect();
                $('select.mdb-select-custom-form').hide();

            },
            error: function (error) {
                console.log(error);
            }
        });
    });

    $( '#location_id' ).change(function() {
        location_id=$('#location_id').children("option:selected").val();
        console.log(location_id);
        $.ajax({
            type: "GET",
            url: '/job/getContact/'+location_id,
            success: function(response)
            {

                console.log(response.contact);
                var html='';
                html+='<span>contact</span>'
                html+='<select name="notifiable[]" class="mdb-select mdb-select-custom-contact" id="notifiable" multiple placeholder="please select">';
                html+='<option value="" disabled>please Select</option>';

                for (var i = response.contact.length - 1; i >= 0; i--) {

                    html+='<option value="'+response.contact[i].id+'">'+response.contact[i].first_name+'</option>';
                }

                html+='</select> <small id="er_notifiable" class="text-danger error_message"></small>';

                $('#contact_div').empty();
                console.log(html, "html");
                $('#contact_div').html(html);
                $('#notifiable').materialSelect();
                $('select.mdb-select-custom-contact').hide();

            },
            error: function (error) {
                console.log(error);
            }
        });

        $.ajax({
            type: "GET",
            url: 'job/location/getall/'+($('#location_id').children("option:selected").val()),
            success: function(response)
            {
                var html='';
                html+='<select name="location_breakdown_name" class="mdb-select mdb-select-custom-form" id="location_break_id" required placeholder="please select">';
                html+='<option value="" selected disabled>please Select</option>';
                console.log(response);
                for (var i = response.break.length - 1; i >= 0; i--) {
                    var selected='';

                    html+='<option '+selected+' value="'+response.break[i].id+'">'+response.break[i].name+'</option>';
                }
                html+='</select> <small id="er_location_break_id" class="text-danger error_message"></small>';
                $('#location_break_div').empty('');
                $('#location_break_div').html(html);
                console.log(html, "html");
                $('#location_break_div').html(html);
                $('#location_break_id').materialSelect();
                $('select.mdb-select-custom-form').hide();
                $('#modallocationsIndex').modal('hide');

            },
            error: function (error) {
                console.log(error);
            }
        });

        var url='job/location/getall/'+($('#location_id').children("option:selected").val());
        $('#location_table').DataTable().clear().destroy();
        location_data(url);








    });

//...................

    $('body').on('change', '#form_id', function() {
        form_id=$('#form_id').children("option:selected").val();
        console.log(form_id);
        $.ajax({
            type: "GET",
            url: '/job/getPopulate/'+form_id,
            success: function(response)
            {
                $('#version_id').val(response.version);
                console.log(response.populate);
                var html='';
                html+='<span>Populate</span>'
                html+='<select name="pre_populate_id" class="mdb-select mdb-select-custom-populate" id="pre_populate_id">';
                html+='<option value="null" selected disabled>Please Select</option>';

                for (var i = response.populate.length - 1; i >= 0; i--) {

                    html+='<option value="'+response.populate[i].id+'">'+response.populate[i].name+'</option>';
                }

                html+='</select> ';

                $('#populate_div').empty();
                console.log(html, "html");
                $('#populate_div').html(html);
                $('#pre_populate_id').materialSelect();
                $('select.mdb-select-custom-populate').hide();
                $('#populate_div').show();

            },
            error: function (error) {
                console.log(error);
            }
        });
    });
    $('#populate_check').change(function(){
        if(this.checked)
            $('#populate_div').show();
        else
            $('#populate_div').hide();

    });
    $("body").on('click','.modalJob',function(){
        $("#job_store_submit").trigger('reset');
        $("input.select-dropdown").attr('style','');
        $("input").attr('style','');
        $('.error_message').html('');

        $("#bulk_upload_img_loader").hide();
        $(".job_store_submit_button").attr('disabled',false);

        $("#note_value").val('');
        $(".note-editable").html('');
        $(".file-upload-preview").hide();
        $(".file-upload-wrapper .card.card-body.has-preview .btn.btn-sm.btn-danger").hide();
        $("#input-file-now").val('');
        $("#form_id").val('');
        $("#pre_populate_id").val('');
        var options = {year: 'numeric', month: 'long', day: 'numeric' };
        var start_date_format  = new Date();
        var start_date=start_date_format.toLocaleDateString("en-US", options);
        // alert(start_date);
        $('.start_date_Edit').val(start_date);

        $('.datepicker').pickadate({selectYears:250,});
        var today = new Date();
        var time = today.getHours() + ":" + today.getMinutes();
        $('#input_starttime').val(time);

        $('select.location_id_select option:first').attr('disabled', true);
        $('select.location_id_select option:first').attr('selected', true);

        $('select.user_id_select option:first').attr('disabled', true);
        $('select.user_id_select option:first').attr('selected', true);

        $('select.location_break_id option:first').attr('disabled', true);
        $('select.location_break_id option:first').attr('selected', true);

        $('select.notifiable_select option:first').attr('disabled', true);
        $('select.notifiable_select option:first').attr('selected', true);

        $('select.form_type_id_select option:first').attr('disabled', true);
        $('select.form_type_id_select option:first').attr('selected', true);

        $('select.form_id_select option:first').attr('disabled', true);
        $('select.form_id_select option:first').attr('selected', true);

        $('select.pre_populate_id_select option:first').attr('disabled', true);
        $('select.pre_populate_id_select option:first').attr('selected', true);

        $('select#pre_time_select option:first').attr('selected', true);
    });
    $('body').on('submit', '#job_store_submit', function(event) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        event.preventDefault();
        var data=$(this).serializeArray();
        console.log(data);
        $('#bulk_upload_img_loader').show();
        $('.job_store_submit_button').attr('disabled',true);
        $.ajax({
            type: 'POST',
            url: '/job/store',
            data:new FormData(document.getElementById('job_store_submit')),
            contentType:false,
            cache:false,
            processData:false,
            success: function(response)
            {
                if (response.status =='validation'){
                    $('#bulk_upload_img_loader').hide();
                    $('.job_store_submit_button').attr('disabled',false);
                    // console.log("Errors : "+JSON.stringify(response.error));
                    // $('#bulk_upload_img_loader').hide();
                    $('#er_project_name').html(response.error.location_id);
                    if(response.error.location_id)
                    {
                        $("[data-activates=select-options-location_id]").attr('style','border-bottom:1px solid #e3342f');
                    }
                    $("#er_location_break_id").html(response.error.location_breakdown_name);

                    if(response.error.location_breakdown_name)
                    {
                        $("[data-activates=select-options-location_break_id]").attr('style','border-bottom:1px solid #e3342f');
                    }

                    $("#er_user_id").html(response.error.user_id);
                    if(response.error.user_id)
                    {
                        $("[data-activates=select-options-user_id]").attr('style','border-bottom:1px solid #e3342f');
                    }
                    $("#er_notifiable").html(response.error.notifiable);
                    if(response.error.notifiable)
                    {
                        $("[data-activates=select-options-notifiable]").attr('style','border-bottom:1px solid #e3342f');
                    }
                    $("#er_form_type").html(response.error.form_type_id);
                    if(response.error.form_type_id)
                    {
                        $("[data-activates=select-options-form_type]").attr('style','border-bottom:1px solid #e3342f');
                    }
                    $("#er_form_id").html(response.error.form_id);
                    if(response.error.form_id)
                    {
                        $("[data-activates=select-options-form_id]").attr('style','border-bottom:1px solid #e3342f');
                    }
                    $("#er_date").html(response.error.date);
                    if(response.error.date)
                    {
                        $("#date_pick").attr('style','border-bottom:1px solid #e3342f');
                    }
                    $("#er_time").html(response.error.time);
                    if(response.error.time)
                    {
                        $("#input_starttime").attr('style','border-bottom:1px solid #e3342f');
                    }

                    return 0;
                }

                if(response.error_ms)
                {
                    $("#bulk_upload_img_loader").hide();
                    $(".job_store_submit_button").attr('disabled',false);
                    toastr["error"](response.error_ms);
                }
                if(response.message)
                {
                    toastr["success"](response.message);
                }
                $("#bulk_upload_img_loader").hide();
                $(".job_store_submit_button").attr('disabled',false);
                console.log(response);
                $('.error_message').html('');
                var url='/job/getall/0';
                job_data(url);
                $("#bulk_upload_img_loader").hide();
                $('#modalJob').modal('hide');
            },
            error: function (error) {
                console.log(error);
            }
        });

    });

});

function closeRadio(id)
{
    to = 'c_answer'+id;
    try{

        document.getElementById(to).value = '';
    }catch(e)
    {
        console.log(e);

    }
    console.log(ref);
    $(ref).prop("checked",false);
}

function fillComment(id)
{

    to = 'c_answer'+id;
    from = 'comment'+id;
    try{
        var val = document.getElementById(from).value;
        console.log(val);
        document.getElementById(to).value = val;
    }catch(e)
    {
        console.log(e);

    }
    var location_id=$('#location_id').children("option:selected").val();
    var user_id=$('#user_id2').val();
    var form_type=$('#form_type2').val();
    var form_id=$('#form_id2').val();
    var cmnt = "#comment"+id;
    var comment = document.getElementById(from).value;
    var date = $('#date_pick').val();
    var time = $('#input_starttime').val();
    var fl = true;
    if(comment=='' || comment==null )
    {
        console.log(comment,"comment");
        document.getElementById(from).style.borderBottom="1px solid #e3342f";
        $("#comment"+id).attr('style','border-bottom:1px solid #e3342f');
        $("#er_comment").html('Field required');
        fl= false;
    }else
    {
        $("#comment"+id).attr('style','border-bottom:1px solid #ced4da');
        $("#er_comment").html('');
    }
    if($("input[name='toggleDiv']:checked").val()==1)
    {

        if(user_id=='' || user_id==null )
        {
            console.log(user_id,"user");

            $("[data-activates=select-options-user_id2]").attr('style','border-bottom:1px solid #e3342f');

            $("#er_user_id").html('Field required');
            fl= false;
        }else
        {
            $("[data-activates=select-options-form_type2]").attr('style','border-bottom:1px solid #ced4da');
            $("#er_form_type").html('');
        }

        if(form_type=='' || form_type==null )
        {
            console.log(form_type,"user");

            $("[data-activates=select-options-form_type2]").attr('style','border-bottom:1px solid #e3342f');

            $("#er_form_type").html('Field required');
            fl= false;
        }else
        {
            $("[data-activates=select-options-form_type2]").attr('style','border-bottom:1px solid #ced4da');
            $("#er_form_type").html('');
        }


        if(form_id=='' || form_id==null )
        {
            console.log(form_id,"user");

            $("[data-activates=select-options-form_id2]").attr('style','border-bottom:1px solid #e3342f');

            $("#er_form_id").html('Field required');
            fl= false;
        }else
        {
            $("[data-activates=select-options-form_id2]").attr('style','border-bottom:1px solid #ced4da');
            $("#er_form_id").html('');
        }

        if(date=='' || date==null )
        {
            console.log(date,"date");

            $("#date_pick").attr('style','border-bottom:1px solid #e3342f');

            $("#er_date").html('Field required');
            fl= false;
        }
        else
        {
            $("#date_pick").attr('style','border-bottom:1px solid #ced4da');
            $("#er_user_id").html('');
        }



        if(time=='' || time==null )
        {
            console.log(time,"time");

            $("#input_starttime").attr('style','border-bottom:1px solid #e3342f');

            $("#er_time").html('Field required');
            fl= false;
        }
        else
        {
            $("#input_starttime").attr('style','border-bottom:1px solid #ced4da');
            $("#er_time").html('');
        }
    }

    console.log(fl);

    if(fl)
    {
        var res = id.split(".");
        var cmnt = "#commentModal"+res[0]+res[1];
        console.log(cmnt);

        $(cmnt).modal('hide');
    }






}

function verify()
{

    console.log("verify");
    $('.img_pop').bind('error', function() {
        console.log('error');
        $(this).hide().after('<i class="fa fa-plus"></i>');
    });
}

function tp(id) {
    var x = document.getElementById("pic_div"+id);
    console.log(x);
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}


function rim(id,pid) {

    var div ="#pic_div"+pid;
    var file ="#file_div"+pid;
    var image = $(div).children("img");
    $(file).children().remove();
    image.remove();
    var input = $("#input-file"+id);
    var html ='';
    html += '<span>Choose file</span>';
    html += '<input type="file" id="input-file'+id+'" name="attachments.'+id+'" data-height="150" />';
    $(file).prepend(html);

    console.log(div,input);
    input.val('');
    console.log(div,input);
}

function readURL(input) {


    if (input.files && input.files[0]) {

        console.log(input.name);
        var name = input.name;
        var res = name.split(".");
        var div ="#pic_div"+res[2];
        console.log(div);
        var image = $(div).children("img");
        image.remove();
        $("#pic_div"+res[2]).prepend('<img src="" id="i'+res[2]+'" class="img_pop"   width="80%" height="60%">');
        var reader = new FileReader();
        reader.onload = function(e) {
            $("#i"+res[2]).attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]); // convert to base64 string





    }
}

$('body').on('change', 'input[type=file]', function(e) {
    console.log(this);
    var $this = $(e.target);
    var $fileField = $this.closest('.file-field');
    var $pathInput = $fileField.find('input.file-path');
    var fileNames = [];

    fileNames.push(e.target.files[0].name);

    $pathInput.val(fileNames.join(', '));
    $pathInput.trigger('change')
    readURL(this);

});


$('body').on('change', 'input:radio', function(e) {
    if($(this).hasClass('0'))
    {

        console.log($(this).attr('name'));
        var name = $(this).attr('name');
        var res = name.split(".");
        console.log('#commentModal'+res[1]+'.'+res[2]);
        var id = "#commentModal"+res[1]+res[2];

        $(id).modal('show');
        $(id).modal({"backdrop": "static"});
        ref =this;
    }
    else
    {
        var name = $(this).attr('name');
        var res = name.split(".");
        to = 'c_answer'+res[1]+"."+res[2];
        console.log(to);
        try
        {

            document.getElementById(to).value = '';
        }catch(e)
        {
            console.log(e);

        }
    }

});


