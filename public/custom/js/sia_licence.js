var selectedrows=[];
 $(document).ready(function() {
    $('body').on('click', '.selectedrowsia_licence', function() {
        var tr_id=$(this).attr('id');
        tr_id=tr_id.split('sia_licence_tr');
        tr_id=tr_id[1];
        var flag=$('#sia_licence'+tr_id).prop('checked');
        if(!flag)
        {
            if(selectedrows.includes(tr_id))
            {
                selectedrows.splice( selectedrows.indexOf(tr_id), 1 );
                $('.sia_licence_export').val(JSON.stringify(selectedrows));
                $('#sia_licence_tr'+tr_id).removeClass('selected');
                $('#sia_licence'+tr_id).attr('checked',false);

            }
            else
            {
                selectedrows.push(tr_id);
                $('.sia_licence_export').val(JSON.stringify(selectedrows));
                $('#sia_licence_tr'+tr_id).addClass('selected');
                $('#sia_licence'+tr_id).attr('checked',true);

            }
        }
    });
    $('#selectedactivebuttonsia_licence').click(function(){
        var url='/licence/active/multiple';
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        if (selectedrows && selectedrows.length) {
       // not empty
            swal({
                title: "Are you sure?",
                text: "Selected option will be active  again ..!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
             },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:url,
                        data: {'ids':selectedrows},
                        success: function(response)
                        {
                            toastr["success"](response.message);
                            $('#sia_licence_table').DataTable().clear().destroy();
                            var url='/licence/getallSoft';
                                sia_licenceAppend(url);

                            swal.close();

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                  swal("Cancelled", "Your Record is safe", "error");

                }
            });
         }
        else
        {
       // empty
            return toastr["error"]("Select at least one record to delete!");
        }
    });
    $('#deleteselectedsia_licence').click(function(){
       var url='/licence/delete/multiple';
       console.log(selectedrows);
       delete_mutilpleDatasia_licence(url,selectedrows);
    });
    $('#deleteselectedsia_licenceSoft').click(function(){
       var url='/licence/delete/soft/multiple';
       delete_mutilpleDatasia_licence(url,selectedrows);
    });
    $('body').on('click', '#create_sia_licence', function(event)
    {
        var company_id=$('#companies_selected_nav').children("option:selected").val();
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        if ($("#schdule_project").val()==""){
            $("#schdule_project_message").html('Field required');
            return 0;
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        event.preventDefault();
        var data=$('#sia_licence_form').serializeArray();
        var obj={
            'company_id':company_id,
            'name':'company_id',
            'value':company_id,
        };
        data.push(obj);
        $.ajax({
            type: 'POST',
            url: '/licence/store',
            data: data,
            success: function(response)
            {
                if(response.error)
                {
                    toastr["error"](response.error);
                }
                else
                {
                    toastr["success"](response.message);
                    console.log(response);
                    $('#sia_licence_table').DataTable().clear().destroy();
                    var url='/licence/getall';
                    sia_licenceAppend(url);
                    $('#modalRegisterLicence').modal('hide');
                }

            },
            error: function (error) {
                console.log(error);
            }
        });

    });

   $('body').on('click', '#edit_sia_licence', function()
    {
        var company_id=$('#companies_selected_nav').children("option:selected").val();
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        event.preventDefault();
        var data=$('#sia_licence_form_edit').serializeArray();
        var obj={'name':'company_id',
                'value':company_id,
                };
        data.push(obj);
        console.log(data);
        $.ajax({
            type: 'POST',
            url: '/licence/update',
            data: data,
            success: function(response)
            {

                toastr["success"](response.message);
                $('#sia_licence_table').DataTable().clear().destroy();
                var url='/licence/getall';
                sia_licenceAppend(url);
                $('#modalEditLicence').modal('hide');

            },
            error: function (error) {
                console.log(error);
            }
        });

    });

    $('#restorebuttonsia_licence').click(function(){

        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        $('#restorebuttonsia_licence').hide();
        $('#deleteselectedsia_licence').hide();
        $('#selectedactivebuttonsia_licence').show();
        $('#deleteselectedsia_licenceSoft').show();
        $('#export_excel_sia_licence').hide()
        $('#export_world_sia_licence').hide()
        $('#export_pdf_sia_licence').hide()
        $('.sia_licence_table').hide();
        $('#activebuttonsia_licence').show();
        $('#sia_licence_table').DataTable().clear().destroy();
        var url='/licence/getallSoft';
        sia_licenceAppend(url);

    });
    $('#activebuttonsia_licence').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        $('#export_excel_sia_licence').show();
        $('#export_world_sia_licence').show();
        $('#export_pdf_sia_licence').show();
        $('.sia_licence_table').show();
        $('#selectedactivebuttonsia_licence').hide();
        $('#deleteselectedsia_licence').show();
        $('#deleteselectedsia_licenceSoft').hide();
        $('#restorebuttonsia_licence').show();
        $('#activebuttonsia_licence').hide();
        $('#sia_licence_table').DataTable().clear().destroy();
        var url='/licence/getall';
        sia_licenceAppend(url);
    });



});
  function getsia_licencedata(id)
    {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type : 'POST',
            url : '/licence/edit',
            data:{'id':id},
            success:function(data)
            {
                console.log(data);
                $('#sia_licence_form_edit').html(data);
                $('#modalEditLicence').modal('show');
                $('.datepicker').pickadate({selectYears:250,});
                $('.mdb_select_sia_licence').materialSelect();
                $('select.mdb_select_sia_licence').hide();

            }
        });


    }
 function restoresia_licencedata(id)
 {
    var url='/licence/active';
    swal({
        title: "Are you sure?",
        text: "Selected option with be active ",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
     },
     function(isConfirm){

       if (isConfirm){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: url,
                data: {'id':id},
                success: function(response)
                {
                    $('#sia_licence_table').DataTable().clear().destroy();

                    if(response.flag==1)
                    {
                        var url='/licence/getall';
                        sia_licenceAppend(url);
                    }
                    else
                    {
                        var url='/licence/getallSoft';
                        sia_licenceAppend(url);

                    }
                    swal.close();
                    toastr["success"](response.message);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
        else {
          swal("Cancelled", "Your Record is safe", "error");

        }
     });
 }
 function delete_mutilpleDatasia_licence(url,selectedrows)
 {
     if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        if (selectedrows && selectedrows.length) {
       // not empty
            swal({
                title: "Are you sure?",
                text: "Selected option will be inactive and can be Active again by the admin only!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
             },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:url,
                        data: {'ids':selectedrows},
                        success: function(response)
                        {

                            toastr["success"](response.message);
                            $('#sia_licence_table').DataTable().clear().destroy();
                            if(response.flag==1)
                            {
                                var url='/licence/getall';
                                sia_licenceAppend(url);
                            }
                            else
                            {
                                var url='/licence/getallSoft';
                                sia_licenceAppend(url);

                            }
                            swal.close();

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                  swal("Cancelled", "Your Record is safe", "error");

                }
            });
         }
        else
        {
       // empty
            return toastr["error"]("Select at least one record to delete!");
        }
 }
 function delete_data_sia_licence(id,url)
 {
    swal({
        title: "Are you sure?",
        text: "Selected option with be Deleted ",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
     },
     function(isConfirm){

       if (isConfirm){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
             $.ajax({
                        type: "POST",
                        url: url,
                        data: {'id':id},
                        success: function(response)
                        {
                            $('#sia_licence_table').DataTable().clear().destroy();
                            if(response.flag==1)
                            {
                                var url='/licence/getall';
                                sia_licenceAppend(url);
                            }
                            else
                            {
                                var url='/licence/getallSoft';
                                sia_licenceAppend(url);

                            }


                            swal.close();
                            toastr["success"](response.message);
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
        } else {
          swal("Cancelled", "Your Record is safe", "error");

        }
     });
 }
function deletesoftsia_licencedata(id)
{
    selectedrows=[];
    var url='/licence/softdelete';
    delete_data_sia_licence(id,url);

}
function deletesia_licencedata(id)
 {
    selectedrows=[];
    var url='/licence/delete';
    delete_data_sia_licence(id,url);
 }
