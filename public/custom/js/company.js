$(document).ready(function() {

    $('body').on('click', '.copy_user', function() {
        var url='/user/copy';
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            method: 'POST',
            type: 'json',
            url: url,
            data: {
                id: $(this).attr('id')
            },

            success: function(response)
            {
                console.log(response.user);
                $('#newuser_name2_id').addClass('active');
                $('#newuser_name2').val(response.user.name);
                $('#newuser_email2_id').addClass('active');
                $('#newuser_email2').val(response.user.email);
                $('#modalRegisterForm2').modal('show');

            },
            error: function (error) {
                console.log(error);
            }
        });
    });

    $('.mdb-select').materialSelect();
    //$('#companies_selected_nav').materialSelect();

    var company_id = 1;

    // buttonHolder.css({'transform' : 'translate(0px)'});

    $('body').on('click','#register_new_company_btn',function (event) {
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        console.log("im here one time");
        event.preventDefault();
        var notetext = document.querySelector("#document-full-create").textContent;
        if (notetext !==''){
            var field_notes=notetext;
        }else{
            var field_notes='Note';
        }
        var name=$('#new_company').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: '/company/store',
            data: {'name':name,field_notes:field_notes},
            success: function(response)
            {
                $('#company_table').DataTable().clear().destroy();
                var company_url='getallcompany';
                companyAppend(company_url);
                if (response.message !==""){
                    toastr["success"](response.message);
                }
                $('#modalRegisterCompany').modal('hide');

            },
            error: function (error) {
                console.log(error);
            }
        });
    });

    $('body').on('click','.edit_company_name_btn',function (event) {
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        var name=$('#edit_user_name').val();
        var id=$('.company_id_hidden_edit').val();
        var notetext = document.querySelector("#document-full-edit").textContent;
        if (notetext !==''){
            var field_notes=notetext;
        }else{
            var field_notes='Note';
        }
        event.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: '/company/update',
            data: {'id':id,'name':name,field_notes:field_notes},
            success: function(response)
            {
                console.log('Testing again');
                $('#company_table').DataTable().clear().destroy();
                var company_url='getallcompany';
                companyAppend(company_url);
                toastr.success("Record Updated Successfully!");
                $("#modalEditFormCompany").modal('hide');
            },
            error: function (error) {
                console.log(error);
            }
        });
    });


    $('#showactivebuttoncompany').click(function(){
        $('#export_excel_company').val(1);
        $('#export_word_company').val(1);
        selectedrows=[];
        $('#restorebutton').show();
        $('.export_company_show_hide').show();
        $('#selectedactivebuttoncompant').hide();
        $('#deleteSelectedCompany_type').val('1');
        $('#showactivebuttoncompany').hide();
        $('#company_table').DataTable().clear().destroy();
        var company_url='getallcompany';
        companyAppend(company_url);
    });

    $('#restorebutton').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        $('#restorebutton').hide();
        $('#export_excel_company').val(2);
        $('.export_company_show_hide').hide();
        $('#export_word_company').val(2);
        $('#deleteSelectedCompany_type').val('2');
        $('#selectedactivebuttoncompant').show();
        $('#showactivebuttoncompany').show();
        $('#company_table').DataTable().clear().destroy();
        selectedrows=[];
        var company_url='/company/soft';
        companyAppend(company_url);
    });

    $('#deleteSelectedCompany').click(function(e) {
        console.log("Selected Rows : "+selectedrows);
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        if (selectedrows && selectedrows.length)
        {
            swal({
                    title: "Are you sure?",
                    text: "Companies will be inactive and can be Active again by the admin only!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Yes, I am sure!',
                    cancelButtonText: "No, cancel it!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm){

                    if (isConfirm){
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $.ajax({
                            type: "POST",
                            url: '/company/multipleDelete',
                            data: {'ids':selectedrows,flag:$("#deleteSelectedCompany_type").val()},
                            success: function(response)
                            {
                                $('#company_table').DataTable().clear().destroy();

                                if ($("#deleteSelectedCompany_type").val()=='1'){
                                    var company_url='getallcompany';
                                }else{
                                    var company_url='/company/soft';
                                }
                                companyAppend(company_url);
                                swal.close()
                                toastr["success"]("Company Hard Deleted Successfully!");
                            },
                            error: function (error) {
                                console.log(error);
                            }
                        });
                    } else {
                        swal("Cancelled", "Your Record is safe", "error");
                        e.preventDefault();
                    }
                });
        }
        else
        {
            return toastr["error"]("Select at least one record to delete!");
        }
    });

    $('#selectedactivebuttoncompant').click(function(e) {
        console.log("Selected Rows : "+selectedrows);
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        if (selectedrows && selectedrows.length)
        {
            swal({
                    title: "Are you sure?",
                    text: "Companies will be inactive and can be Active again by the admin only!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Yes, I am sure!',
                    cancelButtonText: "No, cancel it!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm){

                    if (isConfirm){
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $.ajax({
                            type: "POST",
                            url: '/company/activeMultiple',
                            data: {'ids':selectedrows,flag:$("#deleteSelectedCompany_type").val()},
                            success: function(response)
                            {
                                $('#company_table').DataTable().clear().destroy();
                                if ($("#deleteSelectedCompany_type").val()=='1'){
                                    var company_url='getallcompany';
                                }else{
                                    var company_url='/company/soft';
                                }
                                companyAppend(company_url);
                                swal.close()
                                toastr["success"]("Companies Restore Successfully!");
                            },
                            error: function (error) {
                                console.log(error);
                            }
                        });
                    } else {
                        swal("Cancelled", "Your Record is safe", "error");
                        e.preventDefault();
                    }
                });
        }
        else
        {
            return toastr["error"]("Select at least one record to delete!");
        }
    });
});
var selectedrows=[];
function selectedrow(id)
{
    if(selectedrows.includes(id))
    {
        selectedrows.splice( selectedrows.indexOf(id), 1 );

        $('#company_table tr#'+id).removeClass('selected');
    }
    else
    {
        selectedrows.push(id);
        $('#company_table tr#'+id).addClass('selected');
    }
}


$('body').on('click','#company_checkbox_all',function() {
    $('input:checkbox').not(this).prop('checked', this.checked);
    $('input[type="checkbox"]').each(function(key,value) {
        var id=$(this).attr('id');
        id=id.split('company_check');
        row_id='#company_tr'+id[1];
        if($(this).is(":checked"))
        {
            if(key == 0)
            {
                selectedrows=[];
            }
            if(id[1] != 'box_all')
            {
                selectedrows.push(id[1]);
            }
            $('.company_export').val(JSON.stringify(selectedrows));
            $(row_id).addClass('selected');
        }
        else
        {
            $('.company_export').val(1);
            selectedrows=[];
            $(row_id).removeClass('selected');
        }
    });

});

$('body').on('click', '.selectedrowcompany', function() {
    var id=$(this).attr('id');
    id=id.split('company_check');
    row_id='#company_tr'+id[1];
    if($(this).is(":checked"))
    {
        selectedrows.push(id[1]);
        $('.company_export').val(JSON.stringify(selectedrows));
        $(row_id).addClass('selected');
    }else
    {
        if(selectedrows.includes(id[1]))
        {
            selectedrows.splice( selectedrows.indexOf(id[1]),1);
            $('.company_export').val(JSON.stringify(selectedrows));
            $(row_id).removeClass('selected');
        }
    }
});


function get_company_data(id)
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type : 'post',
        url : '/company',
        data:{'id':id},
        success:function(data)
        {
            console.log(data);
            if (data.field_notes !==null){
                document.querySelector("#document-full-edit").textContent=data.field_notes;
                var quillFull = new Quill('#document-full-edit', {
                    placeholder: "Write something..."
                });
            }else{
                var quillFull = new Quill('#document-full-edit', {
                    placeholder: "Write something..."
                });
            }
            $('#edit_user_name').val(data.name);
            $('#company_id_hidden_edit').val(data.id);
            $("#modalEditFormCompany").modal('show');
        }
    });


}


function deletecompanydata(id)
{
    swal({
            title:'Are you sure?',
            text: "Delete Record.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){
            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type : 'post',
                    url : '/company/delete',
                    data:{'id':id},
                    success:function(data)
                    {
                        setTimeout(function () {
                            swal({
                                    title: "",
                                    text: "Deleted Sucessfully!",
                                    type: "success",
                                    confirmButtonText: "OK"
                                },
                                function(isConfirm){
                                    if (isConfirm) {
                                        $('#company_table').DataTable().clear().destroy();
                                        var company_url='getallcompany';
                                        companyAppend(company_url);
                                    }
                                }); }, 500);
                    }
                });
            } else {
                swal("Cancelled", "Your Record is safe", "error");
                e.preventDefault();
            }
        });
}


function hard_delete_company(id)
{
    swal({
            title: "Are you sure?",
            text: "Selected company with be Hard Deleted and can active from backend again!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){

            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: '/company/hard/delete',
                    data: {'id':id},
                    success: function(response)
                    {
                        $('#company_table').DataTable().clear().destroy();
                        var company_url='/company/soft';
                        companyAppend(company_url);
                        swal.close()
                        toastr["success"]("Company Hard Deleted Successfully!");
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            } else {
                swal("Cancelled", "Your Record is safe", "error");
                e.preventDefault();
            }
        });
}

function restore_company(id)
{
    // var custom_class='';
    // if( $('.edit_cms_disable').css('display') == 'none' ) {
    //     custom_class='assign_class get_text';
    // }
    // else
    // {
    //     custom_class='assign_class';
    // }
    // swal({
    //         title: "Are you sure?",
    //         text: "Selected user with be Active again!",
    //         type: "warning",
    //         showCloseButton: true,
    //         showCancelButton: true,
    //         confirmButtonColor: '#DD6B55',
    //         confirmButtonText: 'Yes, I am sure!',
    //         cancelButtonText: "No, cancel it!",
    //         closeOnConfirm: false,
    //         customClass: "confirm_class",
    //         closeOnCancel: false
    //     },
    //     function(isConfirm){
    //         if( $('.edit_cms_disable').css('display') != 'none' ) {
    //             if (isConfirm){
    //                 $.ajaxSetup({
    //                     headers: {
    //                         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //                     }
    //                 });
    //                 $.ajax({
    //                     type: "POST",
    //                     url:'/company/restore',
    //                     data: {'id':id},
    //                     success: function(response)
    //                     {
    //                         $('#company_table').DataTable().ajax.reload();
    //                         swal.close()
    //                         toastr["success"]("User Activated Successfully!");
    //
    //                     },
    //                     error: function (error) {
    //                         console.log(error);
    //                     }
    //                 });
    //             }
    //             else {
    //                 swal.close();
    //                 // swal("Cancelled", "Your Record is safe", "error");
    //                 // e.preventDefault();
    //             }
    //         }
    //     });
    // var active ='<span class="'+custom_class+' label'+$('#are_you_sure_label').attr('data-id')+'" data-id="'+$('#are_you_sure_label').attr('data-id')+'" data-value="'+$('#are_you_sure_label').val()+'" >'+$('#are_you_sure_label').val()+'</span>';
    // $('.confirm_class h2').html(active);
    // active ='<span class="'+custom_class+' label'+$('#selected_user_with_be_active_again_label').attr('data-id')+'" data-id="'+$('#selected_user_with_be_active_again_label').attr('data-id')+'" data-value="'+$('#selected_user_with_be_active_again_label').val()+'" >'+$('#selected_user_with_be_active_again_label').val()+'</span>';
    // $('.confirm_class p').html(active);
    // active ='<span class="'+custom_class+' label'+$('#cancel_label').attr('data-id')+'" data-id="'+$('#cancel_label').attr('data-id')+'" data-value="'+$('#cancel_label').val()+'" >'+$('#cancel_label').val()+'</span>';
    // $('.cancel').html(active);
    // active ='<span class="'+custom_class+' label'+$('#delete_label').attr('data-id')+'" data-id="'+$('#delete_label').attr('data-id')+'" data-value="'+$('#delete_label').val()+'" >'+$('#delete_label').val()+'</span>';
    // $('.confirm').html(active);



    swal({
            title:'Are you sure?',
            text: "Restore Record.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){
            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url:'/company/restore',
                    data: {'id':id},
                    success: function(response)
                    {
                        $('#company_table').DataTable().clear().destroy();
                        var company_url='/company/soft';
                        companyAppend(company_url);
                        swal.close()
                        toastr["success"]("Company Restored Successfully!");

                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            } else {
                swal("Cancelled", "Your Record is safe", "error");
                e.preventDefault();
            }
        });
}
