var selectedrows=[];
$(document).ready(function() {
    $('body').on('click','#division_checkbox_all',function() {
        $('.selectedrowdevision').not(this).prop('checked', this.checked);
        $('.selectedrowdevision').each(function(key,value) {
            var id=$(this).attr('id');
            console.log(id);
            if(id)
            {
                id=id.split('devision');
                row_id='#devision_tr'+id[1];
                if($(this).is(":checked"))
                {
                    if(key == 0)
                    {
                        selectedrows=[];
                    }
                    if(id[1] != '_checkbox_all')
                    {
                        selectedrows.push(id[1]);
                    }
                    $('.devision_export').val(JSON.stringify(selectedrows));
                    $(row_id).addClass('selected');
                }
                else
                {
                    $('.devision_export').val(1);
                    selectedrows=[];
                    $(row_id).removeClass('selected');
                }
            }
        });
    });


    $('body').on('click', '.selectedrowdevision', function() {
        var id=$(this).attr('id');
        id=id.split('devision');
        row_id='#devision_tr'+id[1];
        if($(this).is(":checked"))
        {
            selectedrows.push(id[1]);
            $('.devision_export').val(JSON.stringify(selectedrows));
            $(row_id).addClass('selected');
        }else
        {
            if(selectedrows.includes(id[1]))
            {
                selectedrows.splice( selectedrows.indexOf(id[1]),1);
                $('.devision_export').val(JSON.stringify(selectedrows));
                $(row_id).removeClass('selected');
            }
        }
    });
    $('#selectedactivebuttondevision').click(function(){
        var url='/devision/active/multiple';
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        if (selectedrows && selectedrows.length) {
            // not empty
            swal({
                    title: "Are you sure?",
                    text: "Selected option will be active  again ..!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Yes, I am sure!',
                    cancelButtonText: "No, cancel it!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm){

                    if (isConfirm){
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $.ajax({
                            type: "POST",
                            url:url,
                            data: {'ids':selectedrows},
                            success: function(response)
                            {

                                toastr["success"](response.message);
                                var url='/devision/getallSoft';
                                if ($("#location_checkbox_all").prop('checked',true)){
                                    $("#location_checkbox_all").prop('checked',false);
                                }
                                $('#devision_table').DataTable().clear().destroy();
                                devision_data(url);
                                swal.close();

                            },
                            error: function (error) {
                                console.log(error);
                            }
                        });

                    } else {
                        swal("Cancelled", "Your Record is safe", "error");

                    }
                });
        }
        else
        {
            // empty
            return toastr["error"]("Select at least one record to delete!");
        }
    });
    $('#deleteselecteddevision').click(function(){
        var url='/devision/delete/multiple';
        delete_mutilpleData_devision(url,selectedrows);
    });
    $('#deleteselecteddevisionSoft').click(function(){
        var url='/devision/delete/soft/multiple';
        delete_mutilpleData_devision(url,selectedrows);
    });

    $('.add-devision').on('click', 'i', () => {
        $.ajax({
            type: 'GET',
            url: '/devision/store',
            success: function(response)
            {
                var division_checkbox=$('#division_checkbox').val();
                var delete_division=$('#delete_division').val();
                var hard_delete_division=$('#hard_delete_division').val();
                var division_name=$('#division_name').val();
                var edit_division=$('#edit_division').val();

                var html='';
                var n_edit = ($("#division_name_create").val()==1) ? "true":"false";
                html+= '<tr id="devision_tr'+response.data.id+'" role="row" class="hide odd">';
                if (division_checkbox==1){
                    html+=' <td><div class="form-check"><input type="checkbox" class="form-check-input selectedrowdevision" id="devisions'+response.data.id+'"><label class="form-check-label" for="devisions'+response.data.id+'"></label></div></td>';
                }

                if(division_name==1){
                    html+='<td class="pt-3-half edit_inline_devision" data-id="'+response.data.id+'" onkeypress="editSelectedRow('+"devision_tr"+response.data.id+')" id="nameD'+response.data.id+'"   contenteditable="'+n_edit+'"></td>';
                }
                html+='<td>';
                if (delete_division ==1){
                    html+='<a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light delete-btn'+response.data.id+'" onclick="deletedevisiondata()"><i class="fas fa-trash"></i></a>';
                }
                if (edit_division ==1){
                    html+='<a type="button" style="display: none;" class="btn btn-primary all_action_btn_margin btn-xs my-0 waves-effect waves-light savedivisiondata devision_show_button_'+response.data.id+' show_tick_btn'+response.data.id+'" id="'+response.data.id+'"><i class="fas fa-check"></i></a>';
                }
                html+='</td></tr>';
                $('#table-devision').find('table').prepend(html);

            },
            error: function (error) {
                console.log(error);
            }
        });


    });
    $('body').on('click', '.savedivisiondata', function() {
        var company_id=$('#companies_selected_nav').children("option:selected").val();
        var tr_id=$(this).attr('id');
        // tr_id=tr_id.split('devision_tr');
        // tr_id=tr_id[1];
        console.log($('#nameD'+tr_id).html());
        var data={
            'id':tr_id,
            'name':$('#nameD'+tr_id).html(),
            'company_id':company_id,
        };
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url:'/devision/update',
            data:data,
            success: function(response)
            {
                if(response.error)
                {
                    toastr["error"](response.error);
                }
                else
                {
                    toastr["success"](response.message);
                    save_and_delete_btn_toggle(tr_id);
                    if(response.flag==1)
                    {
                        $("#devision_tr"+tr_id).attr('style','background-color:#90ee90 !important');
                        // var url='/devision/getall';
                    }
                    else
                    {
                        $("#devision_tr"+tr_id).attr('style','background-color:#ff3547 !important');
                        // var url='/devision/getallsoft';
                    }
                    // $('#devision_table').DataTable().clear().destroy();
                    // devision_data(url);

                }

            }

        });
        // console.log(data);
    });

    $('#restorebuttondevision').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        if ($("#location_checkbox_all").prop('checked',true)){
            $("#location_checkbox_all").prop('checked',false);
        }
        selectedrows=[];
        $('#restorebuttondevision').hide();
        $('#deleteselecteddevision').hide();
        $('#selectedactivebuttondevision').show();
        $('#deleteselecteddevisionSoft').show();
        $('#export_excel_devision').hide();
        $('#export_world_devision').hide();
        $('#export_pdf_devision').hide();
        $('.add-devision').hide();
        $('#activebuttondevision').show();
        var url='/devision/getallSoft';
        $('#devision_table').DataTable().clear().destroy();
        devision_data(url);
    });
    $('#activebuttondevision').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        if ($("#location_checkbox_all").prop('checked',true)){
            $("#location_checkbox_all").prop('checked',false);
        }
        selectedrows=[];
        $('#export_excel_devision').show();
        $('#export_world_devision').show();
        $('#export_pdf_devision').show();
        $('.add-devision').show();
        $('#selectedactivebuttondevision').hide();
        $('#deleteselecteddevision').show();
        $('#deleteselecteddevisionSoft').hide();
        $('#restorebuttondevision').show();
        $('#activebuttondevision').hide();
        var url='/devision/getall';
        $('#devision_table').DataTable().clear().destroy();
        devision_data(url);
    });

});
function restoredevisiondata(id)
{
    var url='/devision/active';
    swal({
            title: "Are you sure?",
            text: "Selected option with be active ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){

            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {'id':id},
                    success: function(response)
                    {
                        if(response.flag==1)
                        {
                            var url='/devision/getall';
                        }
                        else
                        {
                            var url='/devision/getallSoft';
                        }
                        $('#devision_table').DataTable().clear().destroy();
                        devision_data(url);
                        swal.close();
                        toastr["success"](response.message);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            } else {
                swal("Cancelled", "Your Record is safe", "error");

            }
        });
}
function delete_mutilpleData_devision(url,selectedrows)
{
    if( $('.edit_cms_disable').css('display') == 'none' ) {
        console.log('Editer mode On Please change your mode');
        return 0;
    }
    if (selectedrows && selectedrows.length) {
        // not empty
        swal({
                title: "Are you sure?",
                text: "Selected option will be inactive and can be Active again by the admin only!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:url,
                        data: {'ids':selectedrows},
                        success: function(response)
                        {

                            toastr["success"](response.message);
                            if(response.flag==1)
                            {
                                var url='/devision/getall';
                            }
                            else
                            {
                                var url='/devision/getallSoft';
                            }
                            if ($("#location_checkbox_all").prop('checked',true)){
                                $("#location_checkbox_all").prop('checked',false);
                            }
                            $('#devision_table').DataTable().clear().destroy();
                            devision_data(url);
                            swal.close();

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                    swal("Cancelled", "Your Record is safe", "error");

                }
            });
    }
    else
    {
        // empty
        return toastr["error"]("Select at least one record to delete!");
    }
}
function delete_data_division(id,url)
{
    swal({
            title: "Are you sure?",
            text: "Selected option with be Deleted ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){

            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {'id':id},
                    success: function(response)
                    {
                        if(response.flag==1)
                        {
                            var url='/devision/getall';
                        }
                        else
                        {
                            var url='/devision/getallSoft';
                        }
                        $('#devision_table').DataTable().clear().destroy();
                        devision_data(url);
                        swal.close();
                        toastr["success"](response.message);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            } else {
                swal("Cancelled", "Your Record is safe", "error");

            }
        });
}
function deletesoftdevisiondata(id)
{
    var url='/devision/softdelete';
    delete_data_division(id,url);

}
function deletedevisiondata(id)
{
    var url='/devision/delete';
    delete_data_division(id,url);
}
