var selectedrows=[];
$('body').on('click','#contact_checkbox_all',function() {
    $('input:checkbox').not(this).prop('checked', this.checked);
    $('input[type="checkbox"]').each(function(key,value) {
        var id=$(this).attr('id');
        id=id.split('contact');
        row_id='#contact_tr'+id[1];
        if($(this).is(":checked"))
        {
            if(key == 0)
            {
                selectedrows=[];
            }
            if(id[1] != '_checkbox_all')
            {
                selectedrows.push(id[1]);
            }
            $('.contact_export').val(JSON.stringify(selectedrows));
            $(row_id).addClass('selected');
        }
        else
        {
            $('.contact_export').val(1);
            selectedrows=[];
            $(row_id).removeClass('selected');
        }
    });
    // var x = document.getElementsByClassName("selectedRowContact");
    // var i;
    // for (i = 0; i < x.length;i++) {
    //     var tr_id=x[i].id;
    //     tr_id=tr_id.split('contact');
    //     tr_id=tr_id[1];
    //     if(selectedrows.includes(tr_id))
    //     {
    //         selectedrows.splice( selectedrows.indexOf(tr_id), 1 );
    //         if (selectedrows.length >0){
    //             $('.contact_export').val(JSON.stringify(selectedrows));
    //         }else{
    //             $('.contact_export').val('1');
    //         }
    //         $('#contact_tr'+tr_id).removeClass('selected');
    //         $('#contact'+tr_id).attr('checked',false);

    //     }
    //     else
    //     {
    //         selectedrows.push(tr_id);
    //         $('.contact_export').val(JSON.stringify(selectedrows));
    //         $('#contact_tr'+tr_id).addClass('selected');
    //         $('#contact'+tr_id).attr('checked',true);
    //     }
    // }

});


$('body').on('click', '.selectedRowContact', function() {
    // var tr_id=$(this).attr('id');
    // tr_id=tr_id.split('contact');
    // tr_id=tr_id[1];
    //   if(selectedrows.includes(tr_id))
    //   {
    //     selectedrows.splice( selectedrows.indexOf(tr_id), 1 );
    //       if (selectedrows.length >0){
    //           $('.contact_export').val(JSON.stringify(selectedrows));
    //       }else{
    //           $('.contact_export').val('1');
    //       }
    //     $('#contact_tr'+tr_id).removeClass('selected');
    //   }
    //   else
    //   {
    //     selectedrows.push(tr_id);
    //     $('.contact_export').val(JSON.stringify(selectedrows));
    //     $('#contact_tr'+tr_id).addClass('selected');
    //   }
    var id=$(this).attr('id');
    id=id.split('contact');
    row_id='#contact_tr'+id[1];
    if($(this).is(":checked"))
    {
        selectedrows.push(id[1]);
        $('.contact_export').val(JSON.stringify(selectedrows));
        $(row_id).addClass('selected');
    }else
    {
        if(selectedrows.includes(id[1]))
        {
            selectedrows.splice( selectedrows.indexOf(id[1]),1);
            $('.contact_export').val(JSON.stringify(selectedrows));
            $(row_id).removeClass('selected');
        }
    }
});
$('body').on('click', '.contact_create', function() {
    if( $('.edit_cms_disable').css('display') == 'none' ) {
        console.log('Editer mode On Please change your mode');
        return 0;
    }
    var formData = $("#contactForm").serializeObject();
    formData.company_id = $("#companies_selected_nav").val();
    var contacts_type_id = $('.contacts_type_id_create').children('option:selected').val();
    var ajaxcall=1;
    if ($("#first_name").val() =="" ||  $("#email").val() =="" || contacts_type_id=="")
    {
        ajaxcall=0;
    }
    if ($("#first_name").val() ==""){
        $("#first_name").attr('style','border-bottom:1px solid #e64c48');
        $("#first_name_message").html('First name required!');
    }
    if (contacts_type_id ==""){
        $("[data-activates=select-options-contacts_type_id]").attr('style','border-bottom:1px solid #e64c48');
        $("#contacts_type_id_create_message").html('Contact Type required!');
    }
    if ($("#email").val() ==""){
        $("#email").attr('style','border-bottom:1px solid #e64c48');
        $("#email_message").html('Email address required!');
    }
    if(ajaxcall){
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            url: '/contact/contactModalSave',
            data: formData,
            success: function(data)
            {
                if (data.status ==false){
                    $("#email").attr('style','border-bottom:1px solid #e64c48');
                    $("#email_message").html(data.error);
                    return false;
                }

                $("#first_name").attr('style','');
                $("#first_name_message").html('');

                $("#email").attr('style','');
                $("#email_message").html('');

                $("#select-options-contacts_type_id").attr('style','');
                $("#contacts_type_id_create_message").html('');

                $('#contact_table').DataTable().clear().destroy();
                contactAppend();
                toastr["success"]("New Location Added Successfully!");
                $('#modalContactCreate').modal('hide');
            },
            error: function (error) {
                console.log(error);
            }
        });
    }
});
function contactEdit(id)
{
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: '/contact/edit',
        data: {id:id},
        success: function(response)
        {
            var html='';
            for (var i = response.contactsType.length - 1; i >= 0; i--) {
                var selected='';
                if(response.data.contacts_type_id == response.contactsType[i].id)
                {
                    selected='selected';
                }
                html+='<option '+selected+' value="'+response.contactsType[i].id+'">'+response.contactsType[i].name+'</option>';
            }
            $('#contactsType_edit_ajax').html(html);

            document.getElementById("edit_title").value = response.data.title;
            document.getElementById("edit_email").value = response.data.email;
            document.getElementById("edit_name").value = response.data.name;
            document.getElementById("edit_address_street").value = response.data.address_street;
            document.getElementById("edit_town").value = response.data.town;
            document.getElementById("edit_state").value = response.data.state;
            // document.getElementById("edit_country").value = response.data.country;
            document.getElementById("edit_post_code").value = response.data.post_code;
            document.getElementById("edit_first_name").value = response.data.first_name;
            document.getElementById("edit_surname").value = response.data.surname;
            document.getElementById("edit_extension").value = response.data.extension;
            document.getElementById("edit_mobile_number").value = response.data.mobile_number;
            document.getElementById("edit_phone_number").value = response.data.phone_number;
            document.getElementById("edit_other_number").value = response.data.other_number;
            document.getElementById("edit_id").value = response.data.id;
        },
        error: function (error) {
            console.log(error);
        }
    });
}
function update()
{
    if( $('.edit_cms_disable').css('display') == 'none' ) {
        console.log('Editer mode On Please change your mode');
        return 0;
    }
    if ($("#edit_first_name").val() ==""){
        $("#edit_first_name").attr('style','border-bottom:1px solid #e64c48');
        $("#edit_first_name_message").html('First name required!');
    }else if ($("#edit_email").val() ==""){
        $("#edit_email").attr('style','border-bottom:1px solid #e64c48');
        $("#edit_email_message").html('Email address required!');
    }else {
        var formData = $("#contact_form_edit").serializeArray();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            url: '/contact/update',
            data: formData,
            success: function (response) {

                if (response.status ==false){
                    $("#edit_email").attr('style','border-bottom:1px solid #e64c48');
                    $("#edit_email_message").html('Invalid email address');
                    return false;
                }

                $("#edit_first_name").attr('style','');
                $("#edit_first_name_message").html('');

                $("#edit_email").attr('style','');
                $("#edit_email_message").html('');

                $('#contact_table').DataTable().clear().destroy();
                contactAppend();
                toastr["success"](response.message);
                $('#modalContactEdit').modal('hide');
            },
            error: function (error) {
                console.log(error);
            }
        });
    }
}

function deleteContact(id)
{
    swal({
            title:'Are you sure?',
            text: "Delete Record.",
            type: "error",
            showCloseButton: true,
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Delete!",
            cancelButtonText: "Cancel!",
            closeOnConfirm: false,
            customClass: "confirm_class",
            closeOnCancel: false,
        },
        function(isConfirm){
            console.log( isConfirm);
            if( $('.edit_cms_disable').css('display') != 'none' ) {
                if (isConfirm) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type : 'post',
                        url : '/contact/delete',
                        data:{'id':id},
                        success:function(data)
                        {

                            setTimeout(function () {
                                swal({
                                        title: "",
                                        text: "Delete Sucessfully!",
                                        type: "success",
                                        confirmButtonText: "OK"
                                    },

                                    function(isConfirm){

                                        if (isConfirm) {
                                            $('#contact_table').DataTable().clear().destroy();
                                            contactAppend();
                                        }
                                    }); }, 500);


                        }
                    });

                }
                else
                {
                    swal.close();
                }
            }
        });
}
$('.contact_listing_close').click(function(){
    $.ajax({
        type: "GET",
        url: '/contact/contacttype/listing',
        success: function(response)
        {
            var html='';
            html+='<select class="mdb-select-custom-contactsType" name="contacts_type_id" >';
            html+='<option value="" selected disabled>Please Select</option>';
            for (var i = response.contactsType.length - 1; i >= 0; i--) {
                html+='<option value="'+response.contactsType[i].id+'">'+response.contactsType[i].name+'</option>';
            }
            html+='</select> ';
            $('.contactsType_listing').empty();
            $('.contactsType_listing').html(html);
            $('.mdb-select-custom-contactsType').materialSelect();
            $('select.mdb-select-custom-contactsType').hide();
        },
        error: function (error) {
            console.log(error);
        }
    });
});

$('#deleteSelectedContact').click(function(){
    if( $('.edit_cms_disable').css('display') == 'none' ) {
        console.log('Editer mode On Please change your mode');
        return 0;
    }
    if (selectedrows && selectedrows.length) {
        // not empty
        swal({
                title: "Are you sure?",
                text: "Locations will be inactive and can be Active again by the admin only!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:'/contact/multipleDelete',
                        data: {'ids':selectedrows,'flag': $('#flagContact').val()},
                        success: function(response)
                        {

                            $('#contact_table').DataTable().clear().destroy();

                            if($('#flagContact').val()==1)
                            {
                                contactAppend();
                            }
                            else
                            {
                                contactSoftAppend();
                            }
                            $("#contact_checkbox_all").attr('checked',false);
                            swal.close();
                            toastr["success"]("Locations Deleted Successfully!");

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                    swal("Cancelled", "Your Record is safe", "error");
                    e.preventDefault();
                }
            });
    }
    else
    {
        // empty
        return toastr["error"]("Select at least one record to delete!");
    }
});
$('#restore_button_contact').click(function(){
    if( $('.edit_cms_disable').css('display') == 'none' ) {
        console.log('Editer mode On Please change your mode');
        return 0;
    }
    selectedrows=[];
    $("#export_excel_contact_btn").hide();
    $("#export_word_contact_btn").hide();
    $("#export_pdf_contact_btn").hide();
    $('#restore_button_contact').hide();
    $('#export_excel_contact').val(2);
    $('#export_pdf_contact').val(2);
    $('#export_word_contact').val(2);
    $('#flagAddress').val(2);
    $('#show_active_button_contact').show();
    $('#contact_table').DataTable().clear().destroy();
    contactSoftAppend();
});
$('#show_active_button_contact').click(function(){
    if( $('.edit_cms_disable').css('display') == 'none' ) {
        console.log('Editer mode On Please change your mode');
        return 0;
    }
    selectedrows=[];
    $("#export_excel_contact_btn").show();
    $("#export_word_contact_btn").show();
    $("#export_pdf_contact_btn").show();
    $('#show_active_button_contact').hide();
    $('#export_excel_contact').val(1);
    $('#export_pdf_contact').val(1);
    $('#export_word_contact').val(1);
    $('#flagAddress').val(1);
    $('#restore_button_contact').show();
    $("#contact_checkbox_all").attr('checked',false);
    $('#contact_table').DataTable().clear().destroy();
    contactAppend();
});

function restoreContact(id)
{
    swal({
            title: "Are you sure?",
            text: "Selected Location will be Active again!",
            type: "warning",
            showCloseButton: true,
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            customClass: "confirm_class",
            closeOnCancel: false
        },
        function(isConfirm){
            if( $('.edit_cms_disable').css('display') != 'none' ) {
                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:'/contact/restore',
                        data: {'id':id},
                        success: function(response)
                        {
                            $('#contact_table').DataTable().clear().destroy();
                            swal.close()
                            toastr["success"]("Location Activated Successfully!");
                            if ($(":checkbox").prop('checked',true)){
                                $(":checkbox").prop('checked',false);
                            }
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
                }
                else {
                    swal.close();
                    // swal("Cancelled", "Your Record is safe", "error");
                    // e.preventDefault();
                }
            }
        });

}

function hardDeleteContact(id)
{
    swal({
            title: "Are you sure?",
            text: "Selected Location with be Hard Deleted and can active from backend again!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){

            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: '/contact/hardDelete',
                    data: {'id':id},
                    success: function(response)
                    {
                        $('#contact_table').DataTable().clear().destroy();
                        contactAppend();
                        swal.close()
                        toastr["success"]("Location Hard Deleted Successfully!");
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            } else {
                swal("Cancelled", "Your Record is safe", "error");
                e.preventDefault();
            }
        });
}
