$(document).ready(function() {
    var selectedrows=[];
    $('body').on('click','#equipmentGroup_checkbox_all',function() {
        $('input:checkbox').not(this).prop('checked', this.checked);
        $('input[type="checkbox"]').each(function(key,value) {
            var id=$(this).attr('id');
            id=id.split('plant_and_equipments');
            row_id='#equipment_tr'+id[1];
            if($(this).is(":checked"))
            {
                if(key == 0)
                {
                    selectedrows=[];
                }
                if(id[1] != '_checkbox_all')
                {
                    selectedrows.push(id[1]);
                }
                $('.plant_equipment_export').val(JSON.stringify(selectedrows));
                $(row_id).addClass('selected');
            }
            else
            {
                $('.plant_equipment_export').val(1);
                selectedrows=[];
                $(row_id).removeClass('selected');
            }
        });

    });

    $('body').on('click', '.selectedrowequipment', function() {
        var id=$(this).attr('id');
        id=id.split('plant_and_equipments');
        row_id='#equipment_tr'+id[1];
        if($(this).is(":checked"))
        {
            selectedrows.push(id[1]);
            $('.plant_equipment_export').val(JSON.stringify(selectedrows));
            $(row_id).addClass('selected');
        }else
        {
            if(selectedrows.includes(id[1]))
            {
                selectedrows.splice( selectedrows.indexOf(id[1]),1);
                $('.plant_equipment_export').val(JSON.stringify(selectedrows));
                $(row_id).removeClass('selected');
            }
        }
    });


    $('#selectedactivebuttonequipment').click(function(){
        var url='/plant_and_equipment/active/multiple';
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        if (selectedrows && selectedrows.length) {
            // not empty
            swal({
                    title: "Are you sure?",
                    text: "Selected option will be active  again ..!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Yes, I am sure!',
                    cancelButtonText: "No, cancel it!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm){

                    if (isConfirm){
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $.ajax({
                            type: "POST",
                            url:url,
                            data: {'ids':selectedrows},
                            success: function(response)
                            {

                                toastr["success"](response.message);
                                var url='/plant_and_equipment/getallSoft';
                                equipment_data(url);
                                swal.close();
                                if ($("#equipmentGroup_checkbox_all").prop('checked',true)){
                                    $("#equipmentGroup_checkbox_all").prop('checked',false);
                                }
                            },
                            error: function (error) {
                                console.log(error);
                            }
                        });

                    } else {
                        swal("Cancelled", "Your Record is safe", "error");
                        e.preventDefault();
                    }
                });
        }
        else
        {
            // empty
            return toastr["error"]("Select at least one record to delete!");
        }
    });
    $('#deleteselectedequipment').click(function(){
        var url='/plant_and_equipment/delete/multiple';
        delete_mutilpleData(url,selectedrows);
    });
    $('#deleteselectedequipmentSoft').click(function(){
        var url='/plant_and_equipment/delete/soft/multiple';
        delete_mutilpleData(url,selectedrows);
    });

    $('.add-equipment').on('click', 'i', () => {
        $.ajax({
            type: 'GET',
            url: '/plant_and_equipment/store',
            success: function(response)
            {
                var today = new Date();
                var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
                var html='';
                var today = new Date();
                var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
                html+= '<tr id="equipment_tr'+response.data.id+'"  role="row" class="hide odd">';
                if($("#equipmentGroup_checkbox").val() =='1'){
                    html+=' <td><div class="form-check"><input type="checkbox" class="form-check-input selectedrowequipment" id="plant_and_equipments'+response.data.id+'"><label class="form-check-label" for="plant_and_equipments'+response.data.id+'"></label></div></td>';
                }
                if($("#equipmentGroup_qr").val() =='1'){
                    html+='<td class="qr_image'+response.data.id+'"></td>';
                }
                var n_edit = ($("#equipmentGroup_name_create").val()=='1') ? 'true':'false';
                if($("#equipmentGroup_name").val() =='1'){
                    html+='<td class="pt-3-half edit_inlinepe" onkeypress="editSelectedRow('+"equipment_tr"+response.data.id+')" data-id="'+response.data.id+'" id="name'+response.data.id+'"   contenteditable="'+n_edit+'"></td>';
                }
                var e_edit = ($("#equipmentGroup_equipement_create").val()=='1') ? 'true':'false';
                var e_disable = ($("#equipmentGroup_equipement_create").val()=='1') ? '':'disabled';
                if($("#equipmentGroup_equipement").val() =='1'){
                    html+='<td class="pt-3-half edit_inlinepe"  data-id="'+response.data.id+'">';
                    html+='<select id="equipment_edit'+response.data.id+'" onchange="editSelectedRow('+"equipment_tr"+response.data.id+')"  style="background-color: inherit;border: 0px">';
                    for (var j = response.equipments.length - 1; j >= 0; j--) {
                        html+='<option '+e_disable+' value="'+response.equipments[j].id+'"  >'+response.equipments[j].name+'</option>';
                    }
                    html+='</select></td>';
                }
                var s_edit = ($("#equipmentGroup_serialNumber_create").val()=='1') ? 'true':'false';
                if($("#equipmentGroup_serialNumber").val() =='1'){
                    html+='<td class="pt-3-half edit_inlinepe" onkeypress="editSelectedRow('+"equipment_tr"+response.data.id+')" data-id="'+response.data.id+'" id="serial'+response.data.id+'"   contenteditable="'+s_edit+'"></td>';
                }
                var make_edit = ($("#equipmentGroup_make_create").val()=='1') ? 'true':'false';
                if($("#equipmentGroup_make").val() =='1'){
                    html+='<td class="pt-3-half edit_inlinepe" onkeypress="editSelectedRow('+"equipment_tr"+response.data.id+')" data-id="'+response.data.id+'" id="make'+response.data.id+'"   contenteditable="'+make_edit+'"></td>';
                }
                var model_edit = ($("#equipmentGroup_model_create").val()=='1') ? 'true':'false';
                if($("#equipmentGroup_model").val() =='1'){
                    html+='<td class="pt-3-half edit_inlinepe" onkeypress="editSelectedRow('+"equipment_tr"+response.data.id+')" data-id="'+response.data.id+'" id="model'+response.data.id+'"   contenteditable="'+model_edit+'"></td>';
                }
                var l_disabled = ($("#equipmentGroup_lastserviceDate_create").val()=='1') ? '':'disabled';
                if($("#equipmentGroup_lastserviceDate").val() =='1'){
                    html+='<td class="pt-3-half">';
                    // html+=date;
                    html+'</td>';
                }
                var ne_disabled = ($("#equipmentGroup_nextserviceDate_create").val()=='1') ? '':'disabled';
                if($("#equipmentGroup_nextserviceDate").val() =='1'){
                    html+='<td class="pt-3-half">';
                    // html+=date;
                    html+'</td>';
                }
                // html+='<td class="pt-3-half edit_inlinepe" id="last_service_date'+response.data.id+'"  >'+date+'</td>';
                // html+='<td class="pt-3-half edit_inlinepe" id="next_service_date'+response.data.id+'"  >'+date+'</td>';
                var co_edit = ($("#equipmentGroup_condition_create").val()=='1') ? 'true':'false';
                if($("#equipmentGroup_condition").val() =='1'){
                    html+='<td class="pt-3-half edit_inlinepe" onkeypress="editSelectedRow('+"equipment_tr"+response.data.id+')"  data-id="'+response.data.id+'" id="condition'+response.data.id+'"   contenteditable="'+co_edit+'" ></td>';
                }
                //html+='<td class="pt-3-half edit_inlinepe" id="notes'+response.data.id+'"   contenteditable="true" ></td>';
                html+='<td class="pt-3-half">';
                if($("#delete_equipmentGroup").val() =='1'){
                    html+='<a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light delete-btn'+response.data.id+'" onclick="deleteplant_and_equipmentdata()"><i class="fas fa-trash"></i></a>';
                }
                if($("#edit_equipmentGroup").val() =='1'){
                    html+='<a type="button" class="btn btn-primary btn-xs my-0 waves-effect waves-light savepedata equipment_show_button_'+response.data.id+' show_tick_btn'+response.data.id+'" style="display: none;" id="'+response.data.id+'"><i class="fas fa-check"></i></a>';
                    html+='<a type="button" class="btn btn-primary btn-xs my-0 waves-effect waves-light all_action_btn_margin equipment_edit_button_'+response.data.id+' " onclick="getplantandequipmentdata(this.id)"  id="'+response.data.id+'"><i class="fas fa-pencil-alt"></i></a>';
                }
                html+='</td></tr>';
                $('#table-equipment').find('table').prepend(html);
            },
            error: function (error) {
                console.log(error);
            }
        });
    });
    $('body').on('click', '.savepedata ', function() {
        var company_id=$('#companies_selected_nav').children("option:selected").val();
        var tr_id=$(this).attr('id');
        var data={
            'id':tr_id,
            'name':$('#name'+tr_id).html(),
            'equipment_id':$('#equipment_edit'+tr_id).children("option:selected").val(),
            'serial_number':$('#serial'+tr_id).html(),
            'make':$('#make'+tr_id).html(),
            'model':$('#model'+tr_id).html(),
            // 'last_service_date':$('#last_service_date'+tr_id).val(),
            // 'next_service_date':$('#next_service_date'+tr_id).val(),
            'condition':$('#condition'+tr_id).html(),
            'tag':$('#notes'+tr_id).html(),
            'company_id':company_id,
        };
        console.log("Data : "+JSON.stringify(data));
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url:'/plant_and_equipment/update',
            data:data,
            success: function(response)
            {
                if(response.error)
                {
                    toastr["error"](response.error);
                    $("#equipment_tr"+tr_id).attr('style','background-color:#ff3547 !important');
                }
                else
                {
                    toastr["success"](response.message);
                    $('.qr_image'+tr_id).html(response.qr);
                    $("#equipment_tr"+tr_id).attr('style','background-color:#90ee90 !important');
                    // var url='/plant_and_equipment/getall';
                    // equipment_data(url);
                    save_and_delete_btn_toggle(tr_id);
                }

            }
        });

        // console.log(data);

    });

    $('body').on('click', '#update_plantandequipment ', function() {
        var company_id=$('#companies_selected_nav').children("option:selected").val();
        var tr_id=$(this).attr('id');
        var data={
            'id':$("#location_breakdown_id").val(),
            'name':$('#location_breakdown_name_edit').val(),
            'equipment_id':$('#location_breakdown_edit').children("option:selected").val(),
            'serial_number':$('#serial_number_edit').val(),
            'make':$('#make_edit').val(),
            'model':$('#model_edit').val(),
            'last_service_date':$('#last_service_date_edit').val(),
            'next_service_date':$('#next_service_date_edit').val(),
            'condition':$('#condition_edit').val(),
            //'tag':$('#notes'+tr_id).html(),
            'company_id':company_id,
        };
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url:'/plant_and_equipment/update',
            data:data,
            success: function(response)
            {
                if(response.error)
                {
                    toastr["error"](response.error);
                    $("#equipment_tr"+tr_id).attr('style','background-color:#ff3547 !important');
                }
                else
                {
                    toastr["success"](response.message);
                    var url='/plant_and_equipment/getall';
                    equipment_data(url);
                    save_and_delete_btn_toggle(tr_id);
                    $("#modalplantandequipment").modal('hide');
                }

            }
        });

        // console.log(data);

    });

    $('#restorebuttonequipment').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        if ($("#equipmentGroup_checkbox_all").prop('checked',true)){
            $("#equipmentGroup_checkbox_all").prop('checked',false);
        }
        selectedrows=[];
        $("#export_excel_equipment_btn").hide();
        $("#export_world_equipment_btn").hide();
        $("#export_world_pdf_btn").hide();
        $('#restorebuttonequipment').hide();
        $('#deleteselectedequipment').hide();
        $('#selectedactivebuttonequipment').show();
        $('#deleteselectedequipmentSoft').show();
        $('#export_excel_equipment').val(2);
        $('#export_world_equipment').val(2);
        $('#export_world_pdf').val(2);
        $('.add-equipment').hide();
        $('#activebuttonequipment').show();
        var url='/plant_and_equipment/getallSoft';
        equipment_data(url);
    });
    $('#activebuttonequipment').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        if ($("#equipmentGroup_checkbox_all").prop('checked',true)){
            $("#equipmentGroup_checkbox_all").prop('checked',false);
        }
        selectedrows=[];
        $("#export_excel_equipment_btn").show();
        $("#export_world_equipment_btn").show();
        $("#export_world_pdf_btn").show();
        $('#export_excel_equipment').val(1);
        $('#export_world_equipment').val(1);
        $('#export_world_pdf').val(1);
        $('.add-equipment').show();
        $('#selectedactivebuttonequipment').hide();
        $('#deleteselectedequipment').show();
        $('#deleteselectedequipmentSoft').hide();
        $('#restorebuttonequipment').show();
        $('#activebuttonequipment').hide();
        var url='/plant_and_equipment/getall';
        equipment_data(url);
    });

});

function delete_mutilpleData(url,selectedrows)
{
    if( $('.edit_cms_disable').css('display') == 'none' ) {
        console.log('Editer mode On Please change your mode');
        return 0;
    }
    if (selectedrows && selectedrows.length) {
        // not empty
        swal({
                title: "Are you sure?",
                text: "Selected option will be inactive and can be Active again by the admin only!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:url,
                        data: {'ids':selectedrows},
                        success: function(response)
                        {

                            toastr["success"](response.message);
                            if(response.flag==1)
                            {
                                var url='/plant_and_equipment/getall';
                            }
                            else
                            {
                                var url='/plant_and_equipment/getallSoft';
                            }
                            equipment_data(url);
                            if ($("#equipmentGroup_checkbox_all").prop('checked',true)){
                                $("#equipmentGroup_checkbox_all").prop('checked',false);
                            }
                            swal.close();

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                    swal("Cancelled", "Your Record is safe", "error");
                    e.preventDefault();
                }
            });
    }
    else
    {
        // empty
        return toastr["error"]("Select at least one record to delete!");
    }
}
function delete_data_plant_and_equipment(id,url)
{
    swal({
            title: "Are you sure?",
            text: "Selected option with be Deleted ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){

            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {'id':id},
                    success: function(response)
                    {
                        if(response.flag==1)
                        {
                            var url='/plant_and_equipment/getall';
                        }
                        else
                        {
                            var url='/plant_and_equipment/getallSoft';
                        }
                        equipment_data(url);
                        if ($("#equipmentGroup_checkbox_all").prop('checked',true)){
                            $("#equipmentGroup_checkbox_all").prop('checked',false);
                        }
                        swal.close();
                        toastr["success"](response.message);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            } else {
                swal("Cancelled", "Your Record is safe", "error");
                e.preventDefault();
            }
        });
}
function deletesoftplant_and_equipmentdata(id)
{
    var url='/plant_and_equipment/softdelete';
    delete_data_plant_and_equipment(id,url);

}
function deleteplant_and_equipmentdata(id)
{
    var url='/plant_and_equipment/delete';
    delete_data_plant_and_equipment(id,url);
}

function getplantandequipmentdata(id)
{
    console.log(id);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type : 'POST',
        url : '/plant_and_equipment/edit',
        data:{'id':id},
        success:function(data)
        {
            console.log("Data : "+JSON.stringify(data.data));
            $('#location_breakdown_id').val(data.data.id);
            $('#location_breakdown_name_edit').val(data.data.name);
            $('#serial_number_edit').val(data.data.serial_number);
            $('#make_edit').val(data.data.make);
            $('#model_edit').val(data.data.model);
            $('#condition_edit').val(data.data.condition);
            var options = {year: 'numeric', month: 'long', day: 'numeric' };
            var next_service_date  = new Date(data.next_service_date);
            var next_date=next_service_date.toLocaleDateString("en-US", options);
            $('#next_service_date_edit').val(next_date);

            var last_service_date  = new Date(data.last_service_date);
            var last_date=last_service_date.toLocaleDateString("en-US", options);
            $('#last_service_date_edit').val(last_date);
            location_breakdown_html="";
            for (var i = data.location_breakdowns.length - 1; i >= 0; i--) {
                if(data.location_breakdowns[i].id == data.data.location_id)
                {
                    location_breakdown_html+='<option selected value="'+data.location_breakdowns[i].id+'">'+data.location_breakdowns[i].name+'</option>';
                }
                else
                {
                    location_breakdown_html+='<option value="'+data.location_breakdowns[i].id+'">'+data.location_breakdowns[i].name+'</option>';
                }
            }
            $('#location_breakdown_edit').html(location_breakdown_html);
            $("#modalplantandequipment").modal('show');
        }
    });
}

function active_data_plant_and_equipment(id,url)
{
    swal({
            title: "Are you sure?",
            text: "Selected option with be Deleted ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){

            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {'id':id},
                    success: function(response)
                    {
                        if(response.flag==1)
                        {
                            var url='/plant_and_equipment/getall';
                        }
                        else
                        {
                            var url='/plant_and_equipment/getallSoft';
                        }
                        equipment_data(url);
                        swal.close();
                        toastr["success"](response.message);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            } else {
                swal("Cancelled", "Your Record is safe", "error");
                e.preventDefault();
            }
        });
}
function activeplant_and_equipmentdata(id)
{
    var url='/plant_and_equipment/active';
    delete_data_plant_and_equipment(id,url);
}
