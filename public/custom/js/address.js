function save()
{
    if( $('.edit_cms_disable').css('display') == 'none' ) {
        console.log('Editer mode On Please change your mode');
        return 0;
    }
    var request=1;
    var formData = $("#address_type_form").serializeObject();
    if ($("#address_type_id").children('option:selected').val() =="" || $("#email").val() =="" || $("#name").val() ==""){
        if ($("#address_type_id").children('option:selected').val() ==""){
            $("[data-activates=select-options-address_type_id]").attr('style','border-bottom:1px solid #e64c48');
            $("#address_type_id_message").html('Address Type required!');
        }
        if ($("#email").val() ==""){
            $("#email").attr('style','border-bottom:1px solid #e64c48');
            $("#email_address").html('Email address required!');
        }
        if ($("#name").val() ==""){
            $("#name").attr('style','border-bottom:1px solid #e64c48');
            $(".building_name").html('Building name required!');
        }
        request=0;
    }
    if (request){
        formData.company_id = $("#companies_selected_nav").val();
        console.log(formData);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            url: '/address/addressTypeModalSave',
            data: formData,
            success: function(response)
            {
                if (response.status ==false){
                    $("#email").attr('style','border-bottom:1px solid #e64c48');
                    $("#email_address").html('Invalid email address');
                    return false;
                }
                $("#email_address").html('');
                $("#building_name").html('');
                $("#email").attr('style','');
                $("#name").attr('style','');

                $('#location_address').DataTable().clear().destroy();
                locationAppend();
                toastr["success"](response.message);
                setTimeout(function (){
                    $('#address_type_id').html($('#address_type_id').html());
                },500);
                $('#modalAddressCreate').modal('hide');
                $(".form_empty").val('');

            },
            error: function (error) {
                console.log(error);
            }
        });
    }
}
function locationEdit(id)
{
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: '/address/edit',
        data: {id:id},
        success: function(data)
        {
            document.getElementById("edit_telephone").value = data.telephone;
            document.getElementById("edit_email").value = data.email;
            document.getElementById("edit_name").value = data.name;
            document.getElementById("edit_address_street").value = data.address_street;
            document.getElementById("edit_town").value = data.town;
            document.getElementById("edit_state").value = data.state;
            document.getElementById("edit_country").value = data.country;
            document.getElementById("edit_post_code").value = data.post_code;
            document.getElementById("edit_id").value = data.id;
        },
        error: function (error) {
            console.log(error);
        }
    });
}
function update()
{
    if( $('.edit_cms_disable').css('display') == 'none' ) {
        console.log('Editer mode On Please change your mode');
        return 0;
    }
    if ($("#edit_email").val() ==""){
        $("#edit_email").attr('style','border-bottom:1px solid #e64c48');
        $("#edit_email_address").html('Email address required!');
    }else if ($("#edit_name").val() ==""){
        $("#edit_name").attr('style','border-bottom:1px solid #e64c48');
        $("#edit_building_name").html('Building name required!');
    }else {
        var formData = $("#address_type_form_edit").serializeObject();
        console.log(formData);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            url: '/address/update',
            data: formData,
            success: function(response)
            {
                if (response.status ==false){
                    $("#edit_email").attr('style','border-bottom:1px solid #e64c48');
                    $("#edit_email_address").html('Invalid email address');
                    return false;
                }
                $("#edit_email_address").html('');
                $("#edit_email").attr('style','');
                $("#edit_name").attr('style','');
                $("#edit_building_name").html('');
                $("#edit_email").attr('style','');
                $("#edit_name").attr('style','');

                $('#location_address').DataTable().clear().destroy();
                locationAppend();
                toastr["success"](response.message);

                $('#modalAddressEdit').modal('hide');


            },
            error: function (error) {
                console.log(error);
            }
        });
    }
}
function deleteLocation(id)
{
    swal({
            title:'Are you sure?',
            text: "Delete Record.",
            type: "error",
            showCloseButton: true,
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Delete!",
            cancelButtonText: "Cancel!",
            closeOnConfirm: false,
            customClass: "confirm_class",
            closeOnCancel: false,
        },
        function(isConfirm){
            console.log( isConfirm);
            if( $('.edit_cms_disable').css('display') != 'none' ) {
                if (isConfirm) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type : 'post',
                        url : '/address/delete',
                        data:{'id':id},
                        success:function(data)
                        {

                            setTimeout(function () {
                                swal({
                                        title: "",
                                        text: "Delete Sucessfully!",
                                        type: "success",
                                        confirmButtonText: "OK"
                                    },

                                    function(isConfirm){

                                        if (isConfirm) {
                                            $('#location_address').DataTable().clear().destroy();
                                            locationAppend();
                                        }
                                    }); }, 500);


                        }
                    });

                }
                else
                {
                    swal.close();
                }
            }
        });
    var active ='<span class="'+custom_class+' label'+$('#are_you_sure_label').attr('data-id')+'" data-id="'+$('#are_you_sure_label').attr('data-id')+'" data-value="'+$('#are_you_sure_label').val()+'" >'+$('#are_you_sure_label').val()+'</span>';
    $('.confirm_class h2').html(active);
    active ='<span class="'+custom_class+' label'+$('#delete_record_label').attr('data-id')+'" data-id="'+$('#delete_record_label').attr('data-id')+'" data-value="'+$('#delete_record_label').val()+'" >'+$('#delete_record_label').val()+'</span>';
    $('.confirm_class p').html(active);
    active ='<span class="'+custom_class+' label'+$('#cancel_label').attr('data-id')+'" data-id="'+$('#cancel_label').attr('data-id')+'" data-value="'+$('#cancel_label').val()+'" >'+$('#cancel_label').val()+'</span>';
    $('.cancel').html(active);
    active ='<span class="'+custom_class+' label'+$('#delete_label').attr('data-id')+'" data-id="'+$('#delete_label').attr('data-id')+'" data-value="'+$('#delete_label').val()+'" >'+$('#delete_label').val()+'</span>';
    $('.confirm').html(active);
}


var selectedrows=[];

$('body').on('click','#addresslocation_checkbox_all',function() {
    $('input:checkbox').not(this).prop('checked', this.checked);
        $('input[type="checkbox"]').each(function(key,value) {
            var id=$(this).attr('id');
            id=id.split('checked');
            row_id='#addressLocation_tr'+id[1];
            if($(this).is(":checked"))
            {
                if(key == 0)
                {
                    selectedrows=[];
                }
                if(id[1] != '_checkbox_all')
                {
                    selectedrows.push(id[1]);
                }
                $('.address_export').val(JSON.stringify(selectedrows));
                $(row_id).addClass('selected');
            }
            else
            {
                $('.address_export').val(1);
                selectedrows=[];
                $(row_id).removeClass('selected');
            }
        });
    // var x = document.getElementsByClassName("selectedRowLocation");
    // var i;
    // for (i = 0; i < x.length;i++) {
    //     var tr_id=x[i].id;
    //     tr_id=tr_id.split('checked');
    //     tr_id=tr_id[1];
    //     if(selectedrows.includes(tr_id))
    //     {
    //         selectedrows.splice( selectedrows.indexOf(tr_id), 1 );
    //         if (selectedrows.length >0){
    //             $('.address_export').val(JSON.stringify(selectedrows));
    //         }else {
    //             $('.address_export').val('1');
    //         }
    //         $('#addressLocation_tr'+tr_id).removeClass('selected');
    //         $('#checked'+tr_id).attr('checked',false);
    //     }
    //     else
    //     {
    //         selectedrows.push(tr_id);
    //         $('.address_export').val(JSON.stringify(selectedrows));
    //         $('#addressLocation_tr'+tr_id).addClass('selected');
    //         $('#checked'+tr_id).attr('checked',true);
    //     }
    // }

});

$('body').on('click', '.selectedRowLocation', function() {
    // var tr_id=$(this).attr('id');
    // tr_id=tr_id.split('checked');
    // tr_id=tr_id[1];
    // if(selectedrows.includes(tr_id))
    // {
    //     selectedrows.splice( selectedrows.indexOf(tr_id), 1 );
    //     if (selectedrows.length >0){
    //         $('.address_export').val(JSON.stringify(selectedrows));
    //     }else {
    //         $('.address_export').val('1');
    //     }
    //     $('#addressLocation_tr'+tr_id).removeClass('selected');
    //     $('#checked'+tr_id).attr('checked',false);
    // }
    // else
    // {
    //     selectedrows.push(tr_id);
    //     $('.address_export').val(JSON.stringify(selectedrows));
    //     $('#addressLocation_tr'+tr_id).addClass('selected');
    //     $('#checked'+tr_id).attr('checked',true);
    // }
    var id=$(this).attr('id');
        id=id.split('checked');
        row_id='#addressLocation_tr'+id[1];
        if($(this).is(":checked"))
        {
            selectedrows.push(id[1]);
            $('.address_export').val(JSON.stringify(selectedrows));
            $(row_id).addClass('selected');
        }else
        {
            if(selectedrows.includes(id[1]))
            {
                selectedrows.splice( selectedrows.indexOf(id[1]),1);
                $('.address_export').val(JSON.stringify(selectedrows));
                $(row_id).removeClass('selected');
            }
        }
});


function selectedRowLocation(id,checked=null)
{
    console.log(id);
    if (checked!==null){
        var tr_id=id.split('checked');
        id=tr_id[1];
    }else{
        id=id;
    }
    if(selectedrows.includes(id))
    {
        selectedrows.splice( selectedrows.indexOf(id), 1 );
        $('.address_export').val(JSON.stringify(selectedrows));
        $('#location_address tr#'+id).removeClass('selected');
        $('#checked'+id).attr('checked',true);
    }
    else
    {
        selectedrows.push(id);
        $('.address_export').val(JSON.stringify(selectedrows));
        $('#location_address tr#'+id).addClass('selected');
        $('#checked'+id).attr('checked',false);
    }

}

$('#deleteSelectedAddressLocation').click(function(){
    if( $('.edit_cms_disable').css('display') == 'none' ) {
        console.log('Editer mode On Please change your mode');
        return 0;
    }
    if (selectedrows && selectedrows.length)
    {
        swal({
                title: "Are you sure?",
                text: "Locations will be inactive and can be Active again by the admin only!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:'/address/multipleDelete',
                        data: {'ids':selectedrows,'flag': $('#flagAddress').val()},
                        success: function(response)
                        {
                            $('#location_address').DataTable().clear().destroy();
                            if($('#flagAddress').val()==1)
                            {
                                locationAppend();
                            }
                            else
                            {
                                locationSoftAppend();
                            }
                            swal.close();
                            toastr["success"]("Locations Deleted Successfully!");
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
                } else {
                    swal("Cancelled", "Your Record is safe", "error");
                    e.preventDefault();
                }
            });
    }
    else
    {
        return toastr["error"]("Select at least one record to delete!");
    }
});

$('#selected_active_button_address').click(function(){
    if( $('.edit_cms_disable').css('display') == 'none' ) {
        console.log('Editer mode On Please change your mode');
        return 0;
    }
    if (selectedrows && selectedrows.length)
    {
        swal({
                title: "Are you sure?",
                text: "Locations will be inactive and can be Active again by the admin only!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:'/address/multipleActive',
                        data: {'ids':selectedrows,'flag': $('#flagAddress').val()},
                        success: function(response)
                        {
                            $('#location_address').DataTable().clear().destroy();
                            locationSoftAppend();
                            swal.close();
                            toastr["success"]("Locations Deleted Successfully!");
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
                } else {
                    swal("Cancelled", "Your Record is safe", "error");
                    e.preventDefault();
                }
            });
    }
    else
    {
        return toastr["error"]("Select at least one record to activate!");
    }
});

function locationSoftAppend() {
    var table = $('#location_address').dataTable({
        processing: true,
        bInfo:false,
        language: {
            'lengthMenu': '  _MENU_ ',
            'search':'<span class="assign_class label'+$('#search_label').attr('data-id')+'" data-id="'+$('#search_label').attr('data-id')+'" data-value="'+$('#search_label').val()+'" >'+$('#search_label').val()+'</span>',
            'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
            'info': ' _PAGE_ <span class="assign_class label'+$('#of_label').attr('data-id')+'" data-id="'+$('#of_label').attr('data-id')+'" data-value="'+$('#of_label').val()+'" >'+$('#of_label').val()+'</span> _PAGES_',
            'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
            'paginate': {
                'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
            },
            'infoFiltered': "(filtered from _MAX_ total records)"
        },
        "ajax": {
            "url": '/address/getAllSoftLocations',
            "type": 'get',
        },
        "createdRow": function( row, data, dataIndex,columns ) {
            var checkbox='<div class="form-check"><input type="checkbox"  class="form-check-input selectedRowLocation address_checked" id="checked'+data.id+'"><label class="form-check-label" for="checked'+data.id+'"></label></div>';
            $(columns[0]).html(checkbox);
            console.log(data);
            $(row).attr('id', 'addressLocation_tr'+data['id']);
            var temp=data['id'];
            // $(row).attr('onclick',"selectedRowLocation(this.id)");
        },
        columns: [
            {data: 'checkbox', name: 'checkbox',visible:$('#addressType_checkbox').val()},
            {data: 'name', name: 'name',visible:$('#addressType_building').val()},
            {data: 'address_street', name: 'address_street',visible:$('#addressType_addresstype').val()},
            {data: 'telephone', name: 'telephone',visible:$('#addressType_phone').val()},
            {data: 'email', name: 'email',visible:$('#addressType_email').val()},
            {data: 'post_code', name: 'post_code',visible:$('#addressType_postcode').val()},
            {data: 'actions', name: 'actions'},
        ],
        "columnDefs": [ {
            "targets": [0,1],
            "orderable": false
        } ]

    });
    if ($(":checkbox").prop('checked',true)){
        $(":checkbox").prop('checked',false);
    }
}
$('#restore_button_address').click(function(){
    if( $('.edit_cms_disable').css('display') == 'none' ) {
        console.log('Editer mode On Please change your mode');
        return 0;
    }
    selectedrows=[];
    $('#export_excel_address_btn').hide();
    $('#export_word_address_btn').hide();
    $('#export_pdf_address_btn').hide();
    $('#restore_button_address').hide();
    $('#export_excel_address').val(2);
    $('#export_word_address').val(2);
    $('#export_pdf_address').val(2);
    $('#flagAddress').val(2);
    $('#show_active_button_address').show();
    $('#selected_active_button_address').show();
    $('#location_address').DataTable().clear().destroy();
    locationSoftAppend();
});

$('.address_listing_close').click(function(){
    $.ajax({
        type: "GET",
        url: '/address/addresstype/listing',
        success: function(response)
        {
            var html='';
            html+='<option value="" selected disabled>Please Select</option>';
            for (var i = response.addressType.length - 1; i >= 0; i--) {
                html+='<option value="'+response.addressType[i].id+'">'+response.addressType[i].name+'</option>';
            }
            $('#address_type_id').empty();
            $('#address_type_id').html(html);

        },
        error: function (error) {
            console.log(error);
        }
    });
});

$('#show_active_button_address').click(function(){
    if( $('.edit_cms_disable').css('display') == 'none' ) {
        console.log('Editer mode On Please change your mode');
        return 0;
    }
    selectedrows=[];
    $('#export_excel_address_btn').show();
    $('#export_word_address_btn').show();
    $('#export_pdf_address_btn').show();
    $('#show_active_button_address').hide();
    $('#export_excel_address').val(1);
    $('#export_word_address').val(1);
    $('#export_pdf_address').val(1);
    $('#flagAddress').val(1);
    $('#restore_button_address').show();
    $('#selected_active_button_address').hide();
    $('#location_address').DataTable().clear().destroy();
    locationAppend();
});

function restoreLocation(id)
{
    swal({
            title: "Are you sure?",
            text: "Selected Location will be Active again!",
            type: "warning",
            showCloseButton: true,
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            customClass: "confirm_class",
            closeOnCancel: false
        },
        function(isConfirm){
            if( $('.edit_cms_disable').css('display') != 'none' ) {
                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:'/address/restore',
                        data: {'id':id},
                        success: function(response)
                        {
                            $('#location_address').DataTable().clear().destroy();
                            locationSoftAppend();
                            swal.close();
                            toastr["success"]("Location Activated Successfully!");
                            toastr.options = {
                                "closeButton": false,
                                "debug": false,
                                "newestOnTop": false,
                                "progressBar": false,
                                "positionClass": "toast-top-center",
                                "preventDuplicates": false,
                                "onclick": null,
                                "showDuration": "300",
                                "hideDuration": "1000",
                                "timeOut": "5000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            };
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
                }
                else {
                    swal.close();
                    // swal("Cancelled", "Your Record is safe", "error");
                    // e.preventDefault();
                }
            }
        });
    var active ='<span class="'+custom_class+' label'+$('#are_you_sure_label').attr('data-id')+'" data-id="'+$('#are_you_sure_label').attr('data-id')+'" data-value="'+$('#are_you_sure_label').val()+'" >'+$('#are_you_sure_label').val()+'</span>';
    $('.confirm_class h2').html(active);
    active ='<span class="'+custom_class+' label'+$('#selected_user_with_be_active_again_label').attr('data-id')+'" data-id="'+$('#selected_user_with_be_active_again_label').attr('data-id')+'" data-value="'+$('#selected_user_with_be_active_again_label').val()+'" >'+$('#selected_user_with_be_active_again_label').val()+'</span>';
    $('.confirm_class p').html(active);
    active ='<span class="'+custom_class+' label'+$('#cancel_label').attr('data-id')+'" data-id="'+$('#cancel_label').attr('data-id')+'" data-value="'+$('#cancel_label').val()+'" >'+$('#cancel_label').val()+'</span>';
    $('.cancel').html(active);
    active ='<span class="'+custom_class+' label'+$('#delete_label').attr('data-id')+'" data-id="'+$('#delete_label').attr('data-id')+'" data-value="'+$('#delete_label').val()+'" >'+$('#delete_label').val()+'</span>';
    $('.confirm').html(active);

}
function hardDeleteLocation(id)
{
    swal({
            title: "Are you sure?",
            text: "Selected Location with be Hard Deleted and can active from backend again!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){
            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: '/address/hardDelete',
                    data: {'id':id},
                    success: function(response)
                    {
                        $('#location_address').DataTable().clear().destroy();
                        locationSoftAppend();
                        swal.close()
                        toastr["success"]("Location Hard Deleted Successfully!");
                        toastr.options = {
                            "closeButton": false,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-center",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        };
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            } else {
                swal("Cancelled", "Your Record is safe", "error");
                e.preventDefault();
            }
        });
}
