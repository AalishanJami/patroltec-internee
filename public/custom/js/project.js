$(document).ready(function() {
    var selectedrows=[];
    $('#registernewcompany').submit(function () {
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        event.preventDefault();
        var name=$('#new_company').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: '/company/store',
            data: {'name':name},
            success: function(response)
            {
                $('#company_table').DataTable().clear().destroy();
                companyAppend();
                toastr["success"]("New company Created Successfully!");
                $('#modalRegisterCompany').modal('hide');

            },
            error: function (error) {
                console.log(error);
            }
        });
    });
    $('#edit_company_name').submit(function () {
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        event.preventDefault();
        var name=$('#edit_user_name').val();
        var id=$('#user_id_hidden').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: '/company/update',
            data: {'id':id,'name':name},
            success: function(response)
            {
                $('#company_table').DataTable().clear().destroy();
                companyAppend();
                toastr.success("Record Updated Successfully!");
                // $('#modalEditForm').modal('hide');

            },
            error: function (error) {
                console.log(error);
            }
        });});

    $('#showactivebuttoncompany').click(function(){
        $('#export_excel_company').val(1);
        $('#export_word_company').val(1);

        $('#restorebutton').show();
        $('#showactivebuttoncompany').hide();
        $('#company_table').DataTable().clear().destroy();
        companyAppend();
    });

    $('#restorebutton').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        $('#restorebutton').hide();
        $('#export_excel_company').val(2);
        $('#export_word_company').val(2);
        $('#showactivebuttoncompany').show();
        $('#company_table').DataTable().clear().destroy();

        var table = $('#company_table').dataTable({
            processing: true,
            language: {
                'lengthMenu': '<span class="assign_class label'+$('#show_label').attr('data-id')+'" data-id="'+$('#show_label').attr('data-id')+'" data-value="'+$('#show_label').val()+'" >'+$('#show_label').val()+'</span>  _MENU_ <span class="assign_class label'+$('#entries_label').attr('data-id')+'" data-id="'+$('#entries_label').attr('data-id')+'" data-value="'+$('#entries_label').val()+'" >'+$('#entries_label').val()+'</span>',
                'search':'<span class="assign_class label'+$('#search_label').attr('data-id')+'" data-id="'+$('#search_label').attr('data-id')+'" data-value="'+$('#search_label').val()+'" >'+$('#search_label').val()+'</span>',
                'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
                'info': '<span class="assign_class label'+$('#showing_page_label').attr('data-id')+'" data-id="'+$('#showing_page_label').attr('data-id')+'" data-value="'+$('#showing_page_label').val()+'" >'+$('#showing_page_label').val()+'</span> _PAGE_ <span class="assign_class label'+$('#of_label').attr('data-id')+'" data-id="'+$('#of_label').attr('data-id')+'" data-value="'+$('#of_label').val()+'" >'+$('#of_label').val()+'</span> _PAGES_',
                'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
                'paginate': {
                    'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                    'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
                },
                'infoFiltered': "(filtered from _MAX_ total records)"
            },

            "ajax": {
                "url": '/company/soft',
                "type": 'get',
            },
            "createdRow": function( row, data, dataIndex ) {
                $(row).attr('id', data['id']);
                var temp=data['id'];
                $(row).attr('onclick',"selectedrow(this.id)");
            },
            columns: [
                {data: 'checkbox', name: 'checkbox',visible:$('#checkbox_permission').val(),},
                {data: 'name', name: 'name',visible:$('#name_permission').val(),},
                {data: 'actions', name: 'actions'},

            ],
            columnDefs: [{
                orderable: false,
                className: 'select-checkbox',
                targets: 0
            }],
            select: {
                style: 'os',
                selector: 'td:first-child'
            },

        });
        console.log(table);
    });
});
function getcompanydata (id)
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type : 'post',
        url : '/company',
        data:{'id':id},
        success:function(data)
        {
            // console.log(data);
            $('#edit_user_name').val(data.name);
            $('#user_id_hidden').val(data.id);

        }
    });


}

function deletecompanydata(id)
{
    var custom_class='';
    if( $('.edit_cms_disable').css('display') == 'none' ) {
        custom_class='assign_class get_text';
    }
    else
    {
        custom_class='assign_class';
    }
    console.log(custom_class);
    swal({
            title:'Are you sure?',
            text: "Delete Record.",
            type: "error",
            showCloseButton: true,
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Delete!",
            cancelButtonText: "Cancel!",
            closeOnConfirm: false,
            customClass: "confirm_class",
            closeOnCancel: false,
        },
        function(isConfirm){
            console.log( isConfirm);
            if( $('.edit_cms_disable').css('display') != 'none' ) {
                if (isConfirm) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type : 'post',
                        url : '/company/delete',
                        data:{'id':id},
                        success:function(data)
                        {

                            setTimeout(function () {
                                swal({
                                        title: "",
                                        text: "Delete Sucessfully!",
                                        type: "success",
                                        confirmButtonText: "OK"
                                    },

                                    function(isConfirm){

                                        if (isConfirm) {
                                            $('#company_table').DataTable().clear().destroy();
                                            companyAppend();
                                        }
                                    }); }, 500);


                        }
                    });

                }
                else
                {
                    swal.close();
                }
            }
        });

    var active ='<span class="'+custom_class+' label'+$('#are_you_sure_label').attr('data-id')+'" data-id="'+$('#are_you_sure_label').attr('data-id')+'" data-value="'+$('#are_you_sure_label').val()+'" >'+$('#are_you_sure_label').val()+'</span>';
    $('.confirm_class h2').html(active);
    active ='<span class="'+custom_class+' label'+$('#delete_record_label').attr('data-id')+'" data-id="'+$('#delete_record_label').attr('data-id')+'" data-value="'+$('#delete_record_label').val()+'" >'+$('#delete_record_label').val()+'</span>';
    $('.confirm_class p').html(active);
    active ='<span class="'+custom_class+' label'+$('#cancel_label').attr('data-id')+'" data-id="'+$('#cancel_label').attr('data-id')+'" data-value="'+$('#cancel_label').val()+'" >'+$('#cancel_label').val()+'</span>';
    $('.cancel').html(active);
    active ='<span class="'+custom_class+' label'+$('#delete_label').attr('data-id')+'" data-id="'+$('#delete_label').attr('data-id')+'" data-value="'+$('#delete_label').val()+'" >'+$('#delete_label').val()+'</span>';
    $('.confirm').html(active);
}

function hard_delete_company(id)
{
    swal({
            title: "Are you sure?",
            text: "Selected company with be Hard Deleted and can active from backend again!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){

            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: '/company/hard/delete',
                    data: {'id':id},
                    success: function(response)
                    {
                        $('#company_table').DataTable().clear().destroy();
                        companyAppend();
                        swal.close()
                        toastr["success"]("Company Hard Deleted Successfully!");
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            } else {
                swal("Cancelled", "Your Record is safe", "error");
                e.preventDefault();
            }
        });



}

function restore_company(id)
{
    var custom_class='';
    if( $('.edit_cms_disable').css('display') == 'none' ) {
        custom_class='assign_class get_text';
    }
    else
    {
        custom_class='assign_class';
    }
    swal({
            title: "Are you sure?",
            text: "Selected user with be Active again!",
            type: "warning",
            showCloseButton: true,
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            customClass: "confirm_class",
            closeOnCancel: false
        },
        function(isConfirm){
            if( $('.edit_cms_disable').css('display') != 'none' ) {
                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:'/company/restore',
                        data: {'id':id},
                        success: function(response)
                        {
                            $('#company_table').DataTable().ajax.reload();
                            swal.close()
                            toastr["success"]("User Activated Successfully!");
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
                }
                else {
                    swal.close();
                    // swal("Cancelled", "Your Record is safe", "error");
                    // e.preventDefault();
                }
            }
        });
    var active ='<span class="'+custom_class+' label'+$('#are_you_sure_label').attr('data-id')+'" data-id="'+$('#are_you_sure_label').attr('data-id')+'" data-value="'+$('#are_you_sure_label').val()+'" >'+$('#are_you_sure_label').val()+'</span>';
    $('.confirm_class h2').html(active);
    active ='<span class="'+custom_class+' label'+$('#selected_user_with_be_active_again_label').attr('data-id')+'" data-id="'+$('#selected_user_with_be_active_again_label').attr('data-id')+'" data-value="'+$('#selected_user_with_be_active_again_label').val()+'" >'+$('#selected_user_with_be_active_again_label').val()+'</span>';
    $('.confirm_class p').html(active);
    active ='<span class="'+custom_class+' label'+$('#cancel_label').attr('data-id')+'" data-id="'+$('#cancel_label').attr('data-id')+'" data-value="'+$('#cancel_label').val()+'" >'+$('#cancel_label').val()+'</span>';
    $('.cancel').html(active);
    active ='<span class="'+custom_class+' label'+$('#delete_label').attr('data-id')+'" data-id="'+$('#delete_label').attr('data-id')+'" data-value="'+$('#delete_label').val()+'" >'+$('#delete_label').val()+'</span>';
    $('.confirm').html(active);

}
