 $(document).ready(function() {

                $('#filter_id').on('change', function() {
                          //alert($("option:selected", this).text());
                              $('.type_name').empty();
                              $('.type_name').html($("option:selected", this).text());

                              type(this.value);

                        });

                detail($('#score_table tbody tr:eq(0)').attr('id'));
                 var url='/home/getAllForms';
                  complete_data(url);


 	    });


/*
 function getDataLocationMain()
 {

	var formData = $("#LocationSelect").serializeObject();
	
	console.log(formData);


	$.ajax({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          type: "POST",
          url: '/dashboard/getDataLocationMain',
          data: formData,
          success: function(data)
          {
	           console.log(data);
	           $("#topScore").html(data.maxScore);
	           $("#avgScore").html(data.averageScore);
var lineChartData = { labels:data.dates, datasets: [] },
    array =data.score;

array.forEach(function (a, i) {
	console.log(a,i);
    lineChartData.datasets.push({
        label: 'Label ' + i,
        fillColor: 'rgba(220,220,220,0.2)',
        strokeColor: 'rgba(220,220,220,1)',
        pointColor: 'rgba(220,220,220,1)',
        pointStrokeColor: '#fff',
        pointHighlightFill: '#fff',
        pointHighlightStroke:
        'rgba(220,220,220,1)',
        data: [{
          t: data.cdate[i][0],
          y: a[0]
        }],
    });
});







	           		var ctxL = document.getElementById("lineChart").getContext('2d');
				    var myLineChart = new Chart(ctxL, {
				      type: 'line',
				      data: lineChartData,
				                        options: {
				                          legend: {
				                            labels: {
				                              fontColor: "#fff",
				                            }
				                          },
				                          scales: {
				                            xAxes: [{
			                            	  type: 'time',
				                              gridLines: {
				                                display: true,
				                                color: "rgba(255,255,255,.25)"
				                              },
				                              ticks: {
				                                fontColor: "#fff",
				                              },
				                            }],
				                            yAxes: [{
				                              display: true,
				                              gridLines: {
				                                display: true,
				                                color: "rgba(255,255,255,.25)"
				                              },
				                              ticks: {
				                                fontColor: "#fff",
				                              },
				                            }],
				                          }
				                        }
				    });
          },
          error: function (error) {
              console.log(error);
          }
      });
 }*/



 function detail(id)
 {

 	console.log(id);
  $('#score_table > tbody  > tr').each(function(index, tr) { 
   if($(this).attr('id')==id)
   {
    $(this).attr('style',"background-color: lightgreen");
  }else
  {
    $(this).attr('style',"background-color:null");
  }
});

 //	$("#"+id).attr('style',"background-color: lightgreen");
 	$.ajax({
            type: "GET",
            url: '/home/getLocationData/'+id,
            success: function(response)
            {

              console.log(response);

              var html = '<a type="button" class="btn btn btn-flat grey lighten-3 btn-rounded waves-effect font-weight-bold dark-grey-text float-right btn-dash" href="/project/completed_forms/'+response.location.id+'">View full report</a>'

              $('#location_button').empty();
           $('#location_button').html(html);


           $('#head_score').empty();
           $('#head_score').html("Details : "+response.location.name);

           $('#form_score').empty();
           $('#form_score').html(response.form_score);

           $('#max_form_score').empty();
            $('#max_form_score').html(response.form_score_total);

            $('#answer_score').empty();
            $('#answer_score').html(response.response_total);

            $('#max_anwser_score').empty();
            $('#max_anwser_score').html(response.response_score_total);

            $('#required').empty();
            $('#required').html(response.forms_required);

            $('#completed').empty();
            $('#completed').html(response.forms_completed);

            $('#os').empty();
            $('#os').html(response.outstanding_count);

            $('#late').empty();
            $('#late').html(0);

            $('#action_score').empty();
            $('#action_score').html(response.action_score);

            $('#action_max_score').empty();
            $('#action_max_score').html(response.actions_total_score);

            $('#os_2').empty();
            $('#os_2').html(response.actions_outstanding);

            $('#resolved').empty();
            $('#resolved').html(response.actions_resolved);
             
            },
            error: function (error) {
                console.log(error);
            }
        });

 }