  var form_id=null;
  var today = new Date();
var time = today.getHours() + ":" + today.getMinutes();
 $(document).ready(function() {


        $('#modelviewpopup').on('shown.bs.modal', function (e) {
            $('#qr_listing').empty();
            $('#raiseJob').trigger("reset");
            $('select.job_user_id option:first').attr('disabled', true);
            $('select.job_user_id option:first').attr('selected', true);
            $('select.job_location_id option:first').attr('disabled', true);
            $('select.job_location_id option:first').attr('selected', true);
            // document.getElementById("raiseJob").reset();
        });


        $('#input_starttime').val(time);
        $( '#location_id' ).change(function() {
            location_id=$('#location_id').children("option:selected").val();
            console.log(location_id);
            $.ajax({
                type: "GET",
                url: '/schedule/project/'+location_id+'/'+form_id,
                success: function(response)
                {

               console.log(response.location);
              var html='';
              html+='<select name="location_breakdown_id[]" id="location_breakdown_id" class="mdb-select-custom-location colorful-select dropdown-primary md-form" placeholder="Break Downs"  multiple searchable="Search here.." >';
              html+='<option value="0" disabled disabled>Please Select</option>';
                for (var i = response.location.length - 1; i >= 0; i--) {

                        html+='<option value="'+response.location[i].id+'">'+response.location[i].name+'</option>';
                }

                html+='</select><small id="er_location_breakdown_id" style="left: 0%!important;margin-top:-15px;display:block; text-transform: initial !important;" class="text-danger error_message"></small>';
                if(response.form_type=="t")
                html+='<label class="mdb-main-label active ">QR List</label>';
                if(response.form_type=="e")
                html+='<label class="mdb-main-label active">Equipment List</label>';

                $('#qr_listing').empty();
                $('#break_location').val(response.form_type);
                console.log(html, "html");
                if(response.form_type!="m")
                {

                $('#qr_listing').html(html);
                $('.mdb-select-custom-location').materialSelect();
                }
                $('select.mdb-select-custom-location').hide();
                },
                error: function (error) {
                    console.log(error);
                }
            });
        });

});




function selectedForm(id)
{
    console.log("form Selected")
form_id=id;
$('#raise_job_form_id').val(form_id);
$.ajax({
            type: "GET",
            url: '/job/getPopulate/'+form_id,
            success: function(response)
            {

              console.log(response.populate);
              var html='';
              html+='<select name="pre_populate_id" class="mdb-select mdb-select-custom-populate" id="pre_populate_id" placeholder="populate" >';
            html+='<option value="" selected disabled>Pre-Populate</option>';

                for (var i = response.populate.length - 1; i >= 0; i--) {

                        html+='<option value="'+response.populate[i].id+'">'+response.populate[i].name+'</option>';
                }

                html+='</select> ';

                $('#populate_div').empty();
                console.log(html, "html");
                $('#populate_div').html(html);
                $('#pre_populate_id').materialSelect();
                $('select.mdb-select-custom-populate').hide();
                 $('#populate_div').show();

            },
            error: function (error) {
                console.log(error);
            }
        });
}
