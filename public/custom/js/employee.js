 $(document).ready(function() {
    var selectedrows=[];
     $('body').on('click','#trainingmatrix_checkbox_all',function() {
         $('.selectedrowtraning').not(this).prop('checked', this.checked);
         $('.selectedrowtraning').each(function(key,value) {
             var id=$(this).attr('id');
             console.log(id);
             if(id)
             {
                 id=id.split('traning_checked');
                 row_id='#traning_tr'+id[1];
                 if($(this).is(":checked"))
                 {
                     if(key == 0)
                     {
                         selectedrows=[];
                     }
                     if(id[1] != '_checkbox_all')
                     {
                         selectedrows.push(id[1]);
                     }
                     $('.matrix_export_pdf').val(JSON.stringify(selectedrows));
                     $(row_id).addClass('selected');
                 }
                 else
                 {
                     $('.matrix_export_pdf').val(1);
                     selectedrows=[];
                     $(row_id).removeClass('selected');
                 }
             }
         });
     });
     $('body').on('click', '.selectedrowtraning', function() {
         var id=$(this).attr('id');
         id=id.split('traning_checked');
         row_id='#traning_tr'+id[1];
         if($(this).is(":checked"))
         {
             selectedrows.push(id[1]);
             $('.matrix_export_pdf').val(JSON.stringify(selectedrows));
             $(row_id).addClass('selected');
         }else
         {
             if(selectedrows.includes(id[1]))
             {
                 selectedrows.splice( selectedrows.indexOf(id[1]),1);
                 $('.matrix_export_pdf').val(JSON.stringify(selectedrows));
                 $(row_id).removeClass('selected');
             }
         }
     });


     $('body').on('click','#employee_checkbox_all',function() {
         $('input:checkbox').not(this).prop('checked', this.checked);
         $('input[type="checkbox"]').each(function(key,value) {
             var id=$(this).attr('id');
             console.log(id);
             if(id)
             {
                 id=id.split('employee_check');
                 row_id='#employee_tr'+id[1];
                 if($(this).is(":checked"))
                 {
                     if(key == 0)
                     {
                         selectedrows=[];
                     }
                     if(id[1] != '_checkbox_all')
                     {
                         selectedrows.push(id[1]);
                     }
                     $('.employee_export').val(JSON.stringify(selectedrows));
                     $(row_id).addClass('selected');
                 }
                 else
                 {
                     $('.employee_export').val(1);
                     selectedrows=[];
                     $(row_id).removeClass('selected');
                 }
             }
         });
     });

     $('body').on('click', '.selectedrowemployee', function() {
         var id=$(this).attr('id');
         id=id.split('employee_check');
         row_id='#employee_tr'+id[1];
         if($(this).is(":checked"))
         {
             selectedrows.push(id[1]);
             $('.employee_export').val(JSON.stringify(selectedrows));
             $(row_id).addClass('selected');
         }else
         {
             if(selectedrows.includes(id[1]))
             {
                 selectedrows.splice( selectedrows.indexOf(id[1]),1);
                 $('.employee_export').val(JSON.stringify(selectedrows));
                 $(row_id).removeClass('selected');
             }
         }
    });

     $('#selectedactivebuttonemployee').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        var url='/employee/active/multiple';
        if (selectedrows && selectedrows.length) {
       // not empty
            swal({
                title: "Are you sure?",
                text: "Selected option will be active  again ..!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
             },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:url,
                        data: {'ids':selectedrows},
                        success: function(response)
                        {

                            toastr["success"](response.message);
                            var url='/employee/getallSoft';
                            $('#employee_table').DataTable().clear().destroy();
                            employee_data(url);
                            swal.close();

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                  swal("Cancelled", "Your Record is safe", "error");

                }
            });
         }
        else
        {
       // empty
            return toastr["error"]("Select at least one record to delete!");
        }
    });
    $('#deleteselectedemployee').click(function(){
       var url='/employee/delete/multiple';
       delete_mutilpleDataEmployee(url,selectedrows);
    });
    $('#deleteselectedemployeeSoft').click(function(){
       var url='/employee/delete/soft/multiple';
       delete_mutilpleDataEmployee(url,selectedrows);
    });
    $('#restorebuttonemployee').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        $('#export_excel_employee_btn').hide();
        $('#export_world_employee_btn').hide();
        $('#export_pdf_employee_btn').hide();
        selectedrows=[];
        $('#restorebuttonemployee').hide();
        $('#deleteselectedemployee').hide();
        $('#selectedactivebuttonemployee').show();
        $('#deleteselectedemployeeSoft').show();
        $('#export_excel_employee').val(2);
        $('#export_world_employee').val(2);
        $('#export_world_pdf').val(2);
        $('.employee_add').hide();
        $('#activebuttonemployee').show();
        var url='/employee/getallSoft';
        $('#employee_table').DataTable().clear().destroy();
        employee_data(url);
    });
    $('#activebuttonemployee').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        $('#export_excel_employee_btn').show();
        $('#export_world_employee_btn').show();
        $('#export_pdf_employee_btn').show();
        selectedrows=[];
        $('#export_excel_employee').val(1);
        $('#export_world_employee').val(1);
        $('#export_world_pdf').val(1);
        $('.employee_add').show();
        $('#selectedactivebuttonemployee').hide();
        $('#deleteselectedemployee').show();
        $('#deleteselectedemployeeSoft').hide();
        $('#restorebuttonemployee').show();
        $('#activebuttonemployee').hide();
        var url='/employee/getall';
        $('#employee_table').DataTable().clear().destroy();
        employee_data(url);
    });
});
 function restoreemployeedata(id)
 {
    var url='/employee/active';
    swal({
        title: "Are you sure?",
        text: "Selected option with be active ",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
     },
     function(isConfirm){

       if (isConfirm){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
             $.ajax({
                        type: "POST",
                        url: url,
                        data: {'id':id},
                        success: function(response)
                        {
                            if(response.flag==1)
                            {
                                var url='/employee/getall';
                            }
                            else
                            {
                                var url='/employee/getallSoft';
                            }
                            $('#employee_table').DataTable().clear().destroy();
                            employee_data(url);
                            swal.close();
                            toastr["success"](response.message);
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
        } else {
          swal("Cancelled", "Your Record is safe", "error");

        }
     });
 }
function delete_mutilpleDataEmployee(url,selectedrows)
{
    if( $('.edit_cms_disable').css('display') == 'none' ) {
        console.log('Editer mode On Please change your mode');
        return 0;
    }

    if (selectedrows && selectedrows.length) {
   // not empty
        swal({
            title: "Are you sure?",
            text: "Selected option will be inactive and can be Active again by the admin only!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
         },
        function(isConfirm){

            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url:url,
                    data: {'ids':selectedrows},
                    success: function(response)
                    {

                        toastr["success"](response.message);
                        if(response.flag==1)
                        {
                            var url='/employee/getall';
                        }
                        else
                        {
                            var url='/employee/getallSoft';
                        }
                        $('#employee_table').DataTable().clear().destroy();
                        employee_data(url);
                        swal.close();

                    },
                    error: function (error) {
                        console.log(error);
                    }
                });

            } else {
              swal("Cancelled", "Your Record is safe", "error");

            }
        });
     }
    else
    {
   // empty
        return toastr["error"]("Select at least one record to delete!");
    }
}
 function delete_data_employee(id,url)
 {
    swal({
        title: "Are you sure?",
        text: "Selected option with be Deleted ",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
     },
     function(isConfirm){

       if (isConfirm){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
             $.ajax({
                        type: "POST",
                        url: url,
                        data: {'id':id},
                        success: function(response)
                        {
                            if(response.flag==1)
                            {
                                var url='/employee/getall';
                            }
                            else
                            {
                                var url='/employee/getallSoft';
                            }
                            $('#employee_table').DataTable().clear().destroy();
                            employee_data(url);
                            swal.close();
                            toastr["success"](response.message);
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
        } else {
          swal("Cancelled", "Your Record is safe", "error");

        }
     });
 }
function deletesoftemployeedata(id)
{
    var url='/employee/softdelete';
    delete_data_employee(id,url);

}
function deleteemployeedata(id)
 {
    var url='/employee/delete';
    delete_data_employee(id,url);
 }
