function form_data(url)
{
    var table = $('#document').dataTable({
        processing: true,
        "bStateSave": true,
        language: {
            'lengthMenu': '<span class="assign_class label'+$('#show_label').attr('data-id')+'" data-id="'+$('#show_label').attr('data-id')+'" data-value="'+$('#show_label').val()+'" ></span>  _MENU_ <span class="assign_class label'+$('#entries_label').attr('data-id')+'" data-id="'+$('#entries_label').attr('data-id')+'" data-value="'+$('#entries_label').val()+'" ></span>',
            'search':'<span class="assign_class label'+$('#search_id').attr('data-id')+'" data-id="'+$('#search_id').attr('data-id')+'" data-value="'+$('#search_id').val()+'" >'+$('#search_id').val()+'</span>',
            'zeroRecords': ' <span class="assign_class label'+$('#nothing_found_sorry_label').attr('data-id')+'" data-id="'+$('#nothing_found_sorry_label').attr('data-id')+'" data-value="'+$('#nothing_found_sorry_label').val()+'" >'+$('#nothing_found_sorry_label').val()+'</span>',
            'info': '<span class="assign_class label'+$('#showing_page_label').attr('data-id')+'" data-id="'+$('#showing_page_label').attr('data-id')+'" data-value="'+$('#showing_page_label').val()+'" >'+$('#showing_page_label').val()+'</span> _PAGE_ <span class="assign_class label'+$('#of_label').attr('data-id')+'" data-id="'+$('#of_label').attr('data-id')+'" data-value="'+$('#of_label').val()+'" >'+$('#of_label').val()+'</span> _PAGES_',
            'infoEmpty': '<span class="assign_class label'+$('#no_records_available_label').attr('data-id')+'" data-id="'+$('#no_records_available_label').attr('data-id')+'" data-value="'+$('#no_records_available_label').val()+'" >'+$('#no_records_available_label').val()+'</span>',
            'paginate': {
                'previous': '<span class="assign_class label'+$('#previous_label').attr('data-id')+'" data-id="'+$('#previous_label').attr('data-id')+'" data-value="'+$('#previous_label').val()+'" >'+$('#previous_label').val()+'</span>',
                'next': '<span class="assign_class label'+$('#next_label').attr('data-id')+'" data-id="'+$('#next_label').attr('data-id')+'" data-value="'+$('#next_label').val()+'" >'+$('#next_label').val()+'</span>',
            },
            'infoFiltered': " "
        },
        "ajax": {
            "url": url,
            "type": 'get',
        },
        "createdRow": function( row, data, dataIndex,columns )
        {
            var checkbox='';
            checkbox='<div class="form-check"><input type="checkbox" data-id="'+data['id']+'" class="form-check-input form_checked selectedrowform" id="form_file'+data.id+'"><label class="form-check-label" for="form_file'+data.id+'"></label></div>';
            $(columns[0]).html(checkbox);
            $(columns[1]).attr('id', 'ref'+data['id']);
            // $(columns[1]).attr('class', 'edit_inline_form');
            // $(columns[1]).attr('Contenteditable', 'true');
            $(columns[2]).attr('id', 'name'+data['id']);
            // $(columns[2]).attr('class', 'edit_inline_form');
            // $(columns[2]).attr('Contenteditable', 'true');
            var date='<input placeholder="Selected date" value="'+data['valid_to']+'" type="date" id="valid_to'+data['id']+'"  class="form-control datepicker">';
            $(columns[3]).html(date);
            $(columns[3]).attr('id', 'valid_to'+data['id']);
            // $(columns[3]).attr('class', 'edit_inline_form');
            $(row).attr('id', 'form_tr'+data['id']);
            $(row).attr('data-id', data['id']);
            //$(row).attr('class', 'selectedrowform');
        },
        columns: [
            {data:'checkbox', name:'checkbox',visible:$('#documentlibrary_checkbox').val()},
            {data:'ref', name:'ref',visible:$('#documentlibrary_ref').val()},
            {data:'name', name:'name',visible:$('#documentlibrary_name').val()},
            {data:'valid_to', name:'valid_to',visible:$('#documentlibrary_validTo').val()},
            {data:'actions', name:'actions'},
        ],
        columnDefs: [ {
            'targets': [0,4], /* column index */
            'orderable': false, /* true or false */
        }],
    });
    if ($(":checkbox").prop('checked',true)){
        $(":checkbox").prop('checked',false);
    }
}
