$(document).ready(function() {
    var selectedrows=[];
    $('body').on('click','#ethnic_checkbox_all',function() {
        $('.selectedrowethinic').not(this).prop('checked', this.checked);
        $('.selectedrowethinic').each(function(key,value) {
            var id=$(this).attr('id');
            console.log(id);
            if(id)
            {
                id=id.split('ethinic_checked');
                row_id='#ethinic_tr'+id[1];
                if($(this).is(":checked"))
                {
                    if(key == 0)
                    {
                        selectedrows=[];
                    }
                    if(id[1] != '_checkbox_all')
                    {
                        selectedrows.push(id[1]);
                    }
                    $('.ethinic_export').val(JSON.stringify(selectedrows));
                    $(row_id).addClass('selected');
                }
                else
                {
                    $('.ethinic_export').val(1);
                    selectedrows=[];
                    $(row_id).removeClass('selected');
                }
            }
        });
    });
    $('body').on('click', '.selectedrowethinic', function() {
        var id=$(this).attr('id');
        id=id.split('ethinic_checked');
        row_id='#ethinic_tr'+id[1];
        if($(this).is(":checked"))
        {
            selectedrows.push(id[1]);
            $('.ethinic_export').val(JSON.stringify(selectedrows));
            $(row_id).addClass('selected');
        }else
        {
            if(selectedrows.includes(id[1]))
            {
                selectedrows.splice( selectedrows.indexOf(id[1]),1);
                $('.ethinic_export').val(JSON.stringify(selectedrows));
                $(row_id).removeClass('selected');
            }
        }
    });
    $('body').on('click', '.addethnicModal', () => {
        $.ajax({
            type: 'GET',
            url: '/employee/ethinic/store',
            success: function(response)
            {
               var html='';
                html+='<tr id="ethinic_tr'+response.data.id+'" class="selectedrowethinic'+response.data.id+'" role="row" class="hide odd">';
                html+='<td>';
                if ($("#employeeEthnic_checkbox").val()==1){
                    html+='<div class="form-check"><input type="checkbox" class="form-check-input ethinic_checked selectedrowethinic" id="ethinic_checked'+response.data.id+'"><label class="form-check-label" for="ethinic_checked'+response.data.id+'""></label></div>'
                }
                html+='</td>';
                if ($("#employeeEthnic_name").val()==1){
                    var n_create = ($("#edit_employeeEthnic").val()==1 && $("#employeeEthnic_name_create").val()==1)? "true":"false";
                    html+='<td class="pt-3-half edit_inline_ethinic" onkeypress="editSelectedRow('+"ethinic_tr"+response.data.id+')" data-id="'+response.data.id+'" id="ethinic_name'+response.data.id+'"   contenteditable="'+n_create+'"></td>';
                }
                html+='<td>';
                if ($("#delete_employeeEthnic").val()==1){
                    html+='<a type="button" class="btn deleteethinicdata'+response.data.id+' btn-danger btn-xs my-0 waves-effect waves-light" onclick="deleteethinicdata( ' +response.data.id+')"><i class="fas fa-trash"></i></a>'
                }
                if ($("#edit_employeeEthnic").val()==1){
                    html+='<a type="button" class="btn btn-primary all_action_btn_margin btn-xs my-0 waves-effect waves-light saveethinicdata ethinic_show_button_'+response.data.id+'" style="display:none;"  id="'+response.data.id+'"><i class="fas fa-check"></i></a>';
                }
                html+='</td>';
                html+='</tr>';
                $('#table_ethinic').find('table').prepend(html);
            },
            error: function (error) {
                console.log(error);
            }
        });


    });
    $('#selectedactivebuttonethinic').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        var url='/employee/ethinic/active/multiple';
        if (selectedrows && selectedrows.length) {
       // not empty
            swal({
                title: "Are you sure?",
                text: "Selected option will be active  again ..!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
             },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:url,
                        data: {'ids':selectedrows},
                        success: function(response)
                        {

                            toastr["success"](response.message);
                            var url='/employee/ethinic/getallSoft';
                            $('#ethnic_table').DataTable().clear().destroy();
                            ethinic_data(url);
                            swal.close();

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                  swal("Cancelled", "Your Record is safe", "error");

                }
            });
         }
        else
        {
       // empty
            return toastr["error"]("Select at least one record to delete!");
        }
    });
    $('#deleteselectedethinic').click(function(){
       var url='/employee/ethinic/delete/multiple';
       delete_mutilpleDataethinic(url,selectedrows);
    });
    $('#deleteselectedethinicSoft').click(function(){
       var url='/employee/ethinic/delete/soft/multiple';
       delete_mutilpleDataethinic(url,selectedrows);
    });
    $('#restorebuttonethinic').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        $('#export_excel_ethinic_btn').hide();
        $('#export_world_ethinic_btn').hide();
        $('#export_pdf_ethinic_btn').hide();
        $('#restorebuttonethinic').hide();
        $('#deleteselectedethinic').hide();
        $('#selectedactivebuttonethinic').show();
        $('#deleteselectedethinicSoft').show();
        $('.addethinicModal').hide();
        selectedrows=[];
        $('#activebuttonethinic').show();
        var url='/employee/ethinic/getallSoft';
        $('#ethnic_table').DataTable().clear().destroy();
        ethinic_data(url);
    });
    $('#activebuttonethinic').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        $('#export_excel_ethinic_btn').show();
        $('#export_world_ethinic_btn').show();
        $('#export_pdf_ethinic_btn').show();
        $('.addethinicModal').show();
        selectedrows=[];
        $('#selectedactivebuttonethinic').hide();
        $('#deleteselectedethinic').show();
        $('#deleteselectedethinicSoft').hide();
        $('#restorebuttonethinic').show();
        $('#activebuttonethinic').hide();
        var url='/employee/ethinic/getall';
        $('#ethnic_table').DataTable().clear().destroy();
        ethinic_data(url);
    });
    $('body').on('click', '.saveethinicdata', function() {
        var company_id=$('#companies_selected_nav').children("option:selected").val();
        var tr_id=$(this).attr('id');
        var data={
            'id':tr_id,
            'name':$('#ethinic_name'+tr_id).html(),
            'company_id':company_id,
        };
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url:'/employee/ethinic/update',
            data:data,
            success: function(response)
            {
                if(response.error)
                {
                    toastr["error"](response.error);
                    $("#ethinic_tr"+tr_id).attr('style','background-color: #fc685f');
                }
                else
                {
                    $("#ethinic_tr"+tr_id).attr('style','background-color: lightgreen !important');
                    toastr["success"](response.message);
                    $(".ethinic_show_button_"+tr_id).hide();
                    $('.deleteethinicdata'+tr_id).show();

                }
            }
        });
        // console.log(data);
    });
});
function restoreethinicdata(id)
{
    var url='/employee/ethinic/active';
    swal({
        title: "Are you sure?",
        text: "Selected option with be active ",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
     },
     function(isConfirm){

       if (isConfirm){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
             $.ajax({
                type: "POST",
                url: url,
                data: {'id':id},
                success: function(response)
                {
                    if(response.flag==1)
                    {
                        var url='/employee/ethinic/getall';
                    }
                    else
                    {
                        var url='/employee/ethinic/getallSoft';
                    }
                    $('#ethnic_table').DataTable().clear().destroy();
                    ethinic_data(url);
                    swal.close();
                    toastr["success"](response.message);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        } else {
          swal("Cancelled", "Your Record is safe", "error");

        }
     });
}
function delete_data_ethinic(id,url)
{
    swal({
            title: "Are you sure?",
            text: "Selected option will be Deleted ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){
            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {'id':id},
                    success: function(response)
                    {
                        $('#ethnic_table').DataTable().clear().destroy();
                        var url='/employee/ethinic/getall';
                        ethinic_data(url);
                        swal.close();
                        toastr["success"](response.message);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            } else {
                swal("Cancelled", "Your Record is safe", "error");

            }
        });
}

function delete_mutilpleDataethinic(url,selectedrows)
{
    if( $('.edit_cms_disable').css('display') == 'none' ) {
        console.log('Editer mode On Please change your mode');
        return 0;
    }

    if (selectedrows && selectedrows.length) {
   // not empty
        swal({
            title: "Are you sure?",
            text: "Selected option will be inactive and can be Active again by the admin only!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
         },
        function(isConfirm){

            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url:url,
                    data: {'ids':selectedrows},
                    success: function(response)
                    {

                        toastr["success"](response.message);
                        if(response.flag==1)
                        {
                            var url='/employee/ethinic/getall';
                        }
                        else
                        {
                            var url='/employee/ethinic/getallSoft';
                        }
                        $('#ethnic_table').DataTable().clear().destroy();
                        ethinic_data(url);
                        swal.close();

                    },
                    error: function (error) {
                        console.log(error);
                    }
                });

            } else {
              swal("Cancelled", "Your Record is safe", "error");

            }
        });
     }
    else
    {
   // empty
        return toastr["error"]("Select at least one record to delete!");
    }
 }
 function delete_data_ethinic(id,url)
 {
    swal({
        title: "Are you sure?",
        text: "Selected option with be Deleted ",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
     },
     function(isConfirm){

       if (isConfirm){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
             $.ajax({
                type: "POST",
                url: url,
                data: {'id':id},
                success: function(response)
                {
                    if(response.flag==1)
                    {
                        var url='/employee/ethinic/getall';
                    }
                    else
                    {
                        var url='/employee/ethinic/getallSoft';
                    }
                    $('#ethnic_table').DataTable().clear().destroy();
                    ethinic_data(url);
                    swal.close();
                    toastr["success"](response.message);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        } else {
          swal("Cancelled", "Your Record is safe", "error");

        }
     });
 }
function deletesoftethinicdata(id)
{
    var url='/employee/ethinic/softdelete';
    delete_data_ethinic(id,url);

}
function deleteethinicdata(id)
{
    var url='/employee/ethinic/delete';
    delete_data_ethinic(id,url);
}
