$(document).ready(function() {
    var selectedrows=[];
    $('body').on('click','#doc_type_table_checkbox_all',function() {
        $('.selectedrowdoc_type').not(this).prop('checked', this.checked);
        $('.selectedrowdoc_type').each(function(key,value) {
            var id=$(this).attr('id');
            id=id.split('doc_type');
            row_id='#doc_type_tr'+id[1];
            if($(this).is(":checked"))
            {
                if(key == 0)
                {
                    selectedrows=[];
                }
                if(id[1] != '_checkbox_all')
                {
                    selectedrows.push(id[1]);
                }
                $('.doc_type_export').val(JSON.stringify(selectedrows));
                $(row_id).addClass('selected');
            }
            else
            {
                $('.doc_type_export').val(1);
                selectedrows=[];
                $(row_id).removeClass('selected');
            }
        });

    });


    $('body').on('click', '.selectedrowdoc_type', function() {
        var id=$(this).attr('id');
        id=id.split('doc_type');
        row_id='#doc_type_tr'+id[1];
        if($(this).is(":checked"))
        {
            selectedrows.push(id[1]);
            $('.doc_type_export').val(JSON.stringify(selectedrows));
            $(row_id).addClass('selected');
        }else
        {
            if(selectedrows.includes(id[1]))
            {
                selectedrows.splice( selectedrows.indexOf(id[1]),1);
                $('.doc_type_export').val(JSON.stringify(selectedrows));
                $(row_id).removeClass('selected');
            }
        }
    });


    $('#selectedactivebuttondoc_type').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        var url='/doc_type/active/multiple';
        if (selectedrows && selectedrows.length) {
       // not empty
            swal({
                title: "Are you sure?",
                text: "Selected option will be active  again ..!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
             },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:url,
                        data: {'ids':selectedrows},
                        success: function(response)
                        {

                            toastr["success"](response.message);
                            var url='/doc_type/getallSoft';
                            $('#doc_type_table').DataTable().clear().destroy();
                            doc_type_data(url);
                            swal.close();

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                  swal("Cancelled", "Your Record is safe", "error");

                }
            });
         }
        else
        {
       // empty
            return toastr["error"]("Select at least one record to delete!");
        }
    });
    $('#deleteselecteddoc_type').click(function(){
       var url='/doc_type/delete/multiple';
       delete_mutilpleDatadoc_type(url,selectedrows);
    });
    $('#deleteselecteddoc_typeSoft').click(function(){
       var url='/doc_type/delete/soft/multiple';
       delete_mutilpleDatadoc_type(url,selectedrows);
    });
    $('#restorebuttondoc_type').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        selectedrows=[];
        $('#export_excel_doc_type').hide();
        $('#export_world_doc_type').hide();
        $('#export_pdf_doc_type').hide();
        $('#restorebuttondoc_type').hide();
        $('#deleteselecteddoc_type').hide();
        $('#selectedactivebuttondoc_type').show();
        $('#deleteselecteddoc_typeSoft').show();
        $('.adddoc_typeModal').hide();
        $('#activebuttondoc_type').show();
        $('#doc_type_table').DataTable().clear().destroy();
        var url='/doc_type/getallSoft';
        doc_type_data(url);
    });
    $('#activebuttondoc_type').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        selectedrows=[];
        $('#export_excel_doc_type').show();
        $('#export_world_doc_type').show();
        $('#export_pdf_doc_type').show();
        $('.adddoc_typeModal').show();
        $('#selectedactivebuttondoc_type').hide();
        $('#deleteselecteddoc_type').show();
        $('#deleteselecteddoc_typeSoft').hide();
        $('#restorebuttondoc_type').show();
        $('#activebuttondoc_type').hide();
        var url='/doc_type/getall';
        $('#doc_type_table').DataTable().clear().destroy();
        doc_type_data(url);
    });
    $('body').on('click', '.adddoc_typeModal', () => {
        $.ajax({
            type: 'GET',
            url: '/doc_type/store',
            success: function(response)
            {
                var html='';
                html+='<tr id="doc_type_tr'+response.data.id+'" class="selectedrowdoc_type" role="row" class="hide odd">';
                html+='<td>';
                html +='<div class="form-check"><input type="checkbox" class="form-check-input" id="doc_type' + response.data.id + '"><label class="form-check-label" for="doc_type' + response.data.id + '"></label></div>';
                html+='</td>';
                html+='<td class="pt-3-half edit_inline_doc_type" onkeydown="editSelectedRow(doc_type_tr'+ response.data.id +')" data-id="'+response.data.id+'" id="doc_type_name'+response.data.id+'"   contenteditable="true"></td>';
                // html+='<td class="sorting_1">';
                // html += '<a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light" onclick="deletedoc_typedata( ' + response.data.id + ')"><i class="fas fa-trash"></i></a>';
                // html+='</td>';
                html+='<td>';
                html += '<a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light delete-btn'+response.data.id+'" onclick="deletedoc_typedata( ' + response.data.id + ')"><i class="fas fa-trash"></i></a>';
                html+='<a type="button" class="btn btn-primary btn-xs my-0 waves-effect waves-light savedoc_typedata show_tick_btn'+response.data.id+'" style="display: none;"  id="'+response.data.id+'"><i class="fas fa-check"></i></a>';
                html+='</td>';
                html+='</tr>';
                $('#table_doc_type').find('table').prepend(html);
            },
            error: function (error) {
                console.log(error);
            }
        });


    });

    $('body').on('click', '.savedoc_typedata', function() {
        var company_id=$('#companies_selected_nav').children("option:selected").val();
        var tr_id=$(this).attr('id');
        console.log($(this).attr('id'));
        var data={
            'id':tr_id,
            'name':$('#doc_type_name'+tr_id).html(),
            'company_id':company_id,

        };
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url:'/doc_type/update',
            data:data,
            success: function(response)
            {
                console.log(response);
                if(response.error)
                {
                    toastr["error"](response.error);
                    $("#doc_type_tr"+tr_id).attr('style','background-color: #fc685f');
                }
                else
                {
                    $("#doc_type_tr"+tr_id).attr('style','background-color: lightgreen !important');
                    toastr["success"](response.message);

                }
                $(".delete-btn"+tr_id).show();
                $(".show_tick_btn"+tr_id).hide();
            }
        });
        // console.log(data);
    });
});
function restoredoc_typedata(id)
{
    var url='/doc_type/active';
    swal({
        title: "Are you sure?",
        text: "Selected option with be active ",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
     },
     function(isConfirm){

       if (isConfirm){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                    type: "POST",
                    url: url,
                    data: {'id':id},
                    success: function(response)
                    {
                        if(response.flag==1)
                        {
                            var url='/doc_type/getall';
                        }
                        else
                        {
                            var url='/doc_type/getallSoft';
                        }
                        $('#doc_type_table').DataTable().clear().destroy();
                        doc_type_data(url);
                        swal.close();
                        toastr["success"](response.message);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
        } else {
          swal("Cancelled", "Your Record is safe", "error");

        }
     });
}
function delete_data_doc_type(id,url)
{
    swal({
        title: "Are you sure?",
        text: "Selected option will be Deleted ",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
    },
    function(isConfirm){
        if (isConfirm){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: url,
                data: {'id':id},
                success: function(response)
                {
                    $('#doc_type_table').DataTable().clear().destroy();
                    var url='/doc_type/getall';
                    doc_type_data(url);
                    swal.close();
                    toastr["success"](response.message);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        } else {
            swal("Cancelled", "Your Record is safe", "error");

        }
    });
}

function delete_mutilpleDatadoc_type(url,selectedrows)
{
    if( $('.edit_cms_disable').css('display') == 'none' ) {
        console.log('Editer mode On Please change your mode');
        return 0;
    }

    if (selectedrows && selectedrows.length) {
   // not empty
        swal({
            title: "Are you sure?",
            text: "Selected option will be inactive and can be Active again by the admin only!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
         },
        function(isConfirm){

            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url:url,
                    data: {'ids':selectedrows},
                    success: function(response)
                    {

                        toastr["success"](response.message);
                        if(response.flag==1)
                        {
                            var url='/doc_type/getall';
                        }
                        else
                        {
                            var url='/doc_type/getallSoft';
                        }
                        $('#doc_type_table').DataTable().clear().destroy();
                        doc_type_data(url);
                        selectedrows=[];
                        swal.close();

                    },
                    error: function (error) {
                        console.log(error);
                    }
                });

            } else {
              swal("Cancelled", "Your Record is safe", "error");

            }
        });
     }
    else
    {
   // empty
        return toastr["error"]("Select at least one record to delete!");
    }
 }
 function delete_data_doc_type(id,url)
 {
    swal({
        title: "Are you sure?",
        text: "Selected option with be Deleted ",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
     },
     function(isConfirm){

       if (isConfirm){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
             $.ajax({
                        type: "POST",
                        url: url,
                        data: {'id':id},
                        success: function(response)
                        {
                            if(response.flag==1)
                            {
                                var url='/doc_type/getall';
                            }
                            else
                            {
                                var url='/doc_type/getallSoft';
                            }
                            $('#doc_type_table').DataTable().clear().destroy();
                            doc_type_data(url);
                            swal.close();
                            toastr["success"](response.message);
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
        } else {
          swal("Cancelled", "Your Record is safe", "error");

        }
     });
 }
function deletesoftdoc_typedata(id)
{
    var url='/doc_type/softdelete';
    delete_data_doc_type(id,url);

}
function deletedoc_typedata(id)
{
var url='/doc_type/delete';
delete_data_doc_type(id,url);
}

