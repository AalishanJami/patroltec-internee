function saveUplaod()
{
    if( $('.edit_cms_disable').css('display') == 'none' ) {
        console.log('Editer mode On Please change your mode');
        return 0;
    }
    var notes=$('#note').find( '.note-editable, .card-block p' ).html();
    $('#notes').val(notes);
    var formData = $("#uploadForm").serializeObject();
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: '/client/storeUpload',
        data: {'formData':formData,'company_id':company_id},
        success: function(data)
        {
           // $('#modalUpload').modal('hide');
        },
        error: function (error) {
            console.log(error);
        }
    });
}

var selectedrows=[];
$('body').on('click','#upload_checkbox_all',function() {
    $('input:checkbox').not(this).prop('checked', this.checked);
    $('input[type="checkbox"]').each(function(key,value) {
        var id=$(this).attr('id');
        console.log(id);
        if(id)
        {
            id=id.split('upload_check');
            row_id='#upload_tr'+id[1];
            if($(this).is(":checked"))
            {
                if(key == 0)
                {
                    selectedrows=[];
                }
                if(id[1] != '_checkbox_all')
                {
                    selectedrows.push(id[1]);
                }
                $('.upload_export').val(JSON.stringify(selectedrows));
                $(row_id).addClass('selected');
            }
            else
            {
                $('.upload_export').val(1);
                selectedrows=[];
                $(row_id).removeClass('selected');
            }
        }
    });
});

$('body').on('click', '.selectedRowUpload', function() {
    var id=$(this).attr('id');
    id=id.split('upload_check');
    row_id='#upload_tr'+id[1];
    if($(this).is(":checked"))
    {
        selectedrows.push(id[1]);
        $('.upload_export').val(JSON.stringify(selectedrows));
        $(row_id).addClass('selected');
    }else
    {
        if(selectedrows.includes(id[1]))
        {
            selectedrows.splice( selectedrows.indexOf(id[1]),1);
            $('.upload_export').val(JSON.stringify(selectedrows));
            $(row_id).removeClass('selected');
        }
    }
});



function selectedRowUpload(id)
{
    if(selectedrows.includes(id))
    {
        selectedrows.splice( selectedrows.indexOf(id), 1 );
        $('.upload_export').val(JSON.stringify(selectedrows));
        $('#upload_table tr#'+id).removeClass('selected');
    }
    else
    {
        selectedrows.push(id);
        $('.upload_export').val(JSON.stringify(selectedrows));
        $('#upload_table tr#'+id).addClass('selected');
    }

}

function uploadEdit(id)
{
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: '/upload/edit',
        data: {id:id},
        success: function(data)
        {
            console.log("Upload Data : "+data);
            document.getElementById("edit_title").value = data.name;
            $("#edit_id").val(data.id);
            $("#notes_value_edit").val(data.notes);
            $('#upload_note_edit').find('.note-editable' ).html(data.notes);

            $(".file-upload-wrapper>.file-upload").addClass("has-preview");
            $(".file-upload-preview").attr("style","display:block;");
            var base_url = $("#base_url").val()+"/"+data.file_name;
            $(".file-upload-preview>.file-upload-render").html('<img class="file-upload-preview-img" src="'+base_url+'">');
            $("#modalUploadEdit").modal('show');
        },
        error: function (error) {
            console.log(error);
        }
    });
}
function uploadStaticActive(id)
{
    var url='/static_form/active/'+id;
    console.log(url);
    $.ajax({
        type: "GET",
        url: url,
        success: function(response)
        {
            // console.log(response);
            $('#upload_static_table').DataTable().clear().destroy();
            uploadStaticAppend();
            toastr["success"]('Static Form Marked Complete!');
        },
        error: function (error) {
            console.log(error);
        }
    });
}
function checkSelectedStatic(value,checkValue)
{
    // console.log(value);
    if(value == checkValue)
    {
        return 'checked';
    }
    else
    {
        return "";
    }

}
function uploadStaticEdit(id)
{
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: '/static_form/update',
        data: {id:id},
        success: function(response)
        {
            console.log(response);
            document.getElementById("edit_title").value = response.name;
            document.getElementById("edit_id").value = response.id;
            $('.notes_value').summernote('code',response.notes);
            $('#modalstaticEdit').modal('show');
        },
        error: function (error) {
            console.log(error);
        }
    });
}
function deleteUpload(id)
{

    swal({
            title:'Are you sure?',
            text: "Delete Record.",
            type: "error",
            showCloseButton: true,
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Delete!",
            cancelButtonText: "Cancel!",
            closeOnConfirm: false,
            customClass: "confirm_class",
            closeOnCancel: false,
        },
        function(isConfirm){
            console.log( isConfirm);
            if( $('.edit_cms_disable').css('display') != 'none' ) {
                if (isConfirm) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type : 'post',
                        url : '/upload/delete',
                        data:{'id':id},
                        success:function(data)
                        {

                            setTimeout(function () {
                                swal({
                                        title: "",
                                        text: "Delete Sucessfully!",
                                        type: "success",
                                        confirmButtonText: "OK"
                                    },

                                    function(isConfirm){

                                        if (isConfirm) {
                                            $('#upload_table').DataTable().clear().destroy();
                                            uploadAppend();
                                        }
                                    }); }, 500);
                        }
                    });

                }
                else
                {
                    swal.close();
                }
            }
        });

    var active ='<span class="'+custom_class+' label'+$('#are_you_sure_label').attr('data-id')+'" data-id="'+$('#are_you_sure_label').attr('data-id')+'" data-value="'+$('#are_you_sure_label').val()+'" >'+$('#are_you_sure_label').val()+'</span>';
    $('.confirm_class h2').html(active);
    active ='<span class="'+custom_class+' label'+$('#delete_record_label').attr('data-id')+'" data-id="'+$('#delete_record_label').attr('data-id')+'" data-value="'+$('#delete_record_label').val()+'" >'+$('#delete_record_label').val()+'</span>';
    $('.confirm_class p').html(active);
    active ='<span class="'+custom_class+' label'+$('#cancel_label').attr('data-id')+'" data-id="'+$('#cancel_label').attr('data-id')+'" data-value="'+$('#cancel_label').val()+'" >'+$('#cancel_label').val()+'</span>';
    $('.cancel').html(active);
    active ='<span class="'+custom_class+' label'+$('#delete_label').attr('data-id')+'" data-id="'+$('#delete_label').attr('data-id')+'" data-value="'+$('#delete_label').val()+'" >'+$('#delete_label').val()+'</span>';
    $('.confirm').html(active);
}
$(document).ready(function(){
            $('#modalUpload').on('shown.bs.modal', function (e) {
                console.log('#uploadForm');
                document.getElementById("uploadForm").reset();
                    $('#uploadForm').trigger("reset");
                     
            });
       $( '#location_id' ).change(function() {
            location_id=$('#location_id').children("option:selected").val();
            console.log(location_id);
            $.ajax({
                type: "GET",
                url: '/schedule/project/'+location_id,
                success: function(response)
                {

               console.log(response.location);
              var html='';
              html+='<select name="location_breakdown_id[]" id="location_breakdown_id" class="mdb-select-custom-location colorful-select dropdown-primary md-form" multiple searchable="Search here.." >';
              html+='<option value="null" disabled>Please Select</option>';
                for (var i = response.location.length - 1; i >= 0; i--) {

                        html+='<option value="'+response.location[i].id+'">'+response.location[i].name+'</option>';
                }

                html+='</select> ';
                if(response.form_type=="t")
                html+='<label class="mdb-main-label">QR List</label>';
                if(response.form_type=="e")
                html+='<label class="mdb-main-label">Equipment List</label>';

                $('#qr_listing').empty();
                $('#break_location').val(response.form_type);
                console.log(html, "html");
                $('#qr_listing').html(html);
                $('.mdb-select-custom-location').materialSelect();
               $('select.mdb-select-custom-location').hide();

                },
                error: function (error) {
                    console.log(error);
                }
            });
        });
    //.........
    $('#deleteSelectedupload').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        if (selectedrows && selectedrows.length)
        {
            swal({
                    title: "Are you sure?",
                    text: "Data will be inactive and can be Active again by the admin only!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Yes, I am sure!',
                    cancelButtonText: "No, cancel it!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm){

                    if (isConfirm){
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $.ajax({
                            type: "POST",
                            url:'/upload/multipleDelete',
                            data: {'ids':selectedrows,'flag':$("#flagAddress").val()},
                            success: function(response)
                            {
                                $('#upload_table').DataTable().clear().destroy();
                                var flag=$("#flagAddress").val();
                                if (flag==1){
                                    uploadAppend();
                                }else{
                                    uploadSoftAppend();
                                }
                                swal.close();
                                toastr["success"](response.message);
                            },
                            error: function (error) {
                                console.log(error);
                            }
                        });

                    } else {
                        swal("Cancelled", "Your Record is safe", "error");
                        e.preventDefault();
                    }
                });
        }
        else
        {
            return toastr["error"]("Select at least one record to delete!");
        }
    });

    $('#selectedactivebuttonupload').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        if (selectedrows && selectedrows.length)
        {
            swal({
                    title: "Are you sure?",
                    text: "Data will be inactive and can be Active again by the admin only!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Yes, I am sure!',
                    cancelButtonText: "No, cancel it!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm){

                    if (isConfirm){
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $.ajax({
                            type: "POST",
                            url:'/upload/active/multiple',
                            data: {'ids':selectedrows},
                            success: function(response)
                            {
                                $('#upload_table').DataTable().clear().destroy();
                                uploadSoftAppend();
                                swal.close();
                                toastr["success"](response.message);
                            },
                            error: function (error) {
                                console.log(error);
                            }
                        });

                    } else {
                        swal("Cancelled", "Your Record is safe", "error");
                        e.preventDefault();
                    }
                });
        }
        else
        {
            return toastr["error"]("Select at least one record to delete!");
        }
    });

    $('#restore_button_upload').click(function() {
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        $('#restore_button_upload').hide();
        $('#export_excel_upload').val(2);
        $('#export_word_upload').val(2);
        $('#export_pdf_upload').val(2);
        $('#flagAddress').val(2);
        $('.export_excel_upload').hide();
        $('.export_word_upload').hide();
        $('.export_pdf_upload').hide();
        selectedrows=[];
        $('#show_active_button_upload').show();
        $('#selectedactivebuttonupload').show();
        $('#upload_table').DataTable().clear().destroy();
        uploadSoftAppend();
    });

    $('#show_active_button_upload').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        $('#show_active_button_upload').hide();
        $('#export_excel_upload').val(1);
        $('#export_word_upload').val(1);
        $('#export_pdf_upload').val(1);
        selectedrows=[];
        $('.export_excel_upload').show();
        $('.export_word_upload').show();
        $('.export_pdf_upload').show();

        $('#flagAddress').val(1);
        $('#restore_button_upload').show();
        $('#selectedactivebuttonupload').hide();
        $('#upload_table').DataTable().clear().destroy();
        uploadAppend();
    });
});



function restoreUpload(id)
{
    swal({
            title: "Are you sure?",
            text: "Selected Location will be Active again!",
            type: "warning",
            showCloseButton: true,
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            customClass: "confirm_class",
            closeOnCancel: false
        },
        function(isConfirm){
            if( $('.edit_cms_disable').css('display') != 'none' ) {
                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:'/upload/restore',
                        data: {'id':id},
                        success: function(response)
                        {
                            $('#upload_table').DataTable().ajax.reload();
                            swal.close()
                            toastr["success"]("Location Activated Successfully!");
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
                }
                else {
                    swal.close();
                    // swal("Cancelled", "Your Record is safe", "error");
                    // e.preventDefault();
                }
            }
        });
    var active ='<span class="'+custom_class+' label'+$('#are_you_sure_label').attr('data-id')+'" data-id="'+$('#are_you_sure_label').attr('data-id')+'" data-value="'+$('#are_you_sure_label').val()+'" >'+$('#are_you_sure_label').val()+'</span>';
    $('.confirm_class h2').html(active);
    active ='<span class="'+custom_class+' label'+$('#selected_user_with_be_active_again_label').attr('data-id')+'" data-id="'+$('#selected_user_with_be_active_again_label').attr('data-id')+'" data-value="'+$('#selected_user_with_be_active_again_label').val()+'" >'+$('#selected_user_with_be_active_again_label').val()+'</span>';
    $('.confirm_class p').html(active);
    active ='<span class="'+custom_class+' label'+$('#cancel_label').attr('data-id')+'" data-id="'+$('#cancel_label').attr('data-id')+'" data-value="'+$('#cancel_label').val()+'" >'+$('#cancel_label').val()+'</span>';
    $('.cancel').html(active);
    active ='<span class="'+custom_class+' label'+$('#delete_label').attr('data-id')+'" data-id="'+$('#delete_label').attr('data-id')+'" data-value="'+$('#delete_label').val()+'" >'+$('#delete_label').val()+'</span>';
    $('.confirm').html(active);

}


function hardDeleteUpload(id)
{
    swal({
            title: "Are you sure?",
            text: "Selected Location with be Hard Deleted and can active from backend again!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){

            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: '/upload/hardDelete',
                    data: {'id':id},
                    success: function(response)
                    {
                        $('#upload_table').DataTable().clear().destroy();
                        uploadSoftAppend();
                        swal.close()
                        toastr["success"]("Upload Hard Deleted Successfully!");
                        toastr.options = {
                            "closeButton": false,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-center",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        };
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            } else {
                swal("Cancelled", "Your Record is safe", "error");
                e.preventDefault();
            }
        });
}
