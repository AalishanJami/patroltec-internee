$(document).ready(function() {
    var selectedrows=[];

    $('body').on('click','#managername_checkbox_all',function() {
        $('.selectedrowmanager').not(this).prop('checked', this.checked);
        $('.selectedrowmanager').each(function(key,value) {
            var id=$(this).attr('id');
            console.log(id);
            if(id)
            {
                id=id.split('manager');
                row_id='#manager_tr'+id[1];
                if($(this).is(":checked"))
                {
                    if(key == 0)
                    {
                        selectedrows=[];
                    }
                    if(id[1] != '_checkbox_all')
                    {
                        selectedrows.push(id[1]);
                    }
                    $('.manager_export').val(JSON.stringify(selectedrows));
                    $(row_id).addClass('selected');
                }
                else
                {
                    $('.manager_export').val(1);
                    selectedrows=[];
                    $(row_id).removeClass('selected');
                }
            }
        });
    });


    $('body').on('click', '.selectedrowmanager', function() {
        var id=$(this).attr('id');
        id=id.split('manager');
        row_id='#manager_tr'+id[1];
        if($(this).is(":checked"))
        {
            selectedrows.push(id[1]);
            $('.manager_export').val(JSON.stringify(selectedrows));
            $(row_id).addClass('selected');
        }else
        {
            if(selectedrows.includes(id[1]))
            {
                selectedrows.splice( selectedrows.indexOf(id[1]),1);
                $('.manager_export').val(JSON.stringify(selectedrows));
                $(row_id).removeClass('selected');
            }
        }
    });

    $('#selectedactivebuttonmanager').click(function(){
        var url='/manager/active/multiple';
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        if (selectedrows && selectedrows.length) {
            // not empty
            swal({
                    title: "Are you sure?",
                    text: "Selected option will be active  again ..!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Yes, I am sure!',
                    cancelButtonText: "No, cancel it!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm){

                    if (isConfirm){
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $.ajax({
                            type: "POST",
                            url:url,
                            data: {'ids':selectedrows},
                            success: function(response)
                            {

                                toastr["success"](response.message);
                                var url='/manager/getallSoft';
                                if ($("#managername_checkbox_all").prop('checked',true)){
                                    $("#managername_checkbox_all").prop('checked',false);
                                }
                                $('#manager_table').DataTable().clear().destroy();
                                manager_data(url);
                                swal.close();

                            },
                            error: function (error) {
                                console.log(error);
                            }
                        });

                    } else {
                        swal("Cancelled", "Your Record is safe", "error");

                    }
                });
        }
        else
        {
            // empty
            return toastr["error"]("Select at least one record to delete!");
        }
    });
    $('#deleteselectedmanager').click(function(){
        var url='/manager/delete/multiple';
        delete_mutilpleManagerData(url,selectedrows);
    });
    $('#deleteselectedmanagerSoft').click(function(){
        var url='/manager/delete/soft/multiple';
        delete_mutilpleManagerData(url,selectedrows);
    });

    $('.add-manager').on('click', 'i', () => {
        $.ajax({
            type: 'GET',
            url: '/manager/store',
            success: function(response)
            {
                var html='';
                html+= '<tr id="manager_tr'+response.data.id+'"  role="row" class="hide odd">';
                html+=' <td><div class="form-check"><input type="checkbox" class="form-check-input selectedrowmanager" id="manager'+response.data.id+'"><label class="form-check-label" for="manager'+response.data.id+'"></label></div></td>';
                html+='<td class="pt-3-half edit_inline_manager" onkeypress="editSelectedRow('+"manager_tr"+response.data.id+')" id="nameM'+response.data.id+'" ';
                if ($("#managerName_name_create").val()==1)
                {
                    html+='contenteditable="true" ';
                }
                html+='></td>';
                html+='<td class="pt-3-half edit_inline_manager" onkeypress="editSelectedRow('+"manager_tr"+response.data.id+')"  id="nameS'+response.data.id+'" ';
                if ($("#managerName_name_create").val()==1)
                {
                    html+='contenteditable="true" ';
                }
                html+='></td>';
                var p_create="";
                if (!$("#managerName_position_create").val()) {
                    var p_create="disabled";
                }
                html+= '<td class="pt-3-half edit_inline_manager" data-id="' + response.data.id + '" id="position' + response.data.id + '"  >';
                html+= '<div class="form-check float-left">';
                html+= ' <input type="radio" '+p_create+'  value="1" onclick="editSelectedRow('+"manager_tr"+response.data.id+')"  name="position' +response.data.id + '"  class="position' +response.data.id + ' form-check-input" id="staff_position' +response.data.id + '">';
                html+= '<label class="form-check-label" for="staff_position' +response.data.id + '">Staff</label>';
                html+= '</div>';
                html+= '<div class="form-check float-left">';
                html+= ' <input type="radio" '+p_create+' value="2" onclick="editSelectedRow('+"manager_tr"+response.data.id+')"  name="position' +response.data.id + '"  class="position' +response.data.id + ' form-check-input" id="manager_position' +response.data.id + '">';
                html+= '<label class="form-check-label" for="manager_position' +response.data.id + '">Manager</label>';
                html+= '</div>';
                html+= '<div class="form-check float-left">';
                html+= ' <input type="radio" '+p_create+' value="3" onclick="editSelectedRow('+"manager_tr"+response.data.id+')"  name="position' +response.data.id + '"  class="position' +response.data.id + ' form-check-input" id="contract_manager_position' +response.data.id + '">';
                html+= '<label class="form-check-label" for="contract_manager_position' +response.data.id + '">Contract Manager</label>';
                html+= '</div>';
                html+='</td>';
                html+='<td class="pt-3-half">';
                html+='<a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light delete-btn'+response.data.id+'" onclick="deletemanagerdata()"><i class="fas fa-trash"></i></a>';
                html+='<a type="button" class="btn btn-primary btn-xs my-0 waves-effect all_action_btn_margin waves-light save_manager_data manager_show_button_'+response.data.id+' show_tick_btn'+response.data.id+'"  id="'+response.data.id+'"><i class="fas fa-check"></i></a>';
                html+='</td>';
                html+='</tr>';

                $('#table-manager').find('table').prepend(html);
                $('.manager_show_button_'+response.data.id+'').hide();
            },
            error: function (error) {
                console.log(error);
            }
        });


    });
    $('body').on('click', '.save_manager_data', function() {
        var company_id=$('#companies_selected_nav').children("option:selected").val();
        var tr_id=$(this).attr('id');
        if (($("#managerName_name_edit").val() ==1)){
            var namefield= 1;
        }else{
            var namefield=0;
        }
        var data={
            'id':tr_id,
            'first_name':$('#nameM'+tr_id).html(),
            'surname':$('#nameS'+tr_id).html(),
            'position':$("input[name='position"+tr_id+"']:checked").val(),
            'company_id':company_id,
        };
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url:'/manager/update',
            data:data,
            success: function(response)
            {
                if(response.error)
                {
                    toastr["error"](response.error);
                }
                else
                {
                    toastr["success"](response.message);
                    save_and_delete_btn_toggle(tr_id);
                    if(response.flag==1)
                    {
                        $("#manager_tr"+tr_id).attr('style','background-color:#90ee90 !important');
                        // var url='/manager/getall';
                    }
                    else
                    {
                        $("#manager_tr"+tr_id).attr('style','background-color:#ff3547 !important');
                        // var url='/manager/getallsoft';
                    }
                    // $('#manager_table').DataTable().clear().destroy();//
                    // manager_data(url);

                }

            }

        });

        // console.log(data);

    });

    $('#restorebuttonmanager').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        selectedrows=[];
        $('#restorebuttonmanager').hide();
        $('#deleteselectedmanager').hide();
        $('#selectedactivebuttonmanager').show();
        $('#deleteselectedmanagerSoft').show();
        $('#export_excel_manager').hide();
        $('#export_world_manager').hide();
        $('#export_pdf_manager').hide();
        $('.add-manager').hide();
        $('#activebuttonmanager').show();
        var url='/manager/getallSoft';
        $('#manager_table').DataTable().clear().destroy();
        manager_data(url);
    });
    $('#activebuttonmanager').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        selectedrows=[];
        $('#export_excel_manager').show();
        $('#export_world_manager').show();
        $('#export_pdf_manager').show();
        $('.add-manager').show();
        $('#selectedactivebuttonmanager').hide();
        $('#deleteselectedmanager').show();
        $('#deleteselectedmanagerSoft').hide();
        $('#restorebuttonmanager').show();
        $('#activebuttonmanager').hide();
        var url='/manager/getall';
        $('#manager_table').DataTable().clear().destroy();
        manager_data(url);
    });
});
function restoremanagerdata(id)
{
    var url='/manager/active';
    swal({
            title: "Are you sure?",
            text: "Selected option with be active ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){

            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {'id':id},
                    success: function(response)
                    {
                        if(response.flag==1)
                        {
                            var url='/manager/getall';
                        }
                        else
                        {
                            var url='/manager/getallSoft';
                        }
                        $('#manager_table').DataTable().clear().destroy();
                        manager_data(url);
                        swal.close();
                        toastr["success"](response.message);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            } else {
                swal("Cancelled", "Your Record is safe", "error");

            }
        });
}
function delete_mutilpleManagerData(url,selectedrows)
{
    if( $('.edit_cms_disable').css('display') == 'none' ) {
        console.log('Editer mode On Please change your mode');
        return 0;
    }
    if (selectedrows && selectedrows.length) {
        // not empty
        swal({
                title: "Are you sure?",
                text: "Selected option will be inactive and can be Active again by the admin only!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:url,
                        data: {'ids':selectedrows},
                        success: function(response)
                        {

                            toastr["success"](response.message);
                            if(response.flag==1)
                            {
                                var url='/manager/getall';
                            }
                            else
                            {
                                var url='/manager/getallSoft';
                            }
                            if ($("#managername_checkbox_all").prop('checked',true)){
                                $("#managername_checkbox_all").prop('checked',false);
                            }
                            $('#manager_table').DataTable().clear().destroy();
                            manager_data(url);
                            swal.close();

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                    swal("Cancelled", "Your Record is safe", "error");

                }
            });
    }
    else
    {
        // empty
        return toastr["error"]("Select at least one record to delete!");
    }
}
function delete_manager_data(id,url)
{
    swal({
            title: "Are you sure?",
            text: "Selected option with be Deleted ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){

            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {'id':id},
                    success: function(response)
                    {
                        if(response.flag==1)
                        {
                            var get_all_url='/manager/getall';
                        }
                        else
                        {
                            var get_all_url='/manager/getallSoft';
                        }
                        $('#manager_table').DataTable().clear().destroy();
                        manager_data(get_all_url);
                        swal.close();
                        toastr["success"](response.message);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            } else {
                swal("Cancelled", "Your Record is safe", "error");

            }
        });
}
function deletesoftmanagerdata(id)
{
    var url='/manager/softdelete';
    delete_manager_data(id,url);

}
function deletemanagerdata(id)
{
    var url='/manager/delete';
    delete_manager_data(id,url);
}
