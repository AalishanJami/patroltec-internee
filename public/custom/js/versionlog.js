var selectedrows=[];
$('body').on('click','#versionlog_checkbox_all',function() {
    $('input:checkbox').not(this).prop('checked', this.checked);
    $('input[type="checkbox"]').each(function(key,value) {
        var id=$(this).attr('id');
        console.log(id);
        if(id)
        {
            id=id.split('versionlog_check');
            row_id='#versionlog_tr'+id[1];
            if($(this).is(":checked"))
            {
                if(key == 0)
                {
                    selectedrows=[];
                }
                if(id[1] != '_checkbox_all')
                {
                    selectedrows.push(id[1]);
                }
                $('.export_versionlog').val(JSON.stringify(selectedrows));
                $(row_id).addClass('selected');
            }
            else
            {
                $('.export_versionlog').val(1);
                selectedrows=[];
                $(row_id).removeClass('selected');
            }
        }
    });
});

$('body').on('click', '.selectedRowVersionlog', function() {
    var id=$(this).attr('id');
    id=id.split('versionlog_check');
    row_id='#versionlog_tr'+id[1];
    if($(this).is(":checked"))
    {
        selectedrows.push(id[1]);
        $('.export_versionlog').val(JSON.stringify(selectedrows));
        $(row_id).addClass('selected');
    }else
    {
        if(selectedrows.includes(id[1]))
        {
            selectedrows.splice( selectedrows.indexOf(id[1]),1);
            $('.export_versionlog').val(JSON.stringify(selectedrows));
            $(row_id).removeClass('selected');
        }
    }
});

function selectedRowUpload(id)
{
    console.log(id);
    if(selectedrows.includes(id))
    {
        selectedrows.splice( selectedrows.indexOf(id), 1 );
        $('.upload_export').val(JSON.stringify(selectedrows));
        $('#upload_table tr#'+id).removeClass('selected');
    }
    else
    {
        selectedrows.push(id);
        $('.upload_export').val(JSON.stringify(selectedrows));
        $('#upload_table tr#'+id).addClass('selected');
    }

}

function uploadEdit(id)
{
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: '/client/uploadEdit',
        data: {id:id},
        success: function(data)
        {
            document.getElementById("edit_title").value = data.name;
            document.getElementById("edit_id").value = data.id;
        },
        error: function (error) {
            console.log(error);
        }
    });
}
function uploadStaticActive(id)
{
    var url='/static_form/active/'+id;
    console.log(url);
    $.ajax({
        type: "GET",
        url: url,
        success: function(response)
        {
            // console.log(response);
            $('#upload_static_table').DataTable().clear().destroy();
            uploadStaticAppend();
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-top-center",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
            toastr["success"]('Static Form Marked Complete!');
        },
        error: function (error) {
            console.log(error);
        }
    });
}
function checkSelectedStatic(value,checkValue)
{
    // console.log(value);
    if(value == checkValue)
    {
        return 'checked';
    }
    else
    {
        return "";
    }

}
function uploadStaticEdit(id)
{
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: '/static_form/update',
        data: {id:id},
        success: function(response)
        {
            console.log(response);
            document.getElementById("edit_title").value = response.name;
            document.getElementById("edit_id").value = response.id;
            $('.notes_value').summernote('code',response.notes);
            $('#modalstaticEdit').modal('show');
        },
        error: function (error) {
            console.log(error);
        }
    });
}
function deleteVersionLog(id)
{
    swal({
            title:'Are you sure?',
            text: "Delete Record.",
            type: "error",
            showCloseButton: true,
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Delete!",
            cancelButtonText: "Cancel!",
            closeOnConfirm: false,
            customClass: "confirm_class",
            closeOnCancel: false,
        },
        function(isConfirm){
            console.log( isConfirm);
            if( $('.edit_cms_disable').css('display') != 'none' ) {
                if (isConfirm) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type : 'post',
                        url : '/version/log/delete',
                        data:{'id':id},
                        success:function(data)
                        {
                            setTimeout(function () {
                                swal({
                                        title: "",
                                        text: "Delete Sucessfully!",
                                        type: "success",
                                        confirmButtonText: "OK"
                                    },
                                    function(isConfirm){
                                        if (isConfirm) {
                                            $('#version_log_table').DataTable().clear().destroy();
                                            var url='/version/log/getAll';
                                            versionLogData(url);
                                        }
                                    }); }, 500);
                        }
                    });

                }
                else
                {
                    swal.close();
                }
            }
        });

    // var active ='<span class="'+custom_class+' label'+$('#are_you_sure_label').attr('data-id')+'" data-id="'+$('#are_you_sure_label').attr('data-id')+'" data-value="'+$('#are_you_sure_label').val()+'" >'+$('#are_you_sure_label').val()+'</span>';
    // $('.confirm_class h2').html(active);
    // active ='<span class="'+custom_class+' label'+$('#delete_record_label').attr('data-id')+'" data-id="'+$('#delete_record_label').attr('data-id')+'" data-value="'+$('#delete_record_label').val()+'" >'+$('#delete_record_label').val()+'</span>';
    // $('.confirm_class p').html(active);
    // active ='<span class="'+custom_class+' label'+$('#cancel_label').attr('data-id')+'" data-id="'+$('#cancel_label').attr('data-id')+'" data-value="'+$('#cancel_label').val()+'" >'+$('#cancel_label').val()+'</span>';
    // $('.cancel').html(active);
    // active ='<span class="'+custom_class+' label'+$('#delete_label').attr('data-id')+'" data-id="'+$('#delete_label').attr('data-id')+'" data-value="'+$('#delete_label').val()+'" >'+$('#delete_label').val()+'</span>';
    // $('.confirm').html(active);
}
$(document).ready(function(){
    $('#deleteSelectedVersionlog').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        if (selectedrows && selectedrows.length)
        {
            swal({
                    title: "Are you sure?",
                    text: "Data will be inactive and can be Active again by the admin only!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Yes, I am sure!',
                    cancelButtonText: "No, cancel it!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm){

                    if (isConfirm){
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $.ajax({
                            type: "POST",
                            url:'/version/log/multipleDelete',
                            data: {'ids':selectedrows,'flag':$("#flagVersiolog").val()},
                            success: function(response)
                            {
                                $('#version_log_table').DataTable().clear().destroy();
                                if (response.flag==1){
                                    var url='/version/log/getAll';

                                }else{
                                    var url='/version/log/getAllSoft';
                                }
                                versionLogData(url);
                                swal.close();
                                toastr["success"](response.message);
                            },
                            error: function (error) {
                                console.log(error);
                            }
                        });

                    } else {
                        swal("Cancelled", "Your Record is safe", "error");
                        e.preventDefault();
                    }
                });
        }
        else
        {
            return toastr["error"]("Select at least one record to delete!");
        }
    });

    $('#selectedactivebuttonversionlog').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        if (selectedrows && selectedrows.length)
        {
            swal({
                    title: "Are you sure?",
                    text: "Data will be inactive and can be Active again by the admin only!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Yes, I am sure!',
                    cancelButtonText: "No, cancel it!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm){

                    if (isConfirm){
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $.ajax({
                            type: "POST",
                            url:'/version/log/activeMultiple',
                            data: {'ids':selectedrows},
                            success: function(response)
                            {

                                $('#version_log_table').DataTable().clear().destroy();
                                var url='/version/log/getAllSoft';
                                versionLogData(url);
                                swal.close();
                                toastr["success"](response.message);
                            },
                            error: function (error) {
                                console.log(error);
                            }
                        });

                    } else {
                        swal("Cancelled", "Your Record is safe", "error");
                        e.preventDefault();
                    }
                });
        }
        else
        {
            return toastr["error"]("Select at least one record to delete!");
        }
    });

    $('#restore_versionlog').click(function() {
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        $('#restore_versionlog').hide();
        $('#flagVersiolog').val(2);
        $('#export_excel_versionlog').hide();
        selectedrows=[];
        $('#export_word_versionlog').hide();
        $('#export_pdf_versionlog').hide();
        $('#show_active_versionlog').show();
        $('#selectedactivebuttonversionlog').show();
        $('#version_log_table').DataTable().clear().destroy();
        var url='/version/log/getAllSoft';
        versionLogData(url);
    });

    $('#show_active_versionlog').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        $('#show_active_versionlog').hide();
        selectedrows=[];
        $('#export_excel_versionlog').show();
        $('#export_word_versionlog').show();
        $('#export_pdf_versionlog').show();
        $('#flagVersiolog').val(1);
        $('#restore_versionlog').show();
        $('#selectedactivebuttonversionlog').hide();
        $('#show_active_versionlog').hide();
        $('#version_log_table').DataTable().clear().destroy();
        var url='/version/log/getAll';
        versionLogData(url);
    });
});


function restoreVersionLog(id)
{
    swal({
            title: "Are you sure?",
            text: "Selected Location will be Active again!",
            type: "warning",
            showCloseButton: true,
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            customClass: "confirm_class",
            closeOnCancel: false
        },
        function(isConfirm){
            if( $('.edit_cms_disable').css('display') != 'none' ) {
                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:'/version/log/restore',
                        data: {'id':id},
                        success: function(response)
                        {
                            $('#version_log_table').DataTable().clear().destroy();
                            var url='/version/log/getAllSoft';
                            versionLogData(url);
                            swal.close()
                            toastr["success"]("Static Form Activated Successfully!");

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
                }
                else {
                    swal.close();
                    // swal("Cancelled", "Your Record is safe", "error");
                    // e.preventDefault();
                }
            }
        });
    // var active ='<span class="'+custom_class+' label'+$('#are_you_sure_label').attr('data-id')+'" data-id="'+$('#are_you_sure_label').attr('data-id')+'" data-value="'+$('#are_you_sure_label').val()+'" >'+$('#are_you_sure_label').val()+'</span>';
    // $('.confirm_class h2').html(active);
    // active ='<span class="'+custom_class+' label'+$('#selected_user_with_be_active_again_label').attr('data-id')+'" data-id="'+$('#selected_user_with_be_active_again_label').attr('data-id')+'" data-value="'+$('#selected_user_with_be_active_again_label').val()+'" >'+$('#selected_user_with_be_active_again_label').val()+'</span>';
    // $('.confirm_class p').html(active);
    // active ='<span class="'+custom_class+' label'+$('#cancel_label').attr('data-id')+'" data-id="'+$('#cancel_label').attr('data-id')+'" data-value="'+$('#cancel_label').val()+'" >'+$('#cancel_label').val()+'</span>';
    // $('.cancel').html(active);
    // active ='<span class="'+custom_class+' label'+$('#delete_label').attr('data-id')+'" data-id="'+$('#delete_label').attr('data-id')+'" data-value="'+$('#delete_label').val()+'" >'+$('#delete_label').val()+'</span>';
    // $('.confirm').html(active);

}


function hardDeleteVersionLog(id)
{
    swal({
            title: "Are you sure?",
            text: "Selected Upload will be Hard Deleted and can active from backend again!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){

            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: '/version/log/hardDelete',
                    data: {'id':id},
                    success: function(response)
                    {
                        $('#version_log_table').DataTable().clear().destroy();
                        var url='/version/log/getAllSoft';
                        versionLogData(url);
                        swal.close()
                        toastr["success"]("Upload Hard Deleted Successfully!");

                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            } else {
                swal("Cancelled", "Your Record is safe", "error");
                e.preventDefault();
            }
        });
}
