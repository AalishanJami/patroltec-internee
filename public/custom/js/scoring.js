$(document).ready(function() {
    var selectedrows=[];
    $('body').on('click','#bonusscore_checkbox_all',function() {
        $('input:checkbox').not(this).prop('checked', this.checked);
        $('input[type="checkbox"]').each(function(key,value) {
            var id=$(this).attr('id');
            console.log(id);
            if(id)
            {
                id=id.split('scoring');
                row_id='#scoring_tr'+id[1];
                if($(this).is(":checked"))
                {
                    if(key == 0)
                    {
                        selectedrows=[];
                    }
                    if(id[1] != '_checkbox_all')
                    {
                        selectedrows.push(id[1]);
                    }
                    $('.scoring_export').val(JSON.stringify(selectedrows));
                    $(row_id).addClass('selected');
                }
                else
                {
                    $('.scoring_export').val(1);
                    selectedrows=[];
                    $(row_id).removeClass('selected');
                }
            }
        });
    });
    $('body').on('click', '.selectedrowscoring', function() {
        var id=$(this).attr('id');
        id=id.split('scoring');
        row_id='#scoring_tr'+id[1];
        if($(this).is(":checked"))
        {
            selectedrows.push(id[1]);
            $('.scoring_export').val(JSON.stringify(selectedrows));
            $(row_id).addClass('selected');
        }else
        {
            if(selectedrows.includes(id[1]))
            {
                selectedrows.splice( selectedrows.indexOf(id[1]),1);
                $('.scoring_export').val(JSON.stringify(selectedrows));
                $(row_id).removeClass('selected');
            }
        }
    });

    $('#selectedactivebuttonscoring').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        var url='/document/bonus/score/active/multiple';
        if (selectedrows && selectedrows.length) {
            swal({
                    title: "Are you sure?",
                    text: "Selected option will be active  again ..!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Yes, I am sure!',
                    cancelButtonText: "No, cancel it!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm){

                    if (isConfirm){
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $.ajax({
                            type: "POST",
                            url:url,
                            data: {'ids':selectedrows},
                            success: function(response)
                            {

                                toastr["success"](response.message);
                                var url='/document/bonus/score/getallSoft';
                                $('.scoring_table_static').DataTable().clear().destroy();
                                scoring_data(url);
                                swal.close();

                            },
                            error: function (error) {
                                console.log(error);
                            }
                        });

                    } else {
                        swal("Cancelled", "Your Record is safe", "error");

                    }
                });
        }
        else
        {
            // empty
            return toastr["error"]("Select at least one record to delete!");
        }
    });

    $('#deleteselectedscoring').click(function(){
        var url='/document/bonus/score/delete/multiple';
        delete_mutilpleDataScoring(url,selectedrows);
    });
    $('#deleteselectedscoringSoft').click(function(){
        var url='/document/bonus/score/delete/soft/multiple';
        delete_mutilpleDataScoring(url,selectedrows);
    });

    $('.add-scoring').on('click', 'i', () => {
        $.ajax({
            type: 'GET',
            url: '/document/bonus/score/store',
            success: function(response)
            {
                var html='';
                html+= '<tr id="scoring_tr'+response.data.id+'" class="" role="row" class="hide odd">';
                html+=' <td><div class="form-check"><input type="checkbox" class="form-check-input selectedrowscoring" data-id="'+response.data.id+'" id="scoring'+response.data.id+'"><label class="form-check-label" for="scoring'+response.data.id+'"></label></div></td>';

                html+='<td class="pt-3-half edit_inline_scoring" data-id="'+response.data.id+'">';
                var location_create='';
                if (!$("#scoring_location_create").val()) {
                    var location_create='disabled';
                }
                html+='<select '+location_create+' onchange="editSelectedRow('+"scoring_tr"+response.data.id+')" class="location" data-id="'+response.data.id+'" id="location_id'+response.data.id+'"  style="background-color: inherit;border: 0px">';
                html+='<option>Select Location</option>';
                if(!response.flag)
                {
                    html+='<option value="0">All Location</option>';
                }
                for (var j = response.locations.length - 1; j >= 0; j--) {
                    // console.log(response.locations[j].score_location);
                    // if (response.locations[j].score_location ==null){
                        html+='<option value="'+response.locations[j].id+'">'+response.locations[j].name+'</option>';
                    // }
                    // html+='<option value="'+response.locations[j].id+'"  >'+response.locations[j].name+'</option>';
                }
                html+='</select></td>';
                html+='<td class="pt-3-half edit_inline_scoring" onkeypress="editSelectedRow('+"scoring_tr"+response.data.id+')" data-id="'+response.data.id+'" id="score'+response.data.id+'" ';
                if ($("#scoring_score_create").val()) {
                    html+='contenteditable="true"';
                }
                html+='></td>';
                html+='<td class="pt-3-half edit_inline_scoring"  onkeypress="editSelectedRow('+"scoring_tr"+response.data.id+')"data-id="'+response.data.id+'" id="max_visits'+response.data.id+'"';
                if ($("#scoring_max_visit_create").val()) {
                    html+='contenteditable="true"';
                }
                html+='></td>';
                html+='<td class="pt-3-half edit_inline_scoring" data-id="'+response.data.id+'">';
                var dashboard_create='';
                if (!$("#scoring_dashbroad_create").val()) {
                    var dashboard_create='disabled';
                }
                html+='<select '+dashboard_create+' onchange="editSelectedRow('+"scoring_tr"+response.data.id+')" searchable="Search here.."  id="dashboard_name'+response.data.id+'"  style="background-color: inherit;border: 0px">';
                html+='<option value="yes"'+checkSelectedScoring("yes",response.data.dashboard_name)+'>YES</option>';
                html+='<option value="no"'+checkSelectedScoring("no",response.data.dashboard_name)+'>NO</option>';
                html+='</select></td>';
                html+='<td class="pt-3-half edit_inline_scoring" onkeypress="editSelectedRow('+"scoring_tr"+response.data.id+')" data-id="'+response.data.id+'" id="notes'+response.data.id+'"';
                if ($("#scoring_notes_create").val()) {
                    html+='contenteditable="true"';
                }
                html+='></td>';
                html+='<td class="pt-3-half edit_inline_scoring" onkeypress="editSelectedRow('+"scoring_tr"+response.data.id+')"><a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light delete-btn'+response.data.id+'" onclick="deletescoringdata()"><i class="fas fa-trash"></i></a><a type="button" class="btn btn-primary btn-xs all_action_btn_margin my-0 waves-effect waves-light savescoredata show_tick_btn'+response.data.id+'" style="display:none" id="'+response.data.id+'"><i class="fas fa-check"></i></a></td>';
                html+='</tr>';
                console.log(html);
                $('#table-scoring').find('table').prepend(html);
                $('.location').find('option').prop('disabled', false);

                // $('.location').each(function() {
                //     $('.location').not(this).find('option[value="' + this.value + '"]').prop('disabled', true);
                // });
            },
            error: function (error) {
                console.log(error);
            }
        });
    });
    $('body').on('click', '.savescoredata ', function() {
        var company_id=$('#companies_selected_nav').children("option:selected").val();
        var tr_id=$(this).attr('id');
        var location_id=$('#location_id'+tr_id).children("option:selected").val();
        var check=$('#location_id'+tr_id).children("option:selected").is(':disabled');
        if(check)
        {
            toastr["error"]('All ready Selected');
            return;
        }
        var data={
            'id':tr_id,
            'location_id':location_id,
            'score':$('#score'+tr_id).html(),
            'max_visits':$('#max_visits'+tr_id).html(),
            'dashboard_name':$('#dashboard_name'+tr_id).children("option:selected").val(),
            'notes':$('#notes'+tr_id).html(),
            'old_location':$('#old_location'+tr_id).val(),
            'company_id':company_id,
        };
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url:'/document/bonus/score/update',
            data:data,
            success: function(response)
            {
                console.log(response);
                if(response.error)
                {
                    var errors=response.error.replace('location id','location');
                    toastr["error"](errors);
                    $("#scoring_tr"+tr_id).attr('style','background-color:#e3342f !important');
                }
                else
                {
                    toastr["success"](response.message);
                    save_and_delete_btn_toggle(tr_id);
                    if(response.flag==1)
                    {
                        $("#scoring_tr"+tr_id).attr('style','background-color:#8eeb8e !important');
                        var url='/document/bonus/score/getall';
                    }
                    else
                    {
                        $("#scoring_tr"+tr_id).attr('style','background-color:#e3342f !important');
                        var url='/document/bonus/score/getallsoft';
                    }
                    $('#scoring_data').DataTable().clear().destroy();
                    scoring_data(url);
                }
            }

        });

    });
    $('#restorebuttonscoring').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        selectedrows=[];
        $('#restorebuttonscoring').hide();
        $('#deleteselectedscoring').hide();
        $('#selectedactivebuttonscoring').show();
        $('#deleteselectedscoringSoft').show();
        $('#export_excel_scoring').hide();
        $('#export_world_scoring').hide();
        $('#export_pdf_scoring').hide();
        $('.add-scoring').hide();
        $('#activebuttonscoring').show();
        var url='/document/bonus/score/getallSoft';
        $('.scoring_table_static').DataTable().clear().destroy();
        scoring_data(url);
    });
    $('#activebuttonscoring').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        selectedrows=[];
        $('#export_excel_scoring').show();
        $('#export_world_scoring').show();
        $('#export_pdf_scoring').show();
        $('.add-scoring').show();
        $('#selectedactivebuttonscoring').hide();
        $('#deleteselectedscoring').show();
        $('#deleteselectedscoringSoft').hide();
        $('#restorebuttonscoring').show();
        $('#activebuttonscoring').hide();
        var url='/document/bonus/score/getall';
        $('.scoring_table_static').DataTable().clear().destroy();
        scoring_data(url);
    });
});
function restorescoringdata(id)
{
    var url='/document/bonus/score/active';
    swal({
            title: "Are you sure?",
            text: "Selected option with be active ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){

            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {'id':id},
                    success: function(response)
                    {
                        if(response.flag==1)
                        {
                            var url='/document/bonus/score/getall';
                        }
                        else
                        {
                            var url='/document/bonus/score/getallSoft';
                        }
                        $('.scoring_table_static').DataTable().clear().destroy();
                        scoring_data(url);
                        swal.close();
                        toastr["success"](response.message);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            } else {
                swal("Cancelled", "Your Record is safe", "error");

            }
        });
}
function delete_mutilpleDataScoring(url,selectedrows)
{
    if( $('.edit_cms_disable').css('display') == 'none' ) {
        console.log('Editer mode On Please change your mode');
        return 0;
    }
    if (selectedrows && selectedrows.length) {
        // not empty
        swal({
                title: "Are you sure?",
                text: "Selected option will be inactive and can be Active again by the admin only!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:url,
                        data: {'ids':selectedrows},
                        success: function(response)
                        {

                            toastr["success"](response.message);
                            if(response.flag==1)
                            {
                                var url='/document/bonus/score/getall';
                            }
                            else
                            {
                                var url='/document/bonus/score/getallSoft';
                            }
                            $('.scoring_table_static').DataTable().clear().destroy();
                            scoring_data(url);
                            swal.close();

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                    swal("Cancelled", "Your Record is safe", "error");

                }
            });
    }
    else
    {
        // empty
        return toastr["error"]("Select at least one record to delete!");
    }
}
function delete_data_scoring(id,url)
{
    swal({
            title: "Are you sure?",
            text: "Selected option with be Deleted ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){

            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {'id':id},
                    success: function(response)
                    {
                        if(response.flag==1)
                        {
                            var url='/document/bonus/score/getall';
                        }
                        else
                        {
                            var url='/document/bonus/score/getallSoft';
                        }
                        $('.scoring_table_static').DataTable().clear().destroy();
                        scoring_data(url);
                        swal.close();
                        toastr["success"](response.message);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            } else {
                swal("Cancelled", "Your Record is safe", "error");

            }
        });
}
function deletesoftscoringdata(id)
{
    selectedrows=[];
    var url='/document/bonus/score/softdelete';
    delete_data_scoring(id,url);

}
function deletescoringdata(id)
{
    selectedrows=[];
    var url='/document/bonus/score/delete';
    delete_data_scoring(id,url);
}

