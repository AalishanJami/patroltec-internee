$(document).ready(function() {
    var selectedrows=[];
    $('body').on('click','#location_checkbox_all',function() {
        var x = document.getElementsByClassName("selectedrowlocation");
        var i;
        for (i = 0; i < x.length;i++) {
            var tr_id=x[i].id;
            tr_id=tr_id.split('location');
            tr_id=tr_id[1];
            console.log(selectedrows);
            if($("#location_checkbox_all").is(":checked",true))
            {
                selectedrows.push(tr_id);
                $('.location_export').val(JSON.stringify(selectedrows));
                $('#location_tr'+tr_id).addClass('selected');
                $('#location'+tr_id).prop('checked',true);
            }
            else
            {
                selectedrows.splice( selectedrows.indexOf(tr_id), 1 );
                if (selectedrows.length >0){
                    $('.location_export').val(JSON.stringify(selectedrows));
                }else{
                    $('.location_export').val('1');
                }
                $('#location_tr'+tr_id).removeClass('selected');
                $('#location'+tr_id).prop('checked',false);
            }
        }

    });


    $('body').on('click', '.selectedrowlocation', function() {
        var tr_id=$(this).attr('id');
        tr_id=tr_id.split('location');
        tr_id=tr_id[1];
        console.log(selectedrows);
        if(selectedrows.includes(tr_id))
        {
            selectedrows.splice( selectedrows.indexOf(tr_id), 1 );
            if (selectedrows.length >0){
                $('.location_export').val(JSON.stringify(selectedrows));
            }else{
                $('.location_export').val('1');
            }
            $('#location_tr'+tr_id).removeClass('selected');
            // $('#location'+tr_id).attr('checked',false);

        }
        else
        {
            selectedrows.push(tr_id);
            $('.location_export').val(JSON.stringify(selectedrows));
            $('#location_tr'+tr_id).addClass('selected');
            // $('#location'+tr_id).attr('checked',true);

        }
    });
    $('#selectedactivebuttonlocation').click(function(){
        var url='job/location/active/multiple';
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        if (selectedrows && selectedrows.length) {
            // not empty
            swal({
                    title: "Are you sure?",
                    text: "Selected option will be active  again ..!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Yes, I am sure!',
                    cancelButtonText: "No, cancel it!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm){

                    if (isConfirm){
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $.ajax({
                            type: "POST",
                            url:url,
                            data: {'ids':selectedrows},
                            success: function(response)
                            {

                                toastr["success"](response.message);
                                var url='job/location/getallSoft';
                                if ($("#location_checkbox_all").prop('checked',true)){
                                    $("#location_checkbox_all").prop('checked',false);
                                }
                                $('#location_table').DataTable().clear().destroy();
                                location_data(url);
                                swal.close();

                            },
                            error: function (error) {
                                console.log(error);
                            }
                        });

                    } else {
                        swal("Cancelled", "Your Record is safe", "error");

                    }
                });
        }
        else
        {
            // empty
            return toastr["error"]("Select at least one record to delete!");
        }
    });
    $('#deleteselectedlocation').click(function(){
        var url='job/location/delete/multiple';
        delete_mutilpleData_location(url,selectedrows);
    });
    $('#deleteselectedlocationSoft').click(function(){
        var url='job/location/delete/soft/multiple';
        delete_mutilpleData_location(url,selectedrows);
    });

    $('.add-location').on('click', 'i', () => {

         var location_id=$('#location_id').children("option:selected").val();

          if(location_id=='' || location_id==null )
          {
            return  toastr["error"]("Select Location Please");
          }
        $.ajax({
            type: 'GET',
            url: 'job/location/store/'+location_id,
            success: function(response)
            {
                var location_checkbox=$('#joblocation_checkbox').val();
                var delete_location=$('#delete_joblocation').val();
                var hard_delete_location=$('#hard_delete_joblocation').val();
                var location_name=$('#joblocation_name').val();
                var edit_location=$('#joblocation_name_edit').val();

                var html='';
                var n_edit = ($("#joblocation_name_create").val()==1) ? "true":"false";
                html+= '<tr id="location_tr'+response.data.id+'" role="row" class="hide odd">';
                if (location_checkbox==1){
                    html+=' <td><div class="form-check"><input type="checkbox" class="form-check-input location_checked selectedrowlocation" id="location'+response.data.id+'"><label class="form-check-label" for="location'+response.data.id+'"></label></div></td>';
                }

                if(location_name==1){
                    html+='<td class="pt-3-half edit_inline_location" onkeypress="editSelectedRow('+"location_tr"+response.data.id+')" data-id="'+response.data.id+'" id="nameD'+response.data.id+'"   contenteditable="'+n_edit+'"></td>';
                }
                html+='<td>';
                if (delete_location ==1){
                    html+='<a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light" onclick="deletelocationdata()"><i class="fas fa-trash"></i></a>';
                }
                if (edit_location ==1){
                    html+='<a type="button" style="display: none;" class="btn all_action_btn_margin btn-primary btn-xs my-0 waves-effect waves-light savelocationdata location_show_button_'+response.data.id+'" id="'+response.data.id+'"><i class="fas fa-check"></i></a>';
                }
                html+='</td>';
                html+='</tr>';
                $('#location_table_body').prepend(html);

            },
            error: function (error) {
                console.log(error);
            }
        });


    });
    $('body').on('click', '.savelocationdata', function() {
        var company_id=$('#companies_selected_nav').children("option:selected").val();
        var tr_id=$(this).attr('id');
        // tr_id=tr_id.split('location_tr');
        // tr_id=tr_id[1];
        console.log($('#nameD'+tr_id).html());
        var data={
            'id':tr_id,
            'name':$('#nameD'+tr_id).html(),
            'company_id':company_id,
        };
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url:'job/location/update',
            data:data,
            success: function(response)
            {
                if(response.error)
                {
                    toastr["error"](response.error);
                }
                else
                {
                    toastr["success"](response.message);
                    if(response.flag==1)
                    {
                        $("#location_tr"+tr_id).attr('style','background-color:#90ee90 !important');
                        // var url='job/location/getall';
                    }
                    else
                    {
                        $("#location_tr"+tr_id).attr('style','background-color:#ff3547 !important');
                        // var url='job/location/getallsoft';
                    }
                    // $('#location_table').DataTable().clear().destroy();
                    // location_data(url);

                }

            }

        });
        // console.log(data);
    });

    $('#restorebuttonlocation').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        if ($("#location_checkbox_all").prop('checked',true)){
            $("#location_checkbox_all").prop('checked',false);
        }
        $('#restorebuttonlocation').hide();
        $('#deleteselectedlocation').hide();
        $('#selectedactivebuttonlocation').show();
        $('#deleteselectedlocationSoft').show();
        $('#export_excel_location').hide();
        $('#export_world_location').hide();
        $('#export_pdf_location').hide();
        $('.add-location').hide();
        $('#activebuttonlocation').show();
        var url='job/location/getallSoft';
        $('#location_table').DataTable().clear().destroy();
        location_data(url);
    });
    $('#activebuttonlocation').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        if ($("#location_checkbox_all").prop('checked',true)){
            $("#location_checkbox_all").prop('checked',false);
        }
        $('#export_excel_location').show();
        $('#export_world_location').show();
        $('#export_pdf_location').show();
        $('.add-location').show();
        $('#selectedactivebuttonlocation').hide();
        $('#deleteselectedlocation').show();
        $('#deleteselectedlocationSoft').hide();
        $('#restorebuttonlocation').show();
        $('#activebuttonlocation').hide();
        var url='job/location/getall';
        $('#location_table').DataTable().clear().destroy();
        location_data(url);
    });

});
function restorelocationdata(id)
{
    var url='job/location/active';
    swal({
            title: "Are you sure?",
            text: "Selected option with be active ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){

            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {'id':id},
                    success: function(response)
                    {
                        if(response.flag==1)
                        {
                            var url='job/location/getall';
                        }
                        else
                        {
                            var url='job/location/getallSoft';
                        }
                        $('#location_table').DataTable().clear().destroy();
                        location_data(url);
                        swal.close();
                        toastr["success"](response.message);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            } else {
                swal("Cancelled", "Your Record is safe", "error");

            }
        });
}
function delete_mutilpleData_location(url,selectedrows)
{
    if( $('.edit_cms_disable').css('display') == 'none' ) {
        console.log('Editer mode On Please change your mode');
        return 0;
    }
    if (selectedrows && selectedrows.length) {
        // not empty
        swal({
                title: "Are you sure?",
                text: "Selected option will be inactive and can be Active again by the admin only!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:url,
                        data: {'ids':selectedrows},
                        success: function(response)
                        {

                            toastr["success"](response.message);
                            if(response.flag==1)
                            {
                                var url='job/location/getall';
                            }
                            else
                            {
                                var url='job/location/getallSoft';
                            }
                            if ($("#location_checkbox_all").prop('checked',true)){
                                $("#location_checkbox_all").prop('checked',false);
                            }
                            $('#location_table').DataTable().clear().destroy();
                            location_data(url);
                            swal.close();

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                    swal("Cancelled", "Your Record is safe", "error");

                }
            });
    }
    else
    {
        // empty
        return toastr["error"]("Select at least one record to delete!");
    }
}
function delete_data_location(id,url)
{
    swal({
            title: "Are you sure?",
            text: "Selected option with be Deleted ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){

            if (isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {'id':id},
                    success: function(response)
                    {
                        if(response.flag==1)
                        {
                            var url='job/location/getall';
                        }
                        else
                        {
                            var url='job/location/getallSoft';
                        }
                        $('#location_table').DataTable().clear().destroy();
                        location_data(url);
                        swal.close();
                        toastr["success"](response.message);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            } else {
                swal("Cancelled", "Your Record is safe", "error");

            }
        });
}
function deletesoftlocationdata(id)
{
    var url='job/location/softdelete';
    delete_data_location(id,url);

}
function deletelocationdata(id)
{
    var url='job/location/delete';
    delete_data_location(id,url);
}
