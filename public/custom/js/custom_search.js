$(document).ready(function () {
    $('body').on('click', '.search', function() {
        var serach_array={};
        $( '.custom_search' ).each(function() {
            if($(this).val())
            {
                serach_array[$(this).attr('id')]=$(this).val();
            }
        });
        if( $('.type').val() == 2)
        {
            $('#qr_table').DataTable().clear().destroy();
            var url="/report/qr/customSearch";
            qr_data(url,'POST',serach_array);
        }
        else
        {
            $('#plant_and_equipment_table').DataTable().clear().destroy();
            var url="/report/customSearch";
            equipment_data(url,'POST',serach_array);
        }
    });
});
