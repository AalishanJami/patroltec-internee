 $(document).ready(function() {
    var selectedrows=[];
     $('body').on('click','#relationship_checkbox_all',function() {
         $('.selectedrowrelationship').not(this).prop('checked', this.checked);
         $('.selectedrowrelationship').each(function(key,value) {
             var id=$(this).attr('id');
             console.log(id);
             if(id)
             {
                 id=id.split('relationship_checked');
                 row_id='#relationship_tr'+id[1];
                 if($(this).is(":checked"))
                 {
                     if(key == 0)
                     {
                         selectedrows=[];
                     }
                     if(id[1] != '_checkbox_all')
                     {
                         selectedrows.push(id[1]);
                     }
                     $('.relationship_export').val(JSON.stringify(selectedrows));
                     $(row_id).addClass('selected');
                 }
                 else
                 {
                     $('.relationship_export').val(1);
                     selectedrows=[];
                     $(row_id).removeClass('selected');
                 }
             }
         });
     });


     $('body').on('click', '.selectedrowrelationship', function() {
         var id=$(this).attr('id');
         id=id.split('relationship_checked');
         row_id='#relationship_tr'+id[1];
         if($(this).is(":checked"))
         {
             selectedrows.push(id[1]);
             $('.relationship_export').val(JSON.stringify(selectedrows));
             $(row_id).addClass('selected');
         }else
         {
             if(selectedrows.includes(id[1]))
             {
                 selectedrows.splice( selectedrows.indexOf(id[1]),1);
                 $('.relationship_export').val(JSON.stringify(selectedrows));
                 $(row_id).removeClass('selected');
             }
         }
    });
    $('#selectedactivebuttonrelationship').click(function(){
        var url='/relationship/active/multiple';
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        if (selectedrows && selectedrows.length) {
       // not empty
            swal({
                title: "Are you sure?",
                text: "Selected option will be active  again ..!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
             },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:url,
                        data: {'ids':selectedrows},
                        success: function(response)
                        {

                            toastr["success"](response.message);
                            var url='/relationship/getallSoft';
                            $('#relationship_table').DataTable().clear().destroy();
                            relationship_data(url);
                            swal.close();

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                  swal("Cancelled", "Your Record is safe", "error");

                }
            });
         }
        else
        {
       // empty
            return toastr["error"]("Select at least one record to delete!");
        }
    });
    $('#deleteselectedrelationship').click(function(){
       var url='/relationship/delete/multiple';
       delete_mutilpleData(url,selectedrows);
    });
    $('#deleteselectedrelationshipSoft').click(function(){
       var url='/relationship/delete/soft/multiple';
       delete_mutilpleData(url,selectedrows);
    });

    $('.add_relation').on('click', 'i', () => {
      $.ajax({
            type: 'GET',
            url: '/relationship/store',
            success: function(response)
            {
                var html='';
                html+= '<tr id="relationship_tr'+response.data.id+'" class="selectedrowrelationship'+response.data.id+'" role="row" class="hide odd">';
                html+=' <td><div class="form-check"><input type="checkbox" class="form-check-input relationship_checked selectedrowrelationship" id="relationship_checked'+response.data.id+'"><label class="form-check-label" for="relationship_checked'+response.data.id+'""></label></div></td>';
                html+='<td onkeypress="editSelectedRow('+"relationship_tr"+response.data.id+')" class="pt-3-half edit_inline_relationship" id="name'+response.data.id+'" data-id="'+response.data.id+'" ';
                if($('#relationship_create').val())
                {
                    html+='contenteditable="true"';
                }
                html+='></td>';
                html+='<td>';
                html+='<a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light deleterelationshipdata'+response.data.id+'" onclick="deleterelationshipdata()"><i class="fas fa-trash"></i></a>';

                if($('#relationship_create').val())
                {
                    html+='<a type="button" class="btn btn-primary all_action_btn_margin btn-xs my-0 waves-effect waves-light saverelationshipdata saverelationshipdata_show_'+response.data.id+'" style="display: none;" id="'+response.data.id+'"><i class="fas fa-check"></i></a>';
                }
                html+='</td>';
                html+='</tr>';
                $('#table_relationship').find('table').prepend(html);

            },
            error: function (error) {
                console.log(error);
            }
        });


    });
    $('body').on('click', '.saverelationshipdata', function() {
        var company_id=$('#companies_selected_nav').children("option:selected").val();
        var tr_id=$(this).attr('id');
        var data={
            'id':tr_id,
            'name':$('#name'+tr_id).html(),
            'company_id':company_id,
        };
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url:'/relationship/update',
            data:data,
            success: function(response)
            {
                if(response.error)
                {
                    toastr["error"](response.error);
                    $("#relationship_tr"+tr_id).attr('style',"background-color: #fc685f");
                }
                else
                {
                    toastr["success"](response.message);
                    if(response.flag==1)
                    {
                        var url='/answer/getall';
                    }
                    else
                    {
                        var url='/answer/getallsoft';
                    }
                    $('.deleterelationshipdata'+tr_id).show();
                    $('.saverelationshipdata_show_'+tr_id).hide();
                    $("#relationship_tr"+tr_id).attr('style',"background-color: lightgreen !important;");

                }
            }

        });

    });

    $('#restorebuttonrelationship').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        selectedrows=[];
        $('#restorebuttonrelationship').hide();
        $('#deleteselectedrelationship').hide();
        $('#selectedactivebuttonrelationship').show();
        $('#deleteselectedrelationshipSoft').show();
        $('#export_excel_relationship').hide();
        $('#export_world_relationship').hide();
        $('#export_pdf_relationship').hide();
        $('.add_relation').hide();
        $('#activebuttonrelationship').show();
        var url='/relationship/getallSoft';
        $('#relationship_table').DataTable().clear().destroy();
        relationship_data(url);
    });
    $('#activebuttonrelationship').click(function(){
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        selectedrows=[];
        $('#export_excel_relationship').show();
        $('#export_world_relationship').show();
        $('#export_pdf_relationship').show();
        $('.add_relation').show();
        $('#selectedactivebuttonrelationship').hide();
        $('#deleteselectedrelationship').show();
        $('#deleteselectedrelationshipSoft').hide();
        $('#restorebuttonrelationship').show();
        $('#activebuttonrelationship').hide();
        var url='/relationship/getall';
        $('#relationship_table').DataTable().clear().destroy();
        relationship_data(url);
    });



});
 function restorerelationshipdata(id)
 {
    var url='/relationship/active';
    swal({
        title: "Are you sure?",
        text: "Selected option with be active ",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
     },
     function(isConfirm){

       if (isConfirm){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
             $.ajax({
                        type: "POST",
                        url: url,
                        data: {'id':id},
                        success: function(response)
                        {
                            if(response.flag==1)
                            {
                                var url='/relationship/getall';
                            }
                            else
                            {
                                var url='/relationship/getallSoft';
                            }
                            $('#relationship_table').DataTable().clear().destroy();
                            relationship_data(url);
                            swal.close();
                            toastr["success"](response.message);
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
        } else {
          swal("Cancelled", "Your Record is safe", "error");

        }
     });
 }
 function delete_mutilpleData(url,selectedrows)
 {
        if( $('.edit_cms_disable').css('display') == 'none' ) {
            console.log('Editer mode On Please change your mode');
            return 0;
        }

        if (selectedrows && selectedrows.length) {
       // not empty
            swal({
                title: "Are you sure?",
                text: "Selected option will be inactive and can be Active again by the admin only!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
             },
            function(isConfirm){

                if (isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url:url,
                        data: {'ids':selectedrows},
                        success: function(response)
                        {

                            toastr["success"](response.message);
                            if(response.flag==1)
                            {
                                var url='/relationship/getall';
                            }
                            else
                            {
                                var url='/relationship/getallSoft';
                            }
                            $('#relationship_table').DataTable().clear().destroy();
                            relationship_data(url);
                            swal.close();

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                } else {
                  swal("Cancelled", "Your Record is safe", "error");

                }
            });
         }
        else
        {
       // empty
            return toastr["error"]("Select at least one record to delete!");
        }
 }
 function delete_data_relationship(id,url)
 {
    swal({
        title: "Are you sure?",
        text: "Selected option with be Deleted ",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
     },
     function(isConfirm){

       if (isConfirm){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
             $.ajax({
                        type: "POST",
                        url: url,
                        data: {'id':id},
                        success: function(response)
                        {
                            if(response.flag==1)
                            {
                                var url='/relationship/getall';
                            }
                            else
                            {
                                var url='/relationship/getallSoft';
                            }
                            $('#relationship_table').DataTable().clear().destroy();
                            relationship_data(url);
                            swal.close();
                            toastr["success"](response.message);
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
        } else {
          swal("Cancelled", "Your Record is safe", "error");

        }
     });
 }
function deletesoftrelationshipdata(id)
{
    var url='/relationship/softdelete';
    delete_data_relationship(id,url);

}
function deleterelationshipdata(id)
 {
    var url='/relationship/delete';
    delete_data_relationship(id,url);
 }
