function editSelectedRow(id){
    $("#"+id.id).removeClass('selected');
    $("#"+id.id).addClass('selected_row');
    $("#"+id.id).attr('style','');
    //splite  id.id result to fetch only integer id e.g 4
    var split_id =(id.id).replace(/\'/g, '').split(/(\d+)/).filter(Boolean);
    var row_id = split_id[split_id.length - 1];
    $(".delete-btn"+row_id).hide();
    $(".show_tick_btn"+row_id).show();
    $('.showAnswer'+row_id).attr('hidden',false);
    $('.delete_answer'+row_id).hide()
}


function save_and_delete_btn_toggle(id){
    $(".delete-btn"+id).show();
    $(".show_tick_btn"+id).hide();
}


$("body").on("click",":checkbox",function(){
   var tr_id=$(this).parents('tr').attr('id');
   $("#"+tr_id).attr('style','');
   $("#"+tr_id).removeClass('selected_row');
});
$('body').on('click','th>.form-check>input:checkbox',function() {
    $("tr").attr('style','');
    $("tr").removeClass('selected_row');
});
$(document).ready(function() {
    $('.companies_selected').unbind().change(function() {
        var company_id=$(this).children("option:selected").val();
        // var url=$('.current_path').val();
        if(company_id)
        {
            var token = $("meta[name='csrf-token']").attr("content");
            $.ajax({
                url: '/change/company',
                method: "POST",
                data: {
                    default_company:company_id,
                    _token: token
                },
                success: function(response) {
                    toastr["success"](response.message);
                    if(response.flag == 1)
                    {
                        var url = localStorage.getItem('current_path');
                        var url='/'+url;
                        console.log(url);
                        window.location = url;
                    }
                    else
                    {
                        location.reload(true);
                    }
                }
            });
        }
    });
    $('body').on('click', '.sidebarbtn', function(e) {
        if ($(this).css('left') != '250px') {
            console.log('open');
            $('.remove-padding').removeClass("padding-left-custom");
        } else {
            console.log('close');
            $('.remove-padding').addClass("padding-left-custom");
        }
    });
    $('body').on('click', '.fa-question-circle', function(e) {
        var id = $(this).attr('aria-describedby');
        var data = $('#' + id).find('.popover-body').html();
        $('.fa-question-circle').not(this).popover('hide');
        var content = data.split("---");
        console.log(content);
        if (content.length == 2) {
            if ($('.edit_cms_disable').css('display') == 'none') {
                var html = '<span data-id="' + content[0] + '"data-value="' + content[1] + '"class="assign_class get_text label' + content[0] + '">' + content[1] + '</span>';
            } else {
                var html = '<span data-id="' + content[0] + '"data-value="' + content[1] + '"class="assign_class label' + content[0] + '">' + content[1] + '</span>';
            }
            console.log(html);
            $('.popover-body').html(html);
        }
    });

    $('.close').click(function(e) {
        $('input[type="search"]').each(function(key,value) {
            $(this).val('');
        });
    });
    $('body').on('keyup','input[type="search"]', function(e) {
        $('input[type="checkbox"]').prop('checked',false);
        // console.log('ddd');
        $('tr').removeClass('selected');
        // console.log($(this).val());
        if(!$(this).val() || $(this).val() === "")
        {
          console.log('empty');
          $('input[name="pdf_array"]').val(1);
          $('input[name="word_array"]').val(1);
          $('input[name="excel_array"]').val(1);
        }
    });
    $('.form_submit_check').click(function(e) {
        var search='';
        var custom_id='';
        $('input[type="search"]').each(function(key,value) {
            if($(this).val())
            {
                search=$(this).val();
                custom_id='#'+$(this).attr('aria-controls')+' tbody tr';
            }
        });
        var array=[];
        $(custom_id).each(function() {
            array.push($(this).attr('data-id'));
        });
        if(search)
        {
            $('input[name="pdf_array"]').val(JSON.stringify(array));
            $('input[name="word_array"]').val(JSON.stringify(array));
            $('input[name="excel_array"]').val(JSON.stringify(array));
        }
        else if($('input[name="word_array"]').val() == '[]' && $('input[name="excel_array"]').val() == '[]' && $('input[name="pdf_array"]').val() == '[]' && $('input[name="pdf_array"]').val() == 1 && $('input[name="word_array"]').val() == 1 && $('input[name="excel_array"]').val() == 1 )
        {

            $('input[name="pdf_array"]').val(1);
            $('input[name="word_array"]').val(1);
            $('input[name="excel_array"]').val(1);
        }
        else if($('input[name="word_array"]').val() == '[]' && $('input[name="excel_array"]').val() == '[]' && $('input[name="pdf_array"]').val() == '[]' )
        {
            $('input[name="pdf_array"]').val(1);
            $('input[name="word_array"]').val(1);
            $('input[name="excel_array"]').val(1);
        }

        if ($('.edit_cms_disable').css('display') == 'none') {
            e.preventDefault();
            // console.log('Editer mode On Please change your mode');
            return 0;
        }
    });
    $('#close_model').click(function() {
        if ($('.edit_cms_disable').css('display') == 'none') {
            console.log('Editer mode On Please change your mode');
            return 0;
        }
        $('#RoleModal1').modal('hide');
    });

    function addCookies() {
        var data_table=[];
        $( '.dataTables_paginate,.paging_simple_numbers' ).each(function() {
            var id= $( this ).attr('id');
            // data_table[id] =$('#'+id).html();
            data_table.push(
                { 'id' : id, 'html' : $('#'+id).html() }
            );
            }
        );
        window.sessionStorage.setItem("data_table", JSON.stringify(data_table));
    }
    $('body').on('click', '.edit_cms_switch', function(e) {
        addCookies();
        var check = $(this).attr('data-class');
        var data_table = JSON.parse(sessionStorage.getItem("data_table"));
        if (check == 'edit-enable') {
            $('.edit_cms_enable').show();
            if (data_table) {
                    $( data_table ).each(function(index,value) {
                    var temp = value['html'].substring(
                        value['html'].lastIndexOf('<ul class="pagination">') + 1,
                        value['html'].lastIndexOf('</ul>')
                    );
                    temp = temp.split("<li class=");
                    var previous = temp[1].split("<span");
                    if(previous[1])
                    {
                        var previous_btn = previous[1].split("</span");
                        previous_btn = '<li class="previous"><span' + previous_btn[0] + '</span></li>';
                        if(temp[temp.length-1] &&  temp.length !=2)
                        {
                            var next = temp[temp.length-1].split("<span");
                            var next_btn = next[1].split("</span");
                            next_btn = '<li class="next"><span' + next_btn[0] + '</span></li>';
                            var new_ul_listing ='<ul class="pagination">'+ previous_btn + '<li class='  + next_btn +'</ul>';
                            $('#'+value['id']).html(new_ul_listing);

                        }
                        else if(temp[2])
                        {
                            var next = temp[2].split("<span");
                            var next_btn = next[1].split("</span");
                            next_btn = '<li class="next"><span' + next_btn[0] + '</span></li>';
                            var new_ul_listing ='<ul class="pagination">'+  previous_btn + '<li class='  + next_btn +'</ul>';
                           $('#'+value['id']).html(new_ul_listing);
                        }
                    }
                });
            }
            $('.assign_class').addClass("get_text");
            $('.edit_cms_disable').hide();
        } else {
            $( data_table ).each(function(index,value) {
                $('#'+value['id']).html(value['html']);
            });
            $('.edit_cms_enable').hide();
            $('.assign_class').removeClass("get_text");
            $('.edit_cms_disable').show();
        }
    });
    $('#myModal .save-button').unbind().click(function() {
        var data_id = $('#modal_text_id').val();
        var data_value = $('#body-text').summernote('code');
        var token = $("meta[name='csrf-token']").attr("content");
        var placeholders = $('.note-editable ').html();
        $.ajax({
            url: "/changeText",
            method: "POST",
            data: {
                data_id: data_id,
                data_value: data_value,
                _token: token
            },
            success: function(data) {
                $('.label' + data_id).attr('data-value', data.message);
                $('.label' + data_id).html(data.message);
                toastr["success"]("Label Successfully Updated");
            }
        });
    });
    $('body').on('click', '.get_text', function(e) {
        e.preventDefault();
        var textValue = $(this).data('value');
        var data_id = $(this).data('id');
        $("#modal_text_id").val(data_id);
        $("#modal_text_value").val($.trim(textValue));
        $("#body-text").summernote('code', '');
        $("#body-text").summernote('code',$.trim($(this).html()));
        $('.summernote').summernote({
            height: 300,
            tabsize: 2,
            toolbar: [
                // [groupName, [list of button]]
                // ['style', ['bold', 'italic', 'underline', 'clear']],
                // ['font', ['strikethrough', 'superscript', 'subscript']],
                // ['fontsize', ['fontsize']],
                // ['color', ['color']],
                // ['para', ['ul', 'ol', 'paragraph']],
                // ['height', ['height']]
            ],
        });
        $('#myModal').modal('show');
    });

    $('.modal').on('shown.bs.modal', function (e) {
        $('.error_message_text').html('');
        $('.error_message').html('');
        $('span.text-danger').html('');
        $('small.text-danger').html('');
        $('span').siblings('input').attr('style','');
        $('small').siblings('input').attr('style','');
        $('span').siblings('textarea').attr('style','');
        $('small').siblings('textarea').attr('style','');

        $('select').siblings('input').attr('style','');
        $('footer.page-footer').attr('style','z-index:1000');
    });

    /*****tooltip*********/

    setTimeout(function (){
        if ($(".fa-eye").parents('a').attr('title',false)){
            $(".fa-eye").parents('a').attr('datatoggle','tooltip');
            $(".fa-eye").parents('a').attr('title','View Record');
        }

        if ($(".fa-trash").parents('a').attr('title',false)){
            $(".fa-trash").parents('a').attr('datatoggle','tooltip');
            $(".fa-trash").parents('a').attr('title','Delete Record');
        }

        if ($(".fa-pencil-alt").parents('a').attr('title',false)){
            $(".fa-pencil-alt").parents('a').attr('datatoggle','tooltip');
            $(".fa-pencil-alt").parents('a').attr('title','Edit Record');
        }

        if ($(".fa-angle-double-up").parents('a').attr('title',false)){
            $(".fa-angle-double-up").parents('a').attr('datatoggle','tooltip');
            $(".fa-angle-double-up").parents('a').attr('title','Raise Job');
        }

        if ($(".fa-copy").parents('a').attr('title',false)){
            $(".fa-copy").parents('a').attr('datatoggle','tooltip');
            $(".fa-copy").parents('a').attr('title','Duplicate Record');
        }

        if ($(".fa-plus").parents('a').attr('title',false)){
            $(".fa-plus").parents('a').attr('datatoggle','tooltip');
            $(".fa-plus").parents('a').attr('title','Add Record');
        }

        if ($(".fa-check").parents('a').attr('title',false)){
            $(".fa-check").parents('a').attr('datatoggle','tooltip');
            $(".fa-check").parents('a').attr('title','Update Record');
        }

        // $('[data-toggle="tooltip"]').tooltip();
        $('[datatoggle="tooltip"]').tooltip();
    },5000);
     /*******tooltip end******/


    $("body").on('keypress','input',function(){
        $(this).siblings('.error_message_text').html('');
        $(this).siblings('span.text-danger').html('');
        $(this).siblings('small.text-danger').html('');
        $(this).attr('style','');
    });
    $("body").on('keypress','textarea',function(){
        $(this).siblings('.error_message_text').html('');
        $(this).siblings('span.text-danger').html('');
        $(this).siblings('small.text-danger').html('');
        $(this).attr('style','');
    });

    $("body").on('change','select',function(){
        $(this).siblings('.error_message_text').html('');
        $(this).siblings('.error_message').html('');

        $(this).parent('div').siblings('.error_message_text').html('');
        $(this).parent('div').siblings('.error_message').html('');
        $(this).parents('div').siblings('.error_message').html('');
        $(this).parents('div').siblings('.error_message_text').html('');

        $(this).siblings('span.text-danger').html('');
        $(this).siblings('small.text-danger').html('');

        $(this).parent('div').siblings('span.text-danger').html('');
        $(this).parent('div').siblings('small.text-danger').html('');


        $(this).siblings('input').attr('style','');
    });

    $("body").on('change','.datepicker ',function(){
        $(this).siblings('.error_message_text').html('');
        $(this).siblings('.error_message').html('');
        $(this).siblings('span.text-danger').html('');
        $(this).siblings('small.text-danger').html('');
        $(this).attr('style','');
    });



    $("body").on('keydown','.dataTables_filter>label>input',function(){
        var dataTables_filter= $(this).attr("aria-controls");
        setTimeout(function(){
            var table = $('#'+dataTables_filter).dataTable();
            var total=table.$('tr', {"filter":"applied"}).length;
            if (total>=250){
                $("#"+dataTables_filter+"_wrapper").prevAll('input').prev('form').attr('style','display:none;');
                $("#"+dataTables_filter+"_wrapper").prevAll('span').prev('form').attr('style','display:none;');
                $("#"+dataTables_filter+"_wrapper").parents('div').prev('form').attr('style','display:none;');
            }else{
                $("#"+dataTables_filter+"_wrapper").prevAll('input').prev('form').attr('style','');
                $("#"+dataTables_filter+"_wrapper").prevAll('span').prev('form').attr('style','');
                $("#"+dataTables_filter+"_wrapper").parents('div').prev('form').attr('style','');
            }

            if ($(this.target).val()==""){
                var info = table.page.info();
                var count = info.recordsTotal;
                if (count>=250){
                    $("#"+dataTables_filter+"_wrapper").prevAll('input').prev('form').attr('style','display:none;');
                    $("#"+dataTables_filter+"_wrapper").prevAll('span').prev('form').attr('style','display:none;');
                    $("#"+dataTables_filter+"_wrapper").parents('div').prev('form').attr('style','display:none;');
                }else{
                    $("#"+dataTables_filter+"_wrapper").prevAll('input').prev('form').attr('style','');
                    $("#"+dataTables_filter+"_wrapper").prevAll('span').prev('form').attr('style','');
                    $("#"+dataTables_filter+"_wrapper").parents('div').prev('form').attr('style','');
                }
            }
        },1000);
    });


});


/*$('body').on('click','button', function() {
    console.log('button');
    $('button').removeClass('current');
    $('a').removeClass('current');
    $(this).addClass('current');
});
$('body').on('click','a', function() {
    console.log('a');
    $('a').removeClass('current');
    $('button').removeClass('current');
    $(this).addClass('current');
});*/
