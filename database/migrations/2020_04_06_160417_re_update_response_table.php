<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ReUpdateResponseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('responses');
        Schema::enableForeignKeyConstraints();
        Schema::create('responses', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('response_group_id')->nullable();
            $table->foreign('response_group_id')->references('id')->on('response_groups')->onDelete('cascade');

            $table->unsignedBigInteger('company_id')->nullable();
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
            
            $table->unsignedBigInteger('job_id')->nullable();
            $table->foreign('job_id')->references('id')->on('jobs')->onDelete('cascade');

            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');


            $table->string('response_type')->nullable();

            $table->unsignedBigInteger('question_id')->nullable();
            $table->foreign('question_id')->references('id')->on('questions')->onDelete('cascade');

            $table->unsignedBigInteger('answer_id')->nullable();
            $table->foreign('answer_id')->references('id')->on('answers')->onDelete('cascade');

            $table->unsignedBigInteger('form_section_id')->nullable();
            $table->foreign('form_section_id')->references('id')->on('form_sections')->onDelete('cascade');

            $table->string('answer_text')->nullable();
            $table->string('comments')->nullable();
            $table->string('image',250)->nullable();
            $table->string('file_name',250)->nullable();
            $table->integer('score')->nullable();
            $table->boolean('pass')->nullable();
            $table->boolean('fail')->nullable();
            $table->boolean('active')->default(1)->nullable();
            $table->boolean('deleted')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('responses');
    }
}
