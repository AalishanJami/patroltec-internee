<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Update3JobGroups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('job_groups', function (Blueprint $table) {
         $table->unsignedBigInteger('schedule_task_id')->nullable();
            $table->foreign('schedule_task_id')->references('id')->on('schedule_tasks')->onDelete('cascade');
            $table->timestamp('date_time')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
