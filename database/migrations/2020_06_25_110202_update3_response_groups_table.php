<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Update3ResponseGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('response_groups', function (Blueprint $table) {

            $table->unsignedBigInteger('location_breakdown_id')->nullable();
            $table->foreign('location_breakdown_id')->references('id')->on('location_breakdowns')->onDelete('cascade');
            $table->unsignedBigInteger('pre_populate_id')->nullable();
            $table->foreign('pre_populate_id')->references('id')->on('pre_populates')->onDelete('cascade');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
