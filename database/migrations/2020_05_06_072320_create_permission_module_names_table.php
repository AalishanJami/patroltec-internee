<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermissionModuleNamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permission_module_names', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('module_name')->nullable();
            $table->unsignedBigInteger('permission_groups_id')->nullable();
            $table->foreign('permission_groups_id')->references('id')->on('permission_groups')->onDelete('cascade');    
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permission_module_names');
    }
}
