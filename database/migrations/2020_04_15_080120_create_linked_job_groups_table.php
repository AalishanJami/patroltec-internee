<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinkedJobGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('linked_job_groups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('new_job_group_id')->nullable();
            $table->foreign('new_job_group_id')->references('id')->on('job_groups')->onDelete('cascade');

             $table->unsignedBigInteger('old_job_group_id')->nullable();
            $table->foreign('old_job_group_id')->references('id')->on('job_groups')->onDelete('cascade');
                $table->unsignedBigInteger('new_job_id')->nullable();
            $table->foreign('new_job_id')->references('id')->on('jobs')->onDelete('cascade');    $table->unsignedBigInteger('old_job_id')->nullable();
            $table->foreign('old_job_id')->references('id')->on('jobs')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('linked_job_groups');
    }
}
