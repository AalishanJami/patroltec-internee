<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateScheduleTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
           Schema::table('schedule_tasks', function (Blueprint $table) {
            $table->string('post_time_select')->nullable();
            $table->string('pre_time_select')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('schedule_tasks', function (Blueprint $table) {
         $table->dropColumn('post_time_select');
         $table->dropColumn('pre_time_select');
        });
    }
}
