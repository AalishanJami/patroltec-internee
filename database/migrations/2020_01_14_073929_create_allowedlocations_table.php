<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAllowedlocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('allowedlocations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('locations_id')->nullable();
            $table->foreign('locations_id')->references('id')->on('locations')->onDelete('cascade');
            $table->unsignedBigInteger('active_user_id')->nullable();
            $table->unsignedBigInteger('locations_groups_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('allowedlocations');
    }
}
