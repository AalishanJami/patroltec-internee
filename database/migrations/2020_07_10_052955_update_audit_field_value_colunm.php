<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAuditFieldValueColunm extends Migration
{
    public function up()
    {
        Schema::table('audit_fields', function (Blueprint $table) {
            $table->longText('new_value')->nullable()->change();
            $table->longText('old_value')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
