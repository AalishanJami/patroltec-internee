<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ReUpdateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('jobs', function (Blueprint $table) {

           /* $table->dropForeign(['company_id']);
            $table->dropForeign(['user_id']);
            $table->dropForeign(['form_id']);
            $table->dropForeign(['location_id']);
            $table->dropForeign(['contractor_id']);
            $table->dropForeign(['user_group_id']);
            $table->dropForeign(['form_type_id']);
            $table->dropForeign(['question_id']);
            $table->dropForeign(['schedule_id']);
            $table->dropColumn(['company_id', 'user_id', 'form_id', 'location_id', 'contractor_id', 'user_group_id', 'form_type_id', 'question_id', 'schedule_id']);*/

            $table->timestamp('date_time')->nullable()->change();
            $table->timestamp('pre_time')->nullable()->change();
            $table->timestamp('post_time')->nullable()->change();


//            $table->unsignedBigInteger('job_group_id')->nullable();
//            $table->foreign('job_group_id')->references('id')->on('job_group')->onDelete('cascade');
//            $table->unsignedBigInteger('form_section_id')->nullable();
//            $table->foreign('form_section_id')->references('id')->on('form_sections')->onDelete('cascade');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
