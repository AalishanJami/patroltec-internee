<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('first_name',250)->nullable();
            $table->string('surname',250)->nullable();
            $table->unsignedBigInteger('manager_id')->nullable();
            $table->unsignedBigInteger('client_id')->nullable();
            $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade');
            $table->string('position',1)->nullable();
            $table->string('sex',1)->nullable();
            $table->date('dob')->nullable();
            $table->string('payroll_number',50)->nullable();
            $table->string('national_Insurance_number',50)->nullable();
            $table->string('cscs_Card',50)->nullable();
            $table->date('cscs_Card_Expiry_Date',50)->nullable();
            $table->unsignedBigInteger('sexual_orientation_id')->nullable();
            $table->foreign('sexual_orientation_id')->references('id')->on('sexual_orientations')->onDelete('cascade');
            $table->unsignedBigInteger('ethinic_origin_id')->nullable();
            $table->foreign('ethinic_origin_id')->references('id')->on('ethinic_origins')->onDelete('cascade');
            $table->unsignedBigInteger('religion_id')->nullable();
            $table->foreign('religion_id')->references('id')->on('religions')->onDelete('cascade');
            $table->string('phone',50)->nullable();
            $table->string('mobile',50)->nullable();
            $table->string('house_name',100)->nullable();
            $table->string('street',100)->nullable();
            $table->string('town',100)->nullable();
            $table->string('county',100)->nullable();
            $table->string('postcode',100)->nullable();
            $table->string('notes',100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
