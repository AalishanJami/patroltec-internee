<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Update4ScheduleTasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
                if (Schema::hasColumn('schedule_tasks', 'max_display'))
                    {
                         Schema::table('schedule_tasks', function($table) {
                            $table->dropColumn(['max_display']);
                        
                        });
                         Schema::table('schedule_tasks', function($table) {
                                 $table->integer('max_display')->nullable();
                        
                        });
                    }
                    else
                    {
                         Schema::table('schedule_tasks', function(Blueprint $table)
                            {
                                $table->integer('max_display')->nullable();
                            });
                    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
