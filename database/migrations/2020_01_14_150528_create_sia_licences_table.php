<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSiaLicencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sia_licences', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('company_id')->nullable();
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
            $table->string('licence_loc',3)->nullable();
            $table->integer('type')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('first_name',255)->nullable();
            $table->string('surname',255)->nullable();
            $table->boolean('name_match')->nullable();
            $table->date('end')->nullable();
            $table->string('role',255)->nullable();
            $table->string('sia_number',100)->nullable();
            $table->text('notes')->nullable();
            $table->text('status_text')->nullable();
            $table->text('status_text_last')->nullable();
            $table->text('sector')->nullable();
            $table->text('conditions')->nullable();
            $table->text('tracking_number')->nullable();
            $table->text('sia_reference')->nullable();
            $table->boolean('valid')->nullable();
            $table->date('check_no')->nullable();
            $table->date('check_date')->nullable();
            $table->date('check_next')->nullable();
            $table->date('check_last')->nullable();           
            $table->boolean('active')->default(1)->nullable();
            $table->boolean('deleted')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sia_licences');
    }
}
