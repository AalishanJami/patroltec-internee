<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateNformSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('form_sections', function (Blueprint $table) {
            $table->integer('show_header')->default(1)->comment('1=show,0=hide');
            $table->integer('show_footer')->default(1)->comment('1=show,0=hide');
            $table->integer('header_height')->nullable();
            $table->integer('footer_height')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
