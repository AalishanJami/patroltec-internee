<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Update3QuestionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
           Schema::table('questions', function (Blueprint $table) {
            $table->integer('signature_required_app_user')->default(1)->nullable();
            $table->integer('signature_required_app_text_field')->default(0)->nullable();
            $table->integer('signature_required_app_select_menu')->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
