<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ReUpdateResponseGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('response_groups');
        Schema::enableForeignKeyConstraints();
         
          Schema::create('response_groups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('job_group_id')->nullable();
            $table->foreign('job_group_id')->references('id')->on('job_groups')->onDelete('cascade');


            $table->unsignedBigInteger('version_id')->nullable();
            $table->foreign('version_id')->references('id')->on('version_logs')->onDelete('cascade');

            $table->unsignedBigInteger('company_id')->nullable();
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');


            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->unsignedBigInteger('form_id')->nullable();
            $table->foreign('form_id')->references('id')->on('forms')->onDelete('cascade');

            $table->unsignedBigInteger('location_id')->nullable();
            $table->foreign('location_id')->references('id')->on('locations')->onDelete('cascade');

            $table->integer('total_score')->nullable();
            $table->integer('score')->nullable();
            $table->integer('question_score_total_count')->nullable();
            $table->integer('question_score_count')->nullable();
            $table->integer('pass_count')->nullable();
            $table->integer('fail_count')->nullable();
            $table->boolean('active')->default(1)->nullable();
            $table->boolean('deleted')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('response_groups');
    }
}
