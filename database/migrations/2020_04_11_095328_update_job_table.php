<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateJobTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('jobs', function (Blueprint $table) {
            // $table->unsignedBigInteger('user_id')->nullable();
            $table->boolean('display_main_picture')->default(1)->nullable();
            $table->boolean('display_main_file_upload')->default(1)->nullable();
            $table->boolean('display_additional_notes')->default(1)->nullable();
            $table->boolean('signature_required_app_user')->default(1)->nullable();
            $table->boolean('signature_required_app_text_field')->default(0)->nullable();
            $table->boolean('signature_required_app_select_menu')->default(0)->nullable();
            $table->unsignedBigInteger('pre_populate_id')->nullable();
            $table->foreign('pre_populate_id')->references('id')->on('pre_populates')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
