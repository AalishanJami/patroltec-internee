<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FormScoreLocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('form_score_locations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('company_id')->nullable();
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');

             $table->unsignedBigInteger('location_id')->nullable();
            $table->foreign('location_id')->references('id')->on('locations')->onDelete('cascade');

            $table->unsignedBigInteger('form_type_id')->nullable();
            $table->foreign('form_type_id')->references('id')->on('companies')->onDelete('cascade');

            $table->integer('month')->nullable();
            $table->integer('year')->nullable();
            $table->integer('count')->nullable();
            $table->integer('count_response_positive')->nullable();
            $table->integer('count_response_negative')->nullable();
            $table->integer('outstanding_count')->nullable();
            $table->integer('actions_created')->nullable();
            $table->integer('actions_resolved')->nullable();
            $table->integer('actions_outstanding')->nullable();
            $table->integer('actions_total_score')->nullable();
            $table->integer('action_score')->nullable();
            $table->integer('bonus_score')->nullable();
            $table->integer('forms_required')->nullable();
            $table->integer('forms_completed')->nullable();
            $table->integer('form_score')->nullable();
            $table->integer('form_score_total')->nullable();
            $table->integer('response_total')->nullable();
            $table->integer('response_score_total')->nullable();
            $table->integer('score')->nullable();
            $table->integer('max_score')->nullable();






            $table->boolean('active')->default(1)->nullable();
            $table->boolean('deleted')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_score_locations');
        //
    }
}
