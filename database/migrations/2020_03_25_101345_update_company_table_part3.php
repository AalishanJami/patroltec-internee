<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCompanyTablePart3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->string('approve_job_button_color',50)->nullable();
            $table->string('complete_job_button_color',50)->nullable();
            $table->string('refresh_button_color',50)->nullable();
            $table->string('nav_color',50)->nullable();
            $table->string('header_color',50)->nullable();
            $table->text('complete_jobs_online')->nullable();
            $table->text('complete_jobs_offline')->nullable();
            $table->text('panic_alarm_after_success_online')->nullable();
            $table->text('panic_alarm_after_fail_offline')->nullable();
            $table->text('raise_job_online')->nullable();
            $table->text('raise_job_offline')->nullable();
            $table->string('file_path_pictures',50)->nullable();
            $table->string('file_path_docs',50)->nullable();
            $table->string('file_path_signitures',50)->nullable();
            $table->string('tab_info',50)->nullable();
            $table->string('tab_question',50)->nullable();
            $table->string('tab_color',50)->nullable();
            $table->string('tab_file',50)->nullable();
            $table->string('field_date',50)->nullable();
            $table->string('field_time',50)->nullable();
            $table->string('field_location_area',50)->nullable();
            $table->string('field_task_info',50)->nullable();
            $table->string('field_direction',50)->nullable();
            $table->string('field_make',50)->nullable();
            $table->string('field_model',50)->nullable();    
            $table->string('field_information',50)->nullable();
            $table->string('field_notes',50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
