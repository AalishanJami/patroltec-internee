<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserNextKinTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_next_kin', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('company_id')->nullable();
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('relation_ship_id')->nullable();
            $table->foreign('relation_ship_id')->references('id')->on('relation_ships')->onDelete('cascade');
            $table->string('title',10)->nullable();
            $table->string('first_name',250);
            $table->string('surname',250)->nullable();
            $table->string('phone_number',50)->nullable();
            $table->string('extension',10)->nullable();
            $table->string('mobile_number',20)->nullable();
            $table->string('other_number',20)->nullable();
            $table->string('email')->nullable();
            $table->string('address_name',100)->nullable();
            $table->string('address_street',100)->nullable();
            $table->string('address_village',100)->nullable();
            $table->string('town',100)->nullable();
            $table->string('state',100)->nullable();
            $table->string('post_code',50)->nullable();
            $table->string('country',100)->nullable();
            $table->boolean('active')->default(1)->nullable();
            $table->boolean('deleted')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_next_kin');
    }
}
