<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('jobs', function (Blueprint $table) {
            $table->unsignedBigInteger('contractor_id')->nullable();
            $table->foreign('contractor_id')->references('id')->on('contractors')->onDelete('cascade');
            $table->unsignedBigInteger('user_group_id')->nullable();
            $table->foreign('user_group_id')->references('id')->on('user_groups')->onDelete('cascade');
            $table->unsignedBigInteger('form_type_id')->nullable();
            $table->foreign('form_type_id')->references('id')->on('form_types')->onDelete('cascade');
            $table->unsignedBigInteger('question_id')->nullable();
            $table->foreign('question_id')->references('id')->on('questions')->onDelete('cascade');
            $table->unsignedBigInteger('schedule_id')->nullable();
            $table->integer('schedule_id_repeat_no')->nullable();
            $table->integer('job_no')->nullable();
            $table->string('title',250)->nullable();
            $table->string('form_name',250)->nullable();
            $table->date('schedule_date')->nullable();
            $table->date('date_time');
            $table->date('pre_time');
            $table->date('post_time');
            $table->string('t_type',1)->nullable();
            $table->string('service_id',1)->nullable();
            $table->text('details')->nullable();
            $table->boolean('tag')->nullable();
            $table->boolean('tag_job')->nullable();
            $table->integer('sig')->nullable();
            $table->boolean('completed')->nullable();
            $table->boolean('dashboard')->nullable();
            $table->dateTime('date_added')->nullable();
            $table->boolean('hide')->nullable();
            $table->boolean('active')->default(1)->nullable();
            $table->boolean('deleted')->default(0)->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
