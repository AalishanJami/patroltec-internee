<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCompanyTablePart2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->string('profile_title',50)->nullable();
            $table->string('about_us_title',50)->nullable();
            $table->string('contact_us_title',50)->nullable();
            $table->string('faq_title',50)->nullable();
            $table->string('jobs_header_text',50)->nullable();
            $table->text('jobs_outstanding_header_text')->nullable();
            $table->text('raise_form_header_text')->nullable();
            $table->text('write_tags_header_text')->nullable();
            $table->text('write_keys_header_text')->nullable();
            $table->text('customer_list_header_text')->nullable();
            $table->text('profile_header_text')->nullable();
            $table->text('about_us_header_text')->nullable();
            $table->text('about_us_body_text')->nullable();
            $table->text('faq_header_text')->nullable();
            $table->text('faq_body_text')->nullable();
            $table->text('contact_us_header_text')->nullable();
            $table->string('sign_up_approve_button',50)->nullable();
            $table->string('sign_out_button',50)->nullable();
            $table->string('take_main_picture_button',50)->nullable();
            $table->string('select_document_button',50)->nullable();
            $table->string('select_picture_button',50)->nullable();
            $table->string('approve_job_button',50)->nullable();
            $table->string('complete_job_button',50)->nullable();
            $table->string('clear_signiture_button',50)->nullable();
            $table->string('sign_up_button_button_color',50)->nullable();
            $table->string('sign_up_approve_button_color',50)->nullable();
            $table->string('take_question_picture_button_color',50)->nullable();
            $table->string('take_main_picture_button_color',50)->nullable();
            $table->string('select_document_button_color',50)->nullable();
            $table->string('select_picture_button_color',50)->nullable();
            $table->string('clear_signiture_button_color',50)->nullable();
            $table->string('field_equipment_id',50)->nullable();
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
