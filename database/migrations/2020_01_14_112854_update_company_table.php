<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->string('company_name',100)->nullable();
            $table->string('app_name',100)->nullable();
            $table->string('dashboard_text',50)->nullable();
            $table->string('address_1',50)->nullable();
            $table->string('address_2',50)->nullable();
            $table->string('address_3',50)->nullable();
            $table->string('address_4',50)->nullable();
            $table->string('address_5',50)->nullable();
            $table->string('phone_number',50)->nullable();
            $table->string('email',150)->nullable();
            $table->integer('apps_installed')->nullable();
            $table->integer('apps_available')->nullable();
            $table->integer('apps_total')->nullable();
            $table->integer('app_refesh_minutes')->nullable();
            $table->integer('app_job_post_sync_minutes')->nullable();
            $table->integer('app_get_sync_minutes')->nullable();
            $table->integer('app_full_update_minutes')->nullable();
            $table->string('dashboard_title',50)->nullable();
            $table->string('jobs_title',50)->nullable();
            $table->string('jobs_outstanding_title',50)->nullable();
            $table->string('raise_form_title',50)->nullable();
            $table->string('panic_alaram_title',50)->nullable();
            $table->string('write_tags_title',50)->nullable();
            $table->string('write_keys_title',50)->nullable();
            $table->string('customer_list_title',50)->nullable();
            $table->string('field_serial_number',50)->nullable();
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
