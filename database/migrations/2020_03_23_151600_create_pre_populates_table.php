<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrePopulatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pre_populates', function (Blueprint $table) {
            $table->bigIncrements('id');
             
             $table->string('name')->nullable();   
             $table->unsignedBigInteger('form_id')->nullable();
            $table->foreign('form_id')->references('id')->on('forms')->onDelete('cascade');

            $table->unsignedBigInteger('version_id')->nullable();
            $table->foreign('version_id')->references('id')->on('version_logs')->onDelete('cascade');
                $table->boolean('active')->default(1)->nullable();
            $table->boolean('deleted')->default(0)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pre_populates');
    }
}
