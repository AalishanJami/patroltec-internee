<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateResponseGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('response_groups', function (Blueprint $table) {
            $table->unsignedBigInteger('job_id')->nullable();
            $table->foreign('job_id')->references('id')->on('jobs')->onDelete('cascade');
            $table->unsignedBigInteger('version_id')->nullable();
            $table->string('name',250)->nullable();
            $table->string('location_name',250)->nullable();
            $table->integer('total_score_possible')->nullable();
            $table->integer('total_score')->nullable();
            $table->integer('percentage')->nullable();
            $table->integer('count_score')->nullable();
            $table->integer('form_score_question')->nullable();
            $table->integer('completion_score')->nullable();
            $table->integer('score_total')->nullable();
            $table->boolean('pass')->nullable();
            $table->boolean('fail')->nullable();
            $table->boolean('active')->default(1)->nullable();
            $table->boolean('deleted')->default(0)->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
