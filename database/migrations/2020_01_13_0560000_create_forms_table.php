<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('form_type')->nullable();
            $table->integer('form_action')->nullable();
            $table->string('location_type',10)->nullable();
            $table->unsignedBigInteger('company_id')->nullable();
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
            $table->string('name',250)->nullable();
            $table->string('ref',50)->nullable();
            $table->boolean('performance_reporting')->nullable();
            $table->unsignedBigInteger('form_group_id')->nullable();
            $table->unsignedBigInteger('dashboard_reporting_group_id')->nullable();
            $table->boolean('actions_required')->nullable();
            $table->boolean('app_form')->nullable();
            $table->boolean('import_jobs')->nullable();
            $table->boolean('doc_download')->nullable();
            $table->boolean('pdf_download')->nullable();
            $table->date('valid_from')->nullable();
            $table->date('valid_to')->nullable();
            $table->text('notes')->nullable();
            $table->boolean('active')->default(1)->nullable();
            $table->boolean('deleted')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forms');
    }
}
