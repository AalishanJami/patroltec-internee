<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePopulateResponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('populate_responses', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('pre_populate_id')->nullable();
            $table->foreign('pre_populate_id')->references('id')->on('pre_populates')->onDelete('cascade');
            

            $table->unsignedBigInteger('form_section_id')->nullable();
            $table->foreign('form_section_id')->references('id')->on('form_sections')->onDelete('cascade');            
            $table->unsignedBigInteger('answer_id')->nullable();
            $table->foreign('answer_id')->references('id')->on('answers')->onDelete('cascade');

            $table->unsignedBigInteger('question_id')->nullable();
            $table->foreign('question_id')->references('id')->on('questions')->onDelete('cascade');

            $table->string('answer_text',250)->nullable();
            $table->boolean('active')->default(1)->nullable();
            $table->boolean('deleted')->default(0)->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('populate_responses');
    }
}
