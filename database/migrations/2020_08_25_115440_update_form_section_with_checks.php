<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateFormSectionWithChecks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('form_sections', function (Blueprint $table) {
            $table->boolean('is_take_pic')->default(0)->nullable();
            $table->boolean('is_select_doc')->default(0)->nullable();
            $table->boolean('is_select_pic')->default(0)->nullable();
            $table->boolean('is_notes')->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
