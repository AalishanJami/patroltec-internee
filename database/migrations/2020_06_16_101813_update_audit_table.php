<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAuditTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('audits', function (Blueprint $table) {
            $table->dropColumn('event');
            $table->dropColumn('auditable_id');
            $table->dropColumn('auditable_type');
            $table->dropColumn('user_type');
            $table->dropColumn('old_values');
            $table->dropColumn('new_values');
            $table->dropColumn('url');
            $table->dropColumn('user_agent');
            $table->dropColumn('tags');
            $table->dropColumn('name');
            $table->string('user_name')->nullable();
            $table->bigInteger('page_id')->nullable();
            $table->string('page_name')->nullable();
            $table->bigInteger('tab_id')->nullable();
            $table->string('tab_name')->nullable();
            $table->string('action')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
