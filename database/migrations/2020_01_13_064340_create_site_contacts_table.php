<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSiteContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_contacts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',250)->nullable();
            $table->unsignedBigInteger('company_id')->nullable();
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
            $table->unsignedBigInteger('location_id')->nullable();
            $table->foreign('location_id')->references('id')->on('locations')->onDelete('cascade');
            $table->unsignedBigInteger('contacts_type_id')->nullable();
            $table->foreign('contacts_type_id')->references('id')->on('contacts_types')->onDelete('cascade');
            $table->string('title',10)->nullable();
            $table->string('first_name',250);
            $table->string('surname',250)->nullable();
            $table->string('phone_number',15)->nullable();
            $table->string('extension',10)->nullable();
            $table->string('mobile_number',15)->nullable();
            $table->string('other_number',15)->nullable();
            $table->string('email')->unique()->nullable();
            $table->string('address_name',250)->nullable();
            $table->string('address_street',250)->nullable();
            $table->string('address_village',250)->nullable();
            $table->string('town',250)->nullable();
            $table->string('state',250)->nullable();
            $table->string('post_code',50)->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_contacts');
    }
}
