<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameColumnSchedule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('schedule_tasks', function(Blueprint $table)
        {
            $table->dropColumn('recycle_jobs after');
            $table->date('recycle_jobs_after')->nullable();
            $table->integer('tag_swipe')->nullable();
            $table->date('max_display')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
