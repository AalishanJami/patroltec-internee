<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('company_id')->nullable();
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
            $table->unsignedBigInteger('question_group_id')->nullable();
            $table->foreign('question_group_id')->references('id')->on('question_groups')->onDelete('cascade');
            $table->integer('order')->nullable();
            $table->unsignedBigInteger('section_id')->nullable();
            $table->unsignedBigInteger('answer_type_id')->nullable();
            $table->foreign('answer_type_id')->references('id')->on('answer_types')->onDelete('cascade');
            $table->unsignedBigInteger('answer_id')->nullable();
            $table->foreign('answer_id')->references('id')->on('answers')->onDelete('cascade');
            $table->string('question',250)->nullable();
            $table->boolean('import_field')->nullable();
            $table->string('placeholder',250)->nullable();
            $table->boolean('unique_answer')->nullable();
            $table->text('header')->nullable();
            $table->text('footer')->nullable();
            $table->integer('width')->nullable();
            $table->text('border_style')->nullable();
            $table->boolean('show_on_dashboard')->nullable();
            $table->boolean('display_pic')->default(1)->nullable();
            $table->boolean('active')->default(1)->nullable();
            $table->boolean('deleted')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
