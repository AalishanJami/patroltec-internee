<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('users', function (Blueprint $table) {
           $table->unsignedBigInteger('company_id')->nullable();
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
            $table->boolean('all_users')->nullable();
            $table->string('allowed_users')->nullable();
            $table->string('allowed_users_groups')->nullable();
            $table->boolean('all_forms')->nullable();
            $table->string('allowed_forms')->nullable();
            $table->string('allowed_form_groups')->nullable();
            $table->boolean('all_locations')->nullable();
            $table->string('allowed_locations')->nullable();
            $table->string('allowed_locations_groups')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
