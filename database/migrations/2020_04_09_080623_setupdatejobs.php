<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Setupdatejobs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('jobs');
        Schema::enableForeignKeyConstraints();
            Schema::create('jobs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->integer('schedule_id_repeat_no')->nullable();
            $table->integer('job_no')->nullable();
            $table->string('title',250)->nullable();
            $table->string('form_name',250)->nullable();
            $table->date('schedule_date')->nullable();
            $table->timestamp('date_time')->nullable();
            $table->timestamp('pre_time')->nullable();
            $table->timestamp('post_time')->nullable();
            $table->string('t_type',1)->nullable();
            $table->string('service_id',1)->nullable();
            $table->text('details')->nullable();
            $table->boolean('tag')->nullable();
            $table->boolean('tag_job')->nullable();
            $table->integer('sig')->nullable();
            $table->boolean('completed')->nullable();
            $table->boolean('dashboard')->nullable();
            $table->dateTime('date_added')->nullable();
            $table->boolean('hide')->nullable();
            $table->boolean('active')->default(1)->nullable();
            $table->boolean('deleted')->default(0)->nullable();
            $table->unsignedBigInteger('job_group_id')->nullable();
            $table->foreign('job_group_id')->references('id')->on('job_groups')->onDelete('cascade');
            $table->unsignedBigInteger('form_section_id')->nullable();
            $table->foreign('form_section_id')->references('id')->on('form_sections')->onDelete('cascade');
            $table->timestamps();
            });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::dropIfExists('jobs');
        
    }
}
