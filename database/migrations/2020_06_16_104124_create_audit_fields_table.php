<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuditFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('audit_fields', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('audit_id')->nullable();
            $table->foreign('audit_id')->references('id')->on('audits')->onDelete('cascade');
            $table->bigInteger('field_id')->nullable();
            $table->string('field_name')->nullable();
            $table->bigInteger('new_value_id')->nullable();
            $table->string('new_value')->nullable();
            $table->bigInteger('old_value_id')->nullable();
            $table->string('old_value')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('audit_fields');
    }
}
