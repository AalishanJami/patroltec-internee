<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
class SupportPermissionTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission_array = [
            // create Contact permissions
            ['name' => 'view_emailSupport','group'=>'Support Master', 'guard_name' => 'web','permission_name'=>'View','type'=>'','sub_module_name'=>'operation','module_name'=>'Email Support','module_name_id'=>'60'],
            ['name' => 'create_emailSupport','group'=>'Support Master', 'guard_name' => 'web','permission_name'=>'Create','type'=>'','sub_module_name'=>'operation','module_name'=>'Email Support','module_name_id'=>'60'],
            ['name' => 'edit_emailSupport', 'group'=>'Support Master', 'guard_name' => 'web','permission_name'=>'Edit','type'=>'','sub_module_name'=>'operation','module_name'=>'Email Support','module_name_id'=>'60'],
            ['name' => 'delete_emailSupport','group'=>'Support Master', 'guard_name' => 'web','permission_name'=>'Delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Email Support','module_name_id'=>'60'],
            ['name' => 'delete_selected_emailSupport','group'=>'Support Master', 'guard_name' => 'web','permission_name'=>'Selected Delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Email Support','module_name_id'=>'60'],
            ['name' => 'selected_active_emailSupport', 'group'=>'Support Master','guard_name' => 'web','type'=>'','permission_name'=>'Selected Active Multiple','sub_module_name'=>'operation','module_name'=>'Email Support','module_name_id'=>'60'],
            ['name' => 'selected_restore_emailSupport', 'group'=>'Support Master','guard_name' => 'web','type'=>'','permission_name'=>'Selected Restore','sub_module_name'=>'operation','module_name'=>'Email Support','module_name_id'=>'60'],
            ['name' => 'hard_delete_emailSupport','group'=>'Support Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Hard Delete','sub_module_name'=>'operation','module_name'=>'Email Support','module_name_id'=>'60'],
            ['name' => 'active_emailSupport','group'=>'Support Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Active Employee','sub_module_name'=>'operation','module_name'=>'Email Support','module_name_id'=>'60'],
            ['name' => 'select_active_emailSupport','group'=>'Support Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Select Active ','sub_module_name'=>'operation','module_name'=>'Email Support','module_name_id'=>'60'],
            ['name' => 'word_emailSupport', 'group'=>'Support Master','guard_name' => 'web','type'=>'','permission_name'=>'Word Export ','sub_module_name'=>'operation','module_name'=>'Email Support','module_name_id'=>'60'],
            ['name' => 'csv_emailSupport','group'=>'Support Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Csv Export','sub_module_name'=>'operation','module_name'=>'Email Support','module_name_id'=>'60'],
            ['name' => 'pdf_emailSupport', 'group'=>'Support Master','guard_name' => 'web','type'=>'','permission_name'=>'PDF Export','sub_module_name'=>'operation','module_name'=>'Email Support','module_name_id'=>'60'],

            //listing
            ['name' => 'emailSupport_checkbox','group'=>'Support Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Email Support','module_name_id'=>'60'],
            ['name' => 'emailSupport_ticketid', 'group'=>'Support Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Ticket Id','sub_module_name'=>'listing','module_name'=>'Email Support','module_name_id'=>'60'],
            ['name' => 'emailSupport_username', 'group'=>'Support Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Username','sub_module_name'=>'listing','module_name'=>'Email Support','module_name_id'=>'60'],
            ['name' => 'emailSupport_title','group'=>'Support Master', 'guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Title','sub_module_name'=>'listing','module_name'=>'Email Support','module_name_id'=>'60'],
            ['name' => 'emailSupport_message', 'group'=>'Support Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Message','sub_module_name'=>'listing','module_name'=>'Email Support','module_name_id'=>'60'],
            ['name' => 'emailSupport_senddate', 'group'=>'Support Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Send Date','sub_module_name'=>'listing','module_name'=>'Email Support','module_name_id'=>'60'],

            //create
            ['name' => 'emailSupport_checkbox_create','group'=>'Support Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'create','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Email Support','module_name_id'=>'60'],
            ['name' => 'emailSupport_ticketid_create', 'group'=>'Support Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Ticket Id','sub_module_name'=>'listing','module_name'=>'Email Support','module_name_id'=>'60'],
            ['name' => 'emailSupport_username_create', 'group'=>'Support Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Username','sub_module_name'=>'listing','module_name'=>'Email Support','module_name_id'=>'60'],
            ['name' => 'emailSupport_title_create','group'=>'Support Master', 'guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Title','sub_module_name'=>'listing','module_name'=>'Email Support','module_name_id'=>'60'],
            ['name' => 'emailSupport_message_create', 'group'=>'Support Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Message','sub_module_name'=>'listing','module_name'=>'Email Support','module_name_id'=>'60'],
            ['name' => 'emailSupport_senddate_create', 'group'=>'Support Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Send Date','sub_module_name'=>'listing','module_name'=>'Email Support','module_name_id'=>'60'],

            //word
            ['name' => 'emailSupport_checkbox_word_export','group'=>'Support Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'word_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Email Support','module_name_id'=>'60'],
            ['name' => 'emailSupport_ticketid_word_export', 'group'=>'Support Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Ticket Id','sub_module_name'=>'listing','module_name'=>'Email Support','module_name_id'=>'60'],
            ['name' => 'emailSupport_username_word_export', 'group'=>'Support Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Username','sub_module_name'=>'listing','module_name'=>'Email Support','module_name_id'=>'60'],
            ['name' => 'emailSupport_title_word_export','group'=>'Support Master', 'guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Title','sub_module_name'=>'listing','module_name'=>'Email Support','module_name_id'=>'60'],
            ['name' => 'emailSupport_message_word_export', 'group'=>'Support Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Message','sub_module_name'=>'listing','module_name'=>'Email Support','module_name_id'=>'60'],
            ['name' => 'emailSupport_senddate_word_export', 'group'=>'Support Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Send Date','sub_module_name'=>'listing','module_name'=>'Email Support','module_name_id'=>'60'],

            //excel
            ['name' => 'emailSupport_checkbox_excel_export','group'=>'Support Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'excel_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Email Support','module_name_id'=>'60'],
            ['name' => 'emailSupport_ticketid_excel_export', 'group'=>'Support Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Ticket Id','sub_module_name'=>'listing','module_name'=>'Email Support','module_name_id'=>'60'],
            ['name' => 'emailSupport_username_excel_export', 'group'=>'Support Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Username','sub_module_name'=>'listing','module_name'=>'Email Support','module_name_id'=>'60'],
            ['name' => 'emailSupport_title_excel_export','group'=>'Support Master', 'guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Title','sub_module_name'=>'listing','module_name'=>'Email Support','module_name_id'=>'60'],
            ['name' => 'emailSupport_message_excel_export', 'group'=>'Support Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Message','sub_module_name'=>'listing','module_name'=>'Email Support','module_name_id'=>'60'],
            ['name' => 'emailSupport_senddate_excel_export', 'group'=>'Support Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Send Date','sub_module_name'=>'listing','module_name'=>'Email Support','module_name_id'=>'60'],

            //pdf
            ['name' => 'emailSupport_checkbox_pdf_export','group'=>'Support Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'pdf_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Email Support','module_name_id'=>'60'],
            ['name' => 'emailSupport_ticketid_pdf_export', 'group'=>'Support Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Ticket Id','sub_module_name'=>'listing','module_name'=>'Email Support','module_name_id'=>'60'],
            ['name' => 'emailSupport_username_pdf_export', 'group'=>'Support Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Username','sub_module_name'=>'listing','module_name'=>'Email Support','module_name_id'=>'60'],
            ['name' => 'emailSupport_title_pdf_export','group'=>'Support Master', 'guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Title','sub_module_name'=>'listing','module_name'=>'Email Support','module_name_id'=>'60'],
            ['name' => 'emailSupport_message_pdf_export', 'group'=>'Support Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Message','sub_module_name'=>'listing','module_name'=>'Email Support','module_name_id'=>'60'],
            ['name' => 'emailSupport_senddate_pdf_export', 'group'=>'Support Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Send Date','sub_module_name'=>'listing','module_name'=>'Email Support','module_name_id'=>'60'],

            //edit
            ['name' => 'emailSupport_checkbox_edit','group'=>'Support Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'edit','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Email Support','module_name_id'=>'60'],
            ['name' => 'emailSupport_ticketid_edit', 'group'=>'Support Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Ticket Id','sub_module_name'=>'listing','module_name'=>'Email Support','module_name_id'=>'60'],
            ['name' => 'emailSupport_username_edit', 'group'=>'Support Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Username','sub_module_name'=>'listing','module_name'=>'Email Support','module_name_id'=>'60'],
            ['name' => 'emailSupport_title_edit','group'=>'Support Master', 'guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Title','sub_module_name'=>'listing','module_name'=>'Email Support','module_name_id'=>'60'],
            ['name' => 'emailSupport_message_edit', 'group'=>'Support Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Message','sub_module_name'=>'listing','module_name'=>'Email Support','module_name_id'=>'60'],
            ['name' => 'emailSupport_senddate_edit', 'group'=>'Support Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Send Date','sub_module_name'=>'listing','module_name'=>'Email Support','module_name_id'=>'60'],
        ];
        foreach ($permission_array as $key => $permission) {
            $admin_role = Role::find(1);
            $getpermission=Permission::where('name',$permission['name'])->first();
            if (empty($getpermission)){
                $permission=Permission::create($permission);
                $admin_role->givePermissionTo($permission['name']);
            }else{
                $permission=Permission::where('name',$getpermission->name)->update($permission);
                $admin_role->givePermissionTo($getpermission->name);
            }
        }
    }
}
