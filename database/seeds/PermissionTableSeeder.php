 <?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission_array = [
			// create users permissions
			['name' => 'view_users','group'=>'Users Master', 'guard_name' => 'web','permission_name'=>'view','type'=>'','sub_module_name'=>'operation','module_name'=>'user'],
			['name' => 'copy_users','group'=>'Users Master', 'guard_name' => 'web','permission_name'=>'copy','type'=>'','sub_module_name'=>'operation','module_name'=>'user'],
			['name' => 'create_users','group'=>'Users Master', 'guard_name' => 'web','permission_name'=>'create','type'=>'','sub_module_name'=>'operation','module_name'=>'user'],
			['name' => 'manage_roles', 'group'=>'Users Master','guard_name' => 'web','permission_name'=>'role','type'=>'','sub_module_name'=>'operation','module_name'=>'user'],
			['name' => 'edit_users', 'group'=>'Users Master', 'guard_name' => 'web','permission_name'=>'edit','type'=>'','sub_module_name'=>'operation','module_name'=>'user'],
			['name' => 'delete_users','group'=>'Users Master', 'guard_name' => 'web','permission_name'=>'delete','type'=>'','sub_module_name'=>'operation','module_name'=>'user'],
			['name' => 'selected_delete_users','group'=>'Users Master', 'guard_name' => 'web','permission_name'=>'Selected Delete','type'=>'','sub_module_name'=>'operation','module_name'=>'user'],
			['name' => 'selected_restore_delete_users', 'group'=>'Users Master','guard_name' => 'web','type'=>'','permission_name'=>'Selected Restore','sub_module_name'=>'operation','module_name'=>'user'],
			['name' => 'restore_delete_users','group'=>'Users Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Restore Delete','sub_module_name'=>'operation','module_name'=>'user'],
			['name' => 'hard_delete_users','group'=>'Users Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Hard Delete','sub_module_name'=>'operation','module_name'=>'user'],
			['name' => 'active_users','group'=>'Users Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Active Users','sub_module_name'=>'operation','module_name'=>'user'],
			['name' => 'word_users', 'group'=>'Users Master','guard_name' => 'web','type'=>'','permission_name'=>'Word Export Users','sub_module_name'=>'operation','module_name'=>'user'],
			['name' => 'csv_users','group'=>'Users Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Csv Export Users','sub_module_name'=>'operation','module_name'=>'user'],
			['name' => 'pdf_users', 'group'=>'Users Master','guard_name' => 'web','type'=>'','permission_name'=>'PDF Export Users','sub_module_name'=>'operation','module_name'=>'user'],


			//listing in user
			['name' => 'user_checkbox','group'=>'Users Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'user'],
			['name' => 'user_name', 'group'=>'Users Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'name','sub_module_name'=>'listing','module_name'=>'user'],
			['name' => 'user_email','group'=>'Users Master', 'guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'email','sub_module_name'=>'listing','module_name'=>'user'],
			['name' => 'user_company', 'group'=>'Users Master','guard_name' => 'web','type'=>'view','enable'=>'0','permission_name'=>'company','sub_module_name'=>'listing','module_name'=>'user'],
			['name' => 'user_password', 'group'=>'Users Master','guard_name' => 'web','type'=>'view','enable'=>'0','permission_name'=>'your_password','sub_module_name'=>'listing','module_name'=>'user'],

			//listing in user
			['name' => 'user_checkbox_create','group'=>'Users Master', 'guard_name' => 'web','type'=>'create','enable'=>'0','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'user'],
			['name' => 'user_name_create', 'group'=>'Users Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'name','sub_module_name'=>'listing','module_name'=>'user'],
			['name' => 'user_email_create', 'group'=>'Users Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'email','sub_module_name'=>'listing','module_name'=>'user'],
			['name' => 'user_company_create','group'=>'Users Master', 'guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'company','sub_module_name'=>'listing','module_name'=>'user'],
			['name' => 'user_password_create', 'group'=>'Users Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'your_password','sub_module_name'=>'listing','module_name'=>'user'],

			//listing in user
			['name' => 'user_checkbox_word_export','group'=>'Users Master', 'guard_name' => 'web','type'=>'word_export','enable'=>'0','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'user'],
			['name' => 'user_name_word_export', 'group'=>'Users Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'name','sub_module_name'=>'listing','module_name'=>'user'],
			['name' => 'user_email_word_export', 'group'=>'Users Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'email','sub_module_name'=>'listing','module_name'=>'user'],
			['name' => 'user_company_word_export','group'=>'Users Master', 'guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'company','sub_module_name'=>'listing','module_name'=>'user'],
			['name' => 'user_password_word_export', 'group'=>'Users Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'your_password','sub_module_name'=>'listing','module_name'=>'user'],

			//listing in user
			['name' => 'user_checkbox_excel_export','group'=>'Users Master', 'guard_name' => 'web','type'=>'excel_export','enable'=>'0','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'user'],
			['name' => 'user_name_excel_export', 'group'=>'Users Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'name','sub_module_name'=>'listing','module_name'=>'user'],
			['name' => 'user_email_excel_export', 'group'=>'Users Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'email','sub_module_name'=>'listing','module_name'=>'user'],
			['name' => 'user_company_excel_export','group'=>'Users Master', 'guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'company','sub_module_name'=>'listing','module_name'=>'user'],
			['name' => 'user_password_excel_export', 'group'=>'Users Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'your_password','sub_module_name'=>'listing','module_name'=>'user'],
			//
			['name' => 'user_checkbox_pdf_export','group'=>'Users Master', 'guard_name' => 'web','type'=>'pdf_export','enable'=>'0','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'user'],
			['name' => 'user_name_pdf_export', 'group'=>'Users Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'name','sub_module_name'=>'listing','module_name'=>'user'],
			['name' => 'user_email_pdf_export', 'group'=>'Users Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'email','sub_module_name'=>'listing','module_name'=>'user'],
			['name' => 'user_company_pdf_export','group'=>'Users Master', 'guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'company','sub_module_name'=>'listing','module_name'=>'user'],
			['name' => 'user_password_pdf_export', 'group'=>'Users Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'your_password','sub_module_name'=>'listing','module_name'=>'user'],
			//listing in user
			['name' => 'user_checkbox_edit','group'=>'Users Master', 'guard_name' => 'web','type'=>'edit','enable'=>'0','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'user'],
			['name' => 'user_name_edit', 'group'=>'Users Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'name','sub_module_name'=>'listing','module_name'=>'user'],
			['name' => 'user_email_edit', 'group'=>'Users Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'email','sub_module_name'=>'listing','module_name'=>'user'],
			['name' => 'user_company_edit','group'=>'Users Master', 'guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'company','sub_module_name'=>'listing','module_name'=>'user'],
			['name' => 'user_password_edit', 'group'=>'Users Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'your_password','sub_module_name'=>'listing','module_name'=>'user'],



			// create roles permissions
			['name' => 'view_roles','group'=>'Users Master',  'guard_name' => 'web','type'=>'','sub_module_name'=>'operation','permission_name'=>'view','module_name'=>'role'],
			['name' => 'create_roles', 'group'=>'Users Master','guard_name' => 'web','type'=>'','sub_module_name'=>'operation','permission_name'=>'create','module_name'=>'role'],
			['name' => 'edit_roles', 'group'=>'Users Master', 'guard_name' => 'web','type'=>'','sub_module_name'=>'operation','permission_name'=>'edit','module_name'=>'role'],
			['name' => 'delete_roles','group'=>'Users Master', 'guard_name' => 'web','type'=>'','sub_module_name'=>'operation','permission_name'=>'delete','module_name'=>'role'],
			['name' => 'selected_delete_roles','group'=>'Users Master', 'guard_name' => 'web','permission_name'=>'Selected Delete','type'=>'','sub_module_name'=>'operation','module_name'=>'role'],
			['name' => 'selected_restore_delete_roles','group'=>'Users Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Selected Restore','sub_module_name'=>'operation','module_name'=>'role'],
			['name' => 'restore_delete_roles','group'=>'Users Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Restore Delete','sub_module_name'=>'operation','module_name'=>'role'],
			['name' => 'hard_delete_roles','group'=>'Users Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Hard Delete','sub_module_name'=>'operation','module_name'=>'role'],
			['name' => 'active_roles','group'=>'Users Master','guard_name' => 'web','type'=>'','permission_name'=>'Active roles','sub_module_name'=>'operation','module_name'=>'role'],
			// ['name' => 'word_roles', 'guard_name' => 'web','type'=>'','permission_name'=>'Word Export roles','sub_module_name'=>'operation','module_name'=>'role'],
			// ['name' => 'csv_roles', 'guard_name' => 'web','type'=>'','permission_name'=>'Csv Export roles','sub_module_name'=>'operation','module_name'=>'role'],
			['name' => 'operation_roles', 'group'=>'Users Master','guard_name' => 'web','type'=>'','permission_name'=>'Operation roles','sub_module_name'=>'operation','module_name'=>'role'],
			['name' => 'listing_roles','group'=>'Users Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Listing roles','sub_module_name'=>'operation','module_name'=>'role'],

			//listing in role
			['name' => 'role_checkbox', 'group'=>'Users Master','guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'role'],
			['name' => 'role_name','group'=>'Users Master', 'guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'name','sub_module_name'=>'listing','module_name'=>'role'],
			['name' => 'role_module','group'=>'Users Master', 'guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'module','sub_module_name'=>'listing','module_name'=>'role'],
			//listing in role
			['name' => 'role_checkbox_edit','group'=>'Users Master', 'guard_name' => 'web','type'=>'edit','enable'=>'0','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'role'],
			['name' => 'role_name_edit', 'group'=>'Users Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'name','sub_module_name'=>'listing','module_name'=>'role'],
			['name' => 'role_module_edit','group'=>'Users Master', 'guard_name' => 'web','type'=>'edit','enable'=>'0','permission_name'=>'module','sub_module_name'=>'listing','module_name'=>'role'],

			// create Company permissions
			['name' => 'view_company','group'=>'Company',  'guard_name' => 'web','type'=>'','sub_module_name'=>'operation','permission_name'=>'view','module_name'=>'company'],
			['name' => 'create_company', 'group'=>'Company','guard_name' => 'web','type'=>'','sub_module_name'=>'operation','permission_name'=>'create','module_name'=>'company'],
			['name' => 'edit_company', 'group'=>'Company', 'guard_name' => 'web','type'=>'','sub_module_name'=>'operation','permission_name'=>'edit','module_name'=>'company'],
			['name' => 'delete_company','group'=>'Company', 'guard_name' => 'web','type'=>'','sub_module_name'=>'operation','permission_name'=>'delete','module_name'=>'company'],
			['name' => 'selected_delete_company','group'=>'Company', 'guard_name' => 'web','permission_name'=>'Selected Delete','type'=>'','sub_module_name'=>'operation','module_name'=>'company'],
			['name' => 'selected_restore_delete_company','group'=>'Company', 'guard_name' => 'web','type'=>'','permission_name'=>'Selected Restore','sub_module_name'=>'operation','module_name'=>'company'],
			['name' => 'restore_delete_company','group'=>'Company', 'guard_name' => 'web','type'=>'','permission_name'=>'Restore Delete','sub_module_name'=>'operation','module_name'=>'company'],
			['name' => 'hard_delete_company','group'=>'Company', 'guard_name' => 'web','type'=>'','permission_name'=>'Hard Delete','sub_module_name'=>'operation','module_name'=>'company'],
			['name' => 'active_company','group'=>'Company','guard_name' => 'web','type'=>'','permission_name'=>'Active company','sub_module_name'=>'operation','module_name'=>'company'],
			['name' => 'word_company','group'=>'Company','guard_name' => 'web','type'=>'','permission_name'=>'Word Export company','sub_module_name'=>'operation','module_name'=>'company'],
			['name' => 'csv_company', 'group'=>'Company','guard_name' => 'web','type'=>'','permission_name'=>'Csv Export company','sub_module_name'=>'operation','module_name'=>'company'],
			['name' => 'operation_company', 'group'=>'Company','guard_name' => 'web','type'=>'','permission_name'=>'Operation company','sub_module_name'=>'operation','module_name'=>'company'],
			['name' => 'listing_company','group'=>'Company', 'guard_name' => 'web','type'=>'','permission_name'=>'Listing company','sub_module_name'=>'operation','module_name'=>'company'],

			//listing in company
			['name' => 'company_checkbox', 'group'=>'Company','guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'company'],
			['name' => 'company_name','group'=>'Company', 'guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'name','sub_module_name'=>'listing','module_name'=>'company'],

			//listing in company
			['name' => 'company_checkbox_edit','group'=>'Company', 'guard_name' => 'web','type'=>'edit','enable'=>'0','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'company'],
			['name' => 'company_name_edit', 'group'=>'Company','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'name','sub_module_name'=>'listing','module_name'=>'company'],
			// create roles permissions
			['name' => 'view_permissions', 'group'=>'Users Master', 'guard_name' => 'web','sub_module_name'=>'operation','permission_name'=>'view','module_name'=>'permission'],
			// create cms permissions
			['name' => 'view_cms', 'group'=>'CMS', 'guard_name' => 'web','sub_module_name'=>'operation','permission_name'=>'view','module_name'=>'cms'],
		];

		DB::statement('SET FOREIGN_KEY_CHECKS=0;
			');
		DB::table('permissions')->truncate();
		foreach ($permission_array as $key => $permission) {
            $getpermission=Permission::where('name',$permission['name'])->first();
            if (empty($getpermission)){
                Permission::create($permission);
            }else{
                Permission::where('name',$getpermission->name)->update($permission);
            }
		}
		DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
