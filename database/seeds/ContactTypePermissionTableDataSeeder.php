<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class ContactTypePermissionTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission_array = [
            // create Address Type permissions
            ['name' => 'view_contactType','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'view','type'=>'','sub_module_name'=>'operation','module_name'=>'Contact Type'],
            ['name' => 'create_contactType','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'create','type'=>'','sub_module_name'=>'operation','module_name'=>'Contact Type'],
            ['name' => 'edit_contactType', 'group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'edit','type'=>'','sub_module_name'=>'operation','module_name'=>'Contact Type'],
            ['name' => 'delete_contactType','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Contact Type'],
            ['name' => 'selected_delete_contactType','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'Selected Delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Contact Type'],
            ['name' => 'selected_restore_delete_contactType', 'group'=>'Project Master','guard_name' => 'web','type'=>'','permission_name'=>'Selected Restore','sub_module_name'=>'operation','module_name'=>'Contact Type'],
            ['name' => 'selected_active_contactType', 'group'=>'Project Master','guard_name' => 'web','type'=>'','permission_name'=>'Selected Active','sub_module_name'=>'operation','module_name'=>'Contact Type'],
            ['name' => 'restore_delete_contactType','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Restore Delete','sub_module_name'=>'operation','module_name'=>'Contact Type'],
            ['name' => 'hard_delete_contactType','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Hard Delete','sub_module_name'=>'operation','module_name'=>'Contact Type'],
            ['name' => 'active_contactType','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Active Address TypePopup','sub_module_name'=>'operation','module_name'=>'Contact Type'],
            ['name' => 'word_contactType', 'group'=>'Project Master','guard_name' => 'web','type'=>'','permission_name'=>'Word Export Contact Type ','sub_module_name'=>'operation','module_name'=>'Contact Type'],
            ['name' => 'csv_contactType','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Csv Export Contact Type ','sub_module_name'=>'operation','module_name'=>'Contact Type'],
            ['name' => 'pdf_contactType', 'group'=>'Project Master','guard_name' => 'web','type'=>'','permission_name'=>'PDF Export Contact Type ','sub_module_name'=>'operation','module_name'=>'Contact Type'],

            //listing in Address Type
            ['name' => 'contactType_checkbox','group'=>'Project Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Contact Type'],
            ['name' => 'contactType_name', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'name','sub_module_name'=>'listing','module_name'=>'Contact Type'],

            //word in Address Type
            ['name' => 'contactType_checkbox_word_export','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'word_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Contact Type'],
            ['name' => 'contactType_name_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'name','sub_module_name'=>'listing','module_name'=>'Contact Type'],

            //excel in Address Type
            ['name' => 'contactType_checkbox_excel_export','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'excel_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Contact Type'],
            ['name' => 'contactType_name_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'name','sub_module_name'=>'listing','module_name'=>'Contact Type'],

            //pdf in Address Type
            ['name' => 'contactType_checkbox_pdf_export','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'pdf_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Contact Type'],
            ['name' => 'contactType_name_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'name','sub_module_name'=>'listing','module_name'=>'Contact Type'],

            //create in Address Type
            ['name' => 'contactType_checkbox_create','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'create','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Contact Type'],
            ['name' => 'contactType_name_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'name','sub_module_name'=>'listing','module_name'=>'Contact Type'],

            //edit in Address Typ
            ['name' => 'contactType_checkbox_edit','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'edit','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Contact Type'],
            ['name' => 'contactType_name_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'name','sub_module_name'=>'listing','module_name'=>'Contact Type'],

        ];

        foreach ($permission_array as $key => $permission) {
            $admin_role = Role::find(1);
            $getpermission=Permission::where('name',$permission['name'])->first();
            if (empty($getpermission)){
                $permission=Permission::create($permission);
                $admin_role->givePermissionTo($permission['name']);
            }else{
                $permission=Permission::where('name',$getpermission->name)->update($permission);
                $admin_role->givePermissionTo($getpermission->name);
            }
        }
    }
}
