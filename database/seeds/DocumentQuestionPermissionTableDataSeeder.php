<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
class DocumentQuestionPermissionTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission_array = [
            // create Contact permissions
            ['name' => 'view_documentQuestion','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'View','type'=>'','sub_module_name'=>'operation','module_name'=>'Question','module_name_id'=>'54'],
            ['name' => 'create_documentQuestion','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'Create','type'=>'','sub_module_name'=>'operation','module_name'=>'Question','module_name_id'=>'54'],
            ['name' => 'edit_documentQuestion', 'group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'Edit','type'=>'','sub_module_name'=>'operation','module_name'=>'Question','module_name_id'=>'54'],
            ['name' => 'delete_documentQuestion','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'Delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Question','module_name_id'=>'54'],
            ['name' => 'duplicate_documentQuestion','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'Duplicate','type'=>'','sub_module_name'=>'operation','module_name'=>'Question','module_name_id'=>'54'],

            //form view column
            ['name' => 'documentQuestion_question','group'=>'Document Master', 'guard_name' => 'web','enable'=>'1','permission_name'=>'Question','type'=>'view','sub_module_name'=>'listing','module_name'=>'Question','module_name_id'=>'54'],
            ['name' => 'documentQuestion_order','group'=>'Document Master', 'guard_name' => 'web','enable'=>'1','permission_name'=>'Order','type'=>'view','sub_module_name'=>'listing','module_name'=>'Question','module_name_id'=>'54'],
            ['name' => 'documentQuestion_answertype', 'group'=>'Document Master', 'guard_name' => 'web','enable'=>'1','permission_name'=>'Answer Type','type'=>'view','sub_module_name'=>'listing','module_name'=>'Question','module_name_id'=>'54'],
            ['name' => 'documentQuestion_layout','group'=>'Document Master', 'guard_name' => 'web','enable'=>'1','permission_name'=>'Layout','type'=>'view','sub_module_name'=>'listing','module_name'=>'Question','module_name_id'=>'54'],
            ['name' => 'documentQuestion_answer','group'=>'Document Master', 'guard_name' => 'web','enable'=>'1','permission_name'=>'Answer','type'=>'view','sub_module_name'=>'listing','module_name'=>'Question','module_name_id'=>'54'],

            //form create column
            ['name' => 'documentQuestion_question_create','group'=>'Document Master', 'guard_name' => 'web','enable'=>'1','permission_name'=>'Question','type'=>'create','sub_module_name'=>'listing','module_name'=>'Question','module_name_id'=>'54'],
            ['name' => 'documentQuestion_order_create','group'=>'Document Master', 'guard_name' => 'web','enable'=>'1','permission_name'=>'Order','type'=>'create','sub_module_name'=>'listing','module_name'=>'Question','module_name_id'=>'54'],
            ['name' => 'documentQuestion_answertype_create', 'group'=>'Document Master', 'guard_name' => 'web','enable'=>'1','permission_name'=>'Answer Type','type'=>'create','sub_module_name'=>'listing','module_name'=>'Question','module_name_id'=>'54'],
            ['name' => 'documentQuestion_layout_create','group'=>'Document Master', 'guard_name' => 'web','enable'=>'1','permission_name'=>'Layout','type'=>'create','sub_module_name'=>'listing','module_name'=>'Question','module_name_id'=>'54'],
            ['name' => 'documentQuestion_answer_create','group'=>'Document Master', 'guard_name' => 'web','enable'=>'1','permission_name'=>'Answer','type'=>'create','sub_module_name'=>'listing','module_name'=>'Question','module_name_id'=>'54'],


            //word export
            ['name' => 'documentQuestion_question_word_export','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0','permission_name'=>'Question','type'=>'word_export','sub_module_name'=>'listing','module_name'=>'Question','module_name_id'=>'54'],
            ['name' => 'documentQuestion_order_word_export','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0','permission_name'=>'Order','type'=>'word_export','sub_module_name'=>'listing','module_name'=>'Question','module_name_id'=>'54'],
            ['name' => 'documentQuestion_answertype_word_export', 'group'=>'Document Master', 'guard_name' => 'web','enable'=>'0','permission_name'=>'Answer Type','type'=>'word_export','sub_module_name'=>'listing','module_name'=>'Question','module_name_id'=>'54'],
            ['name' => 'documentQuestion_layout_word_export','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0','permission_name'=>'Layout','type'=>'word_export','sub_module_name'=>'listing','module_name'=>'Question','module_name_id'=>'54'],
            ['name' => 'documentQuestion_answer_word_export','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0','permission_name'=>'Answer','type'=>'word_export','sub_module_name'=>'listing','module_name'=>'Question','module_name_id'=>'54'],


            //excel export
            ['name' => 'documentQuestion_question_excel_export','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0','permission_name'=>'Question','type'=>'excel_export','sub_module_name'=>'listing','module_name'=>'Question','module_name_id'=>'54'],
            ['name' => 'documentQuestion_order_excel_export','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0','permission_name'=>'Order','type'=>'excel_export','sub_module_name'=>'listing','module_name'=>'Question','module_name_id'=>'54'],
            ['name' => 'documentQuestion_answertype_excel_export', 'group'=>'Document Master', 'guard_name' => 'web','enable'=>'0','permission_name'=>'Answer Type','type'=>'excel_export','sub_module_name'=>'listing','module_name'=>'Question','module_name_id'=>'54'],
            ['name' => 'documentQuestion_layout_excel_export','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0','permission_name'=>'Layout','type'=>'excel_export','sub_module_name'=>'listing','module_name'=>'Question','module_name_id'=>'54'],
            ['name' => 'documentQuestion_answer_excel_export','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0','permission_name'=>'Answer','type'=>'excel_export','sub_module_name'=>'listing','module_name'=>'Question','module_name_id'=>'54'],


            //pdf export
            ['name' => 'documentQuestion_question_pdf_export','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0','permission_name'=>'Question','type'=>'pdf_export','sub_module_name'=>'listing','module_name'=>'Question','module_name_id'=>'54'],
            ['name' => 'documentQuestion_order_pdf_export','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0','permission_name'=>'Order','type'=>'pdf_export','sub_module_name'=>'listing','module_name'=>'Question','module_name_id'=>'54'],
            ['name' => 'documentQuestion_answertype_pdf_export', 'group'=>'Document Master', 'guard_name' => 'web','enable'=>'0','permission_name'=>'Answer Type','type'=>'pdf_export','sub_module_name'=>'listing','module_name'=>'Question','module_name_id'=>'54'],
            ['name' => 'documentQuestion_layout_pdf_export','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0','permission_name'=>'Layout','type'=>'pdf_export','sub_module_name'=>'listing','module_name'=>'Question','module_name_id'=>'54'],
            ['name' => 'documentQuestion_answer_pdf_export','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0','permission_name'=>'Answer','type'=>'pdf_export','sub_module_name'=>'listing','module_name'=>'Question','module_name_id'=>'54'],

            //form edit column
            ['name' => 'documentQuestion_question_edit','group'=>'Document Master', 'guard_name' => 'web','enable'=>'1','permission_name'=>'Question','type'=>'edit','sub_module_name'=>'listing','module_name'=>'Question','module_name_id'=>'54'],
            ['name' => 'documentQuestion_order_edit','group'=>'Document Master', 'guard_name' => 'web','enable'=>'1','permission_name'=>'Order','type'=>'edit','sub_module_name'=>'listing','module_name'=>'Question','module_name_id'=>'54'],
            ['name' => 'documentQuestion_answertype_edit', 'group'=>'Document Master', 'guard_name' => 'web','enable'=>'1','permission_name'=>'Answer Type','type'=>'edit','sub_module_name'=>'listing','module_name'=>'Question','module_name_id'=>'54'],
            ['name' => 'documentQuestion_layout_edit','group'=>'Document Master', 'guard_name' => 'web','enable'=>'1','permission_name'=>'Layout','type'=>'edit','sub_module_name'=>'listing','module_name'=>'Question','module_name_id'=>'54'],
            ['name' => 'documentQuestion_answer_edit','group'=>'Document Master', 'guard_name' => 'web','enable'=>'1','permission_name'=>'Answer','type'=>'edit','sub_module_name'=>'listing','module_name'=>'Question','module_name_id'=>'54'],
        ];

        foreach ($permission_array as $key => $permission) {
            $admin_role = Role::find(1);
            $getpermission=Permission::where('name',$permission['name'])->first();
            if (empty($getpermission)){
                $permission=Permission::create($permission);
                $admin_role->givePermissionTo($permission['name']);
            }else{
                $permission=Permission::where('name',$getpermission->name)->update($permission);
                $admin_role->givePermissionTo($getpermission->name);
            }
        }
    }
}
