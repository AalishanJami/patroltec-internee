<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
class ReportQRPermissionTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission_array = [
            // create Contact permissions
            ['name' => 'view_qrReport','group'=>'Report Master', 'guard_name' => 'web','permission_name'=>'View','type'=>'','sub_module_name'=>'operation','module_name'=>'QR Report','module_name_id'=>'57'],
            ['name' => 'word_qrReport', 'group'=>'Report Master','guard_name' => 'web','type'=>'','permission_name'=>'Word Export ','sub_module_name'=>'operation','module_name'=>'QR Report','module_name_id'=>'57'],
            ['name' => 'csv_qrReport','group'=>'Report Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Csv Export','sub_module_name'=>'operation','module_name'=>'QR Report','module_name_id'=>'57'],
            ['name' => 'pdf_qrReport', 'group'=>'Report Master','guard_name' => 'web','type'=>'','permission_name'=>'PDF Export','sub_module_name'=>'operation','module_name'=>'QR Report','module_name_id'=>'57'],

            //listing in Contact
            ['name' => 'qrReport_checkbox','group'=>'Report Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'QR Report','module_name_id'=>'57'],
            ['name' => 'qrReport_qr', 'group'=>'Report Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Qr','sub_module_name'=>'listing','module_name'=>'QR Report','module_name_id'=>'57'],
            ['name' => 'qrReport_project', 'group'=>'Report Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Project','sub_module_name'=>'listing','module_name'=>'QR Report','module_name_id'=>'57'],
            ['name' => 'qrReport_name','group'=>'Report Master', 'guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'QR Report','module_name_id'=>'57'],
            ['name' => 'qrReport_directions', 'group'=>'Report Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Directions','sub_module_name'=>'listing','module_name'=>'QR Report','module_name_id'=>'57'],
            ['name' => 'qrReport_information', 'group'=>'Report Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Information','sub_module_name'=>'listing','module_name'=>'QR Report','module_name_id'=>'57'],

            //create in Contact
            ['name' => 'qrReport_checkbox_create','group'=>'Report Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'create','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'QR Report','module_name_id'=>'57'],
            ['name' => 'qrReport_qr_create', 'group'=>'Report Master','guard_name' => 'web','type'=>'create','enable'=>'0','permission_name'=>'Qr','sub_module_name'=>'listing','module_name'=>'QR Report','module_name_id'=>'57'],
            ['name' => 'qrReport_project_create', 'group'=>'Report Master','guard_name' => 'web','type'=>'create','enable'=>'0','permission_name'=>'Project','sub_module_name'=>'listing','module_name'=>'QR Report','module_name_id'=>'57'],
            ['name' => 'qrReport_name_create','group'=>'Report Master', 'guard_name' => 'web','type'=>'create','enable'=>'0','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'QR Report','module_name_id'=>'57'],
            ['name' => 'qrReport_directions_create', 'group'=>'Report Master','guard_name' => 'web','type'=>'create','enable'=>'0','permission_name'=>'Directions','sub_module_name'=>'listing','module_name'=>'QR Report','module_name_id'=>'57'],
            ['name' => 'qrReport_information_create', 'group'=>'Report Master','guard_name' => 'web','type'=>'create','enable'=>'0','permission_name'=>'Information','sub_module_name'=>'listing','module_name'=>'QR Report','module_name_id'=>'57'],

            //word in Contact
            ['name' => 'qrReport_checkbox_word_export','group'=>'Report Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'word_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'QR Report','module_name_id'=>'57'],
            ['name' => 'qrReport_qr_word_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Qr','sub_module_name'=>'listing','module_name'=>'QR Report','module_name_id'=>'57'],
            ['name' => 'qrReport_project_word_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Project','sub_module_name'=>'listing','module_name'=>'QR Report','module_name_id'=>'57'],
            ['name' => 'qrReport_name_word_export','group'=>'Report Master', 'guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'QR Report','module_name_id'=>'57'],
            ['name' => 'qrReport_directions_word_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Directions','sub_module_name'=>'listing','module_name'=>'QR Report','module_name_id'=>'57'],
            ['name' => 'qrReport_information_word_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Information','sub_module_name'=>'listing','module_name'=>'QR Report','module_name_id'=>'57'],

            //excel in Contact
            ['name' => 'qrReport_checkbox_excel_export','group'=>'Report Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'excel_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'QR Report','module_name_id'=>'57'],
            ['name' => 'qrReport_qr_excel_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Qr','sub_module_name'=>'listing','module_name'=>'QR Report','module_name_id'=>'57'],
            ['name' => 'qrReport_project_excel_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Project','sub_module_name'=>'listing','module_name'=>'QR Report','module_name_id'=>'57'],
            ['name' => 'qrReport_name_excel_export','group'=>'Report Master', 'guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'QR Report','module_name_id'=>'57'],
            ['name' => 'qrReport_directions_excel_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Directions','sub_module_name'=>'listing','module_name'=>'QR Report','module_name_id'=>'57'],
            ['name' => 'qrReport_information_excel_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Information','sub_module_name'=>'listing','module_name'=>'QR Report','module_name_id'=>'57'],

            //pdf in Contact
            ['name' => 'qrReport_checkbox_pdf_export','group'=>'Report Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'pdf_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'QR Report','module_name_id'=>'57'],
            ['name' => 'qrReport_qr_pdf_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Qr','sub_module_name'=>'listing','module_name'=>'QR Report','module_name_id'=>'57'],
            ['name' => 'qrReport_project_pdf_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Project','sub_module_name'=>'listing','module_name'=>'QR Report','module_name_id'=>'57'],
            ['name' => 'qrReport_name_pdf_export','group'=>'Report Master', 'guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'QR Report','module_name_id'=>'57'],
            ['name' => 'qrReport_directions_pdf_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Directions','sub_module_name'=>'listing','module_name'=>'QR Report','module_name_id'=>'57'],
            ['name' => 'qrReport_information_pdf_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Information','sub_module_name'=>'listing','module_name'=>'QR Report','module_name_id'=>'57'],


            //edit in Contact
            ['name' => 'qrReport_checkbox_edit','group'=>'Report Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'edit','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'QR Report','module_name_id'=>'57'],
            ['name' => 'qrReport_qr_edit', 'group'=>'Report Master','guard_name' => 'web','type'=>'edit','enable'=>'0','permission_name'=>'Qr','sub_module_name'=>'listing','module_name'=>'QR Report','module_name_id'=>'57'],
            ['name' => 'qrReport_project_edit', 'group'=>'Report Master','guard_name' => 'web','type'=>'edit','enable'=>'0','permission_name'=>'Project','sub_module_name'=>'listing','module_name'=>'QR Report','module_name_id'=>'57'],
            ['name' => 'qrReport_name_edit','group'=>'Report Master', 'guard_name' => 'web','type'=>'edit','enable'=>'0','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'QR Report','module_name_id'=>'57'],
            ['name' => 'qrReport_directions_edit', 'group'=>'Report Master','guard_name' => 'web','type'=>'edit','enable'=>'0','permission_name'=>'Directions','sub_module_name'=>'listing','module_name'=>'QR Report','module_name_id'=>'57'],
            ['name' => 'qrReport_information_edit', 'group'=>'Report Master','guard_name' => 'web','type'=>'edit','enable'=>'0','permission_name'=>'Information','sub_module_name'=>'listing','module_name'=>'QR Report','module_name_id'=>'57'],
        ];
        foreach ($permission_array as $key => $permission) {
            $admin_role = Role::find(1);
            $getpermission=Permission::where('name',$permission['name'])->first();
            if (empty($getpermission)){
                $permission=Permission::create($permission);
                $admin_role->givePermissionTo($permission['name']);
            }else{
                $permission=Permission::where('name',$getpermission->name)->update($permission);
                $admin_role->givePermissionTo($getpermission->name);
            }
        }
    }
}
