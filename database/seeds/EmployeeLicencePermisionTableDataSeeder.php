<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
class EmployeeLicencePermisionTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        $permission_array = [
            // create project permissions
            ['name' => 'view_licence','group'=>'Employee Master', 'guard_name' => 'web','permission_name'=>'view','type'=>'','sub_module_name'=>'operation','module_name'=>'Licence'],
            ['name' => 'create_licence','group'=>'Employee Master', 'guard_name' => 'web','permission_name'=>'create','type'=>'','sub_module_name'=>'operation','module_name'=>'Licence'],
            ['name' => 'edit_licence', 'group'=>'Employee Master', 'guard_name' => 'web','permission_name'=>'edit','type'=>'','sub_module_name'=>'operation','module_name'=>'Licence'],
            ['name' => 'delete_licence','group'=>'Employee Master', 'guard_name' => 'web','permission_name'=>'delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Licence'],
            ['name' => 'delete_selected_licence','group'=>'Employee Master', 'guard_name' => 'web','permission_name'=>'Selected Delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Licence'],
            ['name' => 'selected_restore_licence', 'group'=>'Employee Master','guard_name' => 'web','type'=>'','permission_name'=>'Selected Restore','sub_module_name'=>'operation','module_name'=>'Licence'],
            ['name' => 'restore_delete_licence','group'=>'Employee Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Restore Delete','sub_module_name'=>'operation','module_name'=>'Licence'],
            ['name' => 'hard_delete_licence','group'=>'Employee Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Hard Delete','sub_module_name'=>'operation','module_name'=>'Licence'],
            ['name' => 'active_licence','group'=>'Employee Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Active','sub_module_name'=>'operation','module_name'=>'Licence'],
            ['name' => 'select_active_licence','group'=>'Employee Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Select Active','sub_module_name'=>'operation','module_name'=>'Licence'],
            ['name' => 'word_licence', 'group'=>'Employee Master','guard_name' => 'web','type'=>'','permission_name'=>'Word Export','sub_module_name'=>'operation','module_name'=>'Licence'],
            ['name' => 'csv_licence','group'=>'Employee Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Csv Export','sub_module_name'=>'operation','module_name'=>'Licence'],
            ['name' => 'pdf_licence', 'group'=>'Employee Master','guard_name' => 'web','type'=>'','permission_name'=>'PDF Export','sub_module_name'=>'operation','module_name'=>'Licence'],

            //listing in licence
            ['name' => 'licence_checkbox', 'group'=>'Employee Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Checkbox','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_firstname', 'group'=>'Employee Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'First Name','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_surname','group'=>'Employee Master', 'guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Surname','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_status', 'group'=>'Employee Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Status','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_licence', 'group'=>'Employee Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Licence','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_end_date', 'group'=>'Employee Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'End Date','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_role', 'group'=>'Employee Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Role','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_notes', 'group'=>'Employee Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_licence_no', 'group'=>'Employee Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Licence Number','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_licence_sector', 'group'=>'Employee Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Licence Sector','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_licence_contact', 'group'=>'Employee Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Licence Contact','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_licence_ldn_ref_no', 'group'=>'Employee Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'LDN Reference Number','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_tracking_number', 'group'=>'Employee Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Tracking Number','sub_module_name'=>'listing','module_name'=>'Licence'],

            //word in licence
            ['name' => 'licence_checkbox_word_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'word_export','enable'=>'0','permission_name'=>'Checkbox','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_firstname_word_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'First Name','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_surname_word_export','group'=>'Employee Master', 'guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Surname','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_status_word_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Status','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_licence_word_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Licence','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_end_date_word_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'End Date','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_role_word_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Role','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_notes_word_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_licence_no_word_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Licence Number','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_licence_sector_word_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Licence Sector','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_licence_contact_word_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Licence Contact','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_licence_ldn_ref_no_word_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'LDN Reference Number','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_tracking_number_word_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Tracking Number','sub_module_name'=>'listing','module_name'=>'Licence'],

            //excel in licence
            ['name' => 'licence_checkbox_excel_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'excel_export','enable'=>'0','permission_name'=>'Checkbox','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_firstname_excel_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'First Name','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_surname_excel_export','group'=>'Employee Master', 'guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Surname','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_status_excel_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Status','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_licence_excel_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Licence','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_end_date_excel_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'End Date','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_role_excel_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Role','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_notes_excel_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_licence_no_excel_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Licence Number','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_licence_sector_excel_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Licence Sector','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_licence_contact_excel_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Licence Contact','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_licence_ldn_ref_no_excel_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'LDN Reference Number','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_tracking_number_excel_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Tracking Number','sub_module_name'=>'listing','module_name'=>'Licence'],


            //pdf in licence
            ['name' => 'licence_checkbox_pdf_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'0','permission_name'=>'Checkbox','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_firstname_pdf_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'First Name','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_surname_pdf_export','group'=>'Employee Master', 'guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Surname','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_status_pdf_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Status','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_licence_pdf_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Licence','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_end_date_pdf_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'End Date','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_role_pdf_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Role','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_notes_pdf_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_licence_no_pdf_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Licence Number','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_licence_sector_pdf_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Licence Sector','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_licence_contact_pdf_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Licence Contact','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_licence_ldn_ref_no_pdf_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'LDN Reference Number','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_tracking_number_pdf_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Tracking Number','sub_module_name'=>'listing','module_name'=>'Licence'],


            //create in licence
            ['name' => 'licence_checkbox_create', 'group'=>'Employee Master','guard_name' => 'web','type'=>'create','enable'=>'0','permission_name'=>'Checkbox','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_firstname_create', 'group'=>'Employee Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'First Name','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_surname_create','group'=>'Employee Master', 'guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Surname','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_status_create', 'group'=>'Employee Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Status','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_licence_create', 'group'=>'Employee Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Licence','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_end_date_create', 'group'=>'Employee Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'End Date','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_role_create', 'group'=>'Employee Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Role','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_notes_create', 'group'=>'Employee Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_licence_no_create', 'group'=>'Employee Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Licence Number','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_licence_sector_create', 'group'=>'Employee Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Licence Sector','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_licence_contact_create', 'group'=>'Employee Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Licence Contact','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_licence_ldn_ref_no_create', 'group'=>'Employee Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'LDN Reference Number','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_tracking_number_create', 'group'=>'Employee Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Tracking Number','sub_module_name'=>'listing','module_name'=>'Licence'],


            //edit in licence
            ['name' => 'licence_checkbox_edit', 'group'=>'Employee Master','guard_name' => 'web','type'=>'edit','enable'=>'0','permission_name'=>'Checkbox','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_firstname_edit', 'group'=>'Employee Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'First Name','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_surname_edit','group'=>'Employee Master', 'guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Surname','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_status_edit', 'group'=>'Employee Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Status','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_licence_edit', 'group'=>'Employee Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Licence','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_end_date_edit', 'group'=>'Employee Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'End Date','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_role_edit', 'group'=>'Employee Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Role','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_notes_edit', 'group'=>'Employee Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_licence_no_edit', 'group'=>'Employee Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Licence Number','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_licence_sector_edit', 'group'=>'Employee Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Licence Sector','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_licence_contact_edit', 'group'=>'Employee Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Licence Contact','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_licence_ldn_ref_no_edit', 'group'=>'Employee Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'LDN Reference Number','sub_module_name'=>'listing','module_name'=>'Licence'],
            ['name' => 'licence_tracking_number_edit', 'group'=>'Employee Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Tracking Number','sub_module_name'=>'listing','module_name'=>'Licence'],
        ];
        foreach ($permission_array as $key => $permission) {
            $admin_role = Role::find(1);
            $getpermission=Permission::where('name',$permission['name'])->first();
            if (empty($getpermission)){
                $permission=Permission::create($permission);
                $admin_role->givePermissionTo($permission['name']);
            }else{
                $permission=Permission::where('name',$getpermission->name)->update($permission);
                $admin_role->givePermissionTo($getpermission->name);
            }
        }
    }
}
