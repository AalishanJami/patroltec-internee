<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
class DocumentDynamicFormPermissionTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission_array = [
            // create Contact permissions
            ['name' => 'form_view_documentDynamicForm','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'View','type'=>'','sub_module_name'=>'operation','module_name'=>'Dynamic Form','module_name_id'=>'53'],
            ['name' => 'form_create_documentDynamicForm','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'Create','type'=>'','sub_module_name'=>'operation','module_name'=>'Dynamic Form','module_name_id'=>'53'],
            ['name' => 'form_edit_documentDynamicForm', 'group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'Edit','type'=>'','sub_module_name'=>'operation','module_name'=>'Dynamic Form','module_name_id'=>'53'],
            ['name' => 'form_delete_documentDynamicForm','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'Delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Dynamic Form','module_name_id'=>'53'],
            ['name' => 'form_share_documentDynamicForm','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'Share','type'=>'','sub_module_name'=>'operation','module_name'=>'Dynamic Form','module_name_id'=>'53'],
            ['name' => 'form_activate_documentDynamicForm','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'Activate','type'=>'','sub_module_name'=>'operation','module_name'=>'Dynamic Form','module_name_id'=>'53'],
            ['name' => 'form_duplicate_documentDynamicForm','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'Duplicate','type'=>'','sub_module_name'=>'operation','module_name'=>'Dynamic Form','module_name_id'=>'53'],

            //form view column
            ['name' => 'documentDynamicForm_form_name','group'=>'Document Master', 'guard_name' => 'web','enable'=>'1','permission_name'=>'Form Name','type'=>'view','sub_module_name'=>'listing','module_name'=>'Dynamic Form','module_name_id'=>'53'],
            ['name' => 'documentDynamicForm_form_header','group'=>'Document Master', 'guard_name' => 'web','enable'=>'1','permission_name'=>'Form Header','type'=>'view','sub_module_name'=>'listing','module_name'=>'Dynamic Form','module_name_id'=>'53'],
            ['name' => 'documentDynamicForm_form_footer', 'group'=>'Document Master', 'guard_name' => 'web','enable'=>'1','permission_name'=>'Form Footer','type'=>'view','sub_module_name'=>'listing','module_name'=>'Dynamic Form','module_name_id'=>'53'],
            ['name' => 'documentDynamicForm_form_formwidth','group'=>'Document Master', 'guard_name' => 'web','enable'=>'1','permission_name'=>'Form Width','type'=>'view','sub_module_name'=>'listing','module_name'=>'Dynamic Form','module_name_id'=>'53'],
            ['name' => 'documentDynamicForm_form_border','group'=>'Document Master', 'guard_name' => 'web','enable'=>'1','permission_name'=>'Form Border','type'=>'view','sub_module_name'=>'listing','module_name'=>'Dynamic Form','module_name_id'=>'53'],

            //form create column
            ['name' => 'documentDynamicForm_form_name_create','group'=>'Document Master', 'guard_name' => 'web','enable'=>'1','permission_name'=>'Form Name','type'=>'create','sub_module_name'=>'listing','module_name'=>'Dynamic Form','module_name_id'=>'53'],
            ['name' => 'documentDynamicForm_form_header_create','group'=>'Document Master', 'guard_name' => 'web','enable'=>'1','permission_name'=>'Form Header','type'=>'create','sub_module_name'=>'listing','module_name'=>'Dynamic Form','module_name_id'=>'53'],
            ['name' => 'documentDynamicForm_form_footer_create', 'group'=>'Document Master', 'guard_name' => 'web','enable'=>'1','permission_name'=>'Form Footer','type'=>'create','sub_module_name'=>'listing','module_name'=>'Dynamic Form','module_name_id'=>'53'],
            ['name' => 'documentDynamicForm_form_formwidth_create','group'=>'Document Master', 'guard_name' => 'web','enable'=>'1','permission_name'=>'Form Width','type'=>'create','sub_module_name'=>'listing','module_name'=>'Dynamic Form','module_name_id'=>'53'],
            ['name' => 'documentDynamicForm_form_border_create','group'=>'Document Master', 'guard_name' => 'web','enable'=>'1','permission_name'=>'Form Border','type'=>'create','sub_module_name'=>'listing','module_name'=>'Dynamic Form','module_name_id'=>'53'],


            //word export
            ['name' => 'documentDynamicForm_form_name_word_export','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0','permission_name'=>'Form Name','type'=>'word_export','sub_module_name'=>'listing','module_name'=>'Dynamic Form','module_name_id'=>'53'],
            ['name' => 'documentDynamicForm_form_header_word_export','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0','permission_name'=>'Form Header','type'=>'word_export','sub_module_name'=>'listing','module_name'=>'Dynamic Form','module_name_id'=>'53'],
            ['name' => 'documentDynamicForm_form_footer_word_export', 'group'=>'Document Master', 'guard_name' => 'web','enable'=>'0','permission_name'=>'Form Footer','type'=>'word_export','sub_module_name'=>'listing','module_name'=>'Dynamic Form','module_name_id'=>'53'],
            ['name' => 'documentDynamicForm_form_formwidth_word_export','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0','permission_name'=>'Form Width','type'=>'word_export','sub_module_name'=>'listing','module_name'=>'Dynamic Form','module_name_id'=>'53'],
            ['name' => 'documentDynamicForm_form_border_word_export','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0','permission_name'=>'Form Border','type'=>'word_export','sub_module_name'=>'listing','module_name'=>'Dynamic Form','module_name_id'=>'53'],

            //excel export
            ['name' => 'documentDynamicForm_form_name_excel_export','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0','permission_name'=>'Form Name','type'=>'excel_export','sub_module_name'=>'listing','module_name'=>'Dynamic Form','module_name_id'=>'53'],
            ['name' => 'documentDynamicForm_form_header_excel_export','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0','permission_name'=>'Form Header','type'=>'excel_export','sub_module_name'=>'listing','module_name'=>'Dynamic Form','module_name_id'=>'53'],
            ['name' => 'documentDynamicForm_form_footer_excel_export', 'group'=>'Document Master', 'guard_name' => 'web','enable'=>'0','permission_name'=>'Form Footer','type'=>'excel_export','sub_module_name'=>'listing','module_name'=>'Dynamic Form','module_name_id'=>'53'],
            ['name' => 'documentDynamicForm_form_formwidth_excel_export','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0','permission_name'=>'Form Width','type'=>'excel_export','sub_module_name'=>'listing','module_name'=>'Dynamic Form','module_name_id'=>'53'],
            ['name' => 'documentDynamicForm_form_border_excel_export','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0','permission_name'=>'Form Border','type'=>'excel_export','sub_module_name'=>'listing','module_name'=>'Dynamic Form','module_name_id'=>'53'],

            //pdf export
            ['name' => 'documentDynamicForm_form_name_pdf_export','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0','permission_name'=>'Form Name','type'=>'pdf_export','sub_module_name'=>'listing','module_name'=>'Dynamic Form','module_name_id'=>'53'],
            ['name' => 'documentDynamicForm_form_header_pdf_export','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0','permission_name'=>'Form Header','type'=>'pdf_export','sub_module_name'=>'listing','module_name'=>'Dynamic Form','module_name_id'=>'53'],
            ['name' => 'documentDynamicForm_form_footer_pdf_export', 'group'=>'Document Master', 'guard_name' => 'web','enable'=>'0','permission_name'=>'Form Footer','type'=>'pdf_export','sub_module_name'=>'listing','module_name'=>'Dynamic Form','module_name_id'=>'53'],
            ['name' => 'documentDynamicForm_form_formwidth_pdf_export','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0','permission_name'=>'Form Width','type'=>'pdf_export','sub_module_name'=>'listing','module_name'=>'Dynamic Form','module_name_id'=>'53'],
            ['name' => 'documentDynamicForm_form_border_pdf_export','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0','permission_name'=>'Form Border','type'=>'pdf_export','sub_module_name'=>'listing','module_name'=>'Dynamic Form','module_name_id'=>'53'],


            //form edit column
            ['name' => 'documentDynamicForm_form_name_edit','group'=>'Document Master', 'guard_name' => 'web','enable'=>'1','permission_name'=>'Form Name','type'=>'edit','sub_module_name'=>'listing','module_name'=>'Dynamic Form','module_name_id'=>'53'],
            ['name' => 'documentDynamicForm_form_header_edit','group'=>'Document Master', 'guard_name' => 'web','enable'=>'1','permission_name'=>'Form Header','type'=>'edit','sub_module_name'=>'listing','module_name'=>'Dynamic Form','module_name_id'=>'53'],
            ['name' => 'documentDynamicForm_form_footer_edit', 'group'=>'Document Master', 'guard_name' => 'web','enable'=>'1','permission_name'=>'Form Footer','type'=>'edit','sub_module_name'=>'listing','module_name'=>'Dynamic Form','module_name_id'=>'53'],
            ['name' => 'documentDynamicForm_form_formwidth_edit','group'=>'Document Master', 'guard_name' => 'web','enable'=>'1','permission_name'=>'Form Width','type'=>'edit','sub_module_name'=>'listing','module_name'=>'Dynamic Form','module_name_id'=>'53'],
            ['name' => 'documentDynamicForm_form_border_edit','group'=>'Document Master', 'guard_name' => 'web','enable'=>'1','permission_name'=>'Form Border','type'=>'edit','sub_module_name'=>'listing','module_name'=>'Dynamic Form','module_name_id'=>'53'],
        ];

        foreach ($permission_array as $key => $permission) {
            $admin_role = Role::find(1);
            //Permission::where('name',$permission['name'])->delete();
            $getpermission=Permission::where('name',$permission['name'])->first();
            if (empty($getpermission)){
                $permission=Permission::create($permission);
                $admin_role->givePermissionTo($permission['name']);
            }else{
                $permission=Permission::where('name',$getpermission->name)->update($permission);
                $admin_role->givePermissionTo($getpermission->name);
            }
        }
    }
}
