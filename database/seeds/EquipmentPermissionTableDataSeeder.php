<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class EquipmentPermissionTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission_array = [
            // create Contact permissions
            ['name' => 'view_equipment','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'view','type'=>'','sub_module_name'=>'operation','module_name'=>'Equipment'],
            ['name' => 'create_equipment','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'create','type'=>'','sub_module_name'=>'operation','module_name'=>'Equipment'],
            ['name' => 'edit_equipment', 'group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'edit','type'=>'','sub_module_name'=>'operation','module_name'=>'Equipment'],
            ['name' => 'delete_equipment','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Equipment'],
            ['name' => 'selected_delete_equipment','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'Selected Delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Equipment'],
            ['name' => 'selected_restore_delete_equipment', 'group'=>'Project Master','guard_name' => 'web','type'=>'','permission_name'=>'Selected Restore','sub_module_name'=>'operation','module_name'=>'Equipment'],
            ['name' => 'restore_delete_equipment','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Restore Delete','sub_module_name'=>'operation','module_name'=>'Equipment'],
            ['name' => 'hard_delete_equipment','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Hard Delete','sub_module_name'=>'operation','module_name'=>'Equipment'],
            ['name' => 'active_equipment','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Active Equipment Group','sub_module_name'=>'operation','module_name'=>'Equipment'],
            ['name' => 'word_equipment', 'group'=>'Project Master','guard_name' => 'web','type'=>'','permission_name'=>'Word Export Equipment Group','sub_module_name'=>'operation','module_name'=>'Equipment'],
            ['name' => 'csv_equipment','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Csv Export Equipment Group','sub_module_name'=>'operation','module_name'=>'Equipment'],
            ['name' => 'pdf_equipment', 'group'=>'Project Master','guard_name' => 'web','type'=>'','permission_name'=>'PDF Export Equipment Group','sub_module_name'=>'operation','module_name'=>'Equipment'],

            //listing in Contact
            ['name' => 'equipment_checkbox','group'=>'Project Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Equipment'],
            ['name' => 'equipment_name','group'=>'Project Master', 'guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Equipment'],

            //word in Contact
            ['name' => 'equipment_checkbox_word_export','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'word_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Equipment'],
            ['name' => 'equipment_name_word_export','group'=>'Project Master', 'guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Equipment'],

            //excel in Contact
            ['name' => 'equipment_checkbox_excel_export','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'excel_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Equipment'],
            ['name' => 'equipment_name_excel_export','group'=>'Project Master', 'guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Equipment'],

            //pdf in Contact
            ['name' => 'equipment_checkbox_pdf_export','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'pdf_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Equipment'],
            ['name' => 'equipment_name_pdf_export','group'=>'Project Master', 'guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Equipment'],

            //create in Contact
            ['name' => 'equipment_checkbox_create','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'create','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Equipment'],
            ['name' => 'equipment_name_create','group'=>'Project Master', 'guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Equipment'],

            //edit in Contact
            ['name' => 'equipment_checkbox_edit','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'edit','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Equipment'],
            ['name' => 'equipment_name_edit','group'=>'Project Master', 'guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Equipment'],
        ];

        foreach ($permission_array as $key => $permission) {
            $admin_role = Role::find(1);
            $getpermission=Permission::where('name',$permission['name'])->first();
            if (empty($getpermission)){
                $permission=Permission::create($permission);
                $admin_role->givePermissionTo($permission['name']);
            }else{
                $permission=Permission::where('name',$getpermission->name)->update($permission);
                $admin_role->givePermissionTo($getpermission->name);
            }
        }
    }
}
