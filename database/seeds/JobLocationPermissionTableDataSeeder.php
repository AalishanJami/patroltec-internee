<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class JobLocationPermissionTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission_array = [
            // create Contact permissions
            ['name' => 'view_joblocation','group'=>'Job Master', 'guard_name' => 'web','permission_name'=>'view','type'=>'','sub_module_name'=>'operation','module_name'=>'Job Location','module_name_id'=>'63'],
            ['name' => 'create_joblocation','group'=>'Job Master', 'guard_name' => 'web','permission_name'=>'create','type'=>'','sub_module_name'=>'operation','module_name'=>'Job Location','module_name_id'=>'63'],
            ['name' => 'edit_joblocation', 'group'=>'Job Master', 'guard_name' => 'web','permission_name'=>'edit','type'=>'','sub_module_name'=>'operation','module_name'=>'Job Location','module_name_id'=>'63'],
            ['name' => 'delete_joblocation','group'=>'Job Master', 'guard_name' => 'web','permission_name'=>'delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Job Location','module_name_id'=>'63'],
            ['name' => 'selected_delete_joblocation','group'=>'Job Master', 'guard_name' => 'web','permission_name'=>'Selected Delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Job Location','module_name_id'=>'63'],
            ['name' => 'selected_restore_delete_joblocation', 'group'=>'Job Master','guard_name' => 'web','type'=>'','permission_name'=>'Selected Restore','sub_module_name'=>'operation','module_name'=>'Job Location','module_name_id'=>'63'],
            ['name' => 'restore_delete_joblocation','group'=>'Job Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Restore Delete','sub_module_name'=>'operation','module_name'=>'Job Location','module_name_id'=>'63'],
            ['name' => 'hard_delete_joblocation','group'=>'Job Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Hard Delete','sub_module_name'=>'operation','module_name'=>'Job Location','module_name_id'=>'63'],
            ['name' => 'active_joblocation','group'=>'Job Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Active','sub_module_name'=>'operation','module_name'=>'Job Location','module_name_id'=>'63'],
            ['name' => 'selected_active_joblocation','group'=>'Job Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Selected Active ','sub_module_name'=>'operation','module_name'=>'Job Location','module_name_id'=>'63'],
            ['name' => 'word_joblocation', 'group'=>'Job Master','guard_name' => 'web','type'=>'','permission_name'=>'Word Export ','sub_module_name'=>'operation','module_name'=>'Job Location','module_name_id'=>'63'],
            ['name' => 'csv_joblocation','group'=>'Job Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Csv Export ','sub_module_name'=>'operation','module_name'=>'Job Location','module_name_id'=>'63'],
            ['name' => 'pdf_joblocation', 'group'=>'Job Master','guard_name' => 'web','type'=>'','permission_name'=>'PDF Export','sub_module_name'=>'operation','module_name'=>'Job Location','module_name_id'=>'63'],

            //listing in Contact
            ['name' => 'joblocation_checkbox','group'=>'Job Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Job Location','module_name_id'=>'63'],
            ['name' => 'joblocation_name','group'=>'Job Master', 'guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Job Location','module_name_id'=>'63'],

            //word in Contact
            ['name' => 'joblocation_checkbox_word_export','group'=>'Job Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'word_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Job Location','module_name_id'=>'63'],
            ['name' => 'joblocation_name_word_export','group'=>'Job Master', 'guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Job Location','module_name_id'=>'63'],

            //excel in Contact
            ['name' => 'joblocation_checkbox_excel_export','group'=>'Job Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'excel_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Job Location','module_name_id'=>'63'],
            ['name' => 'joblocation_name_excel_export','group'=>'Job Master', 'guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Job Location','module_name_id'=>'63'],

            //pdf in Contact
            ['name' => 'joblocation_checkbox_pdf_export','group'=>'Job Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'pdf_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Job Location','module_name_id'=>'63'],
            ['name' => 'joblocation_name_pdf_export','group'=>'Job Master', 'guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Job Location','module_name_id'=>'63'],

            //create in Contact
            ['name' => 'joblocation_checkbox_create','group'=>'Job Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'create','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Job Location','module_name_id'=>'63'],
            ['name' => 'joblocation_name_create','group'=>'Job Master', 'guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Job Location','module_name_id'=>'63'],

            //edit in Contact
            ['name' => 'joblocation_checkbox_edit','group'=>'Job Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'edit','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Job Location','module_name_id'=>'63'],
            ['name' => 'joblocation_name_edit','group'=>'Job Master', 'guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Job Location','module_name_id'=>'63'],
        ];

        foreach ($permission_array as $key => $permission) {
            $admin_role = Role::find(1);
            $getpermission=Permission::where('name',$permission['name'])->first();
            if (empty($getpermission)){
                $permission=Permission::create($permission);
                $admin_role->givePermissionTo($permission['name']);
            }else{
                $permission=Permission::where('name',$getpermission->name)->update($permission);
                $admin_role->givePermissionTo($getpermission->name);
            }
        }
    }
}
