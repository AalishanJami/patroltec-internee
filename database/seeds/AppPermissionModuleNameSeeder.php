<?php

use Illuminate\Database\Seeder;

class AppPermissionModuleNameSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         \DB::table('app_permissions_module_names')->insert(array (
        	0 => 
            array (
                'id' => 1,
                'name' => 'Jobs',
                'active' => 1,
                'deleted' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Completed Jobs',
                'active' => 1,
                'deleted' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Forms',
                'active' => 1,
                'deleted' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Completed Forms',
                'active' => 1,
                'deleted' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Raise Form',
                'active' => 1,
                'deleted' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Write Tags',
                'active' => 1,
                'deleted' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Write Keys',
                'active' => 1,
                'deleted' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Faqs',
                'active' => 1,
                'deleted' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Contact Us',
                'active' => 1,
                'deleted' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Profile',
                'active' => 1,
                'deleted' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'Customers',
                'active' => 1,
                'deleted' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'About Us',
                'active' => 1,
                'deleted' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'Share Account',
                'active' => 1,
                'deleted' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
    }
}
