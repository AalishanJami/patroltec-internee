<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
class ClientNotesPermissionTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission_array = [
            // create Contact permissions
            ['name' => 'view_clientNotes','group'=>'Client Master', 'guard_name' => 'web','permission_name'=>'view','type'=>'','sub_module_name'=>'operation','module_name'=>'Client Notes'],
            ['name' => 'create_clientNotes','group'=>'Client Master', 'guard_name' => 'web','permission_name'=>'create','type'=>'','sub_module_name'=>'operation','module_name'=>'Client Notes'],
            ['name' => 'edit_clientNotes', 'group'=>'Client Master', 'guard_name' => 'web','permission_name'=>'edit','type'=>'','sub_module_name'=>'operation','module_name'=>'Client Notes'],
            ['name' => 'delete_clientNotes','group'=>'Client Master', 'guard_name' => 'web','permission_name'=>'delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Client Notes'],
            ['name' => 'selected_delete_clientNotes','group'=>'Client Master', 'guard_name' => 'web','permission_name'=>'Selected Delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Client Notes'],
            ['name' => 'selected_restore_delete_clientNotes', 'group'=>'Client Master','guard_name' => 'web','type'=>'','permission_name'=>'Selected Restore','sub_module_name'=>'operation','module_name'=>'Client Notes'],
            ['name' => 'restore_delete_clientNotes','group'=>'Client Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Restore Delete','sub_module_name'=>'operation','module_name'=>'Client Notes'],
            ['name' => 'hard_delete_clientNotes','group'=>'Client Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Hard Delete','sub_module_name'=>'operation','module_name'=>'Client Notes'],
            ['name' => 'active_clientNotes','group'=>'Client Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Active Client','sub_module_name'=>'operation','module_name'=>'Client Notes'],
            ['name' => 'word_clientNotes', 'group'=>'Client Master','guard_name' => 'web','type'=>'','permission_name'=>'Word Export Client','sub_module_name'=>'operation','module_name'=>'Client Notes'],
            ['name' => 'csv_clientNotes','group'=>'Client Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Csv Export Client','sub_module_name'=>'operation','module_name'=>'Client Notes'],
            ['name' => 'pdf_clientNotes', 'group'=>'Client Master','guard_name' => 'web','type'=>'','permission_name'=>'PDF Export Client','sub_module_name'=>'operation','module_name'=>'Client Notes'],

            //listing in Contact
            ['name' => 'clientNotes_checkbox','group'=>'Client Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Client Notes'],
            ['name' => 'clientNotes_notes','group'=>'Client Master', 'guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Client Notes'],
            ['name' => 'clientNotes_subject', 'group'=>'Client Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Subject','sub_module_name'=>'listing','module_name'=>'Client Notes'],

            //word in Contact
            ['name' => 'clientNotes_checkbox_word_export','group'=>'Client Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'word_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Client Notes'],
            ['name' => 'clientNotes_notes_word_export','group'=>'Client Master', 'guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Client Notes'],
            ['name' => 'clientNotes_subject_word_export', 'group'=>'Client Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Subject','sub_module_name'=>'listing','module_name'=>'Client Notes'],

            //excel in Contact
            ['name' => 'clientNotes_checkbox_excel_export','group'=>'Client Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'excel_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Client Notes'],
            ['name' => 'clientNotes_notes_excel_export','group'=>'Client Master', 'guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Client Notes'],
            ['name' => 'clientNotes_subject_excel_export', 'group'=>'Client Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Subject','sub_module_name'=>'listing','module_name'=>'Client Notes'],

            //pdf in Contact
            ['name' => 'clientNotes_checkbox_pdf_export','group'=>'Client Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'pdf_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Client Notes'],
            ['name' => 'clientNotes_notes_pdf_export','group'=>'Client Master', 'guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Client Notes'],
            ['name' => 'clientNotes_subject_pdf_export', 'group'=>'Client Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Subject','sub_module_name'=>'listing','module_name'=>'Client Notes'],

            //create in Contact
            ['name' => 'clientNotes_checkbox_create','group'=>'Client Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'create','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Client Notes'],
            ['name' => 'clientNotes_notes_create','group'=>'Client Master', 'guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Client Notes'],
            ['name' => 'clientNotes_subject_create', 'group'=>'Client Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Subject','sub_module_name'=>'listing','module_name'=>'Client Notes'],

            //edit in Contact
            ['name' => 'clientNotes_checkbox_edit','group'=>'Client Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'edit','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Client Notes'],
            ['name' => 'clientNotes_notes_edit','group'=>'Client Master', 'guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Client Notes'],
            ['name' => 'clientNotes_subject_edit', 'group'=>'Client Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Subject','sub_module_name'=>'listing','module_name'=>'Client Notes'],
        ];

        foreach ($permission_array as $key => $permission) {
            $admin_role = Role::find(1);
            $getpermission=Permission::where('name',$permission['name'])->first();
            if (empty($getpermission)){
                $permission=Permission::create($permission);
                $admin_role->givePermissionTo($permission['name']);
            }else{
                $permission=Permission::where('name',$getpermission->name)->update($permission);
                $admin_role->givePermissionTo($getpermission->name);
            }
        }
    }
}
