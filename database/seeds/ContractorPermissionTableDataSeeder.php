<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class ContractorPermissionTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission_array = [
            // create Contact permissions
            ['name' => 'view_contractor','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'view','type'=>'','sub_module_name'=>'operation','module_name'=>'Contractor'],
            ['name' => 'create_contractor','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'create','type'=>'','sub_module_name'=>'operation','module_name'=>'Contractor'],
            ['name' => 'edit_contractor', 'group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'edit','type'=>'','sub_module_name'=>'operation','module_name'=>'Contractor'],
            ['name' => 'delete_contractor','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Contractor'],
            ['name' => 'selected_delete_contractor','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'Selected Delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Contractor'],
            ['name' => 'selected_restore_delete_contractor', 'group'=>'Project Master','guard_name' => 'web','type'=>'','permission_name'=>'Selected Restore','sub_module_name'=>'operation','module_name'=>'Contractor'],
            ['name' => 'restore_delete_contractor','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Restore Delete','sub_module_name'=>'operation','module_name'=>'Contractor'],
            ['name' => 'hard_delete_contractor','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Hard Delete','sub_module_name'=>'operation','module_name'=>'Contractor'],
            ['name' => 'active_contractor','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Active','sub_module_name'=>'operation','module_name'=>'Contractor'],
            ['name' => 'selected_active_contractor','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Selected Active','sub_module_name'=>'operation','module_name'=>'Contractor'],
            ['name' => 'word_contractor', 'group'=>'Project Master','guard_name' => 'web','type'=>'','permission_name'=>'Word Export','sub_module_name'=>'operation','module_name'=>'Contractor'],
            ['name' => 'csv_contractor','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Csv Export','sub_module_name'=>'operation','module_name'=>'Contractor'],
            ['name' => 'pdf_contractor', 'group'=>'Project Master','guard_name' => 'web','type'=>'','permission_name'=>'PDF Export','sub_module_name'=>'operation','module_name'=>'Contractor'],

            //listing in Contact
            ['name' => 'contractor_checkbox','group'=>'Project Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Contractor'],
            ['name' => 'contractor_name','group'=>'Project Master', 'guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Contractor'],
            ['name' => 'contractor_start_date', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Start Date','sub_module_name'=>'listing','module_name'=>'Contractor'],
            ['name' => 'contractor_end_date', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'End Date','sub_module_name'=>'listing','module_name'=>'Contractor'],
            ['name' => 'contractor_allowed', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Allowed','sub_module_name'=>'listing','module_name'=>'Contractor'],
            ['name' => 'contractor_banned', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Banned','sub_module_name'=>'listing','module_name'=>'Contractor'],

            //word in Contact
            ['name' => 'contractor_checkbox_word_export','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'word_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Contractor'],
            ['name' => 'contractor_name_word_export','group'=>'Project Master', 'guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Contractor'],
            ['name' => 'contractor_start_date_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Start Date','sub_module_name'=>'listing','module_name'=>'Contractor'],
            ['name' => 'contractor_end_date_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'End Date','sub_module_name'=>'listing','module_name'=>'Contractor'],
            ['name' => 'contractor_allowed_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Allowed','sub_module_name'=>'listing','module_name'=>'Contractor'],
            ['name' => 'contractor_banned_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Banned','sub_module_name'=>'listing','module_name'=>'Contractor'],

            //excel in Contact
            ['name' => 'contractor_checkbox_excel_export','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'excel_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Contractor'],
            ['name' => 'contractor_name_excel_export','group'=>'Project Master', 'guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Contractor'],
            ['name' => 'contractor_start_date_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Start Date','sub_module_name'=>'listing','module_name'=>'Contractor'],
            ['name' => 'contractor_end_date_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'End Date','sub_module_name'=>'listing','module_name'=>'Contractor'],
            ['name' => 'contractor_allowed_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Allowed','sub_module_name'=>'listing','module_name'=>'Contractor'],
            ['name' => 'contractor_banned_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Banned','sub_module_name'=>'listing','module_name'=>'Contractor'],

            //pdf in Contact
            ['name' => 'contractor_checkbox_pdf_export','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'pdf_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Contractor'],
            ['name' => 'contractor_name_pdf_export','group'=>'Project Master', 'guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Contractor'],
            ['name' => 'contractor_start_date_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Start Date','sub_module_name'=>'listing','module_name'=>'Contractor'],
            ['name' => 'contractor_end_date_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'End Date','sub_module_name'=>'listing','module_name'=>'Contractor'],
            ['name' => 'contractor_allowed_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Allowed','sub_module_name'=>'listing','module_name'=>'Contractor'],
            ['name' => 'contractor_banned_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Banned','sub_module_name'=>'listing','module_name'=>'Contractor'],

            //create in Contact
            ['name' => 'contractor_checkbox_create','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'create','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Contractor'],
            ['name' => 'contractor_name_create','group'=>'Project Master', 'guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Contractor'],
            ['name' => 'contractor_start_date_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Start Date','sub_module_name'=>'listing','module_name'=>'Contractor'],
            ['name' => 'contractor_end_date_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'End Date','sub_module_name'=>'listing','module_name'=>'Contractor'],
            ['name' => 'contractor_allowed_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Allowed','sub_module_name'=>'listing','module_name'=>'Contractor'],
            ['name' => 'contractor_banned_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Banned','sub_module_name'=>'listing','module_name'=>'Contractor'],

            //edit in Contact
            ['name' => 'contractor_checkbox_edit','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'edit','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Contractor'],
            ['name' => 'contractor_name_edit','group'=>'Project Master', 'guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Contractor'],
            ['name' => 'contractor_start_date_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Start Date','sub_module_name'=>'listing','module_name'=>'Contractor'],
            ['name' => 'contractor_end_date_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'End Date','sub_module_name'=>'listing','module_name'=>'Contractor'],
            ['name' => 'contractor_allowed_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Allowed','sub_module_name'=>'listing','module_name'=>'Contractor'],
            ['name' => 'contractor_banned_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Banned','sub_module_name'=>'listing','module_name'=>'Contractor'],
        ];

        foreach ($permission_array as $key => $permission) {
            $admin_role = Role::find(1);
            $getpermission=Permission::where('name',$permission['name'])->first();
            if (empty($getpermission)){
                $permission=Permission::create($permission);
                $admin_role->givePermissionTo($permission['name']);
            }else{
                $permission=Permission::where('name',$getpermission->name)->update($permission);
                $admin_role->givePermissionTo($getpermission->name);
            }
        }
    }
}
