<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
class DocumentVersionlogPermissionTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission_array = [
            // create Contact permissions
            ['name' => 'view_documentVersionlogList','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'View','type'=>'','sub_module_name'=>'operation','module_name'=>'Version Log','module_name_id'=>'55'],
            ['name' => 'create_documentVersionlogList','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'Create','type'=>'','sub_module_name'=>'operation','module_name'=>'Version Log','module_name_id'=>'55'],
            ['name' => 'edit_documentVersionlogList', 'group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'Edit','type'=>'','sub_module_name'=>'operation','module_name'=>'Version Log','module_name_id'=>'55'],
            ['name' => 'delete_documentVersionlogList','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'Delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Version Log','module_name_id'=>'55'],
            ['name' => 'selected_delete_documentVersionlogList','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'Selected Delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Version Log','module_name_id'=>'55'],
            ['name' => 'selected_restore_delete_documentVersionlogList', 'group'=>'Document Master','guard_name' => 'web','type'=>'','permission_name'=>'Selected Restore','sub_module_name'=>'operation','module_name'=>'Version Log','module_name_id'=>'55'],
            ['name' => 'restore_delete_documentVersionlogList','group'=>'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Restore Delete','sub_module_name'=>'operation','module_name'=>'Version Log','module_name_id'=>'55'],
            ['name' => 'hard_delete_documentVersionlogList','group'=>'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Hard Delete','sub_module_name'=>'operation','module_name'=>'Version Log','module_name_id'=>'55'],
            ['name' => 'viewdetail_documentVersionlogList','group'=>'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'View Detail','sub_module_name'=>'operation','module_name'=>'Version Log','module_name_id'=>'55'],
            ['name' => 'selected_active_documentVersionlogList','group'=>'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Selected Active ','sub_module_name'=>'operation','module_name'=>'Version Log','module_name_id'=>'55'],
            ['name' => 'show_active_documentVersionlogList','group'=>'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Show Active ','sub_module_name'=>'operation','module_name'=>'Version Log','module_name_id'=>'55'],
            ['name' => 'word_documentVersionlogList', 'group'=>'Document Master','guard_name' => 'web','type'=>'','permission_name'=>'Word Export ','sub_module_name'=>'operation','module_name'=>'Version Log','module_name_id'=>'55'],
            ['name' => 'csv_documentVersionlogList','group'=>'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Csv Export ','sub_module_name'=>'operation','module_name'=>'Version Log','module_name_id'=>'55'],
            ['name' => 'pdf_documentVersionlogList', 'group'=>'Document Master','guard_name' => 'web','type'=>'','permission_name'=>'PDF Export ','sub_module_name'=>'operation','module_name'=>'Version Log','module_name_id'=>'55'],

            //listing in versionlog
            ['name' => 'documentVersionlogList_checkbox','group'=>'Document Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Version Log','module_name_id'=>'55'],
            ['name' => 'documentVersionlogList_versiontype','group'=>'Document Master', 'guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Version Type','sub_module_name'=>'listing','module_name'=>'Version Log','module_name_id'=>'55'],
            ['name' => 'documentVersionlogList_formname', 'group'=>'Document Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Form Name','sub_module_name'=>'listing','module_name'=>'Version Log','module_name_id'=>'55'],
            ['name' => 'documentVersionlogList_username', 'group'=>'Document Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'User Name','sub_module_name'=>'listing','module_name'=>'Version Log','module_name_id'=>'55'],
            ['name' => 'documentVersionlogList_date', 'group'=>'Document Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Date','sub_module_name'=>'listing','module_name'=>'Version Log','module_name_id'=>'55'],

            //word in versionlog
            ['name' => 'documentVersionlogList_checkbox_word_export','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'word_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Version Log','module_name_id'=>'55'],
            ['name' => 'documentVersionlogList_versiontype_word_export','group'=>'Document Master', 'guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Version Type','sub_module_name'=>'listing','module_name'=>'Version Log','module_name_id'=>'55'],
            ['name' => 'documentVersionlogList_formname_word_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Form Name','sub_module_name'=>'listing','module_name'=>'Version Log','module_name_id'=>'55'],
            ['name' => 'documentVersionlogList_username_word_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'User Name','sub_module_name'=>'listing','module_name'=>'Version Log','module_name_id'=>'55'],
            ['name' => 'documentVersionlogList_date_word_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Date','sub_module_name'=>'listing','module_name'=>'Version Log','module_name_id'=>'55'],

            //excel in versionlog
            ['name' => 'documentVersionlogList_checkbox_excel_export','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'excel_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Version Log','module_name_id'=>'55'],
            ['name' => 'documentVersionlogList_versiontype_excel_export','group'=>'Document Master', 'guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Version Type','sub_module_name'=>'listing','module_name'=>'Version Log','module_name_id'=>'55'],
            ['name' => 'documentVersionlogList_formname_excel_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Form Name','sub_module_name'=>'listing','module_name'=>'Version Log','module_name_id'=>'55'],
            ['name' => 'documentVersionlogList_username_excel_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'User Name','sub_module_name'=>'listing','module_name'=>'Version Log','module_name_id'=>'55'],
            ['name' => 'documentVersionlogList_date_excel_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Date','sub_module_name'=>'listing','module_name'=>'Version Log','module_name_id'=>'55'],

            //pdf in versionlog
            ['name' => 'documentVersionlogList_checkbox_pdf_export','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'pdf_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Version Log','module_name_id'=>'55'],
            ['name' => 'documentVersionlogList_versiontype_pdf_export','group'=>'Document Master', 'guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Version Type','sub_module_name'=>'listing','module_name'=>'Version Log','module_name_id'=>'55'],
            ['name' => 'documentVersionlogList_formname_pdf_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Form Name','sub_module_name'=>'listing','module_name'=>'Version Log','module_name_id'=>'55'],
            ['name' => 'documentVersionlogList_username_pdf_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'User Name','sub_module_name'=>'listing','module_name'=>'Version Log','module_name_id'=>'55'],
            ['name' => 'documentVersionlogList_date_pdf_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Date','sub_module_name'=>'listing','module_name'=>'Version Log','module_name_id'=>'55'],

            //create in versionlog
            ['name' => 'documentVersionlogList_checkbox_create','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'create','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Version Log','module_name_id'=>'55'],
            ['name' => 'documentVersionlogList_versiontype_create','group'=>'Document Master', 'guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Version Type','sub_module_name'=>'listing','module_name'=>'Version Log','module_name_id'=>'55'],
            ['name' => 'documentVersionlogList_formname_create', 'group'=>'Document Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Form Name','sub_module_name'=>'listing','module_name'=>'Version Log','module_name_id'=>'55'],
            ['name' => 'documentVersionlogList_username_create', 'group'=>'Document Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'User Name','sub_module_name'=>'listing','module_name'=>'Version Log','module_name_id'=>'55'],
            ['name' => 'documentVersionlogList_date_create', 'group'=>'Document Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Date','sub_module_name'=>'listing','module_name'=>'Version Log','module_name_id'=>'55'],

            //edit in versionlog
            ['name' => 'documentVersionlogList_checkbox_edit','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'edit','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Version Log','module_name_id'=>'55'],
            ['name' => 'documentVersionlogList_versiontype_edit','group'=>'Document Master', 'guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Version Type','sub_module_name'=>'listing','module_name'=>'Version Log','module_name_id'=>'55'],
            ['name' => 'documentVersionlogList_formname_edit', 'group'=>'Document Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Form Name','sub_module_name'=>'listing','module_name'=>'Version Log','module_name_id'=>'55'],
            ['name' => 'documentVersionlogList_username_edit', 'group'=>'Document Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'User Name','sub_module_name'=>'listing','module_name'=>'Version Log','module_name_id'=>'55'],
            ['name' => 'documentVersionlogList_date_edit', 'group'=>'Document Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Date','sub_module_name'=>'listing','module_name'=>'Version Log','module_name_id'=>'55'],

            ////versionlog detail

            ['name' => 'versionLogDetail_form_view','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'View From','type'=>'form_view','sub_module_name'=>'Form','module_name'=>'Version Log','module_name_id'=>'55'],
            ['name' => 'versionLogDetail_form_name','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'Form Name','type'=>'form_view','sub_module_name'=>'Form','module_name'=>'Version Log','module_name_id'=>'55'],
            ['name' => 'versionLogDetail_form_header','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'Form Header','type'=>'form_view','sub_module_name'=>'Form','module_name'=>'Version Log','module_name_id'=>'55'],
            ['name' => 'versionLogDetail_form_footer', 'group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'Form Footer','type'=>'form_view','sub_module_name'=>'Form','module_name'=>'Version Log','module_name_id'=>'55'],
            ['name' => 'versionLogDetail_form_formwidth','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'Form Width','type'=>'form_view','sub_module_name'=>'Form','module_name'=>'Version Log','module_name_id'=>'55'],
            ['name' => 'versionLogDetail_form_border','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'Form Border','type'=>'form_view','sub_module_name'=>'Form','module_name'=>'Version Log','module_name_id'=>'55'],

            //form view column
            ['name' => 'versionLogDetail_question_view','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'View Question','type'=>'question_view','sub_module_name'=>'Question','module_name'=>'Version Log','module_name_id'=>'55'],
            ['name' => 'versionLogDetail_question','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'Question','type'=>'question_view','sub_module_name'=>'Question','module_name'=>'Version Log','module_name_id'=>'55'],
            ['name' => 'versionLogDetail_order','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'Order','type'=>'question_view','sub_module_name'=>'Question','module_name'=>'Version Log','module_name_id'=>'55'],
            ['name' => 'versionLogDetail_answertype','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'Answer Type','type'=>'question_view','sub_module_name'=>'Question','module_name'=>'Version Log','module_name_id'=>'55'],
            ['name' => 'versionLogDetail_layout','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'Layout','type'=>'question_view','sub_module_name'=>'Question','module_name'=>'Version Log','module_name_id'=>'55'],
            ['name' => 'versionLogDetail_answer','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'Answer','type'=>'question_view','sub_module_name'=>'Question','module_name'=>'Version Log','module_name_id'=>'55'],

        ];

        foreach ($permission_array as $key => $permission) {
            $admin_role = Role::find(1);
            $getpermission=Permission::where('name',$permission['name'])->first();
            if (empty($getpermission)){
                $permission=Permission::create($permission);
                $admin_role->givePermissionTo($permission['name']);
            }else{
                $permission=Permission::where('name',$getpermission->name)->update($permission);
                $admin_role->givePermissionTo($getpermission->name);
            }
        }
    }
}
