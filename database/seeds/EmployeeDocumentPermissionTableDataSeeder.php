<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class EmployeeDocumentPermissionTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission_array = [
            // create Contact permissions
            ['name' => 'view_employee_document','group'=>'Employee Master', 'guard_name' => 'web','permission_name'=>'view','type'=>'','sub_module_name'=>'operation','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'view_document_employee_document','group'=>'Employee Master', 'guard_name' => 'web','permission_name'=>'view_document','type'=>'','sub_module_name'=>'operation','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'download_employee_document','group'=>'Employee Master', 'guard_name' => 'web','permission_name'=>'download','type'=>'','sub_module_name'=>'operation','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'create_employee_document','group'=>'Employee Master', 'guard_name' => 'web','permission_name'=>'create','type'=>'','sub_module_name'=>'operation','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'edit_employee_document', 'group'=>'Employee Master', 'guard_name' => 'web','permission_name'=>'edit','type'=>'','sub_module_name'=>'operation','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'delete_employee_document','group'=>'Employee Master', 'guard_name' => 'web','permission_name'=>'delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'selected_delete_employee_document','group'=>'Employee Master', 'guard_name' => 'web','permission_name'=>'Selected Delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'selected_active_button_employee_document','group'=>'Employee Master', 'guard_name' => 'web','permission_name'=>'Selected Active Button','type'=>'','sub_module_name'=>'operation','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'selected_restore_delete_employee_document', 'group'=>'Employee Master','guard_name' => 'web','type'=>'','permission_name'=>'Selected Restore','sub_module_name'=>'operation','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'restore_delete_employee_document','group'=>'Employee Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Restore Delete','sub_module_name'=>'operation','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'hard_delete_employee_document','group'=>'Employee Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Hard Delete','sub_module_name'=>'operation','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'active_employee_document','group'=>'Employee Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Active Document Group','sub_module_name'=>'operation','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'word_employee_document', 'group'=>'Employee Master','guard_name' => 'web','type'=>'','permission_name'=>'Word Export Document Group','sub_module_name'=>'operation','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'csv_employee_document','group'=>'Employee Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Csv Export Document Group','sub_module_name'=>'operation','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'pdf_employee_document', 'group'=>'Employee Master','guard_name' => 'web','type'=>'','permission_name'=>'PDF Export Document Group','sub_module_name'=>'operation','module_name'=>'Document','module_name_id'=>24],

            //listing in Contact
            ['name' => 'employee_document_checkbox','group'=>'Employee Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'employee_document_name','group'=>'Employee Master', 'guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'employee_document_type','group'=>'Employee Master', 'guard_name' => 'web','type'=>'view','enable'=>'0','permission_name'=>'Type','sub_module_name'=>'listing','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'employee_document_issue_date','group'=>'Employee Master', 'guard_name' => 'web','type'=>'view','enable'=>'0','permission_name'=>'Issue Date','sub_module_name'=>'listing','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'employee_document_expiry_date','group'=>'Employee Master', 'guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Expiry Date','sub_module_name'=>'listing','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'employee_document_file','group'=>'Employee Master', 'guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'File','sub_module_name'=>'listing','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'employee_document_notes','group'=>'Employee Master', 'guard_name' => 'web','type'=>'view','enable'=>'0','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Document','module_name_id'=>24],
            //create in Contact
            ['name' => 'employee_document_checkbox_create','group'=>'Employee Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'create','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'employee_document_name_create','group'=>'Employee Master', 'guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'employee_document_type_create','group'=>'Employee Master', 'guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Type','sub_module_name'=>'listing','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'employee_document_issue_date_create','group'=>'Employee Master', 'guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Issue Date','sub_module_name'=>'listing','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'employee_document_expiry_date_create','group'=>'Employee Master', 'guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Expiry Date','sub_module_name'=>'listing','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'employee_document_file_create','group'=>'Employee Master', 'guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'File','sub_module_name'=>'listing','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'employee_document_notes_create','group'=>'Employee Master', 'guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Document','module_name_id'=>24],

            //word in Contact
            ['name' => 'employee_document_checkbox_word_export','group'=>'Employee Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'word_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'employee_document_name_word_export','group'=>'Employee Master', 'guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'employee_document_type_word_export','group'=>'Employee Master', 'guard_name' => 'web','type'=>'word_export','enable'=>'0','permission_name'=>'Type','sub_module_name'=>'listing','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'employee_document_issue_date_word_export','group'=>'Employee Master', 'guard_name' => 'web','type'=>'word_export','enable'=>'0','permission_name'=>'Issue Date','sub_module_name'=>'listing','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'employee_document_expiry_date_word_export','group'=>'Employee Master', 'guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Expiry Date','sub_module_name'=>'listing','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'employee_document_file_word_export','group'=>'Employee Master', 'guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'File','sub_module_name'=>'listing','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'employee_document_notes_word_export','group'=>'Employee Master', 'guard_name' => 'web','type'=>'word_export','enable'=>'0','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Document','module_name_id'=>24],

            //excel in Contact
            ['name' => 'employee_document_checkbox_excel_export','group'=>'Employee Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'excel_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'employee_document_name_excel_export','group'=>'Employee Master', 'guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'employee_document_type_excel_export','group'=>'Employee Master', 'guard_name' => 'web','type'=>'excel_export','enable'=>'0','permission_name'=>'Type','sub_module_name'=>'listing','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'employee_document_issue_date_excel_export','group'=>'Employee Master', 'guard_name' => 'web','type'=>'excel_export','enable'=>'0','permission_name'=>'Issue Date','sub_module_name'=>'listing','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'employee_document_expiry_date_excel_export','group'=>'Employee Master', 'guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Expiry Date','sub_module_name'=>'listing','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'employee_document_file_excel_export','group'=>'Employee Master', 'guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'File','sub_module_name'=>'listing','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'employee_document_notes_excel_export','group'=>'Employee Master', 'guard_name' => 'web','type'=>'excel_export','enable'=>'0','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Document','module_name_id'=>24],

            //pdf in Contact
            ['name' => 'employee_document_checkbox_pdf_export','group'=>'Employee Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'pdf_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'employee_document_name_pdf_export','group'=>'Employee Master', 'guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'employee_document_type_pdf_export','group'=>'Employee Master', 'guard_name' => 'web','type'=>'pdf_export','enable'=>'0','permission_name'=>'Type','sub_module_name'=>'listing','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'employee_document_issue_date_pdf_export','group'=>'Employee Master', 'guard_name' => 'web','type'=>'pdf_export','enable'=>'0','permission_name'=>'Issue Date','sub_module_name'=>'listing','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'employee_document_expiry_date_pdf_export','group'=>'Employee Master', 'guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Expiry Date','sub_module_name'=>'listing','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'employee_document_file_pdf_export','group'=>'Employee Master', 'guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'File','sub_module_name'=>'listing','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'employee_document_notes_pdf_export','group'=>'Employee Master', 'guard_name' => 'web','type'=>'pdf_export','enable'=>'0','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Document','module_name_id'=>24],
            //edit in Contact
            ['name' => 'employee_document_checkbox_edit','group'=>'Employee Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'edit','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'employee_document_name_edit','group'=>'Employee Master', 'guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Document','module_name_id'=>24],
             ['name' => 'employee_document_type_edit','group'=>'Employee Master', 'guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Type','sub_module_name'=>'listing','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'employee_document_issue_date_edit','group'=>'Employee Master', 'guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Issue Date','sub_module_name'=>'listing','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'employee_document_expiry_date_edit','group'=>'Employee Master', 'guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Expiry Date','sub_module_name'=>'listing','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'employee_document_file_edit','group'=>'Employee Master', 'guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'File','sub_module_name'=>'listing','module_name'=>'Document','module_name_id'=>24],
            ['name' => 'employee_document_notes_edit','group'=>'Employee Master', 'guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Document','module_name_id'=>24],
        ];

        foreach ($permission_array as $key => $permission) {
            $admin_role = Role::find(1);
            $getpermission=Permission::where('name',$permission['name'])->first();
            if (empty($getpermission)){
                $permission=Permission::create($permission);
                $admin_role->givePermissionTo($permission['name']);
            }else{
                $permission=Permission::where('name',$getpermission->name)->update($permission);
                $admin_role->givePermissionTo($getpermission->name);
            }
        }
    }
}
