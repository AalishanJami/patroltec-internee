<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class DocumentScoringPermisionTableDataSeeder extends Seeder
{
    public function run()
    {
        $permission_array = [
            // create Scoring permissions
            ['name' => 'view_scoring','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'view','type'=>'','sub_module_name'=>'operation','module_name'=>'Scoring'],
            ['name' => 'create_scoring','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'create','type'=>'','sub_module_name'=>'operation','module_name'=>'Scoring'],
            ['name' => 'edit_scoring', 'group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'edit','type'=>'','sub_module_name'=>'operation','module_name'=>'Scoring'],
            ['name' => 'delete_scoring','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Scoring'],
            ['name' => 'selected_delete_scoring','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'Selected Delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Scoring'],
            ['name' => 'selected_restore_delete_scoring', 'group'=>'Document Master','guard_name' => 'web','type'=>'','permission_name'=>'Selected Restore','sub_module_name'=>'operation','module_name'=>'Scoring'],
            ['name' => 'selected_active_scoring', 'group'=>'Document Master','guard_name' => 'web','type'=>'','permission_name'=>'Selected Active Multiple','sub_module_name'=>'operation','module_name'=>'Scoring'],
            ['name' => 'restore_delete_scoring','group'=>'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Restore','sub_module_name'=>'operation','module_name'=>'Scoring'],
            ['name' => 'hard_delete_scoring','group'=>'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Hard Delete','sub_module_name'=>'operation','module_name'=>'Scoring'],
            ['name' => 'active_scoring','group'=>'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Active Scoring','sub_module_name'=>'operation','module_name'=>'Scoring'],
            ['name' => 'word_scoring', 'group'=>'Document Master','guard_name' => 'web','type'=>'','permission_name'=>'Word Export Scoring','sub_module_name'=>'operation','module_name'=>'Scoring'],
            ['name' => 'csv_scoring','group'=>'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Csv Export Scoring','sub_module_name'=>'operation','module_name'=>'Scoring'],
            ['name' => 'pdf_scoring', 'group'=>'Document Master','guard_name' => 'web','type'=>'','permission_name'=>'PDF Export Scoring','sub_module_name'=>'operation','module_name'=>'Scoring'],

            //listing in Scoring
            ['name' => 'scoring_checkbox','group'=>'Document Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Scoring'],
            ['name' => 'scoring_location', 'group'=>'Document Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'location','sub_module_name'=>'listing','module_name'=>'Scoring'],
            ['name' => 'scoring_score', 'group'=>'Document Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'score','sub_module_name'=>'listing','module_name'=>'Scoring'],
            ['name' => 'scoring_max_visit','group'=>'Document Master', 'guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'max_visit','sub_module_name'=>'listing','module_name'=>'Scoring'],
            ['name' => 'scoring_dashbroad', 'group'=>'Document Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'dashbroad','sub_module_name'=>'listing','module_name'=>'Scoring'],
            ['name' => 'scoring_notes', 'group'=>'Document Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'notes','sub_module_name'=>'listing','module_name'=>'Scoring'],

            //word in Scoring
            ['name' => 'scoring_checkbox_word_export','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'word_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Scoring'],
            ['name' => 'scoring_location_word_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'location','sub_module_name'=>'listing','module_name'=>'Scoring'],
            ['name' => 'scoring_score_word_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'score','sub_module_name'=>'listing','module_name'=>'Scoring'],
            ['name' => 'scoring_max_visit_word_export','group'=>'Document Master', 'guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'max_visit','sub_module_name'=>'listing','module_name'=>'Scoring'],
            ['name' => 'scoring_dashbroad_word_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'dashbroad','sub_module_name'=>'listing','module_name'=>'Scoring'],
            ['name' => 'scoring_notes_word_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'notes','sub_module_name'=>'listing','module_name'=>'Scoring'],

            //excel in Scoring
            ['name' => 'scoring_checkbox_excel_export','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'excel_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Scoring'],
            ['name' => 'scoring_location_excel_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'location','sub_module_name'=>'listing','module_name'=>'Scoring'],
            ['name' => 'scoring_score_excel_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'score','sub_module_name'=>'listing','module_name'=>'Scoring'],
            ['name' => 'scoring_max_visit_excel_export','group'=>'Document Master', 'guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'max_visit','sub_module_name'=>'listing','module_name'=>'Scoring'],
            ['name' => 'scoring_dashbroad_excel_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'dashbroad','sub_module_name'=>'listing','module_name'=>'Scoring'],
            ['name' => 'scoring_notes_excel_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'notes','sub_module_name'=>'listing','module_name'=>'Scoring'],

            //pdf in Scoring
            ['name' => 'scoring_checkbox_pdf_export','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'pdf_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Scoring'],
            ['name' => 'scoring_location_pdf_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'location','sub_module_name'=>'listing','module_name'=>'Scoring'],
            ['name' => 'scoring_score_pdf_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'score','sub_module_name'=>'listing','module_name'=>'Scoring'],
            ['name' => 'scoring_max_visit_pdf_export','group'=>'Document Master', 'guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'max_visit','sub_module_name'=>'listing','module_name'=>'Scoring'],
            ['name' => 'scoring_dashbroad_pdf_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'dashbroad','sub_module_name'=>'listing','module_name'=>'Scoring'],
            ['name' => 'scoring_notes_pdf_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'notes','sub_module_name'=>'listing','module_name'=>'Scoring'],

            //create in Scoring
            ['name' => 'scoring_checkbox_create','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'create','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Scoring'],
            ['name' => 'scoring_location_create', 'group'=>'Document Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'location','sub_module_name'=>'listing','module_name'=>'Scoring'],
            ['name' => 'scoring_score_create', 'group'=>'Document Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'score','sub_module_name'=>'listing','module_name'=>'Scoring'],
            ['name' => 'scoring_max_visit_create','group'=>'Document Master', 'guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'max_visit','sub_module_name'=>'listing','module_name'=>'Scoring'],
            ['name' => 'scoring_dashbroad_create', 'group'=>'Document Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'dashbroad','sub_module_name'=>'listing','module_name'=>'Scoring'],
            ['name' => 'scoring_notes_create', 'group'=>'Document Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'notes','sub_module_name'=>'listing','module_name'=>'Scoring'],

            //edit in Scoring
            ['name' => 'scoring_checkbox_edit','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'edit','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Scoring'],
            ['name' => 'scoring_location_edit', 'group'=>'Document Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'location','sub_module_name'=>'listing','module_name'=>'Scoring'],
            ['name' => 'scoring_score_edit', 'group'=>'Document Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'score','sub_module_name'=>'listing','module_name'=>'Scoring'],
            ['name' => 'scoring_max_visit_edit','group'=>'Document Master', 'guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'max_visit','sub_module_name'=>'listing','module_name'=>'Scoring'],
            ['name' => 'scoring_dashbroad_edit', 'group'=>'Document Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'dashbroad','sub_module_name'=>'listing','module_name'=>'Scoring'],
            ['name' => 'scoring_notes_edit', 'group'=>'Document Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'notes','sub_module_name'=>'listing','module_name'=>'Scoring'],
        ];
        foreach ($permission_array as $key => $permission) {
            $admin_role = Role::find(1);
            $getpermission=Permission::where('name',$permission['name'])->first();
            if (empty($getpermission)){
                $permission=Permission::create($permission);
                $admin_role->givePermissionTo($permission['name']);
            }else{
                $permission=Permission::where('name',$getpermission->name)->update($permission);
                $admin_role->givePermissionTo($getpermission->name);
            }
        }
    }
}
