<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
class EmployeeLocationGroupPermissionTableDataSeeder extends Seeder
{
    public function run()
    {
        $permission_array = [
            // create Contact permissions
            ['name' => 'view_employeeLocationGroupPermision','group'=>'Employee Master', 'guard_name' => 'web','permission_name'=>'view','type'=>'','sub_module_name'=>'operation','module_name'=>'Location Group Permission'],
            //listing in Contact
            ['name' => 'employeeLocationGroupPermision_checkbox','group'=>'Employee Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'edit','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Location Group Permission'],
            ['name' => 'employeeLocationGroupPermision_name','group'=>'Employee Master', 'guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Location Group Permission'],
        ];

        foreach ($permission_array as $key => $permission) {
            $admin_role = Role::find(1);
            $getpermission=Permission::where('name',$permission['name'])->first();
            if (empty($getpermission)){
                $permission=Permission::create($permission);
                $admin_role->givePermissionTo($permission['name']);
            }else{
                $permission=Permission::where('name',$getpermission->name)->update($permission);
                $admin_role->givePermissionTo($getpermission->name);
            }
        }
    }
}
