<?php

use Illuminate\Database\Seeder;

class RoleHasPermissionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('role_has_permissions')->delete();
        
        \DB::table('role_has_permissions')->insert(array (
            0 => 
            array (
                'permission_id' => 62,
                'role_id' => 1,
            ),
            1 => 
            array (
                'permission_id' => 63,
                'role_id' => 1,
            ),
            2 => 
            array (
                'permission_id' => 64,
                'role_id' => 1,
            ),
            3 => 
            array (
                'permission_id' => 65,
                'role_id' => 1,
            ),
            4 => 
            array (
                'permission_id' => 66,
                'role_id' => 1,
            ),
            5 => 
            array (
                'permission_id' => 67,
                'role_id' => 1,
            ),
            6 => 
            array (
                'permission_id' => 68,
                'role_id' => 1,
            ),
            7 => 
            array (
                'permission_id' => 69,
                'role_id' => 1,
            ),
            8 => 
            array (
                'permission_id' => 70,
                'role_id' => 1,
            ),
            9 => 
            array (
                'permission_id' => 71,
                'role_id' => 1,
            ),
            10 => 
            array (
                'permission_id' => 72,
                'role_id' => 1,
            ),
            11 => 
            array (
                'permission_id' => 73,
                'role_id' => 1,
            ),
            12 => 
            array (
                'permission_id' => 74,
                'role_id' => 1,
            ),
            13 => 
            array (
                'permission_id' => 75,
                'role_id' => 1,
            ),
            14 => 
            array (
                'permission_id' => 76,
                'role_id' => 1,
            ),
            15 => 
            array (
                'permission_id' => 80,
                'role_id' => 1,
            ),
            16 => 
            array (
                'permission_id' => 81,
                'role_id' => 1,
            ),
            17 => 
            array (
                'permission_id' => 82,
                'role_id' => 1,
            ),
            18 => 
            array (
                'permission_id' => 83,
                'role_id' => 1,
            ),
            19 => 
            array (
                'permission_id' => 84,
                'role_id' => 1,
            ),
            20 => 
            array (
                'permission_id' => 85,
                'role_id' => 1,
            ),
            21 => 
            array (
                'permission_id' => 86,
                'role_id' => 1,
            ),
            22 => 
            array (
                'permission_id' => 87,
                'role_id' => 1,
            ),
            23 => 
            array (
                'permission_id' => 88,
                'role_id' => 1,
            ),
            24 => 
            array (
                'permission_id' => 89,
                'role_id' => 1,
            ),
            25 => 
            array (
                'permission_id' => 90,
                'role_id' => 1,
            ),
            26 => 
            array (
                'permission_id' => 91,
                'role_id' => 1,
            ),
            27 => 
            array (
                'permission_id' => 92,
                'role_id' => 1,
            ),
            28 => 
            array (
                'permission_id' => 93,
                'role_id' => 1,
            ),
            29 => 
            array (
                'permission_id' => 95,
                'role_id' => 1,
            ),
            30 => 
            array (
                'permission_id' => 96,
                'role_id' => 1,
            ),
            31 => 
            array (
                'permission_id' => 97,
                'role_id' => 1,
            ),
            32 => 
            array (
                'permission_id' => 98,
                'role_id' => 1,
            ),
            33 => 
            array (
                'permission_id' => 99,
                'role_id' => 1,
            ),
            34 => 
            array (
                'permission_id' => 100,
                'role_id' => 1,
            ),
            35 => 
            array (
                'permission_id' => 101,
                'role_id' => 1,
            ),
            36 => 
            array (
                'permission_id' => 102,
                'role_id' => 1,
            ),
            37 => 
            array (
                'permission_id' => 103,
                'role_id' => 1,
            ),
            38 => 
            array (
                'permission_id' => 104,
                'role_id' => 1,
            ),
            39 => 
            array (
                'permission_id' => 105,
                'role_id' => 1,
            ),
            40 => 
            array (
                'permission_id' => 106,
                'role_id' => 1,
            ),
            41 => 
            array (
                'permission_id' => 107,
                'role_id' => 1,
            ),
            42 => 
            array (
                'permission_id' => 108,
                'role_id' => 1,
            ),
            43 => 
            array (
                'permission_id' => 109,
                'role_id' => 1,
            ),
            44 => 
            array (
                'permission_id' => 110,
                'role_id' => 1,
            ),
            45 => 
            array (
                'permission_id' => 111,
                'role_id' => 1,
            ),
            46 => 
            array (
                'permission_id' => 112,
                'role_id' => 1,
            ),
            47 => 
            array (
                'permission_id' => 113,
                'role_id' => 1,
            ),
            48 => 
            array (
                'permission_id' => 115,
                'role_id' => 1,
            ),
            49 => 
            array (
                'permission_id' => 116,
                'role_id' => 1,
            ),
            50 => 
            array (
                'permission_id' => 117,
                'role_id' => 1,
            ),
            51 => 
            array (
                'permission_id' => 118,
                'role_id' => 1,
            ),
            52 => 
            array (
                'permission_id' => 119,
                'role_id' => 1,
            ),
            53 => 
            array (
                'permission_id' => 120,
                'role_id' => 1,
            ),
            54 => 
            array (
                'permission_id' => 121,
                'role_id' => 1,
            ),
            55 => 
            array (
                'permission_id' => 122,
                'role_id' => 1,
            ),
            56 => 
            array (
                'permission_id' => 123,
                'role_id' => 1,
            ),
            57 => 
            array (
                'permission_id' => 125,
                'role_id' => 1,
            ),
            58 => 
            array (
                'permission_id' => 126,
                'role_id' => 1,
            ),
            59 => 
            array (
                'permission_id' => 127,
                'role_id' => 1,
            ),
            60 => 
            array (
                'permission_id' => 128,
                'role_id' => 1,
            ),
            61 => 
            array (
                'permission_id' => 129,
                'role_id' => 1,
            ),
            62 => 
            array (
                'permission_id' => 130,
                'role_id' => 1,
            ),
            63 => 
            array (
                'permission_id' => 131,
                'role_id' => 1,
            ),
            64 => 
            array (
                'permission_id' => 132,
                'role_id' => 1,
            ),
            65 => 
            array (
                'permission_id' => 133,
                'role_id' => 1,
            ),
            66 => 
            array (
                'permission_id' => 135,
                'role_id' => 1,
            ),
            67 => 
            array (
                'permission_id' => 136,
                'role_id' => 1,
            ),
            68 => 
            array (
                'permission_id' => 137,
                'role_id' => 1,
            ),
            69 => 
            array (
                'permission_id' => 138,
                'role_id' => 1,
            ),
            70 => 
            array (
                'permission_id' => 139,
                'role_id' => 1,
            ),
            71 => 
            array (
                'permission_id' => 140,
                'role_id' => 1,
            ),
            72 => 
            array (
                'permission_id' => 141,
                'role_id' => 1,
            ),
            73 => 
            array (
                'permission_id' => 142,
                'role_id' => 1,
            ),
            74 => 
            array (
                'permission_id' => 143,
                'role_id' => 1,
            ),
            75 => 
            array (
                'permission_id' => 145,
                'role_id' => 1,
            ),
            76 => 
            array (
                'permission_id' => 146,
                'role_id' => 1,
            ),
            77 => 
            array (
                'permission_id' => 147,
                'role_id' => 1,
            ),
            78 => 
            array (
                'permission_id' => 148,
                'role_id' => 1,
            ),
            79 => 
            array (
                'permission_id' => 149,
                'role_id' => 1,
            ),
            80 => 
            array (
                'permission_id' => 150,
                'role_id' => 1,
            ),
            81 => 
            array (
                'permission_id' => 151,
                'role_id' => 1,
            ),
            82 => 
            array (
                'permission_id' => 152,
                'role_id' => 1,
            ),
            83 => 
            array (
                'permission_id' => 643,
                'role_id' => 1,
            ),
            84 => 
            array (
                'permission_id' => 644,
                'role_id' => 1,
            ),
            85 => 
            array (
                'permission_id' => 645,
                'role_id' => 1,
            ),
            86 => 
            array (
                'permission_id' => 646,
                'role_id' => 1,
            ),
            87 => 
            array (
                'permission_id' => 647,
                'role_id' => 1,
            ),
            88 => 
            array (
                'permission_id' => 648,
                'role_id' => 1,
            ),
            89 => 
            array (
                'permission_id' => 649,
                'role_id' => 1,
            ),
            90 => 
            array (
                'permission_id' => 650,
                'role_id' => 1,
            ),
            91 => 
            array (
                'permission_id' => 651,
                'role_id' => 1,
            ),
            92 => 
            array (
                'permission_id' => 652,
                'role_id' => 1,
            ),
            93 => 
            array (
                'permission_id' => 653,
                'role_id' => 1,
            ),
            94 => 
            array (
                'permission_id' => 654,
                'role_id' => 1,
            ),
            95 => 
            array (
                'permission_id' => 655,
                'role_id' => 1,
            ),
            96 => 
            array (
                'permission_id' => 656,
                'role_id' => 1,
            ),
            97 => 
            array (
                'permission_id' => 657,
                'role_id' => 1,
            ),
            98 => 
            array (
                'permission_id' => 658,
                'role_id' => 1,
            ),
            99 => 
            array (
                'permission_id' => 659,
                'role_id' => 1,
            ),
            100 => 
            array (
                'permission_id' => 660,
                'role_id' => 1,
            ),
            101 => 
            array (
                'permission_id' => 661,
                'role_id' => 1,
            ),
            102 => 
            array (
                'permission_id' => 662,
                'role_id' => 1,
            ),
            103 => 
            array (
                'permission_id' => 663,
                'role_id' => 1,
            ),
            104 => 
            array (
                'permission_id' => 664,
                'role_id' => 1,
            ),
            105 => 
            array (
                'permission_id' => 665,
                'role_id' => 1,
            ),
            106 => 
            array (
                'permission_id' => 666,
                'role_id' => 1,
            ),
            107 => 
            array (
                'permission_id' => 667,
                'role_id' => 1,
            ),
            108 => 
            array (
                'permission_id' => 668,
                'role_id' => 1,
            ),
            109 => 
            array (
                'permission_id' => 669,
                'role_id' => 1,
            ),
            110 => 
            array (
                'permission_id' => 670,
                'role_id' => 1,
            ),
            111 => 
            array (
                'permission_id' => 671,
                'role_id' => 1,
            ),
            112 => 
            array (
                'permission_id' => 672,
                'role_id' => 1,
            ),
            113 => 
            array (
                'permission_id' => 673,
                'role_id' => 1,
            ),
            114 => 
            array (
                'permission_id' => 674,
                'role_id' => 1,
            ),
            115 => 
            array (
                'permission_id' => 675,
                'role_id' => 1,
            ),
            116 => 
            array (
                'permission_id' => 676,
                'role_id' => 1,
            ),
            117 => 
            array (
                'permission_id' => 677,
                'role_id' => 1,
            ),
            118 => 
            array (
                'permission_id' => 678,
                'role_id' => 1,
            ),
            119 => 
            array (
                'permission_id' => 679,
                'role_id' => 1,
            ),
            120 => 
            array (
                'permission_id' => 680,
                'role_id' => 1,
            ),
            121 => 
            array (
                'permission_id' => 681,
                'role_id' => 1,
            ),
            122 => 
            array (
                'permission_id' => 682,
                'role_id' => 1,
            ),
            123 => 
            array (
                'permission_id' => 683,
                'role_id' => 1,
            ),
            124 => 
            array (
                'permission_id' => 684,
                'role_id' => 1,
            ),
            125 => 
            array (
                'permission_id' => 685,
                'role_id' => 1,
            ),
            126 => 
            array (
                'permission_id' => 686,
                'role_id' => 1,
            ),
            127 => 
            array (
                'permission_id' => 687,
                'role_id' => 1,
            ),
            128 => 
            array (
                'permission_id' => 688,
                'role_id' => 1,
            ),
            129 => 
            array (
                'permission_id' => 689,
                'role_id' => 1,
            ),
            130 => 
            array (
                'permission_id' => 690,
                'role_id' => 1,
            ),
            131 => 
            array (
                'permission_id' => 691,
                'role_id' => 1,
            ),
            132 => 
            array (
                'permission_id' => 692,
                'role_id' => 1,
            ),
            133 => 
            array (
                'permission_id' => 693,
                'role_id' => 1,
            ),
            134 => 
            array (
                'permission_id' => 694,
                'role_id' => 1,
            ),
            135 => 
            array (
                'permission_id' => 695,
                'role_id' => 1,
            ),
            136 => 
            array (
                'permission_id' => 696,
                'role_id' => 1,
            ),
            137 => 
            array (
                'permission_id' => 697,
                'role_id' => 1,
            ),
            138 => 
            array (
                'permission_id' => 698,
                'role_id' => 1,
            ),
            139 => 
            array (
                'permission_id' => 699,
                'role_id' => 1,
            ),
            140 => 
            array (
                'permission_id' => 700,
                'role_id' => 1,
            ),
            141 => 
            array (
                'permission_id' => 701,
                'role_id' => 1,
            ),
            142 => 
            array (
                'permission_id' => 702,
                'role_id' => 1,
            ),
            143 => 
            array (
                'permission_id' => 703,
                'role_id' => 1,
            ),
            144 => 
            array (
                'permission_id' => 704,
                'role_id' => 1,
            ),
            145 => 
            array (
                'permission_id' => 705,
                'role_id' => 1,
            ),
            146 => 
            array (
                'permission_id' => 706,
                'role_id' => 1,
            ),
            147 => 
            array (
                'permission_id' => 707,
                'role_id' => 1,
            ),
            148 => 
            array (
                'permission_id' => 708,
                'role_id' => 1,
            ),
            149 => 
            array (
                'permission_id' => 709,
                'role_id' => 1,
            ),
            150 => 
            array (
                'permission_id' => 710,
                'role_id' => 1,
            ),
            151 => 
            array (
                'permission_id' => 711,
                'role_id' => 1,
            ),
            152 => 
            array (
                'permission_id' => 712,
                'role_id' => 1,
            ),
            153 => 
            array (
                'permission_id' => 713,
                'role_id' => 1,
            ),
            154 => 
            array (
                'permission_id' => 714,
                'role_id' => 1,
            ),
            155 => 
            array (
                'permission_id' => 715,
                'role_id' => 1,
            ),
            156 => 
            array (
                'permission_id' => 716,
                'role_id' => 1,
            ),
            157 => 
            array (
                'permission_id' => 717,
                'role_id' => 1,
            ),
            158 => 
            array (
                'permission_id' => 718,
                'role_id' => 1,
            ),
            159 => 
            array (
                'permission_id' => 719,
                'role_id' => 1,
            ),
            160 => 
            array (
                'permission_id' => 720,
                'role_id' => 1,
            ),
            161 => 
            array (
                'permission_id' => 721,
                'role_id' => 1,
            ),
            162 => 
            array (
                'permission_id' => 722,
                'role_id' => 1,
            ),
            163 => 
            array (
                'permission_id' => 723,
                'role_id' => 1,
            ),
            164 => 
            array (
                'permission_id' => 724,
                'role_id' => 1,
            ),
            165 => 
            array (
                'permission_id' => 725,
                'role_id' => 1,
            ),
            166 => 
            array (
                'permission_id' => 726,
                'role_id' => 1,
            ),
            167 => 
            array (
                'permission_id' => 727,
                'role_id' => 1,
            ),
            168 => 
            array (
                'permission_id' => 728,
                'role_id' => 1,
            ),
            169 => 
            array (
                'permission_id' => 729,
                'role_id' => 1,
            ),
            170 => 
            array (
                'permission_id' => 730,
                'role_id' => 1,
            ),
            171 => 
            array (
                'permission_id' => 731,
                'role_id' => 1,
            ),
            172 => 
            array (
                'permission_id' => 732,
                'role_id' => 1,
            ),
            173 => 
            array (
                'permission_id' => 733,
                'role_id' => 1,
            ),
            174 => 
            array (
                'permission_id' => 734,
                'role_id' => 1,
            ),
            175 => 
            array (
                'permission_id' => 735,
                'role_id' => 1,
            ),
            176 => 
            array (
                'permission_id' => 736,
                'role_id' => 1,
            ),
            177 => 
            array (
                'permission_id' => 737,
                'role_id' => 1,
            ),
            178 => 
            array (
                'permission_id' => 738,
                'role_id' => 1,
            ),
            179 => 
            array (
                'permission_id' => 739,
                'role_id' => 1,
            ),
            180 => 
            array (
                'permission_id' => 740,
                'role_id' => 1,
            ),
            181 => 
            array (
                'permission_id' => 741,
                'role_id' => 1,
            ),
            182 => 
            array (
                'permission_id' => 742,
                'role_id' => 1,
            ),
            183 => 
            array (
                'permission_id' => 743,
                'role_id' => 1,
            ),
            184 => 
            array (
                'permission_id' => 744,
                'role_id' => 1,
            ),
            185 => 
            array (
                'permission_id' => 745,
                'role_id' => 1,
            ),
            186 => 
            array (
                'permission_id' => 746,
                'role_id' => 1,
            ),
            187 => 
            array (
                'permission_id' => 747,
                'role_id' => 1,
            ),
            188 => 
            array (
                'permission_id' => 748,
                'role_id' => 1,
            ),
            189 => 
            array (
                'permission_id' => 749,
                'role_id' => 1,
            ),
            190 => 
            array (
                'permission_id' => 750,
                'role_id' => 1,
            ),
            191 => 
            array (
                'permission_id' => 751,
                'role_id' => 1,
            ),
            192 => 
            array (
                'permission_id' => 752,
                'role_id' => 1,
            ),
            193 => 
            array (
                'permission_id' => 753,
                'role_id' => 1,
            ),
            194 => 
            array (
                'permission_id' => 754,
                'role_id' => 1,
            ),
            195 => 
            array (
                'permission_id' => 755,
                'role_id' => 1,
            ),
            196 => 
            array (
                'permission_id' => 756,
                'role_id' => 1,
            ),
            197 => 
            array (
                'permission_id' => 757,
                'role_id' => 1,
            ),
            198 => 
            array (
                'permission_id' => 758,
                'role_id' => 1,
            ),
            199 => 
            array (
                'permission_id' => 759,
                'role_id' => 1,
            ),
            200 => 
            array (
                'permission_id' => 760,
                'role_id' => 1,
            ),
            201 => 
            array (
                'permission_id' => 761,
                'role_id' => 1,
            ),
            202 => 
            array (
                'permission_id' => 762,
                'role_id' => 1,
            ),
            203 => 
            array (
                'permission_id' => 763,
                'role_id' => 1,
            ),
            204 => 
            array (
                'permission_id' => 764,
                'role_id' => 1,
            ),
            205 => 
            array (
                'permission_id' => 765,
                'role_id' => 1,
            ),
            206 => 
            array (
                'permission_id' => 766,
                'role_id' => 1,
            ),
            207 => 
            array (
                'permission_id' => 767,
                'role_id' => 1,
            ),
            208 => 
            array (
                'permission_id' => 768,
                'role_id' => 1,
            ),
            209 => 
            array (
                'permission_id' => 769,
                'role_id' => 1,
            ),
            210 => 
            array (
                'permission_id' => 770,
                'role_id' => 1,
            ),
            211 => 
            array (
                'permission_id' => 771,
                'role_id' => 1,
            ),
            212 => 
            array (
                'permission_id' => 772,
                'role_id' => 1,
            ),
            213 => 
            array (
                'permission_id' => 773,
                'role_id' => 1,
            ),
            214 => 
            array (
                'permission_id' => 774,
                'role_id' => 1,
            ),
            215 => 
            array (
                'permission_id' => 777,
                'role_id' => 1,
            ),
            216 => 
            array (
                'permission_id' => 778,
                'role_id' => 1,
            ),
            217 => 
            array (
                'permission_id' => 779,
                'role_id' => 1,
            ),
            218 => 
            array (
                'permission_id' => 780,
                'role_id' => 1,
            ),
            219 => 
            array (
                'permission_id' => 781,
                'role_id' => 1,
            ),
            220 => 
            array (
                'permission_id' => 782,
                'role_id' => 1,
            ),
            221 => 
            array (
                'permission_id' => 783,
                'role_id' => 1,
            ),
            222 => 
            array (
                'permission_id' => 784,
                'role_id' => 1,
            ),
            223 => 
            array (
                'permission_id' => 785,
                'role_id' => 1,
            ),
            224 => 
            array (
                'permission_id' => 786,
                'role_id' => 1,
            ),
            225 => 
            array (
                'permission_id' => 788,
                'role_id' => 1,
            ),
            226 => 
            array (
                'permission_id' => 789,
                'role_id' => 1,
            ),
            227 => 
            array (
                'permission_id' => 790,
                'role_id' => 1,
            ),
            228 => 
            array (
                'permission_id' => 791,
                'role_id' => 1,
            ),
            229 => 
            array (
                'permission_id' => 792,
                'role_id' => 1,
            ),
            230 => 
            array (
                'permission_id' => 793,
                'role_id' => 1,
            ),
            231 => 
            array (
                'permission_id' => 794,
                'role_id' => 1,
            ),
            232 => 
            array (
                'permission_id' => 795,
                'role_id' => 1,
            ),
            233 => 
            array (
                'permission_id' => 796,
                'role_id' => 1,
            ),
            234 => 
            array (
                'permission_id' => 797,
                'role_id' => 1,
            ),
            235 => 
            array (
                'permission_id' => 798,
                'role_id' => 1,
            ),
            236 => 
            array (
                'permission_id' => 799,
                'role_id' => 1,
            ),
            237 => 
            array (
                'permission_id' => 800,
                'role_id' => 1,
            ),
            238 => 
            array (
                'permission_id' => 801,
                'role_id' => 1,
            ),
            239 => 
            array (
                'permission_id' => 802,
                'role_id' => 1,
            ),
            240 => 
            array (
                'permission_id' => 803,
                'role_id' => 1,
            ),
            241 => 
            array (
                'permission_id' => 804,
                'role_id' => 1,
            ),
            242 => 
            array (
                'permission_id' => 805,
                'role_id' => 1,
            ),
            243 => 
            array (
                'permission_id' => 807,
                'role_id' => 1,
            ),
            244 => 
            array (
                'permission_id' => 808,
                'role_id' => 1,
            ),
            245 => 
            array (
                'permission_id' => 809,
                'role_id' => 1,
            ),
            246 => 
            array (
                'permission_id' => 810,
                'role_id' => 1,
            ),
            247 => 
            array (
                'permission_id' => 811,
                'role_id' => 1,
            ),
            248 => 
            array (
                'permission_id' => 812,
                'role_id' => 1,
            ),
            249 => 
            array (
                'permission_id' => 813,
                'role_id' => 1,
            ),
            250 => 
            array (
                'permission_id' => 814,
                'role_id' => 1,
            ),
            251 => 
            array (
                'permission_id' => 815,
                'role_id' => 1,
            ),
            252 => 
            array (
                'permission_id' => 816,
                'role_id' => 1,
            ),
            253 => 
            array (
                'permission_id' => 817,
                'role_id' => 1,
            ),
            254 => 
            array (
                'permission_id' => 818,
                'role_id' => 1,
            ),
            255 => 
            array (
                'permission_id' => 819,
                'role_id' => 1,
            ),
            256 => 
            array (
                'permission_id' => 820,
                'role_id' => 1,
            ),
            257 => 
            array (
                'permission_id' => 821,
                'role_id' => 1,
            ),
            258 => 
            array (
                'permission_id' => 822,
                'role_id' => 1,
            ),
            259 => 
            array (
                'permission_id' => 823,
                'role_id' => 1,
            ),
            260 => 
            array (
                'permission_id' => 824,
                'role_id' => 1,
            ),
            261 => 
            array (
                'permission_id' => 825,
                'role_id' => 1,
            ),
            262 => 
            array (
                'permission_id' => 826,
                'role_id' => 1,
            ),
            263 => 
            array (
                'permission_id' => 827,
                'role_id' => 1,
            ),
            264 => 
            array (
                'permission_id' => 828,
                'role_id' => 1,
            ),
            265 => 
            array (
                'permission_id' => 829,
                'role_id' => 1,
            ),
            266 => 
            array (
                'permission_id' => 830,
                'role_id' => 1,
            ),
            267 => 
            array (
                'permission_id' => 831,
                'role_id' => 1,
            ),
            268 => 
            array (
                'permission_id' => 832,
                'role_id' => 1,
            ),
            269 => 
            array (
                'permission_id' => 833,
                'role_id' => 1,
            ),
            270 => 
            array (
                'permission_id' => 834,
                'role_id' => 1,
            ),
            271 => 
            array (
                'permission_id' => 835,
                'role_id' => 1,
            ),
            272 => 
            array (
                'permission_id' => 836,
                'role_id' => 1,
            ),
            273 => 
            array (
                'permission_id' => 837,
                'role_id' => 1,
            ),
            274 => 
            array (
                'permission_id' => 838,
                'role_id' => 1,
            ),
            275 => 
            array (
                'permission_id' => 839,
                'role_id' => 1,
            ),
            276 => 
            array (
                'permission_id' => 840,
                'role_id' => 1,
            ),
            277 => 
            array (
                'permission_id' => 841,
                'role_id' => 1,
            ),
            278 => 
            array (
                'permission_id' => 842,
                'role_id' => 1,
            ),
            279 => 
            array (
                'permission_id' => 843,
                'role_id' => 1,
            ),
            280 => 
            array (
                'permission_id' => 844,
                'role_id' => 1,
            ),
            281 => 
            array (
                'permission_id' => 845,
                'role_id' => 1,
            ),
            282 => 
            array (
                'permission_id' => 846,
                'role_id' => 1,
            ),
            283 => 
            array (
                'permission_id' => 847,
                'role_id' => 1,
            ),
            284 => 
            array (
                'permission_id' => 848,
                'role_id' => 1,
            ),
            285 => 
            array (
                'permission_id' => 849,
                'role_id' => 1,
            ),
            286 => 
            array (
                'permission_id' => 850,
                'role_id' => 1,
            ),
            287 => 
            array (
                'permission_id' => 851,
                'role_id' => 1,
            ),
            288 => 
            array (
                'permission_id' => 852,
                'role_id' => 1,
            ),
            289 => 
            array (
                'permission_id' => 853,
                'role_id' => 1,
            ),
            290 => 
            array (
                'permission_id' => 854,
                'role_id' => 1,
            ),
            291 => 
            array (
                'permission_id' => 855,
                'role_id' => 1,
            ),
            292 => 
            array (
                'permission_id' => 856,
                'role_id' => 1,
            ),
            293 => 
            array (
                'permission_id' => 857,
                'role_id' => 1,
            ),
            294 => 
            array (
                'permission_id' => 858,
                'role_id' => 1,
            ),
            295 => 
            array (
                'permission_id' => 859,
                'role_id' => 1,
            ),
            296 => 
            array (
                'permission_id' => 860,
                'role_id' => 1,
            ),
            297 => 
            array (
                'permission_id' => 861,
                'role_id' => 1,
            ),
            298 => 
            array (
                'permission_id' => 862,
                'role_id' => 1,
            ),
            299 => 
            array (
                'permission_id' => 863,
                'role_id' => 1,
            ),
            300 => 
            array (
                'permission_id' => 864,
                'role_id' => 1,
            ),
            301 => 
            array (
                'permission_id' => 865,
                'role_id' => 1,
            ),
            302 => 
            array (
                'permission_id' => 866,
                'role_id' => 1,
            ),
            303 => 
            array (
                'permission_id' => 867,
                'role_id' => 1,
            ),
            304 => 
            array (
                'permission_id' => 868,
                'role_id' => 1,
            ),
            305 => 
            array (
                'permission_id' => 869,
                'role_id' => 1,
            ),
            306 => 
            array (
                'permission_id' => 870,
                'role_id' => 1,
            ),
            307 => 
            array (
                'permission_id' => 871,
                'role_id' => 1,
            ),
            308 => 
            array (
                'permission_id' => 873,
                'role_id' => 1,
            ),
            309 => 
            array (
                'permission_id' => 874,
                'role_id' => 1,
            ),
            310 => 
            array (
                'permission_id' => 875,
                'role_id' => 1,
            ),
            311 => 
            array (
                'permission_id' => 876,
                'role_id' => 1,
            ),
            312 => 
            array (
                'permission_id' => 877,
                'role_id' => 1,
            ),
            313 => 
            array (
                'permission_id' => 878,
                'role_id' => 1,
            ),
            314 => 
            array (
                'permission_id' => 879,
                'role_id' => 1,
            ),
            315 => 
            array (
                'permission_id' => 880,
                'role_id' => 1,
            ),
            316 => 
            array (
                'permission_id' => 881,
                'role_id' => 1,
            ),
            317 => 
            array (
                'permission_id' => 882,
                'role_id' => 1,
            ),
            318 => 
            array (
                'permission_id' => 883,
                'role_id' => 1,
            ),
            319 => 
            array (
                'permission_id' => 884,
                'role_id' => 1,
            ),
            320 => 
            array (
                'permission_id' => 885,
                'role_id' => 1,
            ),
            321 => 
            array (
                'permission_id' => 886,
                'role_id' => 1,
            ),
            322 => 
            array (
                'permission_id' => 887,
                'role_id' => 1,
            ),
            323 => 
            array (
                'permission_id' => 888,
                'role_id' => 1,
            ),
            324 => 
            array (
                'permission_id' => 889,
                'role_id' => 1,
            ),
            325 => 
            array (
                'permission_id' => 890,
                'role_id' => 1,
            ),
            326 => 
            array (
                'permission_id' => 891,
                'role_id' => 1,
            ),
            327 => 
            array (
                'permission_id' => 892,
                'role_id' => 1,
            ),
            328 => 
            array (
                'permission_id' => 893,
                'role_id' => 1,
            ),
            329 => 
            array (
                'permission_id' => 894,
                'role_id' => 1,
            ),
            330 => 
            array (
                'permission_id' => 895,
                'role_id' => 1,
            ),
            331 => 
            array (
                'permission_id' => 896,
                'role_id' => 1,
            ),
            332 => 
            array (
                'permission_id' => 897,
                'role_id' => 1,
            ),
            333 => 
            array (
                'permission_id' => 898,
                'role_id' => 1,
            ),
            334 => 
            array (
                'permission_id' => 899,
                'role_id' => 1,
            ),
            335 => 
            array (
                'permission_id' => 900,
                'role_id' => 1,
            ),
            336 => 
            array (
                'permission_id' => 901,
                'role_id' => 1,
            ),
            337 => 
            array (
                'permission_id' => 902,
                'role_id' => 1,
            ),
            338 => 
            array (
                'permission_id' => 903,
                'role_id' => 1,
            ),
            339 => 
            array (
                'permission_id' => 904,
                'role_id' => 1,
            ),
            340 => 
            array (
                'permission_id' => 905,
                'role_id' => 1,
            ),
            341 => 
            array (
                'permission_id' => 906,
                'role_id' => 1,
            ),
            342 => 
            array (
                'permission_id' => 907,
                'role_id' => 1,
            ),
            343 => 
            array (
                'permission_id' => 908,
                'role_id' => 1,
            ),
            344 => 
            array (
                'permission_id' => 909,
                'role_id' => 1,
            ),
            345 => 
            array (
                'permission_id' => 910,
                'role_id' => 1,
            ),
            346 => 
            array (
                'permission_id' => 911,
                'role_id' => 1,
            ),
            347 => 
            array (
                'permission_id' => 912,
                'role_id' => 1,
            ),
            348 => 
            array (
                'permission_id' => 913,
                'role_id' => 1,
            ),
            349 => 
            array (
                'permission_id' => 914,
                'role_id' => 1,
            ),
            350 => 
            array (
                'permission_id' => 915,
                'role_id' => 1,
            ),
            351 => 
            array (
                'permission_id' => 916,
                'role_id' => 1,
            ),
            352 => 
            array (
                'permission_id' => 917,
                'role_id' => 1,
            ),
            353 => 
            array (
                'permission_id' => 918,
                'role_id' => 1,
            ),
            354 => 
            array (
                'permission_id' => 919,
                'role_id' => 1,
            ),
            355 => 
            array (
                'permission_id' => 920,
                'role_id' => 1,
            ),
            356 => 
            array (
                'permission_id' => 921,
                'role_id' => 1,
            ),
            357 => 
            array (
                'permission_id' => 922,
                'role_id' => 1,
            ),
            358 => 
            array (
                'permission_id' => 923,
                'role_id' => 1,
            ),
            359 => 
            array (
                'permission_id' => 924,
                'role_id' => 1,
            ),
            360 => 
            array (
                'permission_id' => 925,
                'role_id' => 1,
            ),
            361 => 
            array (
                'permission_id' => 926,
                'role_id' => 1,
            ),
            362 => 
            array (
                'permission_id' => 927,
                'role_id' => 1,
            ),
            363 => 
            array (
                'permission_id' => 928,
                'role_id' => 1,
            ),
            364 => 
            array (
                'permission_id' => 929,
                'role_id' => 1,
            ),
            365 => 
            array (
                'permission_id' => 930,
                'role_id' => 1,
            ),
            366 => 
            array (
                'permission_id' => 931,
                'role_id' => 1,
            ),
            367 => 
            array (
                'permission_id' => 932,
                'role_id' => 1,
            ),
            368 => 
            array (
                'permission_id' => 933,
                'role_id' => 1,
            ),
            369 => 
            array (
                'permission_id' => 934,
                'role_id' => 1,
            ),
            370 => 
            array (
                'permission_id' => 935,
                'role_id' => 1,
            ),
            371 => 
            array (
                'permission_id' => 936,
                'role_id' => 1,
            ),
            372 => 
            array (
                'permission_id' => 937,
                'role_id' => 1,
            ),
            373 => 
            array (
                'permission_id' => 938,
                'role_id' => 1,
            ),
            374 => 
            array (
                'permission_id' => 939,
                'role_id' => 1,
            ),
            375 => 
            array (
                'permission_id' => 940,
                'role_id' => 1,
            ),
            376 => 
            array (
                'permission_id' => 941,
                'role_id' => 1,
            ),
            377 => 
            array (
                'permission_id' => 942,
                'role_id' => 1,
            ),
            378 => 
            array (
                'permission_id' => 943,
                'role_id' => 1,
            ),
            379 => 
            array (
                'permission_id' => 944,
                'role_id' => 1,
            ),
            380 => 
            array (
                'permission_id' => 945,
                'role_id' => 1,
            ),
            381 => 
            array (
                'permission_id' => 946,
                'role_id' => 1,
            ),
            382 => 
            array (
                'permission_id' => 947,
                'role_id' => 1,
            ),
            383 => 
            array (
                'permission_id' => 948,
                'role_id' => 1,
            ),
            384 => 
            array (
                'permission_id' => 949,
                'role_id' => 1,
            ),
            385 => 
            array (
                'permission_id' => 950,
                'role_id' => 1,
            ),
            386 => 
            array (
                'permission_id' => 951,
                'role_id' => 1,
            ),
            387 => 
            array (
                'permission_id' => 952,
                'role_id' => 1,
            ),
            388 => 
            array (
                'permission_id' => 953,
                'role_id' => 1,
            ),
            389 => 
            array (
                'permission_id' => 954,
                'role_id' => 1,
            ),
            390 => 
            array (
                'permission_id' => 955,
                'role_id' => 1,
            ),
            391 => 
            array (
                'permission_id' => 956,
                'role_id' => 1,
            ),
            392 => 
            array (
                'permission_id' => 957,
                'role_id' => 1,
            ),
            393 => 
            array (
                'permission_id' => 958,
                'role_id' => 1,
            ),
            394 => 
            array (
                'permission_id' => 959,
                'role_id' => 1,
            ),
            395 => 
            array (
                'permission_id' => 960,
                'role_id' => 1,
            ),
            396 => 
            array (
                'permission_id' => 961,
                'role_id' => 1,
            ),
            397 => 
            array (
                'permission_id' => 962,
                'role_id' => 1,
            ),
            398 => 
            array (
                'permission_id' => 963,
                'role_id' => 1,
            ),
            399 => 
            array (
                'permission_id' => 964,
                'role_id' => 1,
            ),
            400 => 
            array (
                'permission_id' => 965,
                'role_id' => 1,
            ),
            401 => 
            array (
                'permission_id' => 966,
                'role_id' => 1,
            ),
            402 => 
            array (
                'permission_id' => 967,
                'role_id' => 1,
            ),
            403 => 
            array (
                'permission_id' => 968,
                'role_id' => 1,
            ),
            404 => 
            array (
                'permission_id' => 969,
                'role_id' => 1,
            ),
            405 => 
            array (
                'permission_id' => 970,
                'role_id' => 1,
            ),
            406 => 
            array (
                'permission_id' => 971,
                'role_id' => 1,
            ),
            407 => 
            array (
                'permission_id' => 972,
                'role_id' => 1,
            ),
            408 => 
            array (
                'permission_id' => 973,
                'role_id' => 1,
            ),
            409 => 
            array (
                'permission_id' => 974,
                'role_id' => 1,
            ),
            410 => 
            array (
                'permission_id' => 975,
                'role_id' => 1,
            ),
            411 => 
            array (
                'permission_id' => 976,
                'role_id' => 1,
            ),
            412 => 
            array (
                'permission_id' => 977,
                'role_id' => 1,
            ),
            413 => 
            array (
                'permission_id' => 978,
                'role_id' => 1,
            ),
            414 => 
            array (
                'permission_id' => 979,
                'role_id' => 1,
            ),
            415 => 
            array (
                'permission_id' => 980,
                'role_id' => 1,
            ),
            416 => 
            array (
                'permission_id' => 981,
                'role_id' => 1,
            ),
            417 => 
            array (
                'permission_id' => 982,
                'role_id' => 1,
            ),
            418 => 
            array (
                'permission_id' => 983,
                'role_id' => 1,
            ),
            419 => 
            array (
                'permission_id' => 984,
                'role_id' => 1,
            ),
            420 => 
            array (
                'permission_id' => 985,
                'role_id' => 1,
            ),
            421 => 
            array (
                'permission_id' => 986,
                'role_id' => 1,
            ),
            422 => 
            array (
                'permission_id' => 987,
                'role_id' => 1,
            ),
            423 => 
            array (
                'permission_id' => 988,
                'role_id' => 1,
            ),
            424 => 
            array (
                'permission_id' => 989,
                'role_id' => 1,
            ),
            425 => 
            array (
                'permission_id' => 990,
                'role_id' => 1,
            ),
            426 => 
            array (
                'permission_id' => 991,
                'role_id' => 1,
            ),
            427 => 
            array (
                'permission_id' => 992,
                'role_id' => 1,
            ),
            428 => 
            array (
                'permission_id' => 993,
                'role_id' => 1,
            ),
            429 => 
            array (
                'permission_id' => 994,
                'role_id' => 1,
            ),
            430 => 
            array (
                'permission_id' => 995,
                'role_id' => 1,
            ),
            431 => 
            array (
                'permission_id' => 996,
                'role_id' => 1,
            ),
            432 => 
            array (
                'permission_id' => 997,
                'role_id' => 1,
            ),
            433 => 
            array (
                'permission_id' => 998,
                'role_id' => 1,
            ),
            434 => 
            array (
                'permission_id' => 999,
                'role_id' => 1,
            ),
            435 => 
            array (
                'permission_id' => 1000,
                'role_id' => 1,
            ),
            436 => 
            array (
                'permission_id' => 1001,
                'role_id' => 1,
            ),
            437 => 
            array (
                'permission_id' => 1002,
                'role_id' => 1,
            ),
            438 => 
            array (
                'permission_id' => 1003,
                'role_id' => 1,
            ),
            439 => 
            array (
                'permission_id' => 1004,
                'role_id' => 1,
            ),
            440 => 
            array (
                'permission_id' => 1005,
                'role_id' => 1,
            ),
            441 => 
            array (
                'permission_id' => 1006,
                'role_id' => 1,
            ),
            442 => 
            array (
                'permission_id' => 1007,
                'role_id' => 1,
            ),
            443 => 
            array (
                'permission_id' => 1008,
                'role_id' => 1,
            ),
            444 => 
            array (
                'permission_id' => 1009,
                'role_id' => 1,
            ),
            445 => 
            array (
                'permission_id' => 1010,
                'role_id' => 1,
            ),
            446 => 
            array (
                'permission_id' => 1011,
                'role_id' => 1,
            ),
            447 => 
            array (
                'permission_id' => 1012,
                'role_id' => 1,
            ),
            448 => 
            array (
                'permission_id' => 1013,
                'role_id' => 1,
            ),
            449 => 
            array (
                'permission_id' => 1014,
                'role_id' => 1,
            ),
            450 => 
            array (
                'permission_id' => 1015,
                'role_id' => 1,
            ),
            451 => 
            array (
                'permission_id' => 1016,
                'role_id' => 1,
            ),
            452 => 
            array (
                'permission_id' => 1017,
                'role_id' => 1,
            ),
            453 => 
            array (
                'permission_id' => 1018,
                'role_id' => 1,
            ),
            454 => 
            array (
                'permission_id' => 1019,
                'role_id' => 1,
            ),
            455 => 
            array (
                'permission_id' => 1020,
                'role_id' => 1,
            ),
            456 => 
            array (
                'permission_id' => 1021,
                'role_id' => 1,
            ),
            457 => 
            array (
                'permission_id' => 1022,
                'role_id' => 1,
            ),
            458 => 
            array (
                'permission_id' => 1023,
                'role_id' => 1,
            ),
            459 => 
            array (
                'permission_id' => 1024,
                'role_id' => 1,
            ),
            460 => 
            array (
                'permission_id' => 1025,
                'role_id' => 1,
            ),
            461 => 
            array (
                'permission_id' => 1026,
                'role_id' => 1,
            ),
            462 => 
            array (
                'permission_id' => 1027,
                'role_id' => 1,
            ),
            463 => 
            array (
                'permission_id' => 1028,
                'role_id' => 1,
            ),
            464 => 
            array (
                'permission_id' => 1029,
                'role_id' => 1,
            ),
            465 => 
            array (
                'permission_id' => 1030,
                'role_id' => 1,
            ),
            466 => 
            array (
                'permission_id' => 1031,
                'role_id' => 1,
            ),
            467 => 
            array (
                'permission_id' => 1032,
                'role_id' => 1,
            ),
            468 => 
            array (
                'permission_id' => 1033,
                'role_id' => 1,
            ),
            469 => 
            array (
                'permission_id' => 1034,
                'role_id' => 1,
            ),
            470 => 
            array (
                'permission_id' => 1035,
                'role_id' => 1,
            ),
            471 => 
            array (
                'permission_id' => 1036,
                'role_id' => 1,
            ),
            472 => 
            array (
                'permission_id' => 1037,
                'role_id' => 1,
            ),
            473 => 
            array (
                'permission_id' => 1038,
                'role_id' => 1,
            ),
            474 => 
            array (
                'permission_id' => 1039,
                'role_id' => 1,
            ),
            475 => 
            array (
                'permission_id' => 1040,
                'role_id' => 1,
            ),
            476 => 
            array (
                'permission_id' => 1041,
                'role_id' => 1,
            ),
            477 => 
            array (
                'permission_id' => 1042,
                'role_id' => 1,
            ),
            478 => 
            array (
                'permission_id' => 1043,
                'role_id' => 1,
            ),
            479 => 
            array (
                'permission_id' => 1044,
                'role_id' => 1,
            ),
            480 => 
            array (
                'permission_id' => 1045,
                'role_id' => 1,
            ),
            481 => 
            array (
                'permission_id' => 1046,
                'role_id' => 1,
            ),
            482 => 
            array (
                'permission_id' => 1047,
                'role_id' => 1,
            ),
            483 => 
            array (
                'permission_id' => 1048,
                'role_id' => 1,
            ),
            484 => 
            array (
                'permission_id' => 1049,
                'role_id' => 1,
            ),
            485 => 
            array (
                'permission_id' => 1050,
                'role_id' => 1,
            ),
            486 => 
            array (
                'permission_id' => 1051,
                'role_id' => 1,
            ),
            487 => 
            array (
                'permission_id' => 1052,
                'role_id' => 1,
            ),
            488 => 
            array (
                'permission_id' => 1053,
                'role_id' => 1,
            ),
            489 => 
            array (
                'permission_id' => 1054,
                'role_id' => 1,
            ),
            490 => 
            array (
                'permission_id' => 1055,
                'role_id' => 1,
            ),
            491 => 
            array (
                'permission_id' => 1056,
                'role_id' => 1,
            ),
            492 => 
            array (
                'permission_id' => 1057,
                'role_id' => 1,
            ),
            493 => 
            array (
                'permission_id' => 1058,
                'role_id' => 1,
            ),
            494 => 
            array (
                'permission_id' => 1059,
                'role_id' => 1,
            ),
            495 => 
            array (
                'permission_id' => 1060,
                'role_id' => 1,
            ),
            496 => 
            array (
                'permission_id' => 1061,
                'role_id' => 1,
            ),
            497 => 
            array (
                'permission_id' => 1062,
                'role_id' => 1,
            ),
            498 => 
            array (
                'permission_id' => 1063,
                'role_id' => 1,
            ),
            499 => 
            array (
                'permission_id' => 1064,
                'role_id' => 1,
            ),
        ));
        \DB::table('role_has_permissions')->insert(array (
            0 => 
            array (
                'permission_id' => 1065,
                'role_id' => 1,
            ),
            1 => 
            array (
                'permission_id' => 1066,
                'role_id' => 1,
            ),
            2 => 
            array (
                'permission_id' => 1067,
                'role_id' => 1,
            ),
            3 => 
            array (
                'permission_id' => 1068,
                'role_id' => 1,
            ),
            4 => 
            array (
                'permission_id' => 1069,
                'role_id' => 1,
            ),
            5 => 
            array (
                'permission_id' => 1070,
                'role_id' => 1,
            ),
            6 => 
            array (
                'permission_id' => 1071,
                'role_id' => 1,
            ),
            7 => 
            array (
                'permission_id' => 1072,
                'role_id' => 1,
            ),
            8 => 
            array (
                'permission_id' => 1073,
                'role_id' => 1,
            ),
            9 => 
            array (
                'permission_id' => 1074,
                'role_id' => 1,
            ),
            10 => 
            array (
                'permission_id' => 1075,
                'role_id' => 1,
            ),
            11 => 
            array (
                'permission_id' => 1076,
                'role_id' => 1,
            ),
            12 => 
            array (
                'permission_id' => 1077,
                'role_id' => 1,
            ),
            13 => 
            array (
                'permission_id' => 1078,
                'role_id' => 1,
            ),
            14 => 
            array (
                'permission_id' => 1079,
                'role_id' => 1,
            ),
            15 => 
            array (
                'permission_id' => 1080,
                'role_id' => 1,
            ),
            16 => 
            array (
                'permission_id' => 1081,
                'role_id' => 1,
            ),
            17 => 
            array (
                'permission_id' => 1082,
                'role_id' => 1,
            ),
            18 => 
            array (
                'permission_id' => 1083,
                'role_id' => 1,
            ),
            19 => 
            array (
                'permission_id' => 1084,
                'role_id' => 1,
            ),
            20 => 
            array (
                'permission_id' => 1085,
                'role_id' => 1,
            ),
            21 => 
            array (
                'permission_id' => 1086,
                'role_id' => 1,
            ),
            22 => 
            array (
                'permission_id' => 1087,
                'role_id' => 1,
            ),
            23 => 
            array (
                'permission_id' => 1088,
                'role_id' => 1,
            ),
            24 => 
            array (
                'permission_id' => 1089,
                'role_id' => 1,
            ),
            25 => 
            array (
                'permission_id' => 1090,
                'role_id' => 1,
            ),
            26 => 
            array (
                'permission_id' => 1091,
                'role_id' => 1,
            ),
            27 => 
            array (
                'permission_id' => 1092,
                'role_id' => 1,
            ),
            28 => 
            array (
                'permission_id' => 1093,
                'role_id' => 1,
            ),
            29 => 
            array (
                'permission_id' => 1094,
                'role_id' => 1,
            ),
            30 => 
            array (
                'permission_id' => 1095,
                'role_id' => 1,
            ),
            31 => 
            array (
                'permission_id' => 1096,
                'role_id' => 1,
            ),
            32 => 
            array (
                'permission_id' => 1097,
                'role_id' => 1,
            ),
            33 => 
            array (
                'permission_id' => 1098,
                'role_id' => 1,
            ),
            34 => 
            array (
                'permission_id' => 1099,
                'role_id' => 1,
            ),
            35 => 
            array (
                'permission_id' => 1100,
                'role_id' => 1,
            ),
            36 => 
            array (
                'permission_id' => 1101,
                'role_id' => 1,
            ),
            37 => 
            array (
                'permission_id' => 1102,
                'role_id' => 1,
            ),
            38 => 
            array (
                'permission_id' => 1103,
                'role_id' => 1,
            ),
            39 => 
            array (
                'permission_id' => 1104,
                'role_id' => 1,
            ),
            40 => 
            array (
                'permission_id' => 1105,
                'role_id' => 1,
            ),
            41 => 
            array (
                'permission_id' => 1106,
                'role_id' => 1,
            ),
            42 => 
            array (
                'permission_id' => 1107,
                'role_id' => 1,
            ),
            43 => 
            array (
                'permission_id' => 1108,
                'role_id' => 1,
            ),
            44 => 
            array (
                'permission_id' => 1109,
                'role_id' => 1,
            ),
            45 => 
            array (
                'permission_id' => 1110,
                'role_id' => 1,
            ),
            46 => 
            array (
                'permission_id' => 1111,
                'role_id' => 1,
            ),
            47 => 
            array (
                'permission_id' => 1112,
                'role_id' => 1,
            ),
            48 => 
            array (
                'permission_id' => 1113,
                'role_id' => 1,
            ),
            49 => 
            array (
                'permission_id' => 1114,
                'role_id' => 1,
            ),
            50 => 
            array (
                'permission_id' => 1115,
                'role_id' => 1,
            ),
            51 => 
            array (
                'permission_id' => 1116,
                'role_id' => 1,
            ),
            52 => 
            array (
                'permission_id' => 1117,
                'role_id' => 1,
            ),
            53 => 
            array (
                'permission_id' => 1118,
                'role_id' => 1,
            ),
            54 => 
            array (
                'permission_id' => 1119,
                'role_id' => 1,
            ),
            55 => 
            array (
                'permission_id' => 1120,
                'role_id' => 1,
            ),
            56 => 
            array (
                'permission_id' => 1121,
                'role_id' => 1,
            ),
            57 => 
            array (
                'permission_id' => 1122,
                'role_id' => 1,
            ),
            58 => 
            array (
                'permission_id' => 1123,
                'role_id' => 1,
            ),
            59 => 
            array (
                'permission_id' => 1124,
                'role_id' => 1,
            ),
            60 => 
            array (
                'permission_id' => 1125,
                'role_id' => 1,
            ),
            61 => 
            array (
                'permission_id' => 1126,
                'role_id' => 1,
            ),
            62 => 
            array (
                'permission_id' => 1127,
                'role_id' => 1,
            ),
            63 => 
            array (
                'permission_id' => 1128,
                'role_id' => 1,
            ),
            64 => 
            array (
                'permission_id' => 1129,
                'role_id' => 1,
            ),
            65 => 
            array (
                'permission_id' => 1130,
                'role_id' => 1,
            ),
            66 => 
            array (
                'permission_id' => 1131,
                'role_id' => 1,
            ),
            67 => 
            array (
                'permission_id' => 1132,
                'role_id' => 1,
            ),
            68 => 
            array (
                'permission_id' => 1133,
                'role_id' => 1,
            ),
            69 => 
            array (
                'permission_id' => 1134,
                'role_id' => 1,
            ),
            70 => 
            array (
                'permission_id' => 1135,
                'role_id' => 1,
            ),
            71 => 
            array (
                'permission_id' => 1136,
                'role_id' => 1,
            ),
            72 => 
            array (
                'permission_id' => 1137,
                'role_id' => 1,
            ),
            73 => 
            array (
                'permission_id' => 1138,
                'role_id' => 1,
            ),
            74 => 
            array (
                'permission_id' => 1139,
                'role_id' => 1,
            ),
            75 => 
            array (
                'permission_id' => 1140,
                'role_id' => 1,
            ),
            76 => 
            array (
                'permission_id' => 1141,
                'role_id' => 1,
            ),
            77 => 
            array (
                'permission_id' => 1142,
                'role_id' => 1,
            ),
            78 => 
            array (
                'permission_id' => 1143,
                'role_id' => 1,
            ),
            79 => 
            array (
                'permission_id' => 1144,
                'role_id' => 1,
            ),
            80 => 
            array (
                'permission_id' => 1145,
                'role_id' => 1,
            ),
            81 => 
            array (
                'permission_id' => 1146,
                'role_id' => 1,
            ),
            82 => 
            array (
                'permission_id' => 1147,
                'role_id' => 1,
            ),
            83 => 
            array (
                'permission_id' => 1148,
                'role_id' => 1,
            ),
            84 => 
            array (
                'permission_id' => 1149,
                'role_id' => 1,
            ),
            85 => 
            array (
                'permission_id' => 1150,
                'role_id' => 1,
            ),
            86 => 
            array (
                'permission_id' => 1151,
                'role_id' => 1,
            ),
            87 => 
            array (
                'permission_id' => 1152,
                'role_id' => 1,
            ),
            88 => 
            array (
                'permission_id' => 1153,
                'role_id' => 1,
            ),
            89 => 
            array (
                'permission_id' => 1154,
                'role_id' => 1,
            ),
            90 => 
            array (
                'permission_id' => 1155,
                'role_id' => 1,
            ),
            91 => 
            array (
                'permission_id' => 1156,
                'role_id' => 1,
            ),
            92 => 
            array (
                'permission_id' => 1157,
                'role_id' => 1,
            ),
            93 => 
            array (
                'permission_id' => 1158,
                'role_id' => 1,
            ),
            94 => 
            array (
                'permission_id' => 1159,
                'role_id' => 1,
            ),
            95 => 
            array (
                'permission_id' => 1160,
                'role_id' => 1,
            ),
            96 => 
            array (
                'permission_id' => 1161,
                'role_id' => 1,
            ),
            97 => 
            array (
                'permission_id' => 1162,
                'role_id' => 1,
            ),
            98 => 
            array (
                'permission_id' => 1163,
                'role_id' => 1,
            ),
            99 => 
            array (
                'permission_id' => 1164,
                'role_id' => 1,
            ),
            100 => 
            array (
                'permission_id' => 1165,
                'role_id' => 1,
            ),
            101 => 
            array (
                'permission_id' => 1166,
                'role_id' => 1,
            ),
            102 => 
            array (
                'permission_id' => 1167,
                'role_id' => 1,
            ),
            103 => 
            array (
                'permission_id' => 1168,
                'role_id' => 1,
            ),
            104 => 
            array (
                'permission_id' => 1169,
                'role_id' => 1,
            ),
            105 => 
            array (
                'permission_id' => 1170,
                'role_id' => 1,
            ),
            106 => 
            array (
                'permission_id' => 1171,
                'role_id' => 1,
            ),
            107 => 
            array (
                'permission_id' => 1172,
                'role_id' => 1,
            ),
            108 => 
            array (
                'permission_id' => 1173,
                'role_id' => 1,
            ),
            109 => 
            array (
                'permission_id' => 1174,
                'role_id' => 1,
            ),
            110 => 
            array (
                'permission_id' => 1175,
                'role_id' => 1,
            ),
            111 => 
            array (
                'permission_id' => 1176,
                'role_id' => 1,
            ),
            112 => 
            array (
                'permission_id' => 1177,
                'role_id' => 1,
            ),
            113 => 
            array (
                'permission_id' => 1178,
                'role_id' => 1,
            ),
            114 => 
            array (
                'permission_id' => 1179,
                'role_id' => 1,
            ),
            115 => 
            array (
                'permission_id' => 1180,
                'role_id' => 1,
            ),
            116 => 
            array (
                'permission_id' => 1181,
                'role_id' => 1,
            ),
            117 => 
            array (
                'permission_id' => 1182,
                'role_id' => 1,
            ),
            118 => 
            array (
                'permission_id' => 1183,
                'role_id' => 1,
            ),
            119 => 
            array (
                'permission_id' => 1184,
                'role_id' => 1,
            ),
            120 => 
            array (
                'permission_id' => 1185,
                'role_id' => 1,
            ),
            121 => 
            array (
                'permission_id' => 1186,
                'role_id' => 1,
            ),
            122 => 
            array (
                'permission_id' => 1187,
                'role_id' => 1,
            ),
            123 => 
            array (
                'permission_id' => 1188,
                'role_id' => 1,
            ),
            124 => 
            array (
                'permission_id' => 1189,
                'role_id' => 1,
            ),
            125 => 
            array (
                'permission_id' => 1190,
                'role_id' => 1,
            ),
            126 => 
            array (
                'permission_id' => 1191,
                'role_id' => 1,
            ),
            127 => 
            array (
                'permission_id' => 1192,
                'role_id' => 1,
            ),
            128 => 
            array (
                'permission_id' => 1193,
                'role_id' => 1,
            ),
            129 => 
            array (
                'permission_id' => 1194,
                'role_id' => 1,
            ),
            130 => 
            array (
                'permission_id' => 1195,
                'role_id' => 1,
            ),
            131 => 
            array (
                'permission_id' => 1196,
                'role_id' => 1,
            ),
            132 => 
            array (
                'permission_id' => 1197,
                'role_id' => 1,
            ),
            133 => 
            array (
                'permission_id' => 1198,
                'role_id' => 1,
            ),
            134 => 
            array (
                'permission_id' => 1199,
                'role_id' => 1,
            ),
            135 => 
            array (
                'permission_id' => 1200,
                'role_id' => 1,
            ),
            136 => 
            array (
                'permission_id' => 1201,
                'role_id' => 1,
            ),
            137 => 
            array (
                'permission_id' => 1202,
                'role_id' => 1,
            ),
            138 => 
            array (
                'permission_id' => 1203,
                'role_id' => 1,
            ),
            139 => 
            array (
                'permission_id' => 1204,
                'role_id' => 1,
            ),
            140 => 
            array (
                'permission_id' => 1205,
                'role_id' => 1,
            ),
            141 => 
            array (
                'permission_id' => 1206,
                'role_id' => 1,
            ),
            142 => 
            array (
                'permission_id' => 1207,
                'role_id' => 1,
            ),
            143 => 
            array (
                'permission_id' => 1208,
                'role_id' => 1,
            ),
            144 => 
            array (
                'permission_id' => 1209,
                'role_id' => 1,
            ),
            145 => 
            array (
                'permission_id' => 1210,
                'role_id' => 1,
            ),
            146 => 
            array (
                'permission_id' => 1211,
                'role_id' => 1,
            ),
            147 => 
            array (
                'permission_id' => 1212,
                'role_id' => 1,
            ),
            148 => 
            array (
                'permission_id' => 1213,
                'role_id' => 1,
            ),
            149 => 
            array (
                'permission_id' => 1214,
                'role_id' => 1,
            ),
            150 => 
            array (
                'permission_id' => 1215,
                'role_id' => 1,
            ),
            151 => 
            array (
                'permission_id' => 1216,
                'role_id' => 1,
            ),
            152 => 
            array (
                'permission_id' => 1217,
                'role_id' => 1,
            ),
            153 => 
            array (
                'permission_id' => 1218,
                'role_id' => 1,
            ),
            154 => 
            array (
                'permission_id' => 1219,
                'role_id' => 1,
            ),
            155 => 
            array (
                'permission_id' => 1220,
                'role_id' => 1,
            ),
            156 => 
            array (
                'permission_id' => 1221,
                'role_id' => 1,
            ),
            157 => 
            array (
                'permission_id' => 1222,
                'role_id' => 1,
            ),
            158 => 
            array (
                'permission_id' => 1223,
                'role_id' => 1,
            ),
            159 => 
            array (
                'permission_id' => 1224,
                'role_id' => 1,
            ),
            160 => 
            array (
                'permission_id' => 1225,
                'role_id' => 1,
            ),
            161 => 
            array (
                'permission_id' => 1226,
                'role_id' => 1,
            ),
            162 => 
            array (
                'permission_id' => 1227,
                'role_id' => 1,
            ),
            163 => 
            array (
                'permission_id' => 1228,
                'role_id' => 1,
            ),
            164 => 
            array (
                'permission_id' => 1229,
                'role_id' => 1,
            ),
            165 => 
            array (
                'permission_id' => 1230,
                'role_id' => 1,
            ),
            166 => 
            array (
                'permission_id' => 1231,
                'role_id' => 1,
            ),
            167 => 
            array (
                'permission_id' => 1232,
                'role_id' => 1,
            ),
            168 => 
            array (
                'permission_id' => 1233,
                'role_id' => 1,
            ),
            169 => 
            array (
                'permission_id' => 1234,
                'role_id' => 1,
            ),
            170 => 
            array (
                'permission_id' => 1235,
                'role_id' => 1,
            ),
            171 => 
            array (
                'permission_id' => 1236,
                'role_id' => 1,
            ),
            172 => 
            array (
                'permission_id' => 1237,
                'role_id' => 1,
            ),
            173 => 
            array (
                'permission_id' => 1238,
                'role_id' => 1,
            ),
            174 => 
            array (
                'permission_id' => 1239,
                'role_id' => 1,
            ),
            175 => 
            array (
                'permission_id' => 1240,
                'role_id' => 1,
            ),
            176 => 
            array (
                'permission_id' => 1241,
                'role_id' => 1,
            ),
            177 => 
            array (
                'permission_id' => 1242,
                'role_id' => 1,
            ),
            178 => 
            array (
                'permission_id' => 1243,
                'role_id' => 1,
            ),
            179 => 
            array (
                'permission_id' => 1244,
                'role_id' => 1,
            ),
            180 => 
            array (
                'permission_id' => 1245,
                'role_id' => 1,
            ),
            181 => 
            array (
                'permission_id' => 1246,
                'role_id' => 1,
            ),
            182 => 
            array (
                'permission_id' => 1247,
                'role_id' => 1,
            ),
            183 => 
            array (
                'permission_id' => 1248,
                'role_id' => 1,
            ),
            184 => 
            array (
                'permission_id' => 1249,
                'role_id' => 1,
            ),
            185 => 
            array (
                'permission_id' => 1250,
                'role_id' => 1,
            ),
            186 => 
            array (
                'permission_id' => 1251,
                'role_id' => 1,
            ),
            187 => 
            array (
                'permission_id' => 1252,
                'role_id' => 1,
            ),
            188 => 
            array (
                'permission_id' => 1253,
                'role_id' => 1,
            ),
            189 => 
            array (
                'permission_id' => 1254,
                'role_id' => 1,
            ),
            190 => 
            array (
                'permission_id' => 1255,
                'role_id' => 1,
            ),
            191 => 
            array (
                'permission_id' => 1256,
                'role_id' => 1,
            ),
            192 => 
            array (
                'permission_id' => 1257,
                'role_id' => 1,
            ),
            193 => 
            array (
                'permission_id' => 1258,
                'role_id' => 1,
            ),
            194 => 
            array (
                'permission_id' => 1259,
                'role_id' => 1,
            ),
            195 => 
            array (
                'permission_id' => 1260,
                'role_id' => 1,
            ),
            196 => 
            array (
                'permission_id' => 1261,
                'role_id' => 1,
            ),
            197 => 
            array (
                'permission_id' => 1262,
                'role_id' => 1,
            ),
            198 => 
            array (
                'permission_id' => 1263,
                'role_id' => 1,
            ),
            199 => 
            array (
                'permission_id' => 1264,
                'role_id' => 1,
            ),
            200 => 
            array (
                'permission_id' => 1265,
                'role_id' => 1,
            ),
            201 => 
            array (
                'permission_id' => 1266,
                'role_id' => 1,
            ),
            202 => 
            array (
                'permission_id' => 1267,
                'role_id' => 1,
            ),
            203 => 
            array (
                'permission_id' => 1268,
                'role_id' => 1,
            ),
            204 => 
            array (
                'permission_id' => 1269,
                'role_id' => 1,
            ),
            205 => 
            array (
                'permission_id' => 1270,
                'role_id' => 1,
            ),
            206 => 
            array (
                'permission_id' => 1271,
                'role_id' => 1,
            ),
            207 => 
            array (
                'permission_id' => 1272,
                'role_id' => 1,
            ),
            208 => 
            array (
                'permission_id' => 1273,
                'role_id' => 1,
            ),
            209 => 
            array (
                'permission_id' => 1274,
                'role_id' => 1,
            ),
            210 => 
            array (
                'permission_id' => 1275,
                'role_id' => 1,
            ),
            211 => 
            array (
                'permission_id' => 1276,
                'role_id' => 1,
            ),
            212 => 
            array (
                'permission_id' => 1277,
                'role_id' => 1,
            ),
            213 => 
            array (
                'permission_id' => 1278,
                'role_id' => 1,
            ),
            214 => 
            array (
                'permission_id' => 1279,
                'role_id' => 1,
            ),
            215 => 
            array (
                'permission_id' => 1280,
                'role_id' => 1,
            ),
            216 => 
            array (
                'permission_id' => 1281,
                'role_id' => 1,
            ),
            217 => 
            array (
                'permission_id' => 1282,
                'role_id' => 1,
            ),
            218 => 
            array (
                'permission_id' => 1283,
                'role_id' => 1,
            ),
            219 => 
            array (
                'permission_id' => 1284,
                'role_id' => 1,
            ),
            220 => 
            array (
                'permission_id' => 1285,
                'role_id' => 1,
            ),
            221 => 
            array (
                'permission_id' => 1286,
                'role_id' => 1,
            ),
            222 => 
            array (
                'permission_id' => 1287,
                'role_id' => 1,
            ),
            223 => 
            array (
                'permission_id' => 1288,
                'role_id' => 1,
            ),
            224 => 
            array (
                'permission_id' => 1289,
                'role_id' => 1,
            ),
            225 => 
            array (
                'permission_id' => 1290,
                'role_id' => 1,
            ),
            226 => 
            array (
                'permission_id' => 1291,
                'role_id' => 1,
            ),
            227 => 
            array (
                'permission_id' => 1292,
                'role_id' => 1,
            ),
            228 => 
            array (
                'permission_id' => 1293,
                'role_id' => 1,
            ),
            229 => 
            array (
                'permission_id' => 1294,
                'role_id' => 1,
            ),
            230 => 
            array (
                'permission_id' => 1295,
                'role_id' => 1,
            ),
            231 => 
            array (
                'permission_id' => 1296,
                'role_id' => 1,
            ),
            232 => 
            array (
                'permission_id' => 1297,
                'role_id' => 1,
            ),
            233 => 
            array (
                'permission_id' => 1298,
                'role_id' => 1,
            ),
            234 => 
            array (
                'permission_id' => 1299,
                'role_id' => 1,
            ),
            235 => 
            array (
                'permission_id' => 1300,
                'role_id' => 1,
            ),
            236 => 
            array (
                'permission_id' => 1301,
                'role_id' => 1,
            ),
            237 => 
            array (
                'permission_id' => 1302,
                'role_id' => 1,
            ),
            238 => 
            array (
                'permission_id' => 1303,
                'role_id' => 1,
            ),
            239 => 
            array (
                'permission_id' => 1304,
                'role_id' => 1,
            ),
            240 => 
            array (
                'permission_id' => 1305,
                'role_id' => 1,
            ),
            241 => 
            array (
                'permission_id' => 1306,
                'role_id' => 1,
            ),
            242 => 
            array (
                'permission_id' => 1307,
                'role_id' => 1,
            ),
            243 => 
            array (
                'permission_id' => 1308,
                'role_id' => 1,
            ),
            244 => 
            array (
                'permission_id' => 1309,
                'role_id' => 1,
            ),
            245 => 
            array (
                'permission_id' => 1310,
                'role_id' => 1,
            ),
            246 => 
            array (
                'permission_id' => 1311,
                'role_id' => 1,
            ),
            247 => 
            array (
                'permission_id' => 1312,
                'role_id' => 1,
            ),
            248 => 
            array (
                'permission_id' => 1313,
                'role_id' => 1,
            ),
            249 => 
            array (
                'permission_id' => 1314,
                'role_id' => 1,
            ),
            250 => 
            array (
                'permission_id' => 1315,
                'role_id' => 1,
            ),
            251 => 
            array (
                'permission_id' => 1316,
                'role_id' => 1,
            ),
            252 => 
            array (
                'permission_id' => 1317,
                'role_id' => 1,
            ),
            253 => 
            array (
                'permission_id' => 1318,
                'role_id' => 1,
            ),
            254 => 
            array (
                'permission_id' => 1319,
                'role_id' => 1,
            ),
            255 => 
            array (
                'permission_id' => 1320,
                'role_id' => 1,
            ),
            256 => 
            array (
                'permission_id' => 1321,
                'role_id' => 1,
            ),
            257 => 
            array (
                'permission_id' => 1322,
                'role_id' => 1,
            ),
            258 => 
            array (
                'permission_id' => 1323,
                'role_id' => 1,
            ),
            259 => 
            array (
                'permission_id' => 1324,
                'role_id' => 1,
            ),
            260 => 
            array (
                'permission_id' => 1325,
                'role_id' => 1,
            ),
            261 => 
            array (
                'permission_id' => 1326,
                'role_id' => 1,
            ),
            262 => 
            array (
                'permission_id' => 1327,
                'role_id' => 1,
            ),
            263 => 
            array (
                'permission_id' => 1328,
                'role_id' => 1,
            ),
            264 => 
            array (
                'permission_id' => 1329,
                'role_id' => 1,
            ),
            265 => 
            array (
                'permission_id' => 1330,
                'role_id' => 1,
            ),
            266 => 
            array (
                'permission_id' => 1331,
                'role_id' => 1,
            ),
            267 => 
            array (
                'permission_id' => 1332,
                'role_id' => 1,
            ),
            268 => 
            array (
                'permission_id' => 1333,
                'role_id' => 1,
            ),
            269 => 
            array (
                'permission_id' => 1334,
                'role_id' => 1,
            ),
            270 => 
            array (
                'permission_id' => 1335,
                'role_id' => 1,
            ),
            271 => 
            array (
                'permission_id' => 1336,
                'role_id' => 1,
            ),
            272 => 
            array (
                'permission_id' => 1337,
                'role_id' => 1,
            ),
            273 => 
            array (
                'permission_id' => 1338,
                'role_id' => 1,
            ),
            274 => 
            array (
                'permission_id' => 1339,
                'role_id' => 1,
            ),
            275 => 
            array (
                'permission_id' => 1340,
                'role_id' => 1,
            ),
            276 => 
            array (
                'permission_id' => 1341,
                'role_id' => 1,
            ),
            277 => 
            array (
                'permission_id' => 1342,
                'role_id' => 1,
            ),
            278 => 
            array (
                'permission_id' => 1343,
                'role_id' => 1,
            ),
            279 => 
            array (
                'permission_id' => 1344,
                'role_id' => 1,
            ),
            280 => 
            array (
                'permission_id' => 1345,
                'role_id' => 1,
            ),
            281 => 
            array (
                'permission_id' => 1346,
                'role_id' => 1,
            ),
            282 => 
            array (
                'permission_id' => 1347,
                'role_id' => 1,
            ),
            283 => 
            array (
                'permission_id' => 1348,
                'role_id' => 1,
            ),
            284 => 
            array (
                'permission_id' => 1349,
                'role_id' => 1,
            ),
            285 => 
            array (
                'permission_id' => 1350,
                'role_id' => 1,
            ),
            286 => 
            array (
                'permission_id' => 1351,
                'role_id' => 1,
            ),
            287 => 
            array (
                'permission_id' => 1352,
                'role_id' => 1,
            ),
            288 => 
            array (
                'permission_id' => 1353,
                'role_id' => 1,
            ),
            289 => 
            array (
                'permission_id' => 1354,
                'role_id' => 1,
            ),
            290 => 
            array (
                'permission_id' => 1355,
                'role_id' => 1,
            ),
            291 => 
            array (
                'permission_id' => 1356,
                'role_id' => 1,
            ),
            292 => 
            array (
                'permission_id' => 1357,
                'role_id' => 1,
            ),
            293 => 
            array (
                'permission_id' => 1358,
                'role_id' => 1,
            ),
            294 => 
            array (
                'permission_id' => 1359,
                'role_id' => 1,
            ),
            295 => 
            array (
                'permission_id' => 1360,
                'role_id' => 1,
            ),
            296 => 
            array (
                'permission_id' => 1361,
                'role_id' => 1,
            ),
            297 => 
            array (
                'permission_id' => 1362,
                'role_id' => 1,
            ),
            298 => 
            array (
                'permission_id' => 1363,
                'role_id' => 1,
            ),
            299 => 
            array (
                'permission_id' => 1364,
                'role_id' => 1,
            ),
            300 => 
            array (
                'permission_id' => 1365,
                'role_id' => 1,
            ),
            301 => 
            array (
                'permission_id' => 1366,
                'role_id' => 1,
            ),
            302 => 
            array (
                'permission_id' => 1367,
                'role_id' => 1,
            ),
            303 => 
            array (
                'permission_id' => 1368,
                'role_id' => 1,
            ),
            304 => 
            array (
                'permission_id' => 1369,
                'role_id' => 1,
            ),
            305 => 
            array (
                'permission_id' => 1370,
                'role_id' => 1,
            ),
            306 => 
            array (
                'permission_id' => 1371,
                'role_id' => 1,
            ),
            307 => 
            array (
                'permission_id' => 1372,
                'role_id' => 1,
            ),
            308 => 
            array (
                'permission_id' => 1373,
                'role_id' => 1,
            ),
            309 => 
            array (
                'permission_id' => 1374,
                'role_id' => 1,
            ),
            310 => 
            array (
                'permission_id' => 1375,
                'role_id' => 1,
            ),
            311 => 
            array (
                'permission_id' => 1376,
                'role_id' => 1,
            ),
            312 => 
            array (
                'permission_id' => 1377,
                'role_id' => 1,
            ),
            313 => 
            array (
                'permission_id' => 1378,
                'role_id' => 1,
            ),
            314 => 
            array (
                'permission_id' => 1379,
                'role_id' => 1,
            ),
            315 => 
            array (
                'permission_id' => 1380,
                'role_id' => 1,
            ),
            316 => 
            array (
                'permission_id' => 1381,
                'role_id' => 1,
            ),
            317 => 
            array (
                'permission_id' => 1382,
                'role_id' => 1,
            ),
            318 => 
            array (
                'permission_id' => 1383,
                'role_id' => 1,
            ),
            319 => 
            array (
                'permission_id' => 1384,
                'role_id' => 1,
            ),
            320 => 
            array (
                'permission_id' => 1385,
                'role_id' => 1,
            ),
            321 => 
            array (
                'permission_id' => 1386,
                'role_id' => 1,
            ),
            322 => 
            array (
                'permission_id' => 1387,
                'role_id' => 1,
            ),
            323 => 
            array (
                'permission_id' => 1388,
                'role_id' => 1,
            ),
            324 => 
            array (
                'permission_id' => 1389,
                'role_id' => 1,
            ),
            325 => 
            array (
                'permission_id' => 1390,
                'role_id' => 1,
            ),
            326 => 
            array (
                'permission_id' => 1391,
                'role_id' => 1,
            ),
            327 => 
            array (
                'permission_id' => 1392,
                'role_id' => 1,
            ),
            328 => 
            array (
                'permission_id' => 1393,
                'role_id' => 1,
            ),
            329 => 
            array (
                'permission_id' => 1394,
                'role_id' => 1,
            ),
            330 => 
            array (
                'permission_id' => 1395,
                'role_id' => 1,
            ),
            331 => 
            array (
                'permission_id' => 1396,
                'role_id' => 1,
            ),
            332 => 
            array (
                'permission_id' => 1397,
                'role_id' => 1,
            ),
            333 => 
            array (
                'permission_id' => 1398,
                'role_id' => 1,
            ),
            334 => 
            array (
                'permission_id' => 1399,
                'role_id' => 1,
            ),
            335 => 
            array (
                'permission_id' => 1400,
                'role_id' => 1,
            ),
            336 => 
            array (
                'permission_id' => 1401,
                'role_id' => 1,
            ),
            337 => 
            array (
                'permission_id' => 1402,
                'role_id' => 1,
            ),
            338 => 
            array (
                'permission_id' => 1403,
                'role_id' => 1,
            ),
            339 => 
            array (
                'permission_id' => 1404,
                'role_id' => 1,
            ),
            340 => 
            array (
                'permission_id' => 1405,
                'role_id' => 1,
            ),
            341 => 
            array (
                'permission_id' => 1406,
                'role_id' => 1,
            ),
            342 => 
            array (
                'permission_id' => 1407,
                'role_id' => 1,
            ),
            343 => 
            array (
                'permission_id' => 1408,
                'role_id' => 1,
            ),
            344 => 
            array (
                'permission_id' => 1409,
                'role_id' => 1,
            ),
            345 => 
            array (
                'permission_id' => 1410,
                'role_id' => 1,
            ),
            346 => 
            array (
                'permission_id' => 1411,
                'role_id' => 1,
            ),
            347 => 
            array (
                'permission_id' => 1412,
                'role_id' => 1,
            ),
            348 => 
            array (
                'permission_id' => 1413,
                'role_id' => 1,
            ),
            349 => 
            array (
                'permission_id' => 1414,
                'role_id' => 1,
            ),
            350 => 
            array (
                'permission_id' => 1415,
                'role_id' => 1,
            ),
            351 => 
            array (
                'permission_id' => 1416,
                'role_id' => 1,
            ),
            352 => 
            array (
                'permission_id' => 1417,
                'role_id' => 1,
            ),
            353 => 
            array (
                'permission_id' => 1418,
                'role_id' => 1,
            ),
            354 => 
            array (
                'permission_id' => 1419,
                'role_id' => 1,
            ),
            355 => 
            array (
                'permission_id' => 1420,
                'role_id' => 1,
            ),
            356 => 
            array (
                'permission_id' => 1421,
                'role_id' => 1,
            ),
            357 => 
            array (
                'permission_id' => 1422,
                'role_id' => 1,
            ),
            358 => 
            array (
                'permission_id' => 1423,
                'role_id' => 1,
            ),
            359 => 
            array (
                'permission_id' => 1424,
                'role_id' => 1,
            ),
            360 => 
            array (
                'permission_id' => 1425,
                'role_id' => 1,
            ),
            361 => 
            array (
                'permission_id' => 1426,
                'role_id' => 1,
            ),
            362 => 
            array (
                'permission_id' => 1427,
                'role_id' => 1,
            ),
            363 => 
            array (
                'permission_id' => 1428,
                'role_id' => 1,
            ),
            364 => 
            array (
                'permission_id' => 1429,
                'role_id' => 1,
            ),
            365 => 
            array (
                'permission_id' => 1430,
                'role_id' => 1,
            ),
            366 => 
            array (
                'permission_id' => 1431,
                'role_id' => 1,
            ),
            367 => 
            array (
                'permission_id' => 1432,
                'role_id' => 1,
            ),
            368 => 
            array (
                'permission_id' => 1433,
                'role_id' => 1,
            ),
            369 => 
            array (
                'permission_id' => 1434,
                'role_id' => 1,
            ),
            370 => 
            array (
                'permission_id' => 1435,
                'role_id' => 1,
            ),
            371 => 
            array (
                'permission_id' => 1436,
                'role_id' => 1,
            ),
            372 => 
            array (
                'permission_id' => 1437,
                'role_id' => 1,
            ),
            373 => 
            array (
                'permission_id' => 1438,
                'role_id' => 1,
            ),
            374 => 
            array (
                'permission_id' => 1439,
                'role_id' => 1,
            ),
            375 => 
            array (
                'permission_id' => 1440,
                'role_id' => 1,
            ),
            376 => 
            array (
                'permission_id' => 1441,
                'role_id' => 1,
            ),
            377 => 
            array (
                'permission_id' => 1442,
                'role_id' => 1,
            ),
            378 => 
            array (
                'permission_id' => 1443,
                'role_id' => 1,
            ),
            379 => 
            array (
                'permission_id' => 1444,
                'role_id' => 1,
            ),
            380 => 
            array (
                'permission_id' => 1445,
                'role_id' => 1,
            ),
            381 => 
            array (
                'permission_id' => 1446,
                'role_id' => 1,
            ),
            382 => 
            array (
                'permission_id' => 1447,
                'role_id' => 1,
            ),
            383 => 
            array (
                'permission_id' => 1448,
                'role_id' => 1,
            ),
            384 => 
            array (
                'permission_id' => 1449,
                'role_id' => 1,
            ),
            385 => 
            array (
                'permission_id' => 1450,
                'role_id' => 1,
            ),
            386 => 
            array (
                'permission_id' => 1451,
                'role_id' => 1,
            ),
            387 => 
            array (
                'permission_id' => 1452,
                'role_id' => 1,
            ),
            388 => 
            array (
                'permission_id' => 1453,
                'role_id' => 1,
            ),
            389 => 
            array (
                'permission_id' => 1454,
                'role_id' => 1,
            ),
            390 => 
            array (
                'permission_id' => 1455,
                'role_id' => 1,
            ),
            391 => 
            array (
                'permission_id' => 1456,
                'role_id' => 1,
            ),
            392 => 
            array (
                'permission_id' => 1457,
                'role_id' => 1,
            ),
            393 => 
            array (
                'permission_id' => 1458,
                'role_id' => 1,
            ),
            394 => 
            array (
                'permission_id' => 1459,
                'role_id' => 1,
            ),
            395 => 
            array (
                'permission_id' => 1460,
                'role_id' => 1,
            ),
            396 => 
            array (
                'permission_id' => 1461,
                'role_id' => 1,
            ),
            397 => 
            array (
                'permission_id' => 1462,
                'role_id' => 1,
            ),
            398 => 
            array (
                'permission_id' => 1463,
                'role_id' => 1,
            ),
            399 => 
            array (
                'permission_id' => 1464,
                'role_id' => 1,
            ),
            400 => 
            array (
                'permission_id' => 1465,
                'role_id' => 1,
            ),
            401 => 
            array (
                'permission_id' => 1466,
                'role_id' => 1,
            ),
            402 => 
            array (
                'permission_id' => 1467,
                'role_id' => 1,
            ),
            403 => 
            array (
                'permission_id' => 1468,
                'role_id' => 1,
            ),
            404 => 
            array (
                'permission_id' => 1469,
                'role_id' => 1,
            ),
            405 => 
            array (
                'permission_id' => 1470,
                'role_id' => 1,
            ),
            406 => 
            array (
                'permission_id' => 1471,
                'role_id' => 1,
            ),
            407 => 
            array (
                'permission_id' => 1472,
                'role_id' => 1,
            ),
            408 => 
            array (
                'permission_id' => 1473,
                'role_id' => 1,
            ),
            409 => 
            array (
                'permission_id' => 1474,
                'role_id' => 1,
            ),
            410 => 
            array (
                'permission_id' => 1475,
                'role_id' => 1,
            ),
            411 => 
            array (
                'permission_id' => 1476,
                'role_id' => 1,
            ),
            412 => 
            array (
                'permission_id' => 1477,
                'role_id' => 1,
            ),
            413 => 
            array (
                'permission_id' => 1478,
                'role_id' => 1,
            ),
            414 => 
            array (
                'permission_id' => 1479,
                'role_id' => 1,
            ),
            415 => 
            array (
                'permission_id' => 1480,
                'role_id' => 1,
            ),
            416 => 
            array (
                'permission_id' => 1481,
                'role_id' => 1,
            ),
            417 => 
            array (
                'permission_id' => 1482,
                'role_id' => 1,
            ),
            418 => 
            array (
                'permission_id' => 1483,
                'role_id' => 1,
            ),
            419 => 
            array (
                'permission_id' => 1484,
                'role_id' => 1,
            ),
            420 => 
            array (
                'permission_id' => 1485,
                'role_id' => 1,
            ),
            421 => 
            array (
                'permission_id' => 1486,
                'role_id' => 1,
            ),
            422 => 
            array (
                'permission_id' => 1487,
                'role_id' => 1,
            ),
            423 => 
            array (
                'permission_id' => 1488,
                'role_id' => 1,
            ),
            424 => 
            array (
                'permission_id' => 1489,
                'role_id' => 1,
            ),
            425 => 
            array (
                'permission_id' => 1490,
                'role_id' => 1,
            ),
            426 => 
            array (
                'permission_id' => 1491,
                'role_id' => 1,
            ),
            427 => 
            array (
                'permission_id' => 1492,
                'role_id' => 1,
            ),
            428 => 
            array (
                'permission_id' => 1493,
                'role_id' => 1,
            ),
            429 => 
            array (
                'permission_id' => 1494,
                'role_id' => 1,
            ),
            430 => 
            array (
                'permission_id' => 1495,
                'role_id' => 1,
            ),
            431 => 
            array (
                'permission_id' => 1496,
                'role_id' => 1,
            ),
            432 => 
            array (
                'permission_id' => 1497,
                'role_id' => 1,
            ),
            433 => 
            array (
                'permission_id' => 1498,
                'role_id' => 1,
            ),
            434 => 
            array (
                'permission_id' => 1499,
                'role_id' => 1,
            ),
            435 => 
            array (
                'permission_id' => 1500,
                'role_id' => 1,
            ),
            436 => 
            array (
                'permission_id' => 1501,
                'role_id' => 1,
            ),
            437 => 
            array (
                'permission_id' => 1502,
                'role_id' => 1,
            ),
            438 => 
            array (
                'permission_id' => 1503,
                'role_id' => 1,
            ),
            439 => 
            array (
                'permission_id' => 1504,
                'role_id' => 1,
            ),
            440 => 
            array (
                'permission_id' => 1505,
                'role_id' => 1,
            ),
            441 => 
            array (
                'permission_id' => 1506,
                'role_id' => 1,
            ),
            442 => 
            array (
                'permission_id' => 1507,
                'role_id' => 1,
            ),
            443 => 
            array (
                'permission_id' => 1508,
                'role_id' => 1,
            ),
            444 => 
            array (
                'permission_id' => 1509,
                'role_id' => 1,
            ),
            445 => 
            array (
                'permission_id' => 1510,
                'role_id' => 1,
            ),
            446 => 
            array (
                'permission_id' => 1511,
                'role_id' => 1,
            ),
            447 => 
            array (
                'permission_id' => 1512,
                'role_id' => 1,
            ),
            448 => 
            array (
                'permission_id' => 1513,
                'role_id' => 1,
            ),
            449 => 
            array (
                'permission_id' => 1514,
                'role_id' => 1,
            ),
            450 => 
            array (
                'permission_id' => 1515,
                'role_id' => 1,
            ),
            451 => 
            array (
                'permission_id' => 1516,
                'role_id' => 1,
            ),
            452 => 
            array (
                'permission_id' => 1517,
                'role_id' => 1,
            ),
            453 => 
            array (
                'permission_id' => 1518,
                'role_id' => 1,
            ),
            454 => 
            array (
                'permission_id' => 1519,
                'role_id' => 1,
            ),
            455 => 
            array (
                'permission_id' => 1520,
                'role_id' => 1,
            ),
            456 => 
            array (
                'permission_id' => 1521,
                'role_id' => 1,
            ),
            457 => 
            array (
                'permission_id' => 1522,
                'role_id' => 1,
            ),
            458 => 
            array (
                'permission_id' => 1523,
                'role_id' => 1,
            ),
            459 => 
            array (
                'permission_id' => 1524,
                'role_id' => 1,
            ),
            460 => 
            array (
                'permission_id' => 1525,
                'role_id' => 1,
            ),
            461 => 
            array (
                'permission_id' => 1526,
                'role_id' => 1,
            ),
            462 => 
            array (
                'permission_id' => 1527,
                'role_id' => 1,
            ),
            463 => 
            array (
                'permission_id' => 1528,
                'role_id' => 1,
            ),
            464 => 
            array (
                'permission_id' => 1529,
                'role_id' => 1,
            ),
            465 => 
            array (
                'permission_id' => 1530,
                'role_id' => 1,
            ),
            466 => 
            array (
                'permission_id' => 1531,
                'role_id' => 1,
            ),
            467 => 
            array (
                'permission_id' => 1532,
                'role_id' => 1,
            ),
            468 => 
            array (
                'permission_id' => 1533,
                'role_id' => 1,
            ),
            469 => 
            array (
                'permission_id' => 1534,
                'role_id' => 1,
            ),
            470 => 
            array (
                'permission_id' => 1535,
                'role_id' => 1,
            ),
            471 => 
            array (
                'permission_id' => 1536,
                'role_id' => 1,
            ),
            472 => 
            array (
                'permission_id' => 1537,
                'role_id' => 1,
            ),
            473 => 
            array (
                'permission_id' => 1538,
                'role_id' => 1,
            ),
            474 => 
            array (
                'permission_id' => 1539,
                'role_id' => 1,
            ),
            475 => 
            array (
                'permission_id' => 1540,
                'role_id' => 1,
            ),
            476 => 
            array (
                'permission_id' => 1541,
                'role_id' => 1,
            ),
            477 => 
            array (
                'permission_id' => 1542,
                'role_id' => 1,
            ),
            478 => 
            array (
                'permission_id' => 1543,
                'role_id' => 1,
            ),
            479 => 
            array (
                'permission_id' => 1544,
                'role_id' => 1,
            ),
            480 => 
            array (
                'permission_id' => 1545,
                'role_id' => 1,
            ),
            481 => 
            array (
                'permission_id' => 1546,
                'role_id' => 1,
            ),
            482 => 
            array (
                'permission_id' => 1547,
                'role_id' => 1,
            ),
            483 => 
            array (
                'permission_id' => 1548,
                'role_id' => 1,
            ),
            484 => 
            array (
                'permission_id' => 1552,
                'role_id' => 1,
            ),
            485 => 
            array (
                'permission_id' => 1553,
                'role_id' => 1,
            ),
            486 => 
            array (
                'permission_id' => 1554,
                'role_id' => 1,
            ),
            487 => 
            array (
                'permission_id' => 1555,
                'role_id' => 1,
            ),
            488 => 
            array (
                'permission_id' => 1556,
                'role_id' => 1,
            ),
            489 => 
            array (
                'permission_id' => 1557,
                'role_id' => 1,
            ),
            490 => 
            array (
                'permission_id' => 1558,
                'role_id' => 1,
            ),
            491 => 
            array (
                'permission_id' => 1559,
                'role_id' => 1,
            ),
            492 => 
            array (
                'permission_id' => 1560,
                'role_id' => 1,
            ),
            493 => 
            array (
                'permission_id' => 1561,
                'role_id' => 1,
            ),
            494 => 
            array (
                'permission_id' => 1562,
                'role_id' => 1,
            ),
            495 => 
            array (
                'permission_id' => 1563,
                'role_id' => 1,
            ),
            496 => 
            array (
                'permission_id' => 1564,
                'role_id' => 1,
            ),
            497 => 
            array (
                'permission_id' => 1565,
                'role_id' => 1,
            ),
            498 => 
            array (
                'permission_id' => 1566,
                'role_id' => 1,
            ),
            499 => 
            array (
                'permission_id' => 1567,
                'role_id' => 1,
            ),
        ));
        \DB::table('role_has_permissions')->insert(array (
            0 => 
            array (
                'permission_id' => 1568,
                'role_id' => 1,
            ),
            1 => 
            array (
                'permission_id' => 1569,
                'role_id' => 1,
            ),
            2 => 
            array (
                'permission_id' => 1570,
                'role_id' => 1,
            ),
            3 => 
            array (
                'permission_id' => 1571,
                'role_id' => 1,
            ),
            4 => 
            array (
                'permission_id' => 1572,
                'role_id' => 1,
            ),
            5 => 
            array (
                'permission_id' => 1573,
                'role_id' => 1,
            ),
            6 => 
            array (
                'permission_id' => 1574,
                'role_id' => 1,
            ),
            7 => 
            array (
                'permission_id' => 1575,
                'role_id' => 1,
            ),
            8 => 
            array (
                'permission_id' => 1576,
                'role_id' => 1,
            ),
            9 => 
            array (
                'permission_id' => 1577,
                'role_id' => 1,
            ),
            10 => 
            array (
                'permission_id' => 1578,
                'role_id' => 1,
            ),
            11 => 
            array (
                'permission_id' => 1579,
                'role_id' => 1,
            ),
            12 => 
            array (
                'permission_id' => 1580,
                'role_id' => 1,
            ),
            13 => 
            array (
                'permission_id' => 1581,
                'role_id' => 1,
            ),
            14 => 
            array (
                'permission_id' => 1582,
                'role_id' => 1,
            ),
            15 => 
            array (
                'permission_id' => 1583,
                'role_id' => 1,
            ),
            16 => 
            array (
                'permission_id' => 1584,
                'role_id' => 1,
            ),
            17 => 
            array (
                'permission_id' => 1585,
                'role_id' => 1,
            ),
            18 => 
            array (
                'permission_id' => 1586,
                'role_id' => 1,
            ),
            19 => 
            array (
                'permission_id' => 1587,
                'role_id' => 1,
            ),
            20 => 
            array (
                'permission_id' => 1588,
                'role_id' => 1,
            ),
            21 => 
            array (
                'permission_id' => 1589,
                'role_id' => 1,
            ),
            22 => 
            array (
                'permission_id' => 1590,
                'role_id' => 1,
            ),
            23 => 
            array (
                'permission_id' => 1591,
                'role_id' => 1,
            ),
            24 => 
            array (
                'permission_id' => 1592,
                'role_id' => 1,
            ),
            25 => 
            array (
                'permission_id' => 1593,
                'role_id' => 1,
            ),
            26 => 
            array (
                'permission_id' => 1594,
                'role_id' => 1,
            ),
            27 => 
            array (
                'permission_id' => 1595,
                'role_id' => 1,
            ),
            28 => 
            array (
                'permission_id' => 1596,
                'role_id' => 1,
            ),
            29 => 
            array (
                'permission_id' => 1597,
                'role_id' => 1,
            ),
            30 => 
            array (
                'permission_id' => 1598,
                'role_id' => 1,
            ),
            31 => 
            array (
                'permission_id' => 1599,
                'role_id' => 1,
            ),
            32 => 
            array (
                'permission_id' => 1600,
                'role_id' => 1,
            ),
            33 => 
            array (
                'permission_id' => 1601,
                'role_id' => 1,
            ),
            34 => 
            array (
                'permission_id' => 1602,
                'role_id' => 1,
            ),
            35 => 
            array (
                'permission_id' => 1603,
                'role_id' => 1,
            ),
            36 => 
            array (
                'permission_id' => 1604,
                'role_id' => 1,
            ),
            37 => 
            array (
                'permission_id' => 1605,
                'role_id' => 1,
            ),
            38 => 
            array (
                'permission_id' => 1606,
                'role_id' => 1,
            ),
            39 => 
            array (
                'permission_id' => 1607,
                'role_id' => 1,
            ),
            40 => 
            array (
                'permission_id' => 1608,
                'role_id' => 1,
            ),
            41 => 
            array (
                'permission_id' => 1609,
                'role_id' => 1,
            ),
            42 => 
            array (
                'permission_id' => 1610,
                'role_id' => 1,
            ),
            43 => 
            array (
                'permission_id' => 1611,
                'role_id' => 1,
            ),
            44 => 
            array (
                'permission_id' => 1612,
                'role_id' => 1,
            ),
            45 => 
            array (
                'permission_id' => 1724,
                'role_id' => 1,
            ),
            46 => 
            array (
                'permission_id' => 1725,
                'role_id' => 1,
            ),
            47 => 
            array (
                'permission_id' => 1726,
                'role_id' => 1,
            ),
            48 => 
            array (
                'permission_id' => 1727,
                'role_id' => 1,
            ),
            49 => 
            array (
                'permission_id' => 1728,
                'role_id' => 1,
            ),
            50 => 
            array (
                'permission_id' => 1729,
                'role_id' => 1,
            ),
            51 => 
            array (
                'permission_id' => 1730,
                'role_id' => 1,
            ),
            52 => 
            array (
                'permission_id' => 1731,
                'role_id' => 1,
            ),
            53 => 
            array (
                'permission_id' => 1732,
                'role_id' => 1,
            ),
            54 => 
            array (
                'permission_id' => 1733,
                'role_id' => 1,
            ),
            55 => 
            array (
                'permission_id' => 1734,
                'role_id' => 1,
            ),
            56 => 
            array (
                'permission_id' => 1735,
                'role_id' => 1,
            ),
            57 => 
            array (
                'permission_id' => 1736,
                'role_id' => 1,
            ),
            58 => 
            array (
                'permission_id' => 1737,
                'role_id' => 1,
            ),
            59 => 
            array (
                'permission_id' => 1738,
                'role_id' => 1,
            ),
            60 => 
            array (
                'permission_id' => 1739,
                'role_id' => 1,
            ),
            61 => 
            array (
                'permission_id' => 1740,
                'role_id' => 1,
            ),
            62 => 
            array (
                'permission_id' => 1741,
                'role_id' => 1,
            ),
            63 => 
            array (
                'permission_id' => 1742,
                'role_id' => 1,
            ),
            64 => 
            array (
                'permission_id' => 1743,
                'role_id' => 1,
            ),
            65 => 
            array (
                'permission_id' => 1744,
                'role_id' => 1,
            ),
            66 => 
            array (
                'permission_id' => 1745,
                'role_id' => 1,
            ),
            67 => 
            array (
                'permission_id' => 1746,
                'role_id' => 1,
            ),
            68 => 
            array (
                'permission_id' => 1747,
                'role_id' => 1,
            ),
            69 => 
            array (
                'permission_id' => 1748,
                'role_id' => 1,
            ),
            70 => 
            array (
                'permission_id' => 1749,
                'role_id' => 1,
            ),
            71 => 
            array (
                'permission_id' => 1750,
                'role_id' => 1,
            ),
            72 => 
            array (
                'permission_id' => 1751,
                'role_id' => 1,
            ),
            73 => 
            array (
                'permission_id' => 1752,
                'role_id' => 1,
            ),
            74 => 
            array (
                'permission_id' => 1753,
                'role_id' => 1,
            ),
            75 => 
            array (
                'permission_id' => 1754,
                'role_id' => 1,
            ),
            76 => 
            array (
                'permission_id' => 1755,
                'role_id' => 1,
            ),
            77 => 
            array (
                'permission_id' => 1756,
                'role_id' => 1,
            ),
            78 => 
            array (
                'permission_id' => 1757,
                'role_id' => 1,
            ),
            79 => 
            array (
                'permission_id' => 1758,
                'role_id' => 1,
            ),
            80 => 
            array (
                'permission_id' => 1759,
                'role_id' => 1,
            ),
            81 => 
            array (
                'permission_id' => 1760,
                'role_id' => 1,
            ),
            82 => 
            array (
                'permission_id' => 1761,
                'role_id' => 1,
            ),
            83 => 
            array (
                'permission_id' => 1762,
                'role_id' => 1,
            ),
            84 => 
            array (
                'permission_id' => 1763,
                'role_id' => 1,
            ),
            85 => 
            array (
                'permission_id' => 1764,
                'role_id' => 1,
            ),
            86 => 
            array (
                'permission_id' => 1765,
                'role_id' => 1,
            ),
            87 => 
            array (
                'permission_id' => 1766,
                'role_id' => 1,
            ),
            88 => 
            array (
                'permission_id' => 1767,
                'role_id' => 1,
            ),
            89 => 
            array (
                'permission_id' => 1768,
                'role_id' => 1,
            ),
            90 => 
            array (
                'permission_id' => 1769,
                'role_id' => 1,
            ),
            91 => 
            array (
                'permission_id' => 1770,
                'role_id' => 1,
            ),
            92 => 
            array (
                'permission_id' => 1771,
                'role_id' => 1,
            ),
            93 => 
            array (
                'permission_id' => 1772,
                'role_id' => 1,
            ),
            94 => 
            array (
                'permission_id' => 1773,
                'role_id' => 1,
            ),
            95 => 
            array (
                'permission_id' => 1774,
                'role_id' => 1,
            ),
            96 => 
            array (
                'permission_id' => 1775,
                'role_id' => 1,
            ),
            97 => 
            array (
                'permission_id' => 1776,
                'role_id' => 1,
            ),
            98 => 
            array (
                'permission_id' => 1777,
                'role_id' => 1,
            ),
            99 => 
            array (
                'permission_id' => 1778,
                'role_id' => 1,
            ),
            100 => 
            array (
                'permission_id' => 1779,
                'role_id' => 1,
            ),
            101 => 
            array (
                'permission_id' => 1780,
                'role_id' => 1,
            ),
            102 => 
            array (
                'permission_id' => 1781,
                'role_id' => 1,
            ),
            103 => 
            array (
                'permission_id' => 1782,
                'role_id' => 1,
            ),
            104 => 
            array (
                'permission_id' => 1783,
                'role_id' => 1,
            ),
            105 => 
            array (
                'permission_id' => 1784,
                'role_id' => 1,
            ),
            106 => 
            array (
                'permission_id' => 1785,
                'role_id' => 1,
            ),
            107 => 
            array (
                'permission_id' => 1786,
                'role_id' => 1,
            ),
            108 => 
            array (
                'permission_id' => 1787,
                'role_id' => 1,
            ),
            109 => 
            array (
                'permission_id' => 1788,
                'role_id' => 1,
            ),
            110 => 
            array (
                'permission_id' => 1789,
                'role_id' => 1,
            ),
            111 => 
            array (
                'permission_id' => 1790,
                'role_id' => 1,
            ),
            112 => 
            array (
                'permission_id' => 1791,
                'role_id' => 1,
            ),
            113 => 
            array (
                'permission_id' => 1792,
                'role_id' => 1,
            ),
            114 => 
            array (
                'permission_id' => 1793,
                'role_id' => 1,
            ),
            115 => 
            array (
                'permission_id' => 1794,
                'role_id' => 1,
            ),
            116 => 
            array (
                'permission_id' => 1795,
                'role_id' => 1,
            ),
            117 => 
            array (
                'permission_id' => 1796,
                'role_id' => 1,
            ),
            118 => 
            array (
                'permission_id' => 1797,
                'role_id' => 1,
            ),
            119 => 
            array (
                'permission_id' => 1798,
                'role_id' => 1,
            ),
            120 => 
            array (
                'permission_id' => 1799,
                'role_id' => 1,
            ),
            121 => 
            array (
                'permission_id' => 1800,
                'role_id' => 1,
            ),
            122 => 
            array (
                'permission_id' => 1801,
                'role_id' => 1,
            ),
            123 => 
            array (
                'permission_id' => 1802,
                'role_id' => 1,
            ),
            124 => 
            array (
                'permission_id' => 1803,
                'role_id' => 1,
            ),
            125 => 
            array (
                'permission_id' => 1804,
                'role_id' => 1,
            ),
            126 => 
            array (
                'permission_id' => 1805,
                'role_id' => 1,
            ),
            127 => 
            array (
                'permission_id' => 1806,
                'role_id' => 1,
            ),
            128 => 
            array (
                'permission_id' => 1807,
                'role_id' => 1,
            ),
            129 => 
            array (
                'permission_id' => 1808,
                'role_id' => 1,
            ),
            130 => 
            array (
                'permission_id' => 1809,
                'role_id' => 1,
            ),
            131 => 
            array (
                'permission_id' => 1810,
                'role_id' => 1,
            ),
            132 => 
            array (
                'permission_id' => 1811,
                'role_id' => 1,
            ),
            133 => 
            array (
                'permission_id' => 1812,
                'role_id' => 1,
            ),
            134 => 
            array (
                'permission_id' => 1813,
                'role_id' => 1,
            ),
            135 => 
            array (
                'permission_id' => 1814,
                'role_id' => 1,
            ),
            136 => 
            array (
                'permission_id' => 1815,
                'role_id' => 1,
            ),
            137 => 
            array (
                'permission_id' => 1816,
                'role_id' => 1,
            ),
            138 => 
            array (
                'permission_id' => 1817,
                'role_id' => 1,
            ),
            139 => 
            array (
                'permission_id' => 1818,
                'role_id' => 1,
            ),
            140 => 
            array (
                'permission_id' => 1819,
                'role_id' => 1,
            ),
            141 => 
            array (
                'permission_id' => 1820,
                'role_id' => 1,
            ),
            142 => 
            array (
                'permission_id' => 1821,
                'role_id' => 1,
            ),
            143 => 
            array (
                'permission_id' => 1822,
                'role_id' => 1,
            ),
            144 => 
            array (
                'permission_id' => 1823,
                'role_id' => 1,
            ),
            145 => 
            array (
                'permission_id' => 1824,
                'role_id' => 1,
            ),
            146 => 
            array (
                'permission_id' => 1825,
                'role_id' => 1,
            ),
            147 => 
            array (
                'permission_id' => 1826,
                'role_id' => 1,
            ),
            148 => 
            array (
                'permission_id' => 1827,
                'role_id' => 1,
            ),
            149 => 
            array (
                'permission_id' => 1828,
                'role_id' => 1,
            ),
            150 => 
            array (
                'permission_id' => 1829,
                'role_id' => 1,
            ),
            151 => 
            array (
                'permission_id' => 1830,
                'role_id' => 1,
            ),
            152 => 
            array (
                'permission_id' => 1831,
                'role_id' => 1,
            ),
            153 => 
            array (
                'permission_id' => 1832,
                'role_id' => 1,
            ),
            154 => 
            array (
                'permission_id' => 1833,
                'role_id' => 1,
            ),
            155 => 
            array (
                'permission_id' => 1834,
                'role_id' => 1,
            ),
            156 => 
            array (
                'permission_id' => 1835,
                'role_id' => 1,
            ),
            157 => 
            array (
                'permission_id' => 1836,
                'role_id' => 1,
            ),
            158 => 
            array (
                'permission_id' => 1837,
                'role_id' => 1,
            ),
            159 => 
            array (
                'permission_id' => 1838,
                'role_id' => 1,
            ),
            160 => 
            array (
                'permission_id' => 1839,
                'role_id' => 1,
            ),
            161 => 
            array (
                'permission_id' => 1840,
                'role_id' => 1,
            ),
            162 => 
            array (
                'permission_id' => 1841,
                'role_id' => 1,
            ),
            163 => 
            array (
                'permission_id' => 1842,
                'role_id' => 1,
            ),
            164 => 
            array (
                'permission_id' => 1843,
                'role_id' => 1,
            ),
            165 => 
            array (
                'permission_id' => 1844,
                'role_id' => 1,
            ),
            166 => 
            array (
                'permission_id' => 1845,
                'role_id' => 1,
            ),
            167 => 
            array (
                'permission_id' => 1846,
                'role_id' => 1,
            ),
            168 => 
            array (
                'permission_id' => 1847,
                'role_id' => 1,
            ),
            169 => 
            array (
                'permission_id' => 1848,
                'role_id' => 1,
            ),
            170 => 
            array (
                'permission_id' => 1849,
                'role_id' => 1,
            ),
            171 => 
            array (
                'permission_id' => 1850,
                'role_id' => 1,
            ),
            172 => 
            array (
                'permission_id' => 1851,
                'role_id' => 1,
            ),
            173 => 
            array (
                'permission_id' => 1852,
                'role_id' => 1,
            ),
            174 => 
            array (
                'permission_id' => 1853,
                'role_id' => 1,
            ),
            175 => 
            array (
                'permission_id' => 1854,
                'role_id' => 1,
            ),
            176 => 
            array (
                'permission_id' => 1855,
                'role_id' => 1,
            ),
            177 => 
            array (
                'permission_id' => 1856,
                'role_id' => 1,
            ),
            178 => 
            array (
                'permission_id' => 1862,
                'role_id' => 1,
            ),
            179 => 
            array (
                'permission_id' => 1863,
                'role_id' => 1,
            ),
            180 => 
            array (
                'permission_id' => 1864,
                'role_id' => 1,
            ),
            181 => 
            array (
                'permission_id' => 1865,
                'role_id' => 1,
            ),
            182 => 
            array (
                'permission_id' => 1866,
                'role_id' => 1,
            ),
            183 => 
            array (
                'permission_id' => 1867,
                'role_id' => 1,
            ),
            184 => 
            array (
                'permission_id' => 1868,
                'role_id' => 1,
            ),
            185 => 
            array (
                'permission_id' => 1869,
                'role_id' => 1,
            ),
            186 => 
            array (
                'permission_id' => 1870,
                'role_id' => 1,
            ),
            187 => 
            array (
                'permission_id' => 1871,
                'role_id' => 1,
            ),
            188 => 
            array (
                'permission_id' => 1872,
                'role_id' => 1,
            ),
            189 => 
            array (
                'permission_id' => 1873,
                'role_id' => 1,
            ),
            190 => 
            array (
                'permission_id' => 1874,
                'role_id' => 1,
            ),
            191 => 
            array (
                'permission_id' => 1875,
                'role_id' => 1,
            ),
            192 => 
            array (
                'permission_id' => 1876,
                'role_id' => 1,
            ),
            193 => 
            array (
                'permission_id' => 1877,
                'role_id' => 1,
            ),
            194 => 
            array (
                'permission_id' => 1878,
                'role_id' => 1,
            ),
            195 => 
            array (
                'permission_id' => 1879,
                'role_id' => 1,
            ),
            196 => 
            array (
                'permission_id' => 1880,
                'role_id' => 1,
            ),
            197 => 
            array (
                'permission_id' => 1881,
                'role_id' => 1,
            ),
            198 => 
            array (
                'permission_id' => 1882,
                'role_id' => 1,
            ),
            199 => 
            array (
                'permission_id' => 1883,
                'role_id' => 1,
            ),
            200 => 
            array (
                'permission_id' => 1884,
                'role_id' => 1,
            ),
            201 => 
            array (
                'permission_id' => 1885,
                'role_id' => 1,
            ),
            202 => 
            array (
                'permission_id' => 1886,
                'role_id' => 1,
            ),
            203 => 
            array (
                'permission_id' => 1887,
                'role_id' => 1,
            ),
            204 => 
            array (
                'permission_id' => 1888,
                'role_id' => 1,
            ),
            205 => 
            array (
                'permission_id' => 1889,
                'role_id' => 1,
            ),
            206 => 
            array (
                'permission_id' => 1890,
                'role_id' => 1,
            ),
            207 => 
            array (
                'permission_id' => 1891,
                'role_id' => 1,
            ),
            208 => 
            array (
                'permission_id' => 1892,
                'role_id' => 1,
            ),
            209 => 
            array (
                'permission_id' => 1893,
                'role_id' => 1,
            ),
            210 => 
            array (
                'permission_id' => 1894,
                'role_id' => 1,
            ),
            211 => 
            array (
                'permission_id' => 1895,
                'role_id' => 1,
            ),
            212 => 
            array (
                'permission_id' => 1896,
                'role_id' => 1,
            ),
            213 => 
            array (
                'permission_id' => 1897,
                'role_id' => 1,
            ),
            214 => 
            array (
                'permission_id' => 1898,
                'role_id' => 1,
            ),
            215 => 
            array (
                'permission_id' => 1899,
                'role_id' => 1,
            ),
            216 => 
            array (
                'permission_id' => 1900,
                'role_id' => 1,
            ),
            217 => 
            array (
                'permission_id' => 1901,
                'role_id' => 1,
            ),
            218 => 
            array (
                'permission_id' => 1902,
                'role_id' => 1,
            ),
            219 => 
            array (
                'permission_id' => 1903,
                'role_id' => 1,
            ),
            220 => 
            array (
                'permission_id' => 1904,
                'role_id' => 1,
            ),
            221 => 
            array (
                'permission_id' => 1905,
                'role_id' => 1,
            ),
            222 => 
            array (
                'permission_id' => 1906,
                'role_id' => 1,
            ),
            223 => 
            array (
                'permission_id' => 1907,
                'role_id' => 1,
            ),
            224 => 
            array (
                'permission_id' => 1908,
                'role_id' => 1,
            ),
            225 => 
            array (
                'permission_id' => 1909,
                'role_id' => 1,
            ),
            226 => 
            array (
                'permission_id' => 1910,
                'role_id' => 1,
            ),
            227 => 
            array (
                'permission_id' => 1911,
                'role_id' => 1,
            ),
            228 => 
            array (
                'permission_id' => 1912,
                'role_id' => 1,
            ),
            229 => 
            array (
                'permission_id' => 1913,
                'role_id' => 1,
            ),
            230 => 
            array (
                'permission_id' => 1914,
                'role_id' => 1,
            ),
            231 => 
            array (
                'permission_id' => 1915,
                'role_id' => 1,
            ),
            232 => 
            array (
                'permission_id' => 1916,
                'role_id' => 1,
            ),
            233 => 
            array (
                'permission_id' => 1917,
                'role_id' => 1,
            ),
            234 => 
            array (
                'permission_id' => 1918,
                'role_id' => 1,
            ),
            235 => 
            array (
                'permission_id' => 1919,
                'role_id' => 1,
            ),
            236 => 
            array (
                'permission_id' => 1920,
                'role_id' => 1,
            ),
            237 => 
            array (
                'permission_id' => 1921,
                'role_id' => 1,
            ),
            238 => 
            array (
                'permission_id' => 1922,
                'role_id' => 1,
            ),
            239 => 
            array (
                'permission_id' => 1923,
                'role_id' => 1,
            ),
            240 => 
            array (
                'permission_id' => 1924,
                'role_id' => 1,
            ),
            241 => 
            array (
                'permission_id' => 1925,
                'role_id' => 1,
            ),
            242 => 
            array (
                'permission_id' => 1926,
                'role_id' => 1,
            ),
            243 => 
            array (
                'permission_id' => 1927,
                'role_id' => 1,
            ),
            244 => 
            array (
                'permission_id' => 1928,
                'role_id' => 1,
            ),
            245 => 
            array (
                'permission_id' => 1929,
                'role_id' => 1,
            ),
            246 => 
            array (
                'permission_id' => 1930,
                'role_id' => 1,
            ),
            247 => 
            array (
                'permission_id' => 1931,
                'role_id' => 1,
            ),
            248 => 
            array (
                'permission_id' => 1932,
                'role_id' => 1,
            ),
            249 => 
            array (
                'permission_id' => 1933,
                'role_id' => 1,
            ),
            250 => 
            array (
                'permission_id' => 1934,
                'role_id' => 1,
            ),
            251 => 
            array (
                'permission_id' => 1935,
                'role_id' => 1,
            ),
            252 => 
            array (
                'permission_id' => 1936,
                'role_id' => 1,
            ),
            253 => 
            array (
                'permission_id' => 1937,
                'role_id' => 1,
            ),
            254 => 
            array (
                'permission_id' => 1938,
                'role_id' => 1,
            ),
            255 => 
            array (
                'permission_id' => 1939,
                'role_id' => 1,
            ),
            256 => 
            array (
                'permission_id' => 1940,
                'role_id' => 1,
            ),
            257 => 
            array (
                'permission_id' => 1941,
                'role_id' => 1,
            ),
            258 => 
            array (
                'permission_id' => 1942,
                'role_id' => 1,
            ),
            259 => 
            array (
                'permission_id' => 1943,
                'role_id' => 1,
            ),
            260 => 
            array (
                'permission_id' => 1944,
                'role_id' => 1,
            ),
            261 => 
            array (
                'permission_id' => 1945,
                'role_id' => 1,
            ),
            262 => 
            array (
                'permission_id' => 1946,
                'role_id' => 1,
            ),
            263 => 
            array (
                'permission_id' => 1947,
                'role_id' => 1,
            ),
            264 => 
            array (
                'permission_id' => 1948,
                'role_id' => 1,
            ),
            265 => 
            array (
                'permission_id' => 1949,
                'role_id' => 1,
            ),
            266 => 
            array (
                'permission_id' => 1950,
                'role_id' => 1,
            ),
            267 => 
            array (
                'permission_id' => 1951,
                'role_id' => 1,
            ),
            268 => 
            array (
                'permission_id' => 1952,
                'role_id' => 1,
            ),
            269 => 
            array (
                'permission_id' => 1953,
                'role_id' => 1,
            ),
            270 => 
            array (
                'permission_id' => 1954,
                'role_id' => 1,
            ),
            271 => 
            array (
                'permission_id' => 1955,
                'role_id' => 1,
            ),
            272 => 
            array (
                'permission_id' => 1956,
                'role_id' => 1,
            ),
            273 => 
            array (
                'permission_id' => 1957,
                'role_id' => 1,
            ),
            274 => 
            array (
                'permission_id' => 1958,
                'role_id' => 1,
            ),
            275 => 
            array (
                'permission_id' => 1959,
                'role_id' => 1,
            ),
            276 => 
            array (
                'permission_id' => 1960,
                'role_id' => 1,
            ),
            277 => 
            array (
                'permission_id' => 1961,
                'role_id' => 1,
            ),
            278 => 
            array (
                'permission_id' => 1962,
                'role_id' => 1,
            ),
            279 => 
            array (
                'permission_id' => 1963,
                'role_id' => 1,
            ),
            280 => 
            array (
                'permission_id' => 1964,
                'role_id' => 1,
            ),
            281 => 
            array (
                'permission_id' => 1965,
                'role_id' => 1,
            ),
            282 => 
            array (
                'permission_id' => 1966,
                'role_id' => 1,
            ),
            283 => 
            array (
                'permission_id' => 1967,
                'role_id' => 1,
            ),
            284 => 
            array (
                'permission_id' => 1968,
                'role_id' => 1,
            ),
            285 => 
            array (
                'permission_id' => 1969,
                'role_id' => 1,
            ),
            286 => 
            array (
                'permission_id' => 1970,
                'role_id' => 1,
            ),
            287 => 
            array (
                'permission_id' => 1971,
                'role_id' => 1,
            ),
            288 => 
            array (
                'permission_id' => 1972,
                'role_id' => 1,
            ),
            289 => 
            array (
                'permission_id' => 1973,
                'role_id' => 1,
            ),
            290 => 
            array (
                'permission_id' => 1974,
                'role_id' => 1,
            ),
            291 => 
            array (
                'permission_id' => 1975,
                'role_id' => 1,
            ),
            292 => 
            array (
                'permission_id' => 1976,
                'role_id' => 1,
            ),
            293 => 
            array (
                'permission_id' => 1977,
                'role_id' => 1,
            ),
            294 => 
            array (
                'permission_id' => 1978,
                'role_id' => 1,
            ),
            295 => 
            array (
                'permission_id' => 1979,
                'role_id' => 1,
            ),
            296 => 
            array (
                'permission_id' => 1980,
                'role_id' => 1,
            ),
            297 => 
            array (
                'permission_id' => 1981,
                'role_id' => 1,
            ),
            298 => 
            array (
                'permission_id' => 1982,
                'role_id' => 1,
            ),
            299 => 
            array (
                'permission_id' => 1983,
                'role_id' => 1,
            ),
            300 => 
            array (
                'permission_id' => 1984,
                'role_id' => 1,
            ),
            301 => 
            array (
                'permission_id' => 1985,
                'role_id' => 1,
            ),
            302 => 
            array (
                'permission_id' => 1986,
                'role_id' => 1,
            ),
            303 => 
            array (
                'permission_id' => 1987,
                'role_id' => 1,
            ),
            304 => 
            array (
                'permission_id' => 1988,
                'role_id' => 1,
            ),
            305 => 
            array (
                'permission_id' => 1989,
                'role_id' => 1,
            ),
            306 => 
            array (
                'permission_id' => 1990,
                'role_id' => 1,
            ),
            307 => 
            array (
                'permission_id' => 1991,
                'role_id' => 1,
            ),
            308 => 
            array (
                'permission_id' => 1992,
                'role_id' => 1,
            ),
            309 => 
            array (
                'permission_id' => 1993,
                'role_id' => 1,
            ),
            310 => 
            array (
                'permission_id' => 1994,
                'role_id' => 1,
            ),
            311 => 
            array (
                'permission_id' => 1995,
                'role_id' => 1,
            ),
            312 => 
            array (
                'permission_id' => 1996,
                'role_id' => 1,
            ),
            313 => 
            array (
                'permission_id' => 1997,
                'role_id' => 1,
            ),
            314 => 
            array (
                'permission_id' => 1998,
                'role_id' => 1,
            ),
            315 => 
            array (
                'permission_id' => 1999,
                'role_id' => 1,
            ),
            316 => 
            array (
                'permission_id' => 2000,
                'role_id' => 1,
            ),
            317 => 
            array (
                'permission_id' => 2001,
                'role_id' => 1,
            ),
            318 => 
            array (
                'permission_id' => 2002,
                'role_id' => 1,
            ),
            319 => 
            array (
                'permission_id' => 2003,
                'role_id' => 1,
            ),
            320 => 
            array (
                'permission_id' => 2004,
                'role_id' => 1,
            ),
            321 => 
            array (
                'permission_id' => 2005,
                'role_id' => 1,
            ),
            322 => 
            array (
                'permission_id' => 2006,
                'role_id' => 1,
            ),
            323 => 
            array (
                'permission_id' => 2007,
                'role_id' => 1,
            ),
            324 => 
            array (
                'permission_id' => 2008,
                'role_id' => 1,
            ),
            325 => 
            array (
                'permission_id' => 2009,
                'role_id' => 1,
            ),
            326 => 
            array (
                'permission_id' => 2010,
                'role_id' => 1,
            ),
            327 => 
            array (
                'permission_id' => 2011,
                'role_id' => 1,
            ),
            328 => 
            array (
                'permission_id' => 2012,
                'role_id' => 1,
            ),
            329 => 
            array (
                'permission_id' => 2013,
                'role_id' => 1,
            ),
            330 => 
            array (
                'permission_id' => 2014,
                'role_id' => 1,
            ),
            331 => 
            array (
                'permission_id' => 2015,
                'role_id' => 1,
            ),
            332 => 
            array (
                'permission_id' => 2016,
                'role_id' => 1,
            ),
            333 => 
            array (
                'permission_id' => 2017,
                'role_id' => 1,
            ),
            334 => 
            array (
                'permission_id' => 2018,
                'role_id' => 1,
            ),
            335 => 
            array (
                'permission_id' => 2019,
                'role_id' => 1,
            ),
            336 => 
            array (
                'permission_id' => 2020,
                'role_id' => 1,
            ),
            337 => 
            array (
                'permission_id' => 2021,
                'role_id' => 1,
            ),
            338 => 
            array (
                'permission_id' => 2022,
                'role_id' => 1,
            ),
            339 => 
            array (
                'permission_id' => 2023,
                'role_id' => 1,
            ),
            340 => 
            array (
                'permission_id' => 2024,
                'role_id' => 1,
            ),
            341 => 
            array (
                'permission_id' => 2025,
                'role_id' => 1,
            ),
            342 => 
            array (
                'permission_id' => 2026,
                'role_id' => 1,
            ),
            343 => 
            array (
                'permission_id' => 2027,
                'role_id' => 1,
            ),
            344 => 
            array (
                'permission_id' => 2028,
                'role_id' => 1,
            ),
            345 => 
            array (
                'permission_id' => 2029,
                'role_id' => 1,
            ),
            346 => 
            array (
                'permission_id' => 2030,
                'role_id' => 1,
            ),
            347 => 
            array (
                'permission_id' => 2031,
                'role_id' => 1,
            ),
            348 => 
            array (
                'permission_id' => 2032,
                'role_id' => 1,
            ),
            349 => 
            array (
                'permission_id' => 2033,
                'role_id' => 1,
            ),
            350 => 
            array (
                'permission_id' => 2034,
                'role_id' => 1,
            ),
            351 => 
            array (
                'permission_id' => 2035,
                'role_id' => 1,
            ),
            352 => 
            array (
                'permission_id' => 2036,
                'role_id' => 1,
            ),
            353 => 
            array (
                'permission_id' => 2037,
                'role_id' => 1,
            ),
            354 => 
            array (
                'permission_id' => 2038,
                'role_id' => 1,
            ),
            355 => 
            array (
                'permission_id' => 2039,
                'role_id' => 1,
            ),
            356 => 
            array (
                'permission_id' => 2040,
                'role_id' => 1,
            ),
            357 => 
            array (
                'permission_id' => 2041,
                'role_id' => 1,
            ),
            358 => 
            array (
                'permission_id' => 2042,
                'role_id' => 1,
            ),
            359 => 
            array (
                'permission_id' => 2043,
                'role_id' => 1,
            ),
            360 => 
            array (
                'permission_id' => 2044,
                'role_id' => 1,
            ),
            361 => 
            array (
                'permission_id' => 2045,
                'role_id' => 1,
            ),
            362 => 
            array (
                'permission_id' => 2046,
                'role_id' => 1,
            ),
            363 => 
            array (
                'permission_id' => 2047,
                'role_id' => 1,
            ),
            364 => 
            array (
                'permission_id' => 2048,
                'role_id' => 1,
            ),
            365 => 
            array (
                'permission_id' => 2049,
                'role_id' => 1,
            ),
            366 => 
            array (
                'permission_id' => 2050,
                'role_id' => 1,
            ),
            367 => 
            array (
                'permission_id' => 2051,
                'role_id' => 1,
            ),
            368 => 
            array (
                'permission_id' => 2052,
                'role_id' => 1,
            ),
            369 => 
            array (
                'permission_id' => 2053,
                'role_id' => 1,
            ),
            370 => 
            array (
                'permission_id' => 2054,
                'role_id' => 1,
            ),
            371 => 
            array (
                'permission_id' => 2055,
                'role_id' => 1,
            ),
            372 => 
            array (
                'permission_id' => 2056,
                'role_id' => 1,
            ),
            373 => 
            array (
                'permission_id' => 2057,
                'role_id' => 1,
            ),
            374 => 
            array (
                'permission_id' => 2058,
                'role_id' => 1,
            ),
            375 => 
            array (
                'permission_id' => 2059,
                'role_id' => 1,
            ),
            376 => 
            array (
                'permission_id' => 2060,
                'role_id' => 1,
            ),
            377 => 
            array (
                'permission_id' => 2061,
                'role_id' => 1,
            ),
            378 => 
            array (
                'permission_id' => 2062,
                'role_id' => 1,
            ),
            379 => 
            array (
                'permission_id' => 2063,
                'role_id' => 1,
            ),
            380 => 
            array (
                'permission_id' => 2064,
                'role_id' => 1,
            ),
            381 => 
            array (
                'permission_id' => 2065,
                'role_id' => 1,
            ),
            382 => 
            array (
                'permission_id' => 2066,
                'role_id' => 1,
            ),
            383 => 
            array (
                'permission_id' => 2067,
                'role_id' => 1,
            ),
            384 => 
            array (
                'permission_id' => 2068,
                'role_id' => 1,
            ),
            385 => 
            array (
                'permission_id' => 2069,
                'role_id' => 1,
            ),
            386 => 
            array (
                'permission_id' => 2070,
                'role_id' => 1,
            ),
            387 => 
            array (
                'permission_id' => 2071,
                'role_id' => 1,
            ),
            388 => 
            array (
                'permission_id' => 2072,
                'role_id' => 1,
            ),
            389 => 
            array (
                'permission_id' => 2073,
                'role_id' => 1,
            ),
            390 => 
            array (
                'permission_id' => 2074,
                'role_id' => 1,
            ),
            391 => 
            array (
                'permission_id' => 2075,
                'role_id' => 1,
            ),
            392 => 
            array (
                'permission_id' => 2076,
                'role_id' => 1,
            ),
            393 => 
            array (
                'permission_id' => 2077,
                'role_id' => 1,
            ),
            394 => 
            array (
                'permission_id' => 2078,
                'role_id' => 1,
            ),
            395 => 
            array (
                'permission_id' => 2079,
                'role_id' => 1,
            ),
            396 => 
            array (
                'permission_id' => 2080,
                'role_id' => 1,
            ),
            397 => 
            array (
                'permission_id' => 2081,
                'role_id' => 1,
            ),
            398 => 
            array (
                'permission_id' => 2082,
                'role_id' => 1,
            ),
            399 => 
            array (
                'permission_id' => 2083,
                'role_id' => 1,
            ),
            400 => 
            array (
                'permission_id' => 2084,
                'role_id' => 1,
            ),
            401 => 
            array (
                'permission_id' => 2085,
                'role_id' => 1,
            ),
            402 => 
            array (
                'permission_id' => 2086,
                'role_id' => 1,
            ),
            403 => 
            array (
                'permission_id' => 2087,
                'role_id' => 1,
            ),
            404 => 
            array (
                'permission_id' => 2088,
                'role_id' => 1,
            ),
            405 => 
            array (
                'permission_id' => 2089,
                'role_id' => 1,
            ),
            406 => 
            array (
                'permission_id' => 2090,
                'role_id' => 1,
            ),
            407 => 
            array (
                'permission_id' => 2091,
                'role_id' => 1,
            ),
            408 => 
            array (
                'permission_id' => 2092,
                'role_id' => 1,
            ),
            409 => 
            array (
                'permission_id' => 2093,
                'role_id' => 1,
            ),
            410 => 
            array (
                'permission_id' => 2094,
                'role_id' => 1,
            ),
            411 => 
            array (
                'permission_id' => 2095,
                'role_id' => 1,
            ),
            412 => 
            array (
                'permission_id' => 2096,
                'role_id' => 1,
            ),
            413 => 
            array (
                'permission_id' => 2097,
                'role_id' => 1,
            ),
            414 => 
            array (
                'permission_id' => 2098,
                'role_id' => 1,
            ),
            415 => 
            array (
                'permission_id' => 2099,
                'role_id' => 1,
            ),
            416 => 
            array (
                'permission_id' => 2100,
                'role_id' => 1,
            ),
            417 => 
            array (
                'permission_id' => 2101,
                'role_id' => 1,
            ),
            418 => 
            array (
                'permission_id' => 2102,
                'role_id' => 1,
            ),
            419 => 
            array (
                'permission_id' => 2103,
                'role_id' => 1,
            ),
            420 => 
            array (
                'permission_id' => 2104,
                'role_id' => 1,
            ),
            421 => 
            array (
                'permission_id' => 2105,
                'role_id' => 1,
            ),
            422 => 
            array (
                'permission_id' => 2106,
                'role_id' => 1,
            ),
            423 => 
            array (
                'permission_id' => 2107,
                'role_id' => 1,
            ),
            424 => 
            array (
                'permission_id' => 2108,
                'role_id' => 1,
            ),
            425 => 
            array (
                'permission_id' => 2109,
                'role_id' => 1,
            ),
            426 => 
            array (
                'permission_id' => 2110,
                'role_id' => 1,
            ),
            427 => 
            array (
                'permission_id' => 2111,
                'role_id' => 1,
            ),
            428 => 
            array (
                'permission_id' => 2112,
                'role_id' => 1,
            ),
            429 => 
            array (
                'permission_id' => 2113,
                'role_id' => 1,
            ),
            430 => 
            array (
                'permission_id' => 2114,
                'role_id' => 1,
            ),
            431 => 
            array (
                'permission_id' => 2115,
                'role_id' => 1,
            ),
            432 => 
            array (
                'permission_id' => 2116,
                'role_id' => 1,
            ),
            433 => 
            array (
                'permission_id' => 2117,
                'role_id' => 1,
            ),
            434 => 
            array (
                'permission_id' => 2118,
                'role_id' => 1,
            ),
            435 => 
            array (
                'permission_id' => 2119,
                'role_id' => 1,
            ),
            436 => 
            array (
                'permission_id' => 2120,
                'role_id' => 1,
            ),
            437 => 
            array (
                'permission_id' => 2121,
                'role_id' => 1,
            ),
            438 => 
            array (
                'permission_id' => 2122,
                'role_id' => 1,
            ),
            439 => 
            array (
                'permission_id' => 2123,
                'role_id' => 1,
            ),
            440 => 
            array (
                'permission_id' => 2124,
                'role_id' => 1,
            ),
            441 => 
            array (
                'permission_id' => 2125,
                'role_id' => 1,
            ),
            442 => 
            array (
                'permission_id' => 2126,
                'role_id' => 1,
            ),
            443 => 
            array (
                'permission_id' => 2127,
                'role_id' => 1,
            ),
            444 => 
            array (
                'permission_id' => 2128,
                'role_id' => 1,
            ),
            445 => 
            array (
                'permission_id' => 2129,
                'role_id' => 1,
            ),
            446 => 
            array (
                'permission_id' => 2130,
                'role_id' => 1,
            ),
            447 => 
            array (
                'permission_id' => 2131,
                'role_id' => 1,
            ),
            448 => 
            array (
                'permission_id' => 2132,
                'role_id' => 1,
            ),
            449 => 
            array (
                'permission_id' => 2133,
                'role_id' => 1,
            ),
            450 => 
            array (
                'permission_id' => 2134,
                'role_id' => 1,
            ),
            451 => 
            array (
                'permission_id' => 2135,
                'role_id' => 1,
            ),
            452 => 
            array (
                'permission_id' => 2136,
                'role_id' => 1,
            ),
            453 => 
            array (
                'permission_id' => 2137,
                'role_id' => 1,
            ),
            454 => 
            array (
                'permission_id' => 2138,
                'role_id' => 1,
            ),
            455 => 
            array (
                'permission_id' => 2139,
                'role_id' => 1,
            ),
            456 => 
            array (
                'permission_id' => 2140,
                'role_id' => 1,
            ),
            457 => 
            array (
                'permission_id' => 2141,
                'role_id' => 1,
            ),
            458 => 
            array (
                'permission_id' => 2142,
                'role_id' => 1,
            ),
            459 => 
            array (
                'permission_id' => 2143,
                'role_id' => 1,
            ),
            460 => 
            array (
                'permission_id' => 2144,
                'role_id' => 1,
            ),
            461 => 
            array (
                'permission_id' => 2145,
                'role_id' => 1,
            ),
            462 => 
            array (
                'permission_id' => 2146,
                'role_id' => 1,
            ),
            463 => 
            array (
                'permission_id' => 2147,
                'role_id' => 1,
            ),
            464 => 
            array (
                'permission_id' => 2148,
                'role_id' => 1,
            ),
            465 => 
            array (
                'permission_id' => 2149,
                'role_id' => 1,
            ),
            466 => 
            array (
                'permission_id' => 2150,
                'role_id' => 1,
            ),
            467 => 
            array (
                'permission_id' => 2151,
                'role_id' => 1,
            ),
            468 => 
            array (
                'permission_id' => 2152,
                'role_id' => 1,
            ),
            469 => 
            array (
                'permission_id' => 2153,
                'role_id' => 1,
            ),
            470 => 
            array (
                'permission_id' => 2154,
                'role_id' => 1,
            ),
            471 => 
            array (
                'permission_id' => 2155,
                'role_id' => 1,
            ),
            472 => 
            array (
                'permission_id' => 2156,
                'role_id' => 1,
            ),
            473 => 
            array (
                'permission_id' => 2157,
                'role_id' => 1,
            ),
            474 => 
            array (
                'permission_id' => 2158,
                'role_id' => 1,
            ),
            475 => 
            array (
                'permission_id' => 2159,
                'role_id' => 1,
            ),
            476 => 
            array (
                'permission_id' => 2160,
                'role_id' => 1,
            ),
            477 => 
            array (
                'permission_id' => 2161,
                'role_id' => 1,
            ),
            478 => 
            array (
                'permission_id' => 2162,
                'role_id' => 1,
            ),
            479 => 
            array (
                'permission_id' => 2163,
                'role_id' => 1,
            ),
            480 => 
            array (
                'permission_id' => 2164,
                'role_id' => 1,
            ),
            481 => 
            array (
                'permission_id' => 2165,
                'role_id' => 1,
            ),
            482 => 
            array (
                'permission_id' => 2166,
                'role_id' => 1,
            ),
            483 => 
            array (
                'permission_id' => 2167,
                'role_id' => 1,
            ),
            484 => 
            array (
                'permission_id' => 2168,
                'role_id' => 1,
            ),
            485 => 
            array (
                'permission_id' => 2169,
                'role_id' => 1,
            ),
            486 => 
            array (
                'permission_id' => 2170,
                'role_id' => 1,
            ),
            487 => 
            array (
                'permission_id' => 2171,
                'role_id' => 1,
            ),
            488 => 
            array (
                'permission_id' => 2172,
                'role_id' => 1,
            ),
            489 => 
            array (
                'permission_id' => 2173,
                'role_id' => 1,
            ),
            490 => 
            array (
                'permission_id' => 2174,
                'role_id' => 1,
            ),
            491 => 
            array (
                'permission_id' => 2175,
                'role_id' => 1,
            ),
            492 => 
            array (
                'permission_id' => 2176,
                'role_id' => 1,
            ),
            493 => 
            array (
                'permission_id' => 2177,
                'role_id' => 1,
            ),
            494 => 
            array (
                'permission_id' => 2178,
                'role_id' => 1,
            ),
            495 => 
            array (
                'permission_id' => 2179,
                'role_id' => 1,
            ),
            496 => 
            array (
                'permission_id' => 2180,
                'role_id' => 1,
            ),
            497 => 
            array (
                'permission_id' => 2181,
                'role_id' => 1,
            ),
            498 => 
            array (
                'permission_id' => 2182,
                'role_id' => 1,
            ),
            499 => 
            array (
                'permission_id' => 2183,
                'role_id' => 1,
            ),
        ));
        \DB::table('role_has_permissions')->insert(array (
            0 => 
            array (
                'permission_id' => 2184,
                'role_id' => 1,
            ),
            1 => 
            array (
                'permission_id' => 2185,
                'role_id' => 1,
            ),
            2 => 
            array (
                'permission_id' => 2186,
                'role_id' => 1,
            ),
            3 => 
            array (
                'permission_id' => 2187,
                'role_id' => 1,
            ),
            4 => 
            array (
                'permission_id' => 2188,
                'role_id' => 1,
            ),
            5 => 
            array (
                'permission_id' => 2189,
                'role_id' => 1,
            ),
            6 => 
            array (
                'permission_id' => 2190,
                'role_id' => 1,
            ),
            7 => 
            array (
                'permission_id' => 2191,
                'role_id' => 1,
            ),
            8 => 
            array (
                'permission_id' => 2192,
                'role_id' => 1,
            ),
            9 => 
            array (
                'permission_id' => 2193,
                'role_id' => 1,
            ),
            10 => 
            array (
                'permission_id' => 2194,
                'role_id' => 1,
            ),
            11 => 
            array (
                'permission_id' => 2195,
                'role_id' => 1,
            ),
            12 => 
            array (
                'permission_id' => 2196,
                'role_id' => 1,
            ),
            13 => 
            array (
                'permission_id' => 2197,
                'role_id' => 1,
            ),
            14 => 
            array (
                'permission_id' => 2198,
                'role_id' => 1,
            ),
            15 => 
            array (
                'permission_id' => 2199,
                'role_id' => 1,
            ),
            16 => 
            array (
                'permission_id' => 2200,
                'role_id' => 1,
            ),
            17 => 
            array (
                'permission_id' => 2201,
                'role_id' => 1,
            ),
            18 => 
            array (
                'permission_id' => 2202,
                'role_id' => 1,
            ),
            19 => 
            array (
                'permission_id' => 2203,
                'role_id' => 1,
            ),
            20 => 
            array (
                'permission_id' => 2204,
                'role_id' => 1,
            ),
            21 => 
            array (
                'permission_id' => 2205,
                'role_id' => 1,
            ),
            22 => 
            array (
                'permission_id' => 2206,
                'role_id' => 1,
            ),
            23 => 
            array (
                'permission_id' => 2207,
                'role_id' => 1,
            ),
            24 => 
            array (
                'permission_id' => 2208,
                'role_id' => 1,
            ),
            25 => 
            array (
                'permission_id' => 2209,
                'role_id' => 1,
            ),
            26 => 
            array (
                'permission_id' => 2210,
                'role_id' => 1,
            ),
            27 => 
            array (
                'permission_id' => 2211,
                'role_id' => 1,
            ),
            28 => 
            array (
                'permission_id' => 2212,
                'role_id' => 1,
            ),
            29 => 
            array (
                'permission_id' => 2213,
                'role_id' => 1,
            ),
            30 => 
            array (
                'permission_id' => 2214,
                'role_id' => 1,
            ),
            31 => 
            array (
                'permission_id' => 2215,
                'role_id' => 1,
            ),
            32 => 
            array (
                'permission_id' => 2216,
                'role_id' => 1,
            ),
            33 => 
            array (
                'permission_id' => 2217,
                'role_id' => 1,
            ),
            34 => 
            array (
                'permission_id' => 2218,
                'role_id' => 1,
            ),
            35 => 
            array (
                'permission_id' => 2219,
                'role_id' => 1,
            ),
            36 => 
            array (
                'permission_id' => 2220,
                'role_id' => 1,
            ),
            37 => 
            array (
                'permission_id' => 2221,
                'role_id' => 1,
            ),
            38 => 
            array (
                'permission_id' => 2222,
                'role_id' => 1,
            ),
            39 => 
            array (
                'permission_id' => 2223,
                'role_id' => 1,
            ),
            40 => 
            array (
                'permission_id' => 2224,
                'role_id' => 1,
            ),
            41 => 
            array (
                'permission_id' => 2225,
                'role_id' => 1,
            ),
            42 => 
            array (
                'permission_id' => 2226,
                'role_id' => 1,
            ),
            43 => 
            array (
                'permission_id' => 2227,
                'role_id' => 1,
            ),
            44 => 
            array (
                'permission_id' => 2228,
                'role_id' => 1,
            ),
            45 => 
            array (
                'permission_id' => 2229,
                'role_id' => 1,
            ),
            46 => 
            array (
                'permission_id' => 2230,
                'role_id' => 1,
            ),
            47 => 
            array (
                'permission_id' => 2231,
                'role_id' => 1,
            ),
            48 => 
            array (
                'permission_id' => 2232,
                'role_id' => 1,
            ),
            49 => 
            array (
                'permission_id' => 2233,
                'role_id' => 1,
            ),
            50 => 
            array (
                'permission_id' => 2234,
                'role_id' => 1,
            ),
            51 => 
            array (
                'permission_id' => 2235,
                'role_id' => 1,
            ),
            52 => 
            array (
                'permission_id' => 2236,
                'role_id' => 1,
            ),
            53 => 
            array (
                'permission_id' => 2237,
                'role_id' => 1,
            ),
            54 => 
            array (
                'permission_id' => 2238,
                'role_id' => 1,
            ),
            55 => 
            array (
                'permission_id' => 2239,
                'role_id' => 1,
            ),
            56 => 
            array (
                'permission_id' => 2240,
                'role_id' => 1,
            ),
            57 => 
            array (
                'permission_id' => 2241,
                'role_id' => 1,
            ),
            58 => 
            array (
                'permission_id' => 2242,
                'role_id' => 1,
            ),
            59 => 
            array (
                'permission_id' => 2243,
                'role_id' => 1,
            ),
            60 => 
            array (
                'permission_id' => 2244,
                'role_id' => 1,
            ),
            61 => 
            array (
                'permission_id' => 2245,
                'role_id' => 1,
            ),
            62 => 
            array (
                'permission_id' => 2246,
                'role_id' => 1,
            ),
            63 => 
            array (
                'permission_id' => 2247,
                'role_id' => 1,
            ),
            64 => 
            array (
                'permission_id' => 2248,
                'role_id' => 1,
            ),
            65 => 
            array (
                'permission_id' => 2249,
                'role_id' => 1,
            ),
            66 => 
            array (
                'permission_id' => 2250,
                'role_id' => 1,
            ),
            67 => 
            array (
                'permission_id' => 2251,
                'role_id' => 1,
            ),
            68 => 
            array (
                'permission_id' => 2252,
                'role_id' => 1,
            ),
            69 => 
            array (
                'permission_id' => 2253,
                'role_id' => 1,
            ),
            70 => 
            array (
                'permission_id' => 2254,
                'role_id' => 1,
            ),
            71 => 
            array (
                'permission_id' => 2255,
                'role_id' => 1,
            ),
            72 => 
            array (
                'permission_id' => 2256,
                'role_id' => 1,
            ),
            73 => 
            array (
                'permission_id' => 2257,
                'role_id' => 1,
            ),
            74 => 
            array (
                'permission_id' => 2258,
                'role_id' => 1,
            ),
            75 => 
            array (
                'permission_id' => 2259,
                'role_id' => 1,
            ),
            76 => 
            array (
                'permission_id' => 2260,
                'role_id' => 1,
            ),
            77 => 
            array (
                'permission_id' => 2261,
                'role_id' => 1,
            ),
            78 => 
            array (
                'permission_id' => 2262,
                'role_id' => 1,
            ),
            79 => 
            array (
                'permission_id' => 2263,
                'role_id' => 1,
            ),
            80 => 
            array (
                'permission_id' => 2264,
                'role_id' => 1,
            ),
            81 => 
            array (
                'permission_id' => 2265,
                'role_id' => 1,
            ),
            82 => 
            array (
                'permission_id' => 2266,
                'role_id' => 1,
            ),
            83 => 
            array (
                'permission_id' => 2267,
                'role_id' => 1,
            ),
            84 => 
            array (
                'permission_id' => 2268,
                'role_id' => 1,
            ),
            85 => 
            array (
                'permission_id' => 2269,
                'role_id' => 1,
            ),
            86 => 
            array (
                'permission_id' => 2270,
                'role_id' => 1,
            ),
            87 => 
            array (
                'permission_id' => 2271,
                'role_id' => 1,
            ),
            88 => 
            array (
                'permission_id' => 2272,
                'role_id' => 1,
            ),
            89 => 
            array (
                'permission_id' => 2273,
                'role_id' => 1,
            ),
            90 => 
            array (
                'permission_id' => 2274,
                'role_id' => 1,
            ),
            91 => 
            array (
                'permission_id' => 2275,
                'role_id' => 1,
            ),
            92 => 
            array (
                'permission_id' => 2276,
                'role_id' => 1,
            ),
            93 => 
            array (
                'permission_id' => 2277,
                'role_id' => 1,
            ),
            94 => 
            array (
                'permission_id' => 2278,
                'role_id' => 1,
            ),
            95 => 
            array (
                'permission_id' => 2279,
                'role_id' => 1,
            ),
            96 => 
            array (
                'permission_id' => 2280,
                'role_id' => 1,
            ),
            97 => 
            array (
                'permission_id' => 2281,
                'role_id' => 1,
            ),
            98 => 
            array (
                'permission_id' => 2282,
                'role_id' => 1,
            ),
            99 => 
            array (
                'permission_id' => 2283,
                'role_id' => 1,
            ),
            100 => 
            array (
                'permission_id' => 2284,
                'role_id' => 1,
            ),
            101 => 
            array (
                'permission_id' => 2285,
                'role_id' => 1,
            ),
            102 => 
            array (
                'permission_id' => 2286,
                'role_id' => 1,
            ),
            103 => 
            array (
                'permission_id' => 2287,
                'role_id' => 1,
            ),
            104 => 
            array (
                'permission_id' => 2300,
                'role_id' => 1,
            ),
            105 => 
            array (
                'permission_id' => 2301,
                'role_id' => 1,
            ),
            106 => 
            array (
                'permission_id' => 2302,
                'role_id' => 1,
            ),
            107 => 
            array (
                'permission_id' => 2303,
                'role_id' => 1,
            ),
            108 => 
            array (
                'permission_id' => 2304,
                'role_id' => 1,
            ),
            109 => 
            array (
                'permission_id' => 2305,
                'role_id' => 1,
            ),
            110 => 
            array (
                'permission_id' => 2306,
                'role_id' => 1,
            ),
            111 => 
            array (
                'permission_id' => 2307,
                'role_id' => 1,
            ),
            112 => 
            array (
                'permission_id' => 2308,
                'role_id' => 1,
            ),
            113 => 
            array (
                'permission_id' => 2309,
                'role_id' => 1,
            ),
            114 => 
            array (
                'permission_id' => 2310,
                'role_id' => 1,
            ),
            115 => 
            array (
                'permission_id' => 2311,
                'role_id' => 1,
            ),
            116 => 
            array (
                'permission_id' => 2312,
                'role_id' => 1,
            ),
            117 => 
            array (
                'permission_id' => 2313,
                'role_id' => 1,
            ),
            118 => 
            array (
                'permission_id' => 2314,
                'role_id' => 1,
            ),
            119 => 
            array (
                'permission_id' => 2315,
                'role_id' => 1,
            ),
            120 => 
            array (
                'permission_id' => 2316,
                'role_id' => 1,
            ),
            121 => 
            array (
                'permission_id' => 2317,
                'role_id' => 1,
            ),
            122 => 
            array (
                'permission_id' => 2318,
                'role_id' => 1,
            ),
            123 => 
            array (
                'permission_id' => 2319,
                'role_id' => 1,
            ),
            124 => 
            array (
                'permission_id' => 2320,
                'role_id' => 1,
            ),
            125 => 
            array (
                'permission_id' => 2321,
                'role_id' => 1,
            ),
            126 => 
            array (
                'permission_id' => 2322,
                'role_id' => 1,
            ),
            127 => 
            array (
                'permission_id' => 2323,
                'role_id' => 1,
            ),
            128 => 
            array (
                'permission_id' => 2324,
                'role_id' => 1,
            ),
            129 => 
            array (
                'permission_id' => 2325,
                'role_id' => 1,
            ),
            130 => 
            array (
                'permission_id' => 2326,
                'role_id' => 1,
            ),
            131 => 
            array (
                'permission_id' => 2327,
                'role_id' => 1,
            ),
            132 => 
            array (
                'permission_id' => 2328,
                'role_id' => 1,
            ),
            133 => 
            array (
                'permission_id' => 2329,
                'role_id' => 1,
            ),
            134 => 
            array (
                'permission_id' => 2330,
                'role_id' => 1,
            ),
            135 => 
            array (
                'permission_id' => 2331,
                'role_id' => 1,
            ),
            136 => 
            array (
                'permission_id' => 2332,
                'role_id' => 1,
            ),
            137 => 
            array (
                'permission_id' => 2333,
                'role_id' => 1,
            ),
            138 => 
            array (
                'permission_id' => 2334,
                'role_id' => 1,
            ),
            139 => 
            array (
                'permission_id' => 2335,
                'role_id' => 1,
            ),
            140 => 
            array (
                'permission_id' => 2336,
                'role_id' => 1,
            ),
            141 => 
            array (
                'permission_id' => 2337,
                'role_id' => 1,
            ),
            142 => 
            array (
                'permission_id' => 2338,
                'role_id' => 1,
            ),
            143 => 
            array (
                'permission_id' => 2339,
                'role_id' => 1,
            ),
            144 => 
            array (
                'permission_id' => 2340,
                'role_id' => 1,
            ),
            145 => 
            array (
                'permission_id' => 2341,
                'role_id' => 1,
            ),
            146 => 
            array (
                'permission_id' => 2342,
                'role_id' => 1,
            ),
            147 => 
            array (
                'permission_id' => 2343,
                'role_id' => 1,
            ),
            148 => 
            array (
                'permission_id' => 2344,
                'role_id' => 1,
            ),
            149 => 
            array (
                'permission_id' => 2345,
                'role_id' => 1,
            ),
            150 => 
            array (
                'permission_id' => 2346,
                'role_id' => 1,
            ),
            151 => 
            array (
                'permission_id' => 2347,
                'role_id' => 1,
            ),
            152 => 
            array (
                'permission_id' => 2354,
                'role_id' => 1,
            ),
            153 => 
            array (
                'permission_id' => 2355,
                'role_id' => 1,
            ),
            154 => 
            array (
                'permission_id' => 2356,
                'role_id' => 1,
            ),
            155 => 
            array (
                'permission_id' => 2357,
                'role_id' => 1,
            ),
            156 => 
            array (
                'permission_id' => 2358,
                'role_id' => 1,
            ),
            157 => 
            array (
                'permission_id' => 2359,
                'role_id' => 1,
            ),
            158 => 
            array (
                'permission_id' => 2360,
                'role_id' => 1,
            ),
            159 => 
            array (
                'permission_id' => 2361,
                'role_id' => 1,
            ),
            160 => 
            array (
                'permission_id' => 2362,
                'role_id' => 1,
            ),
            161 => 
            array (
                'permission_id' => 2363,
                'role_id' => 1,
            ),
            162 => 
            array (
                'permission_id' => 2364,
                'role_id' => 1,
            ),
            163 => 
            array (
                'permission_id' => 2365,
                'role_id' => 1,
            ),
            164 => 
            array (
                'permission_id' => 2366,
                'role_id' => 1,
            ),
            165 => 
            array (
                'permission_id' => 2367,
                'role_id' => 1,
            ),
            166 => 
            array (
                'permission_id' => 2368,
                'role_id' => 1,
            ),
            167 => 
            array (
                'permission_id' => 2369,
                'role_id' => 1,
            ),
            168 => 
            array (
                'permission_id' => 2370,
                'role_id' => 1,
            ),
            169 => 
            array (
                'permission_id' => 2371,
                'role_id' => 1,
            ),
            170 => 
            array (
                'permission_id' => 2372,
                'role_id' => 1,
            ),
            171 => 
            array (
                'permission_id' => 2373,
                'role_id' => 1,
            ),
            172 => 
            array (
                'permission_id' => 2374,
                'role_id' => 1,
            ),
            173 => 
            array (
                'permission_id' => 2375,
                'role_id' => 1,
            ),
            174 => 
            array (
                'permission_id' => 2376,
                'role_id' => 1,
            ),
            175 => 
            array (
                'permission_id' => 2377,
                'role_id' => 1,
            ),
            176 => 
            array (
                'permission_id' => 2378,
                'role_id' => 1,
            ),
            177 => 
            array (
                'permission_id' => 2379,
                'role_id' => 1,
            ),
            178 => 
            array (
                'permission_id' => 2380,
                'role_id' => 1,
            ),
            179 => 
            array (
                'permission_id' => 2381,
                'role_id' => 1,
            ),
            180 => 
            array (
                'permission_id' => 2382,
                'role_id' => 1,
            ),
            181 => 
            array (
                'permission_id' => 2383,
                'role_id' => 1,
            ),
            182 => 
            array (
                'permission_id' => 2384,
                'role_id' => 1,
            ),
            183 => 
            array (
                'permission_id' => 2385,
                'role_id' => 1,
            ),
            184 => 
            array (
                'permission_id' => 2386,
                'role_id' => 1,
            ),
            185 => 
            array (
                'permission_id' => 2387,
                'role_id' => 1,
            ),
            186 => 
            array (
                'permission_id' => 2388,
                'role_id' => 1,
            ),
            187 => 
            array (
                'permission_id' => 2389,
                'role_id' => 1,
            ),
            188 => 
            array (
                'permission_id' => 2390,
                'role_id' => 1,
            ),
            189 => 
            array (
                'permission_id' => 2391,
                'role_id' => 1,
            ),
            190 => 
            array (
                'permission_id' => 2392,
                'role_id' => 1,
            ),
            191 => 
            array (
                'permission_id' => 2393,
                'role_id' => 1,
            ),
            192 => 
            array (
                'permission_id' => 2394,
                'role_id' => 1,
            ),
            193 => 
            array (
                'permission_id' => 2395,
                'role_id' => 1,
            ),
            194 => 
            array (
                'permission_id' => 2396,
                'role_id' => 1,
            ),
            195 => 
            array (
                'permission_id' => 2397,
                'role_id' => 1,
            ),
            196 => 
            array (
                'permission_id' => 2398,
                'role_id' => 1,
            ),
            197 => 
            array (
                'permission_id' => 2399,
                'role_id' => 1,
            ),
            198 => 
            array (
                'permission_id' => 2400,
                'role_id' => 1,
            ),
            199 => 
            array (
                'permission_id' => 2401,
                'role_id' => 1,
            ),
            200 => 
            array (
                'permission_id' => 2402,
                'role_id' => 1,
            ),
            201 => 
            array (
                'permission_id' => 2403,
                'role_id' => 1,
            ),
            202 => 
            array (
                'permission_id' => 2404,
                'role_id' => 1,
            ),
            203 => 
            array (
                'permission_id' => 2405,
                'role_id' => 1,
            ),
            204 => 
            array (
                'permission_id' => 2406,
                'role_id' => 1,
            ),
            205 => 
            array (
                'permission_id' => 2407,
                'role_id' => 1,
            ),
            206 => 
            array (
                'permission_id' => 2408,
                'role_id' => 1,
            ),
            207 => 
            array (
                'permission_id' => 2409,
                'role_id' => 1,
            ),
            208 => 
            array (
                'permission_id' => 2410,
                'role_id' => 1,
            ),
            209 => 
            array (
                'permission_id' => 2411,
                'role_id' => 1,
            ),
            210 => 
            array (
                'permission_id' => 2412,
                'role_id' => 1,
            ),
            211 => 
            array (
                'permission_id' => 2413,
                'role_id' => 1,
            ),
            212 => 
            array (
                'permission_id' => 2414,
                'role_id' => 1,
            ),
            213 => 
            array (
                'permission_id' => 2415,
                'role_id' => 1,
            ),
            214 => 
            array (
                'permission_id' => 2416,
                'role_id' => 1,
            ),
            215 => 
            array (
                'permission_id' => 2417,
                'role_id' => 1,
            ),
            216 => 
            array (
                'permission_id' => 2418,
                'role_id' => 1,
            ),
            217 => 
            array (
                'permission_id' => 2419,
                'role_id' => 1,
            ),
            218 => 
            array (
                'permission_id' => 2420,
                'role_id' => 1,
            ),
            219 => 
            array (
                'permission_id' => 2421,
                'role_id' => 1,
            ),
            220 => 
            array (
                'permission_id' => 2422,
                'role_id' => 1,
            ),
            221 => 
            array (
                'permission_id' => 2423,
                'role_id' => 1,
            ),
            222 => 
            array (
                'permission_id' => 2424,
                'role_id' => 1,
            ),
            223 => 
            array (
                'permission_id' => 2425,
                'role_id' => 1,
            ),
            224 => 
            array (
                'permission_id' => 2426,
                'role_id' => 1,
            ),
            225 => 
            array (
                'permission_id' => 2427,
                'role_id' => 1,
            ),
            226 => 
            array (
                'permission_id' => 2428,
                'role_id' => 1,
            ),
            227 => 
            array (
                'permission_id' => 2429,
                'role_id' => 1,
            ),
            228 => 
            array (
                'permission_id' => 2430,
                'role_id' => 1,
            ),
            229 => 
            array (
                'permission_id' => 2431,
                'role_id' => 1,
            ),
            230 => 
            array (
                'permission_id' => 2432,
                'role_id' => 1,
            ),
            231 => 
            array (
                'permission_id' => 2433,
                'role_id' => 1,
            ),
            232 => 
            array (
                'permission_id' => 2434,
                'role_id' => 1,
            ),
            233 => 
            array (
                'permission_id' => 2435,
                'role_id' => 1,
            ),
            234 => 
            array (
                'permission_id' => 2436,
                'role_id' => 1,
            ),
            235 => 
            array (
                'permission_id' => 2437,
                'role_id' => 1,
            ),
            236 => 
            array (
                'permission_id' => 2438,
                'role_id' => 1,
            ),
            237 => 
            array (
                'permission_id' => 2439,
                'role_id' => 1,
            ),
            238 => 
            array (
                'permission_id' => 2440,
                'role_id' => 1,
            ),
            239 => 
            array (
                'permission_id' => 2441,
                'role_id' => 1,
            ),
            240 => 
            array (
                'permission_id' => 2442,
                'role_id' => 1,
            ),
            241 => 
            array (
                'permission_id' => 2443,
                'role_id' => 1,
            ),
            242 => 
            array (
                'permission_id' => 2444,
                'role_id' => 1,
            ),
            243 => 
            array (
                'permission_id' => 2445,
                'role_id' => 1,
            ),
            244 => 
            array (
                'permission_id' => 2446,
                'role_id' => 1,
            ),
            245 => 
            array (
                'permission_id' => 2447,
                'role_id' => 1,
            ),
            246 => 
            array (
                'permission_id' => 2448,
                'role_id' => 1,
            ),
            247 => 
            array (
                'permission_id' => 2449,
                'role_id' => 1,
            ),
            248 => 
            array (
                'permission_id' => 2450,
                'role_id' => 1,
            ),
            249 => 
            array (
                'permission_id' => 2451,
                'role_id' => 1,
            ),
            250 => 
            array (
                'permission_id' => 2452,
                'role_id' => 1,
            ),
            251 => 
            array (
                'permission_id' => 2453,
                'role_id' => 1,
            ),
            252 => 
            array (
                'permission_id' => 2454,
                'role_id' => 1,
            ),
            253 => 
            array (
                'permission_id' => 2455,
                'role_id' => 1,
            ),
            254 => 
            array (
                'permission_id' => 2456,
                'role_id' => 1,
            ),
            255 => 
            array (
                'permission_id' => 2457,
                'role_id' => 1,
            ),
            256 => 
            array (
                'permission_id' => 2458,
                'role_id' => 1,
            ),
            257 => 
            array (
                'permission_id' => 2459,
                'role_id' => 1,
            ),
            258 => 
            array (
                'permission_id' => 2460,
                'role_id' => 1,
            ),
            259 => 
            array (
                'permission_id' => 2461,
                'role_id' => 1,
            ),
            260 => 
            array (
                'permission_id' => 2462,
                'role_id' => 1,
            ),
            261 => 
            array (
                'permission_id' => 2463,
                'role_id' => 1,
            ),
            262 => 
            array (
                'permission_id' => 2464,
                'role_id' => 1,
            ),
            263 => 
            array (
                'permission_id' => 2465,
                'role_id' => 1,
            ),
            264 => 
            array (
                'permission_id' => 2466,
                'role_id' => 1,
            ),
            265 => 
            array (
                'permission_id' => 2467,
                'role_id' => 1,
            ),
            266 => 
            array (
                'permission_id' => 2468,
                'role_id' => 1,
            ),
            267 => 
            array (
                'permission_id' => 2469,
                'role_id' => 1,
            ),
            268 => 
            array (
                'permission_id' => 2470,
                'role_id' => 1,
            ),
            269 => 
            array (
                'permission_id' => 2471,
                'role_id' => 1,
            ),
            270 => 
            array (
                'permission_id' => 2472,
                'role_id' => 1,
            ),
            271 => 
            array (
                'permission_id' => 2473,
                'role_id' => 1,
            ),
            272 => 
            array (
                'permission_id' => 2474,
                'role_id' => 1,
            ),
            273 => 
            array (
                'permission_id' => 2475,
                'role_id' => 1,
            ),
            274 => 
            array (
                'permission_id' => 2476,
                'role_id' => 1,
            ),
            275 => 
            array (
                'permission_id' => 2477,
                'role_id' => 1,
            ),
            276 => 
            array (
                'permission_id' => 2478,
                'role_id' => 1,
            ),
            277 => 
            array (
                'permission_id' => 2479,
                'role_id' => 1,
            ),
            278 => 
            array (
                'permission_id' => 2480,
                'role_id' => 1,
            ),
        ));
        
        
    }
}