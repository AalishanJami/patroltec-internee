<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
class ReportCompleteFormTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
           $permission_array = [
					['name' => 'view_reportCompletedForm','group'=>'Report Master', 'guard_name' => 'web','permission_name'=>'view','type'=>'','sub_module_name'=>'operation','module_name'=>'Completed Forms','module_name_id'=>'62'],
					['name' => 'signoff_reportCompletedForm','group'=>'Job Master', 'guard_name' => 'web','permission_name'=>'Signoff','type'=>'','sub_module_name'=>'operation','module_name'=>'Completed Forms','module_name_id'=>'62'],
					['name' => 'show_signoff_reportCompletedForm','group'=>'Job Master', 'guard_name' => 'web','permission_name'=>'Show Signoff','type'=>'','sub_module_name'=>'operation','module_name'=>'Completed Forms','module_name_id'=>'62'],
           					//listing in Contact
					['name' => 'reportCompletedForm_checkbox','group'=>'Report Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Completed Forms','module_name_id'=>'62'], 
					['name' => 'reportCompletedForm_form_name','group'=>'Report Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'Form Name','sub_module_name'=>'listing','module_name'=>'Completed Forms','module_name_id'=>'62'], 
					['name' => 'reportCompletedForm_location','group'=>'Report Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'location','sub_module_name'=>'listing','module_name'=>'Completed Forms','module_name_id'=>'62'],
					['name' => 'reportCompletedForm_location_breakdown','group'=>'Report Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'location breakdown','sub_module_name'=>'listing','module_name'=>'Completed Forms','module_name_id'=>'62'], 
					['name' => 'reportCompletedForm_user','group'=>'Report Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'user','sub_module_name'=>'listing','module_name'=>'Completed Forms','module_name_id'=>'62'],
					['name' => 'reportCompletedForm_date_time','group'=>'Report Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'date time','sub_module_name'=>'listing','module_name'=>'Completed Forms','module_name_id'=>'62'],
					['name' => 'reportCompletedForm_form_type','group'=>'Report Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'Form Type','sub_module_name'=>'listing','module_name'=>'Completed Forms','module_name_id'=>'62'],
					['name' => 'reportCompletedForm_form_score','group'=>'Report Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'Form Score','sub_module_name'=>'listing','module_name'=>'Completed Forms','module_name_id'=>'62'],
					['name' => 'reportCompletedForm_answer_score','group'=>'Report Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'Answer Score','sub_module_name'=>'listing','module_name'=>'Completed Forms','module_name_id'=>'62'],
          
        ];
          foreach ($permission_array as $key => $permission) {
            $admin_role = Role::find(1);
            $getpermission=Permission::where('name',$permission['name'])->first();
            if (empty($getpermission)){
                $permission=Permission::create($permission);
                $admin_role->givePermissionTo($permission['name']);
            }else{
                $permission=Permission::where('name',$getpermission->name)->update($permission);
                $admin_role->givePermissionTo($getpermission->name);
            }
        }
    }
}
