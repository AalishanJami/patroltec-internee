<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
class ReportEmailPermissionTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission_array = [
            // create Contact permissions
            ['name' => 'view_emailReport','group'=>'Report Master', 'guard_name' => 'web','permission_name'=>'View','type'=>'','sub_module_name'=>'operation','module_name'=>'Email Report','module_name_id'=>'59'],
            ['name' => 'word_emailReport', 'group'=>'Report Master','guard_name' => 'web','type'=>'','permission_name'=>'Word Export ','sub_module_name'=>'operation','module_name'=>'Email Report','module_name_id'=>'59'],
            ['name' => 'csv_emailReport','group'=>'Report Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Csv Export','sub_module_name'=>'operation','module_name'=>'Email Report','module_name_id'=>'59'],
            ['name' => 'pdf_emailReport', 'group'=>'Report Master','guard_name' => 'web','type'=>'','permission_name'=>'PDF Export','sub_module_name'=>'operation','module_name'=>'Email Report','module_name_id'=>'59'],

            //listing
            ['name' => 'emailReport_checkbox','group'=>'Report Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Email Report','module_name_id'=>'59'],
            ['name' => 'emailReport_ticketid', 'group'=>'Report Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Ticket Id','sub_module_name'=>'listing','module_name'=>'Email Report','module_name_id'=>'59'],
            ['name' => 'emailReport_username', 'group'=>'Report Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Username','sub_module_name'=>'listing','module_name'=>'Email Report','module_name_id'=>'59'],
            ['name' => 'emailReport_title','group'=>'Report Master', 'guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Title','sub_module_name'=>'listing','module_name'=>'Email Report','module_name_id'=>'59'],
            ['name' => 'emailReport_message', 'group'=>'Report Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Message','sub_module_name'=>'listing','module_name'=>'Email Report','module_name_id'=>'59'],
            ['name' => 'emailReport_senddate', 'group'=>'Report Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Send Date','sub_module_name'=>'listing','module_name'=>'Email Report','module_name_id'=>'59'],

            //create
            ['name' => 'emailReport_checkbox_create','group'=>'Report Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'create','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Email Report','module_name_id'=>'59'],
            ['name' => 'emailReport_ticketid_create', 'group'=>'Report Master','guard_name' => 'web','type'=>'create','enable'=>'0','permission_name'=>'Ticket Id','sub_module_name'=>'listing','module_name'=>'Email Report','module_name_id'=>'59'],
            ['name' => 'emailReport_username_create', 'group'=>'Report Master','guard_name' => 'web','type'=>'create','enable'=>'0','permission_name'=>'Username','sub_module_name'=>'listing','module_name'=>'Email Report','module_name_id'=>'59'],
            ['name' => 'emailReport_title_create','group'=>'Report Master', 'guard_name' => 'web','type'=>'create','enable'=>'0','permission_name'=>'Title','sub_module_name'=>'listing','module_name'=>'Email Report','module_name_id'=>'59'],
            ['name' => 'emailReport_message_create', 'group'=>'Report Master','guard_name' => 'web','type'=>'create','enable'=>'0','permission_name'=>'Message','sub_module_name'=>'listing','module_name'=>'Email Report','module_name_id'=>'59'],
            ['name' => 'emailReport_senddate_create', 'group'=>'Report Master','guard_name' => 'web','type'=>'create','enable'=>'0','permission_name'=>'Send Date','sub_module_name'=>'listing','module_name'=>'Email Report','module_name_id'=>'59'],

            //word
            ['name' => 'emailReport_checkbox_word_export','group'=>'Report Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'word_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Email Report','module_name_id'=>'59'],
            ['name' => 'emailReport_ticketid_word_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Ticket Id','sub_module_name'=>'listing','module_name'=>'Email Report','module_name_id'=>'59'],
            ['name' => 'emailReport_username_word_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Username','sub_module_name'=>'listing','module_name'=>'Email Report','module_name_id'=>'59'],
            ['name' => 'emailReport_title_word_export','group'=>'Report Master', 'guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Title','sub_module_name'=>'listing','module_name'=>'Email Report','module_name_id'=>'59'],
            ['name' => 'emailReport_message_word_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Message','sub_module_name'=>'listing','module_name'=>'Email Report','module_name_id'=>'59'],
            ['name' => 'emailReport_senddate_word_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Send Date','sub_module_name'=>'listing','module_name'=>'Email Report','module_name_id'=>'59'],

            //excel
            ['name' => 'emailReport_checkbox_excel_export','group'=>'Report Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'excel_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Email Report','module_name_id'=>'59'],
            ['name' => 'emailReport_ticketid_excel_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Ticket Id','sub_module_name'=>'listing','module_name'=>'Email Report','module_name_id'=>'59'],
            ['name' => 'emailReport_username_excel_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Username','sub_module_name'=>'listing','module_name'=>'Email Report','module_name_id'=>'59'],
            ['name' => 'emailReport_title_excel_export','group'=>'Report Master', 'guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Title','sub_module_name'=>'listing','module_name'=>'Email Report','module_name_id'=>'59'],
            ['name' => 'emailReport_message_excel_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Message','sub_module_name'=>'listing','module_name'=>'Email Report','module_name_id'=>'59'],
            ['name' => 'emailReport_senddate_excel_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Send Date','sub_module_name'=>'listing','module_name'=>'Email Report','module_name_id'=>'59'],

            //pdf
            ['name' => 'emailReport_checkbox_pdf_export','group'=>'Report Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'pdf_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Email Report','module_name_id'=>'59'],
            ['name' => 'emailReport_ticketid_pdf_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Ticket Id','sub_module_name'=>'listing','module_name'=>'Email Report','module_name_id'=>'59'],
            ['name' => 'emailReport_username_pdf_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Username','sub_module_name'=>'listing','module_name'=>'Email Report','module_name_id'=>'59'],
            ['name' => 'emailReport_title_pdf_export','group'=>'Report Master', 'guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Title','sub_module_name'=>'listing','module_name'=>'Email Report','module_name_id'=>'59'],
            ['name' => 'emailReport_message_pdf_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Message','sub_module_name'=>'listing','module_name'=>'Email Report','module_name_id'=>'59'],
            ['name' => 'emailReport_senddate_pdf_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Send Date','sub_module_name'=>'listing','module_name'=>'Email Report','module_name_id'=>'59'],

            //edit
            ['name' => 'emailReport_checkbox_edit','group'=>'Report Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'edit','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Email Report','module_name_id'=>'59'],
            ['name' => 'emailReport_ticketid_edit', 'group'=>'Report Master','guard_name' => 'web','type'=>'edit','enable'=>'0','permission_name'=>'Ticket Id','sub_module_name'=>'listing','module_name'=>'Email Report','module_name_id'=>'59'],
            ['name' => 'emailReport_username_edit', 'group'=>'Report Master','guard_name' => 'web','type'=>'edit','enable'=>'0','permission_name'=>'Username','sub_module_name'=>'listing','module_name'=>'Email Report','module_name_id'=>'59'],
            ['name' => 'emailReport_title_edit','group'=>'Report Master', 'guard_name' => 'web','type'=>'edit','enable'=>'0','permission_name'=>'Title','sub_module_name'=>'listing','module_name'=>'Email Report','module_name_id'=>'59'],
            ['name' => 'emailReport_message_edit', 'group'=>'Report Master','guard_name' => 'web','type'=>'edit','enable'=>'0','permission_name'=>'Message','sub_module_name'=>'listing','module_name'=>'Email Report','module_name_id'=>'59'],
            ['name' => 'emailReport_senddate_edit', 'group'=>'Report Master','guard_name' => 'web','type'=>'edit','enable'=>'0','permission_name'=>'Send Date','sub_module_name'=>'listing','module_name'=>'Email Report','module_name_id'=>'59'],
        ];
        foreach ($permission_array as $key => $permission) {
            $admin_role = Role::find(1);
            $getpermission=Permission::where('name',$permission['name'])->first();
            if (empty($getpermission)){
                $permission=Permission::create($permission);
                $admin_role->givePermissionTo($permission['name']);
            }else{
                $permission=Permission::where('name',$getpermission->name)->update($permission);
                $admin_role->givePermissionTo($getpermission->name);
            }
        }
    }
}
