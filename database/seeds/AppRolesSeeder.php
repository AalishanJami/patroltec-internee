<?php

use Illuminate\Database\Seeder;

class AppRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('app_roles')->insert(array (
        	0 => 
            array (
                'id' => 1,
                'name' => 'Full Access',
                'active' => 1,
                'deleted' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Limited Access',
                'active' => 1,
                'deleted' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
    }
}
