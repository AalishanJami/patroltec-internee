<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class EquipmentGroupPermissionTableDataSeeder extends Seeder
{
    public function run()
    {
        $permission_array = [
            // create Contact permissions
            ['name' => 'view_equipmentGroup','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'view','type'=>'','sub_module_name'=>'operation','module_name'=>'Equipment Group'],
            ['name' => 'create_equipmentGroup','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'create','type'=>'','sub_module_name'=>'operation','module_name'=>'Equipment Group'],
            ['name' => 'edit_equipmentGroup', 'group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'edit','type'=>'','sub_module_name'=>'operation','module_name'=>'Equipment Group'],
            ['name' => 'delete_equipmentGroup','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Equipment Group'],
            ['name' => 'selected_delete_equipmentGroup','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'Selected Delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Equipment Group'],
            ['name' => 'selected_restore_delete_equipmentGroup', 'group'=>'Project Master','guard_name' => 'web','type'=>'','permission_name'=>'Selected Restore','sub_module_name'=>'operation','module_name'=>'Equipment Group'],
            ['name' => 'restore_delete_equipmentGroup','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Restore Delete','sub_module_name'=>'operation','module_name'=>'Equipment Group'],
            ['name' => 'selected_active_button_equipmentGroup','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'Selected Active Button','type'=>'','sub_module_name'=>'operation','module_name'=>'Equipment Group'],
            ['name' => 'hard_delete_equipmentGroup','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Hard Delete','sub_module_name'=>'operation','module_name'=>'Equipment Group'],
            ['name' => 'active_equipmentGroup','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Active Equipment Group','sub_module_name'=>'operation','module_name'=>'Equipment Group'],
            ['name' => 'word_equipmentGroup', 'group'=>'Project Master','guard_name' => 'web','type'=>'','permission_name'=>'Word Export Equipment Group','sub_module_name'=>'operation','module_name'=>'Equipment Group'],
            ['name' => 'csv_equipmentGroup','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Csv Export Equipment Group','sub_module_name'=>'operation','module_name'=>'Equipment Group'],
            ['name' => 'pdf_equipmentGroup', 'group'=>'Project Master','guard_name' => 'web','type'=>'','permission_name'=>'PDF Export Equipment Group','sub_module_name'=>'operation','module_name'=>'Equipment Group'],

            //listing in Contact
            ['name' => 'equipmentGroup_checkbox','group'=>'Project Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_qr', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Qr','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_name','group'=>'Project Master', 'guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_equipement', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Equipment','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_serialNumber', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Serial Number','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_make', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Make','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_model', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Model','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_lastserviceDate', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Last Service date','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_nextserviceDate', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Next Service date','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_condition', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Condition','sub_module_name'=>'listing','module_name'=>'Equipment Group'],

            //word in Contact
            ['name' => 'equipmentGroup_checkbox_word_export','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'word_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_qr_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Qr','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_name_word_export','group'=>'Project Master', 'guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_equipement_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Equipment','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_serialNumber_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Serial Number','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_make_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Make','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_model_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Model','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_lastserviceDate_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Last Service date','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_nextserviceDate_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Next Service date','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_condition_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Condition','sub_module_name'=>'listing','module_name'=>'Equipment Group'],

            //excel in Contact
            ['name' => 'equipmentGroup_checkbox_excel_export','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'excel_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_qr_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Qr','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_name_excel_export','group'=>'Project Master', 'guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_equipement_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Equipment','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_serialNumber_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Serial Number','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_make_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Make','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_model_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Model','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_lastserviceDate_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Last Service date','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_nextserviceDate_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Next Service date','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_condition_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Condition','sub_module_name'=>'listing','module_name'=>'Equipment Group'],

            //pdf in Contact
            ['name' => 'equipmentGroup_checkbox_pdf_export','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'pdf_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_qr_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Qr','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_name_pdf_export','group'=>'Project Master', 'guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_equipement_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Equipment','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_serialNumber_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Serial Number','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_make_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Make','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_model_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Model','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_lastserviceDate_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Last Service date','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_nextserviceDate_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Next Service date','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_condition_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Condition','sub_module_name'=>'listing','module_name'=>'Equipment Group'],

            //create in Contact
            ['name' => 'equipmentGroup_checkbox_create','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'create','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_qr_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Qr','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_name_create','group'=>'Project Master', 'guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_equipement_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Equipment','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_serialNumber_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Serial Number','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_make_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Make','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_model_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Model','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_lastserviceDate_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Last Service date','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_nextserviceDate_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Next Service date','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_condition_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Condition','sub_module_name'=>'listing','module_name'=>'Equipment Group'],

            //edit in Contact
            ['name' => 'equipmentGroup_checkbox_edit','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'edit','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_qr_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Qr','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_name_edit','group'=>'Project Master', 'guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_equipement_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Equipment','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_serialNumber_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Serial Number','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_make_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Make','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_model_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Model','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_lastserviceDate_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Last Service date','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_nextserviceDate_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Next Service date','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
            ['name' => 'equipmentGroup_condition_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Condition','sub_module_name'=>'listing','module_name'=>'Equipment Group'],
        ];

        foreach ($permission_array as $key => $permission) {
            $admin_role = Role::find(1);
            $getpermission=Permission::where('name',$permission['name'])->first();
            if (empty($getpermission)){
                $permission=Permission::create($permission);
                $admin_role->givePermissionTo($permission['name']);
            }else{
                $permission=Permission::where('name',$getpermission->name)->update($permission);
                $admin_role->givePermissionTo($getpermission->name);
            }
        }
    }
}
