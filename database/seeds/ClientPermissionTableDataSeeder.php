<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
class ClientPermissionTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission_array = [
            // create Contact permissions
            ['name' => 'view_client','group'=>'Client Master', 'guard_name' => 'web','permission_name'=>'view','type'=>'','sub_module_name'=>'operation','module_name'=>'Client'],
            ['name' => 'create_client','group'=>'Client Master', 'guard_name' => 'web','permission_name'=>'create','type'=>'','sub_module_name'=>'operation','module_name'=>'Client'],
            ['name' => 'edit_client', 'group'=>'Client Master', 'guard_name' => 'web','permission_name'=>'edit','type'=>'','sub_module_name'=>'operation','module_name'=>'Client'],
            ['name' => 'delete_client','group'=>'Client Master', 'guard_name' => 'web','permission_name'=>'delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Client'],
            ['name' => 'selected_delete_client','group'=>'Client Master', 'guard_name' => 'web','permission_name'=>'Selected Delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Client'],
            ['name' => 'selected_restore_delete_client', 'group'=>'Client Master','guard_name' => 'web','type'=>'','permission_name'=>'Selected Restore','sub_module_name'=>'operation','module_name'=>'Client'],
            ['name' => 'restore_delete_client','group'=>'Client Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Restore Delete','sub_module_name'=>'operation','module_name'=>'Client'],
            ['name' => 'hard_delete_client','group'=>'Client Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Hard Delete','sub_module_name'=>'operation','module_name'=>'Client'],
            ['name' => 'active_client','group'=>'Client Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Active Client','sub_module_name'=>'operation','module_name'=>'Client'],
            ['name' => 'slected_active_client','group'=>'Client Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Selected Active Client','sub_module_name'=>'operation','module_name'=>'Client'],
            ['name' => 'word_client', 'group'=>'Client Master','guard_name' => 'web','type'=>'','permission_name'=>'Word Export Client','sub_module_name'=>'operation','module_name'=>'Client'],
            ['name' => 'csv_client','group'=>'Client Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Csv Export Client','sub_module_name'=>'operation','module_name'=>'Client'],
            ['name' => 'pdf_client', 'group'=>'Client Master','guard_name' => 'web','type'=>'','permission_name'=>'PDF Export Client','sub_module_name'=>'operation','module_name'=>'Client'],

            //listing in Contact
            ['name' => 'client_checkbox','group'=>'Client Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Client'],
            ['name' => 'client_name','group'=>'Client Master', 'guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Client'],
            ['name' => 'client_firstname', 'group'=>'Client Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'First Name','sub_module_name'=>'listing','module_name'=>'Client'],
            ['name' => 'client_surname', 'group'=>'Client Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Surname','sub_module_name'=>'listing','module_name'=>'Client'],
            ['name' => 'client_phone', 'group'=>'Client Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Phone','sub_module_name'=>'listing','module_name'=>'Client'],
            ['name' => 'client_email', 'group'=>'Client Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Email','sub_module_name'=>'listing','module_name'=>'Client'],
            ['name' => 'client_address', 'group'=>'Client Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Address','sub_module_name'=>'listing','module_name'=>'Client'],

            //word in Contact
            ['name' => 'client_checkbox_word_export','group'=>'Client Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'word_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Client'],
            ['name' => 'client_name_word_export','group'=>'Client Master', 'guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Client'],
            ['name' => 'client_firstname_word_export', 'group'=>'Client Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'First Name','sub_module_name'=>'listing','module_name'=>'Client'],
            ['name' => 'client_surname_word_export', 'group'=>'Client Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Surname','sub_module_name'=>'listing','module_name'=>'Client'],
            ['name' => 'client_phone_word_export', 'group'=>'Client Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Phone','sub_module_name'=>'listing','module_name'=>'Client'],
            ['name' => 'client_email_word_export', 'group'=>'Client Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Email','sub_module_name'=>'listing','module_name'=>'Client'],
            ['name' => 'client_address_word_export', 'group'=>'Client Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Address','sub_module_name'=>'listing','module_name'=>'Client'],

            //excel in Contact
            ['name' => 'client_checkbox_excel_export','group'=>'Client Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'excel_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Client'],
            ['name' => 'client_name_excel_export','group'=>'Client Master', 'guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Client'],
            ['name' => 'client_firstname_excel_export', 'group'=>'Client Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'First Name','sub_module_name'=>'listing','module_name'=>'Client'],
            ['name' => 'client_surname_excel_export', 'group'=>'Client Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Surname','sub_module_name'=>'listing','module_name'=>'Client'],
            ['name' => 'client_phone_excel_export', 'group'=>'Client Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Phone','sub_module_name'=>'listing','module_name'=>'Client'],
            ['name' => 'client_email_excel_export', 'group'=>'Client Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Email','sub_module_name'=>'listing','module_name'=>'Client'],
            ['name' => 'client_address_excel_export', 'group'=>'Client Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Address','sub_module_name'=>'listing','module_name'=>'Client'],

            //pdf in Contact
            ['name' => 'client_checkbox_pdf_export','group'=>'Client Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'pdf_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Client'],
            ['name' => 'client_name_pdf_export','group'=>'Client Master', 'guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Client'],
            ['name' => 'client_firstname_pdf_export', 'group'=>'Client Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'First Name','sub_module_name'=>'listing','module_name'=>'Client'],
            ['name' => 'client_surname_pdf_export', 'group'=>'Client Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Surname','sub_module_name'=>'listing','module_name'=>'Client'],
            ['name' => 'client_phone_pdf_export', 'group'=>'Client Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Phone','sub_module_name'=>'listing','module_name'=>'Client'],
            ['name' => 'client_email_pdf_export', 'group'=>'Client Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Email','sub_module_name'=>'listing','module_name'=>'Client'],
            ['name' => 'client_address_pdf_export', 'group'=>'Client Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Address','sub_module_name'=>'listing','module_name'=>'Client'],

            //create in Contact
            ['name' => 'client_checkbox_create','group'=>'Client Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'create','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Client'],
            ['name' => 'client_name_create','group'=>'Client Master', 'guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Client'],
            ['name' => 'client_firstname_create', 'group'=>'Client Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'First Name','sub_module_name'=>'listing','module_name'=>'Client'],
            ['name' => 'client_surname_create', 'group'=>'Client Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Surname','sub_module_name'=>'listing','module_name'=>'Client'],
            ['name' => 'client_phone_create', 'group'=>'Client Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Phone','sub_module_name'=>'listing','module_name'=>'Client'],
            ['name' => 'client_email_create', 'group'=>'Client Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Email','sub_module_name'=>'listing','module_name'=>'Client'],
            ['name' => 'client_address_create', 'group'=>'Client Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Address','sub_module_name'=>'listing','module_name'=>'Client'],

            //edit in Contact
            ['name' => 'client_checkbox_edit','group'=>'Client Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'edit','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Client'],
            ['name' => 'client_name_edit','group'=>'Client Master', 'guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Client'],
            ['name' => 'client_firstname_edit', 'group'=>'Client Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'First Name','sub_module_name'=>'listing','module_name'=>'Client'],
            ['name' => 'client_surname_edit', 'group'=>'Client Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Surname','sub_module_name'=>'listing','module_name'=>'Client'],
            ['name' => 'client_phone_edit', 'group'=>'Client Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Phone','sub_module_name'=>'listing','module_name'=>'Client'],
            ['name' => 'client_email_edit', 'group'=>'Client Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Email','sub_module_name'=>'listing','module_name'=>'Client'],
            ['name' => 'client_address_edit', 'group'=>'Client Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Address','sub_module_name'=>'listing','module_name'=>'Client'],
        ];

        foreach ($permission_array as $key => $permission) {
            $admin_role = Role::find(1);
            $getpermission=Permission::where('name',$permission['name'])->first();
            if (empty($getpermission)){
                $permission=Permission::create($permission);
                $admin_role->givePermissionTo($permission['name']);
            }else{
                $permission=Permission::where('name',$getpermission->name)->update($permission);
                $admin_role->givePermissionTo($getpermission->name);
            }
        }
    }
}
