<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class EmployeeCompleteFormTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $permission_array = [
					['name' => 'view_employeeCompletedForm','group'=>'Employee Master', 'guard_name' => 'web','permission_name'=>'view','type'=>'','sub_module_name'=>'operation','module_name'=>'Completed Forms','module_name_id'=>'62'],
					['name' => 'signoff_employeeCompletedForm','group'=>'Job Master', 'guard_name' => 'web','permission_name'=>'Signoff','type'=>'','sub_module_name'=>'operation','module_name'=>'Completed Forms','module_name_id'=>'62'],
					['name' => 'show_signoff_employeeCompletedForm','group'=>'Job Master', 'guard_name' => 'web','permission_name'=>'Show Signoff','type'=>'','sub_module_name'=>'operation','module_name'=>'Completed Forms','module_name_id'=>'62'],
           					//listing in Contact
					['name' => 'employeeCompletedForm_checkbox','group'=>'Employee Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Completed Forms','module_name_id'=>'62'], 
					['name' => 'employeeCompletedForm_form_name','group'=>'Employee Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'Form Name','sub_module_name'=>'listing','module_name'=>'Completed Forms','module_name_id'=>'62'], 
					['name' => 'employeeCompletedForm_location','group'=>'Employee Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'location','sub_module_name'=>'listing','module_name'=>'Completed Forms','module_name_id'=>'62'],
					['name' => 'employeeCompletedForm_location_breakdown','group'=>'Employee Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'location breakdown','sub_module_name'=>'listing','module_name'=>'Completed Forms','module_name_id'=>'62'], 
					['name' => 'employeeCompletedForm_user','group'=>'Employee Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'user','sub_module_name'=>'listing','module_name'=>'Completed Forms','module_name_id'=>'62'],
					['name' => 'employeeCompletedForm_date_time','group'=>'Employee Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'date time','sub_module_name'=>'listing','module_name'=>'Completed Forms','module_name_id'=>'62'],
					['name' => 'employeeCompletedForm_form_type','group'=>'Employee Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'Form Type','sub_module_name'=>'listing','module_name'=>'Completed Forms','module_name_id'=>'62'],
					['name' => 'employeeCompletedForm_form_score','group'=>'Employee Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'Form Score','sub_module_name'=>'listing','module_name'=>'Completed Forms','module_name_id'=>'62'],
					['name' => 'employeeCompletedForm_answer_score','group'=>'Employee Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'Answer Score','sub_module_name'=>'listing','module_name'=>'Completed Forms','module_name_id'=>'62'],
          
        ];
          foreach ($permission_array as $key => $permission) {
            $admin_role = Role::find(1);
            $getpermission=Permission::where('name',$permission['name'])->first();
            if (empty($getpermission)){
                $permission=Permission::create($permission);
                $admin_role->givePermissionTo($permission['name']);
            }else{
                $permission=Permission::where('name',$getpermission->name)->update($permission);
                $admin_role->givePermissionTo($getpermission->name);
            }
        }
    }
}
