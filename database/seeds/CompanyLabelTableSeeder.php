<?php

use Illuminate\Database\Seeder;
use App\CompanyLabel;

class CompanyLabelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $company_labels = [
		  ['id' => '3','label_id' => '4','company_id' => '1','value' => 'Actions..!','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2020-01-01 13:32:21'],
		  ['id' => '4','label_id' => '1','company_id' => '1','value' => 'checkbox','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2019-12-27 06:17:58'],
		  ['id' => '5','label_id' => '3','company_id' => '1','value' => '<p>Emails</p>','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2019-12-31 13:25:47'],
		  ['id' => '6','label_id' => '2','company_id' => '1','value' => '<p>Name</p>','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2020-01-02 11:36:34'],
		  ['id' => '7','label_id' => '5','company_id' => '1','value' => 'Search','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2019-12-27 06:21:52'],
		  ['id' => '8','label_id' => '6','company_id' => '1','value' => 'Lists..!','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2020-01-03 06:16:43'],
		  ['id' => '9','label_id' => '7','company_id' => '1','value' => 'Entries','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2020-01-02 11:41:00'],
		  ['id' => '10','label_id' => '8','company_id' => '1','value' => 'Showing Pages..!','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2020-01-03 06:16:50'],
		  ['id' => '11','label_id' => '9','company_id' => '1','value' => 'Previous','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2020-01-01 04:32:57'],
		  ['id' => '12','label_id' => '10','company_id' => '1','value' => 'Next','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2019-12-27 06:22:59'],
		  ['id' => '13','label_id' => '11','company_id' => '1','value' => 'Delete Selected Users btn','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2020-01-03 06:16:56'],
		  ['id' => '14','label_id' => '12','company_id' => '1','value' => 'Restore Deleted Users','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2019-12-31 13:35:42'],
		  ['id' => '15','label_id' => '13','company_id' => '1','value' => 'to','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2019-12-31 12:06:11'],
		  ['id' => '16','label_id' => '14','company_id' => '1','value' => 'Show Active Users','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2019-12-26 22:55:49'],
		  ['id' => '17','label_id' => '15','company_id' => '1','value' => 'Nothing Found Sorry&nbsp;','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2019-12-27 06:22:43'],
		  ['id' => '18','label_id' => '16','company_id' => '1','value' => 'No records availables.','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2019-12-27 06:22:51'],
		  ['id' => '19','label_id' => '17','company_id' => '1','value' => 'checkbox','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '20','label_id' => '18','company_id' => '1','value' => 'Name','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '21','label_id' => '19','company_id' => '1','value' => 'Emails','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2019-12-27 06:17:03'],
		  ['id' => '22','label_id' => '20','company_id' => '1','value' => 'Actions','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '23','label_id' => '21','company_id' => '1','value' => 'Search','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '24','label_id' => '22','company_id' => '1','value' => 'Show','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '25','label_id' => '23','company_id' => '1','value' => 'Entries..!','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2019-12-26 23:19:44'],
		  ['id' => '26','label_id' => '24','company_id' => '1','value' => 'Showing Page ..in french','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2019-12-31 15:06:19'],
		  ['id' => '27','label_id' => '25','company_id' => '1','value' => 'Previous','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2019-12-27 06:24:08'],
		  ['id' => '28','label_id' => '26','company_id' => '1','value' => 'Next','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '29','label_id' => '27','company_id' => '1','value' => 'Delete Selected Users','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '30','label_id' => '28','company_id' => '1','value' => 'Restore Deleted Users','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '31','label_id' => '29','company_id' => '1','value' => '&nbsp;Of&nbsp;','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2019-12-26 23:19:08'],
		  ['id' => '32','label_id' => '30','company_id' => '1','value' => 'SHow Active Users','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '33','label_id' => '31','company_id' => '1','value' => 'Nothng Found Soory','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2019-12-26 23:19:36'],
		  ['id' => '34','label_id' => '32','company_id' => '1','value' => 'No Records Availabe','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2019-12-26 23:19:31'],
		  ['id' => '35','label_id' => '33','company_id' => '1','value' => 'dashboard','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '36','label_id' => '34','company_id' => '1','value' => 'project','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '37','label_id' => '35','company_id' => '1','value' => 'document_library','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '38','label_id' => '36','company_id' => '1','value' => 'employee_library','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '39','label_id' => '37','company_id' => '1','value' => 'employees','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '40','label_id' => '38','company_id' => '1','value' => 'required_documents','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '41','label_id' => '39','company_id' => '1','value' => 'jobs','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '42','label_id' => '40','company_id' => '1','value' => 'clear_up_notices','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '43','label_id' => '41','company_id' => '1','value' => 'Audit Log..!','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2020-01-01 10:10:27'],
		  ['id' => '44','label_id' => '42','company_id' => '1','value' => 'app_cms','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '45','label_id' => '43','company_id' => '1','value' => 'permissions','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '46','label_id' => '44','company_id' => '1','value' => 'reports','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '48','label_id' => '46','company_id' => '1','value' => 'Jobs testing','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2019-12-31 15:05:50'],
		  ['id' => '49','label_id' => '48','company_id' => '1','value' => 'main details','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2019-12-31 12:58:11'],
		  ['id' => '50','label_id' => '49','company_id' => '1','value' => 'contacts','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '51','label_id' => '50','company_id' => '1','value' => 'plant_and_equipment','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '52','label_id' => '51','company_id' => '1','value' => 'qr_list','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '53','label_id' => '52','company_id' => '1','value' => 'dynamic_form','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '54','label_id' => '53','company_id' => '1','value' => 'static_form_upload','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '55','label_id' => '54','company_id' => '1','value' => 'uploaded_jobs','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '56','label_id' => '55','company_id' => '1','value' => 'version_log','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '57','label_id' => '56','company_id' => '1','value' => 'contact_information','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '58','label_id' => '57','company_id' => '1','value' => 'next_of_kin','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '59','label_id' => '58','company_id' => '1','value' => 'account','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '60','label_id' => '59','company_id' => '1','value' => 'sia_licence','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '61','label_id' => '60','company_id' => '1','value' => 'documents','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '62','label_id' => '61','company_id' => '1','value' => 'user_permissions_tab','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '63','label_id' => '62','company_id' => '1','value' => 'completed_forms','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '64','label_id' => '63','company_id' => '1','value' => 'completed_lists','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '65','label_id' => '64','company_id' => '1','value' => 'fors_database','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '66','label_id' => '65','company_id' => '1','value' => 'cloc_database','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '67','label_id' => '66','company_id' => '1','value' => 'sia_database','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '68','label_id' => '67','company_id' => '1','value' => 'minimize_menu','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '69','label_id' => '68','company_id' => '1','value' => 'Schedule','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '70','label_id' => '69','company_id' => '1','value' => 'site dashBoard','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '71','label_id' => '70','company_id' => '1','value' => 'dashboard','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '72','label_id' => '71','company_id' => '1','value' => 'project','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '73','label_id' => '72','company_id' => '1','value' => 'document_library','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '74','label_id' => '73','company_id' => '1','value' => 'employee_library','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '75','label_id' => '74','company_id' => '1','value' => 'employees','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '76','label_id' => '75','company_id' => '1','value' => 'required_documents','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '77','label_id' => '76','company_id' => '1','value' => 'jobs','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '78','label_id' => '77','company_id' => '1','value' => 'clear_up_notices','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '79','label_id' => '78','company_id' => '1','value' => 'audit_log','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '80','label_id' => '79','company_id' => '1','value' => 'app_cms','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '81','label_id' => '80','company_id' => '1','value' => 'permissions','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '82','label_id' => '81','company_id' => '1','value' => 'reports','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '83','label_id' => '82','company_id' => '1','value' => 'jobs','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '84','label_id' => '83','company_id' => '1','value' => 'main_details','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '85','label_id' => '84','company_id' => '1','value' => 'contacts','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '86','label_id' => '85','company_id' => '1','value' => 'plant_and_equipment','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '87','label_id' => '86','company_id' => '1','value' => 'qr_list','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '88','label_id' => '87','company_id' => '1','value' => 'dynamic_form','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '89','label_id' => '88','company_id' => '1','value' => 'static_form_upload','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '90','label_id' => '89','company_id' => '1','value' => 'uploaded_jobs','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '91','label_id' => '90','company_id' => '1','value' => 'version_log','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '92','label_id' => '91','company_id' => '1','value' => 'contact information','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2019-12-31 00:07:15'],
		  ['id' => '93','label_id' => '92','company_id' => '1','value' => 'next_of_kin','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '94','label_id' => '93','company_id' => '1','value' => 'account','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '95','label_id' => '94','company_id' => '1','value' => 'sia_licence','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '96','label_id' => '95','company_id' => '1','value' => 'documents','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '97','label_id' => '96','company_id' => '1','value' => 'user_permissions_tab','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '98','label_id' => '97','company_id' => '1','value' => 'completed_forms','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '99','label_id' => '98','company_id' => '1','value' => 'completed_lists','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '100','label_id' => '99','company_id' => '1','value' => 'fors_database','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '101','label_id' => '100','company_id' => '1','value' => 'cloc_database','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '102','label_id' => '101','company_id' => '1','value' => 'sia_database','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '103','label_id' => '102','company_id' => '1','value' => 'minimize_menu','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '104','label_id' => '103','company_id' => '1','value' => 'schedule','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '105','label_id' => '104','company_id' => '1','value' => 'site_dashBoard','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '106','label_id' => '105','company_id' => '1','value' => 'Export Excel','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2020-01-02 11:36:04'],
		  ['id' => '107','label_id' => '106','company_id' => '1','value' => 'Export Excel','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '108','label_id' => '107','company_id' => '1','value' => 'Edit','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2020-01-02 11:37:16'],
		  ['id' => '109','label_id' => '108','company_id' => '1','value' => 'Choose Company..!','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2020-01-02 11:38:04'],
		  ['id' => '110','label_id' => '109','company_id' => '1','value' => 'edit','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '111','label_id' => '110','company_id' => '1','value' => 'choose_company','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '112','label_id' => '111','company_id' => '1','value' => 'Update','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2020-01-02 11:38:13'],
		  ['id' => '113','label_id' => '112','company_id' => '1','value' => 'Update Password','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2020-01-01 03:28:55'],
		  ['id' => '114','label_id' => '113','company_id' => '1','value' => 'update','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '115','label_id' => '114','company_id' => '1','value' => 'update_password','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '116','label_id' => '115','company_id' => '1','value' => 'old_values','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '117','label_id' => '116','company_id' => '1','value' => 'new_values','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '118','label_id' => '117','company_id' => '1','value' => 'Ip Address','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2020-01-01 04:32:10'],
		  ['id' => '119','label_id' => '118','company_id' => '1','value' => 'url','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '120','label_id' => '119','company_id' => '1','value' => 'user_agent','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '121','label_id' => '120','company_id' => '1','value' => 'old_values','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '122','label_id' => '121','company_id' => '1','value' => 'new_values','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '123','label_id' => '122','company_id' => '1','value' => 'ip_address','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '124','label_id' => '123','company_id' => '1','value' => 'url','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '125','label_id' => '124','company_id' => '1','value' => 'user_agent','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '126','label_id' => '125','company_id' => '1','value' => 'New User','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2020-01-01 04:43:12'],
		  ['id' => '127','label_id' => '126','company_id' => '1','value' => 'new_user','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '128','label_id' => '127','company_id' => '1','value' => 'Save','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2020-01-02 11:39:08'],
		  ['id' => '129','label_id' => '128','company_id' => '1','value' => 'save','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '130','label_id' => '129','company_id' => '1','value' => 'Your Name','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2020-01-01 04:54:53'],
		  ['id' => '131','label_id' => '130','company_id' => '1','value' => 'your_name','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '132','label_id' => '131','company_id' => '1','value' => 'Confirm Password','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2020-01-01 05:06:05'],
		  ['id' => '133','label_id' => '132','company_id' => '1','value' => 'Password','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2020-01-01 05:02:12'],
		  ['id' => '134','label_id' => '133','company_id' => '1','value' => 'Your Email..!','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2020-01-02 11:37:27'],
		  ['id' => '135','label_id' => '134','company_id' => '1','value' => 'Choose_company','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '136','label_id' => '135','company_id' => '1','value' => 'confirm_password','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '137','label_id' => '136','company_id' => '1','value' => 'password','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '138','label_id' => '137','company_id' => '1','value' => 'your_email','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '139','label_id' => '138','company_id' => '1','value' => 'Choose_company','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '140','label_id' => '139','company_id' => '1','value' => 'Manage Users..!','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2020-01-02 11:39:20'],
		  ['id' => '141','label_id' => '140','company_id' => '1','value' => 'manage_users','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '142','label_id' => '141','company_id' => '1','value' => 'Dashboard','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2020-01-01 11:21:42'],
		  ['id' => '143','label_id' => '142','company_id' => '1','value' => 'dashboard','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '144','label_id' => '143','company_id' => '1','value' => 'Close','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2020-01-01 11:52:52'],
		  ['id' => '145','label_id' => '144','company_id' => '1','value' => 'Select Groups','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2020-01-01 10:23:39'],
		  ['id' => '146','label_id' => '145','company_id' => '1','value' => 'close','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '147','label_id' => '146','company_id' => '1','value' => 'select_groups','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '148','label_id' => '147','company_id' => '1','value' => 'Delete Selected Companies','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2020-01-01 12:13:42'],
		  ['id' => '149','label_id' => '148','company_id' => '1','value' => 'Restore Deleted Companies','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2020-01-01 11:42:14'],
		  ['id' => '150','label_id' => '149','company_id' => '1','value' => 'Show Active Companies','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2020-01-01 13:02:08'],
		  ['id' => '151','label_id' => '150','company_id' => '1','value' => 'edit_role','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '152','label_id' => '151','company_id' => '1','value' => 'New Role','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2020-01-01 11:53:33'],
		  ['id' => '153','label_id' => '152','company_id' => '1','value' => 'Role Name','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2020-01-01 11:53:42'],
		  ['id' => '154','label_id' => '153','company_id' => '1','value' => 'Delete_Selected_Companies','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '155','label_id' => '154','company_id' => '1','value' => 'Restore_Deleted_Companies','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '156','label_id' => '155','company_id' => '1','value' => 'show_active_Companies','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '157','label_id' => '156','company_id' => '1','value' => 'edit_role','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '158','label_id' => '157','company_id' => '1','value' => 'new_role','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '159','label_id' => '158','company_id' => '1','value' => 'role_name','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '160','label_id' => '159','company_id' => '1','value' => 'select permissions','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2020-01-02 11:42:14'],
		  ['id' => '161','label_id' => '160','company_id' => '1','value' => 'select_permissions','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '162','label_id' => '161','company_id' => '1','value' => 'Manage Roles','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2020-01-01 12:05:55'],
		  ['id' => '163','label_id' => '162','company_id' => '1','value' => 'manage_roles','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '164','label_id' => '163','company_id' => '1','value' => 'Manage Companies','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2020-01-01 12:48:56'],
		  ['id' => '165','label_id' => '164','company_id' => '1','value' => 'manage_companies','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '166','label_id' => '165','company_id' => '1','value' => 'New Company','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2020-01-01 12:58:42'],
		  ['id' => '167','label_id' => '166','company_id' => '1','value' => 'Company Name','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2020-01-01 12:55:40'],
		  ['id' => '168','label_id' => '167','company_id' => '1','value' => 'edit_company','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '169','label_id' => '168','company_id' => '1','value' => 'new_company','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '170','label_id' => '169','company_id' => '1','value' => 'company_name','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '171','label_id' => '170','company_id' => '1','value' => 'edit_company','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '172','label_id' => '171','company_id' => '1','value' => 'create..','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2020-01-01 13:29:29'],
		  ['id' => '173','label_id' => '172','company_id' => '1','value' => 'edit..','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2020-01-01 13:29:41'],
		  ['id' => '174','label_id' => '173','company_id' => '1','value' => 'Delete btn','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2020-01-02 11:39:48'],
		  ['id' => '175','label_id' => '174','company_id' => '1','value' => 'Export Excel','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2020-01-01 13:29:51'],
		  ['id' => '176','label_id' => '175','company_id' => '1','value' => 'create','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '177','label_id' => '176','company_id' => '1','value' => 'edit','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '178','label_id' => '178','company_id' => '1','value' => 'delete','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '179','label_id' => '179','company_id' => '1','value' => 'export_excel','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '180','label_id' => '179','company_id' => '1','value' => 'Are You Sure..?','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2020-01-02 11:39:34'],
		  ['id' => '181','label_id' => '180','company_id' => '1','value' => 'Delete Record','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2020-01-02 11:39:41'],
		  ['id' => '182','label_id' => '181','company_id' => '1','value' => 'cancelled','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '183','label_id' => '182','company_id' => '1','value' => 'your_record_is_safe','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '184','label_id' => '183','company_id' => '1','value' => 'are_you_sure?','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '185','label_id' => '184','company_id' => '1','value' => 'delete_record.','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '186','label_id' => '185','company_id' => '1','value' => 'cancelled','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '187','label_id' => '186','company_id' => '1','value' => 'your_record_is_safe','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '188','label_id' => '187','company_id' => '1','value' => 'Cancel','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2020-01-02 07:39:08'],
		  ['id' => '189','label_id' => '188','company_id' => '1','value' => 'cancel','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '190','label_id' => '189','company_id' => '1','value' => 'selected user with be active again','deleted_at' => NULL,'created_at' => NULL,'updated_at' => '2020-01-02 07:39:00'],
		  ['id' => '191','label_id' => '190','company_id' => '1','value' => 'selected_user_with_be_active_again','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],

		  ['id' => '192','label_id' => '191','company_id' => '1','value' => 'export_word_user','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '193','label_id' => '192','company_id' => '1','value' => 'export_word_user','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],

		  ['id' => '194','label_id' => '193','company_id' => '1','value' => 'users','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '195','label_id' => '194','company_id' => '1','value' => 'roles','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '196','label_id' => '195','company_id' => '1','value' => 'permissions','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '197','label_id' => '196','company_id' => '1','value' => 'company','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '198','label_id' => '197','company_id' => '1','value' => 'users','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '199','label_id' => '198','company_id' => '1','value' => 'roles','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '200','label_id' => '199','company_id' => '1','value' => 'permissions','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '201','label_id' => '200','company_id' => '1','value' => 'company','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		   ['id' => '202','label_id' => '201','company_id' => '1','value' => 'Delete_Selected_roles','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		    ['id' => '203','label_id' => '202','company_id' => '1','value' => 'Delete_Selected_roles','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		    ['id' => '204','label_id' => '203','company_id' => '1','value' => 'Restore_roles','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		    ['id' => '205','label_id' => '204','company_id' => '1','value' => 'Restore_roles','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '206','label_id' => '205','company_id' => '1','value' => 'active_roles','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		    ['id' => '207','label_id' => '206','company_id' => '1','value' => 'active_roles','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		    ['id' => '208','label_id' => '207','company_id' => '1','value' => 'users_Master','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		     ['id' => '209','label_id' => '208','company_id' => '1','value' => 'users_Master','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],

		    ['id' => '210','label_id' => '209','company_id' => '1','value' => 'export_word_company','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		    ['id' => '211','label_id' => '210','company_id' => '1','value' => 'export_word_company','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		    ['id' => '212','label_id' => '211','company_id' => '1','value' => 'export_excel_company','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		    ['id' => '213','label_id' => '212','company_id' => '1','value' => 'export_excel_company','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		    ['id' => '214','label_id' => '213','company_id' => '1','value' => 'module','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		    ['id' => '215','label_id' => '214','company_id' => '1','value' => 'module','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],

		    ['id' => '216','label_id' => '215','company_id' => '1','value' => 'event','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		    ['id' => '217','label_id' => '216','company_id' => '1','value' => 'event','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		    ['id' => '218','label_id' => '217','company_id' => '1','value' => 'user_type','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		    ['id' => '219','label_id' => '218','company_id' => '1','value' => 'user_type','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],


		    ['id' => '220','label_id' => '219','company_id' => '1','value' => 'users.model.edit.name.msg','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		    ['id' => '221','label_id' => '220','company_id' => '1','value' => 'users.model.edit.email.msg','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		    ['id' => '222','label_id' => '221','company_id' => '1','value' => 'users.model.edit.password.msg','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		    ['id' => '223','label_id' => '222','company_id' => '1','value' => 'users.model.edit.confirm_password.msg','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		    ['id' => '224','label_id' => '223','company_id' => '1','value' => 'users.model.create.confirm_password.msg','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		    ['id' => '225','label_id' => '224','company_id' => '1','value' => 'users.model.create.password.msg','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		    ['id' => '226','label_id' => '225','company_id' => '1','value' => 'users.model.create.email.msg','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		    ['id' => '227','label_id' => '226','company_id' => '1','value' => 'users.model.create.name.msg','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		    ['id' => '228','label_id' => '227','company_id' => '1','value' => 'company.model.edit.name.msg','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		    ['id' => '229','label_id' => '228','company_id' => '1','value' => 'company.model.create.name.msg','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		    ['id' => '230','label_id' => '229','company_id' => '1','value' => 'role.model.edit.name.msg','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		    ['id' => '231','label_id' => '230','company_id' => '1','value' => 'role.model.create.name.msg','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],

		    ['id' => '232','label_id' => '231','company_id' => '1','value' => 'users.model.edit.name.msg','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		    ['id' => '233','label_id' => '232','company_id' => '1','value' => 'users.model.edit.email.msg','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		    ['id' => '234','label_id' => '233','company_id' => '1','value' => 'users.model.edit.password.msg','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		    ['id' => '235','label_id' => '234','company_id' => '1','value' => 'users.model.edit.confirm_password.msg','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		    ['id' => '236','label_id' => '235','company_id' => '1','value' => 'users.model.create.confirm_password.msg','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		    ['id' => '237','label_id' => '236','company_id' => '1','value' => 'users.model.create.password.msg','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		    ['id' => '238','label_id' => '237','company_id' => '1','value' => 'users.model.create.email.msg','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		    ['id' => '239','label_id' => '238','company_id' => '1','value' => 'users.model.create.name.msg','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		    ['id' => '240','label_id' => '239','company_id' => '1','value' => 'company.model.edit.name.msg','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		    ['id' => '241','label_id' => '240','company_id' => '1','value' => 'company.model.create.name.msg','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		    ['id' => '242','label_id' => '241','company_id' => '1','value' => 'role.model.edit.name.msg','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		    ['id' => '243','label_id' => '242','company_id' => '1','value' => 'role.model.create.name.msg','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],




		     ['label_id' => '243','company_id' => '1','value' => 'Login','created_at' => NULL,'updated_at' => NULL],
		   ['label_id' => '244','company_id' => '1','value' => 'your email','created_at' => NULL,'updated_at' => NULL],
		   ['label_id' => '245','company_id' => '1','value' => 'your password','created_at' => NULL,'updated_at' => NULL],
		   ['label_id' => '246','company_id' => '1','value' => 'Login','created_at' => NULL,'updated_at' => NULL],
		   ['label_id' => '247','company_id' => '1','value' => 'forget your password','created_at' => NULL,'updated_at' => NULL],
		   ['label_id' => '248','company_id' => '1','value' => 'WELCOME','created_at' => NULL,'updated_at' => NULL],
		   ['label_id' => '249','company_id' => '1','value' => 'To Alandale web portal','created_at' => NULL,'updated_at' => NULL],
		   ['label_id' => '250','company_id' => '1','value' => 'Learn More','created_at' => NULL,'updated_at' => NULL],

		   ['label_id' => '251','company_id' => '1','value' => 'Login','created_at' => NULL,'updated_at' => NULL],
		   ['label_id' => '252','company_id' => '1','value' => 'your email','created_at' => NULL,'updated_at' => NULL],
		   ['label_id' => '253','company_id' => '1','value' => 'your password','created_at' => NULL,'updated_at' => NULL],
		   ['label_id' => '254','company_id' => '1','value' => 'Sign Up','created_at' => NULL,'updated_at' => NULL],
		   ['label_id' => '255','company_id' => '1','value' => 'forget your password','created_at' => NULL,'updated_at' => NULL],
		   ['label_id' => '256','company_id' => '1','value' => 'WELCOME','created_at' => NULL,'updated_at' => NULL],
		   ['label_id' => '257','company_id' => '1','value' => 'To Alandale web portal','created_at' => NULL,'updated_at' => NULL],
		   ['label_id' => '258','company_id' => '1','value' => 'Learn More','created_at' => NULL,'updated_at' => NULL],





		];
		foreach ($company_labels as $key => $data) {
			CompanyLabel::create($data);
		}
    }
}
