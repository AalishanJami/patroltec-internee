<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class DocumentCompleteFormTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       
    	 $permission_array = [
					['name' => 'view_documentCompletedForm','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'view','type'=>'','sub_module_name'=>'operation','module_name'=>'Completed Forms','module_name_id'=>'62'],
					['name' => 'signoff_documentCompletedForm','group'=>'Job Master', 'guard_name' => 'web','permission_name'=>'Signoff','type'=>'','sub_module_name'=>'operation','module_name'=>'Completed Forms','module_name_id'=>'62'],
					['name' => 'show_signoff_documentCompletedForm','group'=>'Job Master', 'guard_name' => 'web','permission_name'=>'Show Signoff','type'=>'','sub_module_name'=>'operation','module_name'=>'Completed Forms','module_name_id'=>'62'],
           					//listing in Contact
					['name' => 'documentCompletedForm_checkbox','group'=>'Document Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Completed Forms','module_name_id'=>'62'], 
					['name' => 'documentCompletedForm_form_name','group'=>'Document Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'Form Name','sub_module_name'=>'listing','module_name'=>'Completed Forms','module_name_id'=>'62'], 
					['name' => 'documentCompletedForm_location','group'=>'Document Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'location','sub_module_name'=>'listing','module_name'=>'Completed Forms','module_name_id'=>'62'],
					['name' => 'documentCompletedForm_location_breakdown','group'=>'Document Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'location breakdown','sub_module_name'=>'listing','module_name'=>'Completed Forms','module_name_id'=>'62'], 
					['name' => 'documentCompletedForm_user','group'=>'Document Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'user','sub_module_name'=>'listing','module_name'=>'Completed Forms','module_name_id'=>'62'],
					['name' => 'documentCompletedForm_date_time','group'=>'Document Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'date time','sub_module_name'=>'listing','module_name'=>'Completed Forms','module_name_id'=>'62'],
					['name' => 'documentCompletedForm_form_type','group'=>'Document Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'Form Type','sub_module_name'=>'listing','module_name'=>'Completed Forms','module_name_id'=>'62'],
					['name' => 'documentCompletedForm_form_score','group'=>'Document Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'Form Score','sub_module_name'=>'listing','module_name'=>'Completed Forms','module_name_id'=>'62'],
					['name' => 'documentCompletedForm_answer_score','group'=>'Document Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'Answer Score','sub_module_name'=>'listing','module_name'=>'Completed Forms','module_name_id'=>'62'],
          
        ];
           foreach ($permission_array as $key => $permission) {
            $admin_role = Role::find(1);
            $getpermission=Permission::where('name',$permission['name'])->first();
            if (empty($getpermission)){
                $permission=Permission::create($permission);
                $admin_role->givePermissionTo($permission['name']);
            }else{
                $permission=Permission::where('name',$getpermission->name)->update($permission);
                $admin_role->givePermissionTo($getpermission->name);
            }
        }

           
	}

}
