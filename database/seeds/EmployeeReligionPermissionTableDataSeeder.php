<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
class EmployeeReligionPermissionTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission_array = [
            // create Contact permissions
            ['name' => 'view_employeeReligion','group'=>'Employee Master', 'guard_name' => 'web','permission_name'=>'view','type'=>'','sub_module_name'=>'operation','module_name'=>'Religion'],
            ['name' => 'create_employeeReligion','group'=>'Employee Master', 'guard_name' => 'web','permission_name'=>'create','type'=>'','sub_module_name'=>'operation','module_name'=>'Religion'],
            ['name' => 'edit_employeeReligion', 'group'=>'Employee Master', 'guard_name' => 'web','permission_name'=>'edit','type'=>'','sub_module_name'=>'operation','module_name'=>'Religion'],
            ['name' => 'delete_employeeReligion','group'=>'Employee Master', 'guard_name' => 'web','permission_name'=>'delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Religion'],
            ['name' => 'selected_delete_employeeReligion','group'=>'Employee Master', 'guard_name' => 'web','permission_name'=>'Selected Delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Religion'],
            ['name' => 'selected_restore_delete_employeeReligion', 'group'=>'Employee Master','guard_name' => 'web','type'=>'','permission_name'=>'Selected Restore','sub_module_name'=>'operation','module_name'=>'Religion'],
            ['name' => 'restore_delete_employeeReligion','group'=>'Employee Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Restore Delete','sub_module_name'=>'operation','module_name'=>'Religion'],
            ['name' => 'hard_delete_employeeReligion','group'=>'Employee Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Hard Delete','sub_module_name'=>'operation','module_name'=>'Religion'],
            ['name' => 'active_employeeReligion','group'=>'Employee Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Active','sub_module_name'=>'operation','module_name'=>'Religion'],
            ['name' => 'selected_active_employeeReligion','group'=>'Employee Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Selected Active ','sub_module_name'=>'operation','module_name'=>'Religion'],
            ['name' => 'word_employeeReligion', 'group'=>'Employee Master','guard_name' => 'web','type'=>'','permission_name'=>'Word Export ','sub_module_name'=>'operation','module_name'=>'Religion'],
            ['name' => 'csv_employeeReligion','group'=>'Employee Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Csv Export ','sub_module_name'=>'operation','module_name'=>'Religion'],
            ['name' => 'pdf_employeeReligion', 'group'=>'Employee Master','guard_name' => 'web','type'=>'','permission_name'=>'PDF Export','sub_module_name'=>'operation','module_name'=>'Religion'],

            //listing in Contact
            ['name' => 'employeeReligion_checkbox','group'=>'Employee Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Religion'],
            ['name' => 'employeeReligion_name','group'=>'Employee Master', 'guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Religion'],

            //word in Contact
            ['name' => 'employeeReligion_checkbox_word_export','group'=>'Employee Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'word_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Religion'],
            ['name' => 'employeeReligion_name_word_export','group'=>'Employee Master', 'guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Religion'],

            //excel in Contact
            ['name' => 'employeeReligion_checkbox_excel_export','group'=>'Employee Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'excel_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Religion'],
            ['name' => 'employeeReligion_name_excel_export','group'=>'Employee Master', 'guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Religion'],

            //pdf in Contact
            ['name' => 'employeeReligion_checkbox_pdf_export','group'=>'Employee Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'pdf_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Religion'],
            ['name' => 'employeeReligion_name_pdf_export','group'=>'Employee Master', 'guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Religion'],

            //create in Contact
            ['name' => 'employeeReligion_checkbox_create','group'=>'Employee Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'create','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Religion'],
            ['name' => 'employeeReligion_name_create','group'=>'Employee Master', 'guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Religion'],

            //edit in Contact
            ['name' => 'employeeReligion_checkbox_edit','group'=>'Employee Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'edit','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Religion'],
            ['name' => 'employeeReligion_name_edit','group'=>'Employee Master', 'guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Religion'],
        ];

        foreach ($permission_array as $key => $permission) {
            $admin_role = Role::find(1);
            $getpermission=Permission::where('name',$permission['name'])->first();
            if (empty($getpermission)){
                $permission=Permission::create($permission);
                $admin_role->givePermissionTo($permission['name']);
            }else{
                $permission=Permission::where('name',$getpermission->name)->update($permission);
                $admin_role->givePermissionTo($getpermission->name);
            }
        }
    }
}
