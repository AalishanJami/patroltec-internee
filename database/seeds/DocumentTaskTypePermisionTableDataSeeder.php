<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
class DocumentTaskTypePermisionTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        $permission_array = [
            // create project permissions
            ['name' => 'view_documentTaskType','group' => 'Document Master', 'guard_name' => 'web','permission_name'=>'View','type'=>'','sub_module_name'=>'operation','module_name'=>'Task Type','module_name_id'=>'56'],
            ['name' => 'create_documentTaskType','group' => 'Document Master', 'guard_name' => 'web','permission_name'=>'Create','type'=>'','sub_module_name'=>'operation','module_name'=>'Task Type','module_name_id'=>'56'],
            ['name' => 'edit_documentTaskType', 'group' => 'Document Master', 'guard_name' => 'web','permission_name'=>'Edit','type'=>'','sub_module_name'=>'operation','module_name'=>'Task Type','module_name_id'=>'56'],
            ['name' => 'delete_documentTaskType','group' => 'Document Master', 'guard_name' => 'web','permission_name'=>'Delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Task Type','module_name_id'=>'56'],
            ['name' => 'delete_selected_documentTaskType','group' => 'Document Master', 'guard_name' => 'web','permission_name'=>'Selected Delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Task Type','module_name_id'=>'56'],
            ['name' => 'selected_active_documentTaskType', 'group' => 'Document Master','guard_name' => 'web','type'=>'','permission_name'=>'Selected Active Multiple','sub_module_name'=>'operation','module_name'=>'Task Type','module_name_id'=>'56'],
            ['name' => 'selected_restore_documentTaskType', 'group' => 'Document Master','guard_name' => 'web','type'=>'','permission_name'=>'Selected Restore','sub_module_name'=>'operation','module_name'=>'Task Type','module_name_id'=>'56'],
            ['name' => 'hard_delete_documentTaskType','group' => 'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Hard Delete','sub_module_name'=>'operation','module_name'=>'Task Type','module_name_id'=>'56'],
            ['name' => 'active_documentTaskType','group' => 'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Active Employee','sub_module_name'=>'operation','module_name'=>'Task Type','module_name_id'=>'56'],
            ['name' => 'select_active_documentTaskType','group' => 'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Select Active ','sub_module_name'=>'operation','module_name'=>'Task Type','module_name_id'=>'56'],
            ['name' => 'word_documentTaskType', 'group' => 'Document Master','guard_name' => 'web','type'=>'','permission_name'=>'Word Export ','sub_module_name'=>'operation','module_name'=>'Task Type','module_name_id'=>'56'],
            ['name' => 'csv_documentTaskType','group' => 'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Csv Export ','sub_module_name'=>'operation','module_name'=>'Task Type','module_name_id'=>'56'],
            ['name' => 'pdf_documentTaskType', 'group' => 'Document Master','guard_name' => 'web','type'=>'','permission_name'=>'PDF Export ','sub_module_name'=>'operation','module_name'=>'Task Type','module_name_id'=>'56'],

            //listing in tasktype
            ['name' => 'documentTaskType_checkbox','group' => 'Document Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Task Type','module_name_id'=>'56'],
            ['name' => 'documentTaskType_name', 'group' => 'Document Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Task Type','module_name_id'=>'56'],

            //word in tasktype
            ['name' => 'documentTaskType_checkbox_word_export','group' => 'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'word_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Task Type','module_name_id'=>'56'],
            ['name' => 'documentTaskType_name_word_export', 'group' => 'Document Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Task Type','module_name_id'=>'56'],

            // excel in tasktype
            ['name' => 'documentTaskType_checkbox_excel_export','group' => 'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'excel_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Task Type','module_name_id'=>'56'],
            ['name' => 'documentTaskType_name_excel_export', 'group' => 'Document Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Task Type','module_name_id'=>'56'],

            // pdf in tasktype
            ['name' => 'documentTaskType_checkbox_pdf_export','group' => 'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'pdf_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Task Type','module_name_id'=>'56'],
            ['name' => 'documentTaskType_name_pdf_export', 'group' => 'Document Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Task Type','module_name_id'=>'56'],


            //create in tasktype
            ['name' => 'documentTaskType_checkbox_create','group' => 'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'create','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Task Type','module_name_id'=>'56'],
            ['name' => 'documentTaskType_name_create', 'group' => 'Document Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Task Type','module_name_id'=>'56'],


            //edit in tasktype
            ['name' => 'documentTaskType_checkbox_edit','group' => 'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'edit','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Task Type','module_name_id'=>'56'],
            ['name' => 'documentTaskType_name_edit', 'group' => 'Document Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Task Type','module_name_id'=>'56'],
        ];

        foreach ($permission_array as $key => $permission) {
            $admin_role = Role::find(1);
            $getpermission=Permission::where('name',$permission['name'])->first();
            if (empty($getpermission)){
                $permission=Permission::create($permission);
                $admin_role->givePermissionTo($permission['name']);
            }else{
                $permission=Permission::where('name',$getpermission->name)->update($permission);
                $admin_role->givePermissionTo($getpermission->name);
            }
        }
    }
}
