<?php

use Illuminate\Database\Seeder;

class PermissionModuleNamesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('permission_module_names')->delete();

        \DB::table('permission_module_names')->insert(array (
            0 =>
            array (
                'id' => 1,
                'module_name' => 'user',
                'permission_groups_id' => 1,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 1,
            ),
            1 =>
            array (
                'id' => 2,
                'module_name' => 'role',
                'permission_groups_id' => 1,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 2,
            ),
            2 =>
            array (
                'id' => 3,
                'module_name' => 'permission',
                'permission_groups_id' => 1,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 3,
            ),
            3 =>
            array (
                'id' => 4,
                'module_name' => 'company',
                'permission_groups_id' => 2,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 4,
            ),
            4 =>
            array (
                'id' => 5,
                'module_name' => 'cms',
                'permission_groups_id' => 3,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 5,
            ),
            5 =>
            array (
                'id' => 6,
                'module_name' => 'project',
                'permission_groups_id' => 4,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 1,
            ),
            6 =>
            array (
                'id' => 7,
                'module_name' => 'Contact',
                'permission_groups_id' => 4,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 11,
            ),
            7 =>
            array (
                'id' => 8,
                'module_name' => 'Bonus Score',
                'permission_groups_id' => 4,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 8,
            ),
            8 =>
            array (
                'id' => 9,
                'module_name' => 'Address',
                'permission_groups_id' => 4,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 9,
            ),
            9 =>
            array (
                'id' => 10,
                'module_name' => 'Address Type',
                'permission_groups_id' => 4,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 10,
            ),
            10 =>
            array (
                'id' => 11,
                'module_name' => 'Contact Type',
                'permission_groups_id' => 4,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 12,
            ),
            11 =>
            array (
                'id' => 12,
                'module_name' => 'Equipment Group',
                'permission_groups_id' => 4,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 13,
            ),
            12 =>
            array (
                'id' => 13,
                'module_name' => 'Equipment',
                'permission_groups_id' => 4,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 14,
            ),
            13 =>
            array (
                'id' => 14,
                'module_name' => 'QR',
                'permission_groups_id' => 4,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 15,
            ),
            14 =>
            array (
                'id' => 15,
                'module_name' => 'Contractor',
                'permission_groups_id' => 4,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 2,
            ),
            15 =>
            array (
                'id' => 16,
                'module_name' => 'Manager Name',
                'permission_groups_id' => 4,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 3,
            ),
            16 =>
            array (
                'id' => 17,
                'module_name' => 'Contract Manager Name',
                'permission_groups_id' => 4,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 4,
            ),
            17 =>
            array (
                'id' => 18,
                'module_name' => 'Division',
                'permission_groups_id' => 4,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 5,
            ),
            18 =>
            array (
                'id' => 19,
                'module_name' => 'Customer',
                'permission_groups_id' => 4,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 6,
            ),
            19 =>
            array (
                'id' => 20,
                'module_name' => 'Group',
                'permission_groups_id' => 4,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 7,
            ),
            20 =>
            array (
                'id' => 21,
                'module_name' => 'Client',
                'permission_groups_id' => 5,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 21,
            ),
            21 =>
            array (
                'id' => 22,
                'module_name' => 'Client Notes',
                'permission_groups_id' => 5,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 22,
            ),
            22 =>
            array (
                'id' => 23,
                'module_name' => 'Client Upload',
                'permission_groups_id' => 5,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 23,
            ),
            23 =>
            array (
                'id' => 24,
                'module_name' => 'Document',
                'permission_groups_id' => 6,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 41,
            ),
            24 =>
            array (
                'id' => 25,
                'module_name' => 'Notes',
                'permission_groups_id' => 6,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 40,
            ),
            25 =>
            array (
                'id' => 26,
                'module_name' => 'Account',
                'permission_groups_id' => 6,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 39,
            ),
            26 =>
            array (
                'id' => 27,
                'module_name' => 'Contact Information',
                'permission_groups_id' => 6,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 36,
            ),
            27 =>
            array (
                'id' => 28,
                'module_name' => 'Location Permission',
                'permission_groups_id' => 6,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 35,
            ),
            28 =>
            array (
                'id' => 29,
                'module_name' => 'Location Group Permission',
                'permission_groups_id' => 6,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 34,
            ),
            29 =>
            array (
                'id' => 30,
                'module_name' => 'Folder Permission',
                'permission_groups_id' => 6,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 32,
            ),
            30 =>
            array (
                'id' => 31,
                'module_name' => 'File Permission',
                'permission_groups_id' => 6,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 33,
            ),
            31 =>
            array (
                'id' => 32,
                'module_name' => 'Staff Permission',
                'permission_groups_id' => 6,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 29,
            ),
            32 =>
            array (
                'id' => 33,
                'module_name' => 'Position Permission',
                'permission_groups_id' => 6,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 30,
            ),
            33 =>
            array (
                'id' => 34,
                'module_name' => 'Staff Role Permission',
                'permission_groups_id' => 6,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 31,
            ),
            34 =>
            array (
                'id' => 35,
                'module_name' => 'RelationShip',
                'permission_groups_id' => 6,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 38,
            ),
            35 =>
            array (
                'id' => 36,
                'module_name' => 'Employee',
                'permission_groups_id' => 6,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 24,
            ),
            36 =>
            array (
                'id' => 37,
                'module_name' => 'Next Of Kin',
                'permission_groups_id' => 6,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 37,
            ),
            37 =>
            array (
                'id' => 38,
                'module_name' => 'Licence',
                'permission_groups_id' => 6,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 42,
            ),
            38 =>
            array (
                'id' => 39,
                'module_name' => 'Position',
                'permission_groups_id' => 6,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 25,
            ),
            39 =>
            array (
                'id' => 40,
                'module_name' => 'Sexual Orientation',
                'permission_groups_id' => 6,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 26,
            ),
            40 =>
            array (
                'id' => 41,
                'module_name' => 'Ethnic',
                'permission_groups_id' => 6,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 27,
            ),
            41 =>
            array (
                'id' => 42,
                'module_name' => 'Religion',
                'permission_groups_id' => 6,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 28,
            ),
            42 =>
            array (
                'id' => 43,
                'module_name' => 'Form Type',
                'permission_groups_id' => 7,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 3,
            ),
            43 =>
            array (
                'id' => 44,
                'module_name' => 'Scoring',
                'permission_groups_id' => 7,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 9,
            ),
            44 =>
            array (
                'id' => 45,
                'module_name' => 'Notes',
                'permission_groups_id' => 7,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 13,
            ),
            45 =>
            array (
                'id' => 46,
                'module_name' => 'Answer Group',
                'permission_groups_id' => 7,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 6,
            ),
            46 =>
            array (
                'id' => 47,
                'module_name' => 'Answer',
                'permission_groups_id' => 7,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 7,
            ),
            47 =>
            array (
                'id' => 48,
                'module_name' => 'Document Upload',
                'permission_groups_id' => 7,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 14,
            ),
            48 =>
            array (
                'id' => 49,
                'module_name' => 'Document Library',
                'permission_groups_id' => 7,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 1,
            ),
            49 =>
            array (
                'id' => 50,
                'module_name' => 'Document Folder',
                'permission_groups_id' => 7,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 2,
            ),
            50 =>
            array (
                'id' => 51,
                'module_name' => 'Schedule',
                'permission_groups_id' => 7,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 10,
            ),
            51 =>
            array (
                'id' => 52,
                'module_name' => 'Static Form',
                'permission_groups_id' => 7,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 8,
            ),
            52 =>
            array (
                'id' => 53,
                'module_name' => 'Dynamic Form',
                'permission_groups_id' => 7,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 4,
            ),
            53 =>
            array (
                'id' => 54,
                'module_name' => 'Question',
                'permission_groups_id' => 7,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 5,
            ),
            54 =>
            array (
                'id' => 55,
                'module_name' => 'Version Log',
                'permission_groups_id' => 7,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 12,
            ),
            55 =>
            array (
                'id' => 56,
                'module_name' => 'Task Type',
                'permission_groups_id' => 7,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 11,
            ),
            56 =>
            array (
                'id' => 57,
                'module_name' => 'QR Report',
                'permission_groups_id' => 8,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 57,
            ),
            57 =>
            array (
                'id' => 58,
                'module_name' => 'Plant And Equipment Report',
                'permission_groups_id' => 8,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 58,
            ),
            58 =>
            array (
                'id' => 59,
                'module_name' => 'Email Report',
                'permission_groups_id' => 8,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 59,
            ),
            59 =>
            array (
                'id' => 60,
                'module_name' => 'Email Support',
                'permission_groups_id' => 9,
                'created_at' => '2020-05-06 08:05:05',
                'updated_at' => '2020-05-06 08:05:05',
                'order' => 60,
            ),
            60 =>
            array (
                'id' => 61,
                'module_name' => 'Jobs',
                'permission_groups_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
                'order' => NULL,
            ),
            61 =>
            array (
                'id' => 62,
                'module_name' => 'Completed Forms',
                'permission_groups_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
                'order' => 15,
            ),
            62 =>
                array (
                    'id' => 63,
                    'module_name' => 'Job Location',
                    'permission_groups_id' => 10,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                    'order' => NULL,
            ),
            63 =>
            array (
                'id' => 64,
                'module_name' => 'App Roles & Permissions',
                'permission_groups_id' => 1,
                'created_at' => '2020-08-15 08:05:05',
                'updated_at' => '2020-08-15 08:05:05',
                'order' => 1,
            ),
        ));


    }
}
