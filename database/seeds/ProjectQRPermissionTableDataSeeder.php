<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class ProjectQRPermissionTableDataSeeder extends Seeder
{
    public function run()
    {
        $permission_array = [
            // create Contact permissions
            ['name' => 'view_projectqr','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'view','type'=>'','sub_module_name'=>'operation','module_name'=>'QR'],
            ['name' => 'create_projectqr','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'create','type'=>'','sub_module_name'=>'operation','module_name'=>'QR'],
            ['name' => 'edit_projectqr', 'group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'edit','type'=>'','sub_module_name'=>'operation','module_name'=>'QR'],
            ['name' => 'delete_projectqr','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'delete','type'=>'','sub_module_name'=>'operation','module_name'=>'QR'],
            ['name' => 'selected_delete_projectqr','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'Selected Delete','type'=>'','sub_module_name'=>'operation','module_name'=>'QR'],
            ['name' => 'selected_restore_delete_projectqr', 'group'=>'Project Master','guard_name' => 'web','type'=>'','permission_name'=>'Selected Restore','sub_module_name'=>'operation','module_name'=>'QR'],
            ['name' => 'restore_delete_projectqr','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Restore Delete','sub_module_name'=>'operation','module_name'=>'QR'],
            ['name' => 'selected_active_button_projectqr','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'Selected Active Button','type'=>'','sub_module_name'=>'operation','module_name'=>'QR'],
            ['name' => 'hard_delete_projectqr','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Hard Delete','sub_module_name'=>'operation','module_name'=>'QR'],
            ['name' => 'active_projectqr','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Active Equipment Group','sub_module_name'=>'operation','module_name'=>'QR'],
            ['name' => 'word_projectqr', 'group'=>'Project Master','guard_name' => 'web','type'=>'','permission_name'=>'Word Export Equipment Group','sub_module_name'=>'operation','module_name'=>'QR'],
            ['name' => 'csv_projectqr','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Csv Export Equipment Group','sub_module_name'=>'operation','module_name'=>'QR'],
            ['name' => 'pdf_projectqr', 'group'=>'Project Master','guard_name' => 'web','type'=>'','permission_name'=>'PDF Export Equipment Group','sub_module_name'=>'operation','module_name'=>'QR'],

            //listing in Contact
            ['name' => 'projectqr_checkbox','group'=>'Project Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'QR'],
            ['name' => 'projectqr_qr', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Qr','sub_module_name'=>'listing','module_name'=>'QR'],
            ['name' => 'projectqr_name','group'=>'Project Master', 'guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'QR'],
            ['name' => 'projectqr_directions', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Directions','sub_module_name'=>'listing','module_name'=>'QR'],
            ['name' => 'projectqr_information', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Information','sub_module_name'=>'listing','module_name'=>'QR'],

            //word in Contact
            ['name' => 'projectqr_checkbox_word_export','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'word_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'QR'],
            ['name' => 'projectqr_qr_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Qr','sub_module_name'=>'listing','module_name'=>'QR'],
            ['name' => 'projectqr_name_word_export','group'=>'Project Master', 'guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'QR'],
            ['name' => 'projectqr_directions_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Directions','sub_module_name'=>'listing','module_name'=>'QR'],
            ['name' => 'projectqr_information_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Information','sub_module_name'=>'listing','module_name'=>'QR'],

            //excel in Contact
            ['name' => 'projectqr_checkbox_excel_export','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'excel_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'QR'],
            ['name' => 'projectqr_qr_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Qr','sub_module_name'=>'listing','module_name'=>'QR'],
            ['name' => 'projectqr_name_excel_export','group'=>'Project Master', 'guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'QR'],
            ['name' => 'projectqr_directions_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Directions','sub_module_name'=>'listing','module_name'=>'QR'],
            ['name' => 'projectqr_information_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Information','sub_module_name'=>'listing','module_name'=>'QR'],

            //pdf in Contact
            ['name' => 'projectqr_checkbox_pdf_export','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'pdf_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'QR'],
            ['name' => 'projectqr_qr_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Qr','sub_module_name'=>'listing','module_name'=>'QR'],
            ['name' => 'projectqr_name_pdf_export','group'=>'Project Master', 'guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'QR'],
            ['name' => 'projectqr_directions_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Directions','sub_module_name'=>'listing','module_name'=>'QR'],
            ['name' => 'projectqr_information_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Information','sub_module_name'=>'listing','module_name'=>'QR'],

            //create in Contact
            ['name' => 'projectqr_checkbox_create','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'create','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'QR'],
            ['name' => 'projectqr_qr_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Qr','sub_module_name'=>'listing','module_name'=>'QR'],
            ['name' => 'projectqr_name_create','group'=>'Project Master', 'guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'QR'],
            ['name' => 'projectqr_directions_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Directions','sub_module_name'=>'listing','module_name'=>'QR'],
            ['name' => 'projectqr_information_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Information','sub_module_name'=>'listing','module_name'=>'QR'],

            //edit in Contact
            ['name' => 'projectqr_checkbox_edit','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'edit','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'QR'],
            ['name' => 'projectqr_qr_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Qr','sub_module_name'=>'listing','module_name'=>'QR'],
            ['name' => 'projectqr_name_edit','group'=>'Project Master', 'guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'QR'],
            ['name' => 'projectqr_directions_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Directions','sub_module_name'=>'listing','module_name'=>'QR'],
            ['name' => 'projectqr_information_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Information','sub_module_name'=>'listing','module_name'=>'QR'],
        ];
        foreach ($permission_array as $key => $permission) {
            $admin_role = Role::find(1);
            $getpermission=Permission::where('name',$permission['name'])->first();
            if (empty($getpermission)){
                $permission=Permission::create($permission);
                $admin_role->givePermissionTo($permission['name']);
            }else{
                $permission=Permission::where('name',$getpermission->name)->update($permission);
                $admin_role->givePermissionTo($getpermission->name);
            }
        }
    }
}
