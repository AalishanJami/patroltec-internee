<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
class EmployeeStaffGroupPermissionTableDataSeeder extends Seeder
{
    public function run()
    {
        $permission_array = [
            // create Contact permissions
            ['name' => 'view_employeeStaffGroupPermision','group'=>'Employee Master', 'guard_name' => 'web','permission_name'=>'view','type'=>'','sub_module_name'=>'operation','module_name'=>'Staff Group Permission'],
            //listing in Contact
            ['name' => 'employeeStaffGroupPermision_checkbox','group'=>'Employee Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'edit','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Staff Group Permission'],
            ['name' => 'employeeStaffGroupPermision_name','group'=>'Employee Master', 'guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Staff Group Permission'],
        ];

        foreach ($permission_array as $key => $permission) {
            $admin_role = Role::find(1);
            $getpermission=Permission::where('name',$permission['name'])->first();
            if (empty($getpermission)){
                $permission=Permission::create($permission);
                $admin_role->givePermissionTo($permission['name']);
            }else{
                $permission=Permission::where('name',$getpermission->name)->update($permission);
                $admin_role->givePermissionTo($getpermission->name);
            }
        }
    }
}
