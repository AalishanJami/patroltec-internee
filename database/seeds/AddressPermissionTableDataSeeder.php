<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AddressPermissionTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission_array = [
            // create Address Type permissions
            ['name' => 'view_address','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'view','type'=>'','sub_module_name'=>'operation','module_name'=>'Address'],
            ['name' => 'create_address','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'create','type'=>'','sub_module_name'=>'operation','module_name'=>'Address'],
            ['name' => 'edit_address', 'group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'edit','type'=>'','sub_module_name'=>'operation','module_name'=>'Address'],
            ['name' => 'delete_address','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Address'],
            ['name' => 'selected_delete_address','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'Selected Delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Address'],
            ['name' => 'selected_restore_delete_address', 'group'=>'Project Master','guard_name' => 'web','type'=>'','permission_name'=>'Selected Restore','sub_module_name'=>'operation','module_name'=>'Address'],
            ['name' => 'restore_delete_address','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Restore Delete','sub_module_name'=>'operation','module_name'=>'Address'],
            ['name' => 'hard_delete_address','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Hard Delete','sub_module_name'=>'operation','module_name'=>'Address'],
            ['name' => 'active_address','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Active','sub_module_name'=>'operation','module_name'=>'Address'],
            ['name' => 'slected_active_address','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Slected Active ','sub_module_name'=>'operation','module_name'=>'Address'],
            ['name' => 'word_address', 'group'=>'Project Master','guard_name' => 'web','type'=>'','permission_name'=>'Word Export','sub_module_name'=>'operation','module_name'=>'Address'],
            ['name' => 'csv_address','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Csv Export','sub_module_name'=>'operation','module_name'=>'Address'],
            ['name' => 'pdf_address', 'group'=>'Project Master','guard_name' => 'web','type'=>'','permission_name'=>'PDF Export','sub_module_name'=>'operation','module_name'=>'Address'],

            //listing in Address Type
            ['name' => 'address_checkbox','group'=>'Project Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Address'],
            ['name' => 'address_building', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'building','sub_module_name'=>'listing','module_name'=>'Address'],
            ['name' => 'address_addressType','group'=>'Project Master', 'guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'addresstype','sub_module_name'=>'listing','module_name'=>'Address'],
            ['name' => 'address_phone', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'phone','sub_module_name'=>'listing','module_name'=>'Address'],
            ['name' => 'address_email', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'email','sub_module_name'=>'listing','module_name'=>'Address'],
            ['name' => 'address_postcode', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'postcode','sub_module_name'=>'listing','module_name'=>'Address'],
            ['name' => 'address_town', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'town','sub_module_name'=>'listing','module_name'=>'Address'],
            ['name' => 'address_country', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'country','sub_module_name'=>'listing','module_name'=>'Address'],
            ['name' => 'address_state', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'state','sub_module_name'=>'listing','module_name'=>'Address'],

            //word in Address Type
            ['name' => 'address_checkbox_word_export','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'word_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Address'],
            ['name' => 'address_building_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'building','sub_module_name'=>'listing','module_name'=>'Address'],
            ['name' => 'address_addressType_word_export','group'=>'Project Master', 'guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'addresstype','sub_module_name'=>'listing','module_name'=>'Address'],
            ['name' => 'address_phone_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'phone','sub_module_name'=>'listing','module_name'=>'Address'],
            ['name' => 'address_email_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'email','sub_module_name'=>'listing','module_name'=>'Address'],
            ['name' => 'address_postcode_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'postcode','sub_module_name'=>'listing','module_name'=>'Address'],
            ['name' => 'address_town_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'town','sub_module_name'=>'listing','module_name'=>'Address'],
            ['name' => 'address_country_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'country','sub_module_name'=>'listing','module_name'=>'Address'],
            ['name' => 'address_state_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'state','sub_module_name'=>'listing','module_name'=>'Address'],

            //excel in Address Type
            ['name' => 'address_checkbox_excel_export','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'excel_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Address'],
            ['name' => 'address_building_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'building','sub_module_name'=>'listing','module_name'=>'Address'],
            ['name' => 'address_addressType_excel_export','group'=>'Project Master', 'guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'addresstype','sub_module_name'=>'listing','module_name'=>'Address'],
            ['name' => 'address_phone_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'phone','sub_module_name'=>'listing','module_name'=>'Address'],
            ['name' => 'address_email_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'email','sub_module_name'=>'listing','module_name'=>'Address'],
            ['name' => 'address_postcode_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'postcode','sub_module_name'=>'listing','module_name'=>'Address'],
            ['name' => 'address_town_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'town','sub_module_name'=>'listing','module_name'=>'Address'],
            ['name' => 'address_country_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'country','sub_module_name'=>'listing','module_name'=>'Address'],
            ['name' => 'address_state_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'state','sub_module_name'=>'listing','module_name'=>'Address'],

            //pdf in Address Type
            ['name' => 'address_checkbox_pdf_export','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'pdf_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Address'],
            ['name' => 'address_building_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'building','sub_module_name'=>'listing','module_name'=>'Address'],
            ['name' => 'address_addressType_pdf_export','group'=>'Project Master', 'guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'addresstype','sub_module_name'=>'listing','module_name'=>'Address'],
            ['name' => 'address_phone_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'phone','sub_module_name'=>'listing','module_name'=>'Address'],
            ['name' => 'address_email_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'email','sub_module_name'=>'listing','module_name'=>'Address'],
            ['name' => 'address_postcode_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'postcode','sub_module_name'=>'listing','module_name'=>'Address'],
            ['name' => 'address_town_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'town','sub_module_name'=>'listing','module_name'=>'Address'],
            ['name' => 'address_country_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'country','sub_module_name'=>'listing','module_name'=>'Address'],
            ['name' => 'address_state_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'state','sub_module_name'=>'listing','module_name'=>'Address'],

            //create in Address Type
            ['name' => 'address_checkbox_create','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'create','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Address'],
            ['name' => 'address_building_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'building','sub_module_name'=>'listing','module_name'=>'Address'],
            ['name' => 'address_addressType_create','group'=>'Project Master', 'guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'addresstype','sub_module_name'=>'listing','module_name'=>'Address'],
            ['name' => 'address_phone_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'phone','sub_module_name'=>'listing','module_name'=>'Address'],
            ['name' => 'address_email_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'email','sub_module_name'=>'listing','module_name'=>'Address'],
            ['name' => 'address_postcode_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'postcode','sub_module_name'=>'listing','module_name'=>'Address'],
            ['name' => 'address_town_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'town','sub_module_name'=>'listing','module_name'=>'Address'],
            ['name' => 'address_country_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'country','sub_module_name'=>'listing','module_name'=>'Address'],
            ['name' => 'address_state_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'state','sub_module_name'=>'listing','module_name'=>'Address'],

            //edit in Address Type
            ['name' => 'address_checkbox_edit','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'edit','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Address'],
            ['name' => 'address_building_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'building','sub_module_name'=>'listing','module_name'=>'Address'],
            ['name' => 'address_addressType_edit','group'=>'Project Master', 'guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'addresstype','sub_module_name'=>'listing','module_name'=>'Address'],
            ['name' => 'address_phone_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'phone','sub_module_name'=>'listing','module_name'=>'Address'],
            ['name' => 'address_email_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'email','sub_module_name'=>'listing','module_name'=>'Address'],
            ['name' => 'address_postcode_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'postcode','sub_module_name'=>'listing','module_name'=>'Address'],
            ['name' => 'address_town_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'town','sub_module_name'=>'listing','module_name'=>'Address'],
            ['name' => 'address_country_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'country','sub_module_name'=>'listing','module_name'=>'Address'],
            ['name' => 'address_state_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'state','sub_module_name'=>'listing','module_name'=>'Address'],
        ];

         DB::statement('SET FOREIGN_KEY_CHECKS=0;');
         $address = Permission::where(['module_name'=>'AddressType'])->get();
         if (count($address)>0){
             Permission::where(['module_name'=>'AddressType'])->delete();
         }
        foreach ($permission_array as $key => $permission) {
            $admin_role = Role::find(1);
            $getpermission=Permission::where('name',$permission['name'])->first();
            if (empty($getpermission)){
                $permission=Permission::create($permission);
                $admin_role->givePermissionTo($permission['name']);
            }else{
                $permission=Permission::where('name',$getpermission->name)->update($permission);
                $admin_role->givePermissionTo($getpermission->name);
            }
        }
         DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
