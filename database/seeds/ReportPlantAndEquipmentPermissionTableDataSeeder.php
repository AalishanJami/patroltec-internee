<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
class ReportPlantAndEquipmentPermissionTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission_array = [
            // create report permissions
            ['name' => 'view_plantandequipmentReport','group'=>'Report Master', 'guard_name' => 'web','permission_name'=>'View','type'=>'','sub_module_name'=>'operation','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'word_plantandequipmentReport', 'group'=>'Report Master','guard_name' => 'web','type'=>'','permission_name'=>'Word Export ','sub_module_name'=>'operation','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'csv_plantandequipmentReport','group'=>'Report Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Csv Export','sub_module_name'=>'operation','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'pdf_plantandequipmentReport', 'group'=>'Report Master','guard_name' => 'web','type'=>'','permission_name'=>'PDF Export','sub_module_name'=>'operation','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],

            //listing
            ['name' => 'plantandequipmentReport_checkbox','group'=>'Report Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_qr', 'group'=>'Report Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Qr','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_project', 'group'=>'Report Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Project','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_name','group'=>'Report Master', 'guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_equipment', 'group'=>'Report Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Equipment','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_serialnumber', 'group'=>'Report Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Serial Number','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_make', 'group'=>'Report Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Make','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_model', 'group'=>'Report Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Model','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_lastservicedate', 'group'=>'Report Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Last Service Date','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_nextservicedate', 'group'=>'Report Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Next Service Date','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_condition', 'group'=>'Report Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Condition','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],

            //create
            ['name' => 'plantandequipmentReport_checkbox_create','group'=>'Report Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'create','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_qr_create', 'group'=>'Report Master','guard_name' => 'web','type'=>'create','enable'=>'0','permission_name'=>'Qr','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_project_create', 'group'=>'Report Master','guard_name' => 'web','type'=>'create','enable'=>'0','permission_name'=>'Project','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_name_create','group'=>'Report Master', 'guard_name' => 'web','type'=>'create','enable'=>'0','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_equipment_create', 'group'=>'Report Master','guard_name' => 'web','type'=>'create','enable'=>'0','permission_name'=>'Equipment','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_serialnumber_create', 'group'=>'Report Master','guard_name' => 'web','type'=>'create','enable'=>'0','permission_name'=>'Serial Number','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_make_create', 'group'=>'Report Master','guard_name' => 'web','type'=>'create','enable'=>'0','permission_name'=>'Make','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_model_create', 'group'=>'Report Master','guard_name' => 'web','type'=>'create','enable'=>'0','permission_name'=>'Model','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_lastservicedate_create', 'group'=>'Report Master','guard_name' => 'web','type'=>'create','enable'=>'0','permission_name'=>'Last Service Date','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_nextservicedate_create', 'group'=>'Report Master','guard_name' => 'web','type'=>'create','enable'=>'0','permission_name'=>'Next Service Date','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_condition_create', 'group'=>'Report Master','guard_name' => 'web','type'=>'create','enable'=>'0','permission_name'=>'Condition','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],

            //word export
            ['name' => 'plantandequipmentReport_checkbox_word_export','group'=>'Report Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'word_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_qr_word_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Qr','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_project_word_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Project','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_name_word_export','group'=>'Report Master', 'guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_equipment_word_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Equipment','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_serialnumber_word_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Serial Number','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_make_word_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Make','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_model_word_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Model','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_lastservicedate_word_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Last Service Date','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_nextservicedate_word_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Next Service Date','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_condition_word_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Condition','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],


            //excel in export
            ['name' => 'plantandequipmentReport_checkbox_excel_export','group'=>'Report Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'excel_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_qr_excel_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Qr','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_project_excel_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Project','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_name_excel_export','group'=>'Report Master', 'guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_equipment_excel_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Equipment','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_serialnumber_excel_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Serial Number','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_make_excel_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Make','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_model_excel_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Model','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_lastservicedate_excel_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Last Service Date','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_nextservicedate_excel_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Next Service Date','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_condition_excel_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Condition','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],


            //pdf
            ['name' => 'plantandequipmentReport_checkbox_pdf_export','group'=>'Report Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'pdf_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_qr_pdf_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Qr','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_project_pdf_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Project','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_name_pdf_export','group'=>'Report Master', 'guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_equipment_pdf_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Equipment','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_serialnumber_pdf_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Serial Number','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_make_pdf_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Make','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_model_pdf_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Model','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_lastservicedate_pdf_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Last Service Date','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_nextservicedate_pdf_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Next Service Date','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_condition_pdf_export', 'group'=>'Report Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Condition','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],


            //edit
            ['name' => 'plantandequipmentReport_checkbox_edit','group'=>'Report Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'edit','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_qr_edit', 'group'=>'Report Master','guard_name' => 'web','type'=>'edit','enable'=>'0','permission_name'=>'Qr','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_project_edit', 'group'=>'Report Master','guard_name' => 'web','type'=>'edit','enable'=>'0','permission_name'=>'Project','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_name_edit','group'=>'Report Master', 'guard_name' => 'web','type'=>'edit','enable'=>'0','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_equipment_edit', 'group'=>'Report Master','guard_name' => 'web','type'=>'edit','enable'=>'0','permission_name'=>'Equipment','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_serialnumber_edit', 'group'=>'Report Master','guard_name' => 'web','type'=>'edit','enable'=>'0','permission_name'=>'Serial Number','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_make_edit', 'group'=>'Report Master','guard_name' => 'web','type'=>'edit','enable'=>'0','permission_name'=>'Make','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_model_edit', 'group'=>'Report Master','guard_name' => 'web','type'=>'edit','enable'=>'0','permission_name'=>'Model','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_lastservicedate_edit', 'group'=>'Report Master','guard_name' => 'web','type'=>'edit','enable'=>'0','permission_name'=>'Last Service Date','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_nextservicedate_edit', 'group'=>'Report Master','guard_name' => 'web','type'=>'edit','enable'=>'0','permission_name'=>'Next Service Date','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
            ['name' => 'plantandequipmentReport_condition_edit', 'group'=>'Report Master','guard_name' => 'web','type'=>'edit','enable'=>'0','permission_name'=>'Condition','sub_module_name'=>'listing','module_name'=>'Plant And Equipment Report','module_name_id'=>'58'],
        ];
        foreach ($permission_array as $key => $permission) {
            $admin_role = Role::find(1);
            $getpermission=Permission::where('name',$permission['name'])->first();
            if (empty($getpermission)){
                $permission=Permission::create($permission);
                $admin_role->givePermissionTo($permission['name']);
            }else{
                $permission=Permission::where('name',$getpermission->name)->update($permission);
                $admin_role->givePermissionTo($getpermission->name);
            }
        }
    }
}
