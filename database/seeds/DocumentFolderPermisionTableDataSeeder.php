<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
class DocumentFolderPermisionTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        $permission_array = [
            // create project permissions
            ['name' => 'view_documentfolder','group' => 'Document Master', 'guard_name' => 'web','permission_name'=>'View','type'=>'','sub_module_name'=>'operation','module_name'=>'Document Folder','module_name_id'=>'50'],
            ['name' => 'create_documentfolder','group' => 'Document Master', 'guard_name' => 'web','permission_name'=>'Create','type'=>'','sub_module_name'=>'operation','module_name'=>'Document Folder','module_name_id'=>'50'],
            ['name' => 'edit_documentfolder', 'group' => 'Document Master', 'guard_name' => 'web','permission_name'=>'Edit','type'=>'','sub_module_name'=>'operation','module_name'=>'Document Folder','module_name_id'=>'50'],
            ['name' => 'delete_documentfolder','group' => 'Document Master', 'guard_name' => 'web','permission_name'=>'Delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Document Folder','module_name_id'=>'50'],
            ['name' => 'delete_selected_documentfolder','group' => 'Document Master', 'guard_name' => 'web','permission_name'=>'Selected Delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Document Folder','module_name_id'=>'50'],
            ['name' => 'viewform_group_documentfolder','group' => 'Document Master', 'guard_name' => 'web','permission_name'=>'Selected Delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Document Folder','module_name_id'=>'50'],
            ['name' => 'selected_active_documentfolder', 'group' => 'Document Master','guard_name' => 'web','type'=>'','permission_name'=>'Selected Active Multiple','sub_module_name'=>'operation','module_name'=>'Document Folder','module_name_id'=>'50'],
            ['name' => 'selected_restore_documentfolder', 'group' => 'Document Master','guard_name' => 'web','type'=>'','permission_name'=>'Selected Restore','sub_module_name'=>'operation','module_name'=>'Document Folder','module_name_id'=>'50'],
            ['name' => 'hard_delete_documentfolder','group' => 'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Hard Delete','sub_module_name'=>'operation','module_name'=>'Document Folder','module_name_id'=>'50'],
            ['name' => 'active_documentfolder','group' => 'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Active Employee','sub_module_name'=>'operation','module_name'=>'Document Folder','module_name_id'=>'50'],
            ['name' => 'select_active_documentfolder','group' => 'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Select Active ','sub_module_name'=>'operation','module_name'=>'Document Folder','module_name_id'=>'50'],
            ['name' => 'word_documentfolder', 'group' => 'Document Master','guard_name' => 'web','type'=>'','permission_name'=>'Word Export ','sub_module_name'=>'operation','module_name'=>'Document Folder','module_name_id'=>'50'],
            ['name' => 'csv_documentfolder','group' => 'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Csv Export ','sub_module_name'=>'operation','module_name'=>'Document Folder','module_name_id'=>'50'],
            ['name' => 'pdf_documentfolder', 'group' => 'Document Master','guard_name' => 'web','type'=>'','permission_name'=>'PDF Export ','sub_module_name'=>'operation','module_name'=>'Document Folder','module_name_id'=>'50'],

            //listing in document library
            ['name' => 'documentfolder_checkbox','group' => 'Document Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Document Folder','module_name_id'=>'50'],
            ['name' => 'documentfolder_name', 'group' => 'Document Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Document Folder','module_name_id'=>'50'],

            //word in document library
            ['name' => 'documentfolder_checkbox_word_export','group' => 'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'word_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Document Folder','module_name_id'=>'50'],
            ['name' => 'documentfolder_name_word_export', 'group' => 'Document Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Document Folder','module_name_id'=>'50'],

            // excel in document library
            ['name' => 'documentfolder_checkbox_excel_export','group' => 'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'excel_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Document Folder','module_name_id'=>'50'],
            ['name' => 'documentfolder_name_excel_export', 'group' => 'Document Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Document Folder','module_name_id'=>'50'],

            // pdf in document library
            ['name' => 'documentfolder_checkbox_pdf_export','group' => 'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'pdf_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Document Folder','module_name_id'=>'50'],
            ['name' => 'documentfolder_name_pdf_export', 'group' => 'Document Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Document Folder','module_name_id'=>'50'],


            //create in document library
            ['name' => 'documentfolder_checkbox_create','group' => 'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'create','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Document Folder','module_name_id'=>'50'],
            ['name' => 'documentfolder_name_create', 'group' => 'Document Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Document Folder','module_name_id'=>'50'],


            //edit in document library
            ['name' => 'documentfolder_checkbox_edit','group' => 'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'edit','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Document Folder','module_name_id'=>'50'],
            ['name' => 'documentfolder_name_edit', 'group' => 'Document Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Document Folder','module_name_id'=>'50'],
        ];

        foreach ($permission_array as $key => $permission) {
            $admin_role = Role::find(1);
            $getpermission=Permission::where('name',$permission['name'])->first();
            if (empty($getpermission)){
                $permission=Permission::create($permission);
                $admin_role->givePermissionTo($permission['name']);
            }else{
                $permission=Permission::where('name',$getpermission->name)->update($permission);
                $admin_role->givePermissionTo($getpermission->name);
            }
        }
    }
}
