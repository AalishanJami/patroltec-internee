<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
class DocumentNotesPermissionTableDataSeeder extends Seeder
{
    public function run()
    {
        $permission_array = [
            // create Contact permissions
            ['name' => 'view_documentNotes','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'view','type'=>'','sub_module_name'=>'operation','module_name'=>'Notes'],
            ['name' => 'create_documentNotes','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'create','type'=>'','sub_module_name'=>'operation','module_name'=>'Notes'],
            ['name' => 'edit_documentNotes', 'group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'edit','type'=>'','sub_module_name'=>'operation','module_name'=>'Notes'],
            ['name' => 'delete_documentNotes','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Notes'],
            ['name' => 'selected_delete_documentNotes','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'Selected Delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Notes'],
            ['name' => 'selected_restore_delete_documentNotes', 'group'=>'Document Master','guard_name' => 'web','type'=>'','permission_name'=>'Selected Restore','sub_module_name'=>'operation','module_name'=>'Notes'],
            ['name' => 'restore_delete_documentNotes','group'=>'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Restore Delete','sub_module_name'=>'operation','module_name'=>'Notes'],
            ['name' => 'selected_active_button_documentNotes','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'Selected Active Button','type'=>'','sub_module_name'=>'operation','module_name'=>'Notes'],
            ['name' => 'hard_delete_documentNotes','group'=>'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Hard Delete','sub_module_name'=>'operation','module_name'=>'Notes'],
            ['name' => 'active_documentNotes','group'=>'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Active Document','sub_module_name'=>'operation','module_name'=>'Notes'],
            ['name' => 'word_documentNotes', 'group'=>'Document Master','guard_name' => 'web','type'=>'','permission_name'=>'Word Export Document','sub_module_name'=>'operation','module_name'=>'Notes'],
            ['name' => 'csv_documentNotes','group'=>'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Csv Export Document','sub_module_name'=>'operation','module_name'=>'Notes'],
            ['name' => 'pdf_documentNotes', 'group'=>'Document Master','guard_name' => 'web','type'=>'','permission_name'=>'PDF Export Document','sub_module_name'=>'operation','module_name'=>'Notes'],

            //listing in Contact
            ['name' => 'documentNotes_checkbox','group'=>'Document Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Notes'],
            ['name' => 'documentNotes_notes','group'=>'Document Master', 'guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Notes'],
            ['name' => 'documentNotes_subject', 'group'=>'Document Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Subject','sub_module_name'=>'listing','module_name'=>'Notes'],

            //word in Contact
            ['name' => 'documentNotes_checkbox_word_export','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'word_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Notes'],
            ['name' => 'documentNotes_notes_word_export','group'=>'Document Master', 'guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Notes'],
            ['name' => 'documentNotes_subject_word_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Subject','sub_module_name'=>'listing','module_name'=>'Notes'],

            //excel in Contact
            ['name' => 'documentNotes_checkbox_excel_export','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'excel_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Notes'],
            ['name' => 'documentNotes_notes_excel_export','group'=>'Document Master', 'guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Notes'],
            ['name' => 'documentNotes_subject_excel_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Subject','sub_module_name'=>'listing','module_name'=>'Notes'],

            //pdf in Contact
            ['name' => 'documentNotes_checkbox_pdf_export','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'pdf_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Notes'],
            ['name' => 'documentNotes_notes_pdf_export','group'=>'Document Master', 'guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Notes'],
            ['name' => 'documentNotes_subject_pdf_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Subject','sub_module_name'=>'listing','module_name'=>'Notes'],

            //create in Contact
            ['name' => 'documentNotes_checkbox_create','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'create','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Notes'],
            ['name' => 'documentNotes_notes_create','group'=>'Document Master', 'guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Notes'],
            ['name' => 'documentNotes_subject_create', 'group'=>'Document Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Subject','sub_module_name'=>'listing','module_name'=>'Notes'],

            //edit in Contact
            ['name' => 'documentNotes_checkbox_edit','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'edit','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Notes'],
            ['name' => 'documentNotes_notes_edit','group'=>'Document Master', 'guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Notes'],
            ['name' => 'documentNotes_subject_edit', 'group'=>'Document Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Subject','sub_module_name'=>'listing','module_name'=>'Notes'],
        ];

        foreach ($permission_array as $key => $permission) {
            $admin_role = Role::find(1);
            $getpermission=Permission::where('name',$permission['name'])->first();
            if (empty($getpermission)){
                $permission=Permission::create($permission);
                $admin_role->givePermissionTo($permission['name']);
            }else{
                $permission=Permission::where('name',$getpermission->name)->update($permission);
                $admin_role->givePermissionTo($getpermission->name);
            }
        }
    }
}
