<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //  //php artisan cache:forget spatie.permission.cache

         $this->call(PermissionTableSeeder::class);
//         $this->call(CompanyTableSeeder::class);
//         $this->call(LanguageTableSeeder::class);
         $this->call(LabelsTableSeeder::class);
         $this->call(CompanyLabelsTableSeeder::class);
//         $this->call(AdminDataTableSeeder::class);
         $this->call(RoleDataTableSeeder::class);
         $this->call(PermissionGroupsTableSeeder::class);
         $this->call(PermissionModuleNamesTableSeeder::class);
         $this->call(AnswerTypeTableSeeder::class);
         //project permission
         $this->call(ProjectPermisionTableDataSeeder::class);
         $this->call(BonusScorePermisionTableDataSeeder::class);
         $this->call(AddressPermissionTableDataSeeder::class);
         $this->call(AddressTypePermissionTableDataSeeder::class);
         $this->call(ContactPermissionTableDataSeeder::class);
         $this->call(ContactTypePermissionTableDataSeeder::class);
         $this->call(EquipmentGroupPermissionTableDataSeeder::class);
         $this->call(EquipmentPermissionTableDataSeeder::class);
         $this->call(ProjectQRPermissionTableDataSeeder::class);
         $this->call(ContractorPermissionTableDataSeeder::class);
         $this->call(ManagerNamePermissionTableDataSeeder::class);
         $this->call(ContractManagerNamePermissionTableDataSeeder::class);
         $this->call(DivisionPermissionTableDataSeeder::class);
         $this->call(CustomerPermissionTableDataSeeder::class);

         //$this->call(GroupPermissionTableDataSeeder::class);
          //Client permission
         $this->call(ClientPermissionTableDataSeeder::class);
         $this->call(ClientNotesPermissionTableDataSeeder::class);
         $this->call(ClientUploadPermissionTableDataSeeder::class);
        // Document permision
        $this->call(DocumentLibraryPermisionTableDataSeeder::class);
        $this->call(DocumentFolderPermisionTableDataSeeder::class);
        $this->call(DocumentFormTypePermisionTableDataSeeder::class);
        $this->call(DocumentAnswerPermissionTableDataSeeder::class);
        $this->call(DocumentAnswerGroupPermissionTableDataSeeder::class);
        $this->call(DocumentScoringPermisionTableDataSeeder::class);
        $this->call(DocumentSchedulePermisionTableDataSeeder::class);
        $this->call(DocumentTaskTypePermisionTableDataSeeder::class);
        $this->call(DocumentNotesPermissionTableDataSeeder::class);
        $this->call(DocumentUploadPermissionTableDataSeeder::class);
        $this->call(DocumentStaticFormPermissionTableDataSeeder::class);
        $this->call(DocumentDynamicFormPermissionTableDataSeeder::class);
        $this->call(DocumentQuestionPermissionTableDataSeeder::class);
        $this->call(DocumentVersionlogPermissionTableDataSeeder::class);

        // Employee permision
         $this->call(EmployeePermisionTableDataSeeder::class);
         $this->call(EmployeeStaffPermissionTableDataSeeder::class);
         $this->call(EmployeeStaffGroupPermissionTableDataSeeder::class);
         $this->call(EmployeeStaffRolePermissionTableDataSeeder::class);
         $this->call(EmployeeFilePermissionTableDataSeeder::class);
         $this->call(EmployeeFolderPermissionTableDataSeeder::class);
         $this->call(EmployeeLocationPermissionTableDataSeeder::class);
         $this->call(EmployeeLocationGroupPermissionTableDataSeeder::class);
         $this->call(EmployeeContactPermissionTableDataSeeder::class);
         $this->call(EmployeeRelationshipPermissionTableDataSeeder::class);
         $this->call(EmployeeAccountPermissionTableDataSeeder::class);
         $this->call(EmployeeNotesPermissionTableDataSeeder::class);
         $this->call(EmployeeDocumentPermissionTableDataSeeder::class);
         $this->call(EmployeeNextOfKinPermisionTableDataSeeder::class);
         $this->call(EmployeeLicencePermisionTableDataSeeder::class);
         $this->call(EmployeePositionPermissionTableDataSeeder::class);
         $this->call(EmployeeSexualOrientationPermissionTableDataSeeder::class);
         $this->call(EmployeeEthnicPermissionTableDataSeeder::class);
         $this->call(EmployeeReligionPermissionTableDataSeeder::class);
         $this->call(PermissionsTableSeeder::class);
        //report permission seeder
        $this->call(ReportPlantAndEquipmentPermissionTableDataSeeder::class);
        $this->call(ReportQRPermissionTableDataSeeder::class);
        $this->call(ReportEmailPermissionTableDataSeeder::class);
        $this->call(SupportPermissionTableDataSeeder::class);
        $this->call(JobPermissionTableDataSeeder::class);
        //run seeder for group permission
        //PermissionGroupsTableSeeder first
        //PermissionModuleNamesTableSeeder second
        //PermissionsTableSeeder third
        //RoleDataTableSeeder fourth
//        $this->call(ZaoorLabelsTableSeeder::class);
//        $this->call(ZaoorCompanyLabelsTableSeeder::class);
    }
}
