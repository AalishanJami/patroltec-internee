<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class JobPermissionTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $permission_array = [
            // create Job permissions
            ['name' => 'view_job','group'=>'Job Master', 'guard_name' => 'web','permission_name'=>'view','type'=>'','sub_module_name'=>'operation','module_name'=>'Jobs','module_name_id'=>'61'],
             ['name' => 'create_job','group'=>'Job Master', 'guard_name' => 'web','permission_name'=>'Create','type'=>'','sub_module_name'=>'operation','module_name'=>'Jobs','module_name_id'=>'61'],
              ['name' => 'delete_job','group'=>'Job Master', 'guard_name' => 'web','permission_name'=>'Delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Jobs','module_name_id'=>'61'],
          

            //listing in Job
            ['name' => 'job_checkbox','group'=>'Job Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Jobs','module_name_id'=>'61'],
            ['name' => 'job_form_name','group'=>'Job Master', 'guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Form Name','sub_module_name'=>'listing','module_name'=>'Jobs','module_name_id'=>'61'],
            ['name' => 'job_location', 'group'=>'Job Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Location','sub_module_name'=>'listing','module_name'=>'Jobs','module_name_id'=>'61'],
            ['name' => 'job_location_break_down', 'group'=>'Job Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Location BreakDown','sub_module_name'=>'listing','module_name'=>'Jobs','module_name_id'=>'61'],
               ['name' => 'job_user', 'group'=>'Job Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'User','sub_module_name'=>'listing','module_name'=>'Jobs','module_name_id'=>'61'],
               ['name' => 'job_date_time', 'group'=>'Job Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Date n Time','sub_module_name'=>'listing','module_name'=>'Jobs','module_name_id'=>'61'],
           
        
            //create in Scoring
            ['name' => 'job_project_name_create','group'=>'Job Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'create','permission_name'=>'Project','sub_module_name'=>'listing','module_name'=>'Jobs','module_name_id'=>'61'],
            ['name' => 'job_assign_create','group'=>'Job Master', 'guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Assign To','sub_module_name'=>'listing','module_name'=>'Jobs','module_name_id'=>'61'],
            ['name' => 'job_location_create', 'group'=>'Job Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Location','sub_module_name'=>'listing','module_name'=>'Jobs','module_name_id'=>'61'],
            ['name' => 'job_date_create', 'group'=>'Job Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Date','sub_module_name'=>'listing','module_name'=>'Jobs','module_name_id'=>'61'],
              ['name' => 'job_time_create','group'=>'Job Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'create','permission_name'=>'Time','sub_module_name'=>'listing','module_name'=>'Jobs','module_name_id'=>'61'],
            ['name' => 'job_pre_create','group'=>'Job Master', 'guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'pre','sub_module_name'=>'listing','module_name'=>'Jobs','module_name_id'=>'61'],
            ['name' => 'job_pre_time_create', 'group'=>'Job Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'time','sub_module_name'=>'listing','module_name'=>'Jobs','module_name_id'=>'61'],
            ['name' => 'job_post_create', 'group'=>'Job Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Post','sub_module_name'=>'listing','module_name'=>'Jobs','module_name_id'=>'61'],
            ['name' => 'job_post_time_create', 'group'=>'Job Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'time','sub_module_name'=>'listing','module_name'=>'Jobs','module_name_id'=>'61'],
              ['name' => 'job_user_create','group'=>'Job Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'create','permission_name'=>'user','sub_module_name'=>'listing','module_name'=>'Jobs','module_name_id'=>'61'],
            ['name' => 'job_form_type_create','group'=>'Job Master', 'guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Form Type','sub_module_name'=>'listing','module_name'=>'Jobs','module_name_id'=>'61'],
            ['name' => 'job_form_create', 'group'=>'Job Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Form','sub_module_name'=>'listing','module_name'=>'Jobs','module_name_id'=>'61'],
            ['name' => 'job_populate_create', 'group'=>'Prepopulate','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Upload','sub_module_name'=>'listing','module_name'=>'Jobs','module_name_id'=>'61'],
             ['name' => 'job_populate_checkbox_create', 'group'=>'Job Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Prepopulate checkbox','sub_module_name'=>'listing','module_name'=>'Jobs','module_name_id'=>'61'],
            ['name' => 'job_total_cost_create', 'group'=>'Job Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'total cost','sub_module_name'=>'listing','module_name'=>'Jobs','module_name_id'=>'61'],
              ['name' => 'job_bill_info_create','group'=>'Job Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'create','permission_name'=>'bill info','sub_module_name'=>'listing','module_name'=>'Jobs','module_name_id'=>'61'],
            ['name' => 'job_note_create','group'=>'Job Master', 'guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Jobs','module_name_id'=>'61'],
            ['name' => 'job_upload_create', 'group'=>'Job Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Upload','sub_module_name'=>'listing','module_name'=>'Jobs','module_name_id'=>'61'],
           
        ];
          foreach ($permission_array as $key => $permission) {
            $admin_role = Role::find(1);
            $getpermission=Permission::where('name',$permission['name'])->first();
            if (empty($getpermission)){
                $permission=Permission::create($permission);
                $admin_role->givePermissionTo($permission['name']);
            }else{
                $permission=Permission::where('name',$getpermission->name)->update($permission);
                $admin_role->givePermissionTo($getpermission->name);
            }
        }
    }
}
