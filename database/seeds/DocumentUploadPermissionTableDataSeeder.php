<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
class DocumentUploadPermissionTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission_array = [
            // create Contact permissions
            ['name' => 'view_documentUpload','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'view','type'=>'','sub_module_name'=>'operation','module_name'=>'Document Upload'],
            ['name' => 'create_documentUpload','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'create','type'=>'','sub_module_name'=>'operation','module_name'=>'Document Upload'],
            ['name' => 'edit_documentUpload', 'group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'edit','type'=>'','sub_module_name'=>'operation','module_name'=>'Document Upload'],
            ['name' => 'delete_documentUpload','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Document Upload'],
            ['name' => 'selected_delete_documentUpload','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'Selected Delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Document Upload'],
            ['name' => 'selected_restore_delete_documentUpload', 'group'=>'Document Master','guard_name' => 'web','type'=>'','permission_name'=>'Selected Restore','sub_module_name'=>'operation','module_name'=>'Document Upload'],
            ['name' => 'restore_delete_documentUpload','group'=>'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Restore Delete','sub_module_name'=>'operation','module_name'=>'Document Upload'],
            ['name' => 'hard_delete_documentUpload','group'=>'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Hard Delete','sub_module_name'=>'operation','module_name'=>'Document Upload'],
            ['name' => 'download_documentUpload','group'=>'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Hard Delete','sub_module_name'=>'operation','module_name'=>'Document Upload'],
            ['name' => 'selected_active_documentUpload','group'=>'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Selected Active Client','sub_module_name'=>'operation','module_name'=>'Document Upload'],
            ['name' => 'active_documentUpload','group'=>'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Active Client','sub_module_name'=>'operation','module_name'=>'Document Upload'],
            ['name' => 'word_documentUpload', 'group'=>'Document Master','guard_name' => 'web','type'=>'','permission_name'=>'Word Export Client','sub_module_name'=>'operation','module_name'=>'Document Upload'],
            ['name' => 'csv_documentUpload','group'=>'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Csv Export Client','sub_module_name'=>'operation','module_name'=>'Document Upload'],
            ['name' => 'pdf_documentUpload', 'group'=>'Document Master','guard_name' => 'web','type'=>'','permission_name'=>'PDF Export Client','sub_module_name'=>'operation','module_name'=>'Document Upload'],

            //listing in Contact
            ['name' => 'documentUpload_checkbox','group'=>'Document Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Document Upload'],
            ['name' => 'documentUpload_notes','group'=>'Document Master', 'guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Document Upload'],
            ['name' => 'documentUpload_title', 'group'=>'Document Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Title','sub_module_name'=>'listing','module_name'=>'Document Upload'],
            ['name' => 'documentUpload_upload', 'group'=>'Document Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Upload','sub_module_name'=>'listing','module_name'=>'Document Upload'],
            ['name' => 'documentUpload_status', 'group'=>'Document Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Status','sub_module_name'=>'listing','module_name'=>'Document Upload'],

            //word in Contact
            ['name' => 'documentUpload_checkbox_word_export','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'word_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Document Upload'],
            ['name' => 'documentUpload_notes_word_export','group'=>'Document Master', 'guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Document Upload'],
            ['name' => 'documentUpload_title_word_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Title','sub_module_name'=>'listing','module_name'=>'Document Upload'],
            ['name' => 'documentUpload_upload_word_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Upload','sub_module_name'=>'listing','module_name'=>'Document Upload'],
            ['name' => 'documentUpload_status_word_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Status','sub_module_name'=>'listing','module_name'=>'Document Upload'],

            //excel in Contact
            ['name' => 'documentUpload_checkbox_excel_export','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'excel_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Document Upload'],
            ['name' => 'documentUpload_notes_excel_export','group'=>'Document Master', 'guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Document Upload'],
            ['name' => 'documentUpload_title_excel_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Title','sub_module_name'=>'listing','module_name'=>'Document Upload'],
            ['name' => 'documentUpload_upload_excel_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Upload','sub_module_name'=>'listing','module_name'=>'Document Upload'],
            ['name' => 'documentUpload_status_excel_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Status','sub_module_name'=>'listing','module_name'=>'Document Upload'],

            //pdf in Contact
            ['name' => 'documentUpload_checkbox_pdf_export','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'pdf_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Document Upload'],
            ['name' => 'documentUpload_notes_pdf_export','group'=>'Document Master', 'guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Document Upload'],
            ['name' => 'documentUpload_title_pdf_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Title','sub_module_name'=>'listing','module_name'=>'Document Upload'],
            ['name' => 'documentUpload_upload_pdf_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Upload','sub_module_name'=>'listing','module_name'=>'Document Upload'],
            ['name' => 'documentUpload_status_pdf_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Status','sub_module_name'=>'listing','module_name'=>'Document Upload'],

            //create in Contact
            ['name' => 'documentUpload_checkbox_create','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'create','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Document Upload'],
            ['name' => 'documentUpload_notes_create','group'=>'Document Master', 'guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Document Upload'],
            ['name' => 'documentUpload_title_create', 'group'=>'Document Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Title','sub_module_name'=>'listing','module_name'=>'Document Upload'],
            ['name' => 'documentUpload_upload_create', 'group'=>'Document Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Upload','sub_module_name'=>'listing','module_name'=>'Document Upload'],
            ['name' => 'documentUpload_status_create', 'group'=>'Document Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Status','sub_module_name'=>'listing','module_name'=>'Document Upload'],

            //edit in Contact
            ['name' => 'documentUpload_checkbox_edit','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'edit','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Document Upload'],
            ['name' => 'documentUpload_notes_edit','group'=>'Document Master', 'guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Document Upload'],
            ['name' => 'documentUpload_title_edit', 'group'=>'Document Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Title','sub_module_name'=>'listing','module_name'=>'Document Upload'],
            ['name' => 'documentUpload_upload_edit', 'group'=>'Document Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Upload','sub_module_name'=>'listing','module_name'=>'Document Upload'],
            ['name' => 'documentUpload_status_edit', 'group'=>'Document Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Status','sub_module_name'=>'listing','module_name'=>'Document Upload'],
        ];

        foreach ($permission_array as $key => $permission) {
            $admin_role = Role::find(1);
            $getpermission=Permission::where('name',$permission['name'])->first();
            if (empty($getpermission)){
                $permission=Permission::create($permission);
                $admin_role->givePermissionTo($permission['name']);
            }else{
                $permission=Permission::where('name',$getpermission->name)->update($permission);
                $admin_role->givePermissionTo($getpermission->name);
            }
        }
    }
}
