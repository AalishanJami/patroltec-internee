<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class BonusScorePermisionTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission_array = [
            // create Bonus Score permissions
            ['name' => 'view_bonusScore','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'view','type'=>'','sub_module_name'=>'operation','module_name'=>'Bonus Score'],
            ['name' => 'create_bonusScore','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'create','type'=>'','sub_module_name'=>'operation','module_name'=>'Bonus Score'],
            ['name' => 'edit_bonusScore', 'group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'edit','type'=>'','sub_module_name'=>'operation','module_name'=>'Bonus Score'],
            ['name' => 'delete_bonusScore','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Bonus Score'],
            ['name' => 'selected_delete_bonusScore','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'Selected Delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Bonus Score'],
            ['name' => 'selected_restore_delete_bonusScore', 'group'=>'Project Master','guard_name' => 'web','type'=>'','permission_name'=>'Selected Restore','sub_module_name'=>'operation','module_name'=>'Bonus Score'],
            ['name' => 'selected_active_bonusScore', 'group'=>'Project Master','guard_name' => 'web','type'=>'','permission_name'=>'Selected Active Multiple','sub_module_name'=>'operation','module_name'=>'Bonus Score'],
            ['name' => 'restore_delete_bonusScore','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Restore Delete','sub_module_name'=>'operation','module_name'=>'Bonus Score'],
            ['name' => 'hard_delete_bonusScore','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Hard Delete','sub_module_name'=>'operation','module_name'=>'Bonus Score'],
            ['name' => 'active_bonusScore','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Active Bonus Score','sub_module_name'=>'operation','module_name'=>'Bonus Score'],
            ['name' => 'word_bonusScore', 'group'=>'Project Master','guard_name' => 'web','type'=>'','permission_name'=>'Word Export Bonus Score','sub_module_name'=>'operation','module_name'=>'Bonus Score'],
            ['name' => 'csv_bonusScore','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Csv Export Bonus Score','sub_module_name'=>'operation','module_name'=>'Bonus Score'],
            ['name' => 'pdf_bonusScore', 'group'=>'Project Master','guard_name' => 'web','type'=>'','permission_name'=>'PDF Export Bonus Score','sub_module_name'=>'operation','module_name'=>'Bonus Score'],

            //listing in bonus score
            ['name' => 'bonusScore_checkbox','group'=>'Project Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Bonus Score'],
            ['name' => 'bonusScore_month', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'month','sub_module_name'=>'listing','module_name'=>'Bonus Score'],
            ['name' => 'bonusScore_year','group'=>'Project Master', 'guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'year','sub_module_name'=>'listing','module_name'=>'Bonus Score'],
            ['name' => 'bonusScore_score', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'score','sub_module_name'=>'listing','module_name'=>'Bonus Score'],
            ['name' => 'bonusScore_notes', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'notes','sub_module_name'=>'listing','module_name'=>'Bonus Score'],

            //word in bonus score
            ['name' => 'bonusScore_checkbox_word_export','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'word_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Bonus Score'],
            ['name' => 'bonusScore_month_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'month','sub_module_name'=>'listing','module_name'=>'Bonus Score'],
            ['name' => 'bonusScore_year_word_export','group'=>'Project Master', 'guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'year','sub_module_name'=>'listing','module_name'=>'Bonus Score'],
            ['name' => 'bonusScore_score_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'score','sub_module_name'=>'listing','module_name'=>'Bonus Score'],
            ['name' => 'bonusScore_notes_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'notes','sub_module_name'=>'listing','module_name'=>'Bonus Score'],

            //excel in bonus score
            ['name' => 'bonusScore_checkbox_excel_export','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'excel_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Bonus Score'],
            ['name' => 'bonusScore_month_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'month','sub_module_name'=>'listing','module_name'=>'Bonus Score'],
            ['name' => 'bonusScore_year_excel_export','group'=>'Project Master', 'guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'year','sub_module_name'=>'listing','module_name'=>'Bonus Score'],
            ['name' => 'bonusScore_score_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'score','sub_module_name'=>'listing','module_name'=>'Bonus Score'],
            ['name' => 'bonusScore_notes_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'notes','sub_module_name'=>'listing','module_name'=>'Bonus Score'],

            //pdf in bonus score
            ['name' => 'bonusScore_checkbox_pdf_export','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'pdf_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Bonus Score'],
            ['name' => 'bonusScore_month_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'month','sub_module_name'=>'listing','module_name'=>'Bonus Score'],
            ['name' => 'bonusScore_year_pdf_export','group'=>'Project Master', 'guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'year','sub_module_name'=>'listing','module_name'=>'Bonus Score'],
            ['name' => 'bonusScore_score_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'score','sub_module_name'=>'listing','module_name'=>'Bonus Score'],
            ['name' => 'bonusScore_notes_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'notes','sub_module_name'=>'listing','module_name'=>'Bonus Score'],

            //create in Bonus Score
            ['name' => 'bonusScore_checkbox_create','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'create','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Bonus Score'],
            ['name' => 'bonusScore_month_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'month','sub_module_name'=>'listing','module_name'=>'Bonus Score'],
            ['name' => 'bonusScore_year_create','group'=>'Project Master', 'guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'year','sub_module_name'=>'listing','module_name'=>'Bonus Score'],
            ['name' => 'bonusScore_score_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'score','sub_module_name'=>'listing','module_name'=>'Bonus Score'],
            ['name' => 'bonusScore_notes_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'notes','sub_module_name'=>'listing','module_name'=>'Bonus Score'],

            //edit in Bonus Score
            ['name' => 'bonusScore_checkbox_edit','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'edit','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Bonus Score'],
            ['name' => 'bonusScore_month_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'month','sub_module_name'=>'listing','module_name'=>'Bonus Score'],
            ['name' => 'bonusScore_year_edit','group'=>'Project Master', 'guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'year','sub_module_name'=>'listing','module_name'=>'Bonus Score'],
            ['name' => 'bonusScore_score_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'score','sub_module_name'=>'listing','module_name'=>'Bonus Score'],
            ['name' => 'bonusScore_notes_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'notes','sub_module_name'=>'listing','module_name'=>'Bonus Score'],
        ];
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $address = Permission::where(['module_name'=>'bonusScore'])->get();
        if (count($address)>0){
            Permission::where(['module_name'=>'bonusScore'])->delete();
        }
        foreach ($permission_array as $key => $permission) {
            $admin_role = Role::find(1);
            $getpermission=Permission::where('name',$permission['name'])->first();
            if (empty($getpermission)){
                $permission=Permission::create($permission);
                $admin_role->givePermissionTo($permission['name']);
            }else{
                $permission=Permission::where('name',$getpermission->name)->update($permission);
                $admin_role->givePermissionTo($getpermission->name);
            }
        }
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
