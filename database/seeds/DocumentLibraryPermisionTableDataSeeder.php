<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
class DocumentLibraryPermisionTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        $permission_array = [
            // create project permissions
            ['name' => 'view_documentlibrary','group' => 'Document Master', 'guard_name' => 'web','permission_name'=>'View','type'=>'','sub_module_name'=>'operation','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'create_documentlibrary','group' => 'Document Master', 'guard_name' => 'web','permission_name'=>'Create','type'=>'','sub_module_name'=>'operation','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'edit_documentlibrary', 'group' => 'Document Master', 'guard_name' => 'web','permission_name'=>'Edit','type'=>'','sub_module_name'=>'operation','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'versionlog_documentlibrary', 'group' => 'Document Master', 'guard_name' => 'web','permission_name'=>'Version Log','type'=>'','sub_module_name'=>'operation','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'selectedForm_documentlibrary', 'group' => 'Document Master', 'guard_name' => 'web','permission_name'=>'Selected Form','type'=>'','sub_module_name'=>'operation','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'viewregister_documentlibrary', 'group' => 'Document Master', 'guard_name' => 'web','permission_name'=>'View Register','type'=>'','sub_module_name'=>'operation','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'delete_documentlibrary','group' => 'Document Master', 'guard_name' => 'web','permission_name'=>'Delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'delete_selected_documentlibrary','group' => 'Document Master', 'guard_name' => 'web','permission_name'=>'Selected Delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'selected_active_documentlibrary', 'group' => 'Document Master','guard_name' => 'web','type'=>'','permission_name'=>'Selected Active Multiple','sub_module_name'=>'operation','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'selected_restore_documentlibrary', 'group' => 'Document Master','guard_name' => 'web','type'=>'','permission_name'=>'Selected Restore','sub_module_name'=>'operation','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'hard_delete_documentlibrary','group' => 'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Hard Delete','sub_module_name'=>'operation','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'active_documentlibrary','group' => 'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Active Employee','sub_module_name'=>'operation','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'select_active_documentlibrary','group' => 'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Select Active ','sub_module_name'=>'operation','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'word_documentlibrary', 'group' => 'Document Master','guard_name' => 'web','type'=>'','permission_name'=>'Word Export ','sub_module_name'=>'operation','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'csv_documentlibrary','group' => 'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Csv Export ','sub_module_name'=>'operation','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'pdf_documentlibrary', 'group' => 'Document Master','guard_name' => 'web','type'=>'','permission_name'=>'PDF Export ','sub_module_name'=>'operation','module_name'=>'Document Library','module_name_id'=>'49'],

            //listing in document library
            ['name' => 'documentlibrary_checkbox','group' => 'Document Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_name', 'group' => 'Document Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_ref','group' => 'Document Master', 'guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Reference','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_commonQuestion', 'group' => 'Document Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Common Questions','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_locationType', 'group' => 'Document Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Location Type','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_formsetting', 'group' => 'Document Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Form Setting','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_download', 'group' => 'Document Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Download','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_notes', 'group' => 'Document Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_lockJobFrom', 'group' => 'Document Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Lock Job From','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_lockJobBefore', 'group' => 'Document Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Lock Job Before','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_validFrom', 'group' => 'Document Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Valid From','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_validTo', 'group' => 'Document Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Valid To','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],

            //word in document library
            ['name' => 'documentlibrary_checkbox_word_export','group' => 'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'word_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_name_word_export', 'group' => 'Document Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_ref_word_export','group' => 'Document Master', 'guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Reference','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_commonQuestion_word_export', 'group' => 'Document Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Common Questions','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_locationType_word_export', 'group' => 'Document Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Location Type','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_formsetting_word_export', 'group' => 'Document Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Form Setting','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_download_word_export', 'group' => 'Document Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Download','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_notes_word_export', 'group' => 'Document Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_lockJobFrom_word_export', 'group' => 'Document Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Lock Job From','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_lockJobBefore_word_export', 'group' => 'Document Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Lock Job Before','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_validFrom_word_export', 'group' => 'Document Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Valid From','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_validTo_word_export', 'group' => 'Document Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Valid To','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],

            // excel in document library
            ['name' => 'documentlibrary_checkbox_excel_export','group' => 'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'excel_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_name_excel_export', 'group' => 'Document Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_ref_excel_export','group' => 'Document Master', 'guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Reference','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_commonQuestion_excel_export', 'group' => 'Document Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Common Questions','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_locationType_excel_export', 'group' => 'Document Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Location Type','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_formsetting_excel_export', 'group' => 'Document Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Form Setting','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_download_excel_export', 'group' => 'Document Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Download','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_notes_excel_export', 'group' => 'Document Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_lockJobFrom_excel_export', 'group' => 'Document Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Lock Job From','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_lockJobBefore_excel_export', 'group' => 'Document Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Lock Job Before','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_validFrom_excel_export', 'group' => 'Document Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Valid From','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_validTo_excel_export', 'group' => 'Document Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Valid To','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],

            // pdf in document library
            ['name' => 'documentlibrary_checkbox_pdf_export','group' => 'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'pdf_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_name_pdf_export', 'group' => 'Document Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_ref_pdf_export','group' => 'Document Master', 'guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Reference','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_commonQuestion_pdf_export', 'group' => 'Document Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Common Questions','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_locationType_pdf_export', 'group' => 'Document Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Location Type','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_formsetting_pdf_export', 'group' => 'Document Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Form Setting','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_download_pdf_export', 'group' => 'Document Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Download','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_notes_pdf_export', 'group' => 'Document Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_lockJobFrom_pdf_export', 'group' => 'Document Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Lock Job From','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_lockJobBefore_pdf_export', 'group' => 'Document Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Lock Job Before','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_validFrom_pdf_export', 'group' => 'Document Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Valid From','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_validTo_pdf_export', 'group' => 'Document Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Valid To','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],


            //create in document library
            ['name' => 'documentlibrary_checkbox_create','group' => 'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'create','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_name_create', 'group' => 'Document Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_ref_create','group' => 'Document Master', 'guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Reference','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_commonQuestion_create', 'group' => 'Document Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Common Questions','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_locationType_create', 'group' => 'Document Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Location Type','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_formsetting_create', 'group' => 'Document Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Form Setting','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_download_create', 'group' => 'Document Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Download','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_notes_create', 'group' => 'Document Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_lockJobFrom_create', 'group' => 'Document Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Lock Job From','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_lockJobBefore_create', 'group' => 'Document Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Lock Job Before','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_validFrom_create', 'group' => 'Document Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Valid From','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_validTo_create', 'group' => 'Document Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Valid To','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],


            //edit in document library
            ['name' => 'documentlibrary_checkbox_edit','group' => 'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'edit','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_name_edit', 'group' => 'Document Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_ref_edit','group' => 'Document Master', 'guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Reference','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_commonQuestion_edit', 'group' => 'Document Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Common Questions','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_locationType_edit', 'group' => 'Document Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Location Type','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_formsetting_edit', 'group' => 'Document Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Form Setting','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_download_edit', 'group' => 'Document Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Download','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_notes_edit', 'group' => 'Document Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_lockJobFrom_edit', 'group' => 'Document Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Lock Job From','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_lockJobBefore_edit', 'group' => 'Document Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Lock Job Before','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_validFrom_edit', 'group' => 'Document Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Valid From','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
            ['name' => 'documentlibrary_validTo_edit', 'group' => 'Document Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Valid To','sub_module_name'=>'listing','module_name'=>'Document Library','module_name_id'=>'49'],
        ];

        foreach ($permission_array as $key => $permission) {
            $admin_role = Role::find(1);
            $getpermission=Permission::where('name',$permission['name'])->first();
            if (empty($getpermission)){
                $permission=Permission::create($permission);
                $admin_role->givePermissionTo($permission['name']);
            }else{
                $permission=Permission::where('name',$getpermission->name)->update($permission);
                $admin_role->givePermissionTo($getpermission->name);
            }
        }
    }
}
