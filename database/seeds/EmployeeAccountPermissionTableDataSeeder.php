<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
class EmployeeAccountPermissionTableDataSeeder extends Seeder
{
    public function run()
    {
        $permission_array = [
            ['name' => 'view_employeeAccount','group'=>'Employee Master', 'guard_name' => 'web','permission_name'=>'view','type'=>'','sub_module_name'=>'operation','module_name'=>'Account'],
            //edit in Contact
            ['name' => 'employeeAccount_username_edit','group'=>'Employee Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'edit','permission_name'=>'Username','sub_module_name'=>'listing','module_name'=>'Account'],
            ['name' => 'employeeAccount_password_edit','group'=>'Employee Master', 'guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Password','sub_module_name'=>'listing','module_name'=>'Account'],
            ['name' => 'employeeAccount_user_group_edit', 'group'=>'Employee Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'UserGroup','sub_module_name'=>'listing','module_name'=>'Account'],
        ];

        foreach ($permission_array as $key => $permission) {
            $admin_role = Role::find(1);
            $getpermission=Permission::where('name',$permission['name'])->first();
            if (empty($getpermission)){
                $permission=Permission::create($permission);
                $admin_role->givePermissionTo($permission['name']);
            }else{
                $permission=Permission::where('name',$getpermission->name)->update($permission);
                $admin_role->givePermissionTo($getpermission->name);
            }
        }
    }
}
