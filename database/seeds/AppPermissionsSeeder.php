<?php

use Illuminate\Database\Seeder;

class AppPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         \DB::table('app_permissions')->insert(array (
        	0 => 
            array (
                'id' => 1,
                'name' => 'view_jobs',
                'type' => 'view',
                'group_name' => 'Jobs',
                'app_permission_module_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'view_completed_jobs',
                'type' => 'view',
                'group_name' => 'Completed Jobs',
                'app_permission_module_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'edit_completed_jobs',
                'type' => 'edit',
                'group_name' => 'Completed Jobs',
                'app_permission_module_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'view_forms',
                'type' => 'view',
                'group_name' => 'Forms',
                'app_permission_module_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'view_completed_forms',
                'type' => 'view',
                'group_name' => 'Completed Forms',
                'app_permission_module_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'edit_completed_forms',
                'type' => 'edit',
                'group_name' => 'Completed Forms',
                'app_permission_module_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'view_raise_form',
                'type' => 'view',
                'group_name' => 'Raise Form',
                'app_permission_module_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'view_write_tags',
                'type' => 'view',
                'group_name' => 'Write Tags',
                'app_permission_module_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'view_write_keys',
                'type' => 'view',
                'group_name' => 'Write Keys',
                'app_permission_module_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'view_faq',
                'type' => 'view',
                'group_name' => 'FAQs',
                'app_permission_module_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'view_contact_us',
                'type' => 'view',
                'group_name' => 'Contact Us',
                'app_permission_module_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'view_profile',
                'type' => 'view',
                'group_name' => 'Profile',
                'app_permission_module_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'view_customers',
                'type' => 'view',
                'group_name' => 'Customers',
                'app_permission_module_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'name' => 'view_about_us',
                'type' => 'view',
                'group_name' => 'About Us',
                'app_permission_module_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'name' => 'view_share_account',
                'type' => 'view',
                'group_name' => 'Share Account',
                'app_permission_module_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
    }
}
