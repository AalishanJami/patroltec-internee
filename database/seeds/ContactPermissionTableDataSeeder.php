<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class ContactPermissionTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission_array = [
            // create Contact permissions
            ['name' => 'view_contacts','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'view','type'=>'','sub_module_name'=>'operation','module_name'=>'Contact'],
            ['name' => 'create_contacts','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'create','type'=>'','sub_module_name'=>'operation','module_name'=>'Contact'],
            ['name' => 'edit_contacts', 'group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'edit','type'=>'','sub_module_name'=>'operation','module_name'=>'Contact'],
            ['name' => 'delete_contacts','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Contact'],
            ['name' => 'selected_delete_contacts','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'Selected Delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Contact'],
            ['name' => 'selected_restore_delete_contacts', 'group'=>'Project Master','guard_name' => 'web','type'=>'','permission_name'=>'Selected Restore','sub_module_name'=>'operation','module_name'=>'Contact'],
            ['name' => 'restore_delete_contacts','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Restore Delete','sub_module_name'=>'operation','module_name'=>'Contact'],
            ['name' => 'hard_delete_contacts','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Hard Delete','sub_module_name'=>'operation','module_name'=>'Contact'],
            ['name' => 'active_contacts','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Active Contact','sub_module_name'=>'operation','module_name'=>'Contact'],
            ['name' => 'word_contacts', 'group'=>'Project Master','guard_name' => 'web','type'=>'','permission_name'=>'Word Export Contact','sub_module_name'=>'operation','module_name'=>'Contact'],
            ['name' => 'csv_contacts','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Csv Export Contact','sub_module_name'=>'operation','module_name'=>'Contact'],
            ['name' => 'pdf_contacts', 'group'=>'Project Master','guard_name' => 'web','type'=>'','permission_name'=>'PDF Export Contact','sub_module_name'=>'operation','module_name'=>'Contact'],

            //listing in Contact
            ['name' => 'contacts_checkbox','group'=>'Project Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_contactType', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'contactType','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_title','group'=>'Project Master', 'guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'title','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_firstname', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'firstname','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_name', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'name','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_surname', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'surname','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_email', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'email','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_phonenumber', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'phonenumber','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_extension', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'extension','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_mobilenumber', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'mobilenumber','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_othernumber', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'othernumber','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_buildingname', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'buildingname','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_street', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'street','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_town', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'town','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_state', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'state','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_country', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'country','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_postcode', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'postcode','sub_module_name'=>'listing','module_name'=>'Contact'],

            //word in Contact
            ['name' => 'contacts_checkbox_word_export','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'word_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_contactType_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'contactType','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_title_word_export','group'=>'Project Master', 'guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'title','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_firstname_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'firstname','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_name_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'name','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_surname_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'surname','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_email_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'email','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_phonenumber_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'phonenumber','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_extension_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'extension','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_mobilenumber_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'mobilenumber','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_othernumber_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'othernumber','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_buildingname_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'buildingname','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_street_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'street','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_town_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'town','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_state_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'state','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_country_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'country','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_postcode_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'postcode','sub_module_name'=>'listing','module_name'=>'Contact'],

            //excel in Contact
            ['name' => 'contacts_checkbox_excel_export','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'excel_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_contactType_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'contactType','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_title_excel_export','group'=>'Project Master', 'guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'title','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_firstname_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'firstname','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_name_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'name','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_surname_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'surname','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_email_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'email','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_phonenumber_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'phonenumber','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_extension_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'extension','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_mobilenumber_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'mobilenumber','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_othernumber_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'othernumber','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_buildingname_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'buildingname','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_street_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'street','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_town_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'town','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_state_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'state','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_country_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'country','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_postcode_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'postcode','sub_module_name'=>'listing','module_name'=>'Contact'],

            //pdf in Contact
            ['name' => 'contacts_checkbox_pdf_export','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'pdf_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_contactType_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'contactType','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_title_pdf_export','group'=>'Project Master', 'guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'title','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_firstname_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'firstname','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_name_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'name','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_surname_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'surname','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_email_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'email','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_phonenumber_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'phonenumber','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_extension_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'extension','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_mobilenumber_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'mobilenumber','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_othernumber_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'othernumber','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_buildingname_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'buildingname','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_street_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'street','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_town_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'town','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_state_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'state','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_country_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'country','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_postcode_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'postcode','sub_module_name'=>'listing','module_name'=>'Contact'],

            //create in Contact
            ['name' => 'contacts_checkbox_create','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'create','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_contactType_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'contactType','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_title_create','group'=>'Project Master', 'guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'title','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_firstname_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'firstname','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_name_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'name','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_surname_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'surname','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_email_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'email','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_phonenumber_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'phonenumber','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_extension_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'extension','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_mobilenumber_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'mobilenumber','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_othernumber_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'othernumber','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_buildingname_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'buildingname','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_street_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'street','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_town_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'town','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_state_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'state','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_country_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'country','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_postcode_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'postcode','sub_module_name'=>'listing','module_name'=>'Contact'],

            //edit in Contact
            ['name' => 'contacts_checkbox_edit','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'edit','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_contactType_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'contactType','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_title_edit','group'=>'Project Master', 'guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'title','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_firstname_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'firstname','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_name_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'name','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_surname_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'surname','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_email_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'email','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_phonenumber_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'phonenumber','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_extension_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'extension','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_mobilenumber_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'mobilenumber','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_othernumber_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'othernumber','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_buildingname_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'buildingname','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_street_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'street','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_town_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'town','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_state_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'state','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_country_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'country','sub_module_name'=>'listing','module_name'=>'Contact'],
            ['name' => 'contacts_postcode_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'postcode','sub_module_name'=>'listing','module_name'=>'Contact'],
        ];
        foreach ($permission_array as $key => $permission) {
            $admin_role = Role::find(1);
            $getpermission=Permission::where('name',$permission['name'])->first();
            if (empty($getpermission)){
                $permission=Permission::create($permission);
                $admin_role->givePermissionTo($permission['name']);
            }else{
                $permission=Permission::where('name',$getpermission->name)->update($permission);
                $admin_role->givePermissionTo($getpermission->name);
            }
        }
    }
}
