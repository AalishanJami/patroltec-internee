<?php

use Illuminate\Database\Seeder;
use App\AnswerTypes;
class AnswerTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$data = [
			['name' => 'radio','active' => '1'],
			['name' => 'multi','active' => '1'],
			['name' => 'checkbox','active' => '1'],
			['name' => 'dropdown','active' => '1'],
			['name' => 'date','active' => '1'],
			['name' => 'time','active' => '1'],
			// ['name' => 'datetime','active' => '1'],
			['name' => 'text','active' => '1'],
			['name' => 'comment','active' => '1'],
			['name' => 'richtext','active' => '1'],
			['name' => 'signature text','active' => '1'],
			['name' => 'signature user','active' => '1'],
			['name' => 'signature select','active' => '1'],
			['name' => 'clocs','active' => '1'],
		];
        DB::statement('SET FOREIGN_KEY_CHECKS=0;
			');
        DB::table('answer_types')->truncate();
		foreach ($data as $key => $value) {
		    $answertype=AnswerTypes::where('name',$value['name'])->first();
		    if (empty($answertype)){
                AnswerTypes::create($value);
            }else{
                AnswerTypes::where('name',$answertype->name)->update($value);
            }
		}
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }


}
