<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
class EmployeeNextOfKinPermisionTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        $permission_array = [
            // create project permissions
            ['name' => 'view_nextofkin','group'=>'Employee Master', 'guard_name' => 'web','permission_name'=>'view','type'=>'','sub_module_name'=>'operation','module_name'=>'Next Of Kin'],
            ['name' => 'create_nextofkin','group'=>'Employee Master', 'guard_name' => 'web','permission_name'=>'create','type'=>'','sub_module_name'=>'operation','module_name'=>'Next Of Kin'],
            ['name' => 'edit_nextofkin', 'group'=>'Employee Master', 'guard_name' => 'web','permission_name'=>'edit','type'=>'','sub_module_name'=>'operation','module_name'=>'Next Of Kin'],
            ['name' => 'delete_nextofkin','group'=>'Employee Master', 'guard_name' => 'web','permission_name'=>'delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Next Of Kin'],
            ['name' => 'delete_selected_nextofkin','group'=>'Employee Master', 'guard_name' => 'web','permission_name'=>'Selected Delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Next Of Kin'],
            ['name' => 'selected_active_nextofkin', 'group'=>'Employee Master','guard_name' => 'web','type'=>'','permission_name'=>'Selected Active Multiple','sub_module_name'=>'operation','module_name'=>'Next Of Kin'],
            ['name' => 'selected_restore_nextofkin', 'group'=>'Employee Master','guard_name' => 'web','type'=>'','permission_name'=>'Selected Restore','sub_module_name'=>'operation','module_name'=>'Next Of Kin'],
            ['name' => 'restore_delete_nextofkin','group'=>'Employee Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Restore Delete','sub_module_name'=>'operation','module_name'=>'Next Of Kin'],
            ['name' => 'hard_delete_nextofkin','group'=>'Employee Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Hard Delete','sub_module_name'=>'operation','module_name'=>'Next Of Kin'],
            ['name' => 'active_nextofkin','group'=>'Employee Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Active','sub_module_name'=>'operation','module_name'=>'Next Of Kin'],
            ['name' => 'select_active_nextofkin','group'=>'Employee Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Select Active','sub_module_name'=>'operation','module_name'=>'Next Of Kin'],
            ['name' => 'word_nextofkin', 'group'=>'Employee Master','guard_name' => 'web','type'=>'','permission_name'=>'Word Export','sub_module_name'=>'operation','module_name'=>'Next Of Kin'],
            ['name' => 'csv_nextofkin','group'=>'Employee Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Csv Export','sub_module_name'=>'operation','module_name'=>'Next Of Kin'],
            ['name' => 'pdf_nextofkin', 'group'=>'Employee Master','guard_name' => 'web','type'=>'','permission_name'=>'PDF Export','sub_module_name'=>'operation','module_name'=>'Next Of Kin'],

            //listing in next of kin
            ['name' => 'nextofkin_checkbox', 'group'=>'Employee Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Checkbox','sub_module_name'=>'listing','module_name'=>'Next Of Kin'],
            ['name' => 'nextofkin_title', 'group'=>'Employee Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Title','sub_module_name'=>'listing','module_name'=>'Next Of Kin'],
            ['name' => 'nextofkin_firstname', 'group'=>'Employee Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'First Name','sub_module_name'=>'listing','module_name'=>'Next Of Kin'],
            ['name' => 'nextofkin_surname','group'=>'Employee Master', 'guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Surname','sub_module_name'=>'listing','module_name'=>'Next Of Kin'],
            ['name' => 'nextofkin_email', 'group'=>'Employee Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Email','sub_module_name'=>'listing','module_name'=>'Next Of Kin'],
            ['name' => 'nextofkin_relationship', 'group'=>'Employee Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Relationship','sub_module_name'=>'listing','module_name'=>'Next Of Kin'],
            ['name' => 'nextofkin_phone', 'group'=>'Employee Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Phone','sub_module_name'=>'listing','module_name'=>'Next Of Kin'],

            //word in next of kin
            ['name' => 'nextofkin_checkbox_word_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'word_export','enable'=>'0','permission_name'=>'Checkbox','sub_module_name'=>'listing','module_name'=>'Next Of Kin'],
            ['name' => 'nextofkin_title_word_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Title','sub_module_name'=>'listing','module_name'=>'Next Of Kin'],
            ['name' => 'nextofkin_firstname_word_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'First Name','sub_module_name'=>'listing','module_name'=>'Next Of Kin'],
            ['name' => 'nextofkin_surname_word_export','group'=>'Employee Master', 'guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Surname','sub_module_name'=>'listing','module_name'=>'Next Of Kin'],
            ['name' => 'nextofkin_email_word_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Email','sub_module_name'=>'listing','module_name'=>'Next Of Kin'],
            ['name' => 'nextofkin_relationship_word_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Relationship','sub_module_name'=>'listing','module_name'=>'Next Of Kin'],
            ['name' => 'nextofkin_phone_word_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Phone','sub_module_name'=>'listing','module_name'=>'Next Of Kin'],

            //excel in next of kin
            ['name' => 'nextofkin_checkbox_excel_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'excel_export','enable'=>'0','permission_name'=>'Checkbox','sub_module_name'=>'listing','module_name'=>'Next Of Kin'],
            ['name' => 'nextofkin_title_excel_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Title','sub_module_name'=>'listing','module_name'=>'Next Of Kin'],
            ['name' => 'nextofkin_firstname_excel_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'First Name','sub_module_name'=>'listing','module_name'=>'Next Of Kin'],
            ['name' => 'nextofkin_surname_excel_export','group'=>'Employee Master', 'guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Surname','sub_module_name'=>'listing','module_name'=>'Next Of Kin'],
            ['name' => 'nextofkin_email_excel_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Email','sub_module_name'=>'listing','module_name'=>'Next Of Kin'],
            ['name' => 'nextofkin_relationship_excel_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Relationship','sub_module_name'=>'listing','module_name'=>'Next Of Kin'],
            ['name' => 'nextofkin_phone_excel_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Phone','sub_module_name'=>'listing','module_name'=>'Next Of Kin'],

            //pdf in next of kin
            ['name' => 'nextofkin_checkbox_pdf_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'0','permission_name'=>'Checkbox','sub_module_name'=>'listing','module_name'=>'Next Of Kin'],
            ['name' => 'nextofkin_title_pdf_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Title','sub_module_name'=>'listing','module_name'=>'Next Of Kin'],
            ['name' => 'nextofkin_firstname_pdf_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'First Name','sub_module_name'=>'listing','module_name'=>'Next Of Kin'],
            ['name' => 'nextofkin_surname_pdf_export','group'=>'Employee Master', 'guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Surname','sub_module_name'=>'listing','module_name'=>'Next Of Kin'],
            ['name' => 'nextofkin_email_pdf_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Email','sub_module_name'=>'listing','module_name'=>'Next Of Kin'],
            ['name' => 'nextofkin_relationship_pdf_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Relationship','sub_module_name'=>'listing','module_name'=>'Next Of Kin'],
            ['name' => 'nextofkin_phone_pdf_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Phone','sub_module_name'=>'listing','module_name'=>'Next Of Kin'],


            //create in next of kin
            ['name' => 'nextofkin_checkbox_create', 'group'=>'Employee Master','guard_name' => 'web','type'=>'create','enable'=>'0','permission_name'=>'Checkbox','sub_module_name'=>'listing','module_name'=>'Next Of Kin'],
            ['name' => 'nextofkin_title_create', 'group'=>'Employee Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Title','sub_module_name'=>'listing','module_name'=>'Next Of Kin'],
            ['name' => 'nextofkin_firstname_create', 'group'=>'Employee Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'First Name','sub_module_name'=>'listing','module_name'=>'Next Of Kin'],
            ['name' => 'nextofkin_surname_create','group'=>'Employee Master', 'guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Surname','sub_module_name'=>'listing','module_name'=>'Next Of Kin'],
            ['name' => 'nextofkin_email_create', 'group'=>'Employee Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Email','sub_module_name'=>'listing','module_name'=>'Next Of Kin'],
            ['name' => 'nextofkin_relationship_create', 'group'=>'Employee Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Relationship','sub_module_name'=>'listing','module_name'=>'Next Of Kin'],
            ['name' => 'nextofkin_phone_create', 'group'=>'Employee Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Phone','sub_module_name'=>'listing','module_name'=>'Next Of Kin'],

            //edit in next of kin
            ['name' => 'nextofkin_checkbox_edit', 'group'=>'Employee Master','guard_name' => 'web','type'=>'edit','enable'=>'0','permission_name'=>'Checkbox','sub_module_name'=>'listing','module_name'=>'Next Of Kin'],
            ['name' => 'nextofkin_title_edit', 'group'=>'Employee Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Title','sub_module_name'=>'listing','module_name'=>'Next Of Kin'],
            ['name' => 'nextofkin_firstname_edit', 'group'=>'Employee Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'First Name','sub_module_name'=>'listing','module_name'=>'Next Of Kin'],
            ['name' => 'nextofkin_surname_edit','group'=>'Employee Master', 'guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Surname','sub_module_name'=>'listing','module_name'=>'Next Of Kin'],
            ['name' => 'nextofkin_email_edit', 'group'=>'Employee Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Email','sub_module_name'=>'listing','module_name'=>'Next Of Kin'],
            ['name' => 'nextofkin_relationship_edit', 'group'=>'Employee Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Relationship','sub_module_name'=>'listing','module_name'=>'Next Of Kin'],
            ['name' => 'nextofkin_phone_edit', 'group'=>'Employee Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Phone','sub_module_name'=>'listing','module_name'=>'Next Of Kin'],
        ];
        foreach ($permission_array as $key => $permission) {
            $admin_role = Role::find(1);
            $getpermission=Permission::where('name',$permission['name'])->first();
            if (empty($getpermission)){
                $permission=Permission::create($permission);
                $admin_role->givePermissionTo($permission['name']);
            }else{
                $permission=Permission::where('name',$getpermission->name)->update($permission);
                $admin_role->givePermissionTo($getpermission->name);
            }
        }
    }
}
