<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class EmployeePermisionTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
   public function run(){
        $permission_array = [
			// create project permissions
            ['name' => 'view_employee','group'=>'Employee Master', 'guard_name' => 'web','permission_name'=>'view','type'=>'','sub_module_name'=>'operation'],
            ['name' => 'create_employee','group'=>'Employee Master', 'guard_name' => 'web','permission_name'=>'create','type'=>'','sub_module_name'=>'operation'],
            ['name' => 'edit_employee', 'group'=>'Employee Master', 'guard_name' => 'web','permission_name'=>'edit','type'=>'','sub_module_name'=>'operation'],
			['name' => 'delete_employee','group'=>'Employee Master', 'guard_name' => 'web','permission_name'=>'delete','type'=>'','sub_module_name'=>'operation'],
			['name' => 'delete_selected_employee','group'=>'Employee Master', 'guard_name' => 'web','permission_name'=>'Selected Delete','type'=>'','sub_module_name'=>'operation'],
			['name' => 'selected_active_employee', 'group'=>'Employee Master','guard_name' => 'web','type'=>'','permission_name'=>'Selected Active Multiple','sub_module_name'=>'operation'],
			['name' => 'selected_restore_employee', 'group'=>'Employee Master','guard_name' => 'web','type'=>'','permission_name'=>'Selected Restore','sub_module_name'=>'operation'],
			['name' => 'hard_delete_employee','group'=>'Employee Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Hard Delete','sub_module_name'=>'operation'],
			['name' => 'active_employee','group'=>'Employee Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Active Employee','sub_module_name'=>'operation'],
			['name' => 'select_active_employee','group'=>'Employee Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Select Active Employee','sub_module_name'=>'operation'],
			['name' => 'word_employee', 'group'=>'Employee Master','guard_name' => 'web','type'=>'','permission_name'=>'Word Export Employee','sub_module_name'=>'operation'],
			['name' => 'csv_employee','group'=>'Employee Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Csv Export Employee','sub_module_name'=>'operation'],
			['name' => 'pdf_employee', 'group'=>'Employee Master','guard_name' => 'web','type'=>'','permission_name'=>'PDF Export Employee','sub_module_name'=>'operation'],

			//listing in employee
			['name' => 'employee_checkbox','group'=>'Employee Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'checkbox','sub_module_name'=>'listing'],
			['name' => 'employee_firstname', 'group'=>'Employee Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'First Name','sub_module_name'=>'listing'],
			['name' => 'employee_surname','group'=>'Employee Master', 'guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Surname','sub_module_name'=>'listing'],
			['name' => 'employee_managename', 'group'=>'Employee Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Manager Name','sub_module_name'=>'listing'],
			['name' => 'employee_position', 'group'=>'Employee Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Position','sub_module_name'=>'listing'],
			['name' => 'employee_ethnic', 'group'=>'Employee Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Ethnic','sub_module_name'=>'listing'],
			['name' => 'employee_sexualorientation', 'group'=>'Employee Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Sexual Orientation','sub_module_name'=>'listing','module_name'=>'Employee'],
			['name' => 'employee_managerrole', 'group'=>'Employee Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Manager Role','sub_module_name'=>'listing'],
			['name' => 'employee_sex', 'group'=>'Employee Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Sex','sub_module_name'=>'listing'],
			['name' => 'employee_dofbirth', 'group'=>'Employee Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Date of birth','sub_module_name'=>'listing'],
			['name' => 'employee_visaexdate', 'group'=>'Employee Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Visa Expiry Date','sub_module_name'=>'listing'],
			['name' => 'employee_nationinsiranceno', 'group'=>'Employee Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'National Insurance Number','sub_module_name'=>'listing'],
            ['name' => 'employee_payrolnumber', 'group'=>'Employee Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Payrol Number','sub_module_name'=>'listing'],
            ['name' => 'employee_cs_card', 'group'=>'Employee Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'CSCS Card','sub_module_name'=>'listing'],
            ['name' => 'employee_cs_card_ex_date', 'group'=>'Employee Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'CSCS Card Expiry Date','sub_module_name'=>'listing'],
            ['name' => 'employee_religion', 'group'=>'Employee Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Religion','sub_module_name'=>'listing'],
            ['name' => 'employee_client', 'group'=>'Employee Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Client','sub_module_name'=>'listing'],
            ['name' => 'employee_warning', 'group'=>'Employee Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Warning','sub_module_name'=>'listing'],
            ['name' => 'employee_note', 'group'=>'Employee Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing'],

			//word in employee
			['name' => 'employee_checkbox_word_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'word_export','enable'=>'0','permission_name'=>'checkbox','sub_module_name'=>'listing'],
			['name' => 'employee_firstname_word_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'First Name','sub_module_name'=>'listing'],
			['name' => 'employee_surname_word_export','group'=>'Employee Master', 'guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Surname','sub_module_name'=>'listing'],
			['name' => 'employee_managename_word_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Manager Name','sub_module_name'=>'listing'],
			['name' => 'employee_position_word_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Position','sub_module_name'=>'listing'],
			['name' => 'employee_ethnic_word_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Ethnic','sub_module_name'=>'listing'],
			['name' => 'employee_sexualorientation_word_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Sexual Orientation','sub_module_name'=>'listing','module_name'=>'Employee'],
			['name' => 'employee_managerrole_word_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Manager Role','sub_module_name'=>'listing'],
			['name' => 'employee_sex_word_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Sex','sub_module_name'=>'listing'],
			['name' => 'employee_dofbirth_word_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Date of birth','sub_module_name'=>'listing'],
			['name' => 'employee_visaexdate_word_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Visa Expiry Date','sub_module_name'=>'listing'],
			['name' => 'employee_nationinsiranceno_word_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'National Insurance Number','sub_module_name'=>'listing'],
            ['name' => 'employee_payrolnumber_word_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Payrol Number','sub_module_name'=>'listing'],
            ['name' => 'employee_cs_card_word_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'CSCS Card','sub_module_name'=>'listing'],
            ['name' => 'employee_cs_card_ex_date_word_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'CSCS Card Expiry Date','sub_module_name'=>'listing'],
            ['name' => 'employee_religion_word_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Religion','sub_module_name'=>'listing'],
            ['name' => 'employee_client_word_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Client','sub_module_name'=>'listing'],
            ['name' => 'employee_warning_word_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Warning','sub_module_name'=>'listing'],
            ['name' => 'employee_note_word_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing'],

			// excel in employee
			['name' => 'employee_checkbox_excel_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'excel_export','enable'=>'0','permission_name'=>'checkbox','sub_module_name'=>'listing'],
			['name' => 'employee_firstname_excel_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'First Name','sub_module_name'=>'listing'],
			['name' => 'employee_surname_excel_export','group'=>'Employee Master', 'guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Surname','sub_module_name'=>'listing'],
			['name' => 'employee_managename_excel_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Manager Name','sub_module_name'=>'listing'],
			['name' => 'employee_position_excel_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Position','sub_module_name'=>'listing'],
			['name' => 'employee_ethnic_excel_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Ethnic','sub_module_name'=>'listing'],
			['name' => 'employee_sexualorientation_excel_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Sexual Orientation','sub_module_name'=>'listing','module_name'=>'Employee'],
			['name' => 'employee_managerrole_excel_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Manager Role','sub_module_name'=>'listing'],
			['name' => 'employee_sex_excel_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Sex','sub_module_name'=>'listing'],
			['name' => 'employee_dofbirth_excel_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Date of birth','sub_module_name'=>'listing'],
			['name' => 'employee_visaexdate_excel_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Visa Expiry Date','sub_module_name'=>'listing'],
			['name' => 'employee_nationinsiranceno_excel_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'National Insurance Number','sub_module_name'=>'listing'],
            ['name' => 'employee_payrolnumber_excel_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Payrol Number','sub_module_name'=>'listing'],
            ['name' => 'employee_cs_card_excel_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'CSCS Card','sub_module_name'=>'listing'],
            ['name' => 'employee_cs_card_ex_date_excel_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'CSCS Card Expiry Date','sub_module_name'=>'listing'],
            ['name' => 'employee_religion_excel_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Religion','sub_module_name'=>'listing'],
            ['name' => 'employee_client_excel_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Client','sub_module_name'=>'listing'],
            ['name' => 'employee_warning_excel_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Warning','sub_module_name'=>'listing'],
            ['name' => 'employee_note_excel_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing'],

			// pdf in employee
			['name' => 'employee_checkbox_pdf_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'0','permission_name'=>'checkbox','sub_module_name'=>'listing'],
			['name' => 'employee_firstname_pdf_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'First Name','sub_module_name'=>'listing'],
			['name' => 'employee_surname_pdf_export','group'=>'Employee Master', 'guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Surname','sub_module_name'=>'listing'],
			['name' => 'employee_managename_pdf_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Manager Name','sub_module_name'=>'listing'],
			['name' => 'employee_position_pdf_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Position','sub_module_name'=>'listing'],
			['name' => 'employee_ethnic_pdf_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Ethnic','sub_module_name'=>'listing'],
			['name' => 'employee_sexualorientation_pdf_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Sexual Orientation','sub_module_name'=>'listing','module_name'=>'Employee'],
			['name' => 'employee_managerrole_pdf_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Manager Role','sub_module_name'=>'listing'],
			['name' => 'employee_sex_pdf_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Sex','sub_module_name'=>'listing'],
			['name' => 'employee_dofbirth_pdf_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Date of birth','sub_module_name'=>'listing'],
			['name' => 'employee_visaexdate_pdf_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Visa Expiry Date','sub_module_name'=>'listing'],
			['name' => 'employee_nationinsiranceno_pdf_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'National Insurance Number','sub_module_name'=>'listing'],
            ['name' => 'employee_payrolnumber_pdf_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Payrol Number','sub_module_name'=>'listing'],
            ['name' => 'employee_cs_card_pdf_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'CSCS Card','sub_module_name'=>'listing'],
            ['name' => 'employee_cs_card_ex_date_pdf_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'CSCS Card Expiry Date','sub_module_name'=>'listing'],
            ['name' => 'employee_religion_pdf_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Religion','sub_module_name'=>'listing'],
            ['name' => 'employee_client_pdf_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Client','sub_module_name'=>'listing'],
            ['name' => 'employee_warning_pdf_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Warning','sub_module_name'=>'listing'],
            ['name' => 'employee_note_pdf_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing'],

			//create in employee
			['name' => 'employee_checkbox_create', 'group'=>'Employee Master','guard_name' => 'web','type'=>'create','enable'=>'0','permission_name'=>'checkbox','sub_module_name'=>'listing'],
			['name' => 'employee_firstname_create', 'group'=>'Employee Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'First Name','sub_module_name'=>'listing'],
			['name' => 'employee_surname_create','group'=>'Employee Master', 'guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Surname','sub_module_name'=>'listing'],
			['name' => 'employee_managename_create', 'group'=>'Employee Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Manager Name','sub_module_name'=>'listing'],
			['name' => 'employee_position_create', 'group'=>'Employee Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Position','sub_module_name'=>'listing'],
			['name' => 'employee_ethnic_create', 'group'=>'Employee Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Ethnic','sub_module_name'=>'listing'],
			['name' => 'employee_sexualorientation_create', 'group'=>'Employee Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Sexual Orientation','sub_module_name'=>'listing','module_name'=>'Employee'],
			['name' => 'employee_managerrole_create', 'group'=>'Employee Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Manager Role','sub_module_name'=>'listing'],
			['name' => 'employee_sex_create', 'group'=>'Employee Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Sex','sub_module_name'=>'listing'],
			['name' => 'employee_dofbirth_create', 'group'=>'Employee Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Date of birth','sub_module_name'=>'listing'],
			['name' => 'employee_visaexdate_create', 'group'=>'Employee Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Visa Expiry Date','sub_module_name'=>'listing'],
			['name' => 'employee_nationinsiranceno_create', 'group'=>'Employee Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'National Insurance Number','sub_module_name'=>'listing'],
            ['name' => 'employee_payrolnumber_create', 'group'=>'Employee Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Payrol Number','sub_module_name'=>'listing'],
            ['name' => 'employee_cs_card_create', 'group'=>'Employee Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'CSCS Card','sub_module_name'=>'listing'],
            ['name' => 'employee_cs_card_ex_date_create', 'group'=>'Employee Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'CSCS Card Expiry Date','sub_module_name'=>'listing'],
            ['name' => 'employee_religion_create', 'group'=>'Employee Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Religion','sub_module_name'=>'listing'],
            ['name' => 'employee_client_create', 'group'=>'Employee Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Client','sub_module_name'=>'listing'],
            ['name' => 'employee_warning_create', 'group'=>'Employee Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Warning','sub_module_name'=>'listing'],
            ['name' => 'employee_note_create', 'group'=>'Employee Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing'],

			//edit in employee
			['name' => 'employee_checkbox_edit', 'group'=>'Employee Master','guard_name' => 'web','type'=>'edit','enable'=>'0','permission_name'=>'checkbox','sub_module_name'=>'listing'],
			['name' => 'employee_firstname_edit', 'group'=>'Employee Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'First Name','sub_module_name'=>'listing'],
			['name' => 'employee_surname_edit','group'=>'Employee Master', 'guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Surname','sub_module_name'=>'listing'],
			['name' => 'employee_managename_edit', 'group'=>'Employee Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Manager Name','sub_module_name'=>'listing'],
			['name' => 'employee_position_edit', 'group'=>'Employee Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Position','sub_module_name'=>'listing'],
			['name' => 'employee_ethnic_edit', 'group'=>'Employee Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Ethnic','sub_module_name'=>'listing'],
			['name' => 'employee_sexualorientation_edit', 'group'=>'Employee Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Sexual Orientation','sub_module_name'=>'listing','module_name'=>'Employee'],
			['name' => 'employee_managerrole_edit', 'group'=>'Employee Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Manager Role','sub_module_name'=>'listing'],
			['name' => 'employee_sex_edit', 'group'=>'Employee Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Sex','sub_module_name'=>'listing'],
			['name' => 'employee_dofbirth_edit', 'group'=>'Employee Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Date of birth','sub_module_name'=>'listing'],
			['name' => 'employee_visaexdate_edit', 'group'=>'Employee Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Visa Expiry Date','sub_module_name'=>'listing'],
			['name' => 'employee_nationinsiranceno_edit', 'group'=>'Employee Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'National Insurance Number','sub_module_name'=>'listing'],
            ['name' => 'employee_payrolnumber_edit', 'group'=>'Employee Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Payrol Number','sub_module_name'=>'listing'],
            ['name' => 'employee_cs_card_edit', 'group'=>'Employee Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'CSCS Card','sub_module_name'=>'listing'],
            ['name' => 'employee_cs_card_ex_date_edit', 'group'=>'Employee Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'CSCS Card Expiry Date','sub_module_name'=>'listing'],
            ['name' => 'employee_religion_edit', 'group'=>'Employee Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Religion','sub_module_name'=>'listing'],
            ['name' => 'employee_client_edit', 'group'=>'Employee Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Client','sub_module_name'=>'listing'],
            ['name' => 'employee_warning_edit', 'group'=>'Employee Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Warning','sub_module_name'=>'listing'],
            ['name' => 'employee_note_edit', 'group'=>'Employee Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing'],
		];

        foreach ($permission_array as $key => $permission) {
            $admin_role = Role::find(1);
            $getpermission=Permission::where('name',$permission['name'])->first();
            if (empty($getpermission)){
                $permission=Permission::create($permission);
                $admin_role->givePermissionTo($permission['name']);
            }else{
                $permission=Permission::where('name',$getpermission->name)->update($permission);
                $admin_role->givePermissionTo($getpermission->name);
            }
        }
   }
}
