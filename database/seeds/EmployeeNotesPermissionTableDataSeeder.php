<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
class EmployeeNotesPermissionTableDataSeeder extends Seeder
{
    public function run()
    {
        $permission_array = [
            // create Contact permissions
            ['name' => 'view_employeeNotes','group'=>'Employee Master', 'guard_name' => 'web','permission_name'=>'view','type'=>'','sub_module_name'=>'operation','module_name'=>'Notes'],
            ['name' => 'create_employeeNotes','group'=>'Employee Master', 'guard_name' => 'web','permission_name'=>'create','type'=>'','sub_module_name'=>'operation','module_name'=>'Notes'],
            ['name' => 'edit_employeeNotes', 'group'=>'Employee Master', 'guard_name' => 'web','permission_name'=>'edit','type'=>'','sub_module_name'=>'operation','module_name'=>'Notes'],
            ['name' => 'delete_employeeNotes','group'=>'Employee Master', 'guard_name' => 'web','permission_name'=>'delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Notes'],
            ['name' => 'selected_delete_employeeNotes','group'=>'Employee Master', 'guard_name' => 'web','permission_name'=>'Selected Delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Notes'],
            ['name' => 'selected_restore_delete_employeeNotes', 'group'=>'Employee Master','guard_name' => 'web','type'=>'','permission_name'=>'Selected Restore','sub_module_name'=>'operation','module_name'=>'Notes'],
            ['name' => 'restore_delete_employeeNotes','group'=>'Employee Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Restore Delete','sub_module_name'=>'operation','module_name'=>'Notes'],
            ['name' => 'selected_active_button_employeeNotes','group'=>'Employee Master', 'guard_name' => 'web','permission_name'=>'Selected Active Button','type'=>'','sub_module_name'=>'operation','module_name'=>'Notes'],
            ['name' => 'hard_delete_employeeNotes','group'=>'Employee Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Hard Delete','sub_module_name'=>'operation','module_name'=>'Notes'],
            ['name' => 'active_employeeNotes','group'=>'Employee Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Active employee','sub_module_name'=>'operation','module_name'=>'Notes'],
            ['name' => 'word_employeeNotes', 'group'=>'Employee Master','guard_name' => 'web','type'=>'','permission_name'=>'Word Export employee','sub_module_name'=>'operation','module_name'=>'Notes'],
            ['name' => 'csv_employeeNotes','group'=>'Employee Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Csv Export employee','sub_module_name'=>'operation','module_name'=>'Notes'],
            ['name' => 'pdf_employeeNotes', 'group'=>'Employee Master','guard_name' => 'web','type'=>'','permission_name'=>'PDF Export employee','sub_module_name'=>'operation','module_name'=>'Notes'],

            //listing in Contact
            ['name' => 'employeeNotes_checkbox','group'=>'Employee Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Notes'],
            ['name' => 'employeeNotes_notes','group'=>'Employee Master', 'guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Notes'],
            ['name' => 'employeeNotes_subject', 'group'=>'Employee Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Subject','sub_module_name'=>'listing','module_name'=>'Notes'],

            //word in Contact
            ['name' => 'employeeNotes_checkbox_word_export','group'=>'Employee Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'word_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Notes'],
            ['name' => 'employeeNotes_notes_word_export','group'=>'Employee Master', 'guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Notes'],
            ['name' => 'employeeNotes_subject_word_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Subject','sub_module_name'=>'listing','module_name'=>'Notes'],

            //excel in Contact
            ['name' => 'employeeNotes_checkbox_excel_export','group'=>'Employee Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'excel_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Notes'],
            ['name' => 'employeeNotes_notes_excel_export','group'=>'Employee Master', 'guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Notes'],
            ['name' => 'employeeNotes_subject_excel_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Subject','sub_module_name'=>'listing','module_name'=>'Notes'],

            //pdf in Contact
            ['name' => 'employeeNotes_checkbox_pdf_export','group'=>'Employee Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'pdf_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Notes'],
            ['name' => 'employeeNotes_notes_pdf_export','group'=>'Employee Master', 'guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Notes'],
            ['name' => 'employeeNotes_subject_pdf_export', 'group'=>'Employee Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Subject','sub_module_name'=>'listing','module_name'=>'Notes'],

            //create in Contact
            ['name' => 'employeeNotes_checkbox_create','group'=>'Employee Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'create','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Notes'],
            ['name' => 'employeeNotes_notes_create','group'=>'Employee Master', 'guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Notes'],
            ['name' => 'employeeNotes_subject_create', 'group'=>'Employee Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Subject','sub_module_name'=>'listing','module_name'=>'Notes'],

            //edit in Contact
            ['name' => 'employeeNotes_checkbox_edit','group'=>'Employee Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'edit','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Notes'],
            ['name' => 'employeeNotes_notes_edit','group'=>'Employee Master', 'guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Notes'],
            ['name' => 'employeeNotes_subject_edit', 'group'=>'Employee Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Subject','sub_module_name'=>'listing','module_name'=>'Notes'],
        ];

        foreach ($permission_array as $key => $permission) {
            $admin_role = Role::find(1);
            $getpermission=Permission::where('name',$permission['name'])->first();
            if (empty($getpermission)){
                $permission=Permission::create($permission);
                $admin_role->givePermissionTo($permission['name']);
            }else{
                $permission=Permission::where('name',$getpermission->name)->update($permission);
                $admin_role->givePermissionTo($getpermission->name);
            }
        }
    }
}
