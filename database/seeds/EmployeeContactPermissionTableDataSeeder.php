<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
class EmployeeContactPermissionTableDataSeeder extends Seeder
{
    public function run()
    {
        $permission_array = [
            ['name' => 'view_employeeContact','group'=>'Employee Master', 'guard_name' => 'web','permission_name'=>'view','type'=>'','sub_module_name'=>'operation','module_name'=>'Contact Information'],
            //edit in Contact
            ['name' => 'employeeContact_email_edit','group'=>'Employee Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'edit','permission_name'=>'Email','sub_module_name'=>'listing','module_name'=>'Contact Information'],
            ['name' => 'employeeContact_phone_edit','group'=>'Employee Master', 'guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Phone','sub_module_name'=>'listing','module_name'=>'Contact Information'],
            ['name' => 'employeeContact_mobile_edit', 'group'=>'Employee Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Mobile','sub_module_name'=>'listing','module_name'=>'Contact Information'],
            ['name' => 'employeeContact_address_edit', 'group'=>'Employee Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Address','sub_module_name'=>'listing','module_name'=>'Contact Information'],
            ['name' => 'employeeContact_house_name_edit', 'group'=>'Employee Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'HouseName','sub_module_name'=>'listing','module_name'=>'Contact Information'],
            ['name' => 'employeeContact_street_edit', 'group'=>'Employee Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Street','sub_module_name'=>'listing','module_name'=>'Contact Information'],
            ['name' => 'employeeContact_town_edit', 'group'=>'Employee Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Town','sub_module_name'=>'listing','module_name'=>'Contact Information'],
            ['name' => 'employeeContact_country_edit', 'group'=>'Employee Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Country','sub_module_name'=>'listing','module_name'=>'Contact Information'],
            ['name' => 'employeeContact_post_code_edit', 'group'=>'Employee Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'PostCode','sub_module_name'=>'listing','module_name'=>'Contact Information'],
            

        ];

        foreach ($permission_array as $key => $permission) {
            $admin_role = Role::find(1);
            $getpermission=Permission::where('name',$permission['name'])->first();
            if (empty($getpermission)){
                $permission=Permission::create($permission);
                $admin_role->givePermissionTo($permission['name']);
            }else{
                $permission=Permission::where('name',$getpermission->name)->update($permission);
                $admin_role->givePermissionTo($getpermission->name);
            }
        }
    }
}
