<?php

use Illuminate\Database\Seeder;
use App\PermissionGroup;
class UpdateGroupPermisionSeeder extends Seeder
{
    public function run()
    {
    	$data=array (
	        0 => 
	        array (
	            'id' => 1,
	            'group' => 'user',
	            'order'=>'1',
	        ),
	        1 => 
	        array (
	            'id' => 2,
	            'group' => 'company',
	            'order'=>'8',
	        ),
	        2 => 
	        array (
	            'id' => 3,
	            'group' => 'web_cms',
	            'order'=>'9',
	        ),
	        3 => 
	        array (
	            'id' => 4,
	            'group' => 'project',
	            'order'=>'2',
	        ),
	        4 => 
	        array (
	            'id' => 5,
	            'group' => 'client',
	            'order'=>'3',
	        ),
	        5 => 
	        array (
	            'id' => 6,
	            'group' => 'employees',
	            'order'=>'5',
	        ),
	        6 => 
	        array (
	            'id' => 7,
	            'group' => 'documents',
	            'order'=>'4',
	        ),
	        7 => 
	        array (
	            'id' => 8,
	            'group' => 'reports',
	            'order'=>'10',
	        ),
	        8 => 
	        array (
	            'id' => 9,
	            'group' => 'support',
	            'order'=>'7',
	        ),
	        9 => 
	        array (
	            'id' => 10,
	            'group' => 'job',
	            'order'=>'6',
	        ),
	    );
    	foreach ($data as $key => $value) {
        	PermissionGroup::where('id',$value['id'])->update(['group'=>$value['group'],'order'=>$value['order']]);
    	}
    }
}
