<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
class DocumentSchedulePermisionTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission_array = [
            // create schedule permissions
            ['name' => 'view_schedule','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'view','type'=>'','sub_module_name'=>'operation','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'create_schedule','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'create','type'=>'','sub_module_name'=>'operation','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'edit_schedule', 'group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'edit','type'=>'','sub_module_name'=>'operation','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'delete_schedule','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'selected_delete_schedule','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'Selected Delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'selected_restore_delete_schedule', 'group'=>'Document Master','guard_name' => 'web','type'=>'','permission_name'=>'Selected Restore','sub_module_name'=>'operation','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'selected_active_schedule', 'group'=>'Document Master','guard_name' => 'web','type'=>'','permission_name'=>'Selected Active Multiple','sub_module_name'=>'operation','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'restore_delete_schedule','group'=>'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Restore','sub_module_name'=>'operation','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'hard_delete_schedule','group'=>'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Hard Delete','sub_module_name'=>'operation','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'active_schedule','group'=>'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Active ','sub_module_name'=>'operation','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'word_schedule', 'group'=>'Document Master','guard_name' => 'web','type'=>'','permission_name'=>'Word Export ','sub_module_name'=>'operation','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'csv_schedule','group'=>'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Csv Export ','sub_module_name'=>'operation','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'pdf_schedule', 'group'=>'Document Master','guard_name' => 'web','type'=>'','permission_name'=>'PDF Export ','sub_module_name'=>'operation','module_name'=>'Schedule','module_name_id'=>'51'],

            //listing in schedul
            ['name' => 'schedule_checkbox','group'=>'Document Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_tasktype', 'group'=>'Document Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Task Type','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_projectname', 'group'=>'Document Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Project Name','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_location_breakdown','group'=>'Document Master', 'guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Location Breakdown','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_user', 'group'=>'Document Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'User','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_startdate', 'group'=>'Document Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Start Date','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_time', 'group'=>'Document Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Time','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_enddate', 'group'=>'Document Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'End Date','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_pretime', 'group'=>'Document Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Pre Time','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_posttime', 'group'=>'Document Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Post Time','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_recyclealljobs', 'group'=>'Document Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Recycle All Jobs','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_frequency', 'group'=>'Document Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Frequency','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_frequency_limit', 'group'=>'Document Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Frequency Limit','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_frequency_repeat', 'group'=>'Document Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Frequency Repeat','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_tag_swipe', 'group'=>'Document Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Tag Swipe','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_signature', 'group'=>'Document Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Signature','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],

            //word in schedul
            ['name' => 'schedule_checkbox_word_export','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'word_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_tasktype_word_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Task Type','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_projectname_word_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Project Name','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_location_breakdown_word_export','group'=>'Document Master', 'guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Location Breakdown','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_user_word_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'User','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_startdate_word_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Start Date','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_time_word_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Time','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_enddate_word_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'End Date','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_pretime_word_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Pre Time','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_posttime_word_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Post Time','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_recyclealljobs_word_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Recycle All Jobs','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_frequency_word_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Frequency','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_frequency_limit_word_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Frequency Limit','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_frequency_repeat_word_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Frequency Repeat','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_tag_swipe_word_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Tag Swipe','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_signature_word_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Signature','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],

            //excel in schedul
            ['name' => 'schedule_checkbox_excel_export','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'excel_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_tasktype_excel_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Task Type','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_projectname_excel_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Project Name','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_location_breakdown_excel_export','group'=>'Document Master', 'guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Location Breakdown','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_user_excel_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'User','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_startdate_excel_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Start Date','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_time_excel_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Time','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_enddate_excel_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'End Date','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_pretime_excel_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Pre Time','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_posttime_excel_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Post Time','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_recyclealljobs_excel_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Recycle All Jobs','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_frequency_excel_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Frequency','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_frequency_limit_excel_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Frequency Limit','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_frequency_repeat_excel_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Frequency Repeat','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_tag_swipe_excel_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Tag Swipe','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_signature_excel_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Signature','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],

            //pdf in schedul
            ['name' => 'schedule_checkbox_pdf_export','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'pdf_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_tasktype_pdf_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Task Type','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_projectname_pdf_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Project Name','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_location_breakdown_pdf_export','group'=>'Document Master', 'guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Location Breakdown','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_user_pdf_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'User','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_startdate_pdf_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Start Date','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_time_pdf_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Time','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_enddate_pdf_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'End Date','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_pretime_pdf_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Pre Time','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_posttime_pdf_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Post Time','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_recyclealljobs_pdf_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Recycle All Jobs','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_frequency_pdf_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Frequency','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_frequency_limit_pdf_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Frequency Limit','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_frequency_repeat_pdf_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Frequency Repeat','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_tag_swipe_pdf_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Tag Swipe','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_signature_pdf_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Signature','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],


            //create in schedul
            ['name' => 'schedule_checkbox_create','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'create','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_tasktype_create', 'group'=>'Document Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Task Type','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_projectname_create', 'group'=>'Document Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Project Name','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_location_breakdown_create','group'=>'Document Master', 'guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Location Breakdown','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_user_create', 'group'=>'Document Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'User','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_startdate_create', 'group'=>'Document Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Start Date','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_time_create', 'group'=>'Document Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Time','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_enddate_create', 'group'=>'Document Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'End Date','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_pretime_create', 'group'=>'Document Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Pre Time','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_posttime_create', 'group'=>'Document Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Post Time','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_recyclealljobs_create', 'group'=>'Document Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Recycle All Jobs','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_frequency_create', 'group'=>'Document Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Frequency','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_frequency_limit_create', 'group'=>'Document Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Frequency Limit','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_frequency_repeat_create', 'group'=>'Document Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Frequency Repeat','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_tag_swipe_create', 'group'=>'Document Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Tag Swipe','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_signature_create', 'group'=>'Document Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Signature','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],


            //edit in schedul
            ['name' => 'schedule_checkbox_edit','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'edit','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_tasktype_edit', 'group'=>'Document Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Task Type','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_projectname_edit', 'group'=>'Document Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Project Name','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_location_breakdown_edit','group'=>'Document Master', 'guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Location Breakdown','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_user_edit', 'group'=>'Document Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'User','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_startdate_edit', 'group'=>'Document Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Start Date','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_time_edit', 'group'=>'Document Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Time','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_enddate_edit', 'group'=>'Document Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'End Date','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_pretime_edit', 'group'=>'Document Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Pre Time','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_posttime_edit', 'group'=>'Document Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Post Time','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_recyclealljobs_edit', 'group'=>'Document Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Recycle All Jobs','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_frequency_edit', 'group'=>'Document Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Frequency','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_frequency_limit_edit', 'group'=>'Document Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Frequency Limit','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_frequency_repeat_edit', 'group'=>'Document Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Frequency Repeat','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_tag_swipe_edit', 'group'=>'Document Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Tag Swipe','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],
            ['name' => 'schedule_signature_edit', 'group'=>'Document Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Signature','sub_module_name'=>'listing','module_name'=>'Schedule','module_name_id'=>'51'],

        ];
        foreach ($permission_array as $key => $permission) {
            $admin_role = Role::find(1);
            $getpermission=Permission::where('name',$permission['name'])->first();
            if (empty($getpermission)){
                $permission=Permission::create($permission);
                $admin_role->givePermissionTo($permission['name']);
            }else{
                $permission=Permission::where('name',$getpermission->name)->update($permission);
                $admin_role->givePermissionTo($getpermission->name);
            }
        }
    }
}
