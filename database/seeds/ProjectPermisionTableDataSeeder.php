<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
class ProjectPermisionTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
   public function run(){
        $permission_array = [
			// create project permissions
            ['name' => 'view_project','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'view','type'=>'','sub_module_name'=>'operation','module_name'=>'project'],
            ['name' => 'create_project','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'create','type'=>'','sub_module_name'=>'operation','module_name'=>'project'],
            ['name' => 'edit_project', 'group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'edit','type'=>'','sub_module_name'=>'operation','module_name'=>'project'],
			['name' => 'delete_project','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'delete','type'=>'','sub_module_name'=>'operation','module_name'=>'project'],
			['name' => 'selected_delete_project','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'Selected Delete','type'=>'','sub_module_name'=>'operation','module_name'=>'project'],
			['name' => 'selected_active_button_project', 'group'=>'Project Master','guard_name' => 'web','type'=>'','permission_name'=>'Selected Active Multiple','sub_module_name'=>'operation','module_name'=>'project'],
			['name' => 'selected_restore_delete_project', 'group'=>'Project Master','guard_name' => 'web','type'=>'','permission_name'=>'Selected Restore','sub_module_name'=>'operation','module_name'=>'project'],
			['name' => 'restore_delete_project','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Restore Delete','sub_module_name'=>'operation','module_name'=>'project'],
			['name' => 'hard_delete_project','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Hard Delete','sub_module_name'=>'operation','module_name'=>'project'],
			['name' => 'active_project','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Active Project','sub_module_name'=>'operation','module_name'=>'project'],
			['name' => 'word_project', 'group'=>'Project Master','guard_name' => 'web','type'=>'','permission_name'=>'Word Export Project','sub_module_name'=>'operation','module_name'=>'project'],
			['name' => 'csv_project','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Csv Export Project','sub_module_name'=>'operation','module_name'=>'project'],
			['name' => 'pdf_project', 'group'=>'Project Master','guard_name' => 'web','type'=>'','permission_name'=>'PDF Export Project','sub_module_name'=>'operation','module_name'=>'project'],

			//listing in project
			['name' => 'project_checkbox','group'=>'Project Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_name', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'name','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_devision','group'=>'Project Master', 'guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'devision','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_customer', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'customer','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_group', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'group','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_manager', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'manager','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_contract_manager', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'contract_manager','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_client', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'client','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_start_date', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'start_date','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_expiry_date', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'expiry_date','sub_module_name'=>'listing','module_name'=>'project'],
            ['name' => 'project_ref', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'ref','sub_module_name'=>'listing','module_name'=>'project'],
            ['name' => 'project_account_number', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'account_number','sub_module_name'=>'listing','module_name'=>'project'],
            ['name' => 'project_contract', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'contract','sub_module_name'=>'listing','module_name'=>'project'],
            ['name' => 'project_notes', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'notes','sub_module_name'=>'listing','module_name'=>'project'],
            ['name' => 'project_termination_reason', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'termination_reason','sub_module_name'=>'listing','module_name'=>'project'],


            //word in prject
			['name' => 'project_checkbox_word_export','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'word_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_name_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'name','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_devision_word_export','group'=>'Project Master', 'guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'devision','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_customer_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'customer','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_group_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'group','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_manager_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'manager','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_contract_manager_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'contract_manager','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_client_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'client','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_start_date_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'start_date','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_expiry_date_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'expiry_date','sub_module_name'=>'listing','module_name'=>'project'],
            ['name' => 'project_ref_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'ref','sub_module_name'=>'listing','module_name'=>'project'],
            ['name' => 'project_account_number_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'account_number','sub_module_name'=>'listing','module_name'=>'project'],
            ['name' => 'project_contract_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'contract','sub_module_name'=>'listing','module_name'=>'project'],
            ['name' => 'project_notes_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'notes','sub_module_name'=>'listing','module_name'=>'project'],
            ['name' => 'project_termination_reason_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'termination_reason','sub_module_name'=>'listing','module_name'=>'project'],


            //excel in prject
			['name' => 'project_checkbox_excel_export','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'excel_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_name_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'name','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_devision_excel_export','group'=>'Project Master', 'guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'devision','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_customer_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'customer','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_group_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'group','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_manager_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'manager','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_contract_manager_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'contract_manager','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_client_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'client','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_start_date_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'start_date','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_expiry_date_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'expiry_date','sub_module_name'=>'listing','module_name'=>'project'],
            ['name' => 'project_ref_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'ref','sub_module_name'=>'listing','module_name'=>'project'],
            ['name' => 'project_account_number_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'account_number','sub_module_name'=>'listing','module_name'=>'project'],
            ['name' => 'project_contract_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'contract','sub_module_name'=>'listing','module_name'=>'project'],
            ['name' => 'project_notes_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'notes','sub_module_name'=>'listing','module_name'=>'project'],
            ['name' => 'project_termination_reason_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'termination_reason','sub_module_name'=>'listing','module_name'=>'project'],


            //pdf in prject
			['name' => 'project_checkbox_pdf_export','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'pdf_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_name_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'name','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_devision_pdf_export','group'=>'Project Master', 'guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'devision','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_customer_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'customer','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_group_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'group','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_manager_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'manager','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_contract_manager_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'contract_manager','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_client_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'client','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_start_date_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'start_date','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_expiry_date_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'expiry_date','sub_module_name'=>'listing','module_name'=>'project'],
            ['name' => 'project_ref_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'ref','sub_module_name'=>'listing','module_name'=>'project'],
            ['name' => 'project_account_number_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'account_number','sub_module_name'=>'listing','module_name'=>'project'],
            ['name' => 'project_contract_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'contract','sub_module_name'=>'listing','module_name'=>'project'],
            ['name' => 'project_notes_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'notes','sub_module_name'=>'listing','module_name'=>'project'],
            ['name' => 'project_termination_reason_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'termination_reason','sub_module_name'=>'listing','module_name'=>'project'],


            //create in project
			['name' => 'project_checkbox_create','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'create','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_name_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'name','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_devision_create','group'=>'Project Master', 'guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'devision','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_customer_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'customer','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_group_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'group','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_manager_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'manager','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_contract_manager_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'contract_manager','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_client_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'client','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_start_date_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'start_date','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_expiry_date_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'expiry_date','sub_module_name'=>'listing','module_name'=>'project'],
            ['name' => 'project_ref_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'ref','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_account_number_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'account_number','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_contract_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'contract','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_notes_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'notes','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_termination_reason_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'termination_reason','sub_module_name'=>'listing','module_name'=>'project'],

			//edit in project
			['name' => 'project_checkbox_edit','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'edit','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_name_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'name','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_devision_edit','group'=>'Project Master', 'guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'devision','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_customer_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'customer','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_group_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'group','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_manager_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'manager','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_contract_manager_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'contract_manager','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_client_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'client','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_start_date_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'start_date','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_expiry_date_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'expiry_date','sub_module_name'=>'listing','module_name'=>'project'],
            ['name' => 'project_ref_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'ref','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_account_number_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'account_number','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_contract_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'contract','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_notes_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'notes','sub_module_name'=>'listing','module_name'=>'project'],
			['name' => 'project_termination_reason_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'termination_reason','sub_module_name'=>'listing','module_name'=>'project'],
		];
        foreach ($permission_array as $key => $permission) {
            $admin_role = Role::find(1);
            $getpermission=Permission::where('name',$permission['name'])->first();
            if (empty($getpermission)){
                $permission=Permission::create($permission);
                $admin_role->givePermissionTo($permission['name']);
            }else{
                $permission=Permission::where('name',$getpermission->name)->update($permission);
                $admin_role->givePermissionTo($getpermission->name);
            }
        }
   }
}
