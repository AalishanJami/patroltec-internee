<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
class DocumentStaticFormPermissionTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission_array = [
            // create Contact permissions
            ['name' => 'view_documentStaticForm','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'View','type'=>'','sub_module_name'=>'operation','module_name'=>'Static Form','module_name_id'=>'52'],
            ['name' => 'create_documentStaticForm','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'Create','type'=>'','sub_module_name'=>'operation','module_name'=>'Static Form','module_name_id'=>'52'],
            ['name' => 'edit_documentStaticForm', 'group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'Edit','type'=>'','sub_module_name'=>'operation','module_name'=>'Static Form','module_name_id'=>'52'],
            ['name' => 'delete_documentStaticForm','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'Delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Static Form','module_name_id'=>'52'],
            ['name' => 'selected_delete_documentStaticForm','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'Selected Delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Static Form','module_name_id'=>'52'],
            ['name' => 'selected_restore_delete_documentStaticForm', 'group'=>'Document Master','guard_name' => 'web','type'=>'','permission_name'=>'Selected Restore','sub_module_name'=>'operation','module_name'=>'Static Form','module_name_id'=>'52'],
            ['name' => 'restore_delete_documentStaticForm','group'=>'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Restore Delete','sub_module_name'=>'operation','module_name'=>'Static Form','module_name_id'=>'52'],
            ['name' => 'hard_delete_documentStaticForm','group'=>'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Hard Delete','sub_module_name'=>'operation','module_name'=>'Static Form','module_name_id'=>'52'],
            ['name' => 'activate_documentStaticForm','group'=>'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Activate','sub_module_name'=>'operation','module_name'=>'Static Form','module_name_id'=>'52'],
            ['name' => 'view_detail_documentStaticForm','group'=>'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'View Detail','sub_module_name'=>'operation','module_name'=>'Static Form','module_name_id'=>'52'],
            ['name' => 'selected_active_documentStaticForm','group'=>'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Selected Active ','sub_module_name'=>'operation','module_name'=>'Static Form','module_name_id'=>'52'],
            ['name' => 'show_active_documentStaticForm','group'=>'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Show Active ','sub_module_name'=>'operation','module_name'=>'Static Form','module_name_id'=>'52'],
            ['name' => 'word_documentStaticForm', 'group'=>'Document Master','guard_name' => 'web','type'=>'','permission_name'=>'Word Export ','sub_module_name'=>'operation','module_name'=>'Static Form','module_name_id'=>'52'],
            ['name' => 'csv_documentStaticForm','group'=>'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Csv Export ','sub_module_name'=>'operation','module_name'=>'Static Form','module_name_id'=>'52'],
            ['name' => 'pdf_documentStaticForm', 'group'=>'Document Master','guard_name' => 'web','type'=>'','permission_name'=>'PDF Export ','sub_module_name'=>'operation','module_name'=>'Static Form','module_name_id'=>'52'],

            //listing in Contact
            ['name' => 'documentStaticForm_checkbox','group'=>'Document Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Static Form','module_name_id'=>'52'],
            ['name' => 'documentStaticForm_notes','group'=>'Document Master', 'guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Static Form','module_name_id'=>'52'],
            ['name' => 'documentStaticForm_title', 'group'=>'Document Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Title','sub_module_name'=>'listing','module_name'=>'Static Form','module_name_id'=>'52'],
            ['name' => 'documentStaticForm_upload', 'group'=>'Document Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Upload','sub_module_name'=>'listing','module_name'=>'Static Form','module_name_id'=>'52'],

            //word in Contact
            ['name' => 'documentStaticForm_checkbox_word_export','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'word_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Static Form','module_name_id'=>'52'],
            ['name' => 'documentStaticForm_notes_word_export','group'=>'Document Master', 'guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Static Form','module_name_id'=>'52'],
            ['name' => 'documentStaticForm_title_word_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Title','sub_module_name'=>'listing','module_name'=>'Static Form','module_name_id'=>'52'],
            ['name' => 'documentStaticForm_upload_word_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Upload','sub_module_name'=>'listing','module_name'=>'Static Form','module_name_id'=>'52'],

            //excel in Contact
            ['name' => 'documentStaticForm_checkbox_excel_export','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'excel_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Static Form','module_name_id'=>'52'],
            ['name' => 'documentStaticForm_notes_excel_export','group'=>'Document Master', 'guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Static Form','module_name_id'=>'52'],
            ['name' => 'documentStaticForm_title_excel_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Title','sub_module_name'=>'listing','module_name'=>'Static Form','module_name_id'=>'52'],
            ['name' => 'documentStaticForm_upload_excel_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Upload','sub_module_name'=>'listing','module_name'=>'Static Form','module_name_id'=>'52'],

            //pdf in Contact
            ['name' => 'documentStaticForm_checkbox_pdf_export','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'pdf_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Static Form','module_name_id'=>'52'],
            ['name' => 'documentStaticForm_notes_pdf_export','group'=>'Document Master', 'guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Static Form','module_name_id'=>'52'],
            ['name' => 'documentStaticForm_title_pdf_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Title','sub_module_name'=>'listing','module_name'=>'Static Form','module_name_id'=>'52'],
            ['name' => 'documentStaticForm_upload_pdf_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Upload','sub_module_name'=>'listing','module_name'=>'Static Form','module_name_id'=>'52'],

            //create in Contact
            ['name' => 'documentStaticForm_checkbox_create','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'create','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Static Form','module_name_id'=>'52'],
            ['name' => 'documentStaticForm_notes_create','group'=>'Document Master', 'guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Static Form','module_name_id'=>'52'],
            ['name' => 'documentStaticForm_title_create', 'group'=>'Document Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Title','sub_module_name'=>'listing','module_name'=>'Static Form','module_name_id'=>'52'],
            ['name' => 'documentStaticForm_upload_create', 'group'=>'Document Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Upload','sub_module_name'=>'listing','module_name'=>'Static Form','module_name_id'=>'52'],

            //edit in Contact
            ['name' => 'documentStaticForm_checkbox_edit','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'edit','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Static Form','module_name_id'=>'52'],
            ['name' => 'documentStaticForm_notes_edit','group'=>'Document Master', 'guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Notes','sub_module_name'=>'listing','module_name'=>'Static Form','module_name_id'=>'52'],
            ['name' => 'documentStaticForm_title_edit', 'group'=>'Document Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Title','sub_module_name'=>'listing','module_name'=>'Static Form','module_name_id'=>'52'],
            ['name' => 'documentStaticForm_upload_edit', 'group'=>'Document Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Upload','sub_module_name'=>'listing','module_name'=>'Static Form','module_name_id'=>'52'],
        ];

        foreach ($permission_array as $key => $permission) {
            $admin_role = Role::find(1);
            $getpermission=Permission::where('name',$permission['name'])->first();
            if (empty($getpermission)){
                $permission=Permission::create($permission);
                $admin_role->givePermissionTo($permission['name']);
            }else{
                $permission=Permission::where('name',$getpermission->name)->update($permission);
                $admin_role->givePermissionTo($getpermission->name);
            }
        }
    }
}
