<?php

use Illuminate\Database\Seeder;

class AdminDataTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
		$admin = [
			'name' => 'super admin',
			'email' => 'superadmin@gmail.com',
			'password' => bcrypt('password'),
			'all_companies' => '1',
			'active' => '1',
		];
		$admin = \App\User::create($admin);
	}
}
