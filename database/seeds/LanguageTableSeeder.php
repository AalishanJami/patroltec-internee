<?php

use Illuminate\Database\Seeder;
use App\Language;
class LanguageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $languages = [
		  ['id' => '1','language' => 'English','short_lan' => 'en','status' => '1','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL],
		  ['id' => '2','language' => 'French','short_lan' => 'fr','status' => '1','deleted_at' => NULL,'created_at' => NULL,'updated_at' => NULL]
		];
		foreach ($languages as $key => $permission) {
			Language::create($permission);
		}
    }
}
