<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AddressTypePermissionTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission_array = [
            // create Address Type permissions
            ['name' => 'view_addressType','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'view','type'=>'','sub_module_name'=>'operation','module_name'=>'Address Type'],
            ['name' => 'create_addressType','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'create','type'=>'','sub_module_name'=>'operation','module_name'=>'Address Type'],
            ['name' => 'edit_addressType', 'group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'edit','type'=>'','sub_module_name'=>'operation','module_name'=>'Address Type'],
            ['name' => 'delete_addressType','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Address Type'],
            ['name' => 'selected_delete_addressType','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'Selected Delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Address Type'],
            ['name' => 'selected_restore_delete_addressType', 'group'=>'Project Master','guard_name' => 'web','type'=>'','permission_name'=>'Selected Restore','sub_module_name'=>'operation','module_name'=>'Address Type'],
            ['name' => 'selected_active_addresstype', 'group'=>'Project Master','guard_name' => 'web','type'=>'','permission_name'=>'Select Active','sub_module_name'=>'operation','module_name'=>'Address Type'],
            ['name' => 'restore_delete_addressType','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Restore Delete','sub_module_name'=>'operation','module_name'=>'Address Type'],
            ['name' => 'hard_delete_addressType','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Hard Delete','sub_module_name'=>'operation','module_name'=>'Address Type'],
            ['name' => 'active_addressType','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Show Active Address Type','sub_module_name'=>'operation','module_name'=>'Address Type'],
            ['name' => 'word_addressType', 'group'=>'Project Master','guard_name' => 'web','type'=>'','permission_name'=>'Word Export Address Type','sub_module_name'=>'operation','module_name'=>'Address Type'],
            ['name' => 'csv_addressType','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Csv Export Address Type','sub_module_name'=>'operation','module_name'=>'Address Type'],
            ['name' => 'pdf_addressType', 'group'=>'Project Master','guard_name' => 'web','type'=>'','permission_name'=>'PDF Export Address Type','sub_module_name'=>'operation','module_name'=>'Address Type'],

            //listing in Address Type
            ['name' => 'addressType_checkbox','group'=>'Project Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Address Type'],
            ['name' => 'addressType_name', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'name','sub_module_name'=>'listing','module_name'=>'Address Type'],

            //word in Address Type
            ['name' => 'addressType_checkbox_word_export','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'word_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Address Type'],
            ['name' => 'addressType_name_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'name','sub_module_name'=>'listing','module_name'=>'Address Type'],

            //excel in Address Type
            ['name' => 'addressType_checkbox_excel_export','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'excel_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Address Type'],
            ['name' => 'addressType_name_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'name','sub_module_name'=>'listing','module_name'=>'Address Type'],

            //pdf in Address Type
            ['name' => 'addressType_checkbox_pdf_export','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'pdf_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Address Type'],
            ['name' => 'addressType_name_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'name','sub_module_name'=>'listing','module_name'=>'Address Type'],

            //create in Address Type
            ['name' => 'addressType_checkbox_create','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'create','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Address Type'],
            ['name' => 'addressType_name_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'name','sub_module_name'=>'listing','module_name'=>'Address Type'],

            //edit in Address Type
            ['name' => 'addressType_checkbox_edit','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'edit','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Address Type'],
            ['name' => 'addressType_name_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'name','sub_module_name'=>'listing','module_name'=>'Address Type'],

        ];


        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $address = Permission::where(['module_name'=>'AddressTypePopup'])->get();
        if (count($address)>0){
            Permission::where(['module_name'=>'AddressTypePopup'])->delete();
        }
        foreach ($permission_array as $key => $permission) {
            $admin_role = Role::find(1);
            $getpermission=Permission::where('name',$permission['name'])->first();
            if (empty($getpermission)){
                $permission=Permission::create($permission);
                $admin_role->givePermissionTo($permission['name']);
            }else{
                $permission=Permission::where('name',$getpermission->name)->update($permission);
                $admin_role->givePermissionTo($getpermission->name);
            }
        }
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
