<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
class DocumentAnswerPermissionTableDataSeeder extends Seeder
{
    public function run()
    {
        $permission_array = [
            // create Contact permissions
            ['name' => 'view_document_answer','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'view','type'=>'','sub_module_name'=>'operation','module_name'=>'Answer'],
            ['name' => 'create_document_answer','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'create','type'=>'','sub_module_name'=>'operation','module_name'=>'Answer'],
            ['name' => 'edit_document_answer', 'group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'edit','type'=>'','sub_module_name'=>'operation','module_name'=>'Answer'],
            ['name' => 'upload_document_answer', 'group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'upload Icon','type'=>'','sub_module_name'=>'operation','module_name'=>'Answer'],
            ['name' => 'delete_document_answer','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Answer'],
            ['name' => 'selected_delete_document_answer','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'Selected Delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Answer'],
            ['name' => 'selected_restore_delete_document_answer', 'group'=>'Document Master','guard_name' => 'web','type'=>'','permission_name'=>'ALL Restore Data','sub_module_name'=>'operation','module_name'=>'Answer'],
            ['name' => 'selected_active_document_answer','group'=>'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Select Active ','sub_module_name'=>'operation','module_name'=>'Answer'],
            ['name' => 'restore_delete_document_answer','group'=>'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Restore ','sub_module_name'=>'operation','module_name'=>'Answer'],
            ['name' => 'selected_active_button_document_answer','group'=>'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Select Active','sub_module_name'=>'operation','module_name'=>'Answer'],
            ['name' => 'hard_delete_document_answer','group'=>'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Hard Delete','sub_module_name'=>'operation','module_name'=>'Answer'],
            ['name' => 'active_document_answer','group'=>'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Show Active document_answer','sub_module_name'=>'operation','module_name'=>'Answer'],
            ['name' => 'word_document_answer', 'group'=>'Document Master','guard_name' => 'web','type'=>'','permission_name'=>'Word Export document_answer','sub_module_name'=>'operation','module_name'=>'Answer'],
            ['name' => 'csv_document_answer','group'=>'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Csv Export document_answer','sub_module_name'=>'operation','module_name'=>'Answer'],
            ['name' => 'pdf_document_answer', 'group'=>'Document Master','guard_name' => 'web','type'=>'','permission_name'=>'PDF Export document_answer','sub_module_name'=>'operation','module_name'=>'Answer'],

            //listing in Contact
            ['name' => 'document_answer_checkbox','group'=>'Document Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Answer'],
            ['name' => 'document_answer_name','group'=>'Document Master', 'guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Answer'],
            ['name' => 'document_answer_grade','group'=>'Document Master', 'guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Grade','sub_module_name'=>'listing','module_name'=>'Answer'],
            ['name' => 'document_answer_score','group'=>'Document Master', 'guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Score','sub_module_name'=>'listing','module_name'=>'Answer'],
            ['name' => 'document_answer_image','group'=>'Document Master', 'guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Image','sub_module_name'=>'listing','module_name'=>'Answer'],

            //word in Contact
            ['name' => 'document_answer_checkbox_word_export','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'word_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Answer'],
            ['name' => 'document_answer_name_word_export','group'=>'Document Master', 'guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Answer'],
            ['name' => 'document_answer_grade_word_export','group'=>'Document Master', 'guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Grade','sub_module_name'=>'listing','module_name'=>'Answer'],
            ['name' => 'document_answer_score_word_export','group'=>'Document Master', 'guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Score','sub_module_name'=>'listing','module_name'=>'Answer'],
            ['name' => 'document_answer_image_word_export','group'=>'Document Master', 'guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Image','sub_module_name'=>'listing','module_name'=>'Answer'],

            //excel in Contact
            ['name' => 'document_answer_checkbox_excel_export','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'excel_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Answer'],
            ['name' => 'document_answer_name_excel_export','group'=>'Document Master', 'guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Answer'],
            ['name' => 'document_answer_grade_excel_export','group'=>'Document Master', 'guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Grade','sub_module_name'=>'listing','module_name'=>'Answer'],
            ['name' => 'document_answer_score_excel_export','group'=>'Document Master', 'guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Score','sub_module_name'=>'listing','module_name'=>'Answer'],
            ['name' => 'document_answer_image_excel_export','group'=>'Document Master', 'guard_name' => 'web','type'=>'excel_export','enable'=>'0','permission_name'=>'Image','sub_module_name'=>'listing','module_name'=>'Answer'],

            //pdf in Contact
            ['name' => 'document_answer_checkbox_pdf_export','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'pdf_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Answer'],
            ['name' => 'document_answer_name_pdf_export','group'=>'Document Master', 'guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Answer'],
            ['name' => 'document_answer_grade_pdf_export','group'=>'Document Master', 'guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Grade','sub_module_name'=>'listing','module_name'=>'Answer'],
            ['name' => 'document_answer_score_pdf_export','group'=>'Document Master', 'guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Score','sub_module_name'=>'listing','module_name'=>'Answer'],
            ['name' => 'document_answer_image_pdf_export','group'=>'Document Master', 'guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Image','sub_module_name'=>'listing','module_name'=>'Answer'],

            //create in Contact
            ['name' => 'document_answer_checkbox_create','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'create','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Answer'],
            ['name' => 'document_answer_name_create','group'=>'Document Master', 'guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Answer'],
            ['name' => 'document_answer_grade_create','group'=>'Document Master', 'guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Grade','sub_module_name'=>'listing','module_name'=>'Answer'],
            ['name' => 'document_answer_score_create','group'=>'Document Master', 'guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Score','sub_module_name'=>'listing','module_name'=>'Answer'],
            ['name' => 'document_answer_image_create','group'=>'Document Master', 'guard_name' => 'web','type'=>'create','enable'=>'0','permission_name'=>'Image','sub_module_name'=>'listing','module_name'=>'Answer'],

            //edit in Contact
            ['name' => 'document_answer_checkbox_edit','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'edit','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Answer'],
            ['name' => 'document_answer_name_edit','group'=>'Document Master', 'guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Answer'],
            ['name' => 'document_answer_grade_edit','group'=>'Document Master', 'guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Grade','sub_module_name'=>'listing','module_name'=>'Answer'],
            ['name' => 'document_answer_score_edit','group'=>'Document Master', 'guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Score','sub_module_name'=>'listing','module_name'=>'Answer'],
            ['name' => 'document_answer_image_edit','group'=>'Document Master', 'guard_name' => 'web','type'=>'edit','enable'=>'0','permission_name'=>'Image','sub_module_name'=>'listing','module_name'=>'Answer'],
        ];

        foreach ($permission_array as $key => $permission) {
            $admin_role = Role::find(1);
            $getpermission=Permission::where('name',$permission['name'])->first();
            if (empty($getpermission)){
                $permission=Permission::create($permission);
                $admin_role->givePermissionTo($permission['name']);
            }else{
                $permission=Permission::where('name',$getpermission->name)->update($permission);
                $admin_role->givePermissionTo($getpermission->name);
            }
        }
    }
}
