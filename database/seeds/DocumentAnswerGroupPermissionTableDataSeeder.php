<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
class DocumentAnswerGroupPermissionTableDataSeeder extends Seeder
{
    public function run()
    {
        $permission_array = [
            // create Contact permissions
            ['name' => 'view_document_answer_group','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'view','type'=>'','sub_module_name'=>'operation','module_name'=>'Answer Group'],
            ['name' => 'create_document_answer_group','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'create','type'=>'','sub_module_name'=>'operation','module_name'=>'Answer Group'],
            ['name' => 'edit_document_answer_group', 'group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'edit','type'=>'','sub_module_name'=>'operation','module_name'=>'Answer Group'],
            ['name' => 'delete_document_answer_group','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Answer Group'],
            ['name' => 'selected_delete_document_answer_group','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'Selected Delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Answer Group'],
            ['name' => 'selected_restore_delete_document_answer_group', 'group'=>'Document Master','guard_name' => 'web','type'=>'','permission_name'=>'ALL Restore Data','sub_module_name'=>'operation','module_name'=>'Answer Group'],
            ['name' => 'selected_active_document_answer_group','group'=>'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Select Active ','sub_module_name'=>'operation','module_name'=>'Answer Group'],
            ['name' => 'restore_delete_document_answer_group','group'=>'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Restore ','sub_module_name'=>'operation','module_name'=>'Answer Group'],
            ['name' => 'selected_active_button_document_answer_group','group'=>'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Select Active','sub_module_name'=>'operation','module_name'=>'Answer Group'],
            ['name' => 'hard_delete_document_answer_group','group'=>'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Hard Delete','sub_module_name'=>'operation','module_name'=>'Answer Group'],
            ['name' => 'active_document_answer_group','group'=>'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Show Active document_answer_group','sub_module_name'=>'operation','module_name'=>'Answer Group'],
            ['name' => 'word_document_answer_group', 'group'=>'Document Master','guard_name' => 'web','type'=>'','permission_name'=>'Word Export document_answer_group','sub_module_name'=>'operation','module_name'=>'Answer Group'],
            ['name' => 'csv_document_answer_group','group'=>'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Csv Export document_answer_group','sub_module_name'=>'operation','module_name'=>'Answer Group'],
            ['name' => 'pdf_document_answer_group', 'group'=>'Document Master','guard_name' => 'web','type'=>'','permission_name'=>'PDF Export document_answer_group','sub_module_name'=>'operation','module_name'=>'Answer Group'],

            //listing in Contact
            ['name' => 'document_answer_group_checkbox','group'=>'Document Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Answer Group'],
            ['name' => 'document_answer_group_name','group'=>'Document Master', 'guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Answer Group'],

            //word in Contact
            ['name' => 'document_answer_group_checkbox_word_export','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'word_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Answer Group'],
            ['name' => 'document_answer_group_name_word_export','group'=>'Document Master', 'guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Answer Group'],

            //excel in Contact
            ['name' => 'document_answer_group_checkbox_excel_export','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'excel_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Answer Group'],
            ['name' => 'document_answer_group_name_excel_export','group'=>'Document Master', 'guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Answer Group'],

            //pdf in Contact
            ['name' => 'document_answer_group_checkbox_pdf_export','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'pdf_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Answer Group'],
            ['name' => 'document_answer_group_name_pdf_export','group'=>'Document Master', 'guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Answer Group'],

            //create in Contact
            ['name' => 'document_answer_group_checkbox_create','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'create','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Answer Group'],
            ['name' => 'document_answer_group_name_create','group'=>'Document Master', 'guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Answer Group'],

            //edit in Contact
            ['name' => 'document_answer_group_checkbox_edit','group'=>'Document Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'edit','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Answer Group'],
            ['name' => 'document_answer_group_name_edit','group'=>'Document Master', 'guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Answer Group'],
        ];

        foreach ($permission_array as $key => $permission) {
            $admin_role = Role::find(1);
            $getpermission=Permission::where('name',$permission['name'])->first();
            if (empty($getpermission)){
                $permission=Permission::create($permission);
                $admin_role->givePermissionTo($permission['name']);
            }else{
                $permission=Permission::where('name',$getpermission->name)->update($permission);
                $admin_role->givePermissionTo($getpermission->name);
            }
        }
    }
}
