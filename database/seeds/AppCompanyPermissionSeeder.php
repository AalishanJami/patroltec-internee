<?php

use Illuminate\Database\Seeder;

class AppCompanyPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('app_company_permissions')->insert(array (
        	0 => 
            array (
                'id' => 1,
                'company_id' => 2,
                'app_role_id' => 1,
                'app_permission_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'company_id' => 2,
                'app_role_id' => 1,
                'app_permission_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'company_id' => 2,
                'app_role_id' => 1,
                'app_permission_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'company_id' => 2,
                'app_role_id' => 1,
                'app_permission_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'company_id' => 2,
                'app_role_id' => 1,
                'app_permission_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'company_id' => 2,
                'app_role_id' => 1,
                'app_permission_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'company_id' => 2,
                'app_role_id' => 1,
                'app_permission_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'company_id' => 2,
                'app_role_id' => 1,
                'app_permission_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'company_id' => 2,
                'app_role_id' => 1,
                'app_permission_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'company_id' => 2,
                'app_role_id' => 1,
                'app_permission_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'company_id' => 2,
                'app_role_id' => 1,
                'app_permission_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            11=> 
            array (
                'id' => 12,
                'company_id' => 2,
                'app_role_id' => 1,
                'app_permission_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'company_id' => 2,
                'app_role_id' => 1,
                'app_permission_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'company_id' => 2,
                'app_role_id' => 1,
                'app_permission_id' => 14,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'id' => 21,
                'company_id' => 2,
                'app_role_id' => 1,
                'app_permission_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            15 => 
            array (
                'id' => 22,
                'company_id' => 2,
                'app_role_id' => 1,
                'app_permission_id' => 14,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            16 => 
            array (
                'id' => 15,
                'company_id' => 2,
                'app_role_id' => 1,
                'app_permission_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            17 => 
            array (
                'id' => 16,
                'company_id' => 2,
                'app_role_id' => 2,
                'app_permission_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            18 => 
            array (
                'id' => 17,
                'company_id' => 2,
                'app_role_id' => 2,
                'app_permission_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            19 => 
            array (
                'id' => 18,
                'company_id' => 2,
                'app_role_id' => 2,
                'app_permission_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            20 => 
            array (
                'id' => 19,
                'company_id' => 2,
                'app_role_id' => 2,
                'app_permission_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            21 => 
            array (
                'id' => 20,
                'company_id' => 2,
                'app_role_id' => 2,
                'app_permission_id' => 14,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
    }
}
