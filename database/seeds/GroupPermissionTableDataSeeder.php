<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
class GroupPermissionTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission_array = [
            // create Contact permissions
            ['name' => 'view_group','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'view','type'=>'','sub_module_name'=>'operation','module_name'=>'Group','module_name_id'=>20],
            ['name' => 'create_group','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'create','type'=>'','sub_module_name'=>'operation','module_name'=>'Group','module_name_id'=>20],
            ['name' => 'edit_group', 'group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'edit','type'=>'','sub_module_name'=>'operation','module_name'=>'Group','module_name_id'=>20],
            ['name' => 'delete_group','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Group','module_name_id'=>20],
            ['name' => 'selected_delete_group','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'Selected Delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Group','module_name_id'=>20],
            ['name' => 'selected_restore_delete_group', 'group'=>'Project Master','guard_name' => 'web','type'=>'','permission_name'=>'Selected Restore','sub_module_name'=>'operation','module_name'=>'Group','module_name_id'=>20],
            ['name' => 'restore_delete_group','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Restore Delete','sub_module_name'=>'operation','module_name'=>'Group','module_name_id'=>20],
            ['name' => 'hard_delete_group','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Hard Delete','sub_module_name'=>'operation','module_name'=>'Group','module_name_id'=>20],
            ['name' => 'active_group','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Active','sub_module_name'=>'operation','module_name'=>'Group','module_name_id'=>20],
            ['name' => 'selected_active_group','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Selected Active','sub_module_name'=>'operation','module_name'=>'Group','module_name_id'=>20],
            ['name' => 'word_group', 'group'=>'Project Master','guard_name' => 'web','type'=>'','permission_name'=>'Word Export','sub_module_name'=>'operation','module_name'=>'Group','module_name_id'=>20],
            ['name' => 'csv_group','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Csv Export','sub_module_name'=>'operation','module_name'=>'Group','module_name_id'=>20],
            ['name' => 'pdf_group', 'group'=>'Project Master','guard_name' => 'web','type'=>'','permission_name'=>'PDF Export','sub_module_name'=>'operation','module_name'=>'Group','module_name_id'=>20],

            //listing in Contact
            ['name' => 'group_checkbox','group'=>'Project Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Group','module_name_id'=>20],
            ['name' => 'group_name','group'=>'Project Master', 'guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Group','module_name_id'=>20],
            ['name' => 'group_division','group'=>'Project Master', 'guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Division','sub_module_name'=>'listing','module_name'=>'Group','module_name_id'=>20],
            ['name' => 'group_customer','group'=>'Project Master', 'guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Customer','sub_module_name'=>'listing','module_name'=>'Group','module_name_id'=>20],

            //word in Contact
            ['name' => 'group_checkbox_word_export','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'word_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Group','module_name_id'=>20],
            ['name' => 'group_name_word_export','group'=>'Project Master', 'guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Group','module_name_id'=>20],
            ['name' => 'group_division_word_export','group'=>'Project Master', 'guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Division','sub_module_name'=>'listing','module_name'=>'Group','module_name_id'=>20],
            ['name' => 'group_customer_word_export','group'=>'Project Master', 'guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Customer','sub_module_name'=>'listing','module_name'=>'Group','module_name_id'=>20],

            //excel in Contact
            ['name' => 'group_checkbox_excel_export','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'excel_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Group','module_name_id'=>20],
            ['name' => 'group_name_excel_export','group'=>'Project Master', 'guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Group','module_name_id'=>20],
            ['name' => 'group_division_excel_export','group'=>'Project Master', 'guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Division','sub_module_name'=>'listing','module_name'=>'Group','module_name_id'=>20],
            ['name' => 'group_customer_excel_export','group'=>'Project Master', 'guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Customer','sub_module_name'=>'listing','module_name'=>'Group','module_name_id'=>20],

            //pdf in Contact
            ['name' => 'group_checkbox_pdf_export','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'pdf_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Group','module_name_id'=>20],
            ['name' => 'group_name_pdf_export','group'=>'Project Master', 'guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Group','module_name_id'=>20],
            ['name' => 'group_division_pdf_export','group'=>'Project Master', 'guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Division','sub_module_name'=>'listing','module_name'=>'Group','module_name_id'=>20],
            ['name' => 'group_customer_pdf_export','group'=>'Project Master', 'guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Customer','sub_module_name'=>'listing','module_name'=>'Group','module_name_id'=>20],

            //create in Contact
            ['name' => 'group_checkbox_create','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'create','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Group','module_name_id'=>20],
            ['name' => 'group_name_create','group'=>'Project Master', 'guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Group','module_name_id'=>20],
            ['name' => 'group_division_create','group'=>'Project Master', 'guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Division','sub_module_name'=>'listing','module_name'=>'Group','module_name_id'=>20],
            ['name' => 'group_customer_create','group'=>'Project Master', 'guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Customer','sub_module_name'=>'listing','module_name'=>'Group','module_name_id'=>20],

            //edit in Contact
            ['name' => 'group_checkbox_edit','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'edit','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Group','module_name_id'=>20],
            ['name' => 'group_name_edit','group'=>'Project Master', 'guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Group','module_name_id'=>20],
            ['name' => 'group_division_edit','group'=>'Project Master', 'guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Division','sub_module_name'=>'listing','module_name'=>'Group','module_name_id'=>20],
            ['name' => 'group_customer_edit','group'=>'Project Master', 'guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Customer','sub_module_name'=>'listing','module_name'=>'Group','module_name_id'=>20],
        ];

        foreach ($permission_array as $key => $permission) {
            $admin_role = Role::find(1);
            $getpermission=Permission::where('name',$permission['name'])->first();
            if (empty($getpermission)){
                $permission=Permission::create($permission);
                $admin_role->givePermissionTo($permission['name']);
            }else{
                $permission=Permission::where('name',$getpermission->name)->update($permission);
                $admin_role->givePermissionTo($getpermission->name);
            }
        }
    }
}
