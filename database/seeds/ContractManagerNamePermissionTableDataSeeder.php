<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
class ContractManagerNamePermissionTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission_array = [
            // create Contact permissions
            ['name' => 'view_contractmanagerName','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'view','type'=>'','sub_module_name'=>'operation','module_name'=>'Contract Manager Name'],
            ['name' => 'create_contractmanagerName','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'create','type'=>'','sub_module_name'=>'operation','module_name'=>'Contract Manager Name'],
            ['name' => 'edit_contractmanagerName', 'group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'edit','type'=>'','sub_module_name'=>'operation','module_name'=>'Contract Manager Name'],
            ['name' => 'delete_contractmanagerName','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Contract Manager Name'],
            ['name' => 'selected_delete_contractmanagerName','group'=>'Project Master', 'guard_name' => 'web','permission_name'=>'Selected Delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Contract Manager Name'],
            ['name' => 'selected_restore_delete_contractmanagerName', 'group'=>'Project Master','guard_name' => 'web','type'=>'','permission_name'=>'Selected Restore','sub_module_name'=>'operation','module_name'=>'Contract Manager Name'],
            ['name' => 'restore_delete_contractmanagerName','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Restore Delete','sub_module_name'=>'operation','module_name'=>'Contract Manager Name'],
            ['name' => 'hard_delete_contractmanagerName','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Hard Delete','sub_module_name'=>'operation','module_name'=>'Contract Manager Name'],
            ['name' => 'active_contractmanagerName','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Active','sub_module_name'=>'operation','module_name'=>'Contract Manager Name'],
            ['name' => 'selected_active_contractmanagerName','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Selected Active','sub_module_name'=>'operation','module_name'=>'Contract Manager Name'],
            ['name' => 'word_contractmanagerName', 'group'=>'Project Master','guard_name' => 'web','type'=>'','permission_name'=>'Word Export','sub_module_name'=>'operation','module_name'=>'Contract Manager Name'],
            ['name' => 'csv_contractmanagerName','group'=>'Project Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Csv Export','sub_module_name'=>'operation','module_name'=>'Contract Manager Name'],
            ['name' => 'pdf_contractmanagerName', 'group'=>'Project Master','guard_name' => 'web','type'=>'','permission_name'=>'PDF Export','sub_module_name'=>'operation','module_name'=>'Contract Manager Name'],

            //listing in Contact
            ['name' => 'contractmanagerName_checkbox','group'=>'Project Master', 'guard_name' => 'web','enable'=>'1', 'type'=>'view','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Contract Manager Name'],
            ['name' => 'contractmanagerName_name','group'=>'Project Master', 'guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Contract Manager Name'],
            ['name' => 'contractmanagerName_position', 'group'=>'Project Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Position','sub_module_name'=>'listing','module_name'=>'Contract Manager Name'],

            //word in Contact
            ['name' => 'contractmanagerName_checkbox_word_export','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'word_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Contract Manager Name'],
            ['name' => 'contractmanagerName_name_word_export','group'=>'Project Master', 'guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Contract Manager Name'],
            ['name' => 'contractmanagerName_position_word_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Position','sub_module_name'=>'listing','module_name'=>'Contract Manager Name'],

            //excel in Contact
            ['name' => 'contractmanagerName_checkbox_excel_export','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'excel_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Contract Manager Name'],
            ['name' => 'contractmanagerName_name_excel_export','group'=>'Project Master', 'guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Contract Manager Name'],
            ['name' => 'contractmanagerName_position_excel_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Position','sub_module_name'=>'listing','module_name'=>'Contract Manager Name'],

            //pdf in Contact
            ['name' => 'contractmanagerName_checkbox_pdf_export','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'pdf_export','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Contract Manager Name'],
            ['name' => 'contractmanagerName_name_pdf_export','group'=>'Project Master', 'guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Contract Manager Name'],
            ['name' => 'contractmanagerName_position_pdf_export', 'group'=>'Project Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Position','sub_module_name'=>'listing','module_name'=>'Contract Manager Name'],

            //create in Contact
            ['name' => 'contractmanagerName_checkbox_create','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'create','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Contract Manager Name'],
            ['name' => 'contractmanagerName_name_create','group'=>'Project Master', 'guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Contract Manager Name'],
            ['name' => 'contractmanagerName_position_create', 'group'=>'Project Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Position','sub_module_name'=>'listing','module_name'=>'Contract Manager Name'],

            //edit in Contact
            ['name' => 'contractmanagerName_checkbox_edit','group'=>'Project Master', 'guard_name' => 'web','enable'=>'0', 'type'=>'edit','permission_name'=>'checkbox','sub_module_name'=>'listing','module_name'=>'Contract Manager Name'],
            ['name' => 'contractmanagerName_name_edit','group'=>'Project Master', 'guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Contract Manager Name'],
            ['name' => 'contractmanagerName_position_edit', 'group'=>'Project Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Position','sub_module_name'=>'listing','module_name'=>'Contract Manager Name'],
        ];

        foreach ($permission_array as $key => $permission) {
            $admin_role = Role::find(1);
            $getpermission=Permission::where('name',$permission['name'])->first();
            if (empty($getpermission)){
                $permission=Permission::create($permission);
                $admin_role->givePermissionTo($permission['name']);
            }else{
                $permission=Permission::where('name',$getpermission->name)->update($permission);
                $admin_role->givePermissionTo($getpermission->name);
            }
        }
    }
}
