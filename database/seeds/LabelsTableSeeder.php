<?php

use Illuminate\Database\Seeder;

class LabelsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('labels')->delete();
        
        \DB::table('labels')->insert(array (
            0 => 
            array (
                'id' => 1,
                'key' => 'login',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 06:24:32',
                'updated_at' => '2020-05-12 06:24:32',
            ),
            1 => 
            array (
                'id' => 2,
                'key' => 'login',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 06:24:32',
                'updated_at' => '2020-05-12 06:24:32',
            ),
            2 => 
            array (
                'id' => 3,
                'key' => 'email',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 06:25:30',
                'updated_at' => '2020-05-12 06:25:30',
            ),
            3 => 
            array (
                'id' => 4,
                'key' => 'email',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 06:25:30',
                'updated_at' => '2020-05-12 06:25:30',
            ),
            4 => 
            array (
                'id' => 5,
                'key' => 'password',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 06:26:02',
                'updated_at' => '2020-05-12 06:26:02',
            ),
            5 => 
            array (
                'id' => 6,
                'key' => 'password',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 06:26:03',
                'updated_at' => '2020-05-12 06:26:03',
            ),
            6 => 
            array (
                'id' => 7,
                'key' => 'sign_up',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 06:27:28',
                'updated_at' => '2020-05-12 06:27:28',
            ),
            7 => 
            array (
                'id' => 8,
                'key' => 'sign_up',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 06:27:28',
                'updated_at' => '2020-05-12 06:27:28',
            ),
            8 => 
            array (
                'id' => 9,
                'key' => 'forget_password',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 06:27:47',
                'updated_at' => '2020-05-12 06:27:47',
            ),
            9 => 
            array (
                'id' => 10,
                'key' => 'forget_password',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 06:27:47',
                'updated_at' => '2020-05-12 06:27:47',
            ),
            10 => 
            array (
                'id' => 11,
                'key' => 'content.login.header',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 06:30:30',
                'updated_at' => '2020-05-12 06:30:30',
            ),
            11 => 
            array (
                'id' => 12,
                'key' => 'content.login.header',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 06:30:30',
                'updated_at' => '2020-05-12 06:30:30',
            ),
            12 => 
            array (
                'id' => 13,
                'key' => 'content.login.paragraph',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 06:30:49',
                'updated_at' => '2020-05-12 06:30:49',
            ),
            13 => 
            array (
                'id' => 14,
                'key' => 'content.login.paragraph',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 06:30:49',
                'updated_at' => '2020-05-12 06:30:49',
            ),
            14 => 
            array (
                'id' => 15,
                'key' => 'dashboard',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 06:32:18',
                'updated_at' => '2020-05-12 06:32:18',
            ),
            15 => 
            array (
                'id' => 16,
                'key' => 'dashboard',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 06:32:18',
                'updated_at' => '2020-05-12 06:32:18',
            ),
            16 => 
            array (
                'id' => 17,
                'key' => 'project',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 06:33:03',
                'updated_at' => '2020-05-12 06:33:03',
            ),
            17 => 
            array (
                'id' => 18,
                'key' => 'project',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 06:33:03',
                'updated_at' => '2020-05-12 06:33:03',
            ),
            18 => 
            array (
                'id' => 19,
                'key' => 'client',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 06:35:22',
                'updated_at' => '2020-05-12 06:35:22',
            ),
            19 => 
            array (
                'id' => 20,
                'key' => 'client',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 06:35:22',
                'updated_at' => '2020-05-12 06:35:22',
            ),
            20 => 
            array (
                'id' => 21,
                'key' => 'document_library',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 06:36:21',
                'updated_at' => '2020-05-12 06:36:21',
            ),
            21 => 
            array (
                'id' => 22,
                'key' => 'document_library',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 06:36:21',
                'updated_at' => '2020-05-12 06:36:21',
            ),
            22 => 
            array (
                'id' => 23,
                'key' => 'employees',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 06:36:44',
                'updated_at' => '2020-05-12 06:36:44',
            ),
            23 => 
            array (
                'id' => 24,
                'key' => 'employees',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 06:36:44',
                'updated_at' => '2020-05-12 06:36:44',
            ),
            24 => 
            array (
                'id' => 25,
                'key' => 'required_documents',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 06:37:26',
                'updated_at' => '2020-05-12 06:37:26',
            ),
            25 => 
            array (
                'id' => 26,
                'key' => 'required_documents',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 06:37:26',
                'updated_at' => '2020-05-12 06:37:26',
            ),
            26 => 
            array (
                'id' => 27,
                'key' => 'jobs',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 06:37:38',
                'updated_at' => '2020-05-12 06:37:38',
            ),
            27 => 
            array (
                'id' => 28,
                'key' => 'jobs',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 06:37:38',
                'updated_at' => '2020-05-12 06:37:38',
            ),
            28 => 
            array (
                'id' => 29,
                'key' => 'support',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 06:38:54',
                'updated_at' => '2020-05-12 06:38:54',
            ),
            29 => 
            array (
                'id' => 30,
                'key' => 'support',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 06:38:54',
                'updated_at' => '2020-05-12 06:38:54',
            ),
            30 => 
            array (
                'id' => 31,
                'key' => 'administration',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 06:39:35',
                'updated_at' => '2020-05-12 06:39:35',
            ),
            31 => 
            array (
                'id' => 32,
                'key' => 'administration',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 06:39:35',
                'updated_at' => '2020-05-12 06:39:35',
            ),
            32 => 
            array (
                'id' => 33,
                'key' => 'roles',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 06:39:54',
                'updated_at' => '2020-05-12 06:39:54',
            ),
            33 => 
            array (
                'id' => 34,
                'key' => 'roles',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 06:39:54',
                'updated_at' => '2020-05-12 06:39:54',
            ),
            34 => 
            array (
                'id' => 35,
                'key' => 'permissions',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 06:40:25',
                'updated_at' => '2020-05-12 06:40:25',
            ),
            35 => 
            array (
                'id' => 36,
                'key' => 'permissions',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 06:40:25',
                'updated_at' => '2020-05-12 06:40:25',
            ),
            36 => 
            array (
                'id' => 37,
                'key' => 'audit_log',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 06:40:45',
                'updated_at' => '2020-05-12 06:40:45',
            ),
            37 => 
            array (
                'id' => 38,
                'key' => 'audit_log',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 06:40:45',
                'updated_at' => '2020-05-12 06:40:45',
            ),
            38 => 
            array (
                'id' => 39,
                'key' => 'company',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 06:40:58',
                'updated_at' => '2020-05-12 06:40:58',
            ),
            39 => 
            array (
                'id' => 40,
                'key' => 'company',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 06:40:58',
                'updated_at' => '2020-05-12 06:40:58',
            ),
            40 => 
            array (
                'id' => 41,
                'key' => 'reports',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 06:45:12',
                'updated_at' => '2020-05-12 06:45:12',
            ),
            41 => 
            array (
                'id' => 42,
                'key' => 'reports',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 06:45:12',
                'updated_at' => '2020-05-12 06:45:12',
            ),
            42 => 
            array (
                'id' => 43,
                'key' => 'qr',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 06:45:52',
                'updated_at' => '2020-05-12 06:45:52',
            ),
            43 => 
            array (
                'id' => 44,
                'key' => 'qr',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 06:45:52',
                'updated_at' => '2020-05-12 06:45:52',
            ),
            44 => 
            array (
                'id' => 45,
                'key' => 'plant_and_equipment',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 06:46:50',
                'updated_at' => '2020-05-12 06:46:50',
            ),
            45 => 
            array (
                'id' => 46,
                'key' => 'plant_and_equipment',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 06:46:50',
                'updated_at' => '2020-05-12 06:46:50',
            ),
            46 => 
            array (
                'id' => 47,
                'key' => 'completed_forms',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 06:47:10',
                'updated_at' => '2020-05-12 06:47:10',
            ),
            47 => 
            array (
                'id' => 48,
                'key' => 'completed_forms',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 06:47:10',
                'updated_at' => '2020-05-12 06:47:10',
            ),
            48 => 
            array (
                'id' => 49,
                'key' => 'fors',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 06:47:34',
                'updated_at' => '2020-05-12 06:47:34',
            ),
            49 => 
            array (
                'id' => 50,
                'key' => 'fors',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 06:47:34',
                'updated_at' => '2020-05-12 06:47:34',
            ),
            50 => 
            array (
                'id' => 51,
                'key' => 'cloc',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 06:47:49',
                'updated_at' => '2020-05-12 06:47:49',
            ),
            51 => 
            array (
                'id' => 52,
                'key' => 'cloc',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 06:47:49',
                'updated_at' => '2020-05-12 06:47:49',
            ),
            52 => 
            array (
                'id' => 53,
                'key' => 'sia',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 06:48:06',
                'updated_at' => '2020-05-12 06:48:06',
            ),
            53 => 
            array (
                'id' => 54,
                'key' => 'sia',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 06:48:06',
                'updated_at' => '2020-05-12 06:48:06',
            ),
            54 => 
            array (
                'id' => 55,
                'key' => 'app_location',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 06:48:48',
                'updated_at' => '2020-05-12 06:48:48',
            ),
            55 => 
            array (
                'id' => 56,
                'key' => 'app_location',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 06:48:48',
                'updated_at' => '2020-05-12 06:48:48',
            ),
            56 => 
            array (
                'id' => 57,
                'key' => 'panic_alarms',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 06:49:24',
                'updated_at' => '2020-05-12 06:49:24',
            ),
            57 => 
            array (
                'id' => 58,
                'key' => 'panic_alarms',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 06:49:24',
                'updated_at' => '2020-05-12 06:49:24',
            ),
            58 => 
            array (
                'id' => 59,
                'key' => 'selected_delete',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 06:54:17',
                'updated_at' => '2020-05-12 06:54:17',
            ),
            59 => 
            array (
                'id' => 60,
                'key' => 'selected_delete',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 06:54:17',
                'updated_at' => '2020-05-12 06:54:17',
            ),
            60 => 
            array (
                'id' => 61,
                'key' => 'restore',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 06:54:58',
                'updated_at' => '2020-05-12 06:54:58',
            ),
            61 => 
            array (
                'id' => 62,
                'key' => 'restore',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 06:54:58',
                'updated_at' => '2020-05-12 06:54:58',
            ),
            62 => 
            array (
                'id' => 63,
                'key' => 'selected_active',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 06:55:50',
                'updated_at' => '2020-05-12 06:55:50',
            ),
            63 => 
            array (
                'id' => 64,
                'key' => 'selected_active',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 06:55:50',
                'updated_at' => '2020-05-12 06:55:50',
            ),
            64 => 
            array (
                'id' => 65,
                'key' => 'show_active',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 06:56:13',
                'updated_at' => '2020-05-12 06:56:13',
            ),
            65 => 
            array (
                'id' => 66,
                'key' => 'show_active',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 06:56:13',
                'updated_at' => '2020-05-12 06:56:13',
            ),
            66 => 
            array (
                'id' => 67,
                'key' => 'excel_export',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 06:56:51',
                'updated_at' => '2020-05-12 06:56:51',
            ),
            67 => 
            array (
                'id' => 68,
                'key' => 'excel_export',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 06:56:51',
                'updated_at' => '2020-05-12 06:56:51',
            ),
            68 => 
            array (
                'id' => 69,
                'key' => 'word_export',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 06:57:10',
                'updated_at' => '2020-05-12 06:57:10',
            ),
            69 => 
            array (
                'id' => 70,
                'key' => 'word_export',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 06:57:10',
                'updated_at' => '2020-05-12 06:57:10',
            ),
            70 => 
            array (
                'id' => 71,
                'key' => 'pdf_export',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 06:57:31',
                'updated_at' => '2020-05-12 06:57:31',
            ),
            71 => 
            array (
                'id' => 72,
                'key' => 'pdf_export',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 06:57:31',
                'updated_at' => '2020-05-12 06:57:31',
            ),
            72 => 
            array (
                'id' => 73,
                'key' => 'all',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 06:58:24',
                'updated_at' => '2020-05-12 06:58:24',
            ),
            73 => 
            array (
                'id' => 74,
                'key' => 'all',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 06:58:24',
                'updated_at' => '2020-05-12 06:58:24',
            ),
            74 => 
            array (
                'id' => 75,
                'key' => 'name',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 06:59:41',
                'updated_at' => '2020-05-12 06:59:41',
            ),
            75 => 
            array (
                'id' => 76,
                'key' => 'name',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 06:59:41',
                'updated_at' => '2020-05-12 06:59:41',
            ),
            76 => 
            array (
                'id' => 77,
                'key' => 'devision',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 07:00:47',
                'updated_at' => '2020-05-12 07:00:47',
            ),
            77 => 
            array (
                'id' => 78,
                'key' => 'devision',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 07:00:48',
                'updated_at' => '2020-05-12 07:00:48',
            ),
            78 => 
            array (
                'id' => 79,
                'key' => 'customer',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 07:01:17',
                'updated_at' => '2020-05-12 07:01:17',
            ),
            79 => 
            array (
                'id' => 80,
                'key' => 'customer',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 07:01:17',
                'updated_at' => '2020-05-12 07:01:17',
            ),
            80 => 
            array (
                'id' => 81,
                'key' => 'group',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 07:02:04',
                'updated_at' => '2020-05-12 07:02:04',
            ),
            81 => 
            array (
                'id' => 82,
                'key' => 'group',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 07:02:04',
                'updated_at' => '2020-05-12 07:02:04',
            ),
            82 => 
            array (
                'id' => 83,
                'key' => 'manager',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 07:02:29',
                'updated_at' => '2020-05-12 07:02:29',
            ),
            83 => 
            array (
                'id' => 84,
                'key' => 'manager',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 07:02:29',
                'updated_at' => '2020-05-12 07:02:29',
            ),
            84 => 
            array (
                'id' => 85,
                'key' => 'contract_manager',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 07:02:52',
                'updated_at' => '2020-05-12 07:02:52',
            ),
            85 => 
            array (
                'id' => 86,
                'key' => 'contract_manager',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 07:02:52',
                'updated_at' => '2020-05-12 07:02:52',
            ),
            86 => 
            array (
                'id' => 87,
                'key' => 'start_date',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 07:06:33',
                'updated_at' => '2020-05-12 07:06:33',
            ),
            87 => 
            array (
                'id' => 88,
                'key' => 'start_date',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 07:06:33',
                'updated_at' => '2020-05-12 07:06:33',
            ),
            88 => 
            array (
                'id' => 89,
                'key' => 'expiry_date',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 07:06:53',
                'updated_at' => '2020-05-12 07:06:53',
            ),
            89 => 
            array (
                'id' => 90,
                'key' => 'expiry_date',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 07:06:53',
                'updated_at' => '2020-05-12 07:06:53',
            ),
            90 => 
            array (
                'id' => 91,
                'key' => 'previous',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 07:10:36',
                'updated_at' => '2020-05-12 07:10:36',
            ),
            91 => 
            array (
                'id' => 92,
                'key' => 'previous',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 07:10:36',
                'updated_at' => '2020-05-12 07:10:36',
            ),
            92 => 
            array (
                'id' => 93,
                'key' => 'next',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 07:10:43',
                'updated_at' => '2020-05-12 07:10:43',
            ),
            93 => 
            array (
                'id' => 94,
                'key' => 'next',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 07:10:43',
                'updated_at' => '2020-05-12 07:10:43',
            ),
            94 => 
            array (
                'id' => 95,
                'key' => 'search',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 07:11:05',
                'updated_at' => '2020-05-12 07:11:05',
            ),
            95 => 
            array (
                'id' => 96,
                'key' => 'search',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 07:11:05',
                'updated_at' => '2020-05-12 07:11:05',
            ),
            96 => 
            array (
                'id' => 97,
                'key' => 'detail',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 07:18:41',
                'updated_at' => '2020-05-12 07:18:41',
            ),
            97 => 
            array (
                'id' => 98,
                'key' => 'detail',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 07:18:41',
                'updated_at' => '2020-05-12 07:18:41',
            ),
            98 => 
            array (
                'id' => 99,
                'key' => 'ref',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 07:24:45',
                'updated_at' => '2020-05-12 07:24:45',
            ),
            99 => 
            array (
                'id' => 100,
                'key' => 'ref',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 07:24:45',
                'updated_at' => '2020-05-12 07:24:45',
            ),
            100 => 
            array (
                'id' => 101,
                'key' => 'account_number',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 07:25:09',
                'updated_at' => '2020-05-12 07:25:09',
            ),
            101 => 
            array (
                'id' => 102,
                'key' => 'account_number',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 07:25:09',
                'updated_at' => '2020-05-12 07:25:09',
            ),
            102 => 
            array (
                'id' => 103,
                'key' => 'contractor',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 07:26:11',
                'updated_at' => '2020-05-12 07:26:11',
            ),
            103 => 
            array (
                'id' => 104,
                'key' => 'contractor',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 07:26:11',
                'updated_at' => '2020-05-12 07:26:11',
            ),
            104 => 
            array (
                'id' => 105,
                'key' => 'staff',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 07:35:51',
                'updated_at' => '2020-05-12 07:35:51',
            ),
            105 => 
            array (
                'id' => 106,
                'key' => 'staff',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 07:35:51',
                'updated_at' => '2020-05-12 07:35:51',
            ),
            106 => 
            array (
                'id' => 107,
                'key' => 'notes',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 07:42:41',
                'updated_at' => '2020-05-12 07:42:41',
            ),
            107 => 
            array (
                'id' => 108,
                'key' => 'notes',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 07:42:41',
                'updated_at' => '2020-05-12 07:42:41',
            ),
            108 => 
            array (
                'id' => 109,
                'key' => 'termination_reason',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 07:44:11',
                'updated_at' => '2020-05-12 07:44:11',
            ),
            109 => 
            array (
                'id' => 110,
                'key' => 'termination_reason',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 07:44:11',
                'updated_at' => '2020-05-12 07:44:11',
            ),
            110 => 
            array (
                'id' => 111,
                'key' => 'update',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 07:45:10',
                'updated_at' => '2020-05-12 07:45:10',
            ),
            111 => 
            array (
                'id' => 112,
                'key' => 'update',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 07:45:10',
                'updated_at' => '2020-05-12 07:45:10',
            ),
            112 => 
            array (
                'id' => 113,
                'key' => 'general',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 07:46:54',
                'updated_at' => '2020-05-12 07:46:54',
            ),
            113 => 
            array (
                'id' => 114,
                'key' => 'general',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 07:46:54',
                'updated_at' => '2020-05-12 07:46:54',
            ),
            114 => 
            array (
                'id' => 115,
                'key' => 'allowed',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 07:58:51',
                'updated_at' => '2020-05-12 07:58:51',
            ),
            115 => 
            array (
                'id' => 116,
                'key' => 'allowed',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 07:58:51',
                'updated_at' => '2020-05-12 07:58:51',
            ),
            116 => 
            array (
                'id' => 117,
                'key' => 'banned',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 07:59:15',
                'updated_at' => '2020-05-12 07:59:15',
            ),
            117 => 
            array (
                'id' => 118,
                'key' => 'banned',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 07:59:15',
                'updated_at' => '2020-05-12 07:59:15',
            ),
            118 => 
            array (
                'id' => 119,
                'key' => 'surname',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 08:06:32',
                'updated_at' => '2020-05-12 08:06:32',
            ),
            119 => 
            array (
                'id' => 120,
                'key' => 'surname',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 08:06:32',
                'updated_at' => '2020-05-12 08:06:32',
            ),
            120 => 
            array (
                'id' => 121,
                'key' => 'phone',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 08:06:41',
                'updated_at' => '2020-05-12 08:06:41',
            ),
            121 => 
            array (
                'id' => 122,
                'key' => 'phone',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 08:06:41',
                'updated_at' => '2020-05-12 08:06:41',
            ),
            122 => 
            array (
                'id' => 123,
                'key' => 'address',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 08:06:54',
                'updated_at' => '2020-05-12 08:06:54',
            ),
            123 => 
            array (
                'id' => 124,
                'key' => 'address',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 08:06:54',
                'updated_at' => '2020-05-12 08:06:54',
            ),
            124 => 
            array (
                'id' => 125,
                'key' => 'position',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 08:10:33',
                'updated_at' => '2020-05-12 08:10:33',
            ),
            125 => 
            array (
                'id' => 126,
                'key' => 'position',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 08:10:33',
                'updated_at' => '2020-05-12 08:10:33',
            ),
            126 => 
            array (
                'id' => 127,
                'key' => 'main_detail',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 08:55:28',
                'updated_at' => '2020-05-12 08:55:28',
            ),
            127 => 
            array (
                'id' => 128,
                'key' => 'main_detail',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 08:55:28',
                'updated_at' => '2020-05-12 08:55:28',
            ),
            128 => 
            array (
                'id' => 129,
                'key' => 'bonus_score',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 08:56:14',
                'updated_at' => '2020-05-12 08:56:14',
            ),
            129 => 
            array (
                'id' => 130,
                'key' => 'bonus_score',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 08:56:14',
                'updated_at' => '2020-05-12 08:56:14',
            ),
            130 => 
            array (
                'id' => 131,
                'key' => 'contact',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 08:59:01',
                'updated_at' => '2020-05-12 08:59:01',
            ),
            131 => 
            array (
                'id' => 132,
                'key' => 'contact',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 08:59:01',
                'updated_at' => '2020-05-12 08:59:01',
            ),
            132 => 
            array (
                'id' => 133,
                'key' => 'month',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 09:07:09',
                'updated_at' => '2020-05-12 09:07:09',
            ),
            133 => 
            array (
                'id' => 134,
                'key' => 'month',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 09:07:09',
                'updated_at' => '2020-05-12 09:07:09',
            ),
            134 => 
            array (
                'id' => 135,
                'key' => 'year',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 09:07:27',
                'updated_at' => '2020-05-12 09:07:27',
            ),
            135 => 
            array (
                'id' => 136,
                'key' => 'year',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 09:07:27',
                'updated_at' => '2020-05-12 09:07:27',
            ),
            136 => 
            array (
                'id' => 137,
                'key' => 'score',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 09:07:41',
                'updated_at' => '2020-05-12 09:07:41',
            ),
            137 => 
            array (
                'id' => 138,
                'key' => 'score',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 09:07:41',
                'updated_at' => '2020-05-12 09:07:41',
            ),
            138 => 
            array (
                'id' => 139,
                'key' => 'building',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 09:18:25',
                'updated_at' => '2020-05-12 09:18:25',
            ),
            139 => 
            array (
                'id' => 140,
                'key' => 'building',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 09:18:25',
                'updated_at' => '2020-05-12 09:18:25',
            ),
            140 => 
            array (
                'id' => 141,
                'key' => 'address_type',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 09:19:17',
                'updated_at' => '2020-05-12 09:19:17',
            ),
            141 => 
            array (
                'id' => 142,
                'key' => 'address_type',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 09:19:17',
                'updated_at' => '2020-05-12 09:19:17',
            ),
            142 => 
            array (
                'id' => 143,
                'key' => 'post_code',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 09:20:47',
                'updated_at' => '2020-05-12 09:20:47',
            ),
            143 => 
            array (
                'id' => 144,
                'key' => 'post_code',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 09:20:47',
                'updated_at' => '2020-05-12 09:20:47',
            ),
            144 => 
            array (
                'id' => 145,
                'key' => 'street',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 09:33:40',
                'updated_at' => '2020-05-12 09:33:40',
            ),
            145 => 
            array (
                'id' => 146,
                'key' => 'street',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 09:33:40',
                'updated_at' => '2020-05-12 09:33:40',
            ),
            146 => 
            array (
                'id' => 147,
                'key' => 'town',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 09:34:10',
                'updated_at' => '2020-05-12 09:34:10',
            ),
            147 => 
            array (
                'id' => 148,
                'key' => 'town',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 09:34:10',
                'updated_at' => '2020-05-12 09:34:10',
            ),
            148 => 
            array (
                'id' => 149,
                'key' => 'state',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 09:34:36',
                'updated_at' => '2020-05-12 09:34:36',
            ),
            149 => 
            array (
                'id' => 150,
                'key' => 'state',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 09:34:36',
                'updated_at' => '2020-05-12 09:34:36',
            ),
            150 => 
            array (
                'id' => 151,
                'key' => 'country',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 09:35:03',
                'updated_at' => '2020-05-12 09:35:03',
            ),
            151 => 
            array (
                'id' => 152,
                'key' => 'country',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 09:35:03',
                'updated_at' => '2020-05-12 09:35:03',
            ),
            152 => 
            array (
                'id' => 153,
                'key' => 'number',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-12 09:46:11',
                'updated_at' => '2020-05-12 09:46:11',
            ),
            153 => 
            array (
                'id' => 154,
                'key' => 'number',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-12 09:46:11',
                'updated_at' => '2020-05-12 09:46:11',
            ),
            154 => 
            array (
                'id' => 155,
                'key' => 'first_name',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-13 06:42:57',
                'updated_at' => '2020-05-13 06:42:57',
            ),
            155 => 
            array (
                'id' => 156,
                'key' => 'first_name',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-13 06:42:57',
                'updated_at' => '2020-05-13 06:42:57',
            ),
            156 => 
            array (
                'id' => 157,
                'key' => 'contact_type',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-13 06:43:12',
                'updated_at' => '2020-05-13 06:43:12',
            ),
            157 => 
            array (
                'id' => 158,
                'key' => 'contact_type',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-13 06:43:12',
                'updated_at' => '2020-05-13 06:43:12',
            ),
            158 => 
            array (
                'id' => 159,
                'key' => 'nothing_found_sorry',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-13 06:50:01',
                'updated_at' => '2020-05-13 06:50:01',
            ),
            159 => 
            array (
                'id' => 160,
                'key' => 'nothing_found_sorry',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-13 06:50:01',
                'updated_at' => '2020-05-13 06:50:01',
            ),
            160 => 
            array (
                'id' => 161,
                'key' => 'no_records_available',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-13 06:50:17',
                'updated_at' => '2020-05-13 06:50:17',
            ),
            161 => 
            array (
                'id' => 162,
                'key' => 'no_records_available',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-13 06:50:17',
                'updated_at' => '2020-05-13 06:50:17',
            ),
            162 => 
            array (
                'id' => 163,
                'key' => 'extension',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-13 07:03:31',
                'updated_at' => '2020-05-13 07:03:31',
            ),
            163 => 
            array (
                'id' => 164,
                'key' => 'extension',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-13 07:03:31',
                'updated_at' => '2020-05-13 07:03:31',
            ),
            164 => 
            array (
                'id' => 165,
                'key' => 'mobile_number',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-13 07:03:52',
                'updated_at' => '2020-05-13 07:03:52',
            ),
            165 => 
            array (
                'id' => 166,
                'key' => 'mobile_number',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-13 07:03:52',
                'updated_at' => '2020-05-13 07:03:52',
            ),
            166 => 
            array (
                'id' => 167,
                'key' => 'other_number',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-13 07:04:27',
                'updated_at' => '2020-05-13 07:04:27',
            ),
            167 => 
            array (
                'id' => 168,
                'key' => 'other_number',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-13 07:04:27',
                'updated_at' => '2020-05-13 07:04:27',
            ),
            168 => 
            array (
                'id' => 169,
                'key' => 'title',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-13 07:05:25',
                'updated_at' => '2020-05-13 07:05:25',
            ),
            169 => 
            array (
                'id' => 170,
                'key' => 'title',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-13 07:05:25',
                'updated_at' => '2020-05-13 07:05:25',
            ),
            170 => 
            array (
                'id' => 171,
                'key' => 'save',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-13 07:06:06',
                'updated_at' => '2020-05-13 07:06:06',
            ),
            171 => 
            array (
                'id' => 172,
                'key' => 'save',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-13 07:06:06',
                'updated_at' => '2020-05-13 07:06:06',
            ),
            172 => 
            array (
                'id' => 173,
                'key' => 'key',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-13 07:27:21',
                'updated_at' => '2020-05-13 07:27:21',
            ),
            173 => 
            array (
                'id' => 174,
                'key' => 'key',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-13 07:27:21',
                'updated_at' => '2020-05-13 07:27:21',
            ),
            174 => 
            array (
                'id' => 175,
                'key' => 'value',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-13 07:27:30',
                'updated_at' => '2020-05-13 07:27:30',
            ),
            175 => 
            array (
                'id' => 176,
                'key' => 'value',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-13 07:27:30',
                'updated_at' => '2020-05-13 07:27:30',
            ),
            176 => 
            array (
                'id' => 177,
                'key' => 'equipmentGroup',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-13 08:11:38',
                'updated_at' => '2020-05-13 08:11:38',
            ),
            177 => 
            array (
                'id' => 178,
                'key' => 'equipmentGroup',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-13 08:11:38',
                'updated_at' => '2020-05-13 08:11:38',
            ),
            178 => 
            array (
                'id' => 181,
                'key' => 'equipment',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-13 08:12:17',
                'updated_at' => '2020-05-13 08:12:17',
            ),
            179 => 
            array (
                'id' => 182,
                'key' => 'equipment',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-13 08:12:17',
                'updated_at' => '2020-05-13 08:12:17',
            ),
            180 => 
            array (
                'id' => 183,
                'key' => 'serial_number',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-13 08:17:30',
                'updated_at' => '2020-05-13 08:17:30',
            ),
            181 => 
            array (
                'id' => 184,
                'key' => 'serial_number',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-13 08:17:30',
                'updated_at' => '2020-05-13 08:17:30',
            ),
            182 => 
            array (
                'id' => 185,
                'key' => 'last_service_date',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-13 08:18:13',
                'updated_at' => '2020-05-13 08:18:13',
            ),
            183 => 
            array (
                'id' => 186,
                'key' => 'last_service_date',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-13 08:18:13',
                'updated_at' => '2020-05-13 08:18:13',
            ),
            184 => 
            array (
                'id' => 187,
                'key' => 'next_service_date',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-13 08:18:37',
                'updated_at' => '2020-05-13 08:18:37',
            ),
            185 => 
            array (
                'id' => 188,
                'key' => 'next_service_date',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-13 08:18:37',
                'updated_at' => '2020-05-13 08:18:37',
            ),
            186 => 
            array (
                'id' => 189,
                'key' => 'condition',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-13 08:18:57',
                'updated_at' => '2020-05-13 08:18:57',
            ),
            187 => 
            array (
                'id' => 190,
                'key' => 'condition',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-13 08:18:57',
                'updated_at' => '2020-05-13 08:18:57',
            ),
            188 => 
            array (
                'id' => 191,
                'key' => 'make',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-13 08:19:37',
                'updated_at' => '2020-05-13 08:19:37',
            ),
            189 => 
            array (
                'id' => 192,
                'key' => 'make',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-13 08:19:37',
                'updated_at' => '2020-05-13 08:19:37',
            ),
            190 => 
            array (
                'id' => 193,
                'key' => 'model',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-13 08:19:48',
                'updated_at' => '2020-05-13 08:19:48',
            ),
            191 => 
            array (
                'id' => 194,
                'key' => 'model',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-13 08:19:48',
                'updated_at' => '2020-05-13 08:19:48',
            ),
            192 => 
            array (
                'id' => 195,
                'key' => 'directions',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-13 08:28:13',
                'updated_at' => '2020-05-13 08:28:13',
            ),
            193 => 
            array (
                'id' => 196,
                'key' => 'directions',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-13 08:28:13',
                'updated_at' => '2020-05-13 08:28:13',
            ),
            194 => 
            array (
                'id' => 197,
                'key' => 'information',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-13 08:28:29',
                'updated_at' => '2020-05-13 08:28:29',
            ),
            195 => 
            array (
                'id' => 198,
                'key' => 'information',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-13 08:28:29',
                'updated_at' => '2020-05-13 08:28:29',
            ),
            196 => 
            array (
                'id' => 199,
                'key' => 'training_matrix',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-13 09:01:45',
                'updated_at' => '2020-05-13 09:01:45',
            ),
            197 => 
            array (
                'id' => 200,
                'key' => 'training_matrix',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-13 09:01:45',
                'updated_at' => '2020-05-13 09:01:45',
            ),
            198 => 
            array (
                'id' => 201,
                'key' => 'payroll_number',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-13 09:05:02',
                'updated_at' => '2020-05-13 09:05:02',
            ),
            199 => 
            array (
                'id' => 202,
                'key' => 'payroll_number',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-13 09:05:02',
                'updated_at' => '2020-05-13 09:05:02',
            ),
            200 => 
            array (
                'id' => 203,
                'key' => 'files',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-13 09:16:02',
                'updated_at' => '2020-05-13 09:16:02',
            ),
            201 => 
            array (
                'id' => 204,
                'key' => 'files',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-13 09:16:03',
                'updated_at' => '2020-05-13 09:16:03',
            ),
            202 => 
            array (
                'id' => 205,
                'key' => 'locations',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-13 09:17:07',
                'updated_at' => '2020-05-13 09:17:07',
            ),
            203 => 
            array (
                'id' => 206,
                'key' => 'locations',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-13 09:17:07',
                'updated_at' => '2020-05-13 09:17:07',
            ),
            204 => 
            array (
                'id' => 207,
                'key' => 'contact_information',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-13 09:17:43',
                'updated_at' => '2020-05-13 09:17:43',
            ),
            205 => 
            array (
                'id' => 208,
                'key' => 'contact_information',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-13 09:17:43',
                'updated_at' => '2020-05-13 09:17:43',
            ),
            206 => 
            array (
                'id' => 209,
                'key' => 'next_of_kin',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-13 09:19:22',
                'updated_at' => '2020-05-13 09:19:22',
            ),
            207 => 
            array (
                'id' => 210,
                'key' => 'next_of_kin',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-13 09:19:22',
                'updated_at' => '2020-05-13 09:19:22',
            ),
            208 => 
            array (
                'id' => 211,
                'key' => 'documents',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-13 09:22:47',
                'updated_at' => '2020-05-13 09:22:47',
            ),
            209 => 
            array (
                'id' => 212,
                'key' => 'documents',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-13 09:22:47',
                'updated_at' => '2020-05-13 09:22:47',
            ),
            210 => 
            array (
                'id' => 213,
                'key' => 'account',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-13 09:23:05',
                'updated_at' => '2020-05-13 09:23:05',
            ),
            211 => 
            array (
                'id' => 214,
                'key' => 'account',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-13 09:23:05',
                'updated_at' => '2020-05-13 09:23:05',
            ),
            212 => 
            array (
                'id' => 217,
                'key' => 'employee_type',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-13 09:31:56',
                'updated_at' => '2020-05-13 09:31:56',
            ),
            213 => 
            array (
                'id' => 218,
                'key' => 'employee_type',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-13 09:31:56',
                'updated_at' => '2020-05-13 09:31:56',
            ),
            214 => 
            array (
                'id' => 219,
                'key' => 'sex',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-13 09:35:44',
                'updated_at' => '2020-05-13 09:35:44',
            ),
            215 => 
            array (
                'id' => 220,
                'key' => 'sex',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-13 09:35:44',
                'updated_at' => '2020-05-13 09:35:44',
            ),
            216 => 
            array (
                'id' => 221,
                'key' => 'dob',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-13 09:36:11',
                'updated_at' => '2020-05-13 09:36:11',
            ),
            217 => 
            array (
                'id' => 222,
                'key' => 'dob',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-13 09:36:11',
                'updated_at' => '2020-05-13 09:36:11',
            ),
            218 => 
            array (
                'id' => 223,
                'key' => 'ved',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-13 09:38:05',
                'updated_at' => '2020-05-13 09:38:05',
            ),
            219 => 
            array (
                'id' => 224,
                'key' => 'ved',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-13 09:38:05',
                'updated_at' => '2020-05-13 09:38:05',
            ),
            220 => 
            array (
                'id' => 225,
                'key' => 'national_Insurance_number',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-13 09:39:15',
                'updated_at' => '2020-05-13 09:39:15',
            ),
            221 => 
            array (
                'id' => 226,
                'key' => 'national_Insurance_number',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-13 09:39:15',
                'updated_at' => '2020-05-13 09:39:15',
            ),
            222 => 
            array (
                'id' => 227,
                'key' => 'cscs_card',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-13 09:40:42',
                'updated_at' => '2020-05-13 09:40:42',
            ),
            223 => 
            array (
                'id' => 228,
                'key' => 'cscs_card',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-13 09:40:42',
                'updated_at' => '2020-05-13 09:40:42',
            ),
            224 => 
            array (
                'id' => 229,
                'key' => 'cscs_card_expiry_date',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-13 09:41:15',
                'updated_at' => '2020-05-13 09:41:15',
            ),
            225 => 
            array (
                'id' => 230,
                'key' => 'cscs_card_expiry_date',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-13 09:41:15',
                'updated_at' => '2020-05-13 09:41:15',
            ),
            226 => 
            array (
                'id' => 231,
                'key' => 'sexual_orientation',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-13 09:42:10',
                'updated_at' => '2020-05-13 09:42:10',
            ),
            227 => 
            array (
                'id' => 232,
                'key' => 'sexual_orientation',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-13 09:42:10',
                'updated_at' => '2020-05-13 09:42:10',
            ),
            228 => 
            array (
                'id' => 233,
                'key' => 'ethnic',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-13 09:42:31',
                'updated_at' => '2020-05-13 09:42:31',
            ),
            229 => 
            array (
                'id' => 234,
                'key' => 'ethnic',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-13 09:42:31',
                'updated_at' => '2020-05-13 09:42:31',
            ),
            230 => 
            array (
                'id' => 235,
                'key' => 'religion',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-13 09:42:55',
                'updated_at' => '2020-05-13 09:42:55',
            ),
            231 => 
            array (
                'id' => 236,
                'key' => 'religion',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-13 09:42:55',
                'updated_at' => '2020-05-13 09:42:55',
            ),
            232 => 
            array (
                'id' => 237,
                'key' => 'warnings',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-13 09:43:36',
                'updated_at' => '2020-05-13 09:43:36',
            ),
            233 => 
            array (
                'id' => 238,
                'key' => 'warnings',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-13 09:43:36',
                'updated_at' => '2020-05-13 09:43:36',
            ),
            234 => 
            array (
                'id' => 239,
                'key' => 'folder',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-14 06:02:19',
                'updated_at' => '2020-05-14 06:02:19',
            ),
            235 => 
            array (
                'id' => 240,
                'key' => 'folder',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-14 06:02:19',
                'updated_at' => '2020-05-14 06:02:19',
            ),
            236 => 
            array (
                'id' => 241,
                'key' => 'file',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-14 06:03:02',
                'updated_at' => '2020-05-14 06:03:02',
            ),
            237 => 
            array (
                'id' => 242,
                'key' => 'file',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-14 06:03:02',
                'updated_at' => '2020-05-14 06:03:02',
            ),
            238 => 
            array (
                'id' => 243,
                'key' => 'file_count',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-14 06:04:03',
                'updated_at' => '2020-05-14 06:04:03',
            ),
            239 => 
            array (
                'id' => 244,
                'key' => 'file_count',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-14 06:04:03',
                'updated_at' => '2020-05-14 06:04:03',
            ),
            240 => 
            array (
                'id' => 245,
                'key' => 'folder_name',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-14 06:06:09',
                'updated_at' => '2020-05-14 06:06:09',
            ),
            241 => 
            array (
                'id' => 246,
                'key' => 'folder_name',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-14 06:06:09',
                'updated_at' => '2020-05-14 06:06:09',
            ),
            242 => 
            array (
                'id' => 247,
                'key' => 'location_group',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-14 06:10:29',
                'updated_at' => '2020-05-14 06:10:29',
            ),
            243 => 
            array (
                'id' => 248,
                'key' => 'location_group',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-14 06:10:29',
                'updated_at' => '2020-05-14 06:10:29',
            ),
            244 => 
            array (
                'id' => 249,
                'key' => 'location',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-14 06:10:53',
                'updated_at' => '2020-05-14 06:10:53',
            ),
            245 => 
            array (
                'id' => 250,
                'key' => 'location',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-14 06:10:53',
                'updated_at' => '2020-05-14 06:10:53',
            ),
            246 => 
            array (
                'id' => 251,
                'key' => 'location_count',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-14 06:11:49',
                'updated_at' => '2020-05-14 06:11:49',
            ),
            247 => 
            array (
                'id' => 252,
                'key' => 'location_count',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-14 06:11:49',
                'updated_at' => '2020-05-14 06:11:49',
            ),
            248 => 
            array (
                'id' => 253,
                'key' => 'house_name',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-14 06:19:50',
                'updated_at' => '2020-05-14 06:19:50',
            ),
            249 => 
            array (
                'id' => 254,
                'key' => 'house_name',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-14 06:19:50',
                'updated_at' => '2020-05-14 06:19:50',
            ),
            250 => 
            array (
                'id' => 255,
                'key' => 'relationship',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-14 07:39:50',
                'updated_at' => '2020-05-14 07:39:50',
            ),
            251 => 
            array (
                'id' => 256,
                'key' => 'relationship',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-14 07:39:50',
                'updated_at' => '2020-05-14 07:39:50',
            ),
            252 => 
            array (
                'id' => 257,
                'key' => 'user_name',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-14 07:54:42',
                'updated_at' => '2020-05-14 07:54:42',
            ),
            253 => 
            array (
                'id' => 258,
                'key' => 'user_name',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-14 07:54:42',
                'updated_at' => '2020-05-14 07:54:42',
            ),
            254 => 
            array (
                'id' => 259,
                'key' => 'subject',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-14 08:04:06',
                'updated_at' => '2020-05-14 08:04:06',
            ),
            255 => 
            array (
                'id' => 260,
                'key' => 'subject',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-14 08:04:07',
                'updated_at' => '2020-05-14 08:04:07',
            ),
            256 => 
            array (
                'id' => 261,
                'key' => 'expiry',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-14 08:27:59',
                'updated_at' => '2020-05-14 08:27:59',
            ),
            257 => 
            array (
                'id' => 262,
                'key' => 'expiry',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-14 08:27:59',
                'updated_at' => '2020-05-14 08:27:59',
            ),
            258 => 
            array (
                'id' => 263,
                'key' => 'active',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-14 08:28:19',
                'updated_at' => '2020-05-14 08:28:19',
            ),
            259 => 
            array (
                'id' => 264,
                'key' => 'active',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-14 08:28:19',
                'updated_at' => '2020-05-14 08:28:19',
            ),
            260 => 
            array (
                'id' => 265,
                'key' => 'doc_type',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-14 08:32:19',
                'updated_at' => '2020-05-14 08:32:19',
            ),
            261 => 
            array (
                'id' => 266,
                'key' => 'doc_type',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-14 08:32:19',
                'updated_at' => '2020-05-14 08:32:19',
            ),
            262 => 
            array (
                'id' => 267,
                'key' => 'issue_date',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-14 08:32:54',
                'updated_at' => '2020-05-14 08:32:54',
            ),
            263 => 
            array (
                'id' => 268,
                'key' => 'issue_date',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-14 08:32:54',
                'updated_at' => '2020-05-14 08:32:54',
            ),
            264 => 
            array (
                'id' => 269,
                'key' => 'view',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-14 08:41:14',
                'updated_at' => '2020-05-14 08:41:14',
            ),
            265 => 
            array (
                'id' => 270,
                'key' => 'view',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-14 08:41:14',
                'updated_at' => '2020-05-14 08:41:14',
            ),
            266 => 
            array (
                'id' => 271,
                'key' => 'date',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-14 08:46:54',
                'updated_at' => '2020-05-14 08:46:54',
            ),
            267 => 
            array (
                'id' => 272,
                'key' => 'date',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-14 08:46:54',
                'updated_at' => '2020-05-14 08:46:54',
            ),
            268 => 
            array (
                'id' => 273,
                'key' => 'event',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-14 08:47:08',
                'updated_at' => '2020-05-14 08:47:08',
            ),
            269 => 
            array (
                'id' => 274,
                'key' => 'event',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-14 08:47:08',
                'updated_at' => '2020-05-14 08:47:08',
            ),
            270 => 
            array (
                'id' => 275,
                'key' => 'user_agent',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-14 08:47:31',
                'updated_at' => '2020-05-14 08:47:31',
            ),
            271 => 
            array (
                'id' => 276,
                'key' => 'user_agent',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-14 08:47:31',
                'updated_at' => '2020-05-14 08:47:31',
            ),
            272 => 
            array (
                'id' => 277,
                'key' => 'url',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-14 08:47:46',
                'updated_at' => '2020-05-14 08:47:46',
            ),
            273 => 
            array (
                'id' => 278,
                'key' => 'url',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-14 08:47:46',
                'updated_at' => '2020-05-14 08:47:46',
            ),
            274 => 
            array (
                'id' => 279,
                'key' => 'ip_address',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-14 08:48:01',
                'updated_at' => '2020-05-14 08:48:01',
            ),
            275 => 
            array (
                'id' => 280,
                'key' => 'ip_address',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-14 08:48:01',
                'updated_at' => '2020-05-14 08:48:01',
            ),
            276 => 
            array (
                'id' => 281,
                'key' => 'new_values',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-14 08:48:13',
                'updated_at' => '2020-05-14 08:48:13',
            ),
            277 => 
            array (
                'id' => 282,
                'key' => 'new_values',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-14 08:48:13',
                'updated_at' => '2020-05-14 08:48:13',
            ),
            278 => 
            array (
                'id' => 283,
                'key' => 'old_values',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-14 08:48:25',
                'updated_at' => '2020-05-14 08:48:25',
            ),
            279 => 
            array (
                'id' => 284,
                'key' => 'old_values',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-14 08:48:25',
                'updated_at' => '2020-05-14 08:48:25',
            ),
            280 => 
            array (
                'id' => 285,
                'key' => 'module',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-14 09:25:58',
                'updated_at' => '2020-05-14 09:25:58',
            ),
            281 => 
            array (
                'id' => 286,
                'key' => 'module',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-14 09:25:58',
                'updated_at' => '2020-05-14 09:25:58',
            ),
            282 => 
            array (
                'id' => 287,
                'key' => 'upload',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-15 06:36:01',
                'updated_at' => '2020-05-15 06:36:01',
            ),
            283 => 
            array (
                'id' => 288,
                'key' => 'upload',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-15 06:36:01',
                'updated_at' => '2020-05-15 06:36:01',
            ),
            284 => 
            array (
                'id' => 289,
                'key' => 'max_visit',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-15 07:22:41',
                'updated_at' => '2020-05-15 07:22:41',
            ),
            285 => 
            array (
                'id' => 290,
                'key' => 'max_visit',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-15 07:22:41',
                'updated_at' => '2020-05-15 07:22:41',
            ),
            286 => 
            array (
                'id' => 291,
                'key' => 'scoring',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-15 07:24:40',
                'updated_at' => '2020-05-15 07:24:40',
            ),
            287 => 
            array (
                'id' => 292,
                'key' => 'scoring',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-15 07:24:40',
                'updated_at' => '2020-05-15 07:24:40',
            ),
            288 => 
            array (
                'id' => 293,
                'key' => 'answer_group',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-15 08:56:14',
                'updated_at' => '2020-05-15 08:56:14',
            ),
            289 => 
            array (
                'id' => 294,
                'key' => 'answer_group',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-15 08:56:14',
                'updated_at' => '2020-05-15 08:56:14',
            ),
            290 => 
            array (
                'id' => 295,
                'key' => 'answer',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-15 09:00:58',
                'updated_at' => '2020-05-15 09:00:58',
            ),
            291 => 
            array (
                'id' => 296,
                'key' => 'answer',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-15 09:00:58',
                'updated_at' => '2020-05-15 09:00:58',
            ),
            292 => 
            array (
                'id' => 297,
                'key' => 'sig_select',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-15 09:01:58',
                'updated_at' => '2020-05-15 09:01:58',
            ),
            293 => 
            array (
                'id' => 298,
                'key' => 'sig_select',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-15 09:01:58',
                'updated_at' => '2020-05-15 09:01:58',
            ),
            294 => 
            array (
                'id' => 299,
                'key' => 'grade',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-15 09:07:48',
                'updated_at' => '2020-05-15 09:07:48',
            ),
            295 => 
            array (
                'id' => 300,
                'key' => 'grade',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-15 09:07:48',
                'updated_at' => '2020-05-15 09:07:48',
            ),
            296 => 
            array (
                'id' => 301,
                'key' => 'show_dashboard',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-15 09:23:06',
                'updated_at' => '2020-05-15 09:23:06',
            ),
            297 => 
            array (
                'id' => 302,
                'key' => 'show_dashboard',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-15 09:23:06',
                'updated_at' => '2020-05-15 09:23:06',
            ),
            298 => 
            array (
                'id' => 303,
                'key' => 'detailed_field_stats',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-15 09:23:57',
                'updated_at' => '2020-05-15 09:23:57',
            ),
            299 => 
            array (
                'id' => 304,
                'key' => 'detailed_field_stats',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-15 09:23:57',
                'updated_at' => '2020-05-15 09:23:57',
            ),
            300 => 
            array (
                'id' => 305,
                'key' => 'late_completion_count',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-15 09:24:09',
                'updated_at' => '2020-05-15 09:24:09',
            ),
            301 => 
            array (
                'id' => 306,
                'key' => 'late_completion_count',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-15 09:24:09',
                'updated_at' => '2020-05-15 09:24:09',
            ),
            302 => 
            array (
                'id' => 307,
                'key' => 'outstanding_count',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-15 09:24:20',
                'updated_at' => '2020-05-15 09:24:20',
            ),
            303 => 
            array (
                'id' => 308,
                'key' => 'outstanding_count',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-15 09:24:20',
                'updated_at' => '2020-05-15 09:24:20',
            ),
            304 => 
            array (
                'id' => 309,
                'key' => 'count_response_negative',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-15 09:24:33',
                'updated_at' => '2020-05-15 09:24:33',
            ),
            305 => 
            array (
                'id' => 310,
                'key' => 'count_response_negative',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-15 09:24:33',
                'updated_at' => '2020-05-15 09:24:33',
            ),
            306 => 
            array (
                'id' => 311,
                'key' => 'count_response_positive',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-15 09:24:49',
                'updated_at' => '2020-05-15 09:24:49',
            ),
            307 => 
            array (
                'id' => 312,
                'key' => 'count_response_positive',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-15 09:24:49',
                'updated_at' => '2020-05-15 09:24:49',
            ),
            308 => 
            array (
                'id' => 313,
                'key' => 'count_day',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-15 09:25:00',
                'updated_at' => '2020-05-15 09:25:00',
            ),
            309 => 
            array (
                'id' => 314,
                'key' => 'count_day',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-15 09:25:00',
                'updated_at' => '2020-05-15 09:25:00',
            ),
            310 => 
            array (
                'id' => 315,
                'key' => 'count_dashboard',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-15 09:25:13',
                'updated_at' => '2020-05-15 09:25:13',
            ),
            311 => 
            array (
                'id' => 316,
                'key' => 'count_dashboard',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-15 09:25:13',
                'updated_at' => '2020-05-15 09:25:13',
            ),
            312 => 
            array (
                'id' => 317,
                'key' => 'doc_reference',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 05:51:04',
                'updated_at' => '2020-05-18 05:51:04',
            ),
            313 => 
            array (
                'id' => 318,
                'key' => 'doc_reference',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 05:51:04',
                'updated_at' => '2020-05-18 05:51:04',
            ),
            314 => 
            array (
                'id' => 319,
                'key' => 'delete',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 06:30:32',
                'updated_at' => '2020-05-18 06:30:32',
            ),
            315 => 
            array (
                'id' => 320,
                'key' => 'delete',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 06:30:32',
                'updated_at' => '2020-05-18 06:30:32',
            ),
            316 => 
            array (
                'id' => 321,
                'key' => 'form_type',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 06:34:59',
                'updated_at' => '2020-05-18 06:34:59',
            ),
            317 => 
            array (
                'id' => 322,
                'key' => 'form_type',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 06:34:59',
                'updated_at' => '2020-05-18 06:34:59',
            ),
            318 => 
            array (
                'id' => 323,
                'key' => 'common_questions',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 06:41:35',
                'updated_at' => '2020-05-18 06:41:35',
            ),
            319 => 
            array (
                'id' => 324,
                'key' => 'common_questions',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 06:41:35',
                'updated_at' => '2020-05-18 06:41:35',
            ),
            320 => 
            array (
                'id' => 325,
                'key' => 'location_type',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 06:43:12',
                'updated_at' => '2020-05-18 06:43:12',
            ),
            321 => 
            array (
                'id' => 326,
                'key' => 'location_type',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 06:43:12',
                'updated_at' => '2020-05-18 06:43:12',
            ),
            322 => 
            array (
                'id' => 327,
                'key' => 'form_settings',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 06:44:22',
                'updated_at' => '2020-05-18 06:44:22',
            ),
            323 => 
            array (
                'id' => 328,
                'key' => 'form_settings',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 06:44:22',
                'updated_at' => '2020-05-18 06:44:22',
            ),
            324 => 
            array (
                'id' => 329,
                'key' => 'pre_populate',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 06:45:41',
                'updated_at' => '2020-05-18 06:45:41',
            ),
            325 => 
            array (
                'id' => 330,
                'key' => 'pre_populate',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 06:45:41',
                'updated_at' => '2020-05-18 06:45:41',
            ),
            326 => 
            array (
                'id' => 331,
                'key' => 'show_form_onapp',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 06:49:05',
                'updated_at' => '2020-05-18 06:49:05',
            ),
            327 => 
            array (
                'id' => 332,
                'key' => 'show_form_onapp',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 06:49:05',
                'updated_at' => '2020-05-18 06:49:05',
            ),
            328 => 
            array (
                'id' => 333,
                'key' => 'import_jobs',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 06:51:10',
                'updated_at' => '2020-05-18 06:51:10',
            ),
            329 => 
            array (
                'id' => 334,
                'key' => 'import_jobs',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 06:51:10',
                'updated_at' => '2020-05-18 06:51:10',
            ),
            330 => 
            array (
                'id' => 335,
                'key' => 'reason_for_delete',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 06:52:27',
                'updated_at' => '2020-05-18 06:52:27',
            ),
            331 => 
            array (
                'id' => 336,
                'key' => 'reason_for_delete',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 06:52:27',
                'updated_at' => '2020-05-18 06:52:27',
            ),
            332 => 
            array (
                'id' => 337,
                'key' => 'require_sign_off',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 06:54:13',
                'updated_at' => '2020-05-18 06:54:13',
            ),
            333 => 
            array (
                'id' => 338,
                'key' => 'require_sign_off',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 06:54:13',
                'updated_at' => '2020-05-18 06:54:13',
            ),
            334 => 
            array (
                'id' => 339,
                'key' => 'download',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 06:56:45',
                'updated_at' => '2020-05-18 06:56:45',
            ),
            335 => 
            array (
                'id' => 340,
                'key' => 'download',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 06:56:45',
                'updated_at' => '2020-05-18 06:56:45',
            ),
            336 => 
            array (
                'id' => 341,
                'key' => 'view_only',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 06:57:41',
                'updated_at' => '2020-05-18 06:57:41',
            ),
            337 => 
            array (
                'id' => 342,
                'key' => 'view_only',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 06:57:41',
                'updated_at' => '2020-05-18 06:57:41',
            ),
            338 => 
            array (
                'id' => 343,
                'key' => 'view_and_download_orignal_file_format',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 06:59:13',
                'updated_at' => '2020-05-18 06:59:13',
            ),
            339 => 
            array (
                'id' => 344,
                'key' => 'view_and_download_orignal_file_format',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 06:59:13',
                'updated_at' => '2020-05-18 06:59:13',
            ),
            340 => 
            array (
                'id' => 345,
                'key' => 'view_and_download_pdf',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 07:01:11',
                'updated_at' => '2020-05-18 07:01:11',
            ),
            341 => 
            array (
                'id' => 346,
                'key' => 'view_and_download_pdf',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 07:01:12',
                'updated_at' => '2020-05-18 07:01:12',
            ),
            342 => 
            array (
                'id' => 347,
                'key' => 'view_and_download_pdf_and_orignal_file_format',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 07:02:38',
                'updated_at' => '2020-05-18 07:02:38',
            ),
            343 => 
            array (
                'id' => 348,
                'key' => 'view_and_download_pdf_and_orignal_file_format',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 07:02:38',
                'updated_at' => '2020-05-18 07:02:38',
            ),
            344 => 
            array (
                'id' => 349,
                'key' => 'lock_job_from',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 07:04:51',
                'updated_at' => '2020-05-18 07:04:51',
            ),
            345 => 
            array (
                'id' => 350,
                'key' => 'lock_job_from',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 07:04:51',
                'updated_at' => '2020-05-18 07:04:51',
            ),
            346 => 
            array (
                'id' => 351,
                'key' => 'lock_job_before',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 07:08:07',
                'updated_at' => '2020-05-18 07:08:07',
            ),
            347 => 
            array (
                'id' => 352,
                'key' => 'lock_job_before',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 07:08:07',
                'updated_at' => '2020-05-18 07:08:07',
            ),
            348 => 
            array (
                'id' => 353,
                'key' => 'valid_from',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 07:09:35',
                'updated_at' => '2020-05-18 07:09:35',
            ),
            349 => 
            array (
                'id' => 354,
                'key' => 'valid_from',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 07:09:35',
                'updated_at' => '2020-05-18 07:09:35',
            ),
            350 => 
            array (
                'id' => 355,
                'key' => 'valid_to',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 07:10:35',
                'updated_at' => '2020-05-18 07:10:35',
            ),
            351 => 
            array (
                'id' => 356,
                'key' => 'valid_to',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 07:10:35',
                'updated_at' => '2020-05-18 07:10:35',
            ),
            352 => 
            array (
                'id' => 357,
                'key' => 'dynamic_forms',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 07:23:01',
                'updated_at' => '2020-05-18 07:23:01',
            ),
            353 => 
            array (
                'id' => 358,
                'key' => 'dynamic_forms',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 07:23:01',
                'updated_at' => '2020-05-18 07:23:01',
            ),
            354 => 
            array (
                'id' => 359,
                'key' => 'schedule',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 07:25:03',
                'updated_at' => '2020-05-18 07:25:03',
            ),
            355 => 
            array (
                'id' => 360,
                'key' => 'schedule',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 07:25:03',
                'updated_at' => '2020-05-18 07:25:03',
            ),
            356 => 
            array (
                'id' => 361,
                'key' => 'version_log',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 07:26:08',
                'updated_at' => '2020-05-18 07:26:08',
            ),
            357 => 
            array (
                'id' => 362,
                'key' => 'version_log',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 07:26:08',
                'updated_at' => '2020-05-18 07:26:08',
            ),
            358 => 
            array (
                'id' => 363,
                'key' => 'bulk_import',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 07:29:30',
                'updated_at' => '2020-05-18 07:29:30',
            ),
            359 => 
            array (
                'id' => 364,
                'key' => 'bulk_import',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 07:29:30',
                'updated_at' => '2020-05-18 07:29:30',
            ),
            360 => 
            array (
                'id' => 365,
                'key' => 'view_register',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 07:30:18',
                'updated_at' => '2020-05-18 07:30:18',
            ),
            361 => 
            array (
                'id' => 366,
                'key' => 'view_register',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 07:30:18',
                'updated_at' => '2020-05-18 07:30:18',
            ),
            362 => 
            array (
                'id' => 367,
                'key' => 'location_name',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 08:23:03',
                'updated_at' => '2020-05-18 08:23:03',
            ),
            363 => 
            array (
                'id' => 368,
                'key' => 'location_name',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 08:23:03',
                'updated_at' => '2020-05-18 08:23:03',
            ),
            364 => 
            array (
                'id' => 369,
                'key' => 'time',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 08:25:00',
                'updated_at' => '2020-05-18 08:25:00',
            ),
            365 => 
            array (
                'id' => 370,
                'key' => 'time',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 08:25:00',
                'updated_at' => '2020-05-18 08:25:00',
            ),
            366 => 
            array (
                'id' => 371,
                'key' => 'task_type',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 08:36:43',
                'updated_at' => '2020-05-18 08:36:43',
            ),
            367 => 
            array (
                'id' => 372,
                'key' => 'task_type',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 08:36:43',
                'updated_at' => '2020-05-18 08:36:43',
            ),
            368 => 
            array (
                'id' => 373,
                'key' => 'project_name',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 08:37:53',
                'updated_at' => '2020-05-18 08:37:53',
            ),
            369 => 
            array (
                'id' => 374,
                'key' => 'project_name',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 08:37:53',
                'updated_at' => '2020-05-18 08:37:53',
            ),
            370 => 
            array (
                'id' => 375,
                'key' => 'qr_list',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 08:41:42',
                'updated_at' => '2020-05-18 08:41:42',
            ),
            371 => 
            array (
                'id' => 376,
                'key' => 'qr_list',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 08:41:42',
                'updated_at' => '2020-05-18 08:41:42',
            ),
            372 => 
            array (
                'id' => 377,
                'key' => 'equipment_list',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 08:42:15',
                'updated_at' => '2020-05-18 08:42:15',
            ),
            373 => 
            array (
                'id' => 378,
                'key' => 'equipment_list',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 08:42:15',
                'updated_at' => '2020-05-18 08:42:15',
            ),
            374 => 
            array (
                'id' => 379,
                'key' => 'pre',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 08:49:13',
                'updated_at' => '2020-05-18 08:49:13',
            ),
            375 => 
            array (
                'id' => 380,
                'key' => 'pre',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 08:49:13',
                'updated_at' => '2020-05-18 08:49:13',
            ),
            376 => 
            array (
                'id' => 381,
                'key' => 'post',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 08:50:16',
                'updated_at' => '2020-05-18 08:50:16',
            ),
            377 => 
            array (
                'id' => 382,
                'key' => 'post',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 08:50:16',
                'updated_at' => '2020-05-18 08:50:16',
            ),
            378 => 
            array (
                'id' => 383,
                'key' => 'recycle_all_jobs_after',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 08:51:12',
                'updated_at' => '2020-05-18 08:51:12',
            ),
            379 => 
            array (
                'id' => 384,
                'key' => 'recycle_all_jobs_after',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 08:51:12',
                'updated_at' => '2020-05-18 08:51:12',
            ),
            380 => 
            array (
                'id' => 385,
                'key' => 'do_not_schedule',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 08:52:13',
                'updated_at' => '2020-05-18 08:52:13',
            ),
            381 => 
            array (
                'id' => 386,
                'key' => 'do_not_schedule',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 08:52:13',
                'updated_at' => '2020-05-18 08:52:13',
            ),
            382 => 
            array (
                'id' => 387,
                'key' => 'frequency',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 08:53:22',
                'updated_at' => '2020-05-18 08:53:22',
            ),
            383 => 
            array (
                'id' => 388,
                'key' => 'frequency',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 08:53:22',
                'updated_at' => '2020-05-18 08:53:22',
            ),
            384 => 
            array (
                'id' => 389,
                'key' => 'mon',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 08:54:32',
                'updated_at' => '2020-05-18 08:54:32',
            ),
            385 => 
            array (
                'id' => 390,
                'key' => 'mon',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 08:54:32',
                'updated_at' => '2020-05-18 08:54:32',
            ),
            386 => 
            array (
                'id' => 391,
                'key' => 'thu',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 08:55:30',
                'updated_at' => '2020-05-18 08:55:30',
            ),
            387 => 
            array (
                'id' => 392,
                'key' => 'thu',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 08:55:30',
                'updated_at' => '2020-05-18 08:55:30',
            ),
            388 => 
            array (
                'id' => 393,
                'key' => 'sun',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 08:56:49',
                'updated_at' => '2020-05-18 08:56:49',
            ),
            389 => 
            array (
                'id' => 394,
                'key' => 'sun',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 08:56:49',
                'updated_at' => '2020-05-18 08:56:49',
            ),
            390 => 
            array (
                'id' => 395,
                'key' => 'tue',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 08:58:06',
                'updated_at' => '2020-05-18 08:58:06',
            ),
            391 => 
            array (
                'id' => 396,
                'key' => 'tue',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 08:58:06',
                'updated_at' => '2020-05-18 08:58:06',
            ),
            392 => 
            array (
                'id' => 397,
                'key' => 'fri',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 08:58:47',
                'updated_at' => '2020-05-18 08:58:47',
            ),
            393 => 
            array (
                'id' => 398,
                'key' => 'fri',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 08:58:47',
                'updated_at' => '2020-05-18 08:58:47',
            ),
            394 => 
            array (
                'id' => 399,
                'key' => 'bank',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 08:59:13',
                'updated_at' => '2020-05-18 08:59:13',
            ),
            395 => 
            array (
                'id' => 400,
                'key' => 'bank',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 08:59:13',
                'updated_at' => '2020-05-18 08:59:13',
            ),
            396 => 
            array (
                'id' => 401,
                'key' => 'wed',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 09:00:23',
                'updated_at' => '2020-05-18 09:00:23',
            ),
            397 => 
            array (
                'id' => 402,
                'key' => 'wed',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 09:00:23',
                'updated_at' => '2020-05-18 09:00:23',
            ),
            398 => 
            array (
                'id' => 403,
                'key' => 'sat',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 09:00:57',
                'updated_at' => '2020-05-18 09:00:57',
            ),
            399 => 
            array (
                'id' => 404,
                'key' => 'sat',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 09:00:57',
                'updated_at' => '2020-05-18 09:00:57',
            ),
            400 => 
            array (
                'id' => 405,
                'key' => 'frequency_limit',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 09:04:01',
                'updated_at' => '2020-05-18 09:04:01',
            ),
            401 => 
            array (
                'id' => 406,
                'key' => 'frequency_limit',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 09:04:01',
                'updated_at' => '2020-05-18 09:04:01',
            ),
            402 => 
            array (
                'id' => 407,
                'key' => 'max_display',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 09:05:05',
                'updated_at' => '2020-05-18 09:05:05',
            ),
            403 => 
            array (
                'id' => 408,
                'key' => 'max_display',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 09:05:05',
                'updated_at' => '2020-05-18 09:05:05',
            ),
            404 => 
            array (
                'id' => 409,
                'key' => 'frequency_repeat',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 09:05:47',
                'updated_at' => '2020-05-18 09:05:47',
            ),
            405 => 
            array (
                'id' => 410,
                'key' => 'frequency_repeat',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 09:05:47',
                'updated_at' => '2020-05-18 09:05:47',
            ),
            406 => 
            array (
                'id' => 411,
                'key' => 'gap_mins',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 09:06:40',
                'updated_at' => '2020-05-18 09:06:40',
            ),
            407 => 
            array (
                'id' => 412,
                'key' => 'gap_mins',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 09:06:40',
                'updated_at' => '2020-05-18 09:06:40',
            ),
            408 => 
            array (
                'id' => 413,
                'key' => 'task_details',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 09:07:24',
                'updated_at' => '2020-05-18 09:07:24',
            ),
            409 => 
            array (
                'id' => 414,
                'key' => 'task_details',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 09:07:24',
                'updated_at' => '2020-05-18 09:07:24',
            ),
            410 => 
            array (
                'id' => 415,
                'key' => 'signature',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 09:09:36',
                'updated_at' => '2020-05-18 09:09:36',
            ),
            411 => 
            array (
                'id' => 416,
                'key' => 'signature',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 09:09:36',
                'updated_at' => '2020-05-18 09:09:36',
            ),
            412 => 
            array (
                'id' => 417,
                'key' => 'tag_swipe_required',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 09:30:34',
                'updated_at' => '2020-05-18 09:30:34',
            ),
            413 => 
            array (
                'id' => 418,
                'key' => 'tag_swipe_required',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 09:30:34',
                'updated_at' => '2020-05-18 09:30:34',
            ),
            414 => 
            array (
                'id' => 419,
                'key' => 'complete_by',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 09:49:59',
                'updated_at' => '2020-05-18 09:49:59',
            ),
            415 => 
            array (
                'id' => 420,
                'key' => 'complete_by',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 09:49:59',
                'updated_at' => '2020-05-18 09:49:59',
            ),
            416 => 
            array (
                'id' => 421,
                'key' => 'date_complete',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 09:50:38',
                'updated_at' => '2020-05-18 09:50:38',
            ),
            417 => 
            array (
                'id' => 422,
                'key' => 'date_complete',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 09:50:38',
                'updated_at' => '2020-05-18 09:50:38',
            ),
            418 => 
            array (
                'id' => 423,
                'key' => 'questions',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 09:51:25',
                'updated_at' => '2020-05-18 09:51:25',
            ),
            419 => 
            array (
                'id' => 424,
                'key' => 'questions',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 09:51:25',
                'updated_at' => '2020-05-18 09:51:25',
            ),
            420 => 
            array (
                'id' => 425,
                'key' => 'static_form',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-18 09:59:46',
                'updated_at' => '2020-05-18 09:59:46',
            ),
            421 => 
            array (
                'id' => 426,
                'key' => 'static_form',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-18 09:59:46',
                'updated_at' => '2020-05-18 09:59:46',
            ),
            422 => 
            array (
                'id' => 427,
                'key' => 'version_type',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-19 05:49:01',
                'updated_at' => '2020-05-19 05:49:01',
            ),
            423 => 
            array (
                'id' => 428,
                'key' => 'version_type',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-19 05:49:01',
                'updated_at' => '2020-05-19 05:49:01',
            ),
            424 => 
            array (
                'id' => 429,
                'key' => 'form_name',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-19 05:50:39',
                'updated_at' => '2020-05-19 05:50:39',
            ),
            425 => 
            array (
                'id' => 430,
                'key' => 'form_name',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-19 05:50:39',
                'updated_at' => '2020-05-19 05:50:39',
            ),
            426 => 
            array (
                'id' => 431,
                'key' => 'download_as_Pdf',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-19 06:14:41',
                'updated_at' => '2020-05-19 06:14:41',
            ),
            427 => 
            array (
                'id' => 432,
                'key' => 'download_as_Pdf',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-19 06:14:41',
                'updated_at' => '2020-05-19 06:14:41',
            ),
            428 => 
            array (
                'id' => 433,
                'key' => 'download_as_orginal',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-19 06:18:29',
                'updated_at' => '2020-05-19 06:18:29',
            ),
            429 => 
            array (
                'id' => 434,
                'key' => 'download_as_orginal',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-19 06:18:29',
                'updated_at' => '2020-05-19 06:18:29',
            ),
            430 => 
            array (
                'id' => 435,
                'key' => 'version_log_view',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-19 06:25:55',
                'updated_at' => '2020-05-19 06:25:55',
            ),
            431 => 
            array (
                'id' => 436,
                'key' => 'version_log_view',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-19 06:25:55',
                'updated_at' => '2020-05-19 06:25:55',
            ),
            432 => 
            array (
                'id' => 437,
                'key' => 'status',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-19 07:02:45',
                'updated_at' => '2020-05-19 07:02:45',
            ),
            433 => 
            array (
                'id' => 438,
                'key' => 'status',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-19 07:02:45',
                'updated_at' => '2020-05-19 07:02:45',
            ),
            434 => 
            array (
                'id' => 439,
                'key' => 'draft',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-19 07:10:23',
                'updated_at' => '2020-05-19 07:10:23',
            ),
            435 => 
            array (
                'id' => 440,
                'key' => 'draft',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-19 07:10:23',
                'updated_at' => '2020-05-19 07:10:23',
            ),
            436 => 
            array (
                'id' => 441,
                'key' => 'shared_question',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-19 07:11:49',
                'updated_at' => '2020-05-19 07:11:49',
            ),
            437 => 
            array (
                'id' => 442,
                'key' => 'shared_question',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-19 07:11:49',
                'updated_at' => '2020-05-19 07:11:49',
            ),
            438 => 
            array (
                'id' => 443,
                'key' => 'new_dynamic_form',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-19 07:46:20',
                'updated_at' => '2020-05-19 07:46:20',
            ),
            439 => 
            array (
                'id' => 444,
                'key' => 'new_dynamic_form',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-19 07:46:20',
                'updated_at' => '2020-05-19 07:46:20',
            ),
            440 => 
            array (
                'id' => 445,
                'key' => 'header',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-19 07:47:13',
                'updated_at' => '2020-05-19 07:47:13',
            ),
            441 => 
            array (
                'id' => 446,
                'key' => 'header',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-19 07:47:13',
                'updated_at' => '2020-05-19 07:47:13',
            ),
            442 => 
            array (
                'id' => 447,
                'key' => 'footer',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-19 07:47:40',
                'updated_at' => '2020-05-19 07:47:40',
            ),
            443 => 
            array (
                'id' => 448,
                'key' => 'footer',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-19 07:47:40',
                'updated_at' => '2020-05-19 07:47:40',
            ),
            444 => 
            array (
                'id' => 449,
                'key' => 'border_style',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-19 07:55:07',
                'updated_at' => '2020-05-19 07:55:07',
            ),
            445 => 
            array (
                'id' => 450,
                'key' => 'border_style',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-19 07:55:07',
                'updated_at' => '2020-05-19 07:55:07',
            ),
            446 => 
            array (
                'id' => 451,
                'key' => 'answer_type',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-19 08:10:42',
                'updated_at' => '2020-05-19 08:10:42',
            ),
            447 => 
            array (
                'id' => 452,
                'key' => 'answer_type',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-19 08:10:42',
                'updated_at' => '2020-05-19 08:10:42',
            ),
            448 => 
            array (
                'id' => 453,
                'key' => 'order',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-19 08:10:55',
                'updated_at' => '2020-05-19 08:10:55',
            ),
            449 => 
            array (
                'id' => 454,
                'key' => 'order',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-19 08:10:55',
                'updated_at' => '2020-05-19 08:10:55',
            ),
            450 => 
            array (
                'id' => 455,
                'key' => 'horizontal',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-19 08:11:33',
                'updated_at' => '2020-05-19 08:11:33',
            ),
            451 => 
            array (
                'id' => 456,
                'key' => 'horizontal',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-19 08:11:33',
                'updated_at' => '2020-05-19 08:11:33',
            ),
            452 => 
            array (
                'id' => 457,
                'key' => 'vertical',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-19 08:12:06',
                'updated_at' => '2020-05-19 08:12:06',
            ),
            453 => 
            array (
                'id' => 458,
                'key' => 'vertical',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-19 08:12:06',
                'updated_at' => '2020-05-19 08:12:06',
            ),
            454 => 
            array (
                'id' => 459,
                'key' => 'new',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-19 08:21:16',
                'updated_at' => '2020-05-19 08:21:16',
            ),
            455 => 
            array (
                'id' => 460,
                'key' => 'new',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-19 08:21:16',
                'updated_at' => '2020-05-19 08:21:16',
            ),
            456 => 
            array (
                'id' => 461,
                'key' => 'ticket_id',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-20 05:58:18',
                'updated_at' => '2020-05-20 05:58:18',
            ),
            457 => 
            array (
                'id' => 462,
                'key' => 'ticket_id',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-20 05:58:18',
                'updated_at' => '2020-05-20 05:58:18',
            ),
            458 => 
            array (
                'id' => 463,
                'key' => 'message',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-20 05:59:09',
                'updated_at' => '2020-05-20 05:59:09',
            ),
            459 => 
            array (
                'id' => 464,
                'key' => 'message',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-20 05:59:09',
                'updated_at' => '2020-05-20 05:59:09',
            ),
            460 => 
            array (
                'id' => 465,
                'key' => 'send_date',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-20 05:59:56',
                'updated_at' => '2020-05-20 05:59:56',
            ),
            461 => 
            array (
                'id' => 466,
                'key' => 'send_date',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-20 05:59:56',
                'updated_at' => '2020-05-20 05:59:56',
            ),
            462 => 
            array (
                'id' => 467,
                'key' => 'drop_us_a_message',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-20 07:21:04',
                'updated_at' => '2020-05-20 07:21:04',
            ),
            463 => 
            array (
                'id' => 468,
                'key' => 'drop_us_a_message',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-20 07:21:04',
                'updated_at' => '2020-05-20 07:21:04',
            ),
            464 => 
            array (
                'id' => 469,
                'key' => 'send',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-20 07:23:20',
                'updated_at' => '2020-05-20 07:23:20',
            ),
            465 => 
            array (
                'id' => 470,
                'key' => 'send',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-20 07:23:20',
                'updated_at' => '2020-05-20 07:23:20',
            ),
            466 => 
            array (
                'id' => 471,
                'key' => 'description',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-20 07:26:43',
                'updated_at' => '2020-05-20 07:26:43',
            ),
            467 => 
            array (
                'id' => 472,
                'key' => 'description',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-20 07:26:43',
                'updated_at' => '2020-05-20 07:26:43',
            ),
            468 => 
            array (
                'id' => 473,
                'key' => 'web_cms',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-20 08:14:55',
                'updated_at' => '2020-05-20 08:14:55',
            ),
            469 => 
            array (
                'id' => 474,
                'key' => 'web_cms',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-20 08:14:55',
                'updated_at' => '2020-05-20 08:14:55',
            ),
            470 => 
            array (
                'id' => 475,
                'key' => 'summary',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-05-22 09:24:45',
                'updated_at' => '2020-05-22 09:24:45',
            ),
            471 => 
            array (
                'id' => 476,
                'key' => 'summary',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-05-22 09:24:45',
                'updated_at' => '2020-05-22 09:24:45',
            ),
            472 => 
            array (
                'id' => 477,
                'key' => 'show_header',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-06-02 10:10:35',
                'updated_at' => '2020-06-02 10:10:35',
            ),
            473 => 
            array (
                'id' => 478,
                'key' => 'show_header',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-06-02 10:10:35',
                'updated_at' => '2020-06-02 10:10:35',
            ),
            474 => 
            array (
                'id' => 479,
                'key' => 'header_height',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-06-02 10:11:35',
                'updated_at' => '2020-06-02 10:11:35',
            ),
            475 => 
            array (
                'id' => 480,
                'key' => 'header_height',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-06-02 10:11:35',
                'updated_at' => '2020-06-02 10:11:35',
            ),
            476 => 
            array (
                'id' => 491,
                'key' => 'show_footer',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-06-05 10:01:37',
                'updated_at' => '2020-06-05 10:01:37',
            ),
            477 => 
            array (
                'id' => 492,
                'key' => 'show_footer',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-06-05 10:01:37',
                'updated_at' => '2020-06-05 10:01:37',
            ),
            478 => 
            array (
                'id' => 493,
                'key' => 'footer_height',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-06-05 10:03:00',
                'updated_at' => '2020-06-05 10:03:00',
            ),
            479 => 
            array (
                'id' => 494,
                'key' => 'footer_height',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-06-05 10:03:00',
                'updated_at' => '2020-06-05 10:03:00',
            ),
            480 => 
            array (
                'id' => 495,
                'key' => 'is_repeatable',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-06-05 10:03:26',
                'updated_at' => '2020-06-05 10:03:26',
            ),
            481 => 
            array (
                'id' => 496,
                'key' => 'is_repeatable',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-06-05 10:03:26',
                'updated_at' => '2020-06-05 10:03:26',
            ),
            482 => 
            array (
                'id' => 497,
                'key' => 'location_break_down',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-06-05 02:43:12',
                'updated_at' => '2020-06-05 02:43:12',
            ),
            483 => 
            array (
                'id' => 498,
                'key' => 'location_break_down',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-06-05 02:43:12',
                'updated_at' => '2020-06-05 02:43:12',
            ),
            484 => 
            array (
                'id' => 499,
                'key' => 'date_time',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-06-05 02:45:04',
                'updated_at' => '2020-06-05 02:45:04',
            ),
            485 => 
            array (
                'id' => 500,
                'key' => 'date_time',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-06-05 02:45:04',
                'updated_at' => '2020-06-05 02:45:04',
            ),
            486 => 
            array (
                'id' => 501,
                'key' => 'show_signed_forms',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-06-05 06:56:29',
                'updated_at' => '2020-06-05 06:56:29',
            ),
            487 => 
            array (
                'id' => 502,
                'key' => 'show_signed_forms',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-06-05 06:56:29',
                'updated_at' => '2020-06-05 06:56:29',
            ),
            488 => 
            array (
                'id' => 503,
                'key' => 'show_unsigned_forms',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-06-05 06:57:16',
                'updated_at' => '2020-06-05 06:57:16',
            ),
            489 => 
            array (
                'id' => 504,
                'key' => 'show_unsigned_forms',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-06-05 06:57:16',
                'updated_at' => '2020-06-05 06:57:16',
            ),
            490 => 
            array (
                'id' => 505,
                'key' => 'answer_Score',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-06-05 06:59:29',
                'updated_at' => '2020-06-05 06:59:29',
            ),
            491 => 
            array (
                'id' => 506,
                'key' => 'answer_Score',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-06-05 06:59:29',
                'updated_at' => '2020-06-05 06:59:29',
            ),
            492 => 
            array (
                'id' => 507,
                'key' => 'form_type_score',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-06-05 07:00:17',
                'updated_at' => '2020-06-05 07:00:17',
            ),
            493 => 
            array (
                'id' => 508,
                'key' => 'form_type_score',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-06-05 07:00:17',
                'updated_at' => '2020-06-05 07:00:17',
            ),
            494 => 
            array (
                'id' => 509,
                'key' => 'form_score',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-06-05 07:01:06',
                'updated_at' => '2020-06-05 07:01:06',
            ),
            495 => 
            array (
                'id' => 510,
                'key' => 'form_score',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-06-05 07:01:06',
                'updated_at' => '2020-06-05 07:01:06',
            ),
            496 => 
            array (
                'id' => 511,
                'key' => 'sign_off_header',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-06-08 04:28:53',
                'updated_at' => '2020-06-08 04:28:53',
            ),
            497 => 
            array (
                'id' => 512,
                'key' => 'sign_off_header',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-06-08 04:28:53',
                'updated_at' => '2020-06-08 04:28:53',
            ),
            498 => 
            array (
                'id' => 513,
                'key' => 'comment',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-06-08 04:32:49',
                'updated_at' => '2020-06-08 04:32:49',
            ),
            499 => 
            array (
                'id' => 514,
                'key' => 'comment',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-06-08 04:32:49',
                'updated_at' => '2020-06-08 04:32:49',
            ),
        ));
        \DB::table('labels')->insert(array (
            0 => 
            array (
                'id' => 515,
                'key' => 'report_label',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-06-08 04:34:06',
                'updated_at' => '2020-06-08 04:34:06',
            ),
            1 => 
            array (
                'id' => 516,
                'key' => 'report_label',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-06-08 04:34:06',
                'updated_at' => '2020-06-08 04:34:06',
            ),
            2 => 
            array (
                'id' => 517,
                'key' => 'report_label_yes',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-06-08 04:34:58',
                'updated_at' => '2020-06-08 04:34:58',
            ),
            3 => 
            array (
                'id' => 518,
                'key' => 'report_label_yes',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-06-08 04:34:58',
                'updated_at' => '2020-06-08 04:34:58',
            ),
            4 => 
            array (
                'id' => 519,
                'key' => 'report_label_no',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-06-08 04:35:09',
                'updated_at' => '2020-06-08 04:35:09',
            ),
            5 => 
            array (
                'id' => 520,
                'key' => 'report_label_no',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-06-08 04:35:09',
                'updated_at' => '2020-06-08 04:35:09',
            ),
            6 => 
            array (
                'id' => 521,
                'key' => 'assign_to',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-06-08 04:43:25',
                'updated_at' => '2020-06-08 04:43:25',
            ),
            7 => 
            array (
                'id' => 522,
                'key' => 'assign_to',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-06-08 04:43:25',
                'updated_at' => '2020-06-08 04:43:25',
            ),
            8 => 
            array (
                'id' => 523,
                'key' => 'time_select',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-06-08 04:46:56',
                'updated_at' => '2020-06-08 04:46:56',
            ),
            9 => 
            array (
                'id' => 524,
                'key' => 'time_select',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-06-08 04:46:56',
                'updated_at' => '2020-06-08 04:46:56',
            ),
            10 => 
            array (
                'id' => 525,
                'key' => 'clear_up_notices',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-06-08 04:49:10',
                'updated_at' => '2020-06-08 04:49:10',
            ),
            11 => 
            array (
                'id' => 526,
                'key' => 'clear_up_notices',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-06-08 04:49:10',
                'updated_at' => '2020-06-08 04:49:10',
            ),
            12 => 
            array (
                'id' => 527,
                'key' => 'populate_check',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-06-08 04:52:17',
                'updated_at' => '2020-06-08 04:52:17',
            ),
            13 => 
            array (
                'id' => 528,
                'key' => 'populate_check',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-06-08 04:52:17',
                'updated_at' => '2020-06-08 04:52:17',
            ),
            14 => 
            array (
                'id' => 529,
                'key' => 'bill_link',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-06-08 04:53:36',
                'updated_at' => '2020-06-08 04:53:36',
            ),
            15 => 
            array (
                'id' => 530,
                'key' => 'bill_link',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-06-08 04:53:36',
                'updated_at' => '2020-06-08 04:53:36',
            ),
            16 => 
            array (
                'id' => 531,
                'key' => 'total_cost',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-06-08 04:54:17',
                'updated_at' => '2020-06-08 04:54:17',
            ),
            17 => 
            array (
                'id' => 532,
                'key' => 'total_cost',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-06-08 04:54:17',
                'updated_at' => '2020-06-08 04:54:17',
            ),
            18 => 
            array (
                'id' => 533,
                'key' => 'bill_info',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-06-08 04:54:58',
                'updated_at' => '2020-06-08 04:54:58',
            ),
            19 => 
            array (
                'id' => 534,
                'key' => 'bill_info',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-06-08 04:54:58',
                'updated_at' => '2020-06-08 04:54:58',
            ),
            20 => 
            array (
                'id' => 535,
                'key' => 'copyright',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-06-11 05:37:35',
                'updated_at' => '2020-06-11 05:37:35',
            ),
            21 => 
            array (
                'id' => 536,
                'key' => 'copyright',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-06-11 05:37:35',
                'updated_at' => '2020-06-11 05:37:35',
            ),
            22 => 
            array (
                'id' => 537,
                'key' => 'patroltec',
                'lang' => 'en',
                'language_id' => 1,
                'created_at' => '2020-06-11 05:38:30',
                'updated_at' => '2020-06-11 05:38:30',
            ),
            23 => 
            array (
                'id' => 538,
                'key' => 'patroltec',
                'lang' => 'fr',
                'language_id' => 2,
                'created_at' => '2020-06-11 05:38:30',
                'updated_at' => '2020-06-11 05:38:30',
            ),
        ));
        
        
    }
}