<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
class DocumentFormTypePermisionTableDataSeeder extends Seeder
{
    public function run(){
        $permission_array = [
            // create project permissions
            ['name' => 'view_form_type','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'view','type'=>'','sub_module_name'=>'operation','module_name'=>'Form Type'],
            ['name' => 'create_form_type','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'create','type'=>'','sub_module_name'=>'operation','module_name'=>'Form Type'],
            ['name' => 'edit_form_type', 'group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'edit','type'=>'','sub_module_name'=>'operation','module_name'=>'Form Type'],
            ['name' => 'delete_form_type','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Form Type'],
            ['name' => 'delete_selected_form_type','group'=>'Document Master', 'guard_name' => 'web','permission_name'=>'Selected Delete','type'=>'','sub_module_name'=>'operation','module_name'=>'Form Type'],
            ['name' => 'selected_active_form_type', 'group'=>'Document Master','guard_name' => 'web','type'=>'','permission_name'=>'Selected Active Multiple','sub_module_name'=>'operation','module_name'=>'Form Type'],
            ['name' => 'selected_restore_form_type', 'group'=>'Document Master','guard_name' => 'web','type'=>'','permission_name'=>'Selected Restore','sub_module_name'=>'operation','module_name'=>'Form Type'],
            ['name' => 'restore_delete_form_type','group'=>'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Restore','sub_module_name'=>'operation','module_name'=>'Form Type'],
            ['name' => 'hard_delete_form_type','group'=>'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Hard Delete','sub_module_name'=>'operation','module_name'=>'Form Type'],
            ['name' => 'active_form_type','group'=>'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Active','sub_module_name'=>'operation','module_name'=>'Form Type'],
            ['name' => 'select_active_form_type','group'=>'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Select Active','sub_module_name'=>'operation','module_name'=>'Form Type'],
            ['name' => 'word_form_type', 'group'=>'Document Master','guard_name' => 'web','type'=>'','permission_name'=>'Word Export','sub_module_name'=>'operation','module_name'=>'Form Type'],
            ['name' => 'csv_form_type','group'=>'Document Master', 'guard_name' => 'web','type'=>'','permission_name'=>'Csv Export','sub_module_name'=>'operation','module_name'=>'Form Type'],
            ['name' => 'pdf_form_type', 'group'=>'Document Master','guard_name' => 'web','type'=>'','permission_name'=>'PDF Export','sub_module_name'=>'operation','module_name'=>'Form Type'],

            //listing in Form Type
            ['name' => 'form_type_checkbox', 'group'=>'Document Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Checkbox','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_name', 'group'=>'Document Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_show_dashboard', 'group'=>'Document Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Show Dashboard','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_count_dashboard','group'=>'Document Master', 'guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Count Dashboard','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_count_day', 'group'=>'Document Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Count Day','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_count_response_positive', 'group'=>'Document Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Count Response Positive','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_count_response_negative', 'group'=>'Document Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Count Response Negative','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_outanding_count', 'group'=>'Document Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Outanding Count','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_late_completion_count', 'group'=>'Document Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Late Completion Count','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_detailed_field_stats', 'group'=>'Document Master','guard_name' => 'web','type'=>'view','enable'=>'1','permission_name'=>'Detailed Field Stats','sub_module_name'=>'listing','module_name'=>'Form Type'],
            

            //word in Form Type
            ['name' => 'form_type_checkbox_word_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'word_export','enable'=>'0','permission_name'=>'Checkbox','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_name_word_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_show_dashboard_word_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Show Dashboard','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_count_dashboard_word_export','group'=>'Document Master', 'guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Count Dashboard','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_count_day_word_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Count Day','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_count_response_positive_word_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Count Response Positive','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_count_response_negative_word_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Count Response Negative','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_outanding_count_word_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Outanding Count','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_late_completion_count_word_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Late Completion Count','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_detailed_field_stats_word_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'word_export','enable'=>'1','permission_name'=>'Detailed Field Stats','sub_module_name'=>'listing','module_name'=>'Form Type'],

            //excel in Form Type
            ['name' => 'form_type_checkbox_excel_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'excel_export','enable'=>'0','permission_name'=>'Checkbox','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_name_excel_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_show_dashboard_excel_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Show Dashboard','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_count_dashboard_excel_export','group'=>'Document Master', 'guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Count Dashboard','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_count_day_excel_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Count Day','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_count_response_positive_excel_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Count Response Positive','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_count_response_negative_excel_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Count Response Negative','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_outanding_count_excel_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Outanding Count','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_late_completion_count_excel_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Late Completion Count','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_detailed_field_stats_excel_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'excel_export','enable'=>'1','permission_name'=>'Detailed Field Stats','sub_module_name'=>'listing','module_name'=>'Form Type'],

            //pdf in Form Type
            ['name' => 'form_type_checkbox_pdf_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'0','permission_name'=>'Checkbox','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_name_pdf_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_show_dashboard_pdf_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Show Dashboard','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_count_dashboard_pdf_export','group'=>'Document Master', 'guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Count Dashboard','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_count_day_pdf_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Count Day','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_count_response_positive_pdf_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Count Response Positive','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_count_response_negative_pdf_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Count Response Negative','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_outanding_count_pdf_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Outanding Count','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_late_completion_count_pdf_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Late Completion Count','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_detailed_field_stats_pdf_export', 'group'=>'Document Master','guard_name' => 'web','type'=>'pdf_export','enable'=>'1','permission_name'=>'Detailed Field Stats','sub_module_name'=>'listing','module_name'=>'Form Type'],


            //create in Form Type
            ['name' => 'form_type_checkbox_create', 'group'=>'Document Master','guard_name' => 'web','type'=>'create','enable'=>'0','permission_name'=>'Checkbox','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_name_create', 'group'=>'Document Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_show_dashboard_create', 'group'=>'Document Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Show Dashboard','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_count_dashboard_create','group'=>'Document Master', 'guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Count Dashboard','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_count_day_create', 'group'=>'Document Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Count Day','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_count_response_positive_create', 'group'=>'Document Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Count Response Positive','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_count_response_negative_create', 'group'=>'Document Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Count Response Negative','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_outanding_count_create', 'group'=>'Document Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Outanding Count','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_late_completion_count_create', 'group'=>'Document Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Late Completion Count','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_detailed_field_stats_create', 'group'=>'Document Master','guard_name' => 'web','type'=>'create','enable'=>'1','permission_name'=>'Detailed Field Stats','sub_module_name'=>'listing','module_name'=>'Form Type'],

            //edit in Form Type
            ['name' => 'form_type_checkbox_edit', 'group'=>'Document Master','guard_name' => 'web','type'=>'edit','enable'=>'0','permission_name'=>'Checkbox','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_name_edit', 'group'=>'Document Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Name','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_show_dashboard_edit', 'group'=>'Document Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Show Dashboard','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_count_dashboard_edit','group'=>'Document Master', 'guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Count Dashboard','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_count_day_edit', 'group'=>'Document Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Count Day','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_count_response_positive_edit', 'group'=>'Document Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Count Response Positive','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_count_response_negative_edit', 'group'=>'Document Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Count Response Negative','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_outanding_count_edit', 'group'=>'Document Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Outanding Count','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_late_completion_count_edit', 'group'=>'Document Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Late Completion Count','sub_module_name'=>'listing','module_name'=>'Form Type'],
            ['name' => 'form_type_detailed_field_stats_edit', 'group'=>'Document Master','guard_name' => 'web','type'=>'edit','enable'=>'1','permission_name'=>'Detailed Field Stats','sub_module_name'=>'listing','module_name'=>'Form Type'],
        ];
        foreach ($permission_array as $key => $permission) {
            $admin_role = Role::find(1);
            $getpermission=Permission::where('name',$permission['name'])->first();
            if (empty($getpermission)){
                $permission=Permission::create($permission);
                $admin_role->givePermissionTo($permission['name']);
            }else{
                $permission=Permission::where('name',$getpermission->name)->update($permission);
                $admin_role->givePermissionTo($getpermission->name);
            }
        }
    }
}
