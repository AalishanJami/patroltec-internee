<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/migrate', function () {
    Artisan::call('migrate');
    Artisan::call('db:seed');
    dd('migrate done');
});
Route::get('/reset-permissions', function () {
    DB::statement('SET FOREIGN_KEY_CHECKS=0;');
    DB::table('permissions')->truncate();
    DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    dd('permission reset');
});
Route::get('/', function () {
    return view('auth.login');
});

Route::get('/user/verify-app/{string}', 'VerifyEmail@index');
Auth::routes();
Route::get('testingPdf', 'HomeController@testingPdf');
Route::get('chart-line-ajax', 'HomeController@chartLineAjax');
Route::get('test/mail', 'backend\JobController@testMail');
Route::get('chart-line-ajax-week', 'HomeController@chartLineAjaxWeek');
Route::get('chart-line-ajax-month', 'HomeController@chartLineAjaxMonth');
Route::get('chart-line-ajax-late', 'HomeController@chartLineAjaxLate');
Route::get('chart-line-ajax-out', 'HomeController@chartLineAjaxOut');
Route::get('chart-line-ajax-lm', 'HomeController@chartLineAjaxLm');
Route::get('chart-line-ajax-day', 'HomeController@chartLineAjaxDay');
Route::get('/home/getAllForms', 'HomeController@getAllForms');
Route::get('/home/getAllFormScore', 'HomeController@getAllFormScore');
Route::get('/home/getAllFormScoreLocation', 'HomeController@getAllFormScoreLocation');
Route::get('/home/getLocationData/{id}', 'HomeController@getLocationData');
Route::group(['middleware' => ['language-trans']], function () {
    Route::group(['middleware' => ['is_login']], function () {
        //CompanyChangeController
        Route::post('/company/logo', 'backend\CompanyLogoController@update');
        Route::post('/change/company', 'backend\CompanyChangeController@change');
        //report
        Route::group(['prefix' => '/report'], function () {
            Route::post('/customSearch', 'backend\ReportController@customSearch');
            Route::post('/qr/customSearch', 'backend\ReportController@customSearchQR');
            Route::get('/qr/list', 'backend\ReportController@qrlist');
            Route::get('/qr/list/getAllQRList', 'backend\ReportController@getAllQRList');
            Route::post('/qr/excel/export', 'backend\ReportController@exportExcelQR');
            Route::post('/qr/pdf/export', 'backend\ReportController@exportPdfQR');
            Route::post('/qr/word/export', 'backend\ReportController@exportWordQR');

            Route::get('plant_and_equipment/list', 'backend\ReportController@plantAndEquipmentList');
            Route::post('/plantAndEquipment/excel/export', 'backend\ReportController@exportExcelplantAndEquipment');
            Route::post('/plantAndEquipment/pdf/export', 'backend\ReportController@exportPdfplantAndEquipment');
            Route::post('/plantAndEquipment/word/export', 'backend\ReportController@exportWordplantAndEquipment');

            Route::get('/getAllplantAndEquipmentList', 'backend\ReportController@getAllplantAndEquipmentList');
        });

        //Project Address
        Route::group(['prefix' => '/address'], function () {
            Route::get('/', 'backend\AddressController@index');
            Route::get('/addresstype/listing', 'backend\AddressController@addressType');
            Route::post('/addressTypeModalSave', 'backend\AddressController@addressTypeModalSave');
            Route::post('/edit', 'backend\AddressController@edit');
            Route::post('/update', 'backend\AddressController@update');
            Route::post('/delete', 'backend\AddressController@delete');
            Route::post('/restore', 'backend\AddressController@restore');
            Route::post('/hardDelete', 'backend\AddressController@hardDelete');
            Route::post('/exportExcel','backend\AddressController@exportExcel');
            Route::post('/exportPdf','backend\AddressController@exportPdf');
            Route::post('/exportWord','backend\AddressController@exportWord');
            Route::post('/multipleDelete', 'backend\AddressController@multipleDelete');
            Route::post('/multipleActive', 'backend\AddressController@multipleActive');
            Route::get('/getAllLocations', 'backend\AddressController@getAllLocations');
            Route::get('/getAllSoftLocations', 'backend\AddressController@getAllSoftLocations');
        });
        //Project Contacts
        //project main
        Route::group(['prefix' => 'contact'], function () {
            Route::get('/', 'backend\ContactController@index');
            Route::get('/contacttype/listing', 'backend\ContactController@contactType');
            Route::post('/contactModalSave', 'backend\ContactController@contactModalSave');
            Route::post('/edit', 'backend\ContactController@edit');
            Route::post('/update', 'backend\ContactController@update');
            Route::post('/delete', 'backend\ContactController@delete');
            Route::post('/restore', 'backend\ContactController@restore');
            Route::post('/hardDelete', 'backend\ContactController@hardDelete');
            Route::post('/multipleDelete', 'backend\ContactController@multipleDelete');
            Route::get('/getalldata', 'backend\ContactController@getAllContacts');
            Route::get('/getAllSoftData', 'backend\ContactController@getAllSoftContacts');
            Route::post('/exportExcel','backend\ContactController@exportExcel');
            Route::post('/exportPdf','backend\ContactController@exportPdf');
            Route::post('/exportWord','backend\ContactController@exportWord');
        });
        //project main
        Route::group(['prefix' => 'project'], function () {
            Route::get('/', 'backend\ProjectController@index');
            Route::any('/getAll', 'backend\ProjectController@getAll');
            Route::get('/form_groupcreateDetail', 'backend\ProjectController@createDetail');
            Route::get('/detail/{id}', 'backend\ProjectController@detail');
            Route::get('/getAllSoft', 'backend\ProjectController@getAllSoft');
            Route::post('/updateDetail', 'backend\ProjectController@updateDetail');
            Route::post('/restore', 'backend\ProjectController@restore');
            Route::any('/active/multiple', 'backend\ProjectController@activeMultiple');
            Route::post('/store', 'backend\ProjectController@store');
            Route::post('/update', 'backend\ProjectController@update');
            Route::post('/delete', 'backend\ProjectController@delete');
            Route::post('/hardDelete', 'backend\ProjectController@hardDelete');
            Route::post('/multipleDelete', 'backend\ProjectController@multipleDelete');
            Route::post('/exportExcel','backend\ProjectController@exportExcel');
            Route::post('/exportPdf','backend\ProjectController@exportPdf');
            Route::post('/exportWorld','backend\ProjectController@exportWorld');
        });

        Route::group(['prefix' => 'dashboard'], function () {
            Route::get('/', 'HomeController@index')->name('dashboard');
            Route::post('/getDataLocationMain', 'HomeController@getDataLocationMain');
        });
        //audit log
        Route::group(['prefix' => 'audit'], function () {
            Route::get('/', 'backend\AuditController@index');
            Route::get('/getall', 'backend\AuditController@getAll');
        });
        //text change
        Route::post('changeText', 'backend\LanguageController@changeText');

        Route::get('/home', 'HomeController@index');
        Route::get('/dashboard', 'HomeController@index')->name('dashboard');
        Route::get('/lily', 'HomeController@lily')->name('lily');
        Route::get('/king', 'HomeController@king')->name('king');
        Route::get('/chelsea', 'HomeController@chelsea')->name('chelsea');
        //user module
        Route::post('user/copy','backend\UserController@copyUser');
        Route::get('/users', 'backend\UserController@index');
        Route::post('/user/delete/multiple', 'backend\UserController@multipleDelete');
        Route::post('/user/delete', 'backend\UserController@delete');
        Route::get('/user/soft', 'backend\UserController@getAllSoftUser');
        Route::post('/user/hard/delete', 'backend\UserController@hardDelete');
        Route::post('/user/restore', 'backend\UserController@restore');
        Route::get('/getalluser/{id?}','backend\UserController@getalluser');
        Route::post('/user/role','backend\UserController@role');
        Route::post('/user/role/update','backend\UserController@roleUpdate');
        Route::get('/user/create/company/list','backend\UserController@companyList');
        Route::post('/user/store', 'backend\UserController@store');
        Route::post('/user', 'backend\UserController@edit');
        Route::post('/user/update', 'backend\UserController@update');
        Route::post('user/update/password', 'backend\UserController@updatePassword');

        //Export Excel
        Route::post('/export_excel_user','backend\UserController@export_excel_user')->name('export_excel_user');
        //Export word
        Route::post('/export_word_user','backend\UserController@exportWordUser');
        // export user
        Route::post('/export_pdf_user','backend\UserController@exportPdfUser');
        //Export Excel
        Route::post('/export_excel_company','backend\CompanyController@export_excel_company')->name('export_excel_company');
        //Export word
        Route::Post('/export_word_company','backend\CompanyController@exportWordCompany');
        Route::Post('/export_pdf_company','backend\CompanyController@exportPdfCompany');
        Route::get('/company', 'backend\CompanyController@index');
        Route::get('/getallcompany','backend\CompanyController@getallcompany');
        Route::post('/company/store', 'backend\CompanyController@store');
        Route::post('/company', 'backend\CompanyController@edit');
        Route::post('/company/update', 'backend\CompanyController@update');
        Route::post('/company/delete', 'backend\CompanyController@delete');
        Route::get('/company/soft', 'backend\CompanyController@getAllSoftCompany');
        Route::post('/company/hard/delete', 'backend\CompanyController@hardDelete');
        Route::post('/company/restore', 'backend\CompanyController@restore');
        Route::post('/company/multipleDelete', 'backend\CompanyController@multipleDelete');
        Route::post('/company/activeMultiple', 'backend\CompanyController@activeMultiple');

        // permission
        Route::get('permissions', 'backend\PermissionController@index');

        // roles
        Route::get('roles', 'backend\RoleController@index');
        Route::post('role/delete', 'backend\RoleController@delete');
        Route::get('/getallroll','backend\RoleController@getallroll');
        Route::get('roles/create', 'backend\RoleController@create');
        Route::post('roles', 'backend\RoleController@store');
        Route::get('/roles/soft', 'backend\RoleController@getAllSoftRoll');
        Route::post('/role/hard/delete', 'backend\RoleController@hardDelete');
        Route::post('/role/restore', 'backend\RoleController@restore');
        Route::post('/role/edit', 'backend\RoleController@edit');
        Route::post('/role/delete/multiple', 'backend\RoleController@multipleDelete');
        Route::get('roles/{id}/edit', 'backend\RoleController@edit');
        Route::post('roles/update/{id}', 'backend\RoleController@update');
        Route::get('roles/destroy/{id}', 'backend\RoleController@destroy');
        Route::get('role/company/{id}', 'backend\RoleController@company');
        Route::post('role/update/ajax', 'backend\RoleController@updateAjax');
        Route::post('role/edit/ajax', 'backend\RoleController@getAjax');


        //App role

        Route::group(['prefix' => 'app'], function () {
            Route::get('roles', 'backend\AppRolesController@index');
            Route::get('roles/getallroll', 'backend\AppRolesController@getallroll');
            Route::post('/roles/edit', 'backend\AppRolesController@edit');
            Route::post('roles/update/ajax', 'backend\AppRolesController@updateAjax');
        });

        //edit login page
        Route::get('login/cms', 'backend\EditLoginController@index');
        Route::get('/getalllogincms', 'backend\EditLoginController@getAllLoginCMS');
        Route::post('login/cms', 'backend\EditLoginController@edit');
        Route::post('/login/cms/update', 'backend\EditLoginController@update');
        Route::post('/login/cms/store', 'backend\EditLoginController@store');
        //bonus score
        Route::group(['prefix' => 'bonus/score'], function () {
            Route::get('/', 'backend\BonusScoreController@index');
            Route::get('/getall', 'backend\BonusScoreController@getAll');
            Route::get('/store', 'backend\BonusScoreController@store');
            Route::post('/update', 'backend\BonusScoreController@update');
            Route::post('/delete', 'backend\BonusScoreController@delete');
            Route::get('/getallSoft', 'backend\BonusScoreController@getAllSoft');
            Route::any('/softdelete', 'backend\BonusScoreController@softDelete');
            Route::any('/active', 'backend\BonusScoreController@active');
            Route::any('/delete/multiple', 'backend\BonusScoreController@deleteMultiple');
            Route::any('/delete/soft/multiple', 'backend\BonusScoreController@deleteMultipleSOft');
            Route::any('/active/multiple', 'backend\BonusScoreController@activeMultiple');
            Route::post('/export/world', 'backend\BonusScoreController@exportWorld');
            Route::post('/export/pdf', 'backend\BonusScoreController@exportPdf');
            Route::post('/export/excel', 'backend\BonusScoreController@exportExcel');
        });
        // equipment
        Route::group(['prefix' => 'equipment'], function () {
            Route::get('/getallSoft', 'backend\EquipmentController@getAllSoft');
            Route::get('/getall', 'backend\EquipmentController@getAll');
            Route::get('/store', 'backend\EquipmentController@store');
            Route::post('/update', 'backend\EquipmentController@update');
            Route::post('/delete', 'backend\EquipmentController@delete');
            Route::any('/softdelete', 'backend\EquipmentController@softDelete');
            Route::any('/delete/multiple', 'backend\EquipmentController@deleteMultiple');
            Route::post('/export/world', 'backend\EquipmentController@exportWorld');
            Route::post('/export/pdf', 'backend\EquipmentController@exportPdf');
            Route::post('/export/excel', 'backend\EquipmentController@exportExcel');
            Route::post('/restore', 'backend\EquipmentController@restore');
            Route::post('/restore/multiple', 'backend\EquipmentController@restoreMultiple');
            Route::any('/delete/soft/multiple', 'backend\EquipmentController@deleteMultipleSOft');
            Route::any('/active/multiple', 'backend\EquipmentController@activeMultiple');

        });
        //bonus score
        Route::group(['prefix' => 'bonus/score'], function () {
            Route::get('/getall', 'backend\BonusScoreController@getAll');
            Route::get('/store', 'backend\BonusScoreController@store');
            Route::post('/update', 'backend\BonusScoreController@update');
            Route::post('//delete', 'backend\BonusScoreController@delete');
            Route::get('/getallSoft', 'backend\BonusScoreController@getAllSoft');
            Route::any('/softdelete', 'backend\BonusScoreController@softDelete');
            Route::any('/active', 'backend\BonusScoreController@active');
            Route::any('/delete/multiple', 'backend\BonusScoreController@deleteMultiple');
            Route::any('/delete/soft/multiple', 'backend\BonusScoreController@deleteMultipleSOft');
            Route::any('/active/multiple', 'backend\BonusScoreController@activeMultiple');
            Route::post('/export/world', 'backend\BonusScoreController@exportWorld');
            Route::post('/export/pdf', 'backend\BonusScoreController@exportPdf');
            Route::post('/export/excel', 'backend\BonusScoreController@exportExcel');
        });
        // plant_and_equipment;
        Route::group(['prefix' => 'plant_and_equipment'], function () {
            Route::get('/', 'backend\PlantEquipmentController@index');
            Route::get('/getall', 'backend\PlantEquipmentController@getAll');
            Route::get('/getallSoft', 'backend\PlantEquipmentController@getAllSoft');
            Route::post('/export/world', 'backend\PlantEquipmentController@exportWorld');
            Route::post('/export/pdf', 'backend\PlantEquipmentController@exportPdf');
            Route::post('/export/excel', 'backend\PlantEquipmentController@exportExcel');
            Route::post('/edit', 'backend\PlantEquipmentController@edit');
            Route::get('/store', 'backend\PlantEquipmentController@store');
            Route::any('/update', 'backend\PlantEquipmentController@update');
            Route::any('/delete', 'backend\PlantEquipmentController@delete');
            Route::any('/softdelete', 'backend\PlantEquipmentController@softDelete');
            Route::any('/delete/multiple', 'backend\PlantEquipmentController@deleteMultiple');
            Route::any('/delete/soft/multiple', 'backend\PlantEquipmentController@deleteMultipleSOft');
            Route::any('/active/multiple', 'backend\PlantEquipmentController@activeMultiple');
            Route::any('/active', 'backend\PlantEquipmentController@active');
        });
        //contractor
        Route::group(['prefix' => 'contractor'], function () {
            Route::get('/getall', 'backend\ContractorController@getAll');
            Route::get('/getallSoft', 'backend\ContractorController@getAllSoft');
            Route::get('/store', 'backend\ContractorController@store');
            Route::post('/update', 'backend\ContractorController@update');
            Route::post('/delete', 'backend\ContractorController@delete');
            Route::any('/softdelete', 'backend\ContractorController@softDelete');
            Route::any('/delete/multiple', 'backend\ContractorController@deleteMultiple');
            Route::any('/delete/soft/multiple', 'backend\ContractorController@deleteMultipleSOft');
            Route::any('/active', 'backend\ContractorController@active');
            Route::any('/active/multiple', 'backend\ContractorController@activeMultiple');
            Route::post('/export/world', 'backend\ContractorController@exportWorld');
            Route::post('/export/pdf', 'backend\ContractorController@exportPdf');
            Route::post('/export/excel', 'backend\ContractorController@exportExcel');
        });
        //devison
        Route::group(['prefix' => 'devision'], function () {
            Route::get('/getall', 'backend\DevisionController@getAll');
            Route::get('/store', 'backend\DevisionController@store');
            Route::post('/update', 'backend\DevisionController@update');
            Route::post('/delete', 'backend\DevisionController@delete');
            Route::any('/delete/multiple', 'backend\DevisionController@deleteMultiple');
            Route::get('/getallSoft', 'backend\DevisionController@getAllSoft');
            Route::any('/softdelete', 'backend\DevisionController@softDelete');
            Route::any('/delete/soft/multiple', 'backend\DevisionController@deleteMultipleSOft');
            Route::any('/active', 'backend\DevisionController@active');
            Route::any('/active/multiple', 'backend\DevisionController@activeMultiple');
            Route::post('/export/world', 'backend\DevisionController@exportWorld');
            Route::post('/export/pdf', 'backend\DevisionController@exportPdf');
            Route::post('/export/excel', 'backend\DevisionController@exportExcel');
        });
        //Job Location
        Route::group(['prefix' => 'job/location'], function () {
            Route::get('/getall/{id}', 'JobLocationController@getAll');
            Route::get('/store/{id}', 'JobLocationController@store');
            Route::post('/update', 'JobLocationController@update');
            Route::post('/delete', 'JobLocationController@delete');
            Route::any('/delete/multiple', 'JobLocationController@multipleDelete');
            Route::get('/getallSoft/{id}', 'JobLocationController@getAllSoft');
            Route::any('/softdelete', 'JobLocationController@softDelete');
            Route::any('/delete/soft/multiple', 'JobLocationController@deleteMultipleSOft');
            Route::any('/active', 'backend\JobLocationController@active');
            Route::any('/active/multiple', 'backend\JobLocationController@activeMultiple');
            Route::post('/export/world', 'backend\JobLocationController@exportWorld');
            Route::post('/export/pdf', 'backend\JobLocationController@exportPdf');
            Route::post('/export/excel', 'backend\JobLocationController@exportExcel');
        });
        //customer
        Route::group(['prefix' => 'customer'], function () {
            Route::get('/getall', 'backend\CustomersController@getAll');
            Route::get('/getall/bydivision', 'backend\CustomersController@getAllbyDivision');
            Route::get('/getall/sitegroup', 'backend\CustomersController@getAllbySite');
            Route::get('/store', 'backend\CustomersController@store');
            Route::post('/update', 'backend\CustomersController@update');
            Route::post('/delete', 'backend\CustomersController@delete');
            Route::any('/delete/multiple', 'backend\CustomersController@deleteMultiple');
            Route::get('/getallSoft', 'backend\CustomersController@getAllSoft');
            Route::any('/softdelete', 'backend\CustomersController@softDelete');
            Route::any('/delete/soft/multiple', 'backend\CustomersController@deleteMultipleSOft');
            Route::any('/active', 'backend\CustomersController@active');
            Route::any('/active/multiple', 'backend\CustomersController@activeMultiple');
            Route::post('/export/world', 'backend\CustomersController@exportWorld');
            Route::post('/export/pdf', 'backend\CustomersController@exportPdf');
            Route::post('/export/excel', 'backend\CustomersController@exportExcel');
        });
        //site Group
        Route::group(['prefix' => 'sitegroup'], function () {
            Route::get('/getall', 'backend\SiteGroupsController@getAll');
            Route::get('/store', 'backend\SiteGroupsController@store');
            Route::post('/update', 'backend\SiteGroupsController@update');
            Route::post('/delete', 'backend\SiteGroupsController@delete');
            Route::any('/delete/multiple', 'backend\SiteGroupsController@deleteMultiple');
            Route::get('/getall/getAllbyCustAndDivi', 'backend\SiteGroupsController@getAllbyCustAndDivi');
            Route::get('/getallSoft', 'backend\SiteGroupsController@getAllSoft');
            Route::any('/softdelete', 'backend\SiteGroupsController@softDelete');
            Route::any('/delete/soft/multiple', 'backend\SiteGroupsController@deleteMultipleSOft');
            Route::any('/active', 'backend\SiteGroupsController@active');
            Route::any('/active/multiple', 'backend\SiteGroupsController@activeMultiple');
            Route::post('/export/world', 'backend\SiteGroupsController@exportWorld');
            Route::post('/export/pdf', 'backend\SiteGroupsController@exportPdf');
            Route::post('/export/excel', 'backend\SiteGroupsController@exportExcel');
        });
        //client
        Route::group(['prefix' => 'client'], function () {
            Route::get('/', 'backend\ClientController@index');
            Route::get('/upload', 'backend\ClientController@upload');
            Route::get('/notes', 'backend\ClientController@notes');
            Route::get('/getall', 'backend\ClientController@getAll');
            Route::get('/getAllClientMain', 'backend\ClientController@getAllClientMain');
            Route::get('/getallSoft', 'backend\ClientController@getAllSoftClientMain');
            Route::any('/active', 'backend\ClientController@active');
            Route::any('/active/multiple', 'backend\ClientController@activeMultiple');
            Route::post('/restore', 'backend\ClientController@restore');
            Route::post('/exportPdf', 'backend\ClientController@exportPdf');
            Route::post('/exportWord', 'backend\ClientController@exportWord');
            Route::post('/exportExcel', 'backend\ClientController@exportExcel');
            Route::get('/store', 'backend\ClientController@store');
            Route::post('/exportPdfNote', 'backend\ClientController@exportPdfNote');
            Route::post('/exportWordNote', 'backend\ClientController@exportWordNote');
            Route::post('/exportExcelNote', 'backend\ClientController@exportExcelNote');
            Route::get('/storeDetail', 'backend\ClientController@storeDetail');
            Route::get('/createDetail', 'backend\ClientController@createDetail');
            Route::post('/update', 'backend\ClientController@update');
            Route::post('/updateDetail', 'backend\ClientController@updateDetail');
            Route::post('/delete', 'backend\ClientController@delete');
            Route::post('/deleteMultipleSOft', 'backend\ClientController@deleteMultipleSOft');
            Route::post('/softdelete', 'backend\ClientController@softDelete');
            Route::post('/deleteMultiple', 'backend\ClientController@deleteMultiple');
            Route::post('/storeMain', 'backend\ClientController@storeMain');
            Route::get('/detail/{id}', 'backend\ClientController@detail')->name('detail');
        });


        //uploads
        Route::group(['prefix' => 'upload'], function () {
            Route::post('/getAll', 'backend\UploadController@getAll');
            Route::post('/getAllSoft', 'backend\UploadController@getAllSoft');
            Route::get('/download/orginal/{id}', 'backend\UploadController@orginalDownload');
            Route::post('/upload', 'backend\UploadController@upload');
            Route::post('/update/client', 'backend\UploadController@updateUploadClient')->name('client_updateUpload');
            Route::post('/update', 'backend\UploadController@update');
            Route::post('/store', 'backend\UploadController@store')->name('client_storeUpload');
            Route::post('/hardDelete', 'backend\UploadController@hardDelete');
            Route::post('/delete', 'backend\UploadController@delete');
            Route::post('/edit', 'backend\UploadController@Edit');
            Route::post('/restore', 'backend\UploadController@restore');
            Route::post('/active/multiple', 'backend\UploadController@multipleActive');
            Route::post('/multipleDelete', 'backend\UploadController@multipleDelete');
            Route::post('/exportPdf', 'backend\UploadController@exportPdf');
            Route::post('/exportExcel', 'backend\UploadController@exportExcel');
            Route::post('/exportWorld', 'backend\UploadController@exportWorld');
        });
        //notes
        Route::group(['prefix' => 'note'], function () {
            Route::post('/getall', 'backend\NoteController@getAll');
            Route::post('/edit', 'backend\NoteController@edit');
            Route::post('/store', 'backend\NoteController@store');
            Route::post('/update', 'backend\NoteController@update');
            Route::post('/delete', 'backend\NoteController@delete');
            Route::any('/delete/multiple', 'backend\NoteController@deleteMultiple');
            Route::post('/getallSoft', 'backend\NoteController@getAllSoft');
            Route::any('/softdelete', 'backend\NoteController@softDelete');
            Route::any('/active', 'backend\NoteController@active');
            Route::any('/delete/soft/multiple', 'backend\NoteController@deleteMultipleSOft');
            Route::any('/active/multiple', 'backend\NoteController@activeMultiple');
            Route::post('/export/word', 'backend\NoteController@exportWorld');
            Route::post('/export/pdf', 'backend\NoteController@exportPdf');
            Route::post('/export/excel', 'backend\NoteController@exportExcel');
        });
        //Static Form
        Route::group(['prefix' => 'static_form'], function () {
            Route::get('/view/{id}', 'backend\StaticFormController@view');
            Route::get('/active/{id}', 'backend\StaticFormController@activeStatic');
            Route::get('/getAllUpload', 'backend\StaticFormController@getAllUpload');
            Route::get('/getAllSoftUpload', 'backend\StaticFormController@getAllSoftUpload');
            Route::post('/storeUpload', 'backend\StaticFormController@storeUpload')->name('static_form_storeUpload');
            Route::post('/update', 'backend\StaticFormController@update');
            Route::post('/delete', 'backend\StaticFormController@delete');
            Route::post('/hardDelete', 'backend\StaticFormController@hardDelete');
            Route::post('/restore', 'backend\StaticFormController@restore');
            Route::post('/active/multiple', 'backend\StaticFormController@activeMultiple');
            Route::post('/delete/multiple', 'backend\StaticFormController@deleteMultiple');
            Route::post('/updateUpload', 'backend\StaticFormController@updateUpload')->name('static_form_updateUpload');
            Route::get('/view-pdf/{id}', 'backend\StaticFormController@viewpdf')->name('view-pdf');
            Route::get('/download/pdf/{id}', 'backend\StaticFormController@pdfDownload');
            Route::get('/download/orginal/{id}', 'backend\StaticFormController@orginalDownload');
            Route::post('/export/excel/', 'backend\StaticFormController@exportExcel');
            Route::post('/export/pdf/', 'backend\StaticFormController@exportPdf');
            Route::post('/export/word/', 'backend\StaticFormController@exportWord');
        });
        //manager
        Route::group(['prefix' => 'manager'], function () {
            Route::get('/getalldropdown', 'backend\ManagerController@getAllDropDown');
            Route::get('/getall', 'backend\ManagerController@getAll');
            Route::get('/store', 'backend\ManagerController@store');
            Route::post('/update', 'backend\ManagerController@update');
            Route::post('/delete', 'backend\ManagerController@delete');
            Route::any('/delete/multiple', 'backend\ManagerController@deleteMultiple');
            Route::get('/getallSoft', 'backend\ManagerController@getAllSoft');
            Route::any('/softdelete', 'backend\ManagerController@softDelete');
            Route::any('/delete/soft/multiple', 'backend\ManagerController@deleteMultipleSOft');
            Route::any('/active', 'backend\ManagerController@active');
            Route::any('/active/multiple', 'backend\ManagerController@activeMultiple');
            Route::post('/export/world', 'backend\ManagerController@exportWorld');
            Route::post('/export/pdf', 'backend\ManagerController@exportPdf');
            Route::post('/export/excel', 'backend\ManagerController@exportExcel');
        });
        //contrator manager
        Route::group(['prefix' => 'contratormanager'], function () {
            Route::get('/getall', 'backend\ContractorManagerController@getAll');
            Route::get('/store', 'backend\ContractorManagerController@store');
            Route::post('/update', 'backend\ContractorManagerController@update');
            Route::post('/delete', 'backend\ContractorManagerController@delete');
            Route::any('/delete/multiple', 'backend\ContractorManagerController@deleteMultiple');
            Route::get('/getallSoft', 'backend\ContractorManagerController@getAllSoft');
            Route::any('/softdelete', 'backend\ContractorManagerController@softDelete');
            Route::any('/delete/soft/multiple', 'backend\ContractorManagerController@deleteMultipleSOft');
            Route::any('/active', 'backend\ContractorManagerController@active');
            Route::any('/active/multiple', 'backend\ContractorManagerController@activeMultiple');
            Route::post('/export/world', 'backend\ContractorManagerController@exportWorld');
            Route::post('/export/pdf', 'backend\ContractorManagerController@exportPdf');
            Route::post('/export/excel', 'backend\ContractorManagerController@exportExcel');
        });
        //addresstype
        Route::group(['prefix' => 'addresstype'], function () {
            Route::get('/getall', 'backend\AddressTypeController@getAll');
            Route::get('/store', 'backend\AddressTypeController@store');
            Route::post('/update', 'backend\AddressTypeController@update');
            Route::post('/delete', 'backend\AddressTypeController@delete');
            Route::post('/delete/multiple', 'backend\AddressTypeController@deleteMultiple');
            Route::get('/getallSoft', 'backend\AddressTypeController@getAllSoft');
            Route::any('/softdelete', 'backend\AddressTypeController@softDelete');
            Route::any('/delete/soft/multiple', 'backend\AddressTypeController@deleteMultipleSOft');
            Route::any('/active', 'backend\AddressTypeController@active');
            Route::any('/active/multiple', 'backend\AddressTypeController@activeMultiple');
            Route::post('/export/world', 'backend\AddressTypeController@exportWorld');
            Route::post('/export/pdf', 'backend\AddressTypeController@exportPdf');
            Route::post('/export/excel', 'backend\AddressTypeController@exportExcel');
        });
        //contacttype
        Route::group(['prefix' => 'contacttype'], function () {
            Route::get('/getall', 'backend\ContactTypeController@getAll');
            Route::get('/store', 'backend\ContactTypeController@store');
            Route::post('/update', 'backend\ContactTypeController@update');
            Route::post('/delete', 'backend\ContactTypeController@delete');
            Route::post('/delete/multiple', 'backend\ContactTypeController@deleteMultiple');
            Route::get('/getallSoft', 'backend\ContactTypeController@getAllSoft');
            Route::any('/softdelete', 'backend\ContactTypeController@softDelete');
            Route::any('/delete/soft/multiple', 'backend\ContactTypeController@deleteMultipleSOft');
            Route::any('/active', 'backend\ContactTypeController@active');
            Route::any('/active/multiple', 'backend\ContactTypeController@activeMultiple');
            Route::post('/export/world', 'backend\ContactTypeController@exportWorld');
            Route::post('/export/pdf', 'backend\ContactTypeController@exportPdf');
            Route::post('/export/excel', 'backend\ContactTypeController@exportExcel');
        });
        //document
        Route::group(['prefix' => 'document'], function () {
            Route::get('/detail/{id}', 'backend\DocumentController@detail')->name('document.detail');
            Route::get('/createDetail', 'backend\DocumentController@createDetail');
            Route::get('/notes', 'backend\DocumentController@notes');
            Route::get('/bulk/import', 'backend\DocumentController@upload');
            Route::get('/getAllPopulate', 'backend\DocumentController@getAllPopulate');
            Route::post('/updateDetail', 'backend\DocumentController@updateDetail');
            Route::post('/updatePopulate', 'backend\DocumentController@updatePopulate');
            Route::post('/deletePopulate', 'backend\DocumentController@deletePopulate');
            Route::get('/storePopulate', 'backend\DocumentController@storePopulate');
            Route::post('/raiseJob', 'backend\DocumentController@raiseJob');
            Route::get('/completed_forms', 'backend\CompleteFormsController@document');
        });
        //Form Type
        Route::group(['prefix' => 'form_type'], function () {
            Route::get('/getall', 'backend\FormTypeController@getAll');
            Route::get('/refresh', 'backend\FormTypeController@refresh');
            Route::post('/store', 'backend\FormTypeController@store');
            Route::post('/update', 'backend\FormTypeController@update');
            Route::post('/edit', 'backend\FormTypeController@edit');
            Route::post('/delete', 'backend\FormTypeController@delete');
            Route::any('/delete/multiple', 'backend\FormTypeController@deleteMultiple');
            Route::get('/getallSoft', 'backend\FormTypeController@getAllSoft');
            Route::any('/softdelete', 'backend\FormTypeController@softDelete');
            Route::any('/active', 'backend\FormTypeController@active');
            Route::any('/delete/soft/multiple', 'backend\FormTypeController@deleteMultipleSOft');
            Route::any('/active/multiple', 'backend\FormTypeController@activeMultiple');
            Route::post('/export/world', 'backend\FormTypeController@exportWorld');
            Route::post('/export/pdf', 'backend\FormTypeController@exportPdf');
            Route::post('/export/excel', 'backend\FormTypeController@exportExcel');
        });
        //Qr
        Route::group(['prefix' => 'qr'], function () {
            Route::get('/show', 'backend\QRController@index');
            Route::get('/getall', 'backend\QRController@getAll');
            Route::get('/getallSoft', 'backend\QRController@getAllSoft');
            Route::get('/store', 'backend\QRController@store');
            Route::post('/update', 'backend\QRController@update');
            Route::post('/delete', 'backend\QRController@delete');
            Route::post('/active', 'backend\QRController@active');
            Route::any('/softdelete', 'backend\QRController@softDelete');
            Route::any('/delete/multiple', 'backend\QRController@deleteMultiple');
            Route::any('/delete/soft/multiple', 'backend\QRController@deleteMultipleSOft');
            Route::any('/active/multiple', 'backend\QRController@activeMultiple');
            Route::post('/export/world', 'backend\QRController@exportWorld');
            Route::post('/export/pdf', 'backend\QRController@exportPdf');
            Route::post('/export/excel', 'backend\QRController@exportExcel');
        });
        // document scoring
        Route::group(['prefix' => '/document/bonus/score'], function () {
            Route::get('/', 'backend\ScoreController@index');
            Route::get('/getall', 'backend\ScoreController@getAll');
            Route::get('/store', 'backend\ScoreController@store');
            Route::post('/update', 'backend\ScoreController@update');
            Route::post('/delete', 'backend\ScoreController@delete');
            Route::get('/getallSoft', 'backend\ScoreController@getAllSoft');
            Route::any('/softdelete', 'backend\ScoreController@softDelete');
            Route::any('/active', 'backend\ScoreController@active');
            Route::any('/delete/multiple', 'backend\ScoreController@deleteMultiple');
            Route::any('/delete/soft/multiple', 'backend\ScoreController@deleteMultipleSOft');
            Route::any('/active/multiple', 'backend\ScoreController@activeMultiple');
            Route::post('/export/world', 'backend\ScoreController@exportWorld');
            Route::post('/export/pdf', 'backend\ScoreController@exportPdf');
            Route::post('/export/excel', 'backend\ScoreController@exportExcel');
        });
        // document form_group
        Route::group(['prefix' => '/form_group'], function () {
            Route::post('/paste', 'backend\FormGroupController@paste');
            Route::post('/reorder', 'backend\FormGroupController@reOrder');
            Route::get('/folder/{id}', 'backend\FormGroupController@folderInside');
            Route::get('/', 'backend\FormGroupController@index');
            Route::get('/getall/{id}', 'backend\FormGroupController@getAll');
            Route::get('/store', 'backend\FormGroupController@store');
            Route::post('/update', 'backend\FormGroupController@update');
            Route::post('/delete', 'backend\FormGroupController@delete');
            Route::get('/getallSoft', 'backend\FormGroupController@getAllSoft');
            Route::any('/softdelete', 'backend\FormGroupController@softDelete');
            Route::any('/active', 'backend\FormGroupController@active');
            Route::any('/delete/multiple', 'backend\FormGroupController@deleteMultiple');
            Route::any('/delete/soft/multiple', 'backend\FormGroupController@deleteMultipleSOft');
            Route::any('/active/multiple', 'backend\FormGroupController@activeMultiple');
            Route::post('/export/world', 'backend\FormGroupController@exportWorld');
            Route::post('/export/pdf', 'backend\FormGroupController@exportPdf');
            Route::post('/export/excel', 'backend\FormGroupController@exportExcel');
        });
        Route::group(['prefix' => '/view_register'], function () {
            Route::get('/', 'backend\ViewRegisterController@index');
            Route::get('/getData', 'backend\ViewRegisterController@getData');
            Route::post('/updatePermision', 'backend\ViewRegisterController@updatePermision');

        });
        // document form_group
        Route::group(['prefix' => '/form'], function () {
            Route::get('/folder/{id}', 'backend\FormController@folderInside');
            Route::get('/', 'backend\FormController@index');
            Route::get('/getall', 'backend\FormController@getAll');
            Route::get('/get/{id}', 'backend\FormController@get');
            Route::get('/store', 'backend\FormController@store');
            Route::post('/update', 'backend\FormController@update');
            Route::post('/populate', 'backend\FormController@populate');
            Route::post('/addPopulate', 'backend\FormController@addPopulate');
            Route::post('/delete', 'backend\FormController@delete');
            Route::get('/getallSoft', 'backend\FormController@getAllSoft');
            Route::any('/softdelete', 'backend\FormController@softDelete');
            Route::any('/active', 'backend\FormController@active');
            Route::any('/delete/multiple', 'backend\FormController@deleteMultiple');
            Route::any('/delete/soft/multiple', 'backend\FormController@deleteMultipleSOft');
            Route::any('/active/multiple', 'backend\FormController@activeMultiple');
            Route::post('/export/world', 'backend\FormController@exportWorld');
            Route::post('/export/pdf', 'backend\FormController@exportPdf');
            Route::post('/export/excel', 'backend\FormController@exportExcel');
        });
        //TaskType
        Route::group(['prefix' => 'tasktype'], function () {
            Route::get('/getall', 'backend\TaskTypeController@getAll');
            Route::get('/getAllSoft', 'backend\TaskTypeController@getAllSoft');
            Route::get('/store', 'backend\TaskTypeController@store');
            Route::post('/update', 'backend\TaskTypeController@update');
            Route::post('/delete', 'backend\TaskTypeController@delete');
            Route::any('/delete/multiple', 'backend\TaskTypeController@deleteMultiple');
            Route::any('/delete/soft/multiple', 'backend\TaskTypeController@deleteMultipleSOft');
            Route::post('/active/multiple', 'backend\TaskTypeController@activeMultiple');
            Route::any('/active', 'backend\TaskTypeController@active');
            Route::any('/export/pdf', 'backend\TaskTypeController@exportPdf');
            Route::any('/export/word', 'backend\TaskTypeController@exportWorld');
            Route::any('/export/excel', 'backend\TaskTypeController@exportExcel');
        });
        Route::group(['prefix' => 'question'], function () {
            Route::get('/', 'backend\QuestionController@index');
            Route::get('/edit/{id}', 'backend\QuestionController@edit');
            // Route::get('/getall', 'backend\QuestionController@getAll');
            Route::post('/store', 'backend\QuestionController@store');
            Route::post('/update', 'backend\QuestionController@update');
            Route::post('/delete', 'backend\QuestionController@delete');
        });
        Route::group(['prefix' => 'job'], function () {
            Route::get('/{id?}', 'backend\JobController@index');
            Route::get('/view/{id}', 'backend\JobController@view');
            Route::get('/getForm/{id}', 'backend\JobController@getForm');
            Route::get('/getContact/{id}', 'backend\JobController@getContact');
            Route::get('/getLocation/{id}', 'backend\JobController@getLocation');
            Route::get('/getPopulate/{id}', 'backend\JobController@getPopulate');
            Route::get('/getall/{id?}', 'backend\JobController@getAll');
            Route::get('/view-pdf/{id}', 'backend\JobController@viewPdf');
            Route::post('/store', 'backend\JobController@store');
            Route::post('/replicate', 'backend\JobController@replicate');
            Route::post('/response', 'backend\JobController@response');
            Route::post('/responseForm', 'backend\JobController@responseForm');
            Route::post('/delete', 'backend\JobController@delete');
            Route::post('/multipleDelete', 'backend\JobController@multipleDelete');
            Route::get('/get/version', 'backend\JobController@getVersion');
        });

        Route::group(['prefix' => 'answer_group'], function () {
            Route::get('/getall', 'backend\AnswerGroupController@getAll');
            Route::post('/store', 'backend\AnswerGroupController@store');
            Route::post('/update', 'backend\AnswerGroupController@update');
            Route::post('/delete', 'backend\AnswerGroupController@delete');
            Route::get('/getallSoft', 'backend\AnswerGroupController@getAllSoft');
            Route::post('/softdelete', 'backend\AnswerGroupController@softDelete');
            Route::post('/active', 'backend\AnswerGroupController@active');
            Route::any('/delete/multiple', 'backend\AnswerGroupController@deleteMultiple');
            Route::any('/delete/soft/multiple', 'backend\AnswerGroupController@deleteMultipleSoft');
            Route::any('/active/multiple', 'backend\AnswerGroupController@activeMultiple');
            Route::post('/export/world', 'backend\AnswerGroupController@exportWorld');
            Route::post('/export/pdf', 'backend\AnswerGroupController@exportPdf');
            Route::post('/export/excel', 'backend\AnswerGroupController@exportExcel');

        });
        Route::group(['prefix' => 'answer'], function () {
            Route::get('/getall/{id}', 'backend\AnswerController@getAll');
            Route::post('/store', 'backend\AnswerController@store');
            Route::post('/update', 'backend\AnswerController@update');
            Route::post('/uploadIcon', 'backend\AnswerController@uploadIcon');
            Route::post('/delete', 'backend\AnswerController@delete');
            Route::get('/getallSoft/{id}', 'backend\AnswerController@getAllSoft');
            Route::post('/softdelete', 'backend\AnswerController@softDelete');
            Route::post('/active', 'backend\AnswerController@active');
            Route::any('/delete/multiple', 'backend\AnswerController@deleteMultiple');
            Route::any('/delete/soft/multiple', 'backend\AnswerController@deleteMultipleSoft');
            Route::any('/active/multiple', 'backend\AnswerController@activeMultiple');
            Route::post('/export/world', 'backend\AnswerController@exportWorld');
            Route::post('/export/pdf', 'backend\AnswerController@exportPdf');
            Route::post('/export/excel', 'backend\AnswerController@exportExcel');

        });
        //TaskType
        Route::group(['prefix' => '/document/forms'], function () {
            Route::get('/', 'backend\DocumentController@forms');
            Route::get('/answer', 'backend\DocumentController@answer');
            Route::post('/store', 'backend\DocumentController@store');
            Route::get('/get', 'backend\DocumentController@getAll');
            Route::get('/get/active', 'backend\DocumentController@getAllActive');
            Route::get('/get/question', 'backend\DocumentController@getAllQuestion');
            Route::post('/edit', 'backend\DocumentController@edit');
            Route::post('/complete', 'backend\DocumentController@completeForm');
            Route::post('/update', 'backend\DocumentController@updateForm');
            Route::post('/reorder', 'backend\DocumentController@reOrder');
            Route::post('/delete', 'backend\DocumentController@deleteForm');
            Route::post('/clone', 'backend\DocumentController@cloneForm');
            Route::get('/share', 'backend\DocumentController@share');
            Route::get('/copyActive', 'backend\DocumentController@copyActive');
        });
        //version log
        Route::group(['prefix' => 'version/log'], function () {
            Route::get('/', 'backend\VersionController@index');
            Route::get('/view/{id}', 'backend\VersionController@view');
            Route::get('/getAll', 'backend\VersionController@getAll');
            Route::get('/getAllSoft', 'backend\VersionController@getAllSoft');
            Route::get('/get/version', 'backend\VersionController@getVersion');
            Route::get('/getPopulate/{id}', 'backend\VersionController@getPopulate');
            Route::get('/getPopulate/one/{id}/{question_id}', 'backend\VersionController@getOnePopulate');
            Route::get('/viewLatest/{id}', 'backend\VersionController@viewLatest');
            Route::get('/show-pdf/{id}', 'backend\VersionController@viewpdf')->name('show-pdf');
            Route::post('/delete', 'backend\VersionController@delete');
            Route::post('/multipleDelete', 'backend\VersionController@multipleDelete');
            Route::post('/activeMultiple', 'backend\VersionController@activeMultiple');
            Route::post('/restore', 'backend\VersionController@restore');
            Route::post('/hardDelete', 'backend\VersionController@hardDelete');
            Route::post('/excel/export', 'backend\VersionController@exportExcel');
            Route::post('/pdf/export', 'backend\VersionController@exportPdf');
            Route::post('/word/export', 'backend\VersionController@exportWord');
            Route::get('/pdf/form/export/{id}/{pre_populate_id?}', 'backend\VersionController@exportVersionPdf_view');
             Route::get('/pdf/form/{id}', 'backend\VersionController@exportForm');
        });
        // document schedule
        Route::group(['prefix' => '/schedule'], function () {
            Route::get('/tasktype/listing', 'backend\ScheduleController@tasktype');
            Route::get('/', 'backend\ScheduleController@index');
            Route::get('/test', 'backend\ScheduleController@testCron');
            Route::get('/project/{id}/{form_id?}', 'backend\ScheduleController@getProject');
            Route::get('/getall', 'backend\ScheduleController@getAll');
            Route::any('/store', 'backend\ScheduleController@store');
            Route::post('/edit', 'backend\ScheduleController@edit');
            Route::post('/update', 'backend\ScheduleController@update');
            Route::post('/delete', 'backend\ScheduleController@delete');
            Route::get('/getallSoft', 'backend\ScheduleController@getAllSoft');
            Route::any('/softdelete', 'backend\ScheduleController@softDelete');
            Route::any('/active', 'backend\ScheduleController@active');
            Route::any('/delete/multiple', 'backend\ScheduleController@deleteMultiple');
            Route::any('/delete/soft/multiple', 'backend\ScheduleController@deleteMultipleSOft');
            Route::any('/active/multiple', 'backend\ScheduleController@activeMultiple');
            Route::post('/export/world', 'backend\ScheduleController@exportWorld');
            Route::post('/export/pdf', 'backend\ScheduleController@exportPdf');
            Route::post('/export/excel', 'backend\ScheduleController@exportExcel');
        });
        //RelationShip
        Route::group(['prefix' => 'relationship'], function () {
            Route::get('/add', 'backend\RelationShipController@relationship');
            Route::get('/getall', 'backend\RelationShipController@getAll');
            Route::get('/getallSoft', 'backend\RelationShipController@getAllSoft');
            Route::get('/store', 'backend\RelationShipController@store');
            Route::post('/update', 'backend\RelationShipController@update');
            Route::post('/delete', 'backend\RelationShipController@delete');
            Route::post('/softdelete', 'backend\RelationShipController@softDelete');
            Route::post('/active', 'backend\RelationShipController@restore');
            Route::any('/delete/multiple', 'backend\RelationShipController@deleteMultiple');
            Route::any('/delete/soft/multiple', 'backend\RelationShipController@deleteMultipleSoft');
            Route::any('/active/multiple', 'backend\RelationShipController@activeMultiple');
            Route::post('/export/world', 'backend\RelationShipController@exportWorld');
            Route::post('/export/pdf', 'backend\RelationShipController@exportPdf');
            Route::post('/export/excel', 'backend\RelationShipController@exportExcel');
        });

        Route::group(['prefix' => 'employee'], function () {
            Route::get('/', 'backend\EmployeeController@index');
            Route::get('/getall', 'backend\EmployeeController@getAll');
            Route::get('/detail/{id}', 'backend\EmployeeController@detail');
            Route::get('/createDetail', 'backend\EmployeeController@createDetail');
            Route::post('/updateDetail', 'backend\EmployeeController@updateDetail');
            Route::get('/notes', 'backend\EmployeeController@notes');
            Route::post('/delete', 'backend\EmployeeController@delete');
            Route::get('/getallSoft', 'backend\EmployeeController@getAllSoft');
            Route::any('/softdelete', 'backend\EmployeeController@softDelete');
            Route::any('/active', 'backend\EmployeeController@active');
            Route::any('/delete/multiple', 'backend\EmployeeController@deleteMultiple');
            Route::any('/delete/soft/multiple', 'backend\EmployeeController@deleteMultipleSOft');
            Route::any('/active/multiple', 'backend\EmployeeController@activeMultiple');
            Route::post('/export/world', 'backend\EmployeeController@exportWorld');
            Route::post('/export/pdf', 'backend\EmployeeController@exportPdf');
            Route::post('/export/excel', 'backend\EmployeeController@exportExcel');

            /*****sexual orientation********/
            Route::group(['prefix' => '/sexualorientation'], function () {
                Route::get('/getall', 'backend\SexualOrientationController@getAll');
                Route::get('/getallSoft', 'backend\SexualOrientationController@getAllSoft');
                Route::get('/store', 'backend\SexualOrientationController@store');
                Route::post('/update', 'backend\SexualOrientationController@update');
                Route::post('/delete', 'backend\SexualOrientationController@delete');
                Route::any('/softdelete', 'backend\SexualOrientationController@softDelete');
                Route::any('/active', 'backend\SexualOrientationController@active');
                Route::any('/active/multiple', 'backend\SexualOrientationController@activeMultiple');
                Route::post('/delete/multiple', 'backend\SexualOrientationController@deleteMultiple');
                Route::post('/delete/soft/multiple', 'ba    ckend\SexualOrientationController@deleteMultipleSOft');
                Route::post('/exportPdf', 'backend\SexualOrientationController@exportPdf');
                Route::post('/exportWord', 'backend\SexualOrientationController@exportWord');
                Route::post('/exportExcel', 'backend\SexualOrientationController@exportExcel');
            });
            /*****ethinic********/
            Route::group(['prefix' => '/ethinic'], function () {
                Route::get('/getall', 'backend\EthnicOriginController@getAll');
                Route::get('/getallSoft', 'backend\EthnicOriginController@getAllSoft');
                Route::get('/store', 'backend\EthnicOriginController@store');
                Route::post('/update', 'backend\EthnicOriginController@update');
                Route::post('/delete', 'backend\EthnicOriginController@delete');
                Route::any('/softdelete', 'backend\EthnicOriginController@softDelete');
                Route::any('/active', 'backend\EthnicOriginController@active');
                Route::any('/active/multiple', 'backend\EthnicOriginController@activeMultiple');
                Route::post('/delete/multiple', 'backend\EthnicOriginController@deleteMultiple');
                Route::post('/delete/soft/multiple', 'backend\EthnicOriginController@deleteMultipleSOft');
                Route::post('/exportPdf', 'backend\EthnicOriginController@exportPdf');
                Route::post('/exportWord', 'backend\EthnicOriginController@exportWord');
                Route::post('/exportExcel', 'backend\EthnicOriginController@exportExcel');
            });
            /*****religion********/
            Route::group(['prefix' => '/religion'], function () {
                Route::get('/getall', 'backend\ReligionController@getAll');
                Route::get('/getallSoft', 'backend\ReligionController@getAllSoft');
                Route::get('/store', 'backend\ReligionController@store');
                Route::post('/update', 'backend\ReligionController@update');
                Route::post('/delete', 'backend\ReligionController@delete');
                Route::any('/softdelete', 'backend\ReligionController@softDelete');
                Route::any('/active', 'backend\ReligionController@active');
                Route::any('/active/multiple', 'backend\ReligionController@activeMultiple');
                Route::post('/delete/multiple', 'backend\ReligionController@deleteMultiple');
                Route::post('/delete/soft/multiple', 'backend\ReligionController@deleteMultipleSOft');
                Route::post('/exportPdf', 'backend\ReligionController@exportPdf');
                Route::post('/exportWord', 'backend\ReligionController@exportWord');
                Route::post('/exportExcel', 'backend\ReligionController@exportExcel');
            });
            /*****position********/
            Route::group(['prefix' => '/position'], function () {
                Route::get('/getall', 'backend\PositionController@getAll');
                Route::get('/store', 'backend\PositionController@store');
                Route::post('/update', 'backend\PositionController@update');
                Route::get('/getallSoft', 'backend\PositionController@getAllSoft');
                Route::post('/delete', 'backend\PositionController@delete');
                Route::any('/softdelete', 'backend\PositionController@softDelete');
                Route::any('/active', 'backend\PositionController@active');
                Route::any('/active/multiple', 'backend\PositionController@activeMultiple');
                Route::post('/delete/multiple', 'backend\PositionController@deleteMultiple');
                Route::post('/delete/soft/multiple', 'backend\PositionController@deleteMultipleSOft');
                Route::post('/exportPdf', 'backend\PositionController@exportPdf');
                Route::post('/exportWord', 'backend\PositionController@exportWord');
                Route::post('/exportExcel', 'backend\PositionController@exportExcel');
            });
        });

        Route::group(['prefix' => 'traning'], function () {
            Route::get('/', 'backend\TrainingMatrixController@index');
            Route::get('/getall', 'backend\TrainingMatrixController@getAll');
            Route::post('/delete', 'backend\TrainingMatrixController@delete');
            Route::get('/getallSoft', 'backend\TrainingMatrixController@getAllSoft');
            Route::any('/softdelete', 'backend\TrainingMatrixController@softDelete');
            Route::any('/active', 'backend\TrainingMatrixController@active');
            Route::any('/delete/multiple', 'backend\TrainingMatrixController@deleteMultiple');
            Route::any('/delete/soft/multiple', 'backend\TrainingMatrixController@deleteMultipleSOft');
            Route::any('/active/multiple', 'backend\TrainingMatrixController@activeMultiple');
            Route::post('/export/world', 'backend\TrainingMatrixController@exportWorld');
            Route::post('/export/pdf', 'backend\TrainingMatrixController@exportPdf');
            Route::post('/export/excel', 'backend\TrainingMatrixController@exportExcel');
        });
        ///static page
        Route::get('cms/web', 'backend\CMSController@web');
        Route::get('cms/app', 'backend\CMSController@app');
        Route::get('fors', 'backend\ForsController@index');
        Route::get('clocks', 'backend\ClockController@index');
        Route::get('sia', 'backend\SIAController@index');
        Route::get('/clear_notices', 'backend\ClearNotices@index');
        Route::get('/project/completed_forms/{id?}', 'backend\CompleteFormsController@project');
        Route::get('/documents', 'backend\DocumentController@index');
        Route::get('/documents/folder', 'backend\DocumentController@folder');
        Route::get('document/location', 'backend\DocumentController@location');
        Route::get('/panic/alaram', 'backend\PanicAlaramController@index');
        Route::get('/app/location', 'backend\AppLocationController@index');
        Route::get('/completed/forms', 'backend\CompleteFormsController@index');
        Route::get('completed/list', 'backend\CompleteListController@index');
        //support
        Route::group(['prefix' => '/support'], function () {
            Route::get('/', 'backend\SupportController@index');
            Route::get('/getall', 'backend\SupportController@getAll');
            Route::get('/getAllSoft', 'backend\SupportController@getAllSoft');
            Route::post('/edit', 'backend\SupportController@edit');
            Route::post('/delete', 'backend\SupportController@delete');
            Route::post('/deleteMultiple', 'backend\SupportController@deleteMultiple');
            Route::post('/deleteMultipleSOft', 'backend\SupportController@deleteMultipleSOft');
            Route::post('/restore', 'backend\SupportController@restore');
            Route::post('/restore/mutilple', 'backend\SupportController@restoreMutilple');
            Route::post('/harddelete', 'backend\SupportController@harddelete');
            Route::post('/email', 'backend\SupportController@send');
            Route::post('/export/pdf', 'backend\SupportController@exportPdf');
            Route::post('/export/word', 'backend\SupportController@exportWord');
            Route::post('/export/excel', 'backend\SupportController@exportExcel');
        });
        Route::group(['prefix' => '/emails'], function () {
            Route::get('/', 'backend\EmailController@index');
            Route::get('/getall', 'backend\EmailController@getAll');
            Route::post('/export/pdf', 'backend\EmailController@exportPdf');
            Route::post('/export/word', 'backend\EmailController@exportWord');
            Route::post('/export/excel', 'backend\EmailController@exportExcel');
        });
        //user next kin
        Route::group(['prefix' => '/next/kin'], function () {
            Route::get('/', 'backend\UserNextKinController@index');
            Route::get('/getall', 'backend\UserNextKinController@getAll');
            Route::get('/store', 'backend\UserNextKinController@store');
            Route::post('/update', 'backend\UserNextKinController@update');
            Route::post('/delete', 'backend\UserNextKinController@delete');
            Route::get('/getallSoft', 'backend\UserNextKinController@getAllSoft');
            Route::any('/softdelete', 'backend\UserNextKinController@softDelete');
            Route::any('/active', 'backend\UserNextKinController@restore');
            Route::any('/delete/multiple', 'backend\UserNextKinController@deleteMultiple');
            Route::any('/delete/soft/multiple', 'backend\UserNextKinController@deleteMultipleSOft');
            Route::any('/active/multiple', 'backend\UserNextKinController@activeMultiple');
            Route::post('/export/world', 'backend\UserNextKinController@exportWorld');
            Route::post('/export/pdf', 'backend\UserNextKinController@exportPdf');
            Route::post('/export/excel', 'backend\UserNextKinController@exportExcel');

        });
        //doc_type
        Route::group(['prefix' => '/doc_type'], function () {
            Route::get('/getall', 'backend\DocTypeController@getAll');
            Route::get('/store', 'backend\DocTypeController@store');
            Route::post('/update', 'backend\DocTypeController@update');
            Route::get('/getallSoft', 'backend\DocTypeController@getAllSoft');
            Route::post('/delete', 'backend\DocTypeController@delete');
            Route::any('/softdelete', 'backend\DocTypeController@softDelete');
            Route::any('/active', 'backend\DocTypeController@active');
            Route::any('/active/multiple', 'backend\DocTypeController@activeMultiple');
            Route::post('/delete/multiple', 'backend\DocTypeController@deleteMultiple');
            Route::post('/delete/soft/multiple', 'backend\DocTypeController@deleteMultipleSOft');
            Route::post('/exportPdf', 'backend\DocTypeController@exportPdf');
            Route::post('/exportWord', 'backend\DocTypeController@exportWord');
            Route::post('/exportExcel', 'backend\DocTypeController@exportExcel');
        });
        //user/doc_type
        Route::group(['prefix' => '/user/doc_type'], function () {
            Route::any('/getall', 'backend\UserDocTypeController@getAll');
            Route::get('/store', 'backend\UserDocTypeController@store');
            Route::post('/update', 'backend\UserDocTypeController@update');
            Route::get('/getallSoft', 'backend\UserDocTypeController@getAllSoft');
            Route::post('/delete', 'backend\UserDocTypeController@delete');
            Route::any('/softdelete', 'backend\UserDocTypeController@softDelete');
            Route::any('/active', 'backend\UserDocTypeController@active');
            Route::any('/active/multiple', 'backend\UserDocTypeController@activeMultiple');
            Route::post('/delete/multiple', 'backend\UserDocTypeController@deleteMultiple');
            Route::post('/delete/soft/multiple', 'backend\UserDocTypeController@deleteMultipleSOft');
            Route::post('/exportPdf', 'backend\UserDocTypeController@exportPdf');
            Route::post('/exportWord', 'backend\UserDocTypeController@exportWord');
            Route::post('/exportExcel', 'backend\UserDocTypeController@exportExcel');
        });
        //relationship
        Route::group(['prefix' => '/relationship'], function () {
            Route::get('/add', 'backend\RelationShipController@relationship');
            Route::get('/getall', 'backend\RelationShipController@getAll');
            Route::get('/store', 'backend\RelationShipController@store');
            Route::post('/update', 'backend\RelationShipController@update');
            Route::post('/delete', 'backend\RelationShipController@delete');
            Route::get('/getallSoft', 'backend\RelationShipController@getAllSoft');
            Route::any('/softdelete', 'backend\RelationShipController@softDelete');
            Route::any('/active', 'backend\RelationShipController@restore');
            Route::any('/delete/multiple', 'backend\RelationShipController@deleteMultiple');
            Route::any('/delete/soft/multiple', 'backend\RelationShipController@deleteMultipleSOft');
            Route::any('/active/multiple', 'backend\RelationShipController@activeMultiple');
            Route::post('/export/world', 'backend\RelationShipController@exportWorld');
            Route::post('/export/pdf', 'backend\RelationShipController@exportPdf');
            Route::post('/export/excel', 'backend\RelationShipController@exportExcel');
        });
        //required_documents
        Route::get('/required_documents', 'backend\RequiredDocuments@index');
        // license
        Route::group(['prefix' => '/licence'], function () {
            Route::get('/', 'backend\LicenceController@index');
            Route::get('/getall', 'backend\LicenceController@getAll');
            Route::any('/store', 'backend\LicenceController@store');
            Route::post('/edit', 'backend\LicenceController@edit');
            Route::post('/update', 'backend\LicenceController@update');
            Route::post('/delete', 'backend\LicenceController@delete');
            Route::get('/getallSoft', 'backend\LicenceController@getAllSoft');
            Route::any('/softdelete', 'backend\LicenceController@softDelete');
            Route::any('/active', 'backend\LicenceController@active');
            Route::any('/delete/multiple', 'backend\LicenceController@deleteMultiple');
            Route::any('/delete/soft/multiple', 'backend\LicenceController@deleteMultipleSOft');
            Route::any('/active/multiple', 'backend\LicenceController@activeMultiple');
            Route::post('/export/world', 'backend\LicenceController@exportWorld');
            Route::post('/export/pdf', 'backend\LicenceController@exportPdf');
            Route::post('/export/excel', 'backend\LicenceController@exportExcel');
        });
        Route::group(['prefix' => '/user/document'], function () {
            Route::get('/', 'backend\UploadDocumentController@index');
            Route::get('/getall', 'backend\UploadDocumentController@getAll');
            Route::post('/store', 'backend\UploadDocumentController@store');
            Route::post('/edit', 'backend\UploadDocumentController@edit');
            Route::post('/update', 'backend\UploadDocumentController@update');
            Route::post('/delete', 'backend\UploadDocumentController@delete');
            Route::get('/getallSoft', 'backend\UploadDocumentController@getAllSoft');
            Route::any('/softdelete', 'backend\UploadDocumentController@softDelete');
            Route::any('/active', 'backend\UploadDocumentController@active');
            Route::any('/delete/multiple', 'backend\UploadDocumentController@deleteMultiple');
            Route::get('/download/{id}', 'backend\UploadDocumentController@download');
            Route::get('/view/{id}', 'backend\UploadDocumentController@view');
            Route::get('/view-pdf/{id}', 'backend\UploadDocumentController@viewPdf');
            Route::any('/delete/soft/multiple', 'backend\UploadDocumentController@deleteMultipleSOft');
            Route::any('/active/multiple', 'backend\UploadDocumentController@activeMultiple');
            Route::post('/export/world', 'backend\UploadDocumentController@exportWorld');
            Route::post('/export/pdf', 'backend\UploadDocumentController@exportPdf');
            Route::post('/export/excel', 'backend\UploadDocumentController@exportExcel');
        });
        // contact
        Route::group(['prefix' => '/contact_infomation'], function () {
            Route::get('/', 'backend\ContactInformationController@index');
            Route::post('/update', 'backend\ContactInformationController@update');
        });
        // contact
        Route::group(['prefix' => '/completed_forms'], function () {
            Route::get('/getAllDocument', 'backend\CompleteFormsController@getAllDocument');
            Route::post('/customSearch', 'backend\CompleteFormsController@customSearch');
            Route::get('/report/{id?}', 'backend\CompleteFormsController@report');
            Route::get('/getAllReport/{id?}', 'backend\CompleteFormsController@getAllReport');
            Route::get('/getAllProject/{id?}', 'backend\CompleteFormsController@getAllProject');
            Route::get('/getAllUser', 'backend\CompleteFormsController@getAllUser');
            Route::get('/getAllSignedDocument','backend\CompleteFormsController@getAllSignedDocument');
            Route::get('/getAllSignedReport/{id?}','backend\CompleteFormsController@getAllSignedReport');
            Route::get('/getAllFilterReport/{id?}/{fl}','backend\CompleteFormsController@getAllFilterReport');
            Route::get('/getAllFilterProject/{id?}/{fl}','backend\CompleteFormsController@getAllFilterProject');
            Route::get('/getAllFilterUser/{id?}/{fl}','backend\CompleteFormsController@getAllFilterUser');
            Route::get('/getAllFilterDocument/{id?}/{fl}','backend\CompleteFormsController@getAllFilterDocument');
            Route::get('/getAllSignedProject/{id?}','backend\CompleteFormsController@getAllSignedProject');
            Route::get('/getAllSignedUser','backend\CompleteFormsController@getAllSignedUser');
            Route::post('/signOff', 'backend\CompleteFormsController@signOff');

            Route::get('/viewDocument/{id}', 'backend\CompleteFormsController@viewDocument');
            Route::get('/viewDocument/{id}', 'backend\CompleteFormsController@viewDocument');
            Route::get('/view-pdf/{id}', 'backend\CompleteFormsController@viewpdf');
            Route::get('/get/version', 'backend\CompleteFormsController@getVersion');
            Route::get('/pdf/form/export/{id}', 'backend\CompleteFormsController@exportVersionPdf_view');
        });
        // account
        Route::group(['prefix' => '/account'], function () {
            Route::get('/', 'backend\AccountController@index');
            Route::post('/update', 'backend\AccountController@update');
        });
        // staff in employee
        Route::group(['prefix' => '/staff'], function () {
            Route::get('/', 'backend\StaffController@index');
            Route::post('/role/update', 'backend\StaffController@roleUpdate');
            Route::post('/update', 'backend\StaffController@staffUpdate');
            Route::post('group/update', 'backend\StaffController@staffGroupUpdate');
        });
        //location in employee
        Route::group(['prefix' => '/location'], function () {
            Route::get('/', 'backend\LocationController@index');
            Route::post('/group/update', 'backend\LocationController@groupUpdate');
            Route::post('/update', 'backend\LocationController@locationUpdate');
        });
        //folder files permision in employee
        Route::group(['prefix' => '/employee/documents'], function () {
            Route::get('/', 'backend\FileController@index');
            Route::post('/folder/update', 'backend\FileController@folerUpdate');
            Route::post('/file/update', 'backend\FileController@fileUpdate');
        });
    });
    Route::get('/language/change/to/{locale}', function ($locale) {
        Session::put('locale', $locale);
        App::setLocale($locale);
        return Redirect::back();
    });
});
