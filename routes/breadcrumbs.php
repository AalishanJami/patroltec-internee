<?php
// project/completed_forms
Breadcrumbs::for('project/completed_forms', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('project / Forms ', url('project/completed_forms'));
});

Breadcrumbs::for('Permissions', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('Permissions ', url('permissions'));
});

//

Breadcrumbs::for('completed/Forms', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('Completed / Forms ', url('completed/forms'));
});
//completed/list
Breadcrumbs::for('completed/list', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push(' Completed / List ', url('completed/list'));
});

//employee/detail
Breadcrumbs::for('statics/forms', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('Statics / Forms ', url('statics/forms'));
});
//employee/detail
Breadcrumbs::for('employee/detail', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('Employee / Detail', url('employee/detail'));
});
//client/detail
Breadcrumbs::for('client/detail', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('Client / Detail', url('client/details'));
});
//login/cms
Breadcrumbs::for('login/cms', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('Login / CMS', url('login/cms'));
});
//staff/
Breadcrumbs::for('staff/group', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('Staff / Group', url('staff/group'));
});
//staff
Breadcrumbs::for('staff', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('Staff', url('staff'));
});
//job
Breadcrumbs::for('job', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('Job', url('job'));
});
//app/location
Breadcrumbs::for('app/location', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('App / Location', url('app/location'));
});
//panic/alarm
Breadcrumbs::for('panic/alarm', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('Panic / Alarm', url('panic/alaram'));
});
//Version/log/edit
Breadcrumbs::for('Version/log/edit', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('Version / Log / Edit', url('version/log/edit'));
});
//Version/log
Breadcrumbs::for('Version/log', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('Version / Log', url('version/log'));
});
Breadcrumbs::for('Job/Complete', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('Job / Complete', url('Job/Complete'));
});
//employee/notes
Breadcrumbs::for('employee/notes', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('Employee / Notes', url('employee/notes'));
});
//bulk/import
Breadcrumbs::for('bulk/import', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('Bulk / Import', url('bulk/upload'));
});
//schedule
Breadcrumbs::for('schedule', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('Schedule', url('schedule'));
});
//document/notes
Breadcrumbs::for('document/notes', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('Document / Notes', url('document/notes'));
});
Breadcrumbs::for('document/detail', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('Document / detail', url('document/detail'));
});
//document/folder
Breadcrumbs::for('document/folder', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('Document / Folder', url('document/folder'));
});
Breadcrumbs::for('document/forms', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('Document / Forms', url('document/forms'));
});
Breadcrumbs::for('document/completed_forms', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('Document / Completed Forms', url('document/completed_forms'));
});
//document
Breadcrumbs::for('document', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('Document', url('document'));
});
//account
Breadcrumbs::for('account', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('Account', url('account'));
});
//relationship
Breadcrumbs::for('relationship', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('Relation Ship', url('relationship'));
});
//location
Breadcrumbs::for('location', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('Location ', url('location'));
});
//location_group
Breadcrumbs::for('location_group', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('Location / Group', url('location_group'));
});
//employee
Breadcrumbs::for('employee', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('Employee', url('employee'));
});
//sia
Breadcrumbs::for('sia', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('SIA', url('sia'));
});
//clocks
Breadcrumbs::for('clocks', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('Clocks', url('clocks'));
});
//fors
Breadcrumbs::for('fors', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('Fors', url('fors'));
});
//support
Breadcrumbs::for('support', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('Support', url('support'));
});
//email
Breadcrumbs::for('emails', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('Emails', url('emails'));
});
//question
Breadcrumbs::for('question', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('Question', url('question'));
});
//answer
Breadcrumbs::for('answer', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('Answer', url('answer'));
});
//cms/web
Breadcrumbs::for('cms/web', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('CMS / Web', url('cms/web'));
});
//client/notest
Breadcrumbs::for('cms/app', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('CMS / APP', url('cms/app'));
});

//client/notest
Breadcrumbs::for('client/notes', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('Client / Notes', url('client/notes'));
});
//client/upload
Breadcrumbs::for('client/upload', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('Client / Upload', url('client/upload'));
});

//qr
Breadcrumbs::for('qr', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('QR', url('qr'));
});

//contact_type
Breadcrumbs::for('contact_type', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('contact type', url('contact_type'));
});
//site_groups
Breadcrumbs::for('site_groups', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('site groups', url('site_groups'));
});

//customers
Breadcrumbs::for('customers', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('customers', url('customers'));
});
//divisions
Breadcrumbs::for('divisions', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('divisions', url('divisions'));
});
//contractor
Breadcrumbs::for('contractor', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('contractor', url('contractor'));
});

//plant_and_equipment
Breadcrumbs::for('plant_and_equipment', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('Plant and Equipment', url('plant_and_equipment'));
});


//plant_and_equipment
Breadcrumbs::for('qr_show', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('QR', url('qr/show'));
});


//contact
Breadcrumbs::for('contact', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('Contact', url('contact'));
});

//address
Breadcrumbs::for('address', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('Address', url('address'));
});

//bonus/score
Breadcrumbs::for('bonus/score', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('Bonus / Score', url('bonus/score'));

});
//
Breadcrumbs::for('detail', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('Project / Detail', url('project/detail'));
});

//project
Breadcrumbs::for('project', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('project', url('project'));
});
//clients
Breadcrumbs::for('client', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('clients', url('clients'));
});
// audit
Breadcrumbs::for('audit', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('audit', url('audit'));
});

// Dashboard
Breadcrumbs::for('dashboard', function ($trail) {
    $trail->push('Dashboard', route('dashboard'));
});

// Home > Manage Users
Breadcrumbs::for('manageusers', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('Manage  Users', url('users'));
});
//
// Home > Blog
Breadcrumbs::for('companies', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('Manage Companies', url('company'));
});
//

Breadcrumbs::for('required_documents', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('Required Documents ', url('required_documents'));
});

Breadcrumbs::for('view_register', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('View Register ', url('view_register'));
});

Breadcrumbs::for('training_matrix', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('Training Matrix ', url('training_matrix'));
});

Breadcrumbs::for('training_matrix/details', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('Training Matrix / Details', url('training_matrix/details'));

});

Breadcrumbs::for('training_matrix/notes', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('Training Matrix / Notes', url('training_matrix/notes'));

});

Breadcrumbs::for('training_matrix/documents', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('Training Matrix / Documents', url('training_matrix/documents'));

});

Breadcrumbs::for('clear_notices', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push(' Clear Up Notices ', url('clear_notices'));
});

Breadcrumbs::for('action', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push(' Action ', url('action'));
});



// Home > Blog
Breadcrumbs::for('roles', function ($trail) {
    // $trail->parent('dashboard');
    $trail->push('Manage Roles', url('roles'));
});
//


//// Home > Blog > [Category]
//Breadcrumbs::for('category', function ($trail, $category) {
//    $trail->parent('blog');
//    $trail->push($category->title, route('category', $category->id));
//});
//
//// Home > Blog > [Category] > [Post]
//Breadcrumbs::for('post', function ($trail, $post) {
//    $trail->parent('category', $post->category);
//    $trail->push($post->title, route('post', $post->id));
//});
