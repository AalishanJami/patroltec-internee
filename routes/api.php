<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/

Route::post('/login', 'Api\UserAPIController@login');
Route::post('/register', 'Api\UserAPIController@register');
Route::post('/updateProfile', 'Api\UserAPIController@updateProfile');
Route::post('/contactUs', 'Api\UserAPIController@contactUs');
Route::post('/forgetPassword', 'Api\UserAPIController@forgetPassword');
Route::post('/verifyForgetPassword', 'Api\UserAPIController@verifyForgetPassword');
Route::post('/changePassword', 'Api\UserAPIController@changePassword');

Route::post('/createSharedUser', 'Api\UserAPIController@createSharedUser');
Route::post('/updateSharedUser', 'Api\UserAPIController@updateSharedUser');
Route::get('/getMySharedUsers/{user_id}', 'Api\UserAPIController@getMySharedUsers');
Route::post('/resendSharedUserEmail', 'Api\UserAPIController@resendSharedUserEmail');

Route::any('/refresh/{user_id}', 'Api\UserAPIController@refresh');

Route::post('/getProjects', 'Api\JobsAPIController@getProjects');
Route::get('/getForms/{id}', 'Api\JobsAPIController@getForms');
Route::post('/getEquipments', 'Api\JobsAPIController@getEquipments');

Route::any('/getJobs', 'Api\JobsAPIController@getJobs');
Route::get('/getStaticForm/{id}', 'Api\JobsAPIController@getStaticForm');
Route::post('/raiseJob', 'Api\JobsAPIController@raiseJob');
Route::get('/getJobsToWrite/{string}/{id}', 'Api\JobsAPIController@getJobsToWrite');

Route::post('/completeJob', 'Api\CompleteJobAPIController@completeJob');
Route::post('/completeJobFiles', 'Api\CompleteJobAPIController@completeJobFiles');
Route::post('/autoSubmit', 'Api\CompleteJobAPIController@autoSubmit');

Route::get('/contact/{id}', 'Api\ConfigsAPIController@contact');
Route::get('/faq/{id}', 'Api\ConfigsAPIController@faq');
Route::get('/populate/cms', 'Api\ConfigsAPIController@populateCms');
Route::get('/populate/permissions', 'Api\ConfigsAPIController@setCompanyPermissions');

Route::post('/location', 'Api\LocationAPIController@get');
Route::post('/location/update', 'Api\LocationAPIController@update');
Route::post('/location/create', 'Api\LocationAPIController@create');

Route::get('/forms/{user_id}/{company_id}', 'Api\FormsAPIController@getForms');
Route::get('/forms/completed/{user_id}/{company_id}', 'Api\FormsAPIController@getFormsCompleted');
Route::get('/formSection/{form_id}', 'Api\FormsAPIController@getFormSections');
Route::get('/form-export/{form_id}', 'Api\FormsAPIController@exportForm');
Route::get('/form-download/{form_id}', 'Api\FormsAPIController@downloadForm');
Route::get('/forms/replicate/create/{form_section_id}/{job_group_id}', 'Api\FormsAPIController@createRepeatableSection');
Route::get('/forms/replicate/get/{form_section_id}/{form_id}', 'Api\FormsAPIController@getRepeatableSection');
Route::get('/forms/export/sections/{form_id}', 'Api\FormsAPIController@getSectionsToExport');

Route::post('/performPayment', 'Api\PaymentsAPIController@performPayment');
Route::get('/cancel-sub/{user_id}', 'Api\PaymentsAPIController@cancelSub');
