<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppPermissionModule extends Model
{
    protected $fillable = [
        'name', 'active', 'deleted'
    ];

    protected $table = 'app_permissions_module_names';

    public function appPermissionModuleId() {
        return $this->hasMany('App\AppPermissions','app_permission_module_id','id');
    }
}
