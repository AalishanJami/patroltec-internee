<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrePopulate extends Model
{
   protected $fillable = [
        'name', 'form_id', 'version_id'
    ];
}
