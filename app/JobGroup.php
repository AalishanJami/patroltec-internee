<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobGroup extends Model
{
    protected $fillable = [
//        'company_id', 'title','user_id','form_id','location_id','location_breakdown_id','contractor_id','user_group_id','form_type_id','schedule_id','schedule_id_repeat_no','title','schedule_date','service_id','details','tag','tag_job','sig','completed','dashboard','hide','file_name','notes','sign_off','sign_off_reason','delete_reason','date_time'
        'company_id', 'title','user_id','form_id','location_id','location_breakdown_id','contractor_id','user_group_id','form_type_id','schedule_id','schedule_id_repeat_no','title','schedule_date','service_id','details','sig','completed','dashboard','hide','file_name','notes','sign_off','sign_off_reason','delete_reason','date_time','cost_info','total_cost'
    ];

    public function users(){
        return $this->hasOne('App\User','id','user_id');
    }

    public function forms(){
        return $this->hasOne('App\Forms','id','form_id');
    }

    public function sForms(){
        return $this->hasOne('App\Forms','id','form_id');
    }
    public function formTypes(){
        return $this->hasOne('App\FormType','id','form_type_id');
    }
    public function jobs() {
        return $this->hasMany('App\Jobs','job_group_id','id');
    }   public function responseGroups() {
    return $this->hasMany('App\ResponseGroups','job_group_id','id');
}
    public function cJobs() {
        return $this->hasMany('App\Jobs','job_group_id','id')->where('completed',2);
    }

    public function nJobs() {
        return $this->hasMany('App\Jobs','job_group_id','id')->where('completed',0);
    }
    public function locations(){
        return $this->hasOne('App\Location','id','location_id');
    }

    public function locationBreakdowns(){
        return $this->hasOne('App\LocationBreakdown','id','location_breakdown_id');
    }

    public function form_section(){
        return $this->hasOne('App\FormSections','id','form_section_id')->with('questions.answer_group.answer');

    }
}
