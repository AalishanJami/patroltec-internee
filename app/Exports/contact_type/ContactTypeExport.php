<?php
namespace App\Exports\contact_type;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ContactTypeExport implements FromView
{
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function view(): View
    {
        return view('backend.contact_type.excel', [
            'data' => $this->data
        ]);
    }
}
