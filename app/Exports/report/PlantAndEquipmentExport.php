<?php

namespace App\Exports\report;

use App\ScheduleTask;
use Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class PlantAndEquipmentExport implements FromView
{
    private $data;

    public function __construct($data)
    {

        $this->data = $data;
    }

    public function view(): View
    {
        return view('backend.reports.export.plantandequipment.excel', [
            'data' => $this->data
        ]);
    }
}
