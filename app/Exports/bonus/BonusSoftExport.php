<?php

namespace App\Exports\bonus;

use App\BonusScore;
use Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;


class BonusSoftExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function headings(): array
    {
        return [
            'Month',
            'year',
            'Bonus Score',
            'Notes',
            ];
       
    }

    public function collection()
    {   
        return BonusScore::where('active',0)->where('deleted',0)->get([
            'month','year','bonus_score','notes',
            ]);
        
        
    }
}
