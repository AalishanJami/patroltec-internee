<?php
namespace App\Exports\bonus;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class BonusExportCustom implements FromView
{
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function view(): View
    {
        return view('backend.bonus_score.xml', [
            'data' => $this->data
        ]);
    }
}