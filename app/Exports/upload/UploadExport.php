<?php

namespace App\Exports\upload;

use App\Upload;
use Auth;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
class UploadExport implements  FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */

    private $data;
    private $flag;

    public function __construct($data,$flag)
    {
        $this->data = $data;
        $this->flag = $flag;
    }

    public function view(): View
    {
        return view('backend.bulk_upload.excel', [
            'data' => $this->data,
            'flag' => $this->flag
        ]);
    }
}
