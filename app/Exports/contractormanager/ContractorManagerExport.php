<?php
namespace App\Exports\contractormanager;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
class ContractorManagerExport implements FromView
{
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function view(): View
    {
        return view('backend.user.contractormanager.excel', [
            'data' => $this->data
        ]);
    }
}
