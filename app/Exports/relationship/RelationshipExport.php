<?php
namespace App\Exports\relationship;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class RelationshipExport implements FromView
{
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function view(): View
    {
        return view('backend.relationship.excel', [
            'data' => $this->data
        ]);
    }
}
