<?php
namespace App\Exports\contractor;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
class ContractorExport implements FromView
{
    private $data;

    public function __construct($data)
    {

        $this->data = $data;
    }

    public function view(): View
    {
        return view('backend.contractor.excel', [
            'data' => $this->data
        ]);
    }
}
