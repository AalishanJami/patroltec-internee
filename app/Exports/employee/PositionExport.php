<?php
namespace App\Exports\employee;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use App\Position;
class PositionExport implements FromView
{
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function view(): View
    {
        return view('backend.employee.export.position_excel', [
            'data' => $this->data
        ]);
    }
}
