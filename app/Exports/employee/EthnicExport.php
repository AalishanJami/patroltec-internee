<?php
namespace App\Exports\employee;

use App\EthinicOrigins;
use App\SexualOrientations;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
class EthnicExport implements FromView
{
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function view(): View
    {
        return view('backend.employee.export.ethinic_excel', [
            'data' => $this->data
        ]);
    }
}
