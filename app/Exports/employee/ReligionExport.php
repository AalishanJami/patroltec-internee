<?php
namespace App\Exports\employee;

use App\Religions;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
class ReligionExport implements FromView
{
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function view(): View
    {
        return view('backend.employee.export.religion_excel', [
            'data' => $this->data
        ]);
    }
}
