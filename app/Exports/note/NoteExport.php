<?php

namespace App\Exports\note;

use App\Notes;
use Auth;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;



class NoteExport implements  FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */

    private $data;
    private $flag;

    public function __construct($data,$flag)
    {
        $this->data = $data;
        $this->flag = $flag;
    }

    public function view(): View
    {
        return view('backend.notes.excel', [
            'data' => $this->data,
            'flag' => $this->flag
        ]);
    }
}
