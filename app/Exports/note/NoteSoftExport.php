<?php

namespace App\Exports\note;

use App\LocationAddress;
use Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;


class NoteSoftExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function headings(): array
    {
        return ['title', 'note'];
    }
    public function collection()
    {
        return LocationAddress::where('active',0)->where('deleted', '=',0)->get(['title', 'note']);
    }
}
