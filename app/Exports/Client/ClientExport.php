<?php


namespace App\Exports\client;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
class ClientExport implements FromView
{
    private $data;

    public function __construct($data)
    {

        $this->data = $data;
    }

    public function view(): View
    {
        return view('backend.employee.export.client_excel', [
            'data' => $this->data
        ]);
    }
}
