<?php

namespace App\Exports\project;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;


class ProjectExport implements FromView
{
    private $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

    public function view(): View
    {
        return view('backend.project.excel', [
            'data' => $this->data
        ]);
    }
}

