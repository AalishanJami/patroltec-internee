<?php

namespace App\Exports\project;

use App\Location;
use Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Divisions;
use App\Customers;
use App\SiteGroups;
use App\User;
use App\Clients;
use App\Contractor;
use App\Company;


class ProjectExportSoft implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function headings(): array
    {
        return [
        'name',
        'division_id',
        'customer_id',
        'site_group_id',
        'manager_id',
        'contract_manager_id',
        'client_id',
        'contract_end',
            ];
       
    }

    public function collection()
    {   
        $data =  Location::where('active',1)->get([
        'name',
        'division_id',
        'customer_id',
        'site_group_id',
        'manager_id',
        'contract_manager_id',
        'client_id',
        'contract_end',
            ]);
        //dd($data);
        foreach ($data as $key => $value) {
        
        $data[$key]->division_id = Divisions::where('id',$data[$key]->division_id)->get()->last()->name;
        $data[$key]->customer_id = Customers::where('id',$data[$key]->customer_id)->get()->last()->name;
        $data[$key]->site_group_id = SiteGroups::where('id',$data[$key]->site_group_id)->get()->last()->name;
        $data[$key]->manager_id = User::where('id',$data[$key]->manager_id)->get()->last()->name;
        $data[$key]->contract_manager_id = User::where('id',$data[$key]->contract_manager_id)->get()->last()->name;
        $data[$key]->client_id = Clients::where('id',$data[$key]->client_id)->get()->last()->first_name;
        }
        return $data;

        
        
    }
}
