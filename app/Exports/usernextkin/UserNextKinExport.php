<?php
namespace App\Exports\usernextkin;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class UserNextKinExport implements FromView
{
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function view(): View
    {
        return view('backend.nextkin.xml', [
            'data' => $this->data
        ]);
    }
}
