<?php

namespace App\Exports;

use App\User;
use App\CompanyUser;
use Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;


class UsersExportSoft implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function headings(): array
    {
        if(Auth::user()->all_companies == 1)
        {
             return [
            'id',
            'name',
            'email',
            ];
        }
        else
        {
            if(Auth()->user()->hasPermissionTo('user_name') && Auth()->user()->hasPermissionTo('user_email'))
            {
                return [
                'id',
                'name',
                'email',
                ];
            }
            elseif(Auth()->user()->hasPermissionTo('user_name'))
            {
                 return [
                'id',
                'name'
                ];
            }elseif(Auth()->user()->hasPermissionTo('user_email'))
            {
                return [
                'id',
                'email',
                ];
            }
            else
            {
                return [];
            }
        }
       
    }

    public function collection()
    {
        if(Auth::user()->all_companies == 1)
        {
            return User::where('active',0)->get(['id','name','email']);
        }
        else
        {
            $temp = collect([]);
            $data=CompanyUser::where('user_id',Auth::user()->id)->get();
                foreach ($data as $key => $value) {
                     if(Auth()->user()->hasPermissionTo('user_name') && Auth()->user()->hasPermissionTo('user_email'))
                    {
                        $temp_data=CompanyUser::where('company_id',$value->company_id)->with(array('user'=>function($query){
                        $query->select('id','name','email')->where('active', '=',0);
                        }))->get();
                    }
                    elseif(Auth()->user()->hasPermissionTo('user_name'))
                    {
                        $temp_data=CompanyUser::where('company_id',$value->company_id)->with(array('user'=>function($query){
                        $query->select('id','name')->where('active', '=',0);
                        }))->get();
                    }elseif(Auth()->user()->hasPermissionTo('user_email'))
                    {
                        $temp_data=CompanyUser::where('company_id',$value->company_id)->with(array('user'=>function($query){
                        $query->select('id','email')->where('active', '=',0);
                        }))->get();
                    }
                    else
                    {
                         $temp_data=[];
                    }
                     foreach ($temp_data as $index) {
                        foreach ($index->user as $index_1) {  
                                $temp->push($index_1);
                        }
                     }
                }
                $temp=$temp->unique();
                return $temp;
                
        }
    }
}
