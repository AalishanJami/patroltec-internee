<?php
namespace App\Exports\employee_document;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class EmployeeDocumentExport implements FromView
{
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function view(): View
    {
        return view('backend.employee_document.excel', [
            'data' => $this->data
        ]);
    }
}
