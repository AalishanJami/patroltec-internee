<?php
namespace App\Exports\division;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
class DivisionExport implements FromView
{
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function view(): View
    {
        return view('backend.divisions.excel', [
            'data' => $this->data
        ]);
    }
}
