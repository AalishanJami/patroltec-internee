<?php

namespace App\Exports\support;

use App\ScheduleTask;
use Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class SupportExport implements FromView
{
    private $data;

    public function __construct($data)
    {

        $this->data = $data;
    }

    public function view(): View
    {
        return view('backend.support.excel', [
            'data' => $this->data
        ]);
    }
}
