<?php

namespace App\Exports\tasktype;

use App\ScheduleTask;
use Auth;
use App\TaskType;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class TaskTypeExport implements FromView
{
    private $data;

    public function __construct($data)
    {

        $this->data = $data;
    }

    public function view(): View
    {
        return view('backend.schedule.model.excel', [
            'data' => $this->data
        ]);
    }
}
