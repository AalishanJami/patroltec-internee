<?php
namespace App\Exports\site_group;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
class SiteGroupExport implements FromView
{
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function view(): View
    {
        return view('backend.site_groups.excel', [
            'data' => $this->data
        ]);
    }
}
