<?php

namespace App\Exports\qr;

use App\LocationBreakdown;
use Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;


class QRSoftExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function headings(): array
    {
        return [
            'Name',
            'Directions',
            'Information',
            'Notes',
            ];
       
    }

    public function collection()
    {   
        return LocationBreakdown::where('type','t')->where('active',0)->where('deleted',0)->get([
            'name', 'directions', 'information','tag',
            ]);
        
        
    }
}
