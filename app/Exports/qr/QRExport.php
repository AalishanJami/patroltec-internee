<?php

namespace App\Exports\qr;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class QRExport implements  FromView
{
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function view(): View
    {
        return view('backend.qr.xml', [
            'data' => $this->data
        ]);
    }
}
