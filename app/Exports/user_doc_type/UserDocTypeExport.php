<?php
namespace App\Exports\user_doc_type;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
class UserDocTypeExport implements FromView
{
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function view(): View
    {
        return view('backend.user_doc_type.excel', [
            'data' => $this->data
        ]);
    }
}
