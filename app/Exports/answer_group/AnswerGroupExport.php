<?php
namespace App\Exports\answer_group;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class AnswerGroupExport implements FromView
{
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function view(): View
    {
        return view('backend.document.answer_group.excel', [
            'data' => $this->data
        ]);
    }
}
