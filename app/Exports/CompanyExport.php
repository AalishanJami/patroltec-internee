<?php

namespace App\Exports;

use App\User;
use App\CompanyUser;
use App\Company;
use Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;


class CompanyExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */


    public function headings(): array
    {
        if(Auth::user()->all_companies == 1)
        {
            return [
            'id',
            'name',
            ];
        }
        else
        {
            if(Auth()->user()->hasPermissionTo('company_name'))
            {
               return [
                'id',
                'name',
                ];
            }
            else
            {
                return [
                    'id',
                    ];
                
            } 
        }

         
    }

    public function collection()
    {
        if(Auth::user()->all_companies == 1)
        {
            return Company::where('active',1)->get(['id','name']);
        }
        else
        {
            if(Auth()->user()->hasPermissionTo('company_name'))
            {
                $companies= Company::where('active', '=',1)->get(['id','name']);
                return $companies;
            }
            else
            {
                $companies = collect([]);
                return $companies;
                
            }  
        }

    }   
}
