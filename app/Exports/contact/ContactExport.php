<?php

namespace App\Exports\contact;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;


class ContactExport implements  FromView
{
     private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function view(): View
    {
        return view('backend.contact.xml', [
            'data' => $this->data
        ]);
    }
}
