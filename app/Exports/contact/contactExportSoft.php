<?php

namespace App\Exports\contact;

use App\SiteContacts;
use Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;


class ContactExportSoft implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function headings(): array
    {
        return [

        'title',
        'name',
        'first_name',
        'surname',
        'phone_number',
        'extension',
        'mobile_number',
        'other_number',
        'address_street',
        'email',
        'town',
        'state',
        'post_code',
            ];

    }

    public function collection()
    {
        return SiteContacts::where('active',0)->where('deleted',0)->get([
        'title',
        'name',
        'first_name',
        'surname',
        'phone_number',
        'extension',
        'mobile_number',
        'other_number',
        'address_street',
        'email',
        'town',
        'state',
        'post_code',
            ]);


    }
}
