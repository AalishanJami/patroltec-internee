<?php
namespace App\Exports\doc_type;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
class DocTypeExport implements FromView
{
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function view(): View
    {
        return view('backend.doc_type.excel', [
            'data' => $this->data
        ]);
    }
}
