<?php
namespace App\Exports\scoring;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ScoringExport implements FromView
{
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function view(): View
    {
        return view('backend.scoring.excel', [
            'data' => $this->data
        ]);
    }
}
