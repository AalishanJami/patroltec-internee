<?php

namespace App\Exports\static_form;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;


class StaticForm implements FromView
{
    private $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

    public function view(): View
    {
        return view('backend.document.static_form.excel_export', [
            'data' => $this->data
        ]);
    }
}

