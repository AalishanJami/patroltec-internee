<?php

namespace App\Exports\LocationAddressType;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;


class LocationAddressExport implements FromView
{
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function view(): View
    {
        return view('backend.address.xml', [
            'data' => $this->data
        ]);
    }
}
