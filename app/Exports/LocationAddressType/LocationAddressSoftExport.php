<?php

namespace App\Exports\LocationAddressType;

use App\LocationAddress;
use Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;


class LocationAddressSoftExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function headings(): array
    {
        return [
        'name',
        'address_street',
        'address_village',
        'town',
        'state',
        'post_code',
        'country',
        'telephone',
        'email',
            ];
       
    }

    public function collection()
    {   
        return LocationAddress::where('active',0)->where('deleted', '=',0)->get([
        'name',
        'address_street',
        'address_village',
        'town',
        'state',
        'post_code',
        'country',
        'telephone',
        'email',
            ]);
        
        
    }
}
