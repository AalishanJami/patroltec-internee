<?php
namespace App\Exports\form_type;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class FormTypeExport implements FromView
{
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function view(): View
    {
        return view('backend.document.form_type.excel', [
            'data' => $this->data
        ]);
    }
}