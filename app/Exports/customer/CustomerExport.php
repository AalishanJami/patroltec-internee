<?php
namespace App\Exports\customer;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
class CustomerExport implements FromView
{
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function view(): View
    {
        return view('backend.customers.excel', [
            'data' => $this->data
        ]);
    }
}
