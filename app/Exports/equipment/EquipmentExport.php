<?php

namespace App\Exports\equipment;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class EquipmentExport implements FromView
{
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function view(): View
    {
        return view('backend.plant_and_equipment.xml', [
            'data' => $this->data
        ]);
    }
}

