<?php

namespace App\Exports\equipment;

use App\LocationBreakdown;
use Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;


class EquipmentSoftExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function headings(): array
    {
        return [
            'Name',
            'Equipment',
            'Serial Number',
            'Make',
            'Model',
            'Last Service Date',
            'Next service Date',
            'Condition',
            'Notes',
            ];
       
    }

    public function collection()
    {   
        return LocationBreakdown::where('type','e')->where('active',0)->where('deleted',0)->get([
            'name', 'equipment_id', 'serial_number','make','model','last_service_date','next_service_date','condition','notes',
            ]);
        
        
    }
}
