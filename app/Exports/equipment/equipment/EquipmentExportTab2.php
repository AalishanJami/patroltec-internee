<?php

namespace App\Exports\equipment\equipment;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;


class EquipmentExportTab2 implements FromView
{
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function view(): View
    {
        return view('backend.plant_and_equipment.equipment_xml', [
            'data' => $this->data
        ]);
    }
}