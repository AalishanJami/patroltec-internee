<?php

namespace App\Exports\equipment\equipment;

use App\Equipment;
use Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;


class EquipmentSoftExport2 implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function headings(): array
    {
        return [
            'Name',
            ];
       
    }

    public function collection()
    {   
        return Equipment::where('active',0)->where('deleted',0)->get([
            'name',
            ]);
        
        
    }
}
