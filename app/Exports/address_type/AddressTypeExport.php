<?php
namespace App\Exports\address_type;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class AddressTypeExport implements FromView
{
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function view(): View
    {
        return view('backend.address_type.excel', [
            'data' => $this->data
        ]);
    }
}
