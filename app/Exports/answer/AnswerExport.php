<?php
namespace App\Exports\answer;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class AnswerExport implements FromView
{
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function view(): View
    {
        return view('backend.document.answer.excel', [
            'data' => $this->data
        ]);
    }
}
