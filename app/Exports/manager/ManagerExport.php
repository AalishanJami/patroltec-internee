<?php
namespace App\Exports\manager;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
class ManagerExport implements FromView
{
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function view(): View
    {
        return view('backend.user.manager.excel', [
            'data' => $this->data
        ]);
    }
}
