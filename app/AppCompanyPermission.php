<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppCompanyPermission extends Model
{
    protected $fillable = [
        'company_id', 'app_permission_id', 'app_role_id'
    ];

    protected $table = 'app_company_permissions';

    public function permissionGroupModule() {
        return $this->belongsTo('App\AppPermissions','app_permission_id','id');
    }
}
