<?php

namespace App;
use App\RolePermission;
use Spatie\Permission\Models\Role;
use Illuminate\Database\Eloquent\Model;
use Auth;
use Session;
class CustomRole extends Role
{
	protected $table = 'roles';
    protected $fillable = [
        'company_id', 'name','guard_name',
    ];
    protected $appends = ['enable'];
    public function getEnableAttribute()
    {
    	$data=RolePermission::where('role_id',$this->id);
        if(Auth::user()->all_companies == 1)
        {
            $data=$data->where('employee_id',Session::get('employee_id'))->first();
        }
        else
        {
            $data=$data->where('employee_id',Auth::user()->id)->first();   
        }

        if($data)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
    static public function getRole($flag=null)
    {
        $role=CustomRole::where('company_id',Auth::user()->default_company)->where('deleted',0)->orderBy('name', 'asc');
        if($flag== 1)
        {
           return $role->where('active',0);
        }
        else
        {
            return $role->where('active',1);
        }
    }
    public function customPermissions($value)
    {
        return($this->belongsToMany(
            config('permission.models.permission'),
            config('permission.table_names.role_has_permissions'),
            'role_id',
            'permission_id')->where('module_name_id',$value));
    }
}
