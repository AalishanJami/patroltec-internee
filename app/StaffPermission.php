<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaffPermission extends Model
{
     protected $fillable = [
        'user_id','employee_id','company_id',
    ];
    public function staff(){
        return $this->hasOne('App\User','id','user_id');
    }
}
