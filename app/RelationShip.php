<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RelationShip extends Model
{
     protected $fillable = [
        'name', 'company_id',
    ];
}
