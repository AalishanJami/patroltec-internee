<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactsType extends Model
{
      protected $fillable = [
        'company_id', 'name','active','deleted'
    ];

    public function getContactType(){
        return $this->belongsTo('App\SiteContacts','id','contacts_type_id');
    }
}
