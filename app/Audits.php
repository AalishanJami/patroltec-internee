<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Audits extends Model
{
    protected $table = 'audits';
    protected $fillable = [
		'company_id','ip_address','user_id','user_name','page_id','page_name','tab_id','tab_name','action','db_name',
    ];
     public function user()
    {
        return $this->belongsTo(User::class, 'user_id','id');
    }
    public function audit_fields()
    {
        return $this->hasMany(AuditFields::class, 'audit_id','id');
    }
}
