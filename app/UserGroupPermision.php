<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserGroupPermision extends Model
{
    protected $fillable = [
        'user_id',
    ];
    public function staff(){
        return $this->hasOne('App\User','id','user_id');
    }
}
