<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Auth;
class UserDocType extends Model
{
    protected $fillable = [
		'position_id',
		'doc_type_id',
		'active',
		'deleted',
        'company_id',
    ];
    public function position() {
        return $this->hasOne('App\Position','id','position_id');
    }
    public function doc_type() {
        return $this->hasOne('App\DocType','id','doc_type_id')->orderBy('name', 'asc')->where('active',1)->where('deleted',0);
    }
    static public function getUserType()
    {
        return UserDocType::where('company_id',Auth::user()->default_company)->with('doc_type')->where('active',1)->where('deleted',0)->orderBy('id', 'desc');
    }
    static public function getUserTypeRequired($position_id)
    {
        $tempArray=[];
        $UserDocType=UserDocType::getUserType()->where('position_id',$position_id)->get();
        foreach ($UserDocType as $key => $value) {
            array_push($tempArray,$value->doc_type_id);
        }
        return $tempArray;
    }


}
