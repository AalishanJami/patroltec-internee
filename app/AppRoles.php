<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppRoles extends Model
{
    protected $fillable = [
        'name', 'guard_name', 'active', 'deleted'
    ];
    
    protected $table = 'app_roles';
}
