<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserNextKin extends Model
{
    protected $fillable = [
        'company_id', 'user_id', 'title','relation_ship_id','first_name','surname','email','phone_number','active','deleted'
    ];
    public function company() {
        return $this->hasOne('App\company_id','id','company_id');
    }
     public function relation_ship() {
        return $this->hasOne('App\RelationShip','id','relation_ship_id');
    }
}