<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipment extends Model 
{
    protected $fillable = [
        'company_id', 'name','active','deleted'
    ];
    public function getNameAttribute($value = null)
    {
        if(!$value) {
            return '';
        } else {
            return $value;
        }
    }
    public function equipment()
    {
        return $this->belongsTo('App\Equipment','equipment_id','id');
    }
}
