<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormScoreLocation extends Model
{

	 protected $fillable = [
        'company_id', 'form_type_id','location_id','month','year','count','count_response_positive','count_response_negative','	outstanding_count','actions_created','actions_resolved','actions_outstanding','actions_total_score','action_score','bonus_score','bonus_score','forms_required','forms_completed','	form_score','form_score_total','response_total','	response_score_total','	score','max_score'
    ];
    

  public function location() {
    	return $this->hasOne('App\Location','id','location_id');
    }

       public function formType() {
    	return $this->hasOne('App\FormType','id','form_type_id');
    }
   


}
