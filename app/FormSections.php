<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormSections extends Model
{
    protected $fillable = [
		'company_id',
		'form_id',
		'is_complete',
		'order',
		'name',
		'header',
		'footer',
		'width',
		'border_style',
		'show_on_dashboard',
		'active',
		'deleted',
		'signature',
		'shared_section_id',
		'form_section_column',
		'shared',
		'show_header',
		'show_footer',
		'header_height',
		'footer_height',
		'form_sections_height',
		'is_repeatable',
		'form_repeatable_id',
		'is_take_pic',
		'is_select_doc',
		'is_select_pic',
		'is_notes'
    ];
    public function form_version(){
        return $this->hasOne('App\FormVersion','form_section_id','id');
    }

    public function form_section() {
        return $this->belongsTo('App\FormSections','form_id','id');
    }

    public function jobs() {
        return $this->belongsTo('App\Jobs','id','form_section_id');
    }

    public function form_section_populate() {
        return $this->belongsTo('App\FormSections','form_section_id','id');
    }


    public function questions() {
        return $this->hasMany('App\Questions','section_id')->where('active',1)->where('deleted',0)->orderBy('order', 'asc');
    }

       public function questionsS() {
        return $this->hasMany('App\Questions','section_id')->where('active',1)->where('deleted',0)->orderBy('order', 'asc')->where('answer_type_id','!=',10)->where('answer_type_id','!=',11)->where('answer_type_id','!=',12)->orWhere('active',2);
    }
}
