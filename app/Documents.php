<?php

namespace App;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Documents extends Model
{
    protected $appends = ['enable'];
    protected $fillable = [
        'link_id',
        'link_table',
        'name',
        'notes',
        'expiry',
        'issue',
        'type',
        'file',
        'pdf',
        'doc_type_id'
      
    ];
    public function doc_type() {
        return $this->belongsTo('App\DocType','doc_type_id','id');
    }
    public function getEnableAttribute()
    {
        if ($this->expiry == Carbon::now()->format('jS M Y'))
        {
            return 0;

        }
        elseif (Carbon::parse($this->expiry)->lt(Carbon::now()))
        {
            return 1;

        }
        else
        {
             return 0;
        }
    }
    public function getExpiryAttribute($value = null)
    {
        return Carbon::parse($value)->format('jS M Y');
    }
    public function getIssueAttribute($value = null)
    {
        return Carbon::parse($value)->format('jS M Y');
    }
}
