<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormScoreCount extends Model
{
     protected $fillable = [
        'company_id', 'form_type_id','date','day_code','total_completed','total','active','deleted','completed_late','outstanding_jobs'
    ];

     public function formType() {
    	return $this->hasOne('App\FormType','id','form_type_id');
    }
	public function location() {
    	return $this->hasOne('App\Location','id','location_id');
    }


}
