<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiaLicence extends Model
{
	protected $fillable = [
        'company_id', 'name','active','deleted',
		'contact',
		'type',
		'licence_loc',
		'end',
		'role',
		'notes',
		'sia_number',
		'first_name',
		'surname',
		'sia_reference',
		'tracking_number',
		'user_id',
    ];
}
