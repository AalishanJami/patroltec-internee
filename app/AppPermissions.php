<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppPermissions extends Model
{
    protected $fillable = [
        'name', 'app_permission_module_id', 'group_name'
    ];
    
    protected $table = 'app_permissions';

     public function module(){
        return $this->belongsTo('App\AppPermissionModule','app_permission_module_id','id');
    }
}
