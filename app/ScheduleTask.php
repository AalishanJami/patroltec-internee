<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class ScheduleTask extends Model
{
    protected $fillable = [
		'company_id',
		'form_id',
		'task_type_id',
		'name',
		'start_date',
		'end_date',
		'time',
		'pre_time_mins',
		'post_time_mins',
		'recycle_jobs_after',
		'do_not_schedule',
        'user_id',
		'signature',
		'mon',
		'tue',
		'wed',
		'thu',
		'fri',
		'sat',
		'sun',
		'bank',
		'week_limit',
		'month_limit',
		'year_limit',
		'repeat',
		'max_display',
		'tag_swipe',
		'repeat_min_gap',
		'active','deleted','pre_time_select','post_time_select'
    ];
    public function scheduleTaskLocation() {
    	return $this->belongsTo('App\ScheduleTaskLocation','id','schedule_task_id');
        // return $this->hasMany('App\ScheduleTaskLocation','schedule_task_id','id');
    }


    public function getStartDateAttribute($value)
	{

		return Carbon::parse($value)->format('jS M Y');

	}

}
