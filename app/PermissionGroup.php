<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PermissionGroup extends Model
{
	protected $fillable = [
		'group','order'	
    ];
    public function permissionGroupModule() {
        return $this->hasMany('App\PermissionModuleName','permission_groups_id','id')->orderBy('order');
    }
    
}
