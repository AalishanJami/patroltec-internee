<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyLogo extends Model
{
	protected $fillable = [
        'company_id','logo'
    ];
}
