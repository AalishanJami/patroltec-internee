<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskType extends Model
{
    protected $fillable = [
       'name','active','deleted','company_id'
    ];
}
