<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlantEquipment extends Model
{
	protected $fillable = [
        'name', 'equipment', 'serial_number','make','model','last_service_date','next_service_date','condition','notes','active','deleted','location_id'
    ];
     public function getNameAttribute($value = null)
    {
        if(!$value)
        {
            return '';
        }
        else
        {
            return $value;
        }
    }
     public function getSerialNumberAttribute($value = null)
    {
        if(!$value)
        {
            return '';
        }
        else
        {
            return $value;
        }
    }
     public function getMakeAttribute($value = null)
    {
        if(!$value)
        {
            return '';
        }
        else
        {
            return $value;
        }
    }
     public function getModelAttribute($value = null)
    {
        if(!$value)
        {
            return '';
        }
        else
        {
            return $value;
        }
    }
     public function getConditionAttribute($value = null)
    {
        if(!$value)
        {
            return '';
        }
        else
        {
            return $value;
        }
    }
     public function getNotesAttribute($value = null)
    {
        if(!$value)
        {
            return '';
        }
        else
        {
            return $value;
        }
    }

  
}
