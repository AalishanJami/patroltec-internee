<?php

namespace App;
use Auth;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use App\StaffPermission;
use Session;
use Spatie\Permission\Exceptions\PermissionDoesNotExist;

class User extends Authenticatable 
{
    use Notifiable;
    use HasRoles;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $appends = ['enable','flag'];
    protected $fillable = [
        'name', 'email', 'password','first_name','surname','manager_id','position','system_role','sex','dob','cscs_Card_Expiry_Date','national_Insurance_number','payroll_number','cscs_Card','cscs_Card_Expiry_Date','sexual_orientation_id','ethinic_origin_id','religion_id','notes','all_companies','active','deleted','position_id','address','house_name','street','town','county','postcode','mobile','phone','client_id','visa_expiry_date','warning','default_company', 'app_role_id', 'company_id', 'remember_token', 'email_verified_at','subscription_type', 'subscription_amount', 'subscription_id', 'fp_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function hasRole($roles, string $guard = null): bool
    {
        if (is_string($roles) && false !== strpos($roles, '|')) {
            $roles = $this->convertPipeToArray($roles);
        }
        if (is_string($roles)) {
            return $guard
                ? $this->roles->where('guard_name', $guard)->contains('name', $roles)
                : $this->roles->contains('name', $roles);
        }

        if (is_int($roles)) {
            return $guard
                ? $this->roles->where('guard_name', $guard)->contains('id', $roles)
                : $this->roles->contains('id', $roles);
        }

        if ($roles instanceof Role) {
            return $this->roles->contains('id', $roles->id);
        }
        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->hasRole($role, $guard)) {
                    return true;
                }
            }
            return false;
        }
        $current_role=[];
        foreach ($this->roles as $key => $value) {
            if($value->company_id == Auth::user()->default_company )
            {
                $current_role[$key]=$value;
            }
        }
        return $roles->intersect($guard ? $current_role->where('guard_name', $guard) : $current_role)->isNotEmpty();
    }
    public function staff_permision($flag=null,$id=null){
        $staff_permision=StaffPermission::where('user_id',$this->id);
        if($flag == 1)
        {
            return $staff_permision->where('employee_id',$id)->first();
        }
        else
        {
          return $staff_permision->where('employee_id',Auth::user()->id)->first();
        }
    }
    public function user_group_permision(){
        return $this->belongsTo('App\UserGroupPermision','id','user_id');
    }
    public function getEnableAttribute()
    {
        $id=Session::get('employee_id');
        if(isset(Auth::user()->all_companies)){
            if($this->staff_permision(Auth::user()->all_companies,$id))
            {
                return 1;
            }
            else
            {
                return 0;
            }
        } else {
            return 0;
        }
    }
    public function getFlagAttribute()
    {
        if($this->user_group_permision)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
    static public function getUser($flag=null)
    {
        $data=User::with('locationUserManager')->with('client')
        ->with('employee_document.doc_type')->with('companyUser')
        ->join('company_users', 'company_users.user_id', '=', 'users.id')
        ->where('company_users.company_id',Auth::user()->default_company)
        ->where('users.deleted',0)
        ->select('users.*');
        if($flag== 1)
        {
           return $data->where('active',0); 
        }
        else
        {
            return $data->where('active',1); 
        }
    }
    public function positionData() {
        return $this->hasOne('App\Position','id','position_id');
    }
    public function employee_document() {
        return $this->hasMany('App\Documents','link_id','id');
    }
    public function companyUser() {
        return $this->hasMany('App\CompanyUser','company_id','id');
    }
    public function jobGroups() {
        return $this->hasMany('App\JobGroup','job_group_id','id');
    }

    public function company(){
        return $this->belongsTo('App\Company','company_id','id');
    }
    public function client(){
        return $this->belongsTo('App\Clients','client_id','id');
    }

    public function locationUserManager(){
        return $this->belongsTo('App\User','manager_id','id');
    }

    public function locationUserContractManager(){
        return $this->belongsTo('App\User','id','contract_manager_id');
    }
}
