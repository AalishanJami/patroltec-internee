<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Response extends Model
{
      protected $fillable = [
        'response_group_id', 'company_id','job_id','user_id','response_type', 'question_id','answer_id','form_section_id','answer_text','comments','pass','fail','score'
    ];

   // protected $casts = ['answer_text' => 'array'];


    public function responseGroups(){
        return $this->hasOne('App\ResponseGroups','id','response_group_id');
    }



}
