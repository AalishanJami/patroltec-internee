<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Religions extends Model
{
    protected $fillable=['name'];
}
