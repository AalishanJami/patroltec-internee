<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use App\FolderPermision;
use Session;
use Auth;
class FormGroups extends Model
{
    protected $appends = ['enable','filecount'];
    protected $fillable = [
        'company_id', 'form_group_id','name','order' ,'active','deleted'
    ];
    public function getFileCountAttribute($value = null)
    {
        $data=$this->files();
        return $data->count();
    }
    static public function getFromGroups($flag=null)
    {
        $form_groups=FormGroups::where('company_id',Auth::user()->default_company)->where('deleted',0)->orderBy('id', 'desc');
        if($flag== 1)
        {
           return $form_groups->where('active',0);
        }
        else
        {
            return $form_groups->where('active',1);
        }
    }
    public function files() {
        return $this->hasMany('App\Forms','form_group_id','id');
    }
    public function form_group() {
        return $this->hasOne('App\FormGroups','id','form_group_id');
    }
    public function form_group_delete() {
        return $this->hasOne('App\FormGroups','form_group_id','id')->where('active',1);
    }
    public function getNameAttribute($value = null)
	{
		return (strip_tags($value,'</span>'));
	}
    public function folder_permision($flag=null,$id=null){
        $file_permision=FolderPermision::where('form_groups_id',$this->id);
        if($flag == 1)
        {
            return $file_permision->where('employee_id',$id)->first();
        }
        else
        {
          return $file_permision->where('employee_id',Auth::user()->id)->first();
        }
    }
    public function getEnableAttribute()
    {
        $id=Session::get('employee_id');
        if($this->folder_permision(Auth::user()->all_companies,$id))
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
}

