<?php

namespace App;
use Session;
use Auth;
use App\LocationGroupPermision;
use Illuminate\Database\Eloquent\Model;

class SiteGroups extends Model
{
    protected $appends = ['enable','locationcount'];
    protected $fillable = [
        'company_id','division_id','customer_id', 'name','active','deleted'
    ];
    static public function getSiteGroups($flag=null)
    {
        $site_groups=SiteGroups::where('company_id',Auth::user()->default_company)->where('deleted',0)->orderBy('id', 'desc');
        if($flag== 1)
        {
           return $site_groups->where('active',0);
        }
        else
        {
            return $site_groups->where('active',1);
        }
    }
    public function getLocationCountAttribute($value = null)
    {
        $data=$this->location_count();
        return $data->count();
    }
    public function location_count() {
        return $this->hasMany('App\Location','site_group_id','id');
    }
    public function location(){
        return $this->belongsTo('App\SiteGroups','id','site_group_id');
    }
    public function group_permision($flag=null,$id=null){
        $location_group_permision=LocationGroupPermision::where('site_groups_id',$this->id);
        if($flag == 1)
        {
            return $location_group_permision->where('employee_id',$id)->first();
        }
        else
        {
          return $location_group_permision->where('employee_id',Auth::user()->id)->first();
        }
    }
    public function division() {
        return $this->hasOne('App\Divisions','id','division_id');
    }
    public function customer() {
        return $this->hasOne('App\Customers','id','customer_id');
    }
    public function getEnableAttribute()
    {   
        $id=Session::get('employee_id');
         if(isset(Auth::user()->all_companies)){
            if($this->group_permision(Auth::user()->all_companies,$id))
            {
            	return 1;
            }
            else
            {
            	return 0;
            }
        } else {
            return 0;
        }
    }
    public function getNameAttribute($value = null)
	{
		if(!$value)
		{
			return '';
		}
		else
		{
			return $value;
		}
	}

}
