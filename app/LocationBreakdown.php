<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocationBreakdown extends Model 
{
    protected $fillable = [
        'company_id','location_id','name','type','equipment_id', 'serial_number','make','model','last_service_date','next_service_date','condition', 'directions', 'information','tag','active','deleted',
    ];
    public function getNameAttribute($value = null)
    {
        if(!$value)
        {
            return '';
        }
        else
        {
            return $value;
        }
    }

    public function equipment()
    {
        return $this->belongsTo('App\Equipment','equipment_id','id');
    }

    public function location(){
        return $this->hasOne('App\Location','id','location_id');
    }

    public function getSerialNumberAttribute($value = null)
    {
        if(!$value)
        {
            return '';
        }
        else
        {
            return $value;
        }
    }
    public function getMakeAttribute($value = null)
    {
        if(!$value)
        {
            return '';
        }
        else
        {
            return $value;
        }
    }
    public function getModelAttribute($value = null)
    {
        if(!$value)
        {
            return '';
        }
        else
        {
            return $value;
        }
    }
    public function getConditionAttribute($value = null)
    {
        if(!$value)
        {
            return '';
        }
        else
        {
            return $value;
        }
    }
    public function getInformationAttribute($value = null)
    {
        if(!$value)
        {
            return '';
        }
        else
        {
            return $value;
        }
    }
    public function getTagAttribute($value = null)
    {
        if(!$value)
        {
            return '';
        }
        else
        {
            return $value;
        }
    }

}
