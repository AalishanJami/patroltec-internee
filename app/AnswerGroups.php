<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnswerGroups extends Model
{
	protected $appends = ['flag'];
    protected $fillable = [
		'company_id',
		'name',
		'active',
		'deleted',
        'sig_select',
    ];
    public function answer() {
    	return $this->hasMany('App\Answers','answer_group_id','id');
    }
    public function question() {
        return $this->belongsTo('App\Questions','id', 'answer_groups_id');
    }
    public function getFlagAttribute()
    {
        if($this->question)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
}
