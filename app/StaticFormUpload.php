<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaticFormUpload extends Model
{
    protected $fillable = [
		'company_id',
		'name',
		'file' ,
		'notes',
		'pdf',
		'form_id',
		'is_complete',
    ];
}
