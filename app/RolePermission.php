<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RolePermission extends Model
{
    protected $fillable = [
        'role_id','employee_id','company_id'
    ];
    public function role(){
        return $this->hasOne('App\CustomRole','id','role_id');
    }
}
