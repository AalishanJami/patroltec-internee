<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocationGroupPermision extends Model
{
    protected $fillable = [
        'site_groups_id','employee_id','company_id'
    ];
    public function site_groups(){
        return $this->hasOne('App\SiteGroups','id','site_groups_id');
    }
}
