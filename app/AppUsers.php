<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppUsers extends Model
{
    protected $fillable = [
        'name', 'email', 'active', 'deleted', 'email_verified_at', 'password'
    ];

    protected $table = 'app_user';
}
