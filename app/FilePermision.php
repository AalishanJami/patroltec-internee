<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FilePermision extends Model
{
    protected $fillable = [
        'form_id','employee_id','company_id'
    ];
    public function file(){
        return $this->hasOne('App\Forms','id','form_id');
    }
}
