<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormType extends Model
{
     protected $fillable = [
		'company_id',
		'name',
		'_show_dashboard',
		'count_dashboard',
		'count_day',
		'count_response_positive',
		'count_response_negative',
		'outanding_count',
        'action_score',
		'late_completion_count',
		'detailed_field_stats',
		'active',
		'deleted',
    ];

     public function formSocreCount() {
        return $this->belongsTo('App\FormSocreCount','id', 'form_score_count_id');
    }


  		public function forms() {
        return $this->belongsTo('App\Forms','id', 'form_type');
    }
      public function jobGroup() {
        return $this->hasMany('App\JobGroup','form_type_id', 'id');
    }

}
