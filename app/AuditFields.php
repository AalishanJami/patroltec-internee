<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuditFields extends Model
{
    protected $fillable = [		
		'audit_id','field_id','field_name','new_value_id','new_value','old_value_id','old_value','cms_key','cms_value'		
    ];
}
