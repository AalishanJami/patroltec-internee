<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Upload extends Model 
{
   	protected $fillable = [
     	'name',
     	'link_id',
     	'link_name',
     	'oriinal_file_name',
     	'file_name',
     	'notes',
       	'status',
    ];
}
