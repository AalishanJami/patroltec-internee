<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{
     protected $fillable = [
        'company_id','division_id', 'name','active','deleted', 'app_user'
    ];

    public function division() {
        return $this->hasOne('App\Divisions','id','division_id');
    }
    public function location(){
        return $this->belongsTo('App\Customers','id','customer_id');
    }
     public function getNameAttribute($value = null)
	{
		if(!$value)
		{
			return '';
		}
		else
		{
			return $value;
		}
	}
}
