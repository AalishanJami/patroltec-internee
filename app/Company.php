<?php

namespace App;
use Auth;
use Illuminate\Database\Eloquent\Model;

class Company extends Model 
{
    protected $fillable = [
        'name','field_notes'
    ];

    public function companylogo(){
        return $this->belongsTo('App\CompanyLogo','id','company_id');
    }
    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    static public function getCompanies()
    {
        if(Auth::user()->all_companies == 1)
        {
            $companies = Company::where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $companies = Company::join('company_users','company_users.company_id', '=','companies.id')
            ->where('company_users.user_id', '=',Auth::user()->id)
            ->where('companies.active',1)->where('companies.deleted',0)
            ->select('companies.*')->get();
        }
        return $companies;
    }
    static public function getFirstCompanies()
    {
        $companies = Company::where('active',1)->where('deleted',0)->first();
        return $companies->id;
    }
    public function companyBonusScor(){
        return $this->belongsTo('App\Company','id','company_id');
    }
    public function company(){
        return $this->belongsTo('App\Company','company_id','id');
    }
    public function User(){
        return $this->belongsTo('App\User');
    }
}
