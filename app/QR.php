<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QR extends Model
{
    protected $fillable = [
        'name', 'directions', 'information','notes','active','deleted','location_id'
    ];
  
}
