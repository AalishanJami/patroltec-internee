<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResponseGroups extends Model
{
      protected $fillable = [
        'job_group_id', 'version_id','company_id','user_id','form_id', 'location_id','total_score','score','question_score_total_count','question_score_count','pass_count','fail_count','location_breakdown_id','pre_populate_id'
    ];
     public function responses() {
        return $this->hasMany('App\Response','response_group_id','id');
    }

          public function locations(){
        return $this->hasOne('App\Location','id','location_id');
    }

   public function locationBreakdowns(){
      return $this->hasOne('App\LocationBreakdown','id','location_breakdown_id');
    }
        public function users(){
        return $this->hasOne('App\User','id','user_id');
    }

         public function forms(){
        return $this->hasOne('App\Forms','id','form_id');
    }

    
}
