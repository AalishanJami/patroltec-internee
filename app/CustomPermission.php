<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomPermission extends Model
{
    protected $table = 'permissions';
}
