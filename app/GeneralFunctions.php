<?php

use App\AppCompanyPermission;
use App\Label;
use App\Location;
use App\Scoring;
use App\Audits;
use App\User;
use App\Language;
use App\UserDocType;
use App\CompanyLabel;
use App\RolePermission;
use App\AuditFields;
use App\CompanyLogo;
use App\Company;

    function appPermisionCheck($id,$role_id)
    {
        $company_id=Auth::user()->default_company;
        $permission=AppCompanyPermission::where('company_id',$company_id)->where('app_permission_id',$id)->where('app_role_id',$role_id)->first();
        if (empty($permission)){
            return " ";
        }else{
            return "checked";
        }
    }

    function permisionCheck($permision)
    {
        return Auth()->user()->hasPermissionTo('edit_employee') || Auth::user()->all_companies == 1 ;
    }
    function selectedOrNonSelected($data)
    {
        return ($data==1)?'selected':'Not selected';
    }
    function pdf_view($model,$flag=null,$type=null)
    {
        if($flag)
        {
            $temp_data=DB::table($model)->where('link_id',$flag)->where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->count();
        }
        else
        {
            if (isset($type)){
                $temp_data=DB::table($model)->where('type',$type)->where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->count();
            }else{
                $temp_data=DB::table($model)->where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->count();
            }
        }
        if($temp_data > 250)
        {
            return 'style=display:none;';
        }
        return '';
    }
    function companyImage($value)
    {
        $logo=CompanyLogo::where('company_id',$value->default_company)->first();
        if($logo)
        {
            $logo=$logo->logo;
        }
        return $logo;
    }
    function remove_key()
    {
        Session::forget('client_id');
        Session::forget('employee_id');
        Session::forget('form_id');
        Session::forget('folder_id');
        Session::forget('project_id');
    }
    function breadcrumInner($current_cms,$id,$model,$detail_url,$module,$module_url)
    {
        if(!$id)
        {
            toastr()->success('Session Expired ..!!');
            return redirect($module_url);
        }
        $data=localization();
        $breadcrumb='';
        $breadcrumb=$breadcrumb.'<ol class="breadcrumb">';
            // $breadcrumb=$breadcrumb.breadcrumPath('dashboard',$data,"/dashboard",0,0);
            $breadcrumb=$breadcrumb.breadcrumPath($module,$data,$module_url,0,0);
            $breadcrumb=$breadcrumb.breadcrumPath(getNamefromModel($id,$model),$data,$detail_url,0,1);
            $breadcrumb=$breadcrumb.breadcrumPath($current_cms,$data,'',1,0);
        $breadcrumb=$breadcrumb.'</ol>';
        return $breadcrumb;
    }
    function getNamefromModel($id,$model)
    {
        $temp=DB::table($model)->where('id',$id)->where('active',1)->where('deleted',0)->first();
        switch ($model)
        {
            case 'users':
                return $temp->first_name;
                break;
            case 'clients':
                return $temp->title;
                break;
            default:
                return $temp->name;
                break;
        }

    }
    function breadcrumPath($value,$data,$url=null,$flag,$check)
    {
        $path='';
        $path=$path.'<li class="breadcrumb-item';
        if($flag== 1)
        {
            $path=$path.' active_custom';
        }
        $path=$path.'">';
            if($url)
            {
                $path=$path.'<a href=';
                $path=$path.$url;
                $path=$path.'>';
            }
                    if($check== 1)
                    {
                        $path=$path.$value;
                    }
                    else
                    {
                        $path=$path.'<span class="assign_class label';
                        $path=$path.getKeyid($value,$data);
                        $path=$path.'" data-id="';
                        $path=$path.getKeyid($value,$data);
                        $path=$path.'" data-value="';
                        $path=$path.checkKey($value,$data);
                        $path=$path.'">';
                            $path=$path.checkKey($value,$data);
                        $path=$path.'</span>';

                    }
            if($url)
            {
                $path=$path.'</a>';
            }
        $path=$path.'</li>';
        return $path;
    }
    function scoring($value,$form_id,$location_id=null)
    {
        $tempArray=[];
        foreach ($value as $key => $index) {
            $temp=Scoring::where('location_id',$index->id)->where('company_id',Auth::user()->default_company)->where('form_id',$form_id)->first();
            if(!$temp)
            {
                array_push($tempArray,$index->id);
            }
        }
        if(isset($location_id))
        {
            array_push($tempArray,$location_id);
        }
        // dd($tempArray,$location_id);
        $locations=Location::whereIn('id',$tempArray)->where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->with('score_location')->orderBy('id', 'desc')->get();
        return $locations;
    }
    function dataPermision($model,$data=null,$relation_function=null)
    {
        $temp_permision_name=[];
        if($data || Schema::hasTable($model))
        {
            $temp_data=[];
            if(!empty($data))
            {
                $temp_data=DB::table($model)->whereIn('id',$data)->get();
            }
            foreach ($temp_data as $key => $value) {
                $temp_permision_name['permision-'.$key]=$value->name;
            }

        }
        else
        {
            $model='App\\'.$model;
            $temp_data=$model::with($relation_function)->get();
            // dd($temp_data,$model,$relation_function);
            foreach ($temp_data as $key => $value) {
                if(isset($value->$relation_function->name))
                {
                    $temp_permision_name['permision-'.$key]=$value->$relation_function->name;
                }
            }
        }
        $temp_permision_name['user_id']=Session::get('employee_id');
        return $temp_permision_name;
    }
    function months($id)
    {
        $array[1]='January';
        $array[2]='February';
        $array[3]='March';
        $array[4]='April';
        $array[5]='May';
        $array[6]='June';
        $array[7]='July ';
        $array[8]='August';
        $array[9]='September';
        $array[10]='October';
        $array[11]='November';
        $array[12]='December';
        return $array[$id];
    }
    function audit_log_data($data)
    {
        $temp_array=[];
        unset($data['old_location']);
        unset($data['id']);
        unset($data['company_id']);
        unset($data['completed']);
        unset($data['form_type_id']);
        unset($data['pdf_download']);
        unset($data['doc_download']);
        if(isset($data['show_header']))
        {
            $data['show_header']=selectedOrNonSelected($data['show_header']);
        }
        if(isset($data['show_footer']))
        {
            $data['show_footer']=selectedOrNonSelected($data['show_footer']);
        }
        if(isset($data['is_repeatable']))
        {
            $data['is_repeatable']=selectedOrNonSelected($data['is_repeatable']);
        }
        foreach ($data as $key => $value) {
            if(is_numeric($value) && $key != 'company_id')
                {
                    if($key == 'position')
                    {
                        $temp_index=[];
                        $temp_index[0]=$value;
                        if($value == 2)
                        {
                            $temp_index[1]='Manager';
                        }
                        elseif($value == 3)
                        {
                            $temp_index[1]='Admin';
                        }
                        else
                        {
                            $temp_index[1]='Staff';
                        }
                        $value=$temp_index;
                    }
                    if($key == 'month')
                    {
                        $month_value=months($value);
                        $temp_index=[];
                        $temp_index[0]=$value;
                        $temp_index[1]=$month_value;
                        $value=$temp_index;
                    }
                    if($key == 'manager_id' || $key =='contract_manager_id')
                    {
                        $model_name='users';
                    }
                    else
                    {
                        $model=explode('_id',$key);
                        $model_name=$model[0].'s';

                    }
                    $temp_value='';
                    if($key != 'position')
                    {
                        if($key == 'location_id' && $value == 0)
                        {
                            $temp_index=[];
                            $temp_index[0]=$value;
                            $temp_index[1]='All Location';
                            $value=$temp_index;
                        }
                        elseif (Schema::hasTable($model_name) ) {
                            $temp=DB::table($model_name)->where('id',$value)->first();
                            if(isset($temp))
                            {
                                if (isset($temp->first_name))
                                {
                                    $temp_value=$temp->first_name;
                                }
                                elseif(isset($temp->name))
                                {
                                    $temp_value=$temp->name;
                                }
                                else
                                {
                                   $temp_value=$value;
                                }
                            }
                            $temp_index=[];
                            $temp_index[0]=$value;
                            $temp_index[1]=$temp_value;
                            $value=$temp_index;
                        }

                    }
                }
                $temp_array[$key]=$value;
            }
        return $temp_array;
    }
    function audit_log_runtime($db_name,$page_id)
    {
        $data=DB::table($db_name)->where('id',$page_id)->first();
        $data=(array)$data;
        unset($data['active']);
        unset($data['deleted']);
        unset($data['created_at']);
        unset($data['updated_at']);
        if(isset($data['all_location']) && $data['all_location'] == 1)
        {
            $data['location_id']=0;
        }
        return audit_log_data($data);
    }
    function audit_log($cms_array=null,$update_value_db,$action,$page_name,$tab_name=null,$id=null,$old_value_db=null,$page_id=null,$tab_id=null,$db_name=null,$field_id=null,$field_name=null,$new_value_id=null,$new_value=null,$old_value_id=null,$old_value=null)
    {
        switch ($action)
        {
            case Config::get('constants.delete'):
                $old_value_db=audit_log_runtime($db_name,$page_id);
                break;
            case Config::get('constants.restore'):
                $update_value_db=audit_log_runtime($db_name,$page_id);
                break;
        }
        if($old_value_db)
        {
            // $update_value=array_diff($update_value_db,$old_value_db);
            // $old_value_db=array_intersect_key($old_value_db,$update_value);
            $update_value=$update_value_db;
        }
        else
        {
            $update_value=$update_value_db;
        }
        $ip_address=$_SERVER['REMOTE_ADDR'];
        $company_id=Auth::user()->default_company;
        $user=Auth::user();
        $array=[
            'user_id'=>$user->id,
            'user_name'=>$user->name,
            'page_id'=>$page_id,
            'page_name'=>$page_name,
            'tab_id'=>$tab_id,
            'tab_name'=>$tab_name,
            'action'=>$action,
            'ip_address'=>$ip_address,
            'company_id'=>$company_id,
            'db_name'=>$db_name,
        ];
        $audit=Audits::create($array);
        if($update_value)
        {
            $array=[];
            if(is_array($update_value))
            {
                foreach ($update_value as $key => $index)
                {
                    if($key != 'data_id' && $key != '_token' )
                    {
                        $new_value_id='';
                        $new_value=$index;
                        $old_value_id='';
                        $old_value=isset($old_value_db[$key])?$old_value_db[$key] :'' ;
                        if(is_array($index) && count($index) == 2)
                        {
                            $new_value_id=$index[0];
                            $new_value=$index[1];
                        }
                        if(isset($old_value_db[$key]) && is_array($old_value_db[$key]) && count($old_value_db[$key]) == 2)
                        {
                            $old_value_id=$old_value_db[$key][0];
                            $old_value=$old_value_db[$key][1];
                        }
                        $findme   = 'permision-';
                        $pos = strpos($key, $findme);
                        if ($pos !== false)
                        {
                            $key='permissions';
                        }
                        $findme   = 'company-';
                        $pos = strpos($key, $findme);
                        if ($pos !== false)
                        {
                            $key='company';
                        }
                        if(isset($cms_array[$key]['value']))
                        {
                            $array=[
                                'audit_id'=>$audit->id,
                                'field_id'=>empty($id)?$page_id:$id,
                                'field_name'=>$key,
                                'new_value_id'=>$new_value_id,
                                'new_value'=>$new_value,
                                'old_value_id'=>$old_value_id,
                                'old_value'=>$old_value,
                                'cms_key'=>isset($cms_array[$key]['key'])?$cms_array[$key]['key'] :'',
                                'cms_value'=>isset($cms_array[$key]['value'])?$cms_array[$key]['value'] :'',
                            ];
                            AuditFields::create($array);
                        }
                    }

                }
            }

        }
        elseif(!empty($old_value_db) && count($old_value_db)>0)
        {
            foreach ($old_value_db as $key => $index)
                {
                    if($key != 'data_id' && $key != '_token' )
                    {
                        $old_value_id='';
                        $old_value=$index;
                        $new_value_id='';
                        $new_value='' ;
                        if(is_array($index) && count($index) == 2)
                        {
                            $old_value_id=$index[0];
                            $old_value=$index[1];
                        }
                        if(isset($cms_array[$key]['value']))
                        {
                            $array=[
                                'audit_id'=>$audit->id,
                                'field_id'=>$page_id,
                                'field_name'=>$key,
                                'new_value_id'=>$new_value_id,
                                'new_value'=>$new_value,
                                'old_value_id'=>$old_value_id,
                                'old_value'=>$old_value,
                                'cms_key'=>isset($cms_array[$key]['key'])?$cms_array[$key]['key'] :'',
                                'cms_value'=>isset($cms_array[$key]['value'])?$cms_array[$key]['value'] :'',
                            ];
                            AuditFields::create($array);
                        }
                        // dd($array,$index);
                    }

                }
        }
        return 1;
    }
    function dynamic_colum($value)
    {
        $tempArray=[];
        foreach ($value as $key => $index) {
            array_push($tempArray,$index->doc_type_id);
        }
        return $tempArray;
    }
    function position($value,$index)
    {
        $position =UserDocType::where('position_id',$index['position_id'])->where('active',1)->where('deleted',0)->first();
        if($position)
        {
            $red_color='';
            foreach ($index['employee_document'] as $length => $document) {
                $doc_type_id=UserDocType::where('position_id',$index['position_id'])->where('doc_type_id',$document['doc_type_id'])->where('active',1)->where('deleted',0)->first();
                if(isset($document->doc_type->name) && $doc_type_id && $document->doc_type->name == $value->name)
                {
                    if($document->enable == 0)
                    {
                        return '<p><div   style="font-family: DejaVu Sans, sans-serif;"><span class="col_text_style" style="color:green;font-size: 22px;font-weight: bolder;"><img style="width:29px;height: :auto;" src="'.public_path('uploads/icons/').'trainingmatrix_green_check.png"></span></div></p>';
                    }
                    else
                    {
                        return '<p><div   style="font-family: DejaVu Sans, sans-serif;"><span class="col_text_style" style="color:orange;font-size: 22px;font-weight: bolder;"><img style="width:29px;height: :auto;" src="'.public_path('uploads/icons/').'trainingmatrix_orange_check.png"></span></div></p>';
                    }
                }
                else
                {
                    if($index['employee_document']->count()-1 == $length)
                    {
                        return '<p><div   style="font-family: DejaVu Sans, sans-serif;"><span class="col_text_style" style="color:red;font-size: 22px;font-weight: bolder;"><img style="width:29px;height: :auto;" src="'.public_path('uploads/icons/').'trainingmatrix_green_cros.png"></span></div></p>';

                    }
                }
            }
        }
        else
        {
            return ' ';
        }
    }
    function rolePermissionExist($array,$index)
    {
        foreach ($array as $key => $value) {
            if($value['id']==$index->id)
            {
                return 'checked';

            }
        }
        return 0;

    }
    function sidebarCount($data,$match=null,$value=null,$type=null,$type_value=null,$operator=null,$doc=null)
    {
        if($match && $value && $operator )
        {
            if($doc=='forms')
            {

            return DB::table($data)->where('company_id',Auth::user()->default_company)->where($match,$operator,$value)->where('form_id',Session::get('form_id'))->where('active',1)->where('deleted',0)->count();
            }else if($doc=='projects')
            {
                return DB::table($data)->where('company_id',Auth::user()->default_company)->where($match,$operator,$value)->where('location_id',Session::get('project_id'))->where('active',1)->where('deleted',0)->count();
            }else if($doc=='employees')
            {
                return DB::table($data)->where('company_id',Auth::user()->default_company)->where($match,$operator,$value)->where('user_id',Session::get('employee_id'))->where('active',1)->where('deleted',0)->count();
            }else
            {
                return DB::table($data)->where('company_id',Auth::user()->default_company)->where($match,$operator,$value)->where('active',1)->where('deleted',0)->count();
            }

        }
        else  if($type && $value )
        {
            return DB::table($data)->where('company_id',Auth::user()->default_company)->where($match,$value)->where($type,$type_value)->where('active',1)->where('deleted',0)->count();
        }
        else if($value || $value == 0)
        {
            if($data === 'users')
            {
                return User::getUser()->count();
            }
            elseif($data === 'companies')
            {
                return DB::table($data)->where('active',1)->where('deleted',0)->count();
            }
            else
            {
                return DB::table($data)->where('company_id',Auth::user()->default_company)->where($match,$value)->where('active',1)->where('deleted',0)->count();
            }
        }
        else
        {

            return DB::table($data)->where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->count();
        }
    }
    function sortData($data,$flag=null)
    {
        foreach ($data as $key => $value) {
            if($flag == 1)
            {
                if($value->flag == 0 && Auth::user()->all_companies != 1)
                {
                    $data->forget($key);
                }
            }
            else
            {
                if($value->enable == 0 && Auth::user()->all_companies != 1)
                {
                    $data->forget($key);
                }
            }
        }
        return $data;
    }
    function sortRoleData($data)
    {
        foreach ($data as $key => $value) {
        $flag=RolePermission::where('role_id',$value->id)->first();
            if(!$flag && Auth::user()->all_companies != 1)
            {
                $data->forget($key);
            }
        }
        return $data;
    }
	function localization()
    {
		$labels_array=[];
		if ( !\Session::has('locale') )
        {
            \Session::put('locale', \Config::get('app.locale'));
        }
        $lang=\Config::get('app.locale');
        $company_id=Auth::check() ? Auth::user()->default_company: Company::getFirstCompanies();
        $labels=labelQuery($lang,$company_id);
        $labels = $labels->isEmpty() ? setCompanyLabel($lang,$company_id) : $labels;
    	foreach ($labels as $key => $value) {
            $labels_array[$value->key_index]=$value->value.','.$value->company_labels_id;
    	}
    	return $labels_array;
    }
    function setCompanyLabel($lang,$company_id)
    {
        $labels=Label::where('lang',$lang)->with('companyLabel')->get();
        foreach ($labels as $key => $value) {
            $label_value=isset($value->companyLabel[0]) ? $value->companyLabel[0]->value : $value->key;
            CompanyLabel::create(['label_id'=>$value->id,'company_id'=>$company_id,'value'=>$label_value]);
        }
        return labelQuery($lang,$company_id);
    }
    function labelQuery($lang,$company_id)
    {
        $labels = Label::join('company_labels','company_labels.label_id', '=','labels.id')
        ->where('company_labels.company_id', '=',$company_id)
        ->where('labels.lang','=',$lang)
        ->select('labels.key as key_index','company_labels.id as company_labels_id','company_labels.value')->get();
        return $labels;
    }
    function getKeyid($value,$data)
    {
        if(isset($data[$value]))
        {
            $temp=explode(',',$data[$value]);
            return $temp[1];
        }
        else
        {
            return labelAddOnRunTime($value,2);
            // return 'dummy';
        }
    }
    function checkKey($value,$data)
    {
    	if(isset($data[$value]))
    	{
            $temp=explode(',',$data[$value]);
    		return $temp[0];
    	}
    	else
    	{
           return labelAddOnRunTime($value,1);
            // dd('dummy',$value,$data);
    		// return 'dummy';
    	}
    }
    function labelAddOnRunTime($value,$flag)
    {
        if(!empty($value) && Auth::check())
        {
            $lang=\Config::get('app.locale');
            $labels = Label::join('company_labels','company_labels.label_id', '=','labels.id')
            ->where('company_labels.company_id', '=',Auth::user()->default_company)
            ->where('labels.lang','=',$lang)
            ->where('labels.key','=',$value)
            ->select('labels.key as key_index','company_labels.id as company_labels_id','company_labels.value')->first();
            if(!$labels)
            {
                $labels=Label::create(['key'=>$value,'lang'=>$lang,'language_id'=>language(1)]);
                $company_label=CompanyLabel::create(['label_id'=>$labels->id,'company_id'=>Auth::user()->default_company,'value'=>$value]);
                switch ($flag)
                {
                    case 1:
                        return $company_label->value;
                        break;
                    case 2:
                        return $company_label->id;
                        break;
                }
            }
            else
            {
                switch ($flag)
                {
                    case 1:
                        return $labels->value;
                        break;
                    case 2:
                        return $labels->company_labels_id;
                        break;
                }
            }
        }
    }
    function selectedLanguage()
    {
        if ( !\Session::has('locale') )
        {
            \Session::put('locale', \Config::get('app.locale'));
        }
        return \Config::get('app.locale');
    }
    function language($flag=null)
    {
        // dd($flag);
    	if ( !\Session::has('locale') )
        {
            \Session::put('locale', \Config::get('app.locale'));
        }
        $lang=\Config::get('app.locale');
        foreach (getLanguage() as $key => $value) {
        	if($flag !=1 && $value->short_lan == $lang)
        	{
        		return $value->language;
        	}
            elseif($flag ==1 && $value->short_lan == $lang)
            {
                return $value->id;
            }
        }
    }
    function count_document($data)
    {
        $temp=[];
        foreach ($data->groupby('enable') as $key => $value)
        {
            if($value[0]->enable ==0)
            {
                $temp['count_active']=$value->count();
            }
            else
            {
                $temp['count_expiry']=$value->count();
            }
        }
        return $temp;
    }
    function printHtml($data)
    {
        echo $data;

    }
    function getLanguage()
    {
    	$languages=Language::where('status',1)->get();
        return  $languages;
    }
