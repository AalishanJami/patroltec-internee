<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answers extends Model
{
    protected $fillable = [
		'company_id',	
		'answer',		
		'answer_group_id',		
		'icon',	
		'grade',	
		'score',		
		'text_box',	
		'answers_type_id',	
		'active',	
		'deleted',	
    ];
}
