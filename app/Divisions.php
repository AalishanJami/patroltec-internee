<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Divisions extends Model
{
    protected $fillable = [
        'company_id', 'name','active','deleted', 'app_user'
    ];

    public function locations(){
        return $this->belongsTo('App\Location','id','division_id');
    }


    public function getNameAttribute($value = null)
	{
		if(!$value)
		{
			return '';
		}
		else
		{
			return $value;
		}
	}
}
