<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Auth;
class DocType extends Model
{
    protected $fillable = [
		'name',
		'active',
		'deleted',
		'company_id',
    ];
    static public function getDocType()
    {
    	return DocType::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('name', 'asc');
    }

    public function user_doc_type() {
        return $this->belongsTo('App\UserDocType','id','doc_type_id')->where('active',1)->where('deleted',0);
    }
}
