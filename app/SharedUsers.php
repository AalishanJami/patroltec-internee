<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SharedUsers extends Model
{
    protected $fillable = [
        'user_id', 'app_user_id'
    ];

    protected $table = 'shared_user';
}
