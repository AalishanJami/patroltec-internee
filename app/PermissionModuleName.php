<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PermissionModuleName extends Model
{
    protected $fillable = [
		'module_name','permission_groups_id','order'	
    ];
    public function permissionModule() {
        return $this->hasMany('App\CustomPermission','module_name_id','permission_groups_id');
    }
}
