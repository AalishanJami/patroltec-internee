<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Auth;
class Clients extends Model
{
	protected $fillable = [
        'title', 'email', 'first_name','surname','phone_number','address','company_id','active','deleted'
    ];
    public function getTitleAttribute($value = null)
	{
		if(!$value)
		{
			return '';
		}
		else
		{
			return $value;
		}
	}
	public function getEmailAttribute($value = null)
	{
		if(!$value)
		{
			return '';
		}
		else
		{
			return $value;
		}
	}
	 public function getNameAttribute($value = null)
	{
		if(!$value)
		{
			return '';
		}
		else
		{
			return $value;
		}
	}
	public function getFirstNameAttribute($value = null)
	{
		if(!$value)
		{
			return '';
		}
		else
		{
			return $value;
		}
	}
	public function getSurnameAttribute($value = null)
	{
		if(!$value)
		{
			return '';
		}
		else
		{
			return $value;
		}
	}
	public function getPhoneNumberAttribute($value = null)
	{
		if(!$value)
		{
			return '';
		}
		else
		{
			return $value;
		}
	}
	public function getAddressAttribute($value = null)
	{
		if(!$value)
		{
			return '';
		}
		else
		{
			return $value;
		}
	}
     public function company() {
        return $this->belongsTo('App\Company','company_id','id');
    }

    public function locationClients(){
        return $this->belongsTo('App\Clients','id','client_id');
    }
    static public function getClients($flag=null)
    {
        $data=Clients::where('company_id',Auth::user()->default_company)->where('deleted',0)->orderBy('id','desc');
        if($flag== 1)
        {
           return $data->where('active',0); 
        }
        else
        {
            return $data->where('active',1); 
        }
    }
}
