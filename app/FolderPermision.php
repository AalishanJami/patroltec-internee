<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FolderPermision extends Model
{
    protected $fillable = [
        'form_groups_id','employee_id','company_id'
    ];
    public function folder(){
        return $this->hasOne('App\FormGroups','id','form_groups_id');
    }
}
