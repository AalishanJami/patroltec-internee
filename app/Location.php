<?php

namespace App;
use Session;
use Auth;
use App\LocationPermision;
use Illuminate\Database\Eloquent\Model;
class Location extends Model
{
    protected $appends = ['enable'];
    protected $fillable = [
        'name',
        'division_id',
        'customer_id',
        'contractor_id',
        'site_group_id',
        'manager_id',
        'contract_manager_id',
        'client_id',
        'account_number',
        'termination_reason',
        'ref',
        'contract_start',
        'contract_end',
        'company_id',
    ];
    public function location_permision($flag=null,$id=null){
        $location_permision=LocationPermision::where('location_id',$this->id);
        if($flag == 1)
        {
            return $location_permision->where('employee_id',$id)->first();
        }
        else
        {
          return $location_permision->where('employee_id',Auth::user()->id)->first();
        }
    }
    static public function getLocation($flag=null)
    {
        $locations=Location::where('company_id',Auth::user()->default_company)->where('deleted',0)->orderBy('id', 'desc');
        if($flag== 1)
        {
           return $locations->where('active',0);
        }
        else
        {
            return $locations->where('active',1);
        }
    }
    public function company(){
        return $this->belongsTo('App\Company','company_id','id');
    }
    public function formScoreLocation(){
        return $this->belongsTo('App\FormScoreLocation','id','location_id');
    }
    public function score_location(){
        return $this->hasOne('App\Scoring','location_id','id');
    }
    public function formScoreCount(){
        return $this->belongsTo('App\FormScoreCount','id','location_id');
    }
    public function getEnableAttribute()
    {
        $id=Session::get('employee_id');
        if(isset(Auth::user()->all_companies)){
            if($this->location_permision(Auth::user()->all_companies,$id))
            {
                return 1;
            }
            else
            {
                return 0;
            }
        } else {
            return 0;
        }
    }
    public function devisions(){
        return $this->hasOne('App\Divisions','id','division_id');
    }
    public function customer(){
        return $this->hasOne('App\Customers','id','customer_id');
    }
    public function siteGroup(){
        return $this->hasOne('App\SiteGroups','id','site_group_id');
    }
    public function userManager(){
        return $this->hasOne('App\User','id','manager_id');
    }
    public function userContractManager(){
        return $this->hasOne('App\User','id','contract_manager_id');
    }
    public function clients(){
        return $this->hasOne('App\Clients','id','client_id');
    }
}
