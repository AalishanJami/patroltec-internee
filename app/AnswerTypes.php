<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnswerTypes extends Model
{
//        protected $fillable = [
//        'company_id',
//        'form_id',
//        'order',
//        'name',
//        'question',
//        'header',
//        'footer',
//        'width',
//        'border_style',
//        'show_on_dashboard',
//        'active',
//        'deleted',
//    ];
    protected $fillable = [
        'name',
        'active',
        'deleted',
    ];
    public function questions(){
        return $this->belongsTo('App\Questions');
    }

}
