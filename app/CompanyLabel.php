<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyLabel extends Model 
{
    protected $fillable = [
        'label_id', 'company_id', 'value',
    ];
    public function labels() {
        return $this->belongsTo('App\Label','label_id','id');
    }
}
