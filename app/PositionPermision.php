<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PositionPermision extends Model
{
     protected $fillable = [
        'position_id','user_id','company_id'
    ];
    public function position(){
        return $this->hasOne('App\Position','id','position_id');
    }
    
}
