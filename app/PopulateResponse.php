<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PopulateResponse extends Model
{
    protected $fillable = [
        'pre_populate_id', 'form_section_id', 'question_id', 'answer_id', 'answer_text'
    ];

    public function populate() {
        return $this->belongsTo('App\PopulateResponse','form_section_id','id')->where('active',1)->where('deleted',0);
    }
}
