<?php
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\FormSections;
use App\User;
use App\Forms;
use App\JobGroup;
use App\Jobs;
use App\Response;
use Carbon\Carbon;
use App\Company;
use App\PrePopulate;
use App\Questions;
use App\FormVersion;
use App\Location;
use App\FormType;
use PDF;
use App\Answers;

class FormsAPIController extends Controller
{
   public function getForms($user_id, $company_id){

        $data=JobGroup::with('users','forms','locations','jobs','locationBreakdowns')->where('company_id',$company_id)->where('user_id', $user_id)->orderBy('date_time','desc')->get();
        $array = array();
        $new_array = array();
        foreach($data as $key => $item){
            $check = true;
            foreach($item->jobs as $job){
                if($job->completed != 2 ){
                    $check = false;
                }
            }

            if($check){
                 $data->forget($key);
            }
        }

        if(count($data) == 1){
            foreach($data as $check){
                array_push($array, $check);

                $data = $array;
            }
        }

        foreach($data as $item){
            array_push($new_array, $item);
        }



        // $data = $data->where('active',1)->where('deleted',0);

        return response()->json($new_array);

   }
     public function getFormsCompleted($user_id, $company_id){

        $data=JobGroup::with('users','forms','locations','jobs','locationBreakdowns')->where('company_id',$company_id)->where('user_id', $user_id)->orderBy('date_time','desc')->get();
        $array = array();
        $new_array = array();
        foreach($data as $key => $item){
            $check = true;
            foreach($item->jobs as $job){
                if($job->completed != 2 ){
                    $check = false;
                }
            }

              if(!$check){
                 $data->forget($key);
            }
        }

        if(count($data) == 1){
            foreach($data as $check){
                array_push($array, $check);

                $data = $array;
            }
        }

        foreach($data as $item){
            array_push($new_array, $item);
        }

        return response()->json($new_array);

   }

   public function getFormSections($form_id){
   $jobGroup = JobGroup::where('id', $form_id)->orderBy('date_time', 'DESC')->where('active', 1)->where('completed', '!=', 2)->with('locations')->get();
        $data= [];
        $array = array();
        foreach ($jobGroup as $value){
            $form = Forms::where('id', $value->form_id)->first();
            if($form->app_form == 1){
              $jobs_main = Jobs::where('job_group_id', $value->id)->with('form_section')->get();
            //   $date_time_show = explode(" ",$jobs->date_time)[0];
            // dd($jobs_main->toArray());
            $form_section_collection = array();

              foreach ($jobs_main as $key => $value2) {
                $jobs = $value2;
                $jobs['location_breakdown_id'] = $value->location_breakdown_id;
                $jobs['location_id'] = $value->locations->id;
                $jobs['location_name'] = $value->locations->name;

                if(!isset($jobs->form_section->form_repeatable_id)){
                    if(isset($jobs->id)){
                        if(!array_search($jobs->form_section->id, $form_section_collection)){
                            $jobs['response'] = Response::where('job_id', $jobs->id)->get();
                            array_push($data, $jobs);
                            array_push($form_section_collection, $jobs->form_section->id);
                        }

                    }
              }
              }
            }
        }
    return response()->json($data);
   }

   public function createRepeatableSection($form_section_id, $job_group_id){
      $job = Jobs::where('form_section_id', $form_section_id)->where('job_group_id', $job_group_id)->first();
      $data=FormSections::where('id',$form_section_id)->with('questions')->first()->toArray();
      $questions=$data['questions'];
      $order=$data['order'];
      unset($data['id']);
      unset($data['questions']);
      $data['form_repeatable_id'] = $form_section_id;
      $form_section=FormSections::create($data);

      $newJob = $job->replicate();
      $newJob->form_section_id = $form_section_id;
      $newJob->save();
      //dd($newJob);
      $data = $this->getFormSections($data['form_id']);

       return $data;
   }
   public function getRepeatableSection($form_section_id, $form_id){
       $jobGroup = JobGroup::where('id', $form_id)->orderBy('date_time', 'DESC')->where('active', 1)->where('completed', '!=', 2)->with('locations')->get();
        $data= [];
        $array = array();
        foreach ($jobGroup as $value){
            $form = Forms::where('id', $value->form_id)->first();
            if($form->app_form == 1){
              $jobs_main = Jobs::where('job_group_id', $value->id)->with('form_section')->get();
            //   $date_time_show = explode(" ",$jobs->date_time)[0];

              foreach ($jobs_main as $key => $value2) {
                $jobs = $value2;


                $jobs['location_breakdown_id'] = $value->location_breakdown_id;
                $jobs['location_id'] = $value->locations->id;
                $jobs['location_name'] = $value->locations->name;
                unset($jobs->form_section->questions);
                $questions = Questions::where('section_id', $form_section_id)->with('answer_group.answer')->get();
                $jobs->form_section->questions = $questions;
                // dd($jobs->form_section->questions);

                if($jobs->form_section->id == $form_section_id){
                    if(isset($jobs->id)){
                        $jobs['response'] = Response::where('job_id', $jobs->id)->get();
                        array_push($data, $jobs);
                    }
                }
              }
            }
        }

    return response()->json($data);
   }

    public function exportForm($form_id){
      $jobGroup = JobGroup::where('id', $form_id)->orderBy('date_time', 'DESC')->where('active', 1)->with('locations')->get();
      $groupss= [];
      $array = array();
      foreach ($jobGroup as $value){
        $form = Forms::where('id', $value->form_id)->first();
        $jobs_main = Jobs::where('job_group_id', $value->id)->with('form_section')->get();
        $form_section_collection = array();
        foreach ($jobs_main as $key => $value2) {
          $jobs = $value2;
          $jobs['location_breakdown_id'] = $value->location_breakdown_id;
          $jobs['location_id'] = $value->locations->id;
          $jobs['location_name'] = $value->locations->name;
          if(!isset($jobs->form_section->form_repeatable_id)){
            if(isset($jobs->id)){
              if(!array_search($jobs->form_section->id, $form_section_collection)){
                  // $jobs['response'] = Response::where('job_id', $jobs->id)->get();
                foreach ($jobs->form_section->questions as $key2 => $question) {

                  $response = Response::where('job_id', $jobs->id)->where('question_id', $question->id)->first();
                  if($response){
                     $answer = Answers::where('id', $response->answer_id)->first();
                    $response['answer_data']= $answer;
                  }

                  $jobs['form_section']['questions'][$key2]['response'] = $response;

                }
                  array_push($groupss, $jobs);
                  array_push($form_section_collection, $jobs->form_section->id);
              }
            }
          }
        }
      }
      return view('backend.form_export.index',compact('groupss'));
    }

    public function downloadForm($form_id){
       $jobGroup = JobGroup::where('id', $form_id)->orderBy('date_time', 'DESC')->where('active', 1)->with('locations')->get();
      $groupss= [];
      $array = array();
      foreach ($jobGroup as $value){
        $form = Forms::where('id', $value->form_id)->first();
        $jobs_main = Jobs::where('job_group_id', $value->id)->with('form_section')->get();
        $form_section_collection = array();
        foreach ($jobs_main as $key => $value2) {
          $jobs = $value2;
          $jobs['location_breakdown_id'] = $value->location_breakdown_id;
          $jobs['location_id'] = $value->locations->id;
          $jobs['location_name'] = $value->locations->name;
          if(!isset($jobs->form_section->form_repeatable_id)){
            if(isset($jobs->id)){
              if(!array_search($jobs->form_section->id, $form_section_collection)){
                  // $jobs['response'] = Response::where('job_id', $jobs->id)->get();
                foreach ($jobs->form_section->questions as $key2 => $question) {

                  $response = Response::where('job_id', $jobs->id)->where('question_id', $question->id)->first();
                  if($response){
                     $answer = Answers::where('id', $response->answer_id)->first();
                    $response['answer_data']= $answer;
                  }

                  $jobs['form_section']['questions'][$key2]['response'] = $response;

                }
                  array_push($groupss, $jobs);
                  array_push($form_section_collection, $jobs->form_section->id);
              }
            }
          }
        }
      }
      $pdf = PDF::loadView('backend.form_export.pdf_download', compact('groupss'));
      return $pdf->download('Form.pdf');
      //file_put_contents('public/pdf/'.$random.'_willApp.pdf', $pdf->output());
    }
}
