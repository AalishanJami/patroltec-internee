<?php
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\CompanyInfo;
use App\CompanyCms;
use App\Company;
use App\AppCompanyPermission;

class ConfigsAPIController extends Controller
{
    public function contact($company_id){
        $data = CompanyInfo::where('company_id', $company_id)->where('type', 'contact')->get();
        return response()->json($data);
    }

    public function faq($company_id){
        $data = CompanyInfo::where('company_id', $company_id)->where('type', 'faq')->get();
        return response()->json($data);
    }

    public function populateCms(){
	    $company_cms = CompanyCms::where('company_id', 2)->get();

	    foreach ($company_cms as $key => $cms) {
	    	$insert = CompanyCms::insert([
	    		'company_id' => 1,
	    		'key' => $cms->key,
	    		'value' => $cms->value,
	    	]);
	    }
    }

    public function setCompanyPermissions(){
	    $company_cms = AppCompanyPermission::where('company_id', 2)->get();

	    foreach ($company_cms as $key => $cms) {
	    	$insert = AppCompanyPermission::insert([
	    		'company_id' => 1,
	    		'app_permission_id' => $cms->app_permission_id,
	    		'app_role_id' => $cms->app_role_id,
	    	]);
	    }
    }

}
