<?php
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\JobGroup;
use App\Jobs;
use App\FormSections;
use App\Location;
use App\FormVersion;
use Carbon\Carbon;
use App\User;
use App\LocationPermission;
use App\Forms;
use Validator;
use App\ResponseGroups;
use App\PopulateResponse;
use App\LocationBreakdown;
use App\LocationGroupPermision;
use App\Response;
use App\StaticFormUpload;

class JobsAPIController extends Controller
{
    public function getJobs(Request $request){
      $jobGroup = JobGroup::where('user_id', $request->user_id)->orderBy('date_time', 'DESC')->where('active', 1)->with('locations')->get();
        $data= [];
        $array = array();
        foreach ($jobGroup as $value){
            $form = Forms::where('id', $value->form_id)->first();
            if($form->app_form == 1){
              $jobs = Jobs::where('job_group_id', $value->id)->with('form_section')->first();
              $date_time_show = explode(" ",$jobs->date_time)[0];
              $array_search = in_array($date_time_show, $array);
              if(!$array_search){
                  array_push($array, $date_time_show);
                  $jobs['date_time_show'] = $date_time_show;
              }
              $jobs['location_breakdown_id'] = $value->location_breakdown_id;
              $jobs['location_id'] = $value->locations->id;
              $jobs['location_name'] = $value->locations->name;
              if(isset($jobs->id)){
                $jobs['response'] = Response::where('job_id', $jobs->id)->get();
                array_push($data, $jobs);
              }
            }
        }

        if($data){
            return response()->json([
                    'success' => true,
                    'data' => $data
                ]);
        }
        else {
            return response()->json([
                    'success' => false,
                ]);
        }
    }

    public function getStaticForm($id){
      $data = StaticFormUpload::where('form_id', $id)->first();
      return response()->json($data);
    }

    public function getProjects(Request $request){
        $permission = LocationGroupPermision::where('employee_id', $request->user_id)->where('company_id', $request->company_id)->get();
        $data = array();
        foreach ($permission as $key => $value) {
          $location = Location::where('site_group_id', $value->site_groups_id)->where('company_id', $request->company_id)->get();
          foreach ($location as $key => $value) {
            array_push($data, $value);
          }
        }
      if($data){
            return response()->json([
                    'success' => true,
                    'data' => $data
                ]);
        }
        else {
            return response()->json([
                    'success' => false,
                ]);
        }
    }

    public function getForms($company_id){
        $data = Forms::where('active', 1)->where('deleted', 0)->where('company_id', $company_id)->where('app_form', 1)->get();

        if($data){
            return response()->json([
                    'success' => true,
                    'data' => $data
                ]);
        }
        else {
            return response()->json([
                    'success' => false,
                ]);
        }
    }

    public function getEquipments(Request $request){

        $form_type = Forms::find($request->form_id)->location_type;
        $location = array();
        if($form_type !="m"){
        $location=LocationBreakdown::where('location_id',$request->location_id)->where('type',$form_type)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
      }

      if(isset($location) && !empty($location)){
        return response()->json(['location'=>$location,'form_type'=>$form_type]);
      } else {
        return response()->json(['success'=>false]);
      }
    }

     public function raiseJob(Request $request)
    {

   $input = $request->all();
        //$input['user_id'] = array($input['user_id']);
        $input['completed'] = 0;
        $user = $input['user_id'];
     
          $populate_id =null;
        if (isset($input['pre_populate_id'])) {
           $populate_id =$input['pre_populate_id'];
        }

        unset($input['pre_populate_id']);
       // array_push($users,$input['user_id']);
        //dd($users);
            //dd($input);
         $data = [
                'date'=>'required',
                'time'=>'required',
                ];
        $validator = Validator::make($request->all(), $data);
        if ($validator->fails())
        {
              $notification = array(
                    'message' => 'date and time required',
                    'alert-type' => 'error'
                );
        return response()->json($notification);
        }
        $input['contractor_id'] = Location::find($input['location_id'])->contractor_id;

        //dd($form_sections);
               $form_sections = FormVersion::where('form_id',$input['form_id'])->where('active',1)->where('deleted',0)->orderBy('id','desc')->get();
         $form = Forms::find($input['form_id']);

         $form_sections = $form_sections->groupBy('version_id')->first()->pluck('form_section_id');
         $form_section = array_unique($form_sections->toArray());
         $form_sections = FormSections::whereIn('id',$form_section)->with('questions.answer_type','questions.answer_group.answer')->orderBy('order','asc')->get();


       $date = Carbon::parse($input['date']);
       $time = explode(':',$input['time']);
       $date->setTime($time[0],$time[1]);
       $input['date_time'] = $date;

       if($form->lock_job_after==1)
       {
       $input['post_time'] = $date->addMinutes($form->lock_from);
       }else  if($form->lock_job_after==2)
       {
       $input['post_time'] = $date->addHours($form->lock_from);
       } else  if($form->lock_job_after==3)
       {
       $input['post_time'] = $date->addDays($form->lock_from);
       } else  if($form->lock_job_after==4)
       {
       $input['post_time'] = $date->addWeeks($form->lock_from);
       } else{
       $input['post_time'] = $date;
       }


      if($form->lock_job_before==1)
       {
       $input['pre_time'] = $date->subMinutes($form->lock_to);
       }else  if($form->lock_job_before==2)
       {
       $input['pre_time'] = $date->subHours($form->lock_to);
       } else  if($form->lock_job_before==3)
       {
       $input['pre_time'] = $date->subDays($form->lock_to);
       } else  if($form->lock_job_before==4)
       {
       $input['pre_time'] = $date->subWeeks($form->lock_to);
       } else{
       $input['pre_time'] = $date;
       }

       unset($input['_token']);
       unset($input['date']);
       unset($input['time']);
       $input['title']=$form->name;
       $breakdown = [];

       if(isset( $input['location_breakdown_id'])){

       $breakdown = $input['location_breakdown_id'];
       }
       //dd($breakdown);
       $input['form_type_id'] = $form->type;

    
          $input['user_id'] = $user;
           $input['user_group_id'] = User::find($user)->user_group_id;
          
               if(isset( $input['location_breakdown_id'])){
                   foreach ($breakdown as $value) {
                        $input['location_breakdown_id']=$value;
                        $jobGroup = JobGroup::create($input);
                        $input['job_group_id'] = $jobGroup->id;
                        $x = [];
                        $input['tag'] = $form->req_tag;
                            foreach ($form_sections as $form_section) {
                                array_push($x,"1");
                                $input['form_section_id'] = $form_section->id;
                                $input['order'] = $form_section->order;
                                unset($input['company_id']);
                                $job = Jobs::Create($input);
  
                            if($populate_id!='null' || $populate_id!=null)
                           {
                                unset($input['completed']);

                                $rds = ResponseGroups::where('location_id',$input['location_id'])->where('location_breakdown_id',$value)->where('pre_populate_id',$populate_id)->where('active',1)->where('deleted',0)->get();
                               //dd($rds);
                                if($rds->isEmpty())
                                {
                                        $jobs = Jobs::where('job_group_id',$jobGroup->id)->orderBy('order','asc')->get();
                                          $responseGroups = ResponseGroups::create($input);
                                          $populates = PopulateResponse::where('pre_populate_id',$populate_id)->where('form_section_id',$form_section->id)->where('active',1)->where('deleted',0)->get();
                                          foreach ($populates as $key => $populate) {
                                              $populate->response_group_id = $responseGroups->id;
                                              $populate->job_id = $job->id;
                                              Response::create($populate->toArray());
                                          }
                                }
                                else
                                {
                                    foreach ($rds as $key => $rd) {
                                        $nrd = $rd->replicate();
                                        $nrd->job_group_id = $jobGroup->id;
                                        $nrd->save();
                                        $rps = Response::where('response_group_id',$rd->id)->get();
                                        foreach ($rps as $key => $rp) {
                                            $nrp = $rp->replicate();
                                            $nrp->response_group_id = $nrd->id;
                                            $nrp->job_id = $job->id;
                                            $nrp->save();
                                        }
                                    }
                                }
                       }
                   }
                }
            }
            else
            {
                       $jobGroup = JobGroup::create($input);
                        $input['job_group_id'] = $jobGroup->id;
                        $x = [];
                         $input['tag'] = $form->req_tag;
                           foreach ($form_sections as $form_section) {
                                array_push($x,"1");
                                $input['form_section_id'] = $form_section->id;
                                $input['order'] = $form_section->order;
                                unset($input['company_id']);
                                $job =Jobs::Create($input);

                            if($populate_id=='null' || $populate_id==null)
                           {
                                unset($input['completed']);

                                $rds = ResponseGroups::where('location_id',$input['location_id'])->where('pre_populate_id',$populate_id)->where('active',1)->where('deleted',0)->get();
                               //dd($rds);
                            if($rds->isEmpty())
                            {
                                      $responseGroups = ResponseGroups::create($input);
                                    $jobs = Jobs::where('job_group_id',$jobGroup->id)->orderBy('order','asc')->get();
                                      $populates = PopulateResponse::where('pre_populate_id',$populate_id)->where('form_section_id',$form_section->id)->where('active',1)->where('deleted',0)->get();
                                      foreach ($populates as $key => $populate) {
                                          $populate->response_group_id = $responseGroups->id;
                                          $populate->job_id = $job->id;
                                          Response::create($populate->toArray());
                                      }
                            }
                            else
                            {
                                foreach ($rds as $key => $rd) {
                                    $nrd = $rd->replicate();
                                    $nrd->job_group_id = $jobGroup->id;
                                    $nrd->save();
                                    $rps = Response::where('response_group_id',$rd->id)->get();
                                    foreach ($rps as $key => $rp) {
                                        $nrp = $rp->replicate();
                                        $nrp->response_group_id = $nrd->id;
                                        $nrp->job_id = $job->id;
                                        $nrp->save();

                                    }
                                }
                            }
                       }
                   }
            }
        
        if(empty($x))
        {
        $notification = array(
                    'message' => 'No active Form found with Questions',
                    'alert-type' => 'error'
                );
        return response()->json($notification);
        }
        else
        {
        $notification = array(
                'message' => 'Job raised Successfully',
                'alert-type' => 'success'
            );

        return response()->json($notification);

        }
    }

    public function getJobsToWrite($string, $id){
      $data = LocationBreakdown::where('company_id', $id)->where('type', $string)->where('active', 1)->where('deleted', 0)->with('location')->get();

      if($data){
            return response()->json([
                    'success' => true,
                    'data' => $data
                ]);
        }
        else {
            return response()->json([
                    'success' => false,
                ]);
        }
    }
}
