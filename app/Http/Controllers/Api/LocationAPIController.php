<?php
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Location;
use App\Divisions;
use App\Customers;
use App\SiteGroups;
use App\LocationGroupPermision;

class LocationAPIController extends Controller
{
    public function create(Request $request){
    	$check_division = Divisions::where('company_id', $request->company_id)->where('app_user', 1)->first();
    	$division;
    	if($check_division){
    		$division = $check_division;
    	} else {
    		$division = Divisions::create([
	    		'name' => 'App',
	    		'company_id' => $request->company_id,
	    		'active' => 1,
	    		'deleted' => 0,
	    		'app_user' => 1,
    		]);
    	}
    	$check_customer =  Customers::where('company_id', $request->company_id)->where('division_id', $division->id)->where('app_user', 1)->first();
    	$customer;
    	if($check_customer){
    		$customer = $check_customer;
    	} else {
    		$customer = Customers::create([
	    		'name' => 'World',
	    		'company_id' => $request->company_id,
	    		'division_id' => $division->id,
	    		'active' => 1,
	    		'deleted' => 0,
	    		'app_user' => 1,
    		]);
    	}

		$site_group = SiteGroups::create([
    		'name' => $request->user_name,
    		'company_id' => $request->company_id,
    		'division_id' => $division->id,
    		'customer_id' => $customer->id,
    		'active' => 1,
    		'deleted' => 0
		]);

		$locations = Location::create([
    		'name' => $request->user_name,
    		'company_id' => $request->company_id,
    		'site_group_id' => $site_group->id,
    		'customer_id' => $customer->id,
    		'notes' => $request->notes,
    		'active' => 1,
    		'deleted' => 0,
		]);
		$permission = LocationGroupPermision::create([
			'site_groups_id' => $site_group->id,
			'active' => 1,
    		'deleted' => 0,
    		'company_id' => $request->company_id,
    		'employee_id' => $request->user_id
		]);

		$data['division'] = $division;
		$data['customer'] = $customer;
		$data['site_group'] = $site_group;
		$data['locations'] = $locations;
		$data['permission'] = $permission;

		return response()->json($data);
    }

    public function get(Request $request){
    	$permission = LocationGroupPermision::where('employee_id', $request->user_id)->where('company_id', $request->company_id)->get();
    	$data = array();
    	foreach ($permission as $key => $value) {
    		$location = Location::where('site_group_id', $value->site_groups_id)->where('company_id', $request->company_id)->get();
    		foreach ($location as $key => $value) {
    			array_push($data, $value);
    		}
    	}
    	return response()->json($data);
    }

    public function update(Request $request){
    	$location = Location::where('id', $request->id)->update([
    		'name' => $request->name,
    		'notes' => $request->notes
    	]);
    		
    	return response()->json($location);
    }
}
