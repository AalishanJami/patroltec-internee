<?php
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\GeneralFunctions;
use App\CompanyUser;
use App\CompanyCms;
use App\AppUsers;
use App\SharedUsers;
use Illuminate\Support\Facades\Hash;

class PaymentsAPIController extends Controller
{
    public function performPayment(Request $token){
        \Stripe\Stripe::setApiKey('sk_test_NsFPwRPesgISfZXLt7chRxKj00ceSkwDSN');

        $type = $token->type;
        $amount;
        $price_tag;
        if($type == "1"){
            $amount = 399;
            $price_tag = "price_1HGn4gCLO4uxTDqcfgzjtWBM";
        } else {
            $amount = 2999;
            $price_tag = "price_1HGn4gCLO4uxTDqcyQK7YPnJ";
        }

        // Create a Customer:
        $customer = \Stripe\Customer::create([
            'source' =>  $token->id,
            'email' => $token->email,
        ]);

        // Charge the Customer instead of the card:
        $charge = \Stripe\Charge::create([
            'amount' => $amount,
            'currency' => 'gbp',
            'customer' => $customer->id,
            'description' => 'WillApp',
        ]);

        $subs = new \Stripe\StripeClient(
          'sk_test_NsFPwRPesgISfZXLt7chRxKj00ceSkwDSN'
        );
        
        $subscription = $subs->subscriptions->create([
          'customer' => $customer->id,
          'items' => [
            ['price' => $price_tag],
          ],
        ]);

        $user = User::where('email', $token->email)->update([
            'is_paid' => 1,
            'stripe_id' => $customer->id,
            'subscription_type' => $type,
            'subscription_amount' => $amount,
            'subscription_id' => $subscription->id,
        ]);

        return response()->json('1');
    }

    public function cancelSub($user_id){
        $user = User::where('id', $user_id)->first();
        $stripe = new \Stripe\StripeClient(
          'sk_test_NsFPwRPesgISfZXLt7chRxKj00ceSkwDSN'
        );
        $stripe->subscriptions->cancel(
          $user->subscription_id,
          []
        );
        $user = User::where('id', $user_id)->update([
            'is_paid' => 0,
            'stripe_id' => null,
            'subscription_type' => null,
            'subscription_amount' => null,
            'subscription_id' => null,
        ]);

        return response()->json('1');
    }
}
