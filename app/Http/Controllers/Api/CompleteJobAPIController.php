<?php
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Response;
use App\Jobs;
use App\JobGroup;
use App\ResponseGroups;

class CompleteJobAPIController extends Controller
{
     public function completeJob(Request $request){
        
      $company_id = $request->company_id;
      $user_id = $request->user_id;
      $job_id = $request->job_id;
      $form_section_id = $request->form_section_id;

      $job_group_id = $request->job_group_id;
      
      $response_group_id = null;

      $job_groups = JobGroup::where('id', $job_group_id)->first();

      //dd($job_groups);

      if(isset($request->response_group_id)){
        $response_group_id = $request->response_group_id;
      } else {
        $response_group_id = ResponseGroups::create([
          'job_group_id' => $job_groups->id,
          'company_id' => $company_id,
          'user_id' => $user_id,
          'form_id' => $job_groups->form_id,
          'location_id' => $job_groups->location_id,
        ]);
       $response_group_id = $response_group_id->id;
      }

      $response = $request->q;

      foreach ($response as $key => $value) {
        $check = Response::where('question_id', $value['question_id'])->where('response_group_id', $response_group_id)->delete();
      }
      foreach ($response as $key => $value) {
        if($value['question_type'] == 5 || $value['question_type'] == 6 || $value['question_type'] == 7 || $value['question_type'] == 9){
          $insert = Response::insert([
            'response_group_id' => $response_group_id,
            'job_id' => $job_id,
            'user_id' => $user_id,
            'response_type' => $value['question_type'],
            'question_id' => $value['question_id'],
            'answer_text' => $value['answer_id'],
            'form_section_id' => $form_section_id,
            'company_id' => $company_id,
            'active' => 1
          ]);
        } else if($value['question_type'] == 1 || $value['question_type'] == 2 || $value['question_type'] == 3 || $value['question_type'] == 4){
             $insert = Response::insert([
            'response_group_id' => $response_group_id,
            'user_id' => $user_id,
            'job_id' => $job_id,
            'response_type' => $value['question_type'],
            'question_id' => $value['question_id'],
            'answer_id' => $value['answer_id'],
            'comments' => $value['comment'],
            'form_section_id' => $form_section_id,
            'company_id' => $company_id,
            'active' => 1
          ]);
        } else if($value['question_type'] == 12){
             $insert = Response::insert([
            'response_group_id' => $response_group_id,
            'user_id' => $user_id,
            'job_id' => $job_id,
            'response_type' => $value['question_type'],
            'question_id' => $value['question_id'],
            'answer_id' => $value['answer_id'],
            'image' => $value['signature'],
            'form_section_id' => $form_section_id,
            'company_id' => $company_id,
            'active' => 1
          ]);
        }
        else if($value['question_type'] == 11){
            
             $insert = Response::insert([
            'response_group_id' => $response_group_id,
            'user_id' => $user_id,
            'job_id' => $job_id,
          'response_type' => $value['question_type'],
            'question_id' => $value['question_id'],
            'image' => $value['signature'],
            'form_section_id' => $form_section_id,
            'company_id' => $company_id,
            'active' => 1
          ]);
        }
        else if($value['question_type'] == 10){
             $insert = Response::insert([
            'response_group_id' => $response_group_id,
            'user_id' => $user_id,
            'job_id' => $job_id,
            'response_type' => $value['question_type'],
            'question_id' => $value['question_id'],
            'answer_text' => $value['answer_id'],
            'image' => $value['signature'],
            'form_section_id' => $form_section_id,
            'company_id' => $company_id,
            'active' => 1
          ]);
        }
        $update = Jobs::where('id', $job_id)->update([
          'completed' => 2
        ]);
      }
      return response()->json('1');
    }

    public function autoSubmit(Request $request){
        $job_id = $request->job_id;
        $update = Jobs::where('id', $job_id)->update([
          'completed' => 1
        ]);  
        return response()->json('1');
    }
    
     public function completeJobFiles(Request $request){
         
          $company_id = $request->company_id;
          $user_id = $request->user_id;
          $job_id = $request->job_id;
          $form_section_id = $request->form_section_id;
          
        
         if($file=$request->file('fileName')){
             foreach($file as $fileitem){
                $image_name = pathinfo($fileitem->getClientOriginalName(), PATHINFO_FILENAME);
                $fileitem->move(public_path('uploads/jobs'), $fileitem->getClientOriginalName());
                $image_name = $fileitem->getClientOriginalName();
                $this_job_id = explode("-",$fileitem->getClientOriginalName())[0];
                $this_question_id = explode("-",$fileitem->getClientOriginalName())[1];
                $check = Response::where('job_id', $job_id)->where('user_id', $user_id)->where('company_id', $company_id)->where('form_section_id', $form_section_id)->where('response_type', 'd')->delete();
                $data = Response::where('question_id', $this_question_id)->update([
                  'image' => $fileitem->getClientOriginalName(),
                ]);
             }
        }
        
        if($file=$request->file('selectdocument')){
              $image_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
              $file->move(public_path('uploads/jobs'), $file->getClientOriginalName());
              $check = Response::where('job_id', $job_id)->where('user_id', $user_id)->where('company_id', $company_id)->where('form_section_id', $form_section_id)->where('response_type', 'd')->delete();
              $data = Response::insert([
                  'company_id' => $company_id,
                  'user_id' => $user_id,
                  'job_id' => $job_id,
                  'form_section_id' => $form_section_id,
                  'file_name' => $file->getClientOriginalName(),
                  'response_type' => 'd',
                ]);
        }
        
           if($file=$request->file('takepicture')){
             // return response()->json('inside if');
              $image_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
              $file->move(public_path('uploads/jobs'), $file->getClientOriginalName());
              $check = Response::where('job_id', $job_id)->where('user_id', $user_id)->where('company_id', $company_id)->where('form_section_id', $form_section_id)->where('response_type', 't')->delete();
              $data = Response::insert([
                  'company_id' => $company_id,
                  'user_id' => $user_id,
                  'job_id' => $job_id,
                  'form_section_id' => $form_section_id,
                  'file_name' => $file->getClientOriginalName(),
                  'response_type' => 't',
                ]);
        }
        
        if($file=$request->file('selectpicture')){
             // return response()->json('inside if');
              $image_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
              $file->move(public_path('uploads/jobs'), $file->getClientOriginalName());
              $check = Response::where('job_id', $job_id)->where('user_id', $user_id)->where('company_id', $company_id)->where('form_section_id', $form_section_id)->where('response_type', 's')->delete();
              $data = Response::insert([
                  'company_id' => $company_id,
                  'user_id' => $user_id,
                  'job_id' => $job_id,
                  'form_section_id' => $form_section_id,
                  'file_name' => $file->getClientOriginalName(),
                  'response_type' => 's',
                ]);
        }
        
        if(isset($request->notes)){
              $check = Response::where('job_id', $job_id)->where('user_id', $user_id)->where('company_id', $company_id)->where('form_section_id', $form_section_id)->where('response_type', 'n')->delete();
              $data = Response::insert([
                  'company_id' => $company_id,
                  'user_id' => $user_id,
                  'job_id' => $job_id,
                  'form_section_id' => $form_section_id,
                  'answer_text' => $request->notes,
                  'response_type' => 'n',
                ]);
        }

        $update = Jobs::where('id', $job_id)->update([
          'completed' => 2
        ]);
        
        return response()->json('1');


    }
  
}
