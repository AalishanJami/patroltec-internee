<?php
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\CompanyUser;
use App\CompanyCms;
use App\AppUsers;
use App\SharedUsers;
use App\AppCompanyPermission;
use App\AppPermissionModule;
use App\AppRoles;
use Carbon\Carbon;
use App\Company;
use App\AppPermissions;
use Illuminate\Support\Facades\Hash;
use Mail;

class UserAPIController extends Controller
{
    public function login(Request $request){
        $data = User::where('email', $request->email)->with('company')->first();
        $app_user = AppUsers::where('email', $request->email)->first();
        if($data){
            if (password_verify($request->password, $data->password)) {
                if(isset($data->email_verified_at)){
                    $companyUser = CompanyUser::where('user_id', $data->id)->first();
                    $companyCms = CompanyCms::where('company_id', $companyUser->company_id)->get();
                    $cms = array();
                    foreach ($companyCms as $key => $value) {
                        $cms[$value->key] = $value->value;
                    }
                    $data['cms'] = $cms;
                    $company_permissions = AppCompanyPermission::where('company_id', $companyUser->company_id)->where('app_role_id', 1)->get();
                    $permissions;
                    foreach($company_permissions as $comp_perm){
                        $permission = AppPermissions::where('id', $comp_perm->app_permission_id)->with('module')->first();
                        $permissions[$permission->name] = $permission;
                    }
                    $data['permissions'] = $permissions;
                    return response()->json([
                        'success' => true,
                        'userData' => $data
                    ]);
                } else {
                    return response()->json([
                        'success' => false,
                    ]);
                }
            } else {
                return response()->json([
                    'success' => false,
                ]);
            }
        } else if($app_user){
            if (password_verify($request->password, $app_user->password)) {
                $shared_user = SharedUsers::where('app_user_id', $app_user->id)->first();
                $user = User::where('id', $shared_user->user_id)->with('company')->first();
                $data = $app_user;

                $companyUser = CompanyUser::where('user_id', $user->id)->first();
                $companyCms = CompanyCms::where('company_id', $companyUser->company_id)->get();
                $company = Company::where('id', $companyUser->company_id)->first();
                $company_permissions = AppCompanyPermission::where('company_id', $companyUser->company_id)->where('app_role_id', 2)->get();

                $permissions;
                foreach($company_permissions as $comp_perm){
                    $permission = AppPermissions::where('id', $comp_perm->app_permission_id)->with('module')->first();
                    $permissions[$permission->name] = $permission;
                }

                $cms = array();
                foreach ($companyCms as $key => $value) {
                    $cms[$value->key] = $value->value;
                }
                $data['cms'] = $cms;
                $data['is_shared'] = true;
                $data['company'] = $company;
                $data['origin'] = $user;
                $data['permissions'] = $permissions;
                return response()->json([
                    'success' => true,
                    'userData' => $data
                ]);
            } else {
                return response()->json([
                    'success' => false,
                ]);
            }
        }
        else {
            return response()->json([
                    'success' => false,
                ]);
        }
    }

    public function refresh($user_id){
            $data = User::where('id', $user_id)->with('company')->first();
            $companyUser = CompanyUser::where('user_id', $data->id)->first();
            $companyCms = CompanyCms::where('company_id', $companyUser->company_id)->get();
            $cms = array();
            foreach ($companyCms as $key => $value) {
                $cms[$value->key] = $value->value;
            }
            $data['cms'] = $cms;
            $company_permissions = AppCompanyPermission::where('company_id', $companyUser->company_id)->where('app_role_id', 1)->get();
            $permissions;
            foreach($company_permissions as $comp_perm){
                $permission = AppPermissions::where('id', $comp_perm->app_permission_id)->with('module')->first();
                $permissions[$permission->name] = $permission;
            }
            $data['permissions'] = $permissions;
            return response()->json([
                'success' => true,
                'userData' => $data
            ]);
    }

    public function updateProfile(Request $request){
        $data = User::where('id', $request->id)->update($request->all());
        return response()->json($data);
    }

    public function register(Request $request){
        $check = User::where('email', $request->email)->first();
        if(empty($check)){
            $rand = $this->generateRandomString(30);
            $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'active' => 1,
            'deleted' => 0,
            'company_id' => $request->company_id,
            'password' => Hash::make($request->password),
            'app_role_id' => 1,
            'remember_token' => $rand,
            ]);

            $company_user = CompanyUser::create([
                'company_id' => $request->company_id,
                'user_id' => $user->id
            ]);

            $data['name']=$request->name;
            $data['email']=$request->email;
            $data['token']=$rand;
            Mail::send('backend.app_emails.verify_email', $data, function($message) use ($data, $request) {
                $message->to($request->email, $request->name)->subject('Welcome to WillApp')->from('admin@alandale.patroltec.net','Admin');
            });

             return response()->json([
                'success' => true,
                'userData' => $user
            ]);
        } else {
             return response()->json([
                'success' => false,
            ]);
        }
    }

    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public  function createSharedUser(Request $request){
        $check = AppUsers::where('email', $request->email)->first();
        if(empty($check)){

            $user = AppUsers::create([
            'name' => $request->name,
            'email' => $request->email,
            'active' => 1,
            'deleted' => 0,
            'password' => Hash::make($request->password),
            ]);

            $shared_users = SharedUsers::create([
                'app_user_id' => $user->id,
                'user_id' => $request->user_id
            ]);

            $data['name']=$request->name;
            $data['user_name']=$request->email;
            $data['password']=$request->password;
            $data['will_user_name']=$request->user_name;
            $data['login']="https://dev.patroltec.net/login";
            $data['will_user_name']=$request->user_name;
            $data['company_name']=$request->company_name;
            $data['register']="https://dev.patroltec.net/signup";

            Mail::send('backend.app_emails.shared_user', $data, function($message) use ($data, $request) {
                $message->to($request->email, $request->name)->subject('Welcome to WillApp')->from('admin@xweb4u.com','Admin');
            });

             return response()->json([
                'success' => true,
                'userData' => $user
            ]);
        } else {
             return response()->json([
                'success' => false,
            ]);
        }
    }

    public function resendSharedUserEmail(Request $request){
        $check = AppUsers::where('email', $request->email)->first();
        $date=Carbon::now();
        $data['name']=$check->name;
        $data['user_name']=$request->email;
        $data['password']="";
        $data['will_user_name']=$request->name;
        $data['login']="https://dev.patroltec.net/login";
        $data['company_name']=$request->company_name;
        $data['register']="https://dev.patroltec.net/signup";
        $update = AppUsers::where('email', $request->email)->update([
            'email_verified_at' => $date,
        ]);

        Mail::send('backend.app_emails.shared_user', $data, function($message) use ($data, $request) {
            $message->to($request->email, $request->name)->subject('Welcome to WillApp')->from('admin@xweb4u.com','Admin');
        });

        return response()->json('1');
    }

    public  function updateSharedUser(Request $request){

        $user = AppUsers::where('id', $request->id)->update([
        'name' => $request->name,
        ]);

        if($request->email){
            $check = AppUsers::where('id', '!=', $request->id)->where('email', $request->email)->first();
            if(empty($check)){
                $user = AppUsers::where('id', $request->id)->update([
                    'email' => $request->email
                ]);
            } else {
                return response()->json([
                    'success' => false,
                ]);
            }
        }

        if($request->password){
            $user = AppUsers::where('id', $request->id)->update([
                'password' => Hash::make($request->password)
            ]);
        }

        return response()->json([
            'success' => true,
        ]);
    }

    public function getMySharedUsers($id){
        $shared_users = SharedUsers::where('user_id', $id)->get();
        $users = array();
        foreach ($shared_users as $key => $value) {
            $appuser = AppUsers::where('id', $value->app_user_id)->first();
            array_push($users, $appuser);
        }

        if(empty($users)){
            return response()->json([
                'success' => false,
            ]);
        } else {
            return response()->json([
                'success' => true,
                'userData' => $users
            ]);
        }
    }

    public function performPayment(Request $token){
        \Stripe\Stripe::setApiKey('sk_test_NsFPwRPesgISfZXLt7chRxKj00ceSkwDSN');

            // Token is created using Stripe Checkout or Elements!
            // Get the payment token ID submitted by the form:
            $charge = \Stripe\Charge::create([
              'amount' => 30,
              'currency' => 'gbp',
              'description' => 'WillApp',
              'source' => $token->id,
            ]);
    }

    public function contactUs(Request $request)
    {
        $input=$request->all();
        $data=array();
    $data['name']=$request->name;
        $data['email']=$request->email;
        $data['title']=$request->subject;
        $data['text_message']=$request->message;

        Mail::send('backend.app_emails.contactus', $data, function($message) use ($data) {
            $message->to('aalishanj1@gmail.com', 'Admin')->subject(' Customer Support')->from('admin@xweb4u.com','Admin');
        });

        return response()->json('1');
    }

    public function forgetPassword(Request $request)
    {
        $data=array();
        $user = User::where('email', $request->email)->first();
        if($user){
            $company = Company::where('id', $user->company_id)->first();
            $random = substr(md5(microtime()),rand(0,26),5);
            $user->update([
                'fp_token' => $random,
            ]);
            $data['name'] = $user->name;
            $data['email'] = $user->email;
            $data['company'] = $company->name;
            $data['random'] = $random;

            Mail::send('backend.app_emails.forget_password', $data, function($message) use ($data) {
                $message->to('aalishanj1@gmail.com', 'Admin')->subject('Password Reset')->from('admin@xweb4u.com','Admin');
            });
            return response()->json([
                'success' => true,
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Email address does not exist',
            ]);
        }
    }

    public function verifyForgetPassword(Request $request){
        $user = User::where('email', $request->email)->first();
        if($user){
            if($user->fp_token == $request->token){
                 return response()->json([
                    'success' => true,
                ]);
            } else {
                 return response()->json([
                    'success' => false,
                    'message' => 'Token does not match',
                ]);
            }
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Email address does not exist',
            ]);
        }
    }

    public function changePassword(Request $request){
         $user = User::where('email', $request->email)->first();
        if($user){
            $user->update([
                'password' => Hash::make($request->password),
            ]);
            return response()->json([
                'success' => true,
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Email address does not exist',
            ]);
        }
    }
}
