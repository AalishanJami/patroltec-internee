<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Carbon\Carbon;

class VerifyEmail extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($string)
    {
        $user = User::where('remember_token', $string)->first();
            if($user){
                $user->update([
                'email_verified_at' => Carbon::now(),
            ]);
            echo "EMAIL VERIFIED";
        } else {
            echo "USER NOT FOUND";
        }
        
       
    }
}
