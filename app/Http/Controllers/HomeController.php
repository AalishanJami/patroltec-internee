<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FormScoreCount;
use App\FormScoreLocation;
use App\Charts\UserLineChart;
use App\Jobs;
use App\JobGroup;
use App\FormVersion;
use App\FormSections;
use App\FormType;
use App\Scoring;
use Auth;
use Session;
use SplFixedArray;
use App\ResponseGroups;
use App\Response;
use App\Company;
use CloudConvert;
use App\BonusScore;
use DataTables;
use Carbon\Carbon;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function logo(Request $request){
      $time = time();
      $image = $request->file('image');
      $file = $time.'.'.$image->getClientOriginalExtension();
      $destinationPath = public_path('logo');
      $image->move($destinationPath, $file);
      // Company::where('id',Auth::user()->default_company)->update(['logo'=>$file]);
      toastr()->success('Logo SuccessFully updated!');
      return redirect()->back();
    }
    public function testingPdf(){

        $destinationPath = public_path('pdf_file');
        CloudConvert::file($destinationPath.'/index_versionlog.html')->to('pdf');

        //return response()->download($destinationPath,'index.pdf');
//        $ext='html';
//        $fileName=url('public/pdf_file/index.html');;
//        $url = "https://api.cloudconvert.com/convert";
//        $url .= "?apikey=SECEr24uKG4Cu6GBBHV2AYQ3j3y9C6CMjtqZV3Vr6lkyUwrrBXoLKhb9f3NUukZR";
//        $url .= "&inputformat=html&outputformat=pdf";
//        $url .= "&input=upload";
//        $url .= "&file=" . $fileName;
//        $url .= "&wait=true&download=inline";
//        $curl = curl_init($url);
//        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($curl, CURLOPT_POST, true);
//        $curl_response = curl_exec($curl);
//        //if ($curl_response === false) {
//            $info = curl_getinfo($curl);
//        //}
//            curl_close($curl);
//            echo "<pre>";print_r($curl_response);
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

      $companies = Company::getCompanies();
         $mnt = array('Jan',
                      'Feb',
                      'Mar',
                      'Apr',
                      'May',
                      'Jun',
                      'Jul',
                      'Aug',
                      'Sep',
                      'Oct',
                      'Nov',
                      'Dec'
                    );
        $jobs = Jobs::with('jobGroups.formTypes');
        $jobGroups = JobGroup::with('formTypes','jobs')->where('active',1)->where('deleted',0);
                $days = ['Sunday'=>0,'Monday'=>1,'Tuesday'=>2,'Wednesday'=>3,'Thursday'=>4,'Friday'=>5,'Saturday'=>6,];
                $dates = $jobGroups->get()->groupBy(function($item) {
            return Carbon::parse($item->created_at)->format('l');
         });
    if(!$jobs->get()->isEmpty())
    {
      FormScoreCount::truncate();
      foreach ($dates as $key => $date) {
        $types = $date->groupBy(function($item) {
        return $item->form_type_id;
      });
        //dd($types);
      foreach ($types as $key2 => $type)
      {
        $count =0;
        foreach ($type as $key3 => $x) {

          if($x->jobs ?? $x!=null)
          {
            if(isset($x->jobs->first()->post_time) && $x->jobs->first()->post_time>=$x->jobs->first()->updated_at)
            {
              $count++;
            }

          }
        }
       
        if($key2!=null)
        {
          
          $score = new FormScoreCount();
          $score->day_code =$days[$key];
          $score->total_completed = $type->where('completed',2)->count();
          $score->total = $type->count();
          $score->completed_late = $count;
          $score->completed_late = $count;
          $score->outstanding_jobs =$type->where('completed','!=',2)->count();
          $score->form_type_id = $key2;
          $score->save();
        }
      }
    }


       $responses = ResponseGroups::all();

        $months = $responses->groupBy(function($item) {
            return Carbon::parse($item->created_at)->format('m');
        });

        FormScoreLocation::truncate();
        foreach ($months as $key => $response) {


                 $locations = $response->groupBy(function($item) {
                return $item->location_id;
             });
                 foreach ($locations as $key2 => $location) {
                    $bonus_score = BonusScore::where('month',$key)->where('location_id',$key2)->get()->last();
                    $scoring =  Scoring::whereMonth('created_at',$key)->where('location_id',$key2)->where('form_id',$location[0]->form_id);
                    $scoreL = new FormScoreLocation();
                    $scoreL->count = $location->groupBy('form_id')->count();
                    $scoreL->location_id = $key2;
                    $scoreL->month = $key;
                    $scoreL->count_response_positive= $location->sum('pass_count');
                    $scoreL->count_response_negative= $location->sum('fail_count');
                    if((int)(JobGroup::where('location_id',$location->last()->location_id)->get()->last()->formTypes->action_score ?? 0)>0)
                    {

                    $scoreL->actions_created = JobGroup::where('location_id',$location->last()->location_id)->get()->count();
                     $scoreL->actions_total_score = JobGroup::where('location_id',$location->last()->location_id)->get()->last()->formTypes->action_score;

                    }else{
                      $scoreL->actions_created=0;
                      $scoreL->actions_total_score=0;
                    }

                    if((JobGroup::where('completed',2)->where('location_id',$location->last()->location_id)->get()->last()->formTypes->action_score ?? 0)>0)
                    {

                    $scoreL->actions_resolved = JobGroup::where('completed',2)->where('location_id',$location->last()->location_id)->get()->count();

                    }else{
                      $scoreL->actions_resolved=0;

                    }


                    if(JobGroup::where('completed','!=',2)->where('location_id',$location->last()->location_id)->get()->last()!=null)

                    {

                      if((JobGroup::where('completed','!=',2)->where('location_id',$location->last()->location_id)->get()->last()->formTypes->action_score ?? 0)>0)
                                        {

                                        $scoreL->actions_outstanding = JobGroup::where('completed','!=',2)->where('location_id',$location->last()->location_id)->get()->count();

                                        }else{

                                          $scoreL->actions_outstanding=0;
                                        }

                    }
                    else
                    {
                      $scoreL->actions_outstanding=0;
                    }

                    if($scoreL->action_outstanding==0)
                    {
                      if(JobGroup::where('location_id',$location->last()->location_id)->get()->last()!=null)

                          {  $scoreL->action_score = JobGroup::where('location_id',$location->last()->location_id)->get()->last()->formTypes->action_score ?? 0;
                          }
                    }
                    else
                    {

                      $scoreL->action_Score = (float)((int)$scoreL->action_outstanding/(int)$scoreL->action_resolved);
                    }


                    /*$scoreL->outstanding_count = $jobGroups->find($location[0]->job_group_id)->jobs[0]->whereMonth('post_time',$key)->where('completed',0)->get()->count();*/
                   /* $scoreL->late_completion_count = $jobs->whereMonth('post_time',$key)->where('completed',0)->get()->count();*/
                    if(!empty($bonus_score))
                    $scoreL->bonus_score = $bonus_score->bonus_score;
                    else
                    $scoreL->bonus_score = 0;

                    $scoreL->response_total = $location->sum('score');
                    $scoreL->response_score_total = $location->sum('total_score');
                    $scoreL->forms_required =$scoring->get()->sum('max_visits');
                    //$scoreL->forms_completed = $scoring->where('max_visits','>',0)->get()->count();
                    $scoreL->forms_completed = (JobGroup::where('completed',2)->where('location_id',$location->last()->location_id)->count());
                    $scoreL->outstanding_count = (JobGroup::where('completed','!=',2)->where('location_id',$location->last()->location_id)->count() ?? 0);
                    //$scoreL->form_score = (int) $scoreL->forms_completed *  $scoring->where('max_visits','>',0)->get()->sum('score');
                    if($scoreL->forms_completed>$scoreL->forms_required)
                    {
                    $scoreL->form_score = (int) $scoreL->forms_required *(int)$scoring->get()->sum('score') + (int)$scoreL->response_total;
                    }
                    else
                    {
                         $scoreL->form_score = (int) $scoreL->forms_completed *(int)$scoring->get()->sum('score')+$scoreL->response_total;

                    }
                    $scoreL->form_score_total = (int) $scoreL->forms_required *  $scoring->where('max_visits','>',0)->get()->sum('score')+$scoreL->response_score_total;

                    $scoreL->score =((int)$scoreL->bonus_score+(int)$scoreL->form_score+(int)$scoreL->response_total+(int)$scoreL->action_score);
                    $scoreL->max_score = ((int)$scoreL->bonus_score+(int)$scoreL->form_score_total+(int)$scoreL->response_score_total+(int)$scoreL->actions_total_score);
                    $scoreL->company_id = auth()->user()->default_company;
                    $scoreL->save();
                 }
        }




    }




        $formCounts = FormScoreCount::with('formType')->get()->groupBy('form_type_id');
        $formLocations = FormScoreLocation::with('location')->get();
        $maxScore = $formLocations->max('score');
        $averageScore = round($formLocations->average('score'));
        $locations = $formLocations->pluck('location')->pluck('name','id');
        $filter = FormType::where('_show_dashboard',1)->where('active',1)->where('deleted',0)->get()->pluck('name','id');
        $count_positive = $formLocations->sum('count_response_positive');
        $count_negative = $formLocations->sum('count_response_negative');
        $action_resolved = $formLocations->sum('action_resolved');
        $action_created = $formLocations->sum('actions_created');
        $count = $formLocations->sum('count');
        $completed = $formLocations->sum('forms_completed');
         $api = url('/chart-line-ajax');

        $chart = new UserLineChart;
        $chart->labels(['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'])->load($api);

        $apiW = url('/chart-line-ajax-week');

        $chart_week = new UserLineChart;
        $chart_week->labels(['1', '5', '10', '15', '20', '25', '31'])->load($apiW);


         $apiM = url('/chart-line-ajax-month');

        $chart_month = new UserLineChart;
        $chart_month->labels(['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'])->load($apiM);


       $apiL = url('/chart-line-ajax-late');

        $chart_late = new UserLineChart;
        $chart_late->labels(['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'])->load($apiL);


        $apiO = url('/chart-line-ajax-out');

        $chart_out = new UserLineChart;
        $chart_out->labels(['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'])->load($apiO);

          $apiLm = url('/chart-line-ajax-lm');

        $chart_lm = new UserLineChart;
        $chart_lm->labels(['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun'])->load($apiLm);


          $apiD = url('/chart-line-ajax-day');

        $chart_day = new UserLineChart;
        $chart_day->labels( ["Mon", "Tue", "Wed", "Thur", "Fri", "Sat", "Sun"])->load($apiD);


        return view('backend.dashboard')->with(['formCounts'=>$formCounts,'formLocations'=>$formLocations,'locations'=>$locations,'chart'=>$chart,'count_positive'=>$count_positive,'count_negative'=>$count_negative,'action_resolved'=>$action_resolved,'action_created'=>$action_created,'count'=>$count,'completed'=>$completed,'chart_week'=>$chart_week,'chart_month'=>$chart_month,'chart_late'=>$chart_late,'chart_out'=>$chart_out,'chart_lm'=>$chart_lm,'chart_day'=>$chart_day,'mnt'=>$mnt,'filter'=>$filter,'companies'=>$companies,'maxScore'=>$maxScore,'averageScore'=>$averageScore]);
    }

    public function chartLineAjax(Request $request)
    {

        $ids = $request->has('ids') ? $request->ids : [];
        if($ids!=null)
            $ids = json_decode($ids);
        if(!empty($ids))
        {

       $formLocations = FormScoreLocation::whereIn('location_id',$ids->location_id)->whereBetween('month', [Carbon::parse($ids->from)->month,Carbon::parse($ids->to)->month])->with('location')->get();
        }
        else
        $formLocations = FormScoreLocation::with('location')->get();

        $scoreData = $formLocations->groupBy('location_id');


        $chart = new UserLineChart;
        $chart->labels(['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']);
        foreach ($scoreData as $key => $value) {

       // dd($value->pluck('location')[0]->name);

        $chart->title('locationCharts',14,'#fff')->dataset($value->pluck('location')[0]->name, 'line', $value->pluck('score')->toArray())->options([
                    'fill' => 'true',
                    'color'=>'#fff',
                    'borderColor' => 'rgba(200, 99, 132, .7)',
                    'borderWidth' => '2',
                    'backgroundColor' => 'rgba(0, 137, 132, .2)',
                    'gridLines' => ['display'=>'true','color'=>'rgba(255,255,255,.25)'],
                    'ticks'=>['fontColor'=>'#fff']

                ]);
                }


        return $chart->api();
    }


    public function chartLineAjaxMonth(Request $request)
    {
        $ids = $request->has('ids') ? $request->ids : [];
        $type = $request->has('type') ? $request->type : 0;
        $mnt = array('Jan',
                      'Feb',
                      'Mar',
                      'Apr',
                      'May',
                      'Jun',
                      'Jul',
                      'Aug',
                      'Sep',
                      'Oct',
                      'Nov',
                      'Dec'
                    );

        if($ids!=null)
        $ids = array_map('intval', explode(',', $ids));
        if($type!=0)
        {
        $formCounts = FormScoreCount::with('formType')->where('form_type_id',$type)->get();
        }
        else
        $formCounts = FormScoreCount::with('formType')->get();

        $chart = new UserLineChart;
        $chart->labels(['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']);
        $q = new SplFixedArray(12);
        $q = array_fill(0,12,0);
        $jobGroups = JobGroup::with('formTypes','jobs');
                 $dates = $jobGroups->get()->groupBy(function($item) {
            return Carbon::parse($item->created_at)->format('m');
         });
        foreach ($dates as $key => $date) {

                $types = $date->groupBy(function($item) {
                return $item->form_type_id;
             });
           foreach ($types as $key2 => $value) {
              if($type!=0)
              {


                if($type==$key2)
                {

                  $q[((int)$key-1)]=$value->where('completed',2)->count();
                  $chart->title('locationCharts',14,'#fff')->dataset($value->first()->formTypes->name,'line',$q)->options([
                              'fill' => 'true',
                              'color'=>'#fff',
                              'borderColor' => 'rgba(200, 99, 132, .7)',
                              'borderWidth' => '2',
                              'backgroundColor' => 'rgba(0, 137, 132, .2)',
                              'gridLines' => ['display'=>'true','color'=>'rgba(255,255,255,.25)'],
                              'ticks'=>['fontColor'=>'#fff']
                          ]);

                      }
                    }else
                    {
                      $q[((int)$key-1)]=$value->where('completed',2)->count();
                  $chart->title('locationCharts',14,'#fff')->dataset($value->first()->formTypes->name ?? 'no data' ?? 'no data','line',$q)->options([
                              'fill' => 'true',
                              'color'=>'#fff',
                              'borderColor' => 'rgba(200, 99, 132, .7)',
                              'borderWidth' => '2',
                              'backgroundColor' => 'rgba(0, 137, 132, .2)',
                              'gridLines' => ['display'=>'true','color'=>'rgba(255,255,255,.25)'],
                              'ticks'=>['fontColor'=>'#fff']
                          ]);
                    }
            }
              }

        return $chart->api();
    }

     public function chartLineAjaxLate(Request $request)
    {
        $ids = $request->has('ids') ? $request->ids : [];
         $type = $request->has('type') ? $request->type : 0;
        if($type!=0)
        {
        $formCounts = FormScoreCount::with('formType')->where('form_type_id',$type)->get();
        }
        else
        $formCounts = FormScoreCount::with('formType')->get();

        $chart = new UserLineChart;
        $chart->labels(['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']);

        //dd($formCounts);
        $q = new SplFixedArray(12);
        $q = array_fill(0,12,0);
        $jobGroups = JobGroup::with('formTypes','jobs');
                 $dates = $jobGroups->get()->groupBy(function($item) {
            return Carbon::parse($item->created_at)->format('m');
         });
         //dd($dates);
        foreach ($dates as $key => $date) {

                $types = $date->groupBy(function($item) {
                return $item->form_type_id;
             });

                //dd($types);
           foreach ($types as $key2 => $value) {
                     $count =0;
                       foreach ($value as $key3 => $x) {
                            if($x!=null)
                            {
                                if(isset($x->jobs->first()->post_time) && $x->jobs->first()->post_time>=$x->jobs->first()->updated_at)
                                {
                                  $count++;
                                }
                            }

                      }
           if($type!=0)
              {


                if($type==$key2)
                {

                  $q[((int)$key-1)]=$count;
                  $chart->title('locationCharts',14,'#fff')->dataset($value->first()->formTypes->name ?? 'no data','line',$q)->options([
                              'fill' => 'true',
                              'color'=>'#fff',
                              'borderColor' => 'rgba(200, 99, 132, .7)',
                              'borderWidth' => '2',
                              'backgroundColor' => 'rgba(0, 137, 132, .2)',
                              'gridLines' => ['display'=>'true','color'=>'rgba(255,255,255,.25)'],
                              'ticks'=>['fontColor'=>'#fff']
                          ]);

                      }
                    }else
                    {
                      $q[((int)$key-1)]=$count;
                  $chart->title('locationCharts',14,'#fff')->dataset($value->first()->formTypes->name ?? 'no data','line',$q)->options([
                              'fill' => 'true',
                              'color'=>'#fff',
                              'borderColor' => 'rgba(200, 99, 132, .7)',
                              'borderWidth' => '2',
                              'backgroundColor' => 'rgba(0, 137, 132, .2)',
                              'gridLines' => ['display'=>'true','color'=>'rgba(255,255,255,.25)'],
                              'ticks'=>['fontColor'=>'#fff']
                          ]);
                    }

                }
              }

        return $chart->api();
    }
     public function chartLineAjaxOut(Request $request)
    {
              $ids = $request->has('ids') ? $request->ids : [];
             $type = $request->has('type') ? $request->type : 0;
            if($ids!=null)
            $ids = array_map('intval', explode(',', $ids));
             if($type!=0)
            {
            $formCounts = FormScoreCount::with('formType')->where('form_type_id',$type)->get();
            }
            else
            $formCounts = FormScoreCount::with('formType')->get();
            $chart = new UserLineChart;
            $chart->labels(['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']);
            $q = new SplFixedArray(12);
            $q = array_fill(0,12,0);
            $jobGroups = JobGroup::with('formTypes','jobs');
                     $dates = $jobGroups->get()->groupBy(function($item) {
                return Carbon::parse($item->created_at)->format('m');
             });
            foreach ($dates as $key => $date) {
                    $types = $date->groupBy(function($item) {
                    return $item->form_type_id;
                 });
               foreach ($types as $key2 => $value) {
                if($type!=0)
              {


                if($type==$key2)
                {

                  $q[((int)$key-1)]=$value->where('completed','!=',2)->count();
                  $chart->title('locationCharts',14,'#fff')->dataset($value->first()->formTypes->name ?? 'no data','line',$q)->options([
                              'fill' => 'true',
                              'color'=>'#fff',
                              'borderColor' => 'rgba(200, 99, 132, .7)',
                              'borderWidth' => '2',
                              'backgroundColor' => 'rgba(0, 137, 132, .2)',
                              'gridLines' => ['display'=>'true','color'=>'rgba(255,255,255,.25)'],
                              'ticks'=>['fontColor'=>'#fff']
                          ]);

                      }
                    }else
                    {
                      $q[((int)$key-1)]=$value->where('completed',2)->count();
                  $chart->title('locationCharts',14,'#fff')->dataset($value->first()->formTypes->name ?? 'no data','line',$q)->options([
                              'fill' => 'true',
                              'color'=>'#fff',
                              'borderColor' => 'rgba(200, 99, 132, .7)',
                              'borderWidth' => '2',
                              'backgroundColor' => 'rgba(0, 137, 132, .2)',
                              'gridLines' => ['display'=>'true','color'=>'rgba(255,255,255,.25)'],
                              'ticks'=>['fontColor'=>'#fff']
                          ]);
                    }

                    }
                  }
            return $chart->api();
    }
     public function chartLineAjaxLm(Request $request)
    {
        $ids = $request->has('ids') ? $request->ids : [];
         $type = $request->has('type') ? $request->type : 0;
        if($ids!=null)
        $ids = array_map('intval', explode(',', $ids));
            if($type!=0)
        {
        $formCounts = FormScoreCount::with('formType')->where('form_type_id',$type)->get();
        }
        else
        $formCounts = FormScoreCount::with('formType')->get();
        $chart = new UserLineChart;
        $chart->labels(['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']);
         $q = new SplFixedArray(12);
        $q = array_fill(0,12,0);
        $jobGroups = JobGroup::with('formTypes','jobs');
                 $dates = $jobGroups->get()->groupBy(function($item) {
            return Carbon::parse($item->created_at)->format('m');
         });
        foreach ($dates as $key => $date) {
                $types = $date->groupBy(function($item) {
                return $item->form_type_id;
             });
           foreach ($types as $key2 => $value) {
            if($type!=0)
              {


                if($type==$key2)
                {

                  $q[((int)$key-1)]=$value->where('completed',2)->count();
                  $chart->title('locationCharts',14,'#fff')->dataset($value->first()->formTypes->name ?? 'no data','line',$q)->options([
                              'fill' => 'true',
                              'color'=>'#fff',
                              'borderColor' => 'rgba(200, 99, 132, .7)',
                              'borderWidth' => '2',
                              'backgroundColor' => 'rgba(0, 137, 132, .2)',
                              'gridLines' => ['display'=>'true','color'=>'rgba(255,255,255,.25)'],
                              'ticks'=>['fontColor'=>'#fff']
                          ]);

                      }
                    }else
                    {
                      $q[((int)$key-1)]=$value->where('completed',2)->count();
                  $chart->title('locationCharts',14,'#fff')->dataset($value->first()->formTypes->name ?? 'no data','line',$q)->options([
                              'fill' => 'true',
                              'color'=>'#fff',
                              'borderColor' => 'rgba(200, 99, 132, .7)',
                              'borderWidth' => '2',
                              'backgroundColor' => 'rgba(0, 137, 132, .2)',
                              'gridLines' => ['display'=>'true','color'=>'rgba(255,255,255,.25)'],
                              'ticks'=>['fontColor'=>'#fff']
                          ]);
                    }
                }
              }

        return $chart->api();
    }

   public function chartLineAjaxDay(Request $request)
    {
        $days = ['Sunday'=>0,'Monday'=>1,'Tuesday'=>2,'Wednesday'=>3,'Thursday'=>4,'Friday'=>5,'Saturday'=>6,];
        $ids = $request->has('ids') ? $request->ids : [];
         $type = $request->has('type') ? $request->type : 0;
        if($ids!=null)
        $ids = array_map('intval', explode(',', $ids));
         if($type!=0)
        {
        $formCounts = FormScoreCount::with('formType')->where('form_type_id',$type)->get();
        }
        else
        $formCounts = FormScoreCount::with('formType')->get();
        $chart = new UserLineChart;
        $chart->labels(["Mon", "Tue", "Wed", "Thur", "Fri", "Sat", "Sun"]);
        $q = new SplFixedArray(7);
        $q = array_fill(0,7,0);
        $jobGroups = JobGroup::with('formTypes','jobs');
                 $dates = $jobGroups->get()->groupBy(function($item) {
            return Carbon::parse($item->created_at)->format('l');
         });
                  foreach ($dates as $key => $date) {
                    $types = $date->groupBy(function($item) {
                    return $item->form_type_id;
                 });
              foreach ($types as $key2 => $value) {
                  if($type!=0)
                  {
                      if($type==$key2)
                      {

                  $q[((int)($days[$key])-1)]=$value->where('completed',2)->count();
        $chart->title('locationCharts',14,'#fff')->dataset($value->first()->formTypes->name ?? 'no data', 'horizontalBar',$q)->options([
                    'fill' => 'true',
                    'color'=>'#fff',
                    'borderColor' => 'rgba(200, 99, 132, .7)',
                    'borderWidth' => '2',
                    'backgroundColor' => 'rgba(0, 137, 132, .2)',
                    'gridLines' => ['display'=>'true','color'=>'rgba(255,255,255,.25)'],
                    'ticks'=>['fontColor'=>'#fff']

                ]);

                      }
                  }else
                  {
         $q[((int)($days[$key])-1)]=$value->where('completed',2)->count();
        $chart->title('locationCharts',14,'#fff')->dataset($value->first()->formTypes->name?? 'no data', 'horizontalBar',$q)->options([
                    'fill' => 'true',
                    'color'=>'#fff',
                    'borderColor' => 'rgba(200, 99, 132, .7)',
                    'borderWidth' => '2',
                    'backgroundColor' => 'rgba(0, 137, 132, .2)',
                    'gridLines' => ['display'=>'true','color'=>'rgba(255,255,255,.25)'],
                    'ticks'=>['fontColor'=>'#fff']

                ]);
                  }
        }
         //dd($q,$formCounts);
        return $chart->api();
    }

  }



     public function chartLineAjaxWeek(Request $request)
    {
         $type = $request->has('type') ? $request->type : 0;
 
         $ids = $request->has('ids') ? $request->ids : [];
        if($ids!=null)
            $ids = json_decode($ids);
        if(!empty($ids))
        {

       $formLocations = FormScoreLocation::whereIn('location_id',$ids->location_id)->whereBetween('month', [Carbon::parse($ids->from)->month,Carbon::parse($ids->to)->month])->with('location')->get();
        }
        else
        $formLocations = FormScoreLocation::with('location')->get();

        $scoreData = $formLocations->groupBy('location_id');


        $chart_week = new UserLineChart;
        $chart_week->labels(['1', '5', '10', '15', '20', '25', '31']);
        foreach ($scoreData as $key => $value) {

       // dd($value->pluck('location')[0]->name);

        $chart_week->title('locationCharts',14,'#fff')->dataset($value->pluck('location')[0]->name ?? 'no data', 'line', $value->pluck('count')->toArray())->options([
                    'fill' => 'true',
                    'color'=>'#fff',
                    'borderColor' => 'rgba(200, 99, 132, .7)',
                    'borderWidth' => '2',
                    'backgroundColor' => 'rgba(0, 137, 132, .2)',
                    'gridLines' => ['display'=>'true','color'=>'rgba(255,255,255,.25)'],
                    'ticks'=>['fontColor'=>'#fff']

                ]);
                }


        return $chart_week->api();
    }




    public function getDataLocationMain(Request $request)
    {
        $input = $request->all();

        if(is_array($input['location_id']))
        {

        $formLocations = FormScoreLocation::whereIn('location_id',$input['location_id'])->get();
        }else
        {
        $formLocations = FormScoreLocation::where('location_id',$input['location_id'])->get();
        }
        $dates = $formLocations->pluck('created_at');
        $maxScore = $formLocations->max('score');
        $averageScore = round($formLocations->average('score'));
/*        $formsRequired =   $formLocations->pluck('max_score')->toArray();
        $formsCompleted =  $formLocations->pluck('score')->toArray();*/
    /*   $percent =array_map(function ($a, $b) {return round(($a/$b)*100);}, $formsCompleted, $formsRequired);*/
      //sort($percent);
          $old = FormScoreLocation::where('month',Carbon::parse($input['from'])->month)->get()->last();
           $new = FormScoreLocation::where('month',Carbon::parse($input['to'])->month)->get()->last();

         if($old!=null)
         {
          $old = $old->form_score_total;
         }
         else
         {
          $old=0;
         }

          if($new!=null)
         {
          $new = $new->form_score_total;
         }
         else
         {
          $new=0;
         }

           if((int)$old>0)
           {
           $percent = (((int)$new-(int)$old)/(int)$old)*100;
           }
           else
           {
            $percent = 0;
           }
           $dir = false;
           if($percent>0)
           {
            $dir=true;
           }
        $scoreData = $formLocations->groupBy('location_id');
        $score=[];
        $cdate=[];
          foreach ($scoreData as $key => $value) {

             $scoreData[$key] = $value->pluck('score');
             $selectedDate[$key]= $value->pluck('created_at');
              array_push($score,$scoreData[$key]);
              array_push($cdate,$selectedDate[$key]);

          }
        return response()->json(['maxScore'=>$maxScore,'averageScore'=>$averageScore,'score'=>$score,'cdate'=>$cdate,'dates'=>$dates,'dir'=>$dir,'percent'=>$percent]);

    }

    public function getAllForms(Request $request)
    {

         $ids = $request->has('ids') ? $request->ids : [];

        if($ids!=null)
        $ids =  explode('.', $ids);
      // dd( $ids[0]);
        if(empty($ids))
        {
         $data=JobGroup::with('users','forms','locations','cJobs','locationBreakdowns','responseGroups','formTypes')->where('active',1)->where('deleted',0)->whereYear('date_time', Carbon::now()->year)->whereMonth('date_time', Carbon::now()->month)->get();
          }
          else {

            $data=JobGroup::with('users','forms','locations','cJobs','locationBreakdowns','responseGroups','formTypes')->where('active',1)->where('deleted',0)->whereBetween('date_time', [Carbon::parse($ids[0])->format('Y-m-d')." 00:00:00",Carbon::parse($ids[1])->format('Y-m-d')." 23:59:59"])->get();
          }
            foreach ($data as $key => $value) {
                if($value->cJobs->isEmpty())
                {
                    unset($data[$key]);
                }
                  if($value->sign_off==1)
                {
                     unset($data[$key]);
                }

            }
        
    //dd($data);
          return Datatables::of($data)
                ->addColumn('checkbox', function ($data) {

                })
                ->addColumn('employee_name', function ($data) {
                     $response = "";
                        $response = $data->users->first_name ?? "";
                    return $response;
                }) ->addColumn('location_name', function ($data) {
                   $response = "";

                    $response = $data->locations->name ?? "";
                  return $response;
                }) ->addColumn('location_breakdown', function ($data) {
                   $response = "";

                    $response = $data->locationBreakdowns->name ?? "";
                  return $response;
                })->addColumn('form_name', function ($data) {
                     $response = "";

                        $response = $data->forms->name ?? "";
                    return $response;
                })->addColumn('date_time', function ($data) {
                     $response = "";
                   if(isset($data->jobs[0]->date_time))
                    $response =Carbon::parse( $data->jobs[0]->date_time)->format('jS M Y H:i');
                        return $response;
                })->addColumn('score', function ($data) {
                     $response = "";
                   if(!($data['responseGroups']->isEmpty()))

                   {
                     //dd($data['responseGroups']->sum('score'));
                    $response =$data['responseGroups']->sum('score');}
                        return $response;
                })->addColumn('form_type_score', function ($data) {
                     $response = "";
                   if($data->formTypes)
                    $response =$data->formTypes->action_score;
                        return $response;
                })->addColumn('form_score', function ($data){
                     $response = "";
                     $score = Scoring::where('form_id',$data->form_id)->where('active',1)->where('deleted',0)->where('location_id',$data->location_id)->get()->last();
                     //dd($score);
                       if($score)
                        $response =$score->score;
                        return $response;
                })
                ->addColumn('actions', function ($data) {
                    $response = "";
                    $view = '<a type="button" class="btn btn-primary btn-xs my-0 waves-effect waves-light" href="'.url("/completed_forms/viewDocument").'/'.$data->id.'"><i class="fas fa-eye"></i></a>';
                    $response = $response . $view;
                    return $response;
                })
                ->rawColumns(['actions'])
                ->make(true);
    }

    public function getAllFormScore(Request $request)
    {
         $days = [0=>'Sunday',1=>'Monday',2=>'Tuesday',3=>'Wednesday',4=>'Thursday',5=>'Friday',6=>'Saturday'];

        $data=FormScoreCount::with('formType','location')->where('active',1)->where('deleted',0)->orderBy('created_at', 'desc')->get();

            return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {
            })
            ->addColumn('form_type', function ($data) {
                    $response = $data->formType->name ?? "";
                return $response;
            })->addColumn('day_code', function ($data) use ($days) {
                $response = $days[$data->day_code];
                return $response;
            })->addColumn('total_completed', function ($data) {
                $response = $data->total_completed;
                    return $response;
            })
            ->addColumn('total', function ($data) {
              $response = $data->total;
                return $response;
            })
            ->make(true);
    }


    public function getAllFormScoreLocation(Request $request)
    {
        $mnt = array('Jan',
                      'Feb',
                      'Mar',
                      'Apr',
                      'May',
                      'Jun',
                      'Jul',
                      'Aug',
                      'Sep',
                      'Oct',
                      'Nov',
                      'Dec'
                    );
        $data=FormScoreLocation::with('formType','location')->where('active',1)->where('deleted',0)->orderBy('created_at', 'desc')->get();

            return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {
            })
            /*->addColumn('form_type', function ($data) {
                    $response = $data->formType->name;
                return $response;
            })*/->addColumn('location', function ($data) {
                $response= $data->location->name ?? "" ;
                return $response;
            })->addColumn('month', function ($data) use ($mnt) {
                $response = $mnt[($data->month)-1] ?? "";
                    return $response;
            })->addColumn('outstanding_count', function ($data){
                  $response=0;
                if($data->count>0 && $data->outstanding_count>0)
                {
                    $response = round(($data->outstanding_count/$data->count)*100);
                }
                return $response;
            })->addColumn('action_resolved', function ($data){
                  $response=0;
                if($data->action_created>0 && $data->action_resolved>0)
                {
                    $response = round(($data->action_resolved/$data->action_created)*100);
                }
                return $response;
            })->addColumn('bonus_score', function ($data){
                $response = $data->bonus_score ;
                    return $response;
            })
            ->addColumn('forms_completed', function ($data) {
                $response=0;
                if($data->forms_completed>0 && $data->forms_required>0)
                {
                    $response = round(($data->forms_completed/$data->forms_required)*100);
                }
                return $response;
            })->addColumn('response_total', function ($data) {
              $response = $data->response_total;
                return $response;
            })->addColumn('response_score_total', function ($data) {
              $response = $data->response_score_total;
                return $response;
            })->addColumn('score', function ($data) {
              $response = $data->score;
                return $response;
            })->addColumn('max_score', function ($data) {
              $response = $data->max_score;
                return $response;
            })->addColumn('actions', function ($data) {
                    $response = "";
                    $view = '<a type="button" class="btn btn-primary btn-xs my-0 waves-effect waves-light" href="'.url("/project/completed_forms").'/'.$data->location->id.'"><i class="fas fa-eye"></i></a>';
                    $response = $response . $view;
                    return $response;
                })
                ->rawColumns(['actions'])
            ->make(true);
    }



    public function getLocationData($id)
    {

        $data = FormScoreLocation::with('location')->find($id);
        return response()->json($data);

    }







}
