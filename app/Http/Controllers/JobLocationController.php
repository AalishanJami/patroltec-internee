<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\SiteContacts;
use Spatie\Permission\Models\Role;
use App\Company;
use App\Divisions;
use App\Location;
use App\LocationBreakdown;
use Session;
use Carbon\Carbon;
use Auth;
use App\Exports\division\DivisionExport;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use PDF;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Hash;
use Validator;
use DataTables;
class JobLocationController extends Controller
{
    public function getAll($id)
    {
        $data=LocationBreakdown::where('active',1)->where('deleted',0)->where('location_id',$id)->where('type','m')->get();
        return Datatables::of($data)->with(['break'=>$data])
            ->addColumn('checkbox', function ($data) {
            })
            ->addColumn('submit', function ($data) {
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                $delete = '<a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light" onclick="deletelocationdata( ' . $data->id . ')"><i class="fas fa-trash"></i></a>';
                $response = $response . $delete;
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);

    }
    public function getAllSoft($id)
    {
        $data=LocationBreakdown::where('active',0)->where('deleted',0)->where('location_id',$id)->where('type','m')->get();
        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {
            })
            ->addColumn('submit', function ($data) {
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                $restore_button = '<button type="button"class="btn btn-success btn-xs my-0" onclick="restorelocationdata('.$data->id.')"><i class="fa fa-retweet" aria-hidden="true"></i></button>';
                $hard_delete_button = '<button type="button"class="btn btn-danger btn-xs my-0" onclick="deletesoftlocationdata('.$data->id.')"><i class="fa fa-trash" aria-hidden="true"></i></button>';
                $response = $response . $restore_button;
                $response = $response . $hard_delete_button;
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
    public function store($id)
    {
        $data=LocationBreakdown::create(['name'=>'','type'=>'m','location_id'=>$id]);
        return response()->json(['data'=>$data]);
    }
    public function update(Request $request)
    {
   /*     if (isset($request->name)){
            $data = [
                'name'=>'required',
//                'start'=>'date',
//                'end'=>'date',
            ];
        }else{
          
        }
        $validator = Validator::make($request->all(), $data);
        if ($validator->fails())
        {
            $error = $validator->messages();
            $html='<ul>';
            foreach ($error->all() as $key => $value) {
                $html=$html.'<li>'.$value.'</li>';
            }
            $html=$html.'</ul>';
            return response()->json(['status'=> false, 'error'=> $html]);
        }*/
        $data=$request->all();
        if (isset($request->name)){
            $data['name']=$request->name;
        }else{
            $data['name']='';
        }
        unset($data['company_id']);
        $Divisions=LocationBreakdown::where('id',$data['id']);
        $Divisions_first=$Divisions->first();
        $data['type']='m';
        $Divisions->update($data);
        if($Divisions_first->active == 0)
        {
            $flag=0;
        }
        else
        {
            $flag=1;
        }

        return response()->json(['message'=>'Successfully Added !!','flag'=>$flag]);

    }
    public function softDelete (Request $request)
    {
        LocationBreakdown::where('id',$request->id)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function delete(Request $request)
    {
        LocationBreakdown::where('id',$request->id)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function active(Request $request)
    {
        LocationBreakdown::where('id',$request->id)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
    public function activeMultiple(Request $request)
    {
        LocationBreakdown::whereIn('id',$request->ids)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
     public function deleteMultipleSOft(Request $request)
    {
        LocationBreakdown::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function deleteMultiple(Request $request)
    {
        LocationBreakdown::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function exportPdf(Request $request)
    {
        if($request->pdf_array == 1)
        {
            $data =Divisions::where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->pdf_array);
            $data= Divisions::whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        $date=Carbon::now()->format('D jS M Y');
        $companies='Patroltec';
        $reportname='Divisions';
        $pdf = PDF::loadView('backend.divisions.pdf',compact('data','date','reportname','companies'));
        return $pdf->download('divisions.pdf');
    }
    public function exportWorld(Request $request)
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        if($request->word_array == 1)
        {
            $data= Divisions::where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->word_array);
            $data= Divisions::whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        $table = $section->addTable();
        foreach ($data as $key => $value) {
            if($key==0)
            {
                $table->addRow();
                if(Auth()->user()->hasPermissionTo('division_name_pdf_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Name');
                }
            }
            $table->addRow();
            if(Auth()->user()->hasPermissionTo('division_name_pdf_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText($value->name);
            }
        }
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('divisions.docx');
        return response()->download(public_path('divisions.docx'));
    }
    public function exportExcel(Request $request)
    {
        if($request->excel_array == 1)
        {
            $data= Divisions::where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->excel_array);
            $data= Divisions::whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        return Excel::download(new DivisionExport($data), 'divisions.csv');
    }
}
