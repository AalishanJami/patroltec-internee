<?php

namespace App\Http\Controllers\backend;
use App\Exports\note\NoteExport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\SiteContacts;
use App\ContactsType;
use Spatie\Permission\Models\Role;
use App\Company;
use App\Client;
use App\Clients;
use App\Notes;
use App\CompanyUser;
use App\Upload;
use Auth;
use App\Exports\contact\contactExport;
use App\Exports\contact\contactExportSoft;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use PDF;
use DataTables;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Hash;
use Session;
use Validator;
use App\Exports\Client\ClientExport;
use Config;
class ClientController extends Controller
{
    private $page_name;
    private $tab_name;
    private $db;
    private $cms_array=[];
    public function __construct() {
        $this->page_name ='client';
        $this->tab_name ='main_detail';
        Session::forget('project_id');
        Session::forget('employee_id');
        Session::forget('form_id');
        Session::forget('folder_id');
        $this->db='clients';
        $cms_data=localization();
        $temp_array=[];
        $temp_array['title']['key']='name';
        $temp_array['title']['value']=checkKey('name',$cms_data);
        $temp_array['email']['key']='email';
        $temp_array['email']['value']=checkKey('email',$cms_data);
        $temp_array['first_name']['key']='first_name';
        $temp_array['first_name']['value']=checkKey('first_name',$cms_data);
        $temp_array['surname']['key']='surname';
        $temp_array['surname']['value']=checkKey('surname',$cms_data);
        $temp_array['phone_number']['key']='phone';
        $temp_array['phone_number']['value']=checkKey('phone',$cms_data);
        $temp_array['address']['key']='address';
        $temp_array['address']['value']=checkKey('address',$cms_data);
        $this->cms_array=$temp_array;
    }
    public function index()
    {
        $companies=Company::getCompanies();
        return view('backend.clients.index')->with(['companies'=>$companies]);
    }
    public function store()
    {
        $data=Clients::create(['first_name'=>'','company_id'=>Auth::user()->default_company]);
        audit_log($this->cms_array,'',Config::get('constants.store'),$this->page_name,$this->tab_name,$data->id,'',$data->id,'',$this->db);
        return response()->json(['data'=>$data]);
    }

    public function storeMain()
    {
        $data=Clients::create(['first_name'=>'','company_id'=>Auth::user()->default_company]);
        audit_log($this->cms_array,'',Config::get('constants.store'),$this->page_name,$this->tab_name,$data->id,'',$data->id,'',$this->db);
        return response()->json(['data'=>$data]);
    }
    public function getAllSoftClientMain()
    {
        $data =Clients::where('company_id',Auth::user()->default_company)->where('active',0)->where('deleted',0)->orderBy('id', 'desc')->get();
        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {
            })->addColumn('actions', function ($data) {
                $response = "";
                if(Auth()->user()->hasPermissionTo('restore_delete_client') || Auth::user()->all_companies == 1 ){
                    $restore_button = '<button type="button"class="btn btn-success btn-xs my-0" onclick="restoreClient('.$data->id.')"><i class="fa fa-retweet" aria-hidden="true"></i></button>';
                }else{
                    $restore_button = '';
                }
                if(Auth()->user()->hasPermissionTo('hard_delete_client') || Auth::user()->all_companies == 1 ){
                    $hard_delete_button = '<button type="button"class="btn btn-danger btn-xs my-0" onclick="deletesoftclientdata('.$data->id.')"><i class="fa fa-trash" aria-hidden="true"></i></button>';
                }else{
                    $hard_delete_button ='';
                }
                $response = $response.$hard_delete_button;
                $response = $response.$restore_button;
                return $response;
            })->addColumn('submit', function ($data) {
            })->rawColumns(['actions'])->make(true);
    }

    public function restore(Request $request)
    {
        audit_log($this->cms_array,'',Config::get('constants.restore'),$this->page_name,$this->tab_name,'','',$request->id,'',$this->db);
        Clients::where('id',$request->id)->update(['active'=>1]);
        return response()->json(['message'=>'restore']);

    }

    public function createDetail()
    {
        $data=Clients::create(['first_name'=>'','company_id'=>Auth::user()->default_company]);
    audit_log($this->cms_array,'',Config::get('constants.store'),$this->page_name,$this->tab_name,$data->id,'',$data->id,'',$this->db);
        return $this->detail($data->id);
    }
    public function detail($id)
    {
        Session::put('client_id',$id);
        $client = Clients::where('id',$id)->where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->first();
        if(!$client)
        {
            // toastr()->success('');
            return redirect('client');
        }
        $companies=Company::getCompanies();
        return view('backend.clients.details')->with(['client'=>$client,'companies'=>$companies]);
    }

    public function getAllClientMain()
    {
        $data =Clients::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        return Datatables::of($data)->addColumn('checkbox', function($data){})->addColumn('actions',function($data){
            $response="";
            if(Auth()->user()->hasPermissionTo('edit_client') || Auth::user()->all_companies == 1 ){
                $edit ='<a href="'.url("/client/detail/".$data->id).'" class="btn btn-primary btn-xs mr-2"><i class="fas fa-pencil-alt"></i></a>';
            }else{
                $edit ='';
            }
            if(Auth()->user()->hasPermissionTo('delete_client') || Auth::user()->all_companies == 1 ){
                $delete ='<a class="btn deleteclientdata'.$data->id .' btn-danger btn-xs mr-2" onclick="deleteclientdata( '.$data->id . ')"><i class="fas fa-trash"></i></a>';
            }else{
                $delete ='';
            }
            $response=$response.$edit;
            $response=$response.$delete;
            return $response;
        })->rawColumns(['actions'])->make(true);
    }
    public function updateDetail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'max:10,required',
            // 'first_name' => 'required',
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $input = $request->all();
        unset($input['_token']);
        $client=Clients::find($input['id']);
        audit_log($this->cms_array,audit_log_data($input),Config::get('constants.update'),$this->page_name,$this->tab_name,$input['id'],audit_log_data($client->toArray()),$input['id'],'',$this->db);
        $client->update($input);
        return redirect()->route('detail',['id'=>$input['id']]);
    }

    public function getAll()
    {
        $data =Clients::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {
            })
            ->addColumn('submit', function ($data) {
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                $delete = '<a type="button" class="btn deleteclientdata'.$data->id .' btn-danger btn-xs my-0 waves-effect waves-light delete-btn' . $data->id . '" onclick="deleteclientmodaldata( ' . $data->id . ')"><i class="fas fa-trash"></i></a>';
                $response = $response . $delete;
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    public function getAllSoft()
    {
        $data=Clients::where('company_id',Auth::user()->default_company)->where('active',0)->where('deleted',0)->get();
        return response()->json(['data'=>$data,'flag'=>0]);
    }
    public function storeDetail()
    {
        $data=Clients::create();
        return redirect()->back();
    }

    public function update(Request $request)
    {
        if (isset($request->email)){
            $data = [
                'company_id'=>'required|numeric',
                'email'=>'required|email',
            ];
        }else{
            $data = [
                'company_id'=>'required|numeric',
            ];
        }
        $validator = Validator::make($request->all(), $data);
        if ($validator->fails())
        {
            $error = $validator->messages();
            $html='<ul>';
            foreach ($error->all() as $key => $value) {
                $html=$html.'<li>'.$value.'</li>';
            }
            $html=$html.'</ul>';
            return response()->json(['status'=> false, 'error'=> $html]);
        }
        $data=$request->all();
        if (isset($request->first_name)){
            $data['first_name']=$request->first_name;
        }else{
            $data['first_name']='';
        }
        $Clients=Clients::find($data['id']);
        audit_log($this->cms_array,audit_log_data($data),Config::get('constants.update'),$this->page_name,$this->tab_name,$data['id'],audit_log_data($Clients->toArray()),$data['id'],'',$this->db);
        $Clients->update($data);
        return response()->json(['message'=>'Successfully Added !!','flag'=>1]);

    }

    public function softDelete (Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.soft_delete'),$this->page_name,$this->tab_name,'','',$request->id,'',$this->db);
        $url = $request->header('referer');
        $check = explode("/", $url);
        $check = end($check);
        Clients::where('id',$request->id)->update(['active'=>0,'deleted'=>1]);

        if($check=="clients")
        {
            return response()->json(['message'=>'Successfully Delete','flag'=>0,'flag2'=>1]);
        }
        else
        {
            return response()->json(['message'=>'Successfully Delete','flag'=>0,'flag2'=>0]);

        }
    }
    public function delete(Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.delete'),$this->page_name,$this->tab_name,'','',$request->id,'',$this->db);
        $url = $request->header('referer');
        $check = explode("/", $url);
        $check = end($check);
        Clients::where('id',$request->id)->update(['active'=>0,'deleted'=>0]);
        if($check=="clients")
        {
            return response()->json(['message'=>'Successfully Delete','flag'=>1,'flag2'=>1]);
        }
        else
        {
            return response()->json(['message'=>'Successfully Delete','flag'=>1,'flag2'=>0]);

        }
    }
    public function active(Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.restore'),$this->page_name,$this->tab_name,'','',$request->id,'',$this->db);
        Clients::where('id',$request->id)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
    public function activeMultiple(Request $request)
    {
        Clients::whereIn('id',$request->ids)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
    public function deleteMultipleSOft(Request $request)
    {
        // $url = $request->header('referer');
        // $check = explode("/", $url);
        // $check = end($check);
        Clients::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>1]);

        return response()->json(['message'=>'Successfully Active','flag'=>0]);
        // if($check=="clients")
        // {
        //     return response()->json(['message'=>'Successfully Delete','flag'=>1,'flag2'=>1]);
        // }
        // else
        // {
        //     return response()->json(['message'=>'Successfully Delete','flag'=>1,'flag2'=>0]);
        // }

    }
    public function deleteMultiple(Request $request)
    {
        // if ($request->flag==1){
            Clients::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>0]);
        // }else{
        //     Clients::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>1]);
        // }
        return response()->json(['message'=>'Successfully Active','flag'=>1]);
    }
    public function exportPdf(Request $request){
        if($request->pdf_array == 1)
        {
            $data =Clients::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->pdf_array);
            $data= Clients::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        $companies='Patroltec';
        $reportname='Clients';
        $date=\Carbon\Carbon::now()->format('D jS M Y');
        $pdf = PDF::loadView('backend.employee.export.client_pdf',compact('companies','reportname','data','date'));
        return $pdf->download('clients.pdf');
    }

    public function exportWord(Request $request){
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();

        if($request->word_array == 1)
        {
            $data= Clients::where('company_id',Auth::user()->default_company)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->word_array);
            $data= Clients::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        $table = $section->addTable();
        $row = $table->addRow();
        if(Auth()->user()->hasPermissionTo('client_firstname_word_export') || Auth::user()->all_companies == 1 ){
            $row->addCell()->addText('First Name');
        }
        if(Auth()->user()->hasPermissionTo('client_surname_word_export') || Auth::user()->all_companies == 1 ){
            $row->addCell()->addText('Surname');
        }
        if(Auth()->user()->hasPermissionTo('client_phone_word_export') || Auth::user()->all_companies == 1 ){
            $row->addCell()->addText('Phone');
        }
        if(Auth()->user()->hasPermissionTo('client_email_word_export') || Auth::user()->all_companies == 1 ){
            $row->addCell()->addText('Email');
        }
        if(Auth()->user()->hasPermissionTo('client_address_word_export') || Auth::user()->all_companies == 1 ){
            $row->addCell()->addText('Address');
        }
        foreach ($data as  $value) {
            $row = $table->addRow();
            if(Auth()->user()->hasPermissionTo('client_firstname_word_export') || Auth::user()->all_companies == 1 ){
                $row->addCell()->addText($value->first_name);
            }
            if(Auth()->user()->hasPermissionTo('client_surname_word_export') || Auth::user()->all_companies == 1 ){
                $row->addCell()->addText($value->surname);
            }
            if(Auth()->user()->hasPermissionTo('client_phone_word_export') || Auth::user()->all_companies == 1 ){
                $row->addCell()->addText($value->phone_number);
            }
            if(Auth()->user()->hasPermissionTo('client_email_word_export') || Auth::user()->all_companies == 1 ){
                $row->addCell()->addText($value->email);
            }
            if(Auth()->user()->hasPermissionTo('client_address_word_export') || Auth::user()->all_companies == 1 ){
                $row->addCell()->addText($value->address);
            }
        }
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('clients.docx');
        return response()->download(public_path('clients.docx'));
    }

    public function exportExcel(Request $request)
    {
        if($request->excel_array == 1)
        {
            $data= Clients::where('company_id',Auth::user()->default_company)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
            return Excel::download(new ClientExport($data), 'clients.csv');
        }
        else
        {
            $array=json_decode($request->excel_array);
            $data= Clients::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
            return Excel::download(new ClientExport($data), 'clients.csv');
        }
    }

//    ........................End Client ....................

//    ...........Notes Controller......................
    public function notes()
    {
        $flag=1;
        $companies=Company::getCompanies();
        return view('backend.clients.notes')->with(['flag'=>$flag,'companies'=>$companies]);
    }
//........... Upload ..................
    public function upload()
    {
        $flag=1;
        $companies=Company::getCompanies();
        return view('backend.clients.upload')->with(['flag'=>$flag,'companies'=>$companies]);
    }
}

