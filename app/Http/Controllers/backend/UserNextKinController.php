<?php
namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\UserNextKin;
use App\RelationShip;
use App\Company;
use DataTables;
use Session;
use PDF;
use Config;
use Validator;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\usernextkin\UserNextKinExport;
use Auth;
class UserNextKinController extends Controller
{
    private $page_name;
    private $tab_name;
    private $db;
    private $cms_array=[];
    public function __construct() {
        $this->page_name ='next_of_kin';
        $this->tab_name ='employees';
        $this->db='users';
        $cms_data=localization();
        $temp_array=[];
        $temp_array['title']['key']='title';
        $temp_array['title']['value']=checkKey('title',$cms_data);
        $temp_array['first_name']['key']='first_name';
        $temp_array['first_name']['value']=checkKey('first_name',$cms_data);
        $temp_array['surname']['key']='surname';
        $temp_array['surname']['value']=checkKey('surname',$cms_data);
        $temp_array['email']['key']='email';
        $temp_array['email']['value']=checkKey('email',$cms_data);
        $temp_array['relation_ship_id']['key']='relationship';
        $temp_array['relation_ship_id']['value']=checkKey('relationship',$cms_data);
        $temp_array['phone_number']['key']='number';
        $temp_array['phone_number']['value']=checkKey('number',$cms_data);
        $temp_array['user_id']['key']='employees';
        $temp_array['user_id']['value']=checkKey('employees',$cms_data);
        $this->cms_array=$temp_array;
    }
    public function index()
    {
        $companies=Company::getCompanies();
        return view('backend.nextkin.index')->with(['companies'=>$companies]);
    }
    public function store()
    {
    	$data=UserNextKin::create(['first_name'=>'']);
    audit_log($this->cms_array,'',Config::get('constants.store'),$this->page_name,$this->tab_name,$data->id,'',$data->id,Session::get('employee_id'),$this->db);
        $relationships=RelationShip::where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        return response()->json(['data'=>$data,'relationships'=>$relationships]);
    }
    public function getAll()
    {
    	$data=UserNextKin::where('user_id',Session::get('employee_id'))->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        $relationships=RelationShip::where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        return Datatables::of($data)->with(['relationships'=>$relationships])
            ->addColumn('checkbox', function ($data) {
            })
            ->addColumn('submit', function ($data) {
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                if(Auth()->user()->hasPermissionTo('delete_nextofkin') || Auth::user()->all_companies == 1 ){
                    $delete = '<a type="button" class="btn deletenext_kindata'.$data->id.' btn-danger btn-xs my-0 waves-effect waves-light" onclick="deletenext_kindata( ' . $data->id . ')"><i class="fas fa-trash"></i></a>';
                }else{
                    $delete = '';
                }
                $response = $response . $delete;
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
    public function getAllSoft()
    {
    	$data=UserNextKin::where('user_id',Session::get('employee_id'))->where('active',0)->where('deleted',0)->orderBy('id', 'desc')->get();
        $relationships=RelationShip::where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        return Datatables::of($data)->with(['relationships'=>$relationships])
            ->addColumn('checkbox', function ($data) {
            })
            ->addColumn('submit', function ($data) {
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                $delete = '<a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light" onclick="deletesoftnext_kindata('.$data->id.')"><i class="fas fa-trash"></i></a><a type="button" class="btn btn-success btn-xs my-0 waves-effect waves-light" onclick="restorenext_kindata('.$data->id.')"><i class="fa fa-retweet" aria-hidden="true"></i></a>';
                $response = $response . $delete;
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
    public function update(Request $request)
    {
        if (!isset($request->first_name)){
            $data = [
                'company_id'=>'required|numeric',
                'email' => 'required|email',
            ];
        }elseif (!isset($request->email)){
            $data = [
                'company_id'=>'required|numeric',
                'first_name'=>'required',

            ];
        }else{
            $data = [
                'company_id'=>'required|numeric',
                'email' => 'required|email',
                'first_name'=>'required',
            ];
        }

        $validator = Validator::make($request->all(), $data);
        if ($validator->fails())
        {
            $error = $validator->messages();
            $html='<ul>';
            foreach ($error->all() as $key => $value) {
                $html=$html.'<li>'.$value.'</li>';
            }
            $html=$html.'</ul>';
            return response()->json(['status'=> false, 'error'=> $html]);
        }

        $input=$request->all();
        if (isset($request->first_name)){
            $input['first_name']=$request->first_name;
        }else{
            $input['first_name']='';
        }
        //static add user id
        $input['user_id']= Session::get('employee_id');
        $data=UserNextKin::where('id',$input['id']);
        $first=$data->first();
        // dd($input);
        audit_log($this->cms_array,audit_log_data($input),Config::get('constants.update'),$this->page_name,$this->tab_name,$first->id,audit_log_data($first->toArray()),$first->id,Session::get('employee_id'),$this->db);
        $data->update($input);
        if($first->active == 0)
        {
            $flag=0;
        }
        else
        {
            $flag=1;
        }
        return response()->json(['message'=>'Successfully Updated !!','flag'=>$flag]);
    }
    public function delete(Request $request){
    audit_log($this->cms_array,'',Config::get('constants.delete'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('employee_id'),$this->db);
        UserNextKin::where('id',$request->id)->update(['active'=>0]);
        return response()->json(['message'=>'delete','flag'=>1]);
   	}
   	public function activeMultiple(Request $request){
        UserNextKin::whereIn('id',$request->ids)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'active','flag'=>0]);
   	}
   	public function deleteMultiple(Request $request){
        UserNextKin::whereIn('id',$request->ids)->update(['active'=>0]);
        return response()->json(['message'=>'Deleted','flag'=>1]);
   }
   	public function deleteMultipleSoft(Request $request){
        UserNextKin::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Hard Deleted','flag'=>0]);
   }
    public function restore(Request $request){
    audit_log($this->cms_array,'',Config::get('constants.restore'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('employee_id'),$this->db);
        UserNextKin::where('id',$request->id)->update(['active'=>1]);
        return response()->json(['message'=>'restore','flag'=>0]);
    }
    public function softDelete(Request $request){
    audit_log($this->cms_array,'',Config::get('constants.soft_delete'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('employee_id'),$this->db);
        UserNextKin::where('id',$request->id)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Soft delete','flag'=>0]);
    }

    public function exportExcel(Request $request)
    {
	 	if($request->excel_array == 1)
        {
            $data= UserNextKin::where('user_id',Session::get('employee_id'))->with('relation_ship')->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->excel_array);
            $data=UserNextKin::where('user_id',Session::get('employee_id'))->with('relation_ship')->whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        return Excel::download(new UserNextKinExport($data), 'UserNextKin.csv');
    }

    public function exportPdf(Request $request)
    {
        if($request->pdf_array == 1)
        {
            $data= UserNextKin::where('user_id',Session::get('employee_id'))->with('relation_ship')->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->pdf_array);
            $data=UserNextKin::where('user_id',Session::get('employee_id'))->with('relation_ship')->whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        $companies='Patroltec';
        $reportname='Next of Kin';
        $date=\Carbon\Carbon::now()->format('D jS M Y');
        $pdf = PDF::loadView('backend.nextkin.pdf',compact('companies','reportname','data','date'));
        return $pdf->download('UserNextKin.pdf');
    }

    public function exportWorld(Request $request)
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        if($request->word_array == 1)
        {
            $data= UserNextKin::where('user_id',Session::get('employee_id'))->with('relation_ship')->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->word_array);
            $data=UserNextKin::where('user_id',Session::get('employee_id'))->with('relation_ship')->whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        $table = $section->addTable();
        foreach ($data as $key => $value) {
            if($key==0)
            {
                $table->addRow();
                if(Auth()->user()->hasPermissionTo('nextofkin_title_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Title');
                }
                if(Auth()->user()->hasPermissionTo('nextofkin_firstname_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('First Name');
                }
                if(Auth()->user()->hasPermissionTo('nextofkin_surname_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Surname');
                }
                if(Auth()->user()->hasPermissionTo('nextofkin_email_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Email');
                }
                if(Auth()->user()->hasPermissionTo('nextofkin_relationship_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Relationship');
                }
                if(Auth()->user()->hasPermissionTo('nextofkin_phone_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Phone Number');
                }

            }
            $table->addRow();
            if(Auth()->user()->hasPermissionTo('nextofkin_title_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText($value->title);
            }
            if(Auth()->user()->hasPermissionTo('nextofkin_firstname_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText($value->first_name);
            }
            if(Auth()->user()->hasPermissionTo('nextofkin_surname_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText($value->surname);
            }
            if(Auth()->user()->hasPermissionTo('nextofkin_email_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText($value->email);
            }
            if(Auth()->user()->hasPermissionTo('nextofkin_relationship_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText($value->relation_ship->name);
            }
            if(Auth()->user()->hasPermissionTo('nextofkin_phone_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText($value->phone_number);
            }

        }
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('UserNextKin.docx');
        return response()->download(public_path('UserNextKin.docx'));
    }


}
