<?php
namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\StaticFormUpload;
use DB;
use PDF;
use DataTables;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Hash;
use Config;
use Session;
use Validator;
use Response;
use App\Company;
use CloudConvert;
use App\Exports\static_form\StaticForm;
use Maatwebsite\Excel\Facades\Excel;
use Auth;
use NcJoes\OfficeConverter\OfficeConverter;

class StaticFormController extends Controller
{
    private $page_name;
    private $tab_name;
    private $db;
    private $cms_array=[];
    public function __construct() {
        $this->page_name ='static_form';
        $this->tab_name ='document_library';
        $this->db='static_form_uploads';
        $cms_data=localization();
        $temp_array=[];
        $temp_array['name']['key']='title';
        $temp_array['name']['value']=checkKey('title',$cms_data);
        $temp_array['file']['key']='file';
        $temp_array['file']['value']=checkKey('file',$cms_data);
        $temp_array['notes']['key']='notes';
        $temp_array['notes']['value']=checkKey('notes',$cms_data);
        $this->cms_array=$temp_array;
    }
    public function view($id)
    {
        $companies=Company::getCompanies();
        return view('backend.document.static_view_pdf')->with(['companies'=>$companies,'id'=>$id]);
    }
    public function activeStatic($id)
    {
        StaticFormUpload::where('form_id',Session::get('form_id'))->update(['is_complete'=>0]);
        StaticFormUpload::where('id',$id)->update(['is_complete'=>1]);
        return response()->json(['message'=>'Static Form Marked Complete!']);
    }
    public function viewPdf($id)
    {
        $file = StaticFormUpload::find($id);
        return response()->file($file->pdf);
    }

    public function storeUpload(Request $request){
        $input = $request->all();
        $test =  StaticFormUpload::where('active',1)->where('deleted',0)->update(['is_complete' => 0]);
        $upload =  new StaticFormUpload();
        if($request->file('file'))
        {
//            $time = time();
//            $file = $request->file('file');
//            $input['file'] = $time.'.'.$file->getClientOriginalExtension();
//            $destinationPath = public_path('/uploads/static');
//            $file->move($destinationPath, $input['file']);
//            CloudConvert::file($destinationPath.'/'.$input['file'])->to('pdf');
//            $pdf=explode(".",$input['file']);
//            $upload->file = $destinationPath.'/'.$input['file'];
//            $upload->pdf = $destinationPath.'/'.$pdf[0].'.pdf';

            $time = time();
            $file = $request->file('file');
            //dd($file);
            $name = $file->getClientOriginalName();
            $pdf=explode(".",$name);
            $attachments = $time.'.'.$file->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/static');
            $file->move($destinationPath, $attachments);
            $upload->file = $destinationPath.'/'.$attachments;
            $input['file']= $destinationPath.'/'.$time.'.pdf';
            $converter = new OfficeConverter($destinationPath.'/'.$attachments);
            $converter->convertTo($input['file']);
            $upload->pdf  = $input['file'];
        }
        // $path = $request->file('file')->store('uploads/static');
        $upload->company_id = $input['company_id'];
        $upload->name = $input['title'];
        $upload->form_id = Session::get('form_id');
        $upload->notes = $input['notes'];
        $upload->is_complete = 1;
        $upload->save();
        unset($input['_token']);
        $input['name']=$input['title'];
        unset($input['title']);
        // unset($input['file']);
        audit_log($this->cms_array,audit_log_data($input),Config::get('constants.store'),$this->page_name,$this->tab_name,$upload->id,'',$upload->id,Session::get('form_id'),$this->db);

        return response()->json(['message'=>'uploaded successfully']);
    }

    public function orginalDownload($id)
    {
        $file = StaticFormUpload::find($id);
        return response()->download($file->file);
    }

    public function pdfDownload($id)
    {
        $file = StaticFormUpload::find($id);
        $headers = array(
            'Content-Type: application/pdf',
        );
        return Response::download($file->pdf, $file->name.'.pdf', $headers);
    }

    public function update(Request $request)
    {
        $data=StaticFormUpload::where('id',$request->id)->first();
        return response()->json($data);
    }

    public function restore(Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.restore'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('form_id'),$this->db);
        $data=StaticFormUpload::where('id',$request->id)->update(['active'=>1]);
        return response()->json(['message'=>'Deleted successfullu']);
    }

    public function delete(Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.delete'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('form_id'),$this->db);
        $data=StaticFormUpload::where('id',$request->id)->update(['active'=>0]);
        return response()->json(['message'=>'Deleted successfullu']);
    }

    public function hardDelete(Request $request)
    {
        $data=StaticFormUpload::where('id',$request->id)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Deleted successfullu']);
    }
    public function getAllUpload()
    {
        $data =StaticFormUpload::where('form_id',Session::get('form_id'))->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        return Datatables::of($data)->addColumn('checkbox', function($data){})->addColumn('actions',function($data){
            $response="";
            if(Auth()->user()->hasPermissionTo('activate_documentStaticForm') || Auth::user()->all_companies == 1 ){
                $active ='<a id="' . $data->id . '"  onclick="uploadStaticActive('.$data->id.')" class="btn btn-success  btn-xs " datatoggle="tooltip" title="Complete Form"><i class="fas fa-toggle-on"></i>';
            }else{
                $active ='';
            }
            if(Auth()->user()->hasPermissionTo('view_detail_documentStaticForm') || Auth::user()->all_companies == 1 ){
                $view ='<a  href="/static_form/view/' . $data->id . '" class="btn btn-primary btn-xs "><i class="far fa-eye"></i></a>';
            }else{
                $view ='';
            }
            if(Auth()->user()->hasPermissionTo('edit_documentStaticForm') || Auth::user()->all_companies == 1 ){
                $edit ='<a  id="' . $data->id . '" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modalUploadEdit" onclick="uploadStaticEdit('.$data->id.')"><i class="fas fa-pencil-alt"></i></a>';
            }else{
                $edit ='';
            }
            if(Auth()->user()->hasPermissionTo('delete_documentStaticForm') || Auth::user()->all_companies == 1 ){
                $delete ='<a type="button"class="btn btn-danger btn-xs my-0" onclick="deleteUpload( '.$data->id . ')"><i class="fas fa-trash"></i></a>';
            }else{
                $delete ='';
            }
            $response=$response.$view;
            $response=$response.$edit;
            $response=$response.$delete;
            $response=$response.$active;
            return $response;
        })->rawColumns(['actions'])->make(true);
    }

    public function getAllSoftUpload()
    {
        $data =StaticFormUpload::where('form_id',Session::get('form_id'))->where('active',0)->where('deleted',0)->orderBy('id', 'desc')->get();
        return Datatables::of($data)->addColumn('checkbox', function($data){})->addColumn('actions',function($data){
            $response="";
//            $active ='<a id="' . $data->id . '"  onclick="uploadStaticActive('.$data->id.')" class="btn btn-success  btn-xs "><i class="fas fa-toggle-on"></i>';

            if(Auth()->user()->hasPermissionTo('view_detail_documentStaticForm') || Auth::user()->all_companies == 1 ){
                $view ='<a  href="/static_form/view/' . $data->id . '" class="btn btn-primary btn-xs "><i class="far fa-eye"></i></a>';
            }else{
                $view ='';
            }
            if(Auth()->user()->hasPermissionTo('restore_delete_documentStaticForm') || Auth::user()->all_companies == 1 ){
                $restore ='<a  id="' . $data->id . '" class="btn btn-info btn-xs" onclick="restoreUpload('.$data->id.')"><i class="fas fa-retweet"></i></a>';
            }else{
                $restore ='';
            }
            if(Auth()->user()->hasPermissionTo('hard_delete_documentStaticForm') || Auth::user()->all_companies == 1 ){
                $delete ='<a type="button"class="btn btn-danger btn-xs my-0" onclick="hardDeleteUpload( '.$data->id . ')"><i class="fas fa-trash"></i></a>';
            }else{
                $delete ='';
            }
            $response=$response.$view;
            $response=$response.$restore;
            $response=$response.$delete;
//            $response=$response.$active;
            return $response;
        })->rawColumns(['actions'])->make(true);
    }
    public function updateUpload(Request $request)
    {
        $input = $request->all();
        $uploaddata = StaticFormUpload::find($input['id']);
        $upload = StaticFormUpload::find($input['id']);
        if($request->file('file'))
        {
//            $file = $request->file('file');
//            $time = time();
//            $input['file'] = $time.'.'.$file->getClientOriginalExtension();
//            $destinationPath = public_path('/uploads/static');
//            $file->move($destinationPath, $input['file']);
//            CloudConvert::file($destinationPath.'/'.$input['file'])->to('pdf');
//            $pdf=explode(".",$input['file']);
//            $upload->file = $destinationPath.'/'.$input['file'];
//            $upload->pdf = $destinationPath.'/'.$pdf[0].'.pdf';


            $time = time();
            $file = $request->file('file');
            //dd($file);
            $name = $file->getClientOriginalName();
            $pdf=explode(".",$name);
            $attachments = $time.'.'.$file->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/static');
            $file->move($destinationPath, $attachments);
            $upload->file = $destinationPath.'/'.$attachments;
            $input['file']= $destinationPath.'/'.$time.'.pdf';
            $converter = new OfficeConverter($destinationPath.'/'.$attachments);
            $converter->convertTo($input['file']);
            $upload->pdf  = $input['file'];
        }
        $upload->form_id = Session::get('form_id');
        $upload->name = $input['name'];
        if($input['notes'])
        {
            $upload->notes = $input['notes'];
        }
        $upload->update();
        unset($input['_token']);
        unset($input['file']);
        audit_log($this->cms_array,audit_log_data($input),Config::get('constants.update'),$this->page_name,$this->tab_name,$uploaddata->id,audit_log_data($uploaddata->toArray()),$uploaddata->id,Session::get('form_id'),$this->db);

//      toastr()->success('Static Form Successfully updated!');
//      return redirect()->back();
        return response()->json(['message'=>'uploaded successfully']);
    }

    public function activeMultiple(Request $request)
    {
        StaticFormUpload::whereIn('id',$request->ids)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
    public function deleteMultiple(Request $request)
    {
        StaticFormUpload::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>$request->flag]);
    }

    public function exportExcel(Request $request){
        if($request->excel_array == 1)
        {
            $data =StaticFormUpload::where('form_id',Session::get('form_id'))->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
            $data=sortData($data);
        }
        else
        {
            $array=json_decode($request->excel_array);
            $data =StaticFormUpload::whereIn('id',$array)->where('form_id',Session::get('form_id'))->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
            $data=sortData($data);
        }
        return Excel::download(new StaticForm($data), 'static_form.csv');
    }
    public function exportPdf(Request $request){
        if($request->pdf_array == 1)
        {
            $data =StaticFormUpload::where('form_id',Session::get('form_id'))->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
            $data=sortData($data);
        }
        else
        {
            $array=json_decode($request->pdf_array);
            $data =StaticFormUpload::whereIn('id',$array)->where('form_id',Session::get('form_id'))->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
            $data=sortData($data);
        }
        $companies='Patroltec';
        $reportname='Static Form';
        $date=\Carbon\Carbon::now()->format('D jS M Y');
        $pdf = PDF::loadView('backend.document.static_form.pdf_export',compact('companies','reportname','data','date'));
        return $pdf->download('static_form.pdf');
    }
  
    public function exportWord(Request $request){
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        if($request->word_array == 1)
        {
            $data =StaticFormUpload::where('form_id',Session::get('form_id'))->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
            $data=sortData($data);
        }
        else
        {
            $array=json_decode($request->word_array);
            $data =StaticFormUpload::whereIn('id',$array)->where('form_id',Session::get('form_id'))->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
            $data=sortData($data);
        }
        $table = $section->addTable();
        foreach ($data as $key => $value) {
            if($key==0)
            {
                $row = $table->addRow();
                if(Auth()->user()->hasPermissionTo('documentStaticForm_title_word_export') || Auth::user()->all_companies == 1 ){
                    $row->addCell()->addText('Title');
                }else{
                    $row->addCell()->addText('');
                }
                if(Auth()->user()->hasPermissionTo('documentStaticForm_notes_word_export') || Auth::user()->all_companies == 1 ){
                    $row->addCell()->addText('Note');
                }else{
                    $row->addCell()->addText('');
                }
            }
            $table->addRow();
            if(Auth()->user()->hasPermissionTo('documentStaticForm_title_word_export') || Auth::user()->all_companies == 1 ){
                $row->addCell()->addText($value->name);
            }else{
                $row->addCell()->addText('');
            }
            if(Auth()->user()->hasPermissionTo('documentStaticForm_notes_word_export') || Auth::user()->all_companies == 1 ){
                $row->addCell()->addText(strip_tags($value->notes));
            }else{
                $row->addCell()->addText('');
            }


        }
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('static_form.docx');
        return response()->download(public_path('static_form.docx'));
    }
}
