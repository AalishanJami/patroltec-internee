<?php
namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CMSController extends Controller
{
    public function web()
    {
    	return view('backend.cms.web.index');
    }
     public function app()
    {
    	return view('backend.cms.app.index');
    }
   
}
