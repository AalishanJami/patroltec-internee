<?php
namespace App\Http\Controllers\backend;
use App\Forms;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\VersionLog;
use App\FormVersion;
use App\FormSections;
use App\Company;
use Auth;
use Config;
use Session;
use App\PrePopulate;
use App\PopulateResponse;
use DataTables;
use App\Exports\versionlog\VersionLogExport;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
class VersionController extends Controller
{
    private $page_name;
    private $tab_name;
    private $db;
    private $cms_array=[];
    public function __construct() {
        $this->page_name ='version_log';
        $this->tab_name ='document_library';
        $this->db='version_logs';
        $cms_data=localization();
        $temp_array=[];
        $temp_array['version_type']['key']='version_type';
        $temp_array['version_type']['value']=checkKey('version_type',$cms_data);
        $temp_array['form_name']['key']='form_name';
        $temp_array['form_name']['value']=checkKey('form_name',$cms_data);
        $temp_array['name']['key']='name';
        $temp_array['name']['value']=checkKey('name',$cms_data);
        $this->cms_array=$temp_array;
    }
    public function index()
    {
    	$version_log=VersionLog::with('company','user','form')->where('form_id',Session::get('form_id'))->where('active',1)->where('deleted',0)->orderBy('id', 'DESC')->get();
    	$companies=Company::getCompanies();
    	return view('backend.version.index')->with(['companies'=>$companies,'version_log'=>$version_log]);
    }
    public function view($id)
    {
        $form = Forms::find(Session::get('form_id'));
        if (!empty($form)){
            $form_name=$form->name;
        }else{
            $form_name='';
        }
        $form_version = FormVersion::where('version_id',$id)->get()->pluck('form_section_id');
        // ->where('active',1)->where('deleted',0)

        $form_version = array_values($form_version->toArray());
        $form_section = FormSections::whereIn('id',$form_version)->orderBy('order', 'asc')->with('questions.answer_type','questions.answer_group.answer')->get();
        //dd($form_section);
        $dynamic_data=FormVersion::where('version_id',$id)->with('form_Section.questions.answer_type','form_Section.questions.answer_group.answer')->orderBy('id', 'desc')->get();
       // dd($dynamic_data);
        if(!$dynamic_data->isEmpty())
        {
            $id_static=$dynamic_data[0]->version->static_form_uploads_id;
        }
        else
        {
            $id_static=null;
        }
    	$companies=Company::where('active',1)->where('deleted',0)->get();
        $pre_populate = PrePopulate::where('form_id',Session::get('form_id'))->orderBy('id', 'desc')->pluck('name','id')->toArray();
    	return view('backend.version.view_pdf')->with(['companies'=>$companies,'form_name'=>$form_name,'dynamic_data'=>$dynamic_data,'version_id'=>$id,'id_static'=>$id_static,'pre_populate'=>$pre_populate]);
    }

    public function getVersion(Request $request)
    {
        $form_version = FormVersion::where('version_id',$request->varsion_id)->get()->pluck('form_section_id');
        $form_version = array_values($form_version->toArray());
        $form_section = FormSections::whereIn('id',$form_version)->orderBy('order', 'asc')->with('questions.answer_type','questions.answer_group.answer')->orderBy('id','desc')->get();
        return response()->json(['form_section'=>$form_section]);
    }

    public function getAll()
    {
        $data =VersionLog::with('company','user','form')->where('form_id',Session::get('form_id'))->where('active',1)->where('deleted',0)->orderBy('id', 'DESC')->get();
        return Datatables::of($data)->addColumn('checkbox', function($data){})->addColumn('id', function($data){
            return $data->id;
        })->addColumn('form_name', function($data){
            if (!empty($data->form)){
                return $data->form->name;
            }else{
                return "";
            }
        })->addColumn('user_name', function($data){
            if (!empty($data->user)){
                return $data->user->name;
            }else{
                return "";
            }
        })->addColumn('actions',function($data){
            $response="";
            if(Auth()->user()->hasPermissionTo('viewdetail_documentVersionlogList') || Auth::user()->all_companies == 1 ){
                $view='<a href="/version/log/view/'.$data->id.'" class="btn btn-primary btn-xs"><i class="far fa-eye"></i></a>';
            }else{
                $view='';
            }
            if(Auth()->user()->hasPermissionTo('delete_documentVersionlogList') || Auth::user()->all_companies == 1 ){
                $delete='<a href="javascript:;" class="btn btn-danger btn-xs" onclick="deleteVersionLog('.$data->id.')"><i class="fa fa-trash"></i></a>';
            }else{
                $delete='';
            }
            $response=$response.$view.' '.$delete;
            return $response;
        })->rawColumns(['actions'])->make(true);
    }

    public function getAllSoft()
    {
        $data =VersionLog::with('company','user','form')->where('form_id',Session::get('form_id'))->where('active',0)->where('deleted',0)->orderBy('id', 'DESC')->get();
        return Datatables::of($data)->addColumn('checkbox', function($data){})->addColumn('id', function($data){
            return $data->id;
        })->addColumn('form_name', function($data){
            if (!empty($data->form)){
                return $data->form->name;
            }else{
                return "";
            }
        })->addColumn('user_name', function($data){
            if (!empty($data->user)){
                return $data->user->name;
            }else{
                return "";
            }
        })->addColumn('actions',function($data){
            $response="";
            if(Auth()->user()->hasPermissionTo('viewdetail_documentVersionlogList') || Auth::user()->all_companies == 1 ){
                $restore='<a href="javascript:;" class="btn btn-primary btn-xs all_action_btn_margin"     onclick="restoreVersionLog('.$data->id.')"><i class="fa fa-retweet"></i></a>';
            }else{
                $restore='';
            }
            if(Auth()->user()->hasPermissionTo('viewdetail_documentVersionlogList') || Auth::user()->all_companies == 1 ){
                $delete='<a href="javascript:;" class="btn btn-danger btn-xs" onclick="hardDeleteVersionLog('.$data->id.')"><i class="fa fa-trash"></i></a>';
            }else{
                $delete='';
            }
            $response=$response.$delete.$restore;
            return $response;
        })->rawColumns(['actions'])->make(true);
    }

    public function delete(Request $request){
    audit_log($this->cms_array,'',Config::get('constants.delete'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('form_id'),$this->db);
        VersionLog::where('id',$request->id)->update(['active'=>0]);
        return response()->json(['message'=>'deleted successfully']);
    }

    public function restore(Request $request){
    audit_log($this->cms_array,'',Config::get('constants.restore'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('form_id'),$this->db);
        VersionLog::where('id',$request->id)->update(['active'=>1]);
        return response()->json(['message'=>'restore']);
    }
    public function hardDelete(Request $request){

    audit_log($this->cms_array,'',Config::get('constants.soft_delete'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('form_id'),$this->db);
        VersionLog::where('id',$request->id)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'hard delete']);
    }
    public function multipleDelete(Request $request){
        if($request->flag==1) {
            VersionLog::whereIn('id',$request->ids)->update(['active'=>0]);
            return response()->json(['message'=>'Deleted','flag'=>$request->flag]);
        }else if ($request->flag==2) {
            VersionLog::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>1]);
            return response()->json(['message'=>'Deleted','flag'=>$request->flag]);
        }
    }

    public function activeMultiple(Request $request)
    {
        VersionLog::whereIn('id',$request->ids)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>$request->flag]);
    }
    public function viewPdf($id)
	{
		$file = VersionLog::find($id);
		return response()->file($file->path_pdf);
	}

    public function viewLatest($id)
    {
        Session::put('form_id',$id);
        $latest = VersionLog::where('form_id',$id)->get()->last()->id;
        return $this->view($latest);
    }
    public function getPopulate($id)
    {
        $response = PopulateResponse::where('pre_populate_id',$id)->where('active',1)->where('deleted',0)->get();
        return response()->json($response);
    }

      public function exportForm($form_id)
    {
        $form = Forms::find($form_id);
        if (!empty($form)){
            $form_name=$form->name;
        }else{
            $form_name='';
        }
        $form_section = FormSections::where('form_id',$form_id)->orderBy('order', 'asc')->with('questions.answer_type','questions.answer_group.answer')->get();
        
            $id_static=null;

          $dynamic_data;
          $id = 0;
        $date=Carbon::now()->format('D jS M Y');
        $dynamic_data= array();
        $pre_populate_id = 0;
        $companies=Company::where('active',1)->where('deleted',0)->get();
        $pre_populate = PrePopulate::where('form_id',$form_id)->orderBy('id', 'desc')->pluck('name','id')->toArray();
     
       return view('backend.form_export.pdf',compact('companies','form_name','date','dynamic_data', 'id_static', 'pre_populate', 'pre_populate_id', 'form_id' ));
        // return $pdf->download('static_form.pdf');
    }

    public function getOnePopulate($id,$question_id)
    {
        $response = PopulateResponse::where('pre_populate_id',$id)->where('question_id',$question_id)->where('active',1)->where('deleted',0)->first();
        return response()->json($response);
    }
    public function exportExcel(Request $request){
        if($request->excel_array == 1)
        {
            $data =VersionLog::with('company','user','form')->where('form_id',Session::get('form_id'))->where('active',1)->where('deleted',0)->orderBy('id', 'DESC')->get();
//            $data=sortData($data);
        }
        else
        {
            $array=json_decode($request->excel_array);
            $data =VersionLog::whereIn('id',$array)->with('company','user','form')->where('form_id',Session::get('form_id'))->where('active',1)->where('deleted',0)->orderBy('id', 'DESC')->get();
//            $data=sortData($data);
        }
        return Excel::download(new VersionLogExport($data), 'versionlog.csv');
    }

    public function exportVersionPdf_view($id,$pre_populate_id=null)
    {
        $form = Forms::find(Session::get('form_id'));
        if (!empty($form)){
            $form_name=$form->name;
        }else{
            $form_name='';
        }
        $form_version = FormVersion::where('version_id',$id)->where('active',1)->where('deleted',0)->get()->pluck('form_section_id');

        $form_version = array_values($form_version->toArray());
        // dd($form_version);
        $form_section = FormSections::whereIn('id',$form_version)->orderBy('order', 'asc')->with('questions.answer_type','questions.answer_group.answer')->get();
        //dd($form_section);
        $dynamic_data=FormVersion::where('version_id',$id)->with('form_Section.questions.answer_type','form_Section.questions.answer_group.answer')->orderBy('id', 'desc')->get();
        if(!$dynamic_data->isEmpty())
        {
            $id_static=$dynamic_data[0]->version->static_form_uploads_id;
        }
        else
        {
            $id_static=null;
        }
        $date=Carbon::now()->format('D jS M Y');
        $companies=Company::where('active',1)->where('deleted',0)->get();
        $pre_populate = PrePopulate::where('form_id',Session::get('form_id'))->orderBy('id', 'desc')->pluck('name','id')->toArray();
        return view('backend.version.pdf_layout_print')->with(['companies'=>$companies,'version_id'=>$id,'form_name'=>$form_name,'date'=>$date,'form_name'=>$form_name,'dynamic_data'=>$dynamic_data,'version_id'=>$id,'id_static'=>$id_static,'pre_populate'=>$pre_populate,'pre_populate_id'=>$pre_populate_id]);
    }


//    public function exportVersionPdf_view(){
//        return view('backend.version.pdf_layout_testing');
//    }


    public function exportVersionPdf(Request $request){
        if (isset($request->varsion_id)){
            $form = Forms::find(Session::get('form_id'));
            $formname='';
            if (!empty($form)){
                $formname=$form->name;
            }
            $pre_populate_id=$request->pre_populate_id;
            $form_version = FormVersion::where('version_id',$request->varsion_id)->where('active',1)->where('deleted',0)->get()->pluck('form_section_id');
            $form_version = array_values($form_version->toArray());
            $form_section = FormSections::whereIn('id',$form_version)->orderBy('order', 'asc')->with('questions.answer_type','questions.populate','questions.answer_group.answer')->orderBy('id','desc')->get();
            //dd($form_section);

            $date=Carbon::now()->format('D jS M Y');
            $pdf = PDF::loadView('backend.version.pdf_layout_testing',compact('formname','pre_populate_id','form_section','companies','reportname','date'));
            return $pdf->download('versionlog.pdf');
        }else{
            return redirect()->back();
        }
    }

    public function exportPdf(Request $request){
        if($request->pdf_array == 1)
        {
            $data =VersionLog::with('company','user','form')->where('form_id',Session::get('form_id'))->where('active',1)->where('deleted',0)->orderBy('id', 'DESC')->get();
            //$data=sortData($data);
        }
        else
        {
            $array=json_decode($request->pdf_array);
            $data =VersionLog::whereIn('id',$array)->with('company','user','form')->where('form_id',Session::get('form_id'))->where('active',1)->where('deleted',0)->orderBy('id', 'DESC')->get();
            //$data=sortData($data);
        }
        $companies='Patroltec';
        $reportname='Version Log';
        $date=\Carbon\Carbon::now()->format('D jS M Y');
        $pdf = PDF::loadView('backend.version.export_pdf',compact('companies','reportname','data','date'));
        return $pdf->download('versionlog.pdf');
    }

    public function exportWord(Request $request){
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        if($request->word_array == 1)
        {
            $data =VersionLog::with('company','user','form')->where('form_id',Session::get('form_id'))->where('active',1)->where('deleted',0)->orderBy('id', 'DESC')->get();
        }
        else
        {
            $array=json_decode($request->word_array);
            $data =VersionLog::whereIn('id',$array)->with('company','user','form')->where('form_id',Session::get('form_id'))->where('active',1)->where('deleted',0)->orderBy('id', 'DESC')->get();
        }
        $section->addText('Version Log listing');
        $table = $section->addTable();
        foreach ($data as $key => $value) {
            if($key==0)
            {
                $table->addRow();
                if(Auth()->user()->hasPermissionTo('documentVersionlogList_formname_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Form Name');
                }else{
                    $table->addCell(1750)->addText('');
                }
                if(Auth()->user()->hasPermissionTo('documentVersionlogList_username_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Name');
                }else{
                    $table->addCell(1750)->addText('');
                }
                if(Auth()->user()->hasPermissionTo('documentVersionlogList_date_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Date Added');
                }else{
                    $table->addCell(1750)->addText('');
                }
            }
            $table->addRow();
            if(Auth()->user()->hasPermissionTo('documentVersionlogList_formname_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText($value->form->name);
            }else{
                $table->addCell(1750)->addText('');
            }
            if(Auth()->user()->hasPermissionTo('documentVersionlogList_username_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText($value->user->name);
            }else{
                $table->addCell(1750)->addText('');
            }
            if(Auth()->user()->hasPermissionTo('documentVersionlogList_date_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText($value->date_time);
            }else{
                $table->addCell(1750)->addText('');
            }
        }
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('versionlog.docx');
        return response()->download(public_path('versionlog.docx'));
    }
}
