<?php
namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\LocationBreakdown;
use App\Company;
use PDF;
use Session;
use Validator;
use App\Exports\qr\QRExport;
use App\Exports\qr\QRSoftExport;
use Maatwebsite\Excel\Facades\Excel;
use DataTables;
use Auth;
use Config;
class QRController extends Controller{
    private $page_name;
    private $tab_name;
    private $db;
    private $cms_array=[];
    public function __construct() {
        $this->page_name ='qr';
        $this->tab_name ='project';
        $this->db='location_breakdowns';
        $cms_data=localization();
        $temp_array=[];
        $temp_array['name']['key']='name';
        $temp_array['name']['value']=checkKey('name',$cms_data);
        $temp_array['directions']['key']='directions';
        $temp_array['directions']['value']=checkKey('directions',$cms_data);
        $temp_array['information']['key']='information';
        $temp_array['information']['value']=checkKey('information',$cms_data);
        $temp_array['type']['key']='type';
        $temp_array['type']['value']=checkKey('type',$cms_data);
        $temp_array['location_id']['key']='project';
        $temp_array['location_id']['value']=checkKey('project',$cms_data);
        $this->cms_array=$temp_array;
    }
    public function index()
    {
        $companies=Company::getCompanies();
        return view('backend.qr.index')->with(['companies'=>$companies]);
    }
    public function getAll(){
        $data=LocationBreakdown::where('type','t')->where('location_id',Session::get('project_id'))->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        return Datatables::of($data)->with(['flag'=>1])
            ->addColumn('checkbox', function ($data) {})->addColumn('qr', function ($data) {})
            ->addColumn('submit', function ($data) {})
            ->addColumn('flag', function ($data) {return '1';})
            ->addColumn('actions', function ($data) {
                $response = "";
                if(Auth()->user()->hasPermissionTo('delete_projectqr') || Auth::user()->all_companies == 1 ){
                    $delete = '<a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light delete-btn' . $data->id . '" onclick="deleteqrdata( ' . $data->id . ')"><i class="fas fa-trash"></i></a>';
                }else{
                    $delete = '';
                }
                $response = $response . $delete;
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);
//        return response()->json(['data'=>$data,'flag'=>1]);
    }
    public function getAllSoft()
    {
        $data=LocationBreakdown::where('type','t')->where('active',0)->where('deleted',0)->orderBy('id', 'desc')->get();
        return Datatables::of($data)->with(['flag'=>0])
            ->addColumn('checkbox', function ($data) {})->addColumn('qr', function ($data) {})
            ->addColumn('submit', function ($data) {})
            ->addColumn('flag', function ($data) {return '0';})
            ->addColumn('actions', function ($data) {
                $response = "";
                if(Auth()->user()->hasPermissionTo('hard_delete_projectqr') || Auth::user()->all_companies == 1 ){
                    $delete = '<a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light" onclick="deletesoftqrdata( ' . $data->id . ')"><i class="fas fa-trash"></i></a>';
                }else{
                    $delete = '';
                }
                if(Auth()->user()->hasPermissionTo('restore_delete_projectqr') || Auth::user()->all_companies == 1 ){
                    $restore = '<a type="button" class="btn btn-success all_action_btn_margin btn-xs my-0 waves-effect waves-light" onclick="restoreqrdata( ' . $data->id . ')"><i class="fa fa-retweet"></i></a>';
                }else{
                    $restore = '';
                }
                $response = $response . $delete;
                $response = $response . $restore;
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);
//        return response()->json(['data'=>$data,'flag'=>0]);
    }

    public function store()
    {
        $data=LocationBreakdown::create(['name'=>'','type'=>'t']);
    audit_log($this->cms_array,'',Config::get('constants.store'),$this->page_name,$this->tab_name,$data->id,'',$data->id,Session::get('project_id'),$this->db);
        return response()->json(['data'=>$data]);
    }
    public function update(Request $request)
    {
        if (isset($request->name)){
            $data = [
                'name'=>'required',
                'company_id'=>'required|numeric',
            ];
        }else{
            $data = [
                'company_id'=>'required|numeric',
            ];
        }

        $location_id=Session::get('project_id');
        $validator = Validator::make($request->all(), $data);
        if ($validator->fails())
        {
            $error = $validator->messages();
            $html='<ul>';
            foreach ($error->all() as $key => $value) {
                $html=$html.'<li>'.$value.'</li>';
            }
            $html=$html.'</ul>';
            return response()->json(['status'=> false, 'error'=> $html]);
        }
        $data=$request->all();
        $data['location_id']=Session::get('project_id');
        $data['type']='t';

        if(isset($data['name']))
        {
            $name='qr/'.$data['name'].'.png';
            $newPath = public_path($name);
            $oldPath = public_path('qr/qr.png');  // publc/images/2.jpg
            if (\File::copy($oldPath , $newPath)) {
                \QrCode::size(80)->format('png')->generate($data['name'], public_path($name));
            }
            $data['name']=$request->name;
        }else{
            $data['name']='';
        }
        $location=LocationBreakdown::find($data['id']);
        audit_log($this->cms_array,audit_log_data($data),Config::get('constants.update'),$this->page_name,$this->tab_name,$location->id,audit_log_data($location->toArray()),$location->id,Session::get('project_id'),$this->db);
        $location->update($data);
        $qr_image='';
        if($data['name'])
        {
            $qr_image='<img src="'.url('qr').'\\'.$data['name'].'.png"  width="50" height="50">';
        }
        return response()->json(['message'=>'Successfully Added !!','qr'=>$qr_image]);

    }
    public function active(Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.restore'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('project_id'),$this->db);
        LocationBreakdown::where('id',$request->id)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
    public function deleteMultipleSOft(Request $request)
    {
        LocationBreakdown::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function deleteMultiple(Request $request)
    {
        LocationBreakdown::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function delete(Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.delete'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('project_id'),$this->db);
        LocationBreakdown::where('id',$request->id)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function softDelete(Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.soft_delete'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('project_id'),$this->db);
        LocationBreakdown::where('id',$request->id)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function activeMultiple(Request $request)
    {
        LocationBreakdown::whereIn('id',$request->ids)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active']);
    }
    public function exportPdf(Request $request)
    {
        if($request->pdf_array == 1)
        {
            $data= LocationBreakdown::where('company_id',Auth::user()->default_company)->where('location_id',Session::get('project_id'))->where('type','t')->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->pdf_array);
            $data= LocationBreakdown::where('company_id',Auth::user()->default_company)->where('location_id',Session::get('project_id'))->whereIn('id',$array)->where('type','t')->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        $companies='Patroltec';
        $reportname='Qr';
        $date=\Carbon\Carbon::now()->format('D jS M Y');
        $pdf = PDF::loadView('backend.qr.pdf',compact('companies','reportname','data','date'));
        return $pdf->download('qr.pdf');
    }

    public function exportWorld(Request $request)
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
//        if($request->export_world_qr == 1)
//        {
            if($request->word_array == 1)
            {
                $data= LocationBreakdown::where('type','t')->where('location_id',Session::get('project_id'))->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
            }
            else
            {
                $array=json_decode($request->word_array);
                $data= LocationBreakdown::whereIn('id',$array)->where('type','t')->where('location_id',Session::get('project_id'))->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
            }
//        }
//        else
//        {
//            $data= LocationBreakdown::where('company_id',Auth::user()->default_company)->where('type','t')->where('active', '=',0)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
//        }
        $table = $section->addTable();
        foreach ($data as $key => $value) {
            if($key==0)
            {
                $table->addRow();
                if(Auth()->user()->hasPermissionTo('projectqr_qr_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('QR');
                }
                if(Auth()->user()->hasPermissionTo('projectqr_name_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Name');
                }
                if(Auth()->user()->hasPermissionTo('projectqr_directions_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Direction');
                }
                if(Auth()->user()->hasPermissionTo('projectqr_information_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Information');
                }
            }
            $table->addRow();
            if(Auth()->user()->hasPermissionTo('projectqr_qr_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addImage(public_path('qr/'.$value->name.'.png'));
            }
            if(Auth()->user()->hasPermissionTo('projectqr_name_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText($value->name);
            }
            if(Auth()->user()->hasPermissionTo('projectqr_directions_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText(strip_tags($value->directions));
            }
            if(Auth()->user()->hasPermissionTo('projectqr_information_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText(strip_tags($value->information));
            }


        }
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('qr.docx');
        return response()->download(public_path('qr.docx'));
    }
    public function exportExcel(Request $request)
    {
//        if($request->export_excel_qr == 1)
//        {
            if($request->excel_array == 1)
            {
                $data= LocationBreakdown::where('type','t')->where('location_id',Session::get('project_id'))->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
                return Excel::download(new QRExport($data), 'qr.csv');
            }
            else
            {
                $array=json_decode($request->excel_array);
                $data= LocationBreakdown::whereIn('id',$array)->where('type','t')->where('location_id',Session::get('project_id'))->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
                return Excel::download(new QRExport($data), 'qr.csv');
            }
//        }
//        else
//        {
//            return Excel::download(new QRSoftExport, 'qr.csv');
//            //for soft delete
//
//        }
    }


}
