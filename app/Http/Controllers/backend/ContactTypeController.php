<?php
namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ContactsType;
use Validator;
use DataTables;
use Auth;
use PDF;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\contact_type\ContactTypeExport;
use Config;
use Session;
class ContactTypeController extends Controller
{
    private $page_name;
    private $tab_name;
    private $db;
    private $cms_array=[];
    public function __construct() {
        $this->page_name ='contact_type';
        $this->tab_name ='project';
        $this->db='contact_types';
        $cms_data=localization();
        $temp_array=[];
        $temp_array['name']['key']='name';
        $temp_array['name']['value']=checkKey('name',$cms_data);
        $this->cms_array=$temp_array;
    }
    public function getAll()
    {
        $data=ContactsType::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {
            })
            ->addColumn('submit', function ($data) {
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                if(Auth()->user()->hasPermissionTo('delete_addressType') || Auth::user()->all_companies == 1 )
                {
                    $delete = '<a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light delete-btn'.$data->id.'" onclick="deletecontacttypedata( ' . $data->id . ')"><i class="fas fa-trash"></i></a>';
                    $response = $response . $delete;
                }
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);
       
    }
    public function getAllSoft()
    {
        $data=ContactsType::where('company_id',Auth::user()->default_company)->where('active',0)->where('deleted',0)->orderBy('id', 'desc')->get();
        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {
            })
            ->addColumn('submit', function ($data) {
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                if(Auth()->user()->hasPermissionTo('hard_delete_addressType') || Auth::user()->all_companies == 1 )
                {
                    $delete = '<a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light" onclick="deletesoftcontacttypedata('.$data->id.')"><i class="fas fa-trash"></i></a>';
                    $response = $response . $delete;
                }
                if(Auth()->user()->hasPermissionTo('restore_delete_addressType') || Auth::user()->all_companies == 1 )
                {
                    $restore = '<a type="button" class="btn btn-success btn-xs my-0 waves-effect waves-light" onclick="restorecontacttypedata('.$data->id.')"><i class="fa fa-retweet" aria-hidden="true"></i></a>';
                    $response = $response . $restore;
                }
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
    public function store()
    {
        $data=ContactsType::create(['name'=>'']);
    audit_log($this->cms_array,'',Config::get('constants.store'),$this->page_name,$this->tab_name,$data->id,'',$data->id,Session::get('project_id'),$this->db);
        return response()->json(['data'=>$data]);
    }
    public function update(Request $request)
    {
        // dd($request->all());
        $data = [
                'company_id'=>'required|numeric',
                ];
        $validator = Validator::make($request->all(), $data);
        if ($validator->fails())
        {
            $error = $validator->messages();  
            $html='<ul>';
            foreach ($error->all() as $key => $value) {
                $html=$html.'<li>'.$value.'</li>';
            }
            $html=$html.'</ul>';
            return response()->json(['status'=> false, 'error'=> $html]);
        }
        $data=$request->all();
        $ContactsType=ContactsType::where('id',$data['id']);
        $ContactsType_first=$ContactsType->first();
        audit_log($this->cms_array,audit_log_data($data),Config::get('constants.update'),$this->page_name,$this->tab_name,$ContactsType_first->id,audit_log_data($ContactsType_first->toArray()),$ContactsType_first->id,Session::get('project_id'),$this->db);
        $ContactsType->update($data);
        if($ContactsType_first->active == 0)
        {
            $flag=0;
        }
        else
        {
            $flag=1;
        }
       
        return response()->json(['message'=>'Successfully Added !!','flag'=>$flag]);
        
    }
    public function softDelete (Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.soft_delete'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('project_id'),$this->db);
        ContactsType::where('id',$request->id)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function delete(Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.delete'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('project_id'),$this->db);
        ContactsType::where('id',$request->id)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function active(Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.restore'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('project_id'),$this->db);
        ContactsType::where('id',$request->id)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
    public function activeMultiple(Request $request)
    {
        ContactsType::whereIn('id',$request->ids)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
     public function deleteMultipleSOft(Request $request)
    {
        ContactsType::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function deleteMultiple(Request $request)
    {
        ContactsType::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
   public function exportExcel(Request $request)
    {
        if($request->excel_array == 1)
        {
            $data= ContactsType::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->excel_array);
            $data=ContactsType::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        return Excel::download(new ContactTypeExport($data), 'contact_type.csv');
    }
    public function exportPdf(Request $request)
    {
        if($request->pdf_array == 1)
        {
            $data= ContactsType::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->pdf_array);
            $data=ContactsType::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        $companies='Patroltec';
        $reportname='Contact Type';
        $date=\Carbon\Carbon::now()->format('D jS M Y');
        $pdf = PDF::loadView('backend.contact_type.pdf',compact('companies','reportname','data','date'));
        return $pdf->download('contact_type.pdf');
    }
    public function exportWorld(Request $request)
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        if($request->word_array == 1)
        {
            $data= ContactsType::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->word_array);
            $data=ContactsType::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        $table = $section->addTable();
        foreach ($data as $key => $value) {
            if($key==0)
            {
                $table->addRow();
                if(Auth()->user()->hasPermissionTo('contactType_name_word_export') || Auth::user()->all_companies == 1 )
                {
                    $table->addCell(1750)->addText('Name');
                }
            }
            $table->addRow();
            if(Auth()->user()->hasPermissionTo('contactType_name_word_export') || Auth::user()->all_companies == 1 )
                {
                    $table->addCell(1750)->addText($value->name);
                }
           
        }
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('contact_type.docx');
        return response()->download(public_path('contact_type.docx'));
    }
    
}