<?php
namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Company;
use Session;
use App\Documents;
use App\User;
use PDF;
use App\UserDocType;
use Validator;
use Auth;
use Config;
use App\Exports\employee_document\EmployeeDocumentExport;
use App\DocType;
use Carbon\Carbon;
use CloudConvert;
use Maatwebsite\Excel\Facades\Excel;
use DataTables;
use NcJoes\OfficeConverter\OfficeConverter;
class UploadDocumentController extends Controller
{

    private $page_name;
    private $tab_name;
    private $db;
    private $cms_array=[];
    public function __construct() {
        $this->page_name ='documents';
        $this->tab_name ='employees';
        $this->db='documents';
        $cms_data=localization();
        $temp_array=[];
        $temp_array['name']['key']='name';
        $temp_array['name']['value']=checkKey('name',$cms_data);
        $temp_array['doc_type_id']['key']='doc_type';
        $temp_array['doc_type_id']['value']=checkKey('doc_type',$cms_data);
        $temp_array['issue']['key']='issue_date';
        $temp_array['issue']['value']=checkKey('issue_date',$cms_data);
        $temp_array['expiry']['key']='expiry_date';
        $temp_array['expiry']['value']=checkKey('expiry_date',$cms_data);
        $temp_array['file']['key']='file';
        $temp_array['file']['value']=checkKey('file',$cms_data);
        $temp_array['notes']['key']='notes';
        $temp_array['notes']['value']=checkKey('notes',$cms_data);
        $this->cms_array=$temp_array;
    }
    public function index()
    {
        $companies=Company::getCompanies();
        $link_id=Session::get('employee_id');
        $user=User::where('id',$link_id)->with('positionData')->first();
        $user_doc_type=UserDocType::getUserTypeRequired($user->position_id);
        $doc_types =DocType::with('user_doc_type.position')->where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        $match='(required)';
        return view('backend.employee_document.index')->with(['companies'=>$companies,'position_id'=>$user->position_id,'doc_types'=>$doc_types,'link_id'=>$link_id,'match'=>$match,'user'=>$user,'user_doc_type'=>$user_doc_type]);
    }
    public function getAllSoft(Request $request){
        $data =Documents::where('link_id',Session::get('employee_id'))->with('doc_type')->orderBy('id', 'desc')->where('active',0)->where('deleted',0)->get();
        $count_document=count_document($data);
        return Datatables::of($data)->with(['count_document'=>$count_document])
        ->addColumn('checkbox', function($data){})
        ->addColumn('doc_type', function($data){
            if (!empty($data->doc_type)){
                return $data->doc_type->name;
            }
        })
        ->addColumn('file', function($data){
            $file = str_replace('/home/eurotech/dev.patroltec.net/public/uploads/static/','',$data->file);
            return $file;
        })
        ->addColumn('actions', function($data){
                $response="";
                if(Auth()->user()->hasPermissionTo('hard_delete_employee_document') || Auth::user()->all_companies == 1)
                {
                    $delete = '<a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light" onclick="deletesoftemployee_documentdata('.$data->id.')"><i class="fas fa-trash"></i></a>';
                    $response=$response.$delete;
                }
                if(Auth()->user()->hasPermissionTo('restore_delete_employee_document') || Auth::user()->all_companies == 1)
                {
                    $restore = '<a type="button" class="btn btn-success btn-xs my-0 waves-effect waves-light" onclick="restoreemployee_documentdata('.$data->id.')"><i class="fa fa-retweet" aria-hidden="true"></i></a>';
                    $response=$response.$restore;
                }
                return $response;
            })->rawColumns(['actions'])->make(true);
    }
    public function view($id)
    {
        $companies =Company::where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        return view('backend.employee_document.view')->with(['companies'=>$companies,'id'=>$id]);
    }

    public function viewPdf($id)
    {
      $file = Documents::find($id);
      return response()->file($file->pdf);
    }

    public function download($id)
    {
        $file = Documents::find($id);
//        $destinationPath = public_path('employee_documents/'.$file->file);
        return response()->download($file->file);
    }
    public function softDelete(Request $request){
    audit_log($this->cms_array,'',Config::get('constants.soft_delete'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('employee_id'),$this->db);
        Documents::where('id',$request->id)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Soft delete','flag'=>0]);
    }
    public function getAll(Request $request){
        $data =Documents::where('link_id',Session::get('employee_id'))->orderBy('id', 'desc')->where('active',1)->where('deleted',0)->get();
        $count_document=count_document($data);
        return Datatables::of($data)->with(['count_document'=>$count_document])
        ->addColumn('checkbox', function($data){})
        ->addColumn('doc_type', function($data){
            if (!empty($data->doc_type)){
                return $data->doc_type->name;
            }
        })
        ->addColumn('file', function($data){
            $file = str_replace('/home/eurotech/dev.patroltec.net/public/uploads/static/','',$data->file);
            return $file;
        })
        ->addColumn('actions', function($data){
                $response="";
                if(Auth()->user()->hasPermissionTo('download_employee_document') || Auth::user()->all_companies == 1)
                {
                    $download='<a href=/user/document/download/'.$data->id.' class="btn btn-warning btn-xs"><i class="fa fa-download"></i></a>';
                    $response=$response.$download;
                }
                if(Auth()->user()->hasPermissionTo('view_document_employee_document') || Auth::user()->all_companies == 1)
                {
                    $view='<a href=/user/document/view/'.$data->id.' class="btn btn-success btn-xs"><i class="fas fa-eye"></i></a>';
                    $response=$response.$view;
                }
                if(Auth()->user()->hasPermissionTo('edit_employee_document') || Auth::user()->all_companies == 1)
                {
                    $edit =' <a class="btn btn-primary btn-xs" onclick="getemployee_documentdata('.$data->id.')" ><i class="fas fa-pencil-alt fa-xs"></i></a>';
                    $response=$response.$edit;
                }
                if(Auth()->user()->hasPermissionTo('delete_employee_document') || Auth::user()->all_companies == 1)
                {
                    $delete ='<a class="btn btn-danger btn-xs" onclick="deleteemployee_documentdata('.$data->id.')"><i class="fas fa-trash fa-xs"></i></a>';
                    $response=$response.$delete;
                }
                return $response;
            })->rawColumns(['actions'])->make(true);
    }
    public function edit(Request $request)
    {
        $document=Documents::where('id',$request->id)->first();
        $link_id=Session::get('employee_id');
        $user=User::where('id',$link_id)->with('positionData')->first();
        $user_doc_type=UserDocType::getUserTypeRequired($user->position_id);
        $doc_types =DocType::with('user_doc_type.position')->where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        $match='(required)';
        return response()->json(['document'=>$document,'doc_types'=>$doc_types,'position_id'=>$user->position_id,'match'=>$match,'user_doc_type'=>$user_doc_type]);
    }
    public function update(Request $request)
    {
        $data=[
            'company_id'=>'required|numeric',
            'doc_type_id'=>'required',
//            'file'=>'required',
            'name'=>'required',
        ];
        $validator = Validator::make($request->all(), $data);
        if ($validator->fails())
        {
            $errors = $validator->errors();

            if ($errors->first('doc_type_id')){
                $error['doc_type_id']='Field required';
            }
            if ($errors->first('file')){
                $error['file']='Field required';
            }
            if ($errors->first('name')){
                $error['name']='Field required';
            }
            return response()->json(['status'=> 'validation', 'error'=> $error]);
        }

        $input=$request->all();
        if(isset($request->file))
        {
//            $time = time();
//            $file = $request->file('file');
//            $input['file'] = $time.'.'.$file->getClientOriginalExtension();
//            $destinationPath = public_path('employee_documents');
//            $file->move($destinationPath, $input['file']);
//            CloudConvert::file($destinationPath.'/'.$input['file'])->to('pdf');
//            $pdf=explode(".",$input['file']);
//            $input['pdf'] = $destinationPath.'/'.$pdf[0].'.pdf';

            $time = time();
            $file = $request->file('file');
            //dd($file);
            $name = $file->getClientOriginalName();
            $pdf=explode(".",$name);
            $attachments = $time.'.'.$file->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/static');
            $file->move($destinationPath, $attachments);
            $input['file']= $destinationPath.'/'.$time.'.pdf';
            $converter = new OfficeConverter($destinationPath.'/'.$attachments);
            $converter->convertTo($input['file']);
            $input['pdf']  = $input['file'];
        }
        $input['expiry'] = Carbon::parse($input['expiry']);
        $input['issue'] = Carbon::parse($input['issue']);
        unset($input['_token']);
        $data=Documents::where('id',$input['id']);
        $first=$data->first();
        $audit_input=$input;
        unset($audit_input['link_table']);
        unset($audit_input['link_id']);
        unset($audit_input['pdf']);
        $audit_input['issue']=Carbon::parse($audit_input['issue'])->format('jS M Y');
        $audit_input['expiry']=Carbon::parse($audit_input['expiry'])->format('jS M Y');
        audit_log($this->cms_array,audit_log_data($audit_input),Config::get('constants.update'),$this->page_name,$this->tab_name,$input['id'],audit_log_data($first->toArray()),$input['id'],Session::get('employee_id'),$this->db);
        $data->update($input);
        return response()->json(['message'=>'Successfully Update !!']);
    }
    public function store(Request $request)
    {
        // dd($request->all());
        $data=[
            'company_id'=>'required|numeric',
            'doc_type_id'=>'required',
            'file'=>'required',
            'name'=>'required',
        ];
        $validator = Validator::make($request->all(), $data);
        if ($validator->fails())
        {
            $errors = $validator->errors();

            if ($errors->first('doc_type_id')){
                $error['doc_type_id']='Field required';
            }
            if ($errors->first('file')){
                $error['file']='Field required';
            }
            if ($errors->first('name')){
                $error['name']='Field required';
            }
            return response()->json(['status'=> 'validation', 'error'=> $error]);
        }


        $input=$request->all();
//        $time = time();
//        $file = $request->file('file');
//        $input['file'] = $time.'.'.$file->getClientOriginalExtension();
//        $destinationPath = public_path('employee_documents');
//        $file->move($destinationPath, $input['file']);
//        CloudConvert::file($destinationPath.'/'.$input['file'])->to('pdf');
//        $pdf=explode(".",$input['file']);
//        $input['pdf'] = $destinationPath.'/'.$pdf[0].'.pdf';


        $time = time();
        $file = $request->file('file');
        //dd($file);
        $name = $file->getClientOriginalName();
        $pdf=explode(".",$name);
        $attachments = $time.'.'.$file->getClientOriginalExtension();
        $destinationPath = public_path('/uploads/static');
        $file->move($destinationPath, $attachments);
        $input['file']= $destinationPath.'/'.$time.'.pdf';
        $converter = new OfficeConverter($destinationPath.'/'.$attachments);
        $converter->convertTo($input['file']);
        $input['pdf']  = $input['file'];



        $input['expiry'] = Carbon::parse($input['expiry']);
        $input['issue'] = Carbon::parse($input['issue']);
        $document=Documents::create($input);
        unset($input['link_table']);
        unset($input['link_id']);
        unset($input['pdf']);
        $input['issue']=Carbon::parse($input['issue'])->format('jS M Y');
        $input['expiry']=Carbon::parse($input['expiry'])->format('jS M Y');
        audit_log($this->cms_array,audit_log_data($input),Config::get('constants.store'),$this->page_name,$this->tab_name,$document->id,'',$document->id,Session::get('employee_id'),$this->db);
        return response()->json(['message'=>'Successfully Added !!']);
    }
    public function delete(Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.delete'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('employee_id'),$this->db);
        Documents::where('id',$request->id)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function active(Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.restore'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('employee_id'),$this->db);
        Documents::where('id',$request->id)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
    public function activeMultiple(Request $request)
    {
        Documents::whereIn('id',$request->ids)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
     public function deleteMultipleSOft(Request $request)
    {
        Documents::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }

    public function deleteMultiple(Request $request){
        Documents::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }

    public function exportPdf(Request $request)
    {
        if($request->pdf_array == 1)
        {
           $data= Documents::where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->pdf_array);
            $data= Documents::whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        $companies='Patroltec';
        $reportname='Document';
        $date=\Carbon\Carbon::now()->format('D jS M Y');
        $pdf = PDF::loadView('backend.employee_document.pdf',compact('companies','reportname','data','date'));
        return $pdf->download('document.pdf');
    }

    public function exportWorld(Request $request)
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        if($request->word_array == 1)
        {
            $data= Documents::where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->word_array);
            $data= Documents::whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        $table = $section->addTable();
        foreach ($data as $key => $value) {
            if($key==0)
            {
                $table->addRow();
                if(Auth()->user()->hasPermissionTo('employee_document_name_word_export') || Auth::user()->all_companies == 1)
                {
                    $table->addCell(1750)->addText('Name');
                }
                if(Auth()->user()->hasPermissionTo('employee_document_file_word_export') || Auth::user()->all_companies == 1)
                {
                    $table->addCell(1750)->addText('File');
                }
                if(Auth()->user()->hasPermissionTo('employee_document_expiry_date_word_export') || Auth::user()->all_companies == 1)
                {
                    $table->addCell(1750)->addText('Expiry');
                }
            }
            $table->addRow();
            if(Auth()->user()->hasPermissionTo('employee_document_name_word_export') || Auth::user()->all_companies == 1)
            {
                $table->addCell(1750)->addText($value->name);
            }
            if(Auth()->user()->hasPermissionTo('employee_document_file_word_export') || Auth::user()->all_companies == 1)
            {
                $table->addCell(1750)->addText($value->file);
            }
            if(Auth()->user()->hasPermissionTo('employee_document_expiry_date_word_export')|| Auth::user()->all_companies == 1)
            {
                $table->addCell(1750)->addText($value->expiry);
            }

        }
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('document.docx');
        return response()->download(public_path('document.docx'));
    }
    public function exportExcel(Request $request)
    {
        if($request->excel_array == 1)
        {
            $data= Documents::where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->excel_array);
            $data= Documents::whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        return Excel::download(new EmployeeDocumentExport($data), 'document.csv');
    }

}
