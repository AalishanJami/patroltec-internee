<?php
namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Location;
use App\Divisions;
use App\Customers;
use App\SiteGroups;
use App\User;
use App\Clients;
use App\Contractor;
use Spatie\Permission\Models\Role;
use App\Company;
use App\CompanyUser;
use Auth;
use App\Exports\project\projectExport;
use App\Exports\project\projectExportSoft;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use Config;
use Session;
use DB;
use DataTables;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Hash;
use Validator;

class ProjectController extends Controller
{
    private $page_name;
    private $tab_name;
    private $db;
    private $cms_array=[];
    public function __construct() {
        $this->page_name ='project';
        $this->tab_name ='main_detail';
        $this->db='locations';
        $cms_data=localization();
        $temp_array=[];
        $temp_array['name']['key']='name';
        $temp_array['name']['value']=checkKey('name',$cms_data);
        $temp_array['ref']['key']='ref';
        $temp_array['ref']['value']=checkKey('ref',$cms_data);
        $temp_array['account_number']['key']='account_number';
        $temp_array['account_number']['value']=checkKey('account_number',$cms_data);
        $temp_array['contractor_id']['key']='contractor';
        $temp_array['contractor_id']['value']=checkKey('contractor',$cms_data);
        $temp_array['client_id']['key']='client';
        $temp_array['client_id']['value']=checkKey('client',$cms_data);
        $temp_array['manager_id']['key']='manager';
        $temp_array['manager_id']['value']=checkKey('manager',$cms_data);
        $temp_array['contract_manager_id']['key']='contract_manager';
        $temp_array['contract_manager_id']['value']=checkKey('contract_manager',$cms_data);
        $temp_array['division_id']['key']='devision';
        $temp_array['division_id']['value']=checkKey('devision',$cms_data);
        $temp_array['customer_id']['key']='customer';
        $temp_array['customer_id']['value']=checkKey('customer',$cms_data);
        $temp_array['site_group_id']['key']='group';
        $temp_array['site_group_id']['value']=checkKey('group',$cms_data);
        $temp_array['notes']['key']='notes';
        $temp_array['notes']['value']=checkKey('notes',$cms_data);
        $temp_array['contract_start']['key']='start_date';
        $temp_array['contract_start']['value']=checkKey('start_date',$cms_data);
        $temp_array['contract_end']['key']='expiry_date';
        $temp_array['contract_end']['value']=checkKey('expiry_date',$cms_data);
        $temp_array['termination_reason']['key']='termination_reason';
        $temp_array['termination_reason']['value']=checkKey('termination_reason',$cms_data);
        $this->cms_array=$temp_array;
        Session::forget('employee_id');
        Session::forget('form_id');
        Session::forget('folder_id');
    }
    public function index(){
        $data =Location::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        $data=sortData($data);
    	$companies=Company::getCompanies();
    	$contractors = Contractor::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get()->pluck('name','id');
    	$managers = User::where('company_id',Auth::user()->default_company)->where('position',2)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get()->pluck('first_name','id');
    	$contractManagers = User::where('company_id',Auth::user()->default_company)->where('position',3)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get()->pluck('first_name','id');
    	$clients = Clients::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get()->pluck('first_name','id');
    	$divisions = Divisions::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get()->pluck('name','id');
    	$customers = Customers::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get()->pluck('name','id');
    	$groups = SiteGroups::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        $groups=sortData($groups);
    	return view('backend.project.index')->with(['companies'=>$companies,'contractors'=>$contractors,'managers'=>$managers,'contractManagers'=>$contractManagers,'clients'=>$clients,'divisions'=>$divisions,'customers'=>$customers,'groups'=>$groups]);
    }
    public function createDetail(){
        $data=Location::create(['name'=>'Edit  Project']);
       return $this->detail($data->id);
    }
    public function store(Request $request){
    	$input = $request->all();
    	$location=Location::create($input);
        audit_log($this->cms_array,audit_log_data($input),Config::get('constants.store'),$this->page_name,$this->tab_name,$location->id,'',$location->id,'',$this->db);
    	return response()->json(['message'=>'New Location Added Successfully!','location_id'=>$location->id]);
    }
    public function updateDetail(Request $request){
        $data = [
            'name'=>'required',
        ];
        $validator = Validator::make($request->all(), $data);
        if ($validator->fails()) {
            return back()->withErrors($validator)->with('style','border-bottom:1px solid #e36db1;')->withInput();
        }
        $input = $request->all();
        if(isset($input['contract_start']))
        {
            $input['contract_start']=str_replace(',','',$input['contract_start']);
            $date = Carbon::parse($input['contract_start'])->toDateString();
            $input['contract_start'] = $date;
        }
        if(isset($input['contract_end']))
        {
            $input['contract_end']=str_replace(',','',$input['contract_end']);
            $date = Carbon::parse($input['contract_end'])->toDateString();
            $input['contract_end'] = $date;

        }
        unset($input['_token']);
        $location=Location::find($input['id']);
        audit_log($this->cms_array,audit_log_data($input),Config::get('constants.update'),$this->page_name,$this->tab_name,$location->id,audit_log_data($location->toArray()),$location->id,'',$this->db);
        $location->update($input);
    	return redirect()->back();
    }

	public function getAll(Request $request){
        // dd($request->all());
    	$data =Location::where('company_id',Auth::user()->default_company)->with('devisions','customer','siteGroup','userManager','userContractManager','clients')->orderBy('id', 'desc')->where('active',1)->where('deleted',0)->get();
        $data=sortData($data);
    	return Datatables::of($data)->addColumn('checkbox', function($data){})
        ->addColumn('actions', function($data){
                $response="";
                $edit ='<a href="'.url("/project/detail/".$data->id).'" class="btn btn-primary btn-xs mr-2"><i class="fas fa-pencil-alt"></i></a>';
                $delete ='<a class="btn btn-danger btn-xs my-0" onclick="deleteProject( '.$data->id . ')"><i class="fas fa-trash"></i></a>';
                if(Auth()->user()->hasPermissionTo('edit_project') || Auth::user()->all_companies == 1)
                {
                    $response=$response.$edit;
                }
                if(Auth()->user()->hasPermissionTo('delete_project') || Auth::user()->all_companies == 1)
                {
                    $response=$response.$delete;
                }
                return $response;
            })->addColumn('divisions', function($data){
                $response="";
                if (!empty($data->devisions)){
                    $response = $data->devisions->name;
                }else{
                    $response = "";
                }
                return $response;
            })->addColumn('customer_id', function($data){
                $response="";
                if (!empty($data->customer)){
                    $response = $data->customer->name;
                }else{
                    $response = "";

                }
                return $response;
            })->addColumn('site_group_id', function($data){
                $response="";
                if (!empty($data->siteGroup)){
                    $response = $data->siteGroup->name;
                }else{
                    $response = "";
                }
                return $response;
            })->addColumn('manager_id', function($data){
                $response="";
                if (!empty($data->userManager)){
                    $response = $data->userManager->first_name;
                }else{
                    $response = "";
                }
                return $response;
            })->addColumn('contract_manager_id', function($data){
                $response="";
                if (!empty($data->userContractManager)){
                    $response = $data->userContractManager->first_name;
                }else{
                    $response = "";
                }
                return $response;
            })->addColumn('client_id', function($data){
                $response="";
                if (!empty($data->clients)){
                    $response = $data->clients->first_name;
                }else{
                    $response = "";
                }
                return $response;
            })->rawColumns(['actions'])->make(true);
    }
    public function getAllSoft(Request $request){
  		$data =Location::where('company_id',Auth::user()->default_company)->where('active',0)->where('deleted',0)->orderBy('id', 'desc')->get();
        $data=sortData($data);
        return Datatables::of($data)->addColumn('checkbox', function($data){})
       ->addColumn('actions', function ($data) {
               $response = "";
               $Restore_button = '<a class="btn btn-success btn-xs my-0" onclick="restoreProject( ' . $data->id . ')"><i class="fa fa-retweet" aria-hidden="true"></i></a>';
               $hard_delete_button = '<a class="btn btn-danger all_action_btn_margin btn-xs my-0" onclick="hardDeleteProject( ' . $data->id . ')"><i class="fa fa-trash" aria-hidden="true"></i></a>';
               if(Auth()->user()->hasPermissionTo('restore_delete_project') || Auth::user()->all_companies == 1)
                {
                    $response = $response . $Restore_button;
                }
                if(Auth()->user()->hasPermissionTo('hard_delete_project') || Auth::user()->all_companies == 1)
                {
                    $response = $response . $hard_delete_button;
                }
               return $response;
            })->addColumn('divisions', function($data){
                $response="";
                $key = Divisions::where('id',$data->division_id)->get()->last();
                if($key!=null) {
                    $response = $key->name;
                }
                return $response;
            })->addColumn('customer_id', function($data){
                $response="";
                $key = Customers::where('id',$data->customer_id)->get()->last();
                if($key!=null) {
                    $response = $key->name;
                }
                return $response;
            })->addColumn('site_group_id', function($data){
                $response="";
                $key = SiteGroups::where('id',$data->site_group_id)->get()->last();
                if($key!=null) {
                    $response = $key->name;
                }
                return $response;
            })->addColumn('manager_id', function($data){
                $response="";
                $key = User::where('id',$data->manager_id)->get()->last();
                if($key!=null) {
                    $response = $key->name;
                }return $response;
            })->addColumn('contract_manager_id', function($data){
                $response="";
                $key = User::where('id',$data->contract_manager_id)->get()->last();
                if($key!=null) {
                    $response = $key->name;
                }
                return $response;
            })->addColumn('client_id', function($data){
                $response="";
                $key = Clients::where('id',$data->client_id)->get()->last();
                if($key!=null) {
                    $response = $key->first_name;
                }
                return $response;
            })->rawColumns(['actions'])->make(true);
    }

   public function delete(Request $request){
    audit_log($this->cms_array,'',Config::get('constants.delete'),$this->page_name,$this->tab_name,'','',$request->id,'',$this->db);
        $location=Location::where('id',$request->id)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'delete']);
   }

   public function multipleDelete(Request $request){
    // dd($request->all());
        if($request->flag==1) {
            Location::whereIn('id',$request->ids)->update(['active'=>0]);
        return response()->json(['message'=>'Deleted']);
        }else if ($request->flag==2) {
            Location::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Deleted']);
        }
   }

    public function detail($id){
    	$project = Location::where('id',$id)->where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->first();
        if(!$project)
        {
            // toastr()->success('');
            return redirect('project');
        }
    	Session::put('project_id',$id);
        $companies=Company::getCompanies();
        $contractors = Contractor::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id','desc')->get()->pluck('name','id');
        $managers = User::where('company_id',Auth::user()->default_company)->where('position',2)->where('active',1)->where('deleted',0)->orderBy('id','desc')->get()->pluck('first_name','id');
        $contractManagers = User::where('company_id',Auth::user()->default_company)->where('position',3)->where('active',1)->where('deleted',0)->orderBy('id','desc')->get()->pluck('first_name','id');
        $clients = Clients::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id','desc')->get()->pluck('first_name','id');
        $divisions = Divisions::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id','desc')->get()->pluck('name','id');
        $customers = Customers::where('company_id',Auth::user()->default_company)->where('division_id',$project->division_id)->where('active',1)->where('deleted',0)->orderBy('id','desc')->get()->pluck('name','id');
        $groups = SiteGroups::where('company_id',Auth::user()->default_company)->where('customer_id',$project->customer_id)->where('division_id',$project->division_id)->where('active',1)->where('deleted',0)->orderBy('id','desc')->get();
        $groups=sortData($groups);
    	return view('backend.project.details')->with(['companies'=>$companies,'contractors'=>$contractors,'managers'=>$managers,'contractManagers'=>$contractManagers,'clients'=>$clients,'divisions'=>$divisions,'customers'=>$customers,'groups'=>$groups,'project'=>$project]);
    }

    public function restore(Request $request){
    audit_log($this->cms_array,'',Config::get('constants.restore'),$this->page_name,$this->tab_name,'','',$request->id,'',$this->db);
        Location::where('id',$request->id)->update(['active'=>1]);
        return response()->json(['message'=>'restore']);
    }
    public function hardDelete(Request $request){
    audit_log($this->cms_array,'',Config::get('constants.soft_delete'),$this->page_name,$this->tab_name,'','',$request->id,'',$this->db);
        Location::where('id',$request->id)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'hard delete']);
    }
    public function activeMultiple(Request $request)
    {
        Location::whereIn('id',$request->ids)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
    public function exportExcel(Request $request){
        if($request->excel_array == 1)
        {
            $data= Location::where('company_id',Auth::user()->default_company)->with('devisions','customer','siteGroup','userManager','userContractManager','clients')->where('active', '=',1)->where('deleted',0)->orderBy('id', 'desc')->get();
            $data=sortData($data);
        }
        else
        {
            $array=json_decode($request->excel_array);
                $data= Location::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->with('devisions','customer','siteGroup','userManager','userContractManager','clients')->where('active', '=',1)->where('deleted',0)->orderBy('id', 'desc')->get();
            $data=sortData($data);
        }
        return Excel::download(new projectExport($data), 'projects.csv');
    }
    public function exportPdf(Request $request){
        if($request->pdf_array == 1)
        {
            $data= Location::where('company_id',Auth::user()->default_company)->with('devisions','customer','siteGroup','userManager','userContractManager','clients')->where('active', '=',1)->where('deleted',0)->orderBy('id', 'desc')->get();
            $data=sortData($data);
        }
        else
        {
            $array=json_decode($request->pdf_array);
            $data= Location::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->with('devisions','customer','siteGroup','userManager','userContractManager','clients')->where('active', '=',1)->where('deleted',0)->orderBy('id', 'desc')->get();
            $data=sortData($data);
        }
        $companies=Company::getCompanies();
        $contractors = Contractor::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->get();
        $managers = User::where('company_id',Auth::user()->default_company)->where('position',2)->where('active',1)->where('deleted',0)->get();
        $contractManagers = User::where('company_id',Auth::user()->default_company)->where('position',3)->where('active',1)->where('deleted',0)->get();
        $clients = Clients::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->get();
        $divisions = Divisions::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->get();
        $customers = Customers::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->get();
        $groups = SiteGroups::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->get();
        $date=Carbon::now()->format('D jS M Y');
        $groups=sortData($groups);
        $companies='Patroltec';
        $reportname='Project';
        // return view('backend.project.pdf',compact('companies','contractors','managers','contractManagers','clients','divisions','customers','groups','data','date'));
        $pdf = PDF::loadView('backend.project.pdf',compact('companies','reportname','contractors','managers','contractManagers','clients','divisions','customers','groups','data','date'));

        return $pdf->download('project.pdf');
    }
    public function exportWorld(Request $request)
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        if($request->word_array == 1)
        {
            $data= Location::where('company_id',Auth::user()->default_company)->with('devisions','customer','siteGroup','userManager','userContractManager','clients')->where('active', '=',1)->where('deleted',0)->orderBy('id', 'desc')->get();
            $data=sortData($data);
        }
        else
        {
            $array=json_decode($request->word_array);
            $data= Location::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->with('devisions','customer','siteGroup','userManager','userContractManager','clients')->where('active', '=',1)->where('deleted',0)->orderBy('id', 'desc')->get();
            $data=sortData($data);
        }
        $section->addText('Project listing');
        $table = $section->addTable();
        foreach ($data as $key => $value) {
            if($key==0)
            {
                $table->addRow();
                if(Auth()->user()->hasPermissionTo('project_devision_word_export') || Auth::user()->all_companies == 1 )
                {
                    $table->addCell(1750)->addText('Division');
                }
                if(Auth()->user()->hasPermissionTo('project_customer_word_export') || Auth::user()->all_companies == 1 )
                {
                    $table->addCell(1750)->addText('Customer');
                }
                if(Auth()->user()->hasPermissionTo('project_group_word_export') || Auth::user()->all_companies == 1 )
                {
                    $table->addCell(1750)->addText('Site Group');
                }
                if(Auth()->user()->hasPermissionTo('project_manager_word_export') || Auth::user()->all_companies == 1 )
                {
                    $table->addCell(1750)->addText('User Manager');
                }
                if(Auth()->user()->hasPermissionTo('project_contract_manager_word_export') || Auth::user()->all_companies == 1 )
                {
                    $table->addCell(1750)->addText('Contract Manager');
                }
                if(Auth()->user()->hasPermissionTo('project_client_word_export') || Auth::user()->all_companies == 1 )
                {
                    $table->addCell(1750)->addText('Client');
                }
            }
            $table->addRow();
            if (!empty($value->devisions)){
                $devisions =$value->devisions->name;
            }else{
                $devisions ="";
            }
            if (!empty($value->customer)){
                $customer =$value->customer->name;
            }else{
                $customer ="";
            }
            if (!empty($value->siteGroup)){
                $siteGroup =$value->siteGroup->name;
            }else{
                $siteGroup ="";
            }
            if (!empty($value->userManager)){
                $userManager =$value->userManager->name;
            }else{
                $userManager ="";
            }
            if (!empty($value->userContractManager)){
                $userContractManager =$value->userContractManager->name;
            }else{
                $userContractManager ="";
            }
            if (!empty($value->clients)){
                $clients =$value->clients->first_name;
            }else{
                $clients ="";
            }

            if(Auth()->user()->hasPermissionTo('project_devision_word_export') || Auth::user()->all_companies == 1 )
            {
                $table->addCell(1750)->addText($devisions);
            }
            if(Auth()->user()->hasPermissionTo('project_customer_word_export') || Auth::user()->all_companies == 1 )
            {
                $table->addCell(1750)->addText($customer);
            }
            if(Auth()->user()->hasPermissionTo('project_group_word_export') || Auth::user()->all_companies == 1 )
            {
                $table->addCell(1750)->addText($siteGroup);
            }
            if(Auth()->user()->hasPermissionTo('project_manager_word_export') || Auth::user()->all_companies == 1 )
            {
                $table->addCell(1750)->addText($userManager);
            }
            if(Auth()->user()->hasPermissionTo('project_contract_manager_word_export') || Auth::user()->all_companies == 1 )
            {
                $table->addCell(1750)->addText($userContractManager);
            }
            if(Auth()->user()->hasPermissionTo('project_client_word_export') || Auth::user()->all_companies == 1 )
            {
                $table->addCell(1750)->addText($clients);
            }
        }
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('project.docx');
        return response()->download(public_path('project.docx'));
    }
}
