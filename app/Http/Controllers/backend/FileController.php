<?php
namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\FormGroups;
use App\FolderPermision;
use App\FilePermision;
use App\Forms;
use App\Company;
use Config;
use Session;
use Auth;
class FileController extends Controller
{
    private $page_name;
    private $tab_name;
    private $db;
    private $cms_array=[];
    public function __construct() {
        $this->page_name ='files';
        $this->tab_name ='employees';
        $this->db='folder_permisions';
        $cms_data=localization();
        $temp_array=[];
        $temp_array['user_id']['key']='user';
        $temp_array['user_id']['value']=checkKey('user',$cms_data);
        $temp_array['permissions']['key']='permissions';
        $temp_array['permissions']['value']=checkKey('permissions',$cms_data);
        $this->cms_array=$temp_array;
    }
    public function index()
    {
    	$companies=Company::getCompanies();
        $folders=FormGroups::getFromGroups()->get();
        $folders=sortData($folders);
        $folders_array=[];
        foreach ($folders as $key => $value) {
            if($value['enable'] == 1)
            {
                $folders_array[$key]=$value['id'];
            }
        }
        if(count($folders_array) > 0)
        {
            $files=Forms::getFrom()->orWhereIn('form_group_id',$folders_array)->orwhere('form_group_id', '=', 0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $files=Forms::getFrom()->where('form_group_id', '=', 0)->get();
        }
        $files=sortData($files);
    	return view('backend.document.user_document')->with(['folders'=>$folders,'files'=>$files,'companies'=>$companies]);
    }
    public function folerUpdate(Request $request)
    {
        // form_groups
        $old_value=dataPermision('FolderPermision','','folder');
        $new_value=dataPermision('form_groups',$request->folder);    	
        if($request->folder_id)
        {
            FolderPermision::where('employee_id',Session::get('employee_id'))->whereIn('form_groups_id',$request->folder_id)->delete();
    	}
        if(isset($request->folder))
    	{
	    	foreach ($request->folder as $key => $value) {
	    		FolderPermision::create(['form_groups_id'=>$value,'employee_id'=>Session::get('employee_id'),'company_id'=>Auth::user()->default_company]);
	    	}
    	}
        audit_log($this->cms_array,audit_log_data($new_value),Config::get('constants.update'),$this->page_name,$this->tab_name,Session::get('employee_id'),audit_log_data($old_value),Session::get('employee_id'),Session::get('employee_id'),$this->db);
    	toastr()->success('Folder Permision Successfully Updated!');
        return redirect('/employee/documents');
    }
    public function fileUpdate(Request $request)
    {
        $old_value=dataPermision('FilePermision','','file');
        $new_value=dataPermision('forms',$request->file);
        if($request->file_id)
        {
            FilePermision::where('employee_id',Session::get('employee_id'))->whereIn('form_id',$request->file_id)->delete();
        }
    	if(isset($request->file))
    	{
	    	foreach ($request->file as $key => $value) {
	    		FilePermision::create(['form_id'=>$value,'employee_id'=>Session::get('employee_id'),'company_id'=>Auth::user()->default_company]);
	    	}
    	}
        audit_log($this->cms_array,audit_log_data($new_value),Config::get('constants.update'),$this->page_name,$this->tab_name,Session::get('employee_id'),audit_log_data($old_value),Session::get('employee_id'),Session::get('employee_id'),'file_permisions');
    	toastr()->success('File Permision Successfully Updated!');
        return redirect('/employee/documents');
    }
    


   
}
