<?php
namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use App\Label;
use App\CompanyLabel;
class EditLoginController extends Controller
{
    public function index()
    {
    	return view('backend.edit_login.index');
    }
    public function getAllLoginCMS()
    {
    	$labels=localization();
		if ( !\Session::has('locale') )
        {
            \Session::put('locale', \Config::get('app.locale'));
        }
        $lang=\Config::get('app.locale');
    	$data=Label::where('lang',$lang)->with('companyLabelGetOne')->get();
        return Datatables::of($data)
            ->addColumn('value', function ($data) {
            	$value='';
            	if(isset($data->companyLabelGetOne->value))
            	{
            		$value=$data->companyLabelGetOne->value;
            	}

                return $value;
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                $edit = '<a class="btn btn-primary btn-xs" onclick="cms_edit( ' . $data->id . ')"><i class="fas fa-pencil-alt"></i></a>';
                $response = $response . $edit;
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);

    }
    public function edit(Request $request)
    {
    	$lang=selectedLanguage();
    	$data=Label::where('lang',$lang)->where('id',$request->id)->with('companyLabelGetOne')->first();
    	return $data;
    }
    public function update(Request $request)
    {
    	CompanyLabel::where('id',$request->id)->update(['value'=>$request->value]);
    	return response()->json(['message'=>'Value Updated successfully']);
    }
    public function store(Request $request)
    {
    	foreach (getLanguage() as $key => $value) {
    		$label=Label::create(['key'=>$request->key,'lang'=>$value->short_lan,'language_id'=>$value->id]);
    		CompanyLabel::create(['label_id'=>$label->id,'company_id'=>1,'value'=>$request->value]);
    	}
    	return response()->json(['message'=>'Value Create successfully']);
    }

   
}
