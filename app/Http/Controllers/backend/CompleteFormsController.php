<?php
namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Jobs;
use App\JobGroup;
use App\FormVersion;
use App\FormSections;
use App\Company;
use Auth;
use Session;
use Config;
use App\ResponseGroups;
use App\Response;
use App\User;
use App\FormType;
use DataTables;
use CloudConvert;
use Carbon\Carbon;
use Validator;
use App\Location;
use App\LocationBreakdown;
use App\Forms;
use App\Scoring;
class CompleteFormsController extends Controller
{
    private $page_name;
    private $tab_name;
    public function __construct() {
        $this->page_name ='Completed Form';
        $this->tab_name ='Completed Form';
    }
    public function index()
    {
        $companies = Company::getCompanies();
        $users  = User::where('active',1)->where('deleted',0)->where('company_id',Auth::user()->default_company)->orderBy('id', 'desc')->get()->pluck('first_name','id');
        $types = FormType::where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get()->pluck('name','id');
        return view('backend.complete_form.index')->with(['users'=>$users,'types'=>$types,'companies'=>$companies]);
    }

    public function getAllUser()
    {

        $data=JobGroup::with('users','forms','locations','cJobs','locationBreakdowns','responseGroups')->where('active',1)->where('deleted',0)->where('user_id',Session::get('employee_id'))->where('completed',2)->where('company_id',Auth::user()->default_company)->get();
        foreach ($data as $key => $value) {
            if($value->cJobs->isEmpty())
            {
                unset($data[$key]);
            }
            if($value->sign_off==1)
            {
                unset($data[$key]);
            }

        }
        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {
            })
            ->addColumn('employee_name', function ($data) {
                $response = "";
                $response = $data->users->first_name ?? "";
                return $response;
            }) ->addColumn('location_name', function ($data) {
                $response = "";
                $response = $data->locations->name ?? "";
                return $response;
            }) ->addColumn('location_breakdown', function ($data) {
                $response = "";
                $response = $data->locationBreakdowns->name ?? "";
                return $response;
            })->addColumn('form_name', function ($data) {
                $response = "";
                $response = $data->forms->name ?? "";
                return $response;
            })->addColumn('date_time', function ($data) {
                $response = "";
                if(isset($data->jobs[0]->date_time))
                    $response =Carbon::parse( $data->jobs[0]->date_time)->format('jS M Y H:i');
                return $response;
            })->addColumn('score', function ($data) {
                $response = "";
                if($data->responseGroups)
                    $response =$data->responseGroups->sum('score');
                return $response;
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                if($data->id)
                {
                    $view = '<a type="button" class="btn btn-primary btn-xs my-0 waves-effect waves-light" href="'.url("/completed_forms/viewDocument").'/'.$data->id.'"><i class="fas fa-eye"></i></a>';
                    $response = $response . $view;
                    if(!$data->sign_off)
                    {
                        $location_id =null;
                        $location_break =null;
                        if($data->locations)
                            $location_id=$data->locations->id;
                        if($data->locationBreakdowns)
                            $location_break=$data->locationBreakdowns->id;

                        $off = '<a  data-toggle="modal" data-target="#modelSignOff" class="btn btn-success all_action_btn_margin btn-xs my-0 waves-effect waves-light" onclick="selectedJob('.$data->id.','.$location_id.','.$location_break.')" ><i class="fas fa-pen-nib"></i></a>';
                        $response = $response . $off;

                    }
                }
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);

    }

    public function getAllFilterUser($id=null,$fl)
    {

        $data=JobGroup::with('users','forms','locations','cJobs','locationBreakdowns','responseGroups')->where('active',1)->where('deleted',0)->where('user_id',Session::get('employee_id'))->where('completed',2)->where('company_id',Auth::user()->default_company)->get();

        foreach ($data as $key => $value) {
            if($value->cJobs->isEmpty())
            {
                unset($data[$key]);
            }
            if($fl=='approved')
            {
                if($value->sign_off==-1 || $value->sign_off==null )
                    unset($data[$key]);
            }
            else if($fl=='rejected')
            {
                if($value->sign_off==1 || $value->sign_off==null)
                    unset($data[$key]);
            }
            else if($fl=='not_required')
            {
                if($value->forms->req_sign_off==1 || $value->sign_off==null)
                    unset($data[$key]);
            }

        }

        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {

            })
            ->addColumn('employee_name', function ($data) {
                $response = "";
                $response = $data->users->first_name ?? "";
                return $response;
            }) ->addColumn('location_name', function ($data) {
                $response = "";

                $response = $data->locations->name ?? "";
                return $response;
            }) ->addColumn('location_breakdown', function ($data) {
                $response = "";

                $response = $data->locationBreakdowns->name ?? "";
                return $response;
            })->addColumn('form_name', function ($data) {
                $response = "";

                $response = $data->forms->name ?? "";
                return $response;
            })->addColumn('date_time', function ($data) {
                $response = "";
                if(isset($data->jobs[0]->date_time))
                    $response =Carbon::parse( $data->jobs[0]->date_time)->format('jS M Y H:i');
                return $response;
            })->addColumn('score', function ($data) {
                $response = "";
                if($data->responseGroups)
                    $response =$data->responseGroups->sum('score');
                return $response;
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                if($data->id)
                {
                    $view = '<a type="button" class="btn btn-primary btn-xs my-0 waves-effect waves-light" href="'.url("/completed_forms/viewDocument").'/'.$data->id.'"><i class="fas fa-eye"></i></a>';
                    $response = $response . $view;
                    if(!$data->sign_off)
                    {

                        $location_id =null;
                        $location_break =null;
                        if($data->locations)
                            $location_id=$data->locations->id;
                        if($data->locationBreakdowns)
                            $location_break=$data->locationBreakdowns->id;

                        $off = '<a  data-toggle="modal" data-target="#modelSignOff" class="btn btn-success all_action_btn_margin btn-xs my-0 waves-effect waves-light" onclick="selectedJob('.$data->id.','.$location_id.','.$location_break.')" ><i class="fas fa-pen-nib"></i></a>';
                        $response = $response . $off;

                    }
                }
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);

    }

    public function getAllSignedUser()
    {

        $data=JobGroup::with('users','sForms','locations','cJobs','locationBreakdowns','responseGroups')->where('active',1)->where('deleted',0)->where('user_id',Session::get('employee_id'))->where('company_id',Auth::user()->default_company)->get();

        foreach ($data as $key => $value) {
            if($value->cJobs->isEmpty())
            {
                unset($data[$key]);
            }
            if($value->sign_off==0)
            {
                unset($data[$key]);
            }

        }

        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {

            })
            ->addColumn('employee_name', function ($data) {
                $response = "";
                $response = $data->users->first_name ?? "";
                return $response;
            }) ->addColumn('location_name', function ($data) {
                $response = "";

                $response = $data->locations->name ?? "";
                return $response;
            }) ->addColumn('location_breakdown', function ($data) {
                $response = "";

                $response = $data->locationBreakdowns->name ?? "";
                return $response;
            })->addColumn('form_name', function ($data) {
                $response = "";

                $response = $data->forms->name ?? "";
                return $response;
            })->addColumn('date_time', function ($data) {
                $response = "";
                if(isset($data->jobs[0]->date_time))
                    $response =Carbon::parse( $data->jobs[0]->date_time)->format('jS M Y H:i');
                return $response;
            })->addColumn('score', function ($data) {
                $response = "";
                if($data->responseGroups)
                    $response =$data->responseGroups->sum('score');
                return $response;
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                if($data->id)
                {
                    $view = '<a type="button" class="btn btn-primary btn-xs my-0 waves-effect waves-light" href="'.url("/completed_forms/viewDocument").'/'.$data->id.'"><i class="fas fa-eye"></i></a>';
                    $response = $response . $view;
                }
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);

    }

    //.......Documnet...
    public function document()
    {
        $companies = Company::getCompanies();
        $users  = User::getUser()->get()->pluck('first_name','id');
        $types = FormType::where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get()->pluck('name','id');
        return view('backend.complete_form.document')->with(['users'=>$users,'types'=>$types,'companies'=>$companies]);
    }
    public function getAllDocument()
    {
        $data=JobGroup::with('users','forms','locations','cJobs','locationBreakdowns','responseGroups')->where('active',1)->where('deleted',0)->where('form_id',Session::get('form_id'))->where('completed',2)->where('company_id',Auth::user()->default_company)->get();
        foreach ($data as $key => $value) {
            if($value->cJobs->isEmpty())
            {
                unset($data[$key]);
            }
            if($value->sign_off==1 || $value->sign_off==-1)
            {
                unset($data[$key]);
            }

        }
        $fl=null;
        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {

            })
            ->addColumn('employee_name', function ($data) {
                $response = "";
                $response = $data->users->first_name ?? "";
                return $response;
            }) ->addColumn('location_name', function ($data) {
                $response = "";

                $response = $data->locations->name ?? "";
                return $response;
            }) ->addColumn('location_breakdown', function ($data) {
                $response = "";

                $response = $data->locationBreakdowns->name ?? "";
                return $response;
            })->addColumn('form_name', function ($data) {
                $response = "";

                $response = $data->forms->name ?? "";
                return $response;
            })->addColumn('date_time', function ($data) {
                $response = "";
                if(isset($data->jobs[0]->date_time))
                    $response =Carbon::parse( $data->jobs[0]->date_time)->format('jS M Y H:i');
                return $response;
            })->addColumn('score', function ($data) {
                $response = "";
                if($data->responseGroups)
                    $response =$data->responseGroups->sum('score');
                return $response;
            })->addColumn('status', function ($data)  use ($fl){
                $response = "";
                if($fl=='approved')
                {
                    $response = 'completed';
                }
                else if($fl=='rejected')
                {
                    $response = 'failed';
                }
                else if($fl=='not_required')
                {

                    $response = 'completed';
                }
                else if($fl=='all')
                {
                    if($data->sign_off==1)
                        $response = 'completed';
                    if($data->sign_off==-1)
                        $response = 'failed';
                    if($data->forms->req_sign_off==1)
                        $response = 'completed';
                    if($data->sign_off==null)
                        $response = 'waitin approval';
                }
                else
                {
                    $response = 'waitin approval';
                }


                return $response;
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                if($data->id)
                {
                    $view = '<a type="button" class="btn btn-primary btn-xs my-0 waves-effect waves-light" href="'.url("/completed_forms/viewDocument").'/'.$data->id.'"><i class="fas fa-eye"></i></a>';
                    $response = $response . $view;
                    if(!$data->sign_off)
                    {

                        $location_id =null;
                        $location_break =null;
                        if($data->locations)
                            $location_id=$data->locations->id;
                        if($data->locationBreakdowns)
                            $location_break=$data->locationBreakdowns->id;

                        $off = '<a  data-toggle="modal" data-target="#modelSignOff" class="btn btn-success all_action_btn_margin btn-xs my-0 waves-effect waves-light" onclick="selectedJob('.$data->id.','.$location_id.','.$location_break.')" ><i class="fas fa-pen-nib"></i></a>';
                        $response = $response . $off;

                    }
                }
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);

    }

    public function getAllFilterDocument($id=null,$fl)
    {

        $data=JobGroup::with('users','forms','locations','cJobs','locationBreakdowns','responseGroups')->where('active',1)->where('deleted',0)->where('form_id',Session::get('form_id'))->where('completed',2)->where('company_id',Auth::user()->default_company)->get();


        foreach ($data as $key => $value) {
            if($value->cJobs->isEmpty())
            {
                unset($data[$key]);
            }
            if($fl=='approved')
            {
                if($value->sign_off==-1 || $value->sign_off==null )
                    unset($data[$key]);
            }
            else if($fl=='rejected')
            {
                if($value->sign_off==1 || $value->sign_off==null)
                    unset($data[$key]);
            }
            else if($fl=='not_required')
            {
                if($value->forms->req_sign_off==1 || $value->sign_off==null)
                    unset($data[$key]);
            }

        }
        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {

            })
            ->addColumn('employee_name', function ($data) {
                $response = "";
                $response = $data->users->first_name ?? "";
                return $response;
            }) ->addColumn('location_name', function ($data) {
                $response = "";

                $response = $data->locations->name ?? "";
                return $response;
            }) ->addColumn('location_breakdown', function ($data) {
                $response = "";

                $response = $data->locationBreakdowns->name ?? "";
                return $response;
            })->addColumn('form_name', function ($data) {
                $response = "";

                $response = $data->forms->name ?? "";
                return $response;
            })->addColumn('date_time', function ($data) {
                $response = "";
                if(isset($data->jobs[0]->date_time))
                    $response =Carbon::parse( $data->jobs[0]->date_time)->format('jS M Y H:i');
                return $response;
            })->addColumn('score', function ($data) {
                $response = "";
                if($data->responseGroups)
                    $response =$data->responseGroups->sum('score');
                return $response;
            })->addColumn('status', function ($data)  use ($fl){
                $response = "";
                if($fl=='approved')
                {
                    $response = 'completed';
                }
                else if($fl=='rejected')
                {
                    $response = 'failed';
                }
                else if($fl=='not_required')
                {

                    $response = 'completed';
                }
                else if($fl=='all')
                {
                    if($data->sign_off==1)
                        $response = 'completed';
                    if($data->sign_off==-1)
                        $response = 'failed';
                    if($data->forms->req_sign_off==1)
                        $response = 'completed';
                    if($data->sign_off==null)
                        $response = 'waitin approval';
                }
                else
                {
                    $response = 'waitin approval';
                }


                return $response;
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                if($data->id)
                {
                    $view = '<a type="button" class="btn btn-primary btn-xs my-0 waves-effect waves-light" href="'.url("/completed_forms/viewDocument").'/'.$data->id.'"><i class="fas fa-eye"></i></a>';
                    $response = $response . $view;
                    if(!$data->sign_off)
                    {

                        $location_id =null;
                        $location_break =null;
                        if($data->locations)
                            $location_id=$data->locations->id;
                        if($data->locationBreakdowns)
                            $location_break=$data->locationBreakdowns->id;

                        $off = '<a  data-toggle="modal" data-target="#modelSignOff" class="btn btn-success all_action_btn_margin btn-xs my-0 waves-effect waves-light" onclick="selectedJob('.$data->id.','.$location_id.','.$location_break.')" ><i class="fas fa-pen-nib"></i></a>';
                        $response = $response . $off;

                    }
                }
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);

    }

    public function getAllSignedDocument()
    {

        $data=JobGroup::with('users','sForms','locations','cJobs','locationBreakdowns','responseGroups')->where('active',1)->where('deleted',0)->where('form_id',Session::get('form_id'))->where('company_id',Auth::user()->default_company)->get();

        foreach ($data as $key => $value) {
            if($value->cJobs->isEmpty())
            {
                unset($data[$key]);
            }
            if($value->sign_off==0)
            {
                unset($data[$key]);
            }

        }
        $fl='approved';
        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {

            })
            ->addColumn('employee_name', function ($data) {
                $response = "";
                $response = $data->users->first_name ?? "";
                return $response;
            }) ->addColumn('location_name', function ($data) {
                $response = "";

                $response = $data->locations->name ?? "";
                return $response;
            }) ->addColumn('location_breakdown', function ($data) {
                $response = "";

                $response = $data->locationBreakdowns->name ?? "";
                return $response;
            })->addColumn('form_name', function ($data) {
                $response = "";

                $response = $data->forms->name ?? "";
                return $response;
            })->addColumn('date_time', function ($data) {
                $response = "";
                if(isset($data->jobs[0]->date_time))
                    $response =Carbon::parse( $data->jobs[0]->date_time)->format('jS M Y H:i');
                return $response;
            })->addColumn('score', function ($data) {
                $response = "";
                if($data->responseGroups)
                    $response =$data->responseGroups->sum('score');
                return $response;
            })->addColumn('status', function ($data)  use ($fl){
                $response = "";
                if($fl=='approved')
                {
                    $response = 'completed';
                }
                else if($fl=='rejected')
                {
                    $response = 'failed';
                }
                else if($fl=='not_required')
                {

                    $response = 'completed';
                }
                else if($fl=='all')
                {
                    if($data->sign_off==1)
                        $response = 'completed';
                    if($data->sign_off==-1)
                        $response = 'failed';
                    if($data->forms->req_sign_off==1)
                        $response = 'completed';
                    if($data->sign_off==null)
                        $response = 'waitin approval';
                }
                else
                {
                    $response = 'waitin approval';
                }


                return $response;
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                if($data->id)
                {
                    $view = '<a type="button" class="btn btn-primary btn-xs my-0 waves-effect waves-light" href="'.url("/completed_forms/viewDocument").'/'.$data->id.'"><i class="fas fa-eye"></i></a>';
                    $response = $response . $view;
                }
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);

    }


    public function viewDocument($id)
    {
        $companies=Company::getCompanies();

        $job_group = JobGroup::with('users','forms','locations','locationBreakdowns','responseGroups')->find($id);
        $jobs = Jobs::with('form_Section.questions.answer_type','form_Section.questions.answer_group.answer','form_Section.form_version.version')->where('job_group_id',$job_group->id)->orderBy('order', 'asc')->get();
        $ids = $jobs->pluck('id');
        $response_groups = ResponseGroups::with('responses')->where('job_group_id',$job_group->id)->where('form_id',$job_group->form_id)->where('company_id',Auth::user()->default_company)->get();
        //dd($job_group->responseGroups);
        $populate = null;

        $id_static='';
        if(isset($job->form_Section->form_version->version->static_form_uploads_id))
        {
            $id_static=$job->form_Section->form_version->version->static_form_uploads_id;
        }




        return view('backend.complete_form.viewjs')->with(['jobs'=>$jobs,'companies'=>$companies,'id_static'=>$id_static,'job_group'=>$job_group,'populate'=>$populate,'response_groups'=>$response_groups,'ids'=>$ids]);
    }



    public function getVersion(Request $request)
    {
        $jobs = Jobs::with('form_Section.questionsS.answer_type','form_Section.questionsS.answer_group.answer','form_Section.form_version.version')->where('job_group_id',$request->varsion_id)->where('active',1)->where('deleted',0)->orderBy('order','asc')->get();
        $form_id = JobGroup::find($request->varsion_id)->form_id;

        $form_sections = $jobs->pluck('form_section_id');
        $form_sections = array_values($form_sections->toArray());
        $form_section = FormSections::whereIn('id',$form_sections)->with('questionsS.answer_type','questionsS.answer_group.answer')->orderBy('order','asc')->get();
        $response_groups = Response::whereIn('job_id',$jobs->pluck('id'))->whereIn('form_section_id',$form_sections)->where('active',1)->where('deleted',0)->get();
        $rs = ResponseGroups::with('responses')->where('job_group_id',$request->varsion_id)->where('form_id',$form_id)->get();
        //dd($rs);


        return response()->json(['form_section'=>$form_section,'jobs'=>$jobs,'response_groups'=>$response_groups,'rs'=>$rs]);
    }
    public function viewPdf($id)
    {
        $destinationPath = public_path('/uploads/response');

        return response()->file($destinationPath.'/'.$id.'.pdf');
    }

    public function signOff(Request $request)
    {

        $input = $request->all();
        //dd($input);
        if($input['toggleDiv']==0)
        {
            $jobdata=JobGroup::where('id',$input['job_group_id'])->first();
            JobGroup::find($input['job_group_id'])->update(['sign_off'=>-1,'sign_off_reason'=>$input['comment']]);
            /* audit_log($input,Config::get('constants.update'),$this->page_name,$this->tab_name,$input['job_group_id'],$jobdata->toArray(),$input['job_group_id']);*/
            unset($input['job_group_id']);
            unset($input['comment']);

            $data = [
                'date'=>'required',
                'time'=>'required',
            ];
            $validator = Validator::make($request->all(), $data);
            if ($validator->fails())
            {
                $notification = array(
                    'message' => 'date and time required',
                    'alert-type' => 'error'
                );
                return redirect()->back()->with($notification);
            }

            $input['contractor_id'] = Location::find($input['location_id'])->contractor_id;
            $input['user_group_id'] = User::find($input['user_id'])->user_group_id;
            $form_sections = FormVersion::where('form_id',$input['form_id'])->where('active',1)->where('deleted',0)->orderBy('id','desc')->get();
            $form = Forms::find($input['form_id']);

            $form_sections = $form_sections->groupBy('version_id')->first()->pluck('form_section_id');
            $form_section = array_unique($form_sections->toArray());
            $form_sections = FormSections::whereIn('id',$form_section)->with('questions.answer_type','questions.answer_group.answer')->orderBy('order','asc')->get();



            $date = Carbon::parse($input['date']);
            $time = explode(':',$input['time']);
            $date->setTime($time[0],$time[1]);
            $input['date_time'] = $date;


            unset($input['_token']);
            unset($input['date']);
            unset($input['time']);
            $input['title']=$form->name;
            $input['completed']=0;
            $input['company_id'] = Auth::user()->default_company;
            $jobGroup = JobGroup::create($input);

            $input['job_group_id'] = $jobGroup->id;

            $x = [];
            foreach ($form_sections as $form_section) {

                array_push($x,"1");
                $input['form_section_id'] = $form_section->id;
                //dd($input);
                Jobs::Create($input);

            }
            if(empty($x))
            {
                $notification = array(
                    'message' => 'No active Form found with Questions',
                    'alert-type' => 'error'
                );
                return redirect()->back()->with($notification);
            }
            else
            {
                $notification = array(
                    'message' => 'Job raised Successfully',
                    'alert-type' => 'success'
                );
                return redirect()->back()->with($notification);

            }

        }else
        {
            $jobdata=JobGroup::where('id',$input['job_group_id'])->first();
            JobGroup::find($input['job_group_id'])->update(['sign_off'=>1,'sign_off_reason'=>$input['comment']]);
            $notification = array(
                'message' => 'Job SignedOff Successfully',
                'alert-type' => 'success'
            );
            /*  audit_log($input,Config::get('constants.update'),$this->page_name,$this->tab_name,$input['job_group_id'],$jobdata->toArray(),$input['job_group_id']);*/

            return redirect()->back()->with($notification);
        }



    }

//........project..........

    public function project($id=null)
    {
        $companies = Company::getCompanies();
        $users  = User::getUser()->get()->pluck('first_name','id');
        $types = FormType::where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get()->pluck('name','id');

        if($id==null)
        {
            $id=0;
        }else
        {
           Session::put('project_id',$id);
        }
        return view('backend.complete_form.project')->with(['users'=>$users,'types'=>$types,'id'=>$id,'companies'=>$companies]);
    }


    public function getAllProject($id=null)
    {

        if($id==null || $id=='null')
        {
            $data=JobGroup::with('users','forms','locations','cJobs','locationBreakdowns','responseGroups')->where('active',1)->where('deleted',0)->where('location_id',Session::get('project_id'))->where('company_id',Auth::user()->default_company)->get();
        }
        else
        {
            $data=JobGroup::with('users','forms','locations','cJobs','locationBreakdowns','responseGroups')->where('active',1)->where('deleted',0)->where('location_id',$id)->where('company_id',Auth::user()->default_company)->get();

        }


        foreach ($data as $key => $value) {
            if($value->cJobs->isEmpty())
            {
                unset($data[$key]);
            }
            if($value->sign_off==1 || $value->sign_off==-1)
            {
                unset($data[$key]);
            }

        }
        $fl=null;
        //  dd($data);

        $total =0;
        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {

            })
            ->addColumn('employee_name', function ($data) {
                $response = "";
                $response = $data->users->first_name ?? "";
                return $response;
            }) ->addColumn('location_name', function ($data) {
                $response = "";

                $response = $data->locations->name ?? "";
                return $response;
            }) ->addColumn('location_breakdown', function ($data) {
                $response = "";

                $response = $data->locationBreakdowns->name ?? "";
                return $response;
            })->addColumn('form_name', function ($data) {
                $response = "";

                $response = $data->forms->name ?? "";
                return $response;
            })->addColumn('date_time', function ($data) {
                $response = "";
                if(isset($data->jobs[0]->date_time))
                    $response =Carbon::parse( $data->jobs[0]->date_time)->format('jS M Y H:i');
                return $response;
            })->addColumn('score', function ($data){
                $response = 0;

                if($data->responseGroups)
                    $response =$data->responseGroups->sum('score');
                return $response;
            })->addColumn('form_type_score', function ($data){
                $response = 0;
                if($data->formTypes)
                    $response =$data->formTypes->action_score;

                return $response;
            })->addColumn('form_score', function ($data) {
                $response = 0;
                $score = Scoring::where('form_id',$data->form_id)->where('active',1)->where('deleted',0)->where('location_id',$data->location_id)->get()->last();
                //dd($score);
                if($score)
                    $response =$score->score;
                return $response;
            })->addColumn('total_score', function ($data){
            })->addColumn('status', function ($data)  use ($fl){
                $response = "";
                if($fl=='approved')
                {
                    $response = 'completed';
                }
                else if($fl=='rejected')
                {
                    $response = 'failed';
                }
                else if($fl=='not_required')
                {

                    $response = 'completed';
                }
                else if($fl=='all')
                {
                    if($data->sign_off==1)
                        $response = 'completed';
                    if($data->sign_off==-1)
                        $response = 'failed';
                    if($data->forms->req_sign_off==1)
                        $response = 'completed';
                    if($data->sign_off==null)
                        $response = 'waitin approval';
                }
                else
                {
                    $response = 'waitin approval';
                }


                return $response;
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                if($data->id)
                {

                    $view = '<a type="button" class="btn btn-primary btn-xs my-0 waves-effect waves-light" href="'.url("/completed_forms/viewDocument").'/'.$data->id.'"><i class="fas fa-eye"></i></a>';

                    $response = $response . $view;
                    if(!$data->sign_off)
                    {

                        $location_id =null;
                        $location_break =null;
                        if($data->locations)
                            $location_id=$data->locations->id;
                        if($data->locationBreakdowns)
                            $location_break=$data->locationBreakdowns->id;

                        $off = '<a  data-toggle="modal" data-target="#modelSignOff" class="btn btn-success all_action_btn_margin btn-xs my-0 waves-effect waves-light" onclick="selectedJob('.$data->id.','.$location_id.','.$location_break.')" ><i class="fas fa-pen-nib"></i></a>';
                        $response = $response . $off;
                    }
                }
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);

    }
    public function getAllFilterProject($id=null,$fl)
    {

        if($id==null || $id=='null')
        {
            $data=JobGroup::with('users','forms','locations','cJobs','locationBreakdowns','responseGroups')->where('active',1)->where('deleted',0)->where('location_id',Session::get('project_id'))->where('company_id',Auth::user()->default_company)->get();
        }
        else
        {
            $data=JobGroup::with('users','forms','locations','cJobs','locationBreakdowns','responseGroups')->where('active',1)->where('deleted',0)->where('location_id',$id)->where('company_id',Auth::user()->default_company)->get();
        }

        foreach ($data as $key => $value) {
            if($value->cJobs->isEmpty())
            {
                unset($data[$key]);
            }
            if($fl=='approved')
            {
                if($value->sign_off==-1 || $value->sign_off==null )
                    unset($data[$key]);
            }
            else if($fl=='rejected')
            {
                if($value->sign_off==1 || $value->sign_off==null)
                    unset($data[$key]);
            }
            else if($fl=='not_required')
            {
                if($value->forms->req_sign_off==1 || $value->sign_off==null)
                    unset($data[$key]);
            }

        }
        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {

            })
            ->addColumn('employee_name', function ($data) {
                $response = "";
                $response = $data->users->first_name ?? "";
                return $response;
            }) ->addColumn('location_name', function ($data) {
                $response = "";

                $response = $data->locations->name ?? "";
                return $response;
            }) ->addColumn('location_breakdown', function ($data) {
                $response = "";

                $response = $data->locationBreakdowns->name ?? "";
                return $response;
            })->addColumn('form_name', function ($data) {
                $response = "";

                $response = $data->forms->name ?? "";
                return $response;
            })->addColumn('date_time', function ($data) {
                $response = "";
                if(isset($data->jobs[0]->date_time))
                    $response =Carbon::parse( $data->jobs[0]->date_time)->format('jS M Y H:i');
                return $response;
            })->addColumn('score', function ($data) {
                $response = "";
                if($data->responseGroups)
                    $response =$data->responseGroups->sum('score');
                return $response;
            })->addColumn('form_type_score', function ($data) {
                $response = "";
                if($data->formTypes)
                    $response =$data->formTypes->action_score;
                return $response;
            })->addColumn('total_score', function ($data){
            })->addColumn('form_score', function ($data){
                $response = "";
                $score = Scoring::where('form_id',$data->form_id)->where('active',1)->where('deleted',0)->where('location_id',$data->location_id)->get()->last();
                //dd($score);
                if($score)
                    $response =$score->score;
                return $response;
            })->addColumn('status', function ($data)  use ($fl){
                $response = "";
                if($fl=='approved')
                {
                    $response = 'completed';
                }
                else if($fl=='rejected')
                {
                    $response = 'failed';
                }
                else if($fl=='not_required')
                {

                    $response = 'completed';
                }
                else if($fl=='all')
                {
                    if($data->sign_off==1)
                        $response = 'completed';
                    if($data->sign_off==-1)
                        $response = 'failed';
                    if($data->forms->req_sign_off==1)
                        $response = 'completed';
                    if($data->sign_off==null)
                        $response = 'waitin approval';
                }
                else
                {
                    $response = 'waitin approval';
                }


                return $response;
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                $view = '<a type="button" class="btn btn-primary btn-xs my-0 waves-effect waves-light" href="'.url("/completed_forms/viewDocument").'/'.$data->id.'"><i class="fas fa-eye"></i></a>';
                $response = $response . $view;
                if(!$data->sign_off)
                {

                    $location_id =null;
                    $location_break =null;
                    if($data->locations)
                        $location_id=$data->locations->id;
                    if($data->locationBreakdowns)
                        $location_break=$data->locationBreakdowns->id;

                    $off = '<a  data-toggle="modal" data-target="#modelSignOff" class="btn btn-success btn-xs my-0 all_action_btn_margin waves-effect waves-light" onclick="selectedJob('.$data->id.','.$location_id.','.$location_break.')" ><i class="fas fa-pen-nib"></i></a>';
                    $response = $response . $off;
                }

                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);


    }


    public function getAllSignedProject($id=null)
    {

        if($id==null || $id=='null')
        {

            $data=JobGroup::with('users','sForms','locations','cJobs','locationBreakdowns','responseGroups')->where('active',1)->where('deleted',0)->where('location_id',Session::get('project_id'))->where('company_id',Auth::user()->default_company)->get();
        }
        else
        {
            $data=JobGroup::with('users','sForms','locations','cJobs','locationBreakdowns','responseGroups')->where('active',1)->where('deleted',0)->where('location_id',$id)->where('company_id',Auth::user()->default_company)->get();
        }
        foreach ($data as $key => $value) {
            if($value->cJobs->isEmpty())
            {
                unset($data[$key]);
            }
            if($value->sign_off==0)
            {
                unset($data[$key]);
            }

        }
        $fl='approved';
        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {

            })
            ->addColumn('employee_name', function ($data) {
                $response = "";
                $response = $data->users->first_name ?? "";
                return $response;
            }) ->addColumn('location_name', function ($data) {
                $response = "";

                $response = $data->locations->name ?? "";
                return $response;
            }) ->addColumn('location_breakdown', function ($data) {
                $response = "";

                $response = $data->locationBreakdowns->name ?? "";
                return $response;
            })->addColumn('form_name', function ($data) {
                $response = "";

                $response = $data->forms->name ?? "";
                return $response;
            })->addColumn('date_time', function ($data) {
                $response = "";
                if(isset($data->jobs[0]->date_time))
                    $response =Carbon::parse( $data->jobs[0]->date_time)->format('jS M Y H:i');
                return $response;
            })->addColumn('score', function ($data) {
                $response = "";

                if($data->responseGroups)
                    $response =$data->responseGroups->sum('score');
                return $response;
             })->addColumn('total_score', function ($data){
            })->addColumn('form_type_score', function ($data) {
                $response = "";
                if($data->formTypes)
                    $response =$data->formTypes->action_score;
                return $response;
            })->addColumn('form_score', function ($data){
                $response = "";
                $score = Scoring::where('form_id',$data->form_id)->where('active',1)->where('deleted',0)->where('location_id',$data->location_id)->get()->last();
                //dd($score);
                if($score)
                    $response =$score->score;
                return $response;
            })->addColumn('status', function ($data)  use ($fl){
                $response = "";
                if($fl=='approved')
                {
                    $response = 'completed';
                }
                else if($fl=='rejected')
                {
                    $response = 'failed';
                }
                else if($fl=='not_required')
                {

                    $response = 'completed';
                }
                else if($fl=='all')
                {
                    if($data->sign_off==1)
                        $response = 'completed';
                    if($data->sign_off==-1)
                        $response = 'failed';
                    if($data->forms->req_sign_off==1)
                        $response = 'completed';
                    if($data->sign_off==null)
                        $response = 'waitin approval';
                }
                else
                {
                    $response = 'waitin approval';
                }


                return $response;
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                $view = '<a type="button" class="btn btn-primary btn-xs my-0 waves-effect waves-light" href="'.url("/completed_forms/viewDocument").'/'.$data->id.'"><i class="fas fa-eye"></i></a>';
                $response = $response . $view;
                if(!$data->sign_off)
                {

                    $location_id =null;
                    $location_break =null;
                    if($data->locations)
                        $location_id=$data->locations->id;
                    if($data->locationBreakdowns)
                        $location_break=$data->locationBreakdowns->id;

                    $off = '<a  data-toggle="modal" data-target="#modelSignOff" class="btn btn-success all_action_btn_margin btn-xs my-0 waves-effect waves-light" onclick="selectedJob('.$data->id.','.$location_id.','.$location_break.')" ><i class="fas fa-pen-nib"></i></a>';
                    $response = $response . $off;
                }

                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

////........report............

    public function report($id=null)
    {
        remove_key();
        $companies = Company::getCompanies();
        $users  = User::where('active',1)->where('deleted',0)->where('company_id',Auth::user()->default_company)->orderBy('id', 'desc')->get()->pluck('first_name','id');
        $types = FormType::where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get()->pluck('name','id');
        if($id==null)
        {
            $id=0;
        }
        return view('backend.complete_form.report')->with(['users'=>$users,'types'=>$types,'id'=>$id,'companies'=>$companies]);
    }
    public function getAllReport($id=null)
    {

        if($id==null || $id=='null')
        {
            $data=JobGroup::with('users','forms','locations','cJobs','locationBreakdowns','responseGroups')->where('active',1)->where('deleted',0)->where('company_id',Auth::user()->default_company)->get();
        }
        else
        {
            $data = FormType::with('jobGroup.users','jobGroup.forms','jobGroup.locations','jobGroup.nJobs','jobGroup.locationBreakdowns')->where('company_id',Auth::user()->default_company)->get()->find($id);

            $data = $data->JobGroup;
        }
        foreach ($data as $key => $value) {
            if($value->cJobs->isEmpty())
            {
                unset($data[$key]);
            }
            if($value->sign_off==1 || $value->sign_off==-1)
            {
                unset($data[$key]);
            }

        }

        return $this->completeFormListDataTable($data);


    }

    public function customSearch(Request $request)
    {


//        if($id==null)
//        {

        $data=JobGroup::with('users','forms','locations','cJobs','locationBreakdowns','responseGroups')
            ->where('active',1)->where('company_id',Auth::user()->default_company)->where('deleted',0);
//        }
//        else
//        {
//            $data = FormType::with('jobGroup.users','jobGroup.forms','jobGroup.locations','jobGroup.nJobs','jobGroup.locationBreakdowns')->get()->find($id);
//
//            $data = $data->JobGroup;
//        }


        if($request->data)
        {
            foreach ($request->data as $key => $value) {
                if($key == 'form_name'){
                    $data = $data->whereHas('forms', function ($query) use ($value) {
                        $query->where('name','LIKE','%'.$value.'%');

                    });
                } else if($key == 'location_name'){
                    $data = $data->whereHas('locations', function ($query) use ($value) {
                        $query->where('name','LIKE','%'.$value.'%');

                    });
                }else if($key == 'location_break_down'){
                    $data = $data->whereHas('locationBreakdowns', function ($query) use ($value) {
                        $query->where('name','LIKE','%'.$value.'%');
                    });
                }else if($key == 'user_name'){
                    $data = $data->whereHas('users', function ($query) use ($value) {
                        $query->where('name','LIKE','%'.$value.'%');

                    });
                }else if($key == 'date_time'){
                    $date_time = Carbon::parse($value)->toDateString();
                    $data = $data->with(['cJobs' => function($query) use ($date_time) {
                        $query->where('date_time','LIKE','%'.$date_time.'%');
                    }]);
                }

            }
        }
        $data = $data->get();
////        dd($dataArray);
//        $dataArray=$dataArray->with('location','equipment')->orderBy('id', 'desc')->get();
        // dd($dataArray);
        return $this->completeFormListDataTable($data);
    }

    public function completeFormListDataTable($data)
    {
        foreach ($data as $key => $value) {
            if($value->cJobs->isEmpty())
            {
                unset($data[$key]);
            }
            if($value->sign_off==1)
            {
                unset($data[$key]);
            }

        }
        $fl=null;
        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {

            })
            ->addColumn('employee_name', function ($data) {
                $response = "";
                $response = $data->users->first_name ?? "";
                return $response;
            }) ->addColumn('location_name', function ($data) {
                $response = "";

                $response = $data->locations->name ?? "";
                return $response;
            }) ->addColumn('location_breakdown', function ($data) {
                $response = "";

                $response = $data->locationBreakdowns->name ?? "";
                return $response;
            })->addColumn('form_name', function ($data) {
                $response = "";

                $response = $data->forms->name ?? "";
                return $response;
            })->addColumn('date_time', function ($data) {
                $response = "";
                if(isset($data->jobs[0]->date_time))
                    $response =Carbon::parse( $data->jobs[0]->date_time)->format('jS M Y H:i');
                return $response;
            })->addColumn('score', function ($data) {
                $response = "";
                if($data->responseGroups)
                {
                    $ob =$data->responseGroups->sum('score');
                    $total =$data->responseGroups->sum('total_score');
                    if($total!=0)
                    {
                        $response = round(($ob/$total)*100)."%";
                    }

                }
                return $response;
            })->addColumn('status', function ($data)  use ($fl){
                $response = "";
                if($fl=='approved')
                {
                    $response = 'completed';
                }
                else if($fl=='rejected')
                {
                    $response = 'failed';
                }
                else if($fl=='not_required')
                {

                    $response = 'completed';
                }
                else if($fl=='all')
                {
                    if($data->sign_off==1)
                        $response = 'completed';
                    if($data->sign_off==-1)
                        $response = 'failed';
                    if($data->forms->req_sign_off==1)
                        $response = 'completed';
                    if($data->sign_off==null)
                        $response = 'waitin approval';
                }
                else
                {
                    $response = 'waitin approval';
                }


                return $response;
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                $view = '<a type="button" class="btn btn-primary btn-xs my-0 waves-effect waves-light" href="'.url("/completed_forms/viewDocument").'/'.$data->id.'"><i class="fas fa-eye"></i></a>';
                $response = $response . $view;
                if(!$data->sign_off)
                {
                    $location_id =null;
                    $location_break =null;
                    if($data->locations)
                        $location_id=$data->locations->id;
                    if($data->locationBreakdowns)
                        $location_break=$data->locationBreakdowns->id;

                    $off = '<a  data-toggle="modal" data-target="#modelSignOff" class="btn btn-success all_action_btn_margin btn-xs my-0 waves-effect waves-light" onclick="selectedJob('.$data->id.','.$location_id.','.$location_break.')" ><i class="fas fa-pen-nib"></i></a>';
                    $response = $response . $off;
                }

                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }


    public function getAllSignedReport($id=null)
    {

        if($id==null|| $id=='null')
        {

            $data=JobGroup::with('users','sForms','locations','cJobs','locationBreakdowns','responseGroups')->where('active',1)->where('deleted',0)->where('company_id',Auth::user()->default_company)->get();
        }
        else
        {
            $data = FormType::with('jobGroup.users','jobGroup.forms','jobGroup.locations','jobGroup.nJobs','jobGroup.locationBreakdowns')->where('company_id',Auth::user()->default_company)->get()->find($id);

            $data = $data->JobGroup;
        }

        foreach ($data as $key => $value) {
            if($value->cJobs->isEmpty())
            {
                unset($data[$key]);
            }
            if($value->sign_off==0)
            {
                unset($data[$key]);
            }

        }
        $fl='approved';
        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {

            })
            ->addColumn('employee_name', function ($data) {
                $response = "";
                $response = $data->users->first_name ?? "";
                return $response;
            }) ->addColumn('location_name', function ($data) {
                $response = "";

                $response = $data->locations->name ?? "";
                return $response;
            }) ->addColumn('location_breakdown', function ($data) {
                $response = "";

                $response = $data->locationBreakdowns->name ?? "";
                return $response;
            })->addColumn('form_name', function ($data) {
                $response = "";

                $response = $data->forms->name ?? "";
                return $response;
            })->addColumn('date_time', function ($data) {
                $response = "";
                if(isset($data->jobs[0]->date_time))
                    $response =Carbon::parse( $data->jobs[0]->date_time)->format('jS M Y H:i');
                return $response;
            })->addColumn('score', function ($data) {
                $response = "";
                if($data->responseGroups)
                    $response =$data->responseGroups->sum('score');
                return $response;
            })->addColumn('status', function ($data)  use ($fl){
                $response = "";
                if($fl=='approved')
                {
                    $response = 'completed';
                }
                else if($fl=='rejected')
                {
                    $response = 'failed';
                }
                else if($fl=='not_required')
                {

                    $response = 'completed';
                }
                else if($fl=='all')
                {
                    if($data->sign_off==1)
                        $response = 'completed';
                    if($data->sign_off==-1)
                        $response = 'failed';
                    if($data->forms->req_sign_off==1)
                        $response = 'completed';
                    if($data->sign_off==null)
                        $response = 'waitin approval';
                }
                else
                {
                    $response = 'waitin approval';
                }


                return $response;
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                $view = '<a type="button" class="btn btn-primary btn-xs my-0 waves-effect waves-light" href="'.url("/completed_forms/viewDocument").'/'.$data->id.'"><i class="fas fa-eye"></i></a>';
                $response = $response . $view;
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);

    }


    public function getAllFilterReport($id=null,$fl)
    {

        if($id==null || $id=='null' )
        {
            $data=JobGroup::with('users','forms','locations','cJobs','locationBreakdowns','responseGroups')->where('active',1)->where('deleted',0)->where('company_id',Auth::user()->default_company)->get();
        }
        else
        {
            $data = FormType::with('jobGroup.users','jobGroup.forms','jobGroup.locations','jobGroup.nJobs','jobGroup.locationBreakdowns')->get()->where('company_id',Auth::user()->default_company)->find($id);

            $data = $data->JobGroup;
        }


        foreach ($data as $key => $value) {
            if($value->cJobs->isEmpty())
            {
                unset($data[$key]);
            }
            if($fl=='approved')
            {
                if($value->sign_off==-1 || $value->sign_off==null )
                    unset($data[$key]);
            }
            else if($fl=='rejected')
            {
                if($value->sign_off==1 || $value->sign_off==null)
                    unset($data[$key]);
            }
            else if($fl=='not_required')
            {
                if($value->forms->req_sign_off==1 || $value->sign_off==null)
                    unset($data[$key]);
            }

        }

        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {

            })
            ->addColumn('employee_name', function ($data) {
                $response = "";
                $response = $data->users->first_name ?? "";
                return $response;
            }) ->addColumn('location_name', function ($data) {
                $response = "";

                $response = $data->locations->name ?? "";
                return $response;
            }) ->addColumn('location_breakdown', function ($data) {
                $response = "";

                $response = $data->locationBreakdowns->name ?? "";
                return $response;
            })->addColumn('form_name', function ($data) {
                $response = "";

                $response = $data->forms->name ?? "";
                return $response;
            })->addColumn('date_time', function ($data) {
                $response = "";
                if(isset($data->jobs[0]->date_time))
                    $response =Carbon::parse( $data->jobs[0]->date_time)->format('jS M Y H:i');
                return $response;
            })->addColumn('score', function ($data) {
                $response = "";
                if($data->responseGroups)
                    $response =$data->responseGroups->sum('score');
                return $response;
            })->addColumn('status', function ($data)  use ($fl){
                $response = "";
                if($fl=='approved')
                {
                    $response = 'completed';
                }
                else if($fl=='rejected')
                {
                    $response = 'failed';
                }
                else if($fl=='not_required')
                {

                    $response = 'completed';
                }
                else if($fl=='all')
                {
                    if($data->sign_off==1)
                        $response = 'completed';
                    if($data->sign_off==-1)
                        $response = 'failed';
                    if($data->sign_off==null)
                        $response = 'waitin approval';
                }
                else
                {
                    $response = 'waitin approval';
                }


                return $response;
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                $view = '<a type="button" class="btn btn-primary btn-xs my-0 waves-effect waves-light" href="'.url("/completed_forms/viewDocument").'/'.$data->id.'"><i class="fas fa-eye"></i></a>';
                $response = $response . $view;
                if(!$data->sign_off)
                {

                    $location_id =null;
                    $location_break =null;
                    if($data->locations)
                        $location_id=$data->locations->id;
                    if($data->locationBreakdowns)
                        $location_break=$data->locationBreakdowns->id;

                    $off = '<a  data-toggle="modal" data-target="#modelSignOff" class="btn btn-success all_action_btn_margin btn-xs my-0 waves-effect waves-light" onclick="selectedJob('.$data->id.','.$location_id.','.$location_break.')" ><i class="fas fa-pen-nib"></i></a>';
                    $response = $response . $off;
                }

                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);

    }


    public function exportVersionPdf_view($id)
    {
        $job_group = JobGroup::with('users','forms','locations','locationBreakdowns')->find($id);
        $jobs = Jobs::with('form_Section.questions.answer_type','form_Section.questions.answer_group.answer','form_Section.form_version.version')->where('job_group_id',$job_group->id)->orderBy('order', 'asc')->get();
        $ids = $jobs->pluck('id');
        $response_groups = ResponseGroups::with('responses')->where('job_group_id',$job_group->id)->where('form_id',$job_group->form_id)->get();

        $form_id = JobGroup::find($id)->form_id;
        $form_sections = $jobs->pluck('form_section_id');
        $form_sections = array_values($form_sections->toArray());
        $form_section = FormSections::whereIn('id',$form_sections)->orderBy('order', 'asc')->with('questions.answer_type','questions.answer_group.answer')->get();
        $response_groups = Response::whereIn('job_id',$jobs->pluck('id'))->whereIn('form_section_id',$form_sections)->where('active',1)->where('deleted',0)->get();

        $date=Carbon::now()->format('D jS M Y');
        $companies=Company::where('active',1)->where('deleted',0)->get();
        return view('backend.complete_form.model.print_layout')->with(['companies'=>$companies,'dynamic_data'=>$form_section,'job_group'=>$job_group,'jobs'=>$jobs,'ids'=>$ids]);
    }

}

