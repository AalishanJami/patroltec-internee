<?php
namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\AnswerGroups;
use App\Questions;
use PDF;
use Maatwebsite\Excel\Facades\Excel;
use Validator;
use DataTables;
use Auth;
use Config;
use Session;
use App\Exports\answer_group\AnswerGroupExport;

class AnswerGroupController extends Controller
{
    private $page_name;
    private $tab_name;
    private $db;
    private $cms_array=[];
    public function __construct() {
        $this->page_name ='answer_group';
        $this->tab_name ='document_library';
        $this->db='answer_groups';
        $cms_data=localization();
        $temp_array=[];
        $temp_array['name']['key']='name';
        $temp_array['name']['value']=checkKey('name',$cms_data);
        $temp_array['sig_select']['key']='sig_select';
        $temp_array['sig_select']['value']=checkKey('sig_select',$cms_data);
        $this->cms_array=$temp_array;
    }
    public function index()
    {
        return view('backend.answer.index');
    }
     public function answer()
    {
        return view('backend.answer.answer');
    }
    public function getAll()
    {
        $data=AnswerGroups::where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        return Datatables::of($data)
        ->addColumn('checkbox', function ($data) {})
            ->addColumn('sig_select', function ($data) { return $data->sig_select;})
            ->addColumn('submit', function ($data) {})
        ->addColumn('actions', function ($data) {
            $response = "";
            if(Auth()->user()->hasPermissionTo('delete_document_answer_group') || Auth::user()->all_companies == 1 )
            {
                $delete='';
                if($data->flag == 0)
                {
                    $delete= '<span class="remove_answer_main delete_answergrp'.$data->id.'" id="'. $data->id .'"><a type="button" class="btn btn-danger btn-xs my-0"><i class="fas fa-trash"></i></a> </span>';
                }
                $response = $response . $delete;
            }
            if(Auth()->user()->hasPermissionTo('edit_document_answer_group') || Auth::user()->all_companies == 1 )
            {
                $edit = '<span><a onclick="get_group_answer_id(' . $data->id .')" type="button" class="btn btn-primary btn-xs mr-2"><i class="fas fa-pencil-alt"></i></a> </span>';
                $response = $response . $edit;
            }


            return $response;
        })
        ->rawColumns(['actions'])
        ->make(true);
    }
    public function get($id)
    {
        $data=AnswerGroups::where('active',1)->where('deleted',0)->get();
        return response()->json(['data'=>$data,'flag'=>1]);

    }
    public function getAllSoft()
    {
        $data=AnswerGroups::where('active',0)->where('deleted',0)->orderBy('id', 'desc')->get();
        return Datatables::of($data)
        ->addColumn('checkbox', function ($data) {})
        ->addColumn('submit', function ($data) {})
        ->addColumn('sig_select', function ($data) { return $data->sig_select;})
        ->addColumn('actions', function ($data) {
            $response = "";
            if(Auth()->user()->hasPermissionTo('restore_delete_document_answer_group') || Auth::user()->all_companies == 1 )
            {
                $restore = '<span><a type="button" class="btn btn-success btn-xs my-0 waves-effect waves-light" onclick="restoreanswer_groupdata('.$data->id.')"><i class="fa fa-retweet" aria-hidden="true"></i></a> </span>';
                $response = $response . $restore;
            }
            if(Auth()->user()->hasPermissionTo('hard_delete_document_answer_group') || Auth::user()->all_companies == 1 )
            {
                $delete= '<span><a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light" onclick="deletesoftanswer_groupdata('.$data->id.')"><i class="fas fa-trash"></i></a></span>';
                $response = $response . $delete;
            }
            return $response;
        })
        ->rawColumns(['actions'])
        ->make(true);
    }
    public function store(Request $request)
    {
        $data=AnswerGroups::create([
            'company_id' => $request->company_id
        ]);
    audit_log($this->cms_array,'',Config::get('constants.store'),$this->page_name,$this->tab_name,$data->id,'',$data->id,Session::get('form_id'),$this->db);
        return response()->json(['data'=>$data]);
    }
    public function update(Request $request)
    {
        $data = [
            'company_id'=>'required|numeric',
            'name'=>'required',
        ];
        $validator = Validator::make($request->all(), $data);
        if ($validator->fails())
        {
            $error = $validator->messages();
            $html='<ul>';
            foreach ($error->all() as $key => $value) {
                $html=$html.'<li>'.$value.'</li>';
            }
            $html=$html.'</ul>';
            return response()->json(['status'=> false, 'error'=> $html]);
        }
        $data=$request->all();
        // dd($data);
        if (!isset($data['sig_select'])){
            $data['sig_select']=0;
        }else{
            $data['sig_select']=1;
        }
        $answer_group=AnswerGroups::where('id',$data['id']);
        $answer_group_first=$answer_group->first();
        $answer_group->update($data);
        if($answer_group_first->active == 0)
        {
            $flag=0;
        }
        else
        {
            $flag=1;
        }
        $answer_group_first=$answer_group_first->toArray();
        unset($answer_group_first['question']);
        audit_log($this->cms_array,audit_log_data($data),Config::get('constants.update'),$this->page_name,$this->tab_name,$answer_group_first["id"],audit_log_data($answer_group_first),$answer_group_first["id"],Session::get('form_id'),$this->db);
        return response()->json(['message'=>'Successfully Added !!','flag'=>$flag]);

    }
    public function softDelete (Request $request)
    {
        AnswerGroups::where('id',$request->id)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function delete(Request $request)
    {
        $questions = Questions::where('answer_groups_id', $request->id)->get();
    audit_log($this->cms_array,'',Config::get('constants.delete'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('form_id'),$this->db);

        if(count($questions) > 0){
            return response()->json(['message'=>'Answer Already in use','flag'=>2]);
        } else {
            AnswerGroups::where('id',$request->id)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
        }
    }
    public function active(Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.restore'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('form_id'),$this->db);
        AnswerGroups::where('id',$request->id)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>1]);
    }
    public function activeMultiple(Request $request)
    {
        AnswerGroups::whereIn('id',$request->ids)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>1]);
    }
     public function deleteMultipleSOft(Request $request)
    {
        AnswerGroups::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function deleteMultiple(Request $request)
    {
        foreach ($request->ids as $key => $value) {
           $questions = Questions::where('answer_groups_id', $value)->get();
            if(count($questions) > 0){
                return response()->json(['message'=>'Answer Already in use','flag'=>2]);
            }
        }
        AnswerGroups::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function exportExcel(Request $request)
    {
        if($request->excel_array == 1)
        {
            $data= AnswerGroups::where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->excel_array);
            $data=AnswerGroups::whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        return Excel::download(new AnswerGroupExport($data), 'answerGroup.csv');
    }
    public function exportPdf(Request $request)
    {
        if($request->pdf_array == 1)
        {
            $data= AnswerGroups::where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->pdf_array);
            $data=AnswerGroups::whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        $companies='Patroltec';
        $reportname='Answer Group';
        $date=\Carbon\Carbon::now()->format('D jS M Y');
        $pdf = PDF::loadView('backend.document.answer_group.pdf',compact('companies','reportname','data','date'));
        return $pdf->download('answerGroup.pdf');
    }
    public function exportWorld(Request $request)
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        if($request->word_array == 1)
        {
            $data= AnswerGroups::where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->word_array);
            $data=AnswerGroups::whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        $table = $section->addTable();
        foreach ($data as $key => $value) {
            if($key==0)
            {
                if(Auth()->user()->hasPermissionTo('document_answer_group_name_word_export') || Auth::user()->all_companies == 1 )
                {
                    $table->addRow();
                    $table->addCell(1750)->addText('Name');
                }

            }
            if(Auth()->user()->hasPermissionTo('document_answer_group_name_word_export') || Auth::user()->all_companies == 1 )
            {
                $table->addRow();
                $table->addCell(1750)->addText($value->name);
            }

        }
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('answerGroup.docx');
        return response()->download(public_path('answerGroup.docx'));
    }

}
