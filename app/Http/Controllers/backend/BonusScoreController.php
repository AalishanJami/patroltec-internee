<?php
namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Company;
use App\Exports\bonus\BonusExport;
use App\Exports\bonus\BonusExportCustom;
use App\Exports\bonus\BonusSoftExport;
use App\BonusScore;
use PDF;
use Session;
use DataTables;
use Maatwebsite\Excel\Facades\Excel;
use Validator;
use Config;
use Auth;
class BonusScoreController extends Controller
{
    private $page_name;
    private $tab_name;
    private $db;
    private $cms_array=[];
    public function __construct() {
        $this->page_name ='bonus_score';
        $this->tab_name ='project';
        $this->db='bonus_scores';
        $cms_data=localization();
        $temp_array=[];
        $temp_array['month']['key']='month';
        $temp_array['month']['value']=checkKey('month',$cms_data);
        $temp_array['year']['key']='year';
        $temp_array['year']['value']=checkKey('year',$cms_data);
        $temp_array['bonus_score']['key']='score';
        $temp_array['bonus_score']['value']=checkKey('score',$cms_data);
        $temp_array['notes']['key']='notes';
        $temp_array['notes']['value']=checkKey('notes',$cms_data);
        $this->cms_array=$temp_array;
    }
    public function index()
    {
        $companies=Company::getCompanies();
        return view('backend.bonus_score.index')->with(['companies'=>$companies]);
    }
    public function getAll()
    {
        $data=BonusScore::where('company_id',Auth::user()->default_company)->where('location_id',Session::get('project_id'))->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {
            })
            ->addColumn('submit', function ($data) {
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                if(Auth()->user()->hasPermissionTo('delete_bonusScore') || Auth::user()->all_companies == 1 ){
                    $delete = '<a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light delete-btn' . $data->id . '" onclick="deletebonus_scoredata( ' . $data->id . ')"><i class="fas fa-trash"></i></a>';
                }else{
                    $delete = '';
                }
                $response = $response . $delete;
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);

        // return response()->json(['data'=>$data,'flag'=>1]);
    }
    public function getAllSoft()
    {
        $data=BonusScore::where('company_id',Auth::user()->default_company)->where('location_id',Session::get('project_id'))->where('active',0)->where('deleted',0)->orderBy('id', 'desc')->get();
        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {
            })
            ->addColumn('submit', function ($data) {
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                if(Auth()->user()->hasPermissionTo('hard_delete_bonusScore') || Auth::user()->all_companies == 1 )
                {
                    $delete = '<a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light" onclick="deletesoftbonus_scoredata('.$data->id.')"><i class="fas fa-trash"></i></a>';
                    $response = $response . $delete;
                }
                if(Auth()->user()->hasPermissionTo('restore_delete_bonusScore') || Auth::user()->all_companies == 1 )
                {
                    $restore = '<a type="button" class="btn btn-success btn-xs my-0 all_action_btn_margin waves-effect waves-light" onclick="restorebonus_scoredata('.$data->id.')"><i class="fa fa-retweet" aria-hidden="true"></i></a>';
                    $response = $response . $restore;
                }
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
    public function store(Request $request)
    {
        if (isset($request->bonus_score)){
            $data = [
                'bonus_score'=>'numeric',
                'year'=>'numeric|digits:4',
            ];
            $validator = Validator::make($request->all(), $data);
            if ($validator->fails())
            {
                $error = $validator->messages();
                $html='<ul>';
                foreach ($error->all() as $key => $value) {
                    $html=$html.'<li>'.$value.'</li>';
                }
                $html=$html.'</ul>';
                return response()->json(['status'=> false, 'error'=> $html]);
            }
        }
        $data=BonusScore::create(['location_id'=>Session::get('project_id')]);
    audit_log($this->cms_array,'',Config::get('constants.store'),$this->page_name,$this->tab_name,$data->id,'',$data->id,Session::get('project_id'),$this->db);
        return response()->json(['data'=>$data]);
    }
    public function update(Request $request)
    {
        $data= [
            'bonus_score'=>'required|numeric',
            'year'=>'required|digits:4',
            'company_id'=>'required|numeric',
        ];
        $validator = Validator::make($request->all(), $data);
        if ($validator->fails())
        {
            $error = $validator->messages();
            $html='<ul>';
            foreach ($error->all() as $key => $value) {
                $html=$html.'<li>'.$value.'</li>';
            }
            $html=$html.'</ul>';
            return response()->json(['status'=> false, 'error'=> $html]);
        }
        $data=$request->all();
        $bonus_score=BonusScore::find($data['id']);
        $bonus=$bonus_score->first();
        audit_log($this->cms_array,audit_log_data($data),Config::get('constants.update'),$this->page_name,$this->tab_name,$bonus->id,audit_log_data($bonus->toArray()),$bonus->id,Session::get('project_id'),$this->db);
        $bonus_score->update($data);
        if($bonus->active == 0)
        {
            $flag=0;
        }
        else
        {
            $flag=1;
        }
        return response()->json(['message'=>'Successfully Added !!','flag'=>$flag]);

    }
    public function softDelete (Request $request)
    {
   audit_log($this->cms_array,'',Config::get('constants.soft_delete'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('project_id'),$this->db);
        BonusScore::where('id',$request->id)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function delete(Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.delete'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('project_id'),$this->db);
        BonusScore::where('id',$request->id)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function active(Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.restore'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('project_id'),$this->db);
        BonusScore::where('id',$request->id)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
    public function activeMultiple(Request $request)
    {
        BonusScore::whereIn('id',$request->ids)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
    public function deleteMultipleSOft(Request $request)
    {
        BonusScore::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function deleteMultiple(Request $request)
    {
        BonusScore::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function exportPdf(Request $request)
    {
        if($request->pdf_array == 1)
        {
            $data= BonusScore::where('location_id',Session::get('project_id'))->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->with('companyBonusScor')->get();
        }
        else
        {
            $array=json_decode($request->pdf_array);
            $data=BonusScore::where('location_id',Session::get('project_id'))->whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->with('companyBonusScor')->get();
        }
        $companies='Patroltec';
        $reportname='Bonus Score';
        $date=\Carbon\Carbon::now()->format('D jS M Y');
        $pdf = PDF::loadView('backend.bonus_score.pdf',compact('companies','reportname','data','date'));
        return $pdf->download('bonus.pdf');
    }

    public function exportWorld(Request $request)
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        if($request->word_array == 1)
        {
            $data= BonusScore::where('location_id',Session::get('project_id'))->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->word_array);
            $data=BonusScore::where('location_id',Session::get('project_id'))->whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        $table = $section->addTable();
        foreach ($data as $key => $value) {
            if($key==0)
            {
                $table->addRow();
                if(Auth()->user()->hasPermissionTo('bonusScore_month_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Month');
                }
                if(Auth()->user()->hasPermissionTo('bonusScore_year_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Year');
                }
                if(Auth()->user()->hasPermissionTo('bonusScore_score_word_export') || Auth::user()->all_companies == 1 ) {
                    $table->addCell(1750)->addText('Bonus Score');
                }
                if(Auth()->user()->hasPermissionTo('bonusScore_notes_word_export') || Auth::user()->all_companies == 1 ) {
                    $table->addCell(1750)->addText('Notes');
                }
            }
            $table->addRow();

            if(Auth()->user()->hasPermissionTo('bonusScore_month_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText($value->month);
            }
            if(Auth()->user()->hasPermissionTo('bonusScore_year_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText($value->year);
            }
            if(Auth()->user()->hasPermissionTo('bonusScore_score_word_export') || Auth::user()->all_companies == 1 ) {
                $table->addCell(1750)->addText($value->bonus_score);
            }
            if(Auth()->user()->hasPermissionTo('bonusScore_notes_word_export') || Auth::user()->all_companies == 1 ) {
                $table->addCell(1750)->addText($value->notes);
            }

        }
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('bonus.docx');
        return response()->download(public_path('bonus.docx'));
    }
    public function exportExcel(Request $request)
    {
        //for active user
        if($request->excel_array == 1)
        {
            $data=BonusScore::where('location_id',Session::get('project_id'))->where('active',1)->where('deleted',0)->get([
                'month','year','bonus_score','notes',
            ]);
            return Excel::download(new BonusExportCustom($data), 'bonus.csv');
        }
        else
        {
            $array=json_decode($request->excel_array);
            $data=BonusScore::where('location_id',Session::get('project_id'))->whereIn('id',$array)->where('active',1)->where('deleted',0)->get([
                'month','year','bonus_score','notes',
            ]);
            return Excel::download(new BonusExportCustom($data), 'bonus.csv');
        }
    }
    public function detail()
    {
        return view('backend.project.details');
    }
    public function bonusScore()
    {
        return view('backend.bonus_score.index_doc');
    }
}
