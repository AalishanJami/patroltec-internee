<?php
namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Position;
use App\PositionPermision;
use App\StaffPermission;
use App\RolePermission;
use App\CustomRole;
use App\Company;
use Config;
use Session;
use Auth;
class StaffController extends Controller
{
    private $page_name;
    private $tab_name;
    private $db;
    private $cms_array=[];
    public function __construct() {
        $this->page_name ='staff';
        $this->tab_name ='employees';
        // $this->db='folder_permisions';
        $cms_data=localization();
        $temp_array=[];
        $temp_array['user_id']['key']='user';
        $temp_array['user_id']['value']=checkKey('user',$cms_data);
        $temp_array['permissions']['key']='permissions';
        $temp_array['permissions']['value']=checkKey('permissions',$cms_data);
        $this->cms_array=$temp_array;
    }
    public function index()
    {
        $companies=Company::getCompanies();
        $positions=Position::getPosition()->get();
        $positions=sortData($positions);
        $users_array=[];
        foreach ($positions as $key => $value) {
            if($value['enable'] == 1)
            {
                $users_array[$key]=$value['id'];
            }
        }
        $users=User::getUser()->whereIn('users.position_id',$users_array)->get();
        $users=sortData($users);
        $roles_array=[];
        $count=0;
        foreach ($users as $key => $value) {
            foreach ($value->roles as $role) {
                $roles_array[$count]=$role->id;
                $count++;
            }
        }
        $roles=CustomRole::getRole()->whereIn('id',$roles_array)->get();
        $roles=sortData($roles);
        return view('backend.staff.index')->with(['users'=>$users,'companies'=>$companies,'roles'=>$roles,'positions'=>$positions]);
    }
    public function staffUpdate(Request $request)
    {
        $old_value=dataPermision('StaffPermission','','staff');
        $new_value=dataPermision('users',$request->user);
        if($request->user_id)
        {
            StaffPermission::where('employee_id',Session::get('employee_id'))->whereIn('user_id',$request->user_id)->delete();
        }
        if(isset($request->user))
        {
            foreach ($request->user as $key => $value) {
                StaffPermission::create(['user_id'=>$value,'employee_id'=>Session::get('employee_id'),'company_id'=>Auth::user()->default_company]);
            }
        }
        audit_log($this->cms_array,audit_log_data($new_value),Config::get('constants.update'),$this->page_name,$this->tab_name,Session::get('employee_id'),audit_log_data($old_value),Session::get('employee_id'),Session::get('employee_id'),'staff_permisions');
        toastr()->success('Staff Permision Successfully Updated!');
        return redirect('/staff');
    }
    public function staffGroupUpdate(Request $request)
    {
        $old_value=dataPermision('PositionPermision','','position');
        $new_value=dataPermision('positions',$request->user_group);
        if($request->position_id)
        {
            PositionPermision::where('user_id',Session::get('employee_id'))->whereIn('position_id',$request->position_id)->delete();
        }
        if(isset($request->user_group))
        {
            foreach ($request->user_group as $key => $value) {
                PositionPermision::create(['position_id'=>$value,'user_id'=>Session::get('employee_id'),'company_id'=>Auth::user()->default_company]);
            }
        }
        audit_log($this->cms_array,audit_log_data($new_value),Config::get('constants.update'),$this->page_name,$this->tab_name,Session::get('employee_id'),audit_log_data($old_value),Session::get('employee_id'),Session::get('employee_id'),'position_permisions');
        toastr()->success('Position Permision Successfully Updated!');
        return redirect('/staff');
    }
    public function roleUpdate(Request $request)
    {
        $old_value=dataPermision('RolePermission','','role');
        $new_value=dataPermision('roles',$request->role);
        if($request->role_id)
        {
            RolePermission::where('employee_id',Session::get('employee_id'))->whereIn('role_id',$request->role_id)->delete();
        }
        if(isset($request->role))
        {
            foreach ($request->role as $key => $value) {
                RolePermission::create(['role_id'=>$value,'employee_id'=>Session::get('employee_id'),'company_id'=>Auth::user()->default_company]);
            }
        }
        audit_log($this->cms_array,audit_log_data($new_value),Config::get('constants.update'),$this->page_name,$this->tab_name,Session::get('employee_id'),audit_log_data($old_value),Session::get('employee_id'),Session::get('employee_id'),'role_permisions');
        toastr()->success('Staff role Permision Successfully Updated!');
        return redirect('/staff');
    }

    public function group()
    {
        return view('backend.staff.group');
    }

}
