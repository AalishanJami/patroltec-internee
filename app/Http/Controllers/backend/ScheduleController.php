<?php
namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\TaskType;
use App\LocationBreakdown;
use App\Company;
use App\Location;
use App\ScheduleTask;
use Auth;
use App\ScheduleTaskLocation;
use DataTables;
use Validator;
use Config;
use App\Forms;
use App\Exports\schedule\ScheduleExport;
use App\Exports\schedule\ScheduleSoftExport;
use PDF;
use Session;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;
use App\Jobs;
use App\JobGroup;
use App\User;
use App\FormVersion;
class ScheduleController extends Controller
{
    private $page_name;
    private $tab_name;
    private $db;
    private $cms_array=[];

    public function __construct() {
        $this->page_name ='schedule';
        $this->tab_name ='document_library';
        $this->db='schedule_tasks';
        $cms_data=localization();

        $temp_array=[];
        $temp_array['task_type_id']['key']='task_type';
        $temp_array['task_type_id']['value']=checkKey('task_type',$cms_data);

        $temp_array['location_id']['key']='project_name';
        $temp_array['location_id']['value']=checkKey('project_name',$cms_data);

        $temp_array['user_id']['key']='user_name';
        $temp_array['user_id']['value']=checkKey('user_name',$cms_data);

        $temp_array['start_date']['key']='start_date';
        $temp_array['start_date']['value']=checkKey('start_date',$cms_data);

        $temp_array['end_date']['key']='expiry_date';
        $temp_array['end_date']['value']=checkKey('expiry_date',$cms_data);

        $temp_array['pre_time_mins']['key']='pre';
        $temp_array['pre_time_mins']['value']=checkKey('pre',$cms_data);

        $temp_array['pre_time_select']['key']='time';
        $temp_array['pre_time_select']['value']=checkKey('time',$cms_data);

        $temp_array['post_time_mins']['key']='post';
        $temp_array['post_time_mins']['value']=checkKey('post',$cms_data);

        $temp_array['post_time_select']['key']='time';
        $temp_array['post_time_select']['value']=checkKey('time',$cms_data);

        $temp_array['recycle_jobs_after']['key']='recycle_all_jobs_after';
        $temp_array['recycle_jobs_after']['value']=checkKey('recycle_all_jobs_after',$cms_data);

        $temp_array['do_not_schedule']['key']='do_not_schedule';
        $temp_array['do_not_schedule']['value']=checkKey('do_not_schedule',$cms_data);

        $temp_array['mon']['key']='mon';
        $temp_array['mon']['value']=checkKey('mon',$cms_data);

        $temp_array['tue']['key']='tue';
        $temp_array['tue']['value']=checkKey('tue',$cms_data);

        $temp_array['wed']['key']='wed';
        $temp_array['wed']['value']=checkKey('wed',$cms_data);

        $temp_array['thu']['key']='thu';
        $temp_array['thu']['value']=checkKey('thu',$cms_data);

        $temp_array['fri']['key']='fri';
        $temp_array['fri']['value']=checkKey('fri',$cms_data);

        $temp_array['sat']['key']='sat';
        $temp_array['sat']['value']=checkKey('sat',$cms_data);

        $temp_array['sun']['key']='sun';
        $temp_array['sun']['value']=checkKey('sun',$cms_data);

        $temp_array['frequency_limit']['key']='frequency_limit';
        $temp_array['frequency_limit']['value']=checkKey('frequency_limit',$cms_data);

        $temp_array['frequency_type']['key']='';
        $temp_array['frequency_type']['value']='';

        $temp_array['max_display']['key']='max_display';
        $temp_array['max_display']['value']=checkKey('max_display',$cms_data);

        $temp_array['repeat']['key']='frequency_repeat';
        $temp_array['repeat']['value']=checkKey('frequency_repeat',$cms_data);

        $temp_array['repeat_min_gap']['key']='gap_mins';
        $temp_array['repeat_min_gap']['value']=checkKey('gap_mins',$cms_data);

        $temp_array['tag_swipe']['key']='tag_swipe_required';
        $temp_array['tag_swipe']['value']=checkKey('tag_swipe_required',$cms_data);

        $this->cms_array=$temp_array;

    }
    public function index()
    {
        $companies=Company::getCompanies();
        $locations=Location::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        $task_type=TaskType::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        $users = User::where('company_id',Auth::user()->default_company)->get()->pluck('first_name','id');
        if (session::has('form_id')){
            $form_type = Forms::find(Session::get('form_id'))->location_type;
        }else{
            return redirect('form_group');
        }
        return view('backend.schedule.index')->with(['form_type'=>$form_type,'companies'=>$companies,'task_type'=>$task_type,'locations'=>$locations,'users'=>$users]);
    }

    public function tasktype()
    {
        $task_type=TaskType::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        return response()->json(['task_type'=>$task_type]);
    }
    public function getProject($id,$form_id=null)
    {
        if($form_id==null)
        {
            $form_type = Forms::find(Session::get('form_id'))->location_type;
        }
        else
        {
            $form_type = Forms::find($form_id)->location_type;
        }
        if($form_type =="m")
        {
            $location=LocationBreakdown::where('location_id',$id)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $location=LocationBreakdown::where('location_id',$id)->where('type',$form_type)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        return response()->json(['location'=>$location,'form_type'=>$form_type]);
    }
    public function makeSummery($data){
        $day='';
        if($data->mon=='1' && $data->tue=='1' && $data->wed=='1' && $data->thu=='1' && $data->fri=='1' && $data->sat=='1' && $data->sun=='1'){
            $day.='Every Day,';
        } else {
            $days='';
            $duplicated = array();
            $day_value=[$data->mon,$data->tue,$data->wed,$data->thu,$data->fri,$data->sat,$data->sun,$data->bank];
            $day_name=['Mon','Tue','wed','Thu','Fri','Sat','Sun','Bank'];
            foreach($day_value as $key=>$value){
                if (!empty($value)){
                    if ($value=='1'){
                        $days.=$day_name[$key].' ,';
                        $duplicated[] = $day_name[$key];
                    }
                }
            }
            $day.=substr_replace($days, ".", -1);
        }

        if(!empty($data->repeat)) {
            if ($data->repeat>1){
                $time='';
                if(!empty($data->repeat_min_gap)) {
                    if ($data->repeat_min_gap>1){
                        $time=$data->repeat_min_gap." minutes.";
                    }else{
                        $time=$data->repeat_min_gap." minute.";
                    }
                }
                $day.=" Repeat ".$data->repeat." times every ".$time;
            }
        }

        if(!empty($data->week_limit) && $data->week_limit!==0)
        {
            $day.=" Limit ".$data->week_limit;
            if(!empty($data->max_display)) {
                if ($data->max_display>1) {
                    $day .= " per week with  max display of  " . $data->max_display . " forms.";
                }else{
                    $day .= " per week with  max display of  " . $data->max_display . " form.";
                }
            }
        }else if(!empty($data->month_limit) && $data->month_limit!==0)
        {
            $day.=" Limit ".$data->month_limit;
            if(!empty($data->max_display)) {
                if ($data->max_display>1) {
                    $day .= " per month with  max display of  " . $data->max_display . " forms.";
                }else{
                    $day .= " per month with  max display of  " . $data->max_display . " form.";
                }
            }
        } else  if(!empty($data->year_limit) && $data->year_limit!==0)
        {
            $day.=" Limit ".$data->year_limit;
            if(!empty($data->max_display)) {
                if ($data->max_display>1) {
                    $day .= " per year with  max display of  " . $data->max_display . " forms.";
                }else{
                    $day .= " per year with  max display of  " . $data->max_display . " form.";
                }
            }
        }

        return $day;
    }
    public function getAll()
    {
        $form_id=Session::get('form_id');
        $data=ScheduleTask::where('form_id',$form_id)->with('scheduleTaskLocation.company')->with('scheduleTaskLocation.location')->with('scheduleTaskLocation.location_breakdowns')->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        return Datatables::of($data)
            ->addColumn('project_name', function ($data) {
                $name='';
                if(isset($data->scheduleTaskLocation->location->name))
                {
                    $name =$data->scheduleTaskLocation->location->name;
                }
                return $name  =$data->scheduleTaskLocation->location->name ?? '';
            })
            ->addColumn('summery', function ($data) {
                return $this->makeSummery($data);
            })
            ->addColumn('checkbox', function ($data) {
                $checkbox="";
                if(Auth()->user()->hasPermissionTo('schedule_checkbox') || Auth::user()->all_companies == 1 ){
                    $checkbox=$checkbox.'<div class="form-check"><input type="checkbox" class="form-check-input selectedrowschedule"  id="schedule' . $data->id . '"><label class="form-check-label" for="schedule' . $data->id . '" "=""></label></div>';
                }
                return $checkbox;
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                if(Auth()->user()->hasPermissionTo('edit_schedule') || Auth::user()->all_companies == 1 ){
                    $edit = '<a onClick="getscheduledata( ' . $data->id . ')" id="' . $data->id . '" class="btn btn-primary btn-xs" href=""  data-toggle="modal" data-target="#modalEditForm"><i class="fas fa-pencil-alt"></i></a>';
                }else{
                    $edit = '';
                }
                if(Auth()->user()->hasPermissionTo('delete_schedule') || Auth::user()->all_companies == 1 ){
                    $delete = '<a href="javascript:;" type="button"class="btn all_action_btn_margin btn-danger btn-xs my-0" onclick="deletescheduledata( ' . $data->id . ')"><i class="fas fa-trash"></i></a>';
                }else{
                    $delete = '';
                }
                $response = $response . $edit;
                $response = $response . $delete;
                return $response;
            })
            ->rawColumns(['actions','checkbox'])
            ->make(true);
    }
    public function edit(Request $request)
    {
        $data=ScheduleTask::where('id',$request->id)->with('scheduleTaskLocation.company')->with('scheduleTaskLocation.location')->with('scheduleTaskLocation.location_breakdowns')->where('active',1)->where('deleted',0)->first();
        $companies=Company::getCompanies();
        $locations=Location::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        $task_type=TaskType::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        $form_type = Forms::find(Session::get('form_id'))->location_type;
        $location_id='';
        if(isset($data->scheduleTaskLocation->location_id))
        {
            $location_id=$data->scheduleTaskLocation->location_id;
        }
        $location_breakdowns=LocationBreakdown::where('location_id',$location_id)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        $scheduletasklocation=ScheduleTaskLocation::where('schedule_task_id',$request->id)->orderBy('id', 'desc')->get();
        $start_date=Carbon::parse($data->start_date);
        return response()->json(['data'=>$data,'start_date'=>$start_date,'scheduletasklocation'=>$scheduletasklocation,'companies'=>$companies,'task_type'=>$task_type,'locations'=>$locations,'location_breakdowns'=>$location_breakdowns,'form_type'=>$form_type]);
    }
    public function getAllSoft()
    {
        $form_id=Session::get('form_id');
        $data=ScheduleTask::where('form_id',$form_id)->with('scheduleTaskLocation.company')->with('scheduleTaskLocation.location')->with('scheduleTaskLocation.location_breakdowns')->where('active',0)->where('deleted',0)->orderBy('id', 'desc')->get();
        return Datatables::of($data)
            ->addColumn('project_name', function ($data) {
                $name='';
                if(isset($data->scheduleTaskLocation->location->name))
                {
                    $name =$data->scheduleTaskLocation->location->name;
                }
                return $name;
            })
            ->addColumn('summery', function ($data) {
                return $this->makeSummery($data);
            })
            ->addColumn('checkbox', function ($data) {
                $checkbox="";
                if(Auth()->user()->hasPermissionTo('schedule_checkbox') || Auth::user()->all_companies == 1 ){
                    $checkbox=$checkbox.'<div class="form-check"><input type="checkbox" class="form-check-input selectedrowschedule" id="schedule' . $data->id . '"><label class="form-check-label" for="schedule' . $data->id . '" "=""></label></div>';
                }
                return $checkbox;
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                if(Auth()->user()->hasPermissionTo('restore_delete_schedule') || Auth::user()->all_companies == 1 ){
                    $Restore_button = '<a type="button"class="btn all_action_btn_margin btn-success btn-xs my-0" onclick="restorescheduledata( ' . $data->id . ')"><i class="fa fa-retweet" aria-hidden="true"></i></a>';
                }else{
                    $Restore_button = '';
                }

                if(Auth()->user()->hasPermissionTo('hard_delete_schedule') || Auth::user()->all_companies == 1 ){
                    $hard_delete_button = '<a type="button"class="btn btn-danger btn-xs my-0" onclick="deletesoftscheduledata( ' . $data->id . ')"><i class="fa fa-trash" aria-hidden="true"></i></a>';
                }else{
                    $hard_delete_button = '';
                }
                $response = $response . $hard_delete_button;
                $response = $response . $Restore_button;
                return $response;
            })
            ->rawColumns(['actions','checkbox'])
            ->make(true);
    }

    public function store(Request $request)
    {
        if ($request->form_type_create!=="m"){
            $data= [
                'company_id'=>'required|numeric',
                'location_id'=>'required|numeric',
                'location_breakdown_id'=>'required',
                'start_date'=>'required',
                'time'=>'required',
                'end_date'=>'required',
                'pre_time_mins'=>'required',
                'post_time_mins'=>'required',
            ];
        }else{
            $data= [
                'company_id'=>'required|numeric',
                'location_id'=>'required|numeric',
                'start_date'=>'required',
                'time'=>'required',
                'end_date'=>'required',
                'pre_time_mins'=>'required',
                'post_time_mins'=>'required',
            ];
        }

        $validator = Validator::make($request->all(), $data);

        if(isset($request->mon) ||isset($request->tue) ||isset($request->wed) ||isset($request->thu) ||isset($request->fri) ||isset($request->sat) ||isset($request->sun) ||isset($request->bank) ){
            $frequency = true;
        }else{
            $frequency = false;
        }

        if ($validator->fails() || $frequency == false)
        {
            $errors = $validator->errors();
            $error=array();
            if ($request->form_type_create !=="m"){
                if ($errors->first('location_breakdown_id')){
                    $error['location_breakdown_id']='Field required';
                }
            }

            if ($errors->first('start_date')){
                $error['start_date']='Field required';
            }
            if ($errors->first('end_date')){
                $error['end_date']='Field required';
            }
            if ($errors->first('pre_time_mins')){
                $error['pre_time_mins']='Field required';
            }
            if ($errors->first('post_time_mins')){
                $error['post_time_mins']='Field required';
            }
            if ($errors->first('input_starttime')){
                $error['input_starttime']='Field required';
            }
            if ($errors->first('time')){
                $error['time']='Field required';
            }
            if($frequency == false){
                $error['frequency']='select any frequency';
            }
            if ($errors->first('location_id')){
                $error['location_id']='Field required';
            }
            return response()->json(['status'=> 'validation', 'error'=> $error]);
        }

        $data=$request->all();
        //dd($data);
        $form_id=Session::get('form_id');
        $data['form_id']=$form_id;
        if(isset($data['start_date']))
            $data['start_date'] = Carbon::parse($data['start_date']);

        if(isset($data['end_date']))
            $data['end_date'] =Carbon::parse($data['end_date']);

        if(isset($data['recycle_jobs_after']))
            $data['recycle_jobs_after'] = date('Y/m/d', strtotime($data['recycle_jobs_after']));
        if(isset($data['max_display']))
            $data['max_display'] = $data['max_display'];

        if($data['frequency_type']==4)
        {
            $data['week_limit'] = $data['frequency_limit'];
        }else  if($data['frequency_type']==5)
        {
            $data['month_limit'] = $data['frequency_limit'];
        } else  if($data['frequency_type']==6)
        {
            $data['year_limit'] = $data['frequency_limit'];
        }
        unset($data['_token']);

        if ($data['user_id']){
            $data['user_id']=json_encode($data['user_id']);
        }else{
            $data['user_id']=null;
        }

        $schedule_task_id=ScheduleTask::create($data);

        if(isset($data['recycle_jobs_after'])){
            $data['recycle_jobs_after'] = date('Y-m-d', strtotime($data['recycle_jobs_after']));
        }

        if ($request->form_type_create !=="m"){
            if(isset($data['location_breakdown_id']))
            {
                foreach ($data['location_breakdown_id'] as $key => $value) {
                    $temp_data=['company_id'=>$data['company_id'],'schedule_task_id'=>$schedule_task_id->id,'location_id'=>$data['location_id'],'location_breakdown_id'=>$value];
                    ScheduleTaskLocation::create($temp_data);
                }
            }
            else{
                $temp_data=['company_id'=>$data['company_id'],'schedule_task_id'=>$schedule_task_id->id,'location_id'=>$data['location_id']];
                ScheduleTaskLocation::create($temp_data);
            }
        }else{
            $temp_data=['company_id'=>$data['company_id'],'schedule_task_id'=>$schedule_task_id->id,'location_id'=>$data['location_id']];
            ScheduleTaskLocation::create($temp_data);
        }

        if(isset($data['start_date'])){
            $data['start_date'] = Carbon::parse($data['start_date'])->toDateString();
        }
        if(isset($data['end_date'])){
            $data['end_date'] =Carbon::parse($data['end_date'])->toDateString();
        }
        if ($request->form_type_create !=="m"){
            $data['location_breakdown_id']=json_encode($data['location_breakdown_id']);
        }

  audit_log($this->cms_array,'',Config::get('constants.store'),$this->page_name,$this->tab_name,$schedule_task_id->id,'',$schedule_task_id->id,$form_id,$this->db);

        $start_date = Carbon::parse($data['start_date']);
        $end_date = Carbon::parse($data['end_date']);
        $checkdate = $start_date;
        $time = explode(':',$data['time']);
        $checkdate->setTime($time[0],$time[1]);
        $dt = [];
        /*  while($checkdate->lt($end_date))
        {
            if($data['repeat']>1)
            {
                $i=0;
                while($i<$data['repeat'])
                {
                    $case = $checkdate->dayOfWeek;
                    switch ($case) {
                                    case 0:
                                        if(isset($data['sun']))
                                            {
                                             $tempdate=$checkdate;
                                            array_push($dt,$tempdate);
                                              $data['date_time']=$tempdate;
                                            $this->job($data);
                                            $checkdate->addMinutes($data['repeat_min_gap']);
                                            }
                                        break;
                                    case 1:
                                        if(isset($data['mon']))
                                            {
                                             $tempdate=$checkdate;
                                            array_push($dt,$tempdate);
                                              $data['date_time']=$tempdate;
                                            $this->job($data);
                                            $checkdate->addMinutes($data['repeat_min_gap']);
                                            }
                                        break;
                                    case 2:
                                        if(isset($data['tue']))
                                            {
                                             $tempdate=$checkdate;
                                            array_push($dt,$tempdate);
                                              $data['date_time']=$tempdate;
                                            $this->job($data);
                                            $checkdate->addMinutes($data['repeat_min_gap']);
                                            }
                                        break;
                                    case 3:
                                        if(isset($data['wed']))
                                            {
                                             $tempdate=$checkdate;
                                            array_push($dt,$tempdate);
                                              $data['date_time']=$tempdate;
                                            $this->job($data);
                                            $checkdate->addMinutes($data['repeat_min_gap']);
                                            }
                                        break;
                                    case 4:
                                        if(isset($data['thu']))
                                            {
                                             $tempdate=$checkdate;
                                            array_push($dt,$tempdate);
                                              $data['date_time']=$tempdate;
                                            $checkdate->addMinutes($data['repeat_min_gap']);
                                            }
                                        break;
                                    case 5:
                                        if(isset($data['fri']))
                                            {
                                             $tempdate=$checkdate;
                                            array_push($dt,$tempdate);
                                              $data['date_time']=$tempdate;
                                            $this->job($data);
                                            $checkdate->addMinutes($data['repeat_min_gap']);
                                            }
                                        break;
                                    case 6:
                                        if(isset($data['sat']))
                                            {
                                             $tempdate=$checkdate;
                                            array_push($dt,$tempdate);
                                              $data['date_time']=$tempdate;
                                            $this->job($data);
                                            $checkdate->addMinutes($data['repeat_min_gap']);
                                            }
                                        break;
                                    default:
                               return response()->json(['message'=>'Something Went wrong']);
                            }

                        $i++;

                }//while




            }//frequency repeat if




            $checkdate->addDays(1);
        }*///while start and end date
        /*\Artisan::call('schedule:task');*/
        return response()->json(['message'=>'Save Data Successfully..!!']);
    }

    public function update(Request $request)
    {

        if ($request->form_type_edit !=="m"){
            $data = [
                'company_id'=>'required|numeric',
                'location_id'=>'required|numeric',
                'location_breakdown_id'=>'required',
                'start_date'=>'required',
                'time'=>'required',
                'end_date'=>'required',
                'pre_time_mins'=>'required',
                'post_time_mins'=>'required',
            ];
        }else{
            $data = [
                'company_id'=>'required|numeric',
                'location_id'=>'required|numeric',
                'start_date'=>'required',
                'time'=>'required',
                'end_date'=>'required',
                'pre_time_mins'=>'required',
                'post_time_mins'=>'required',
            ];
        }

        $validator = Validator::make($request->all(), $data);
        if(isset($request->mon) ||isset($request->tue) ||isset($request->wed) ||isset($request->thu) ||isset($request->fri) ||isset($request->sat) ||isset($request->sun) ||isset($request->bank) )
            $frequency = true;
        else
            $frequency = false;

        if ($validator->fails() || $frequency==false)
        {
            $errors = $validator->errors();
            $error=array();
            if ($errors->first('location_id')){
                $error['location_id']='Field required';
            }
            if ($request->form_type_edit !=="m"){
                if ($errors->first('location_breakdown_id')){
                    $error['location_breakdown_id']='Field required';
                }
            }

            if ($errors->first('start_date')){
                $error['start_date']='Field required';
            }
            if ($errors->first('end_date')){
                $error['end_date']='Field required';
            }
            if ($errors->first('pre_time_mins')){
                $error['pre_time_mins']='Field required';
            }
            if ($errors->first('post_time_mins')){
                $error['post_time_mins']='Field required';
            }
            if ($errors->first('input_starttime')){
                $error['input_starttime']='Field required';
            }
            if ($errors->first('time')){
                $error['time']='Field required';
            }

            if($frequency == false){
                $error['frequency']='select any frequency';
            }
            return response()->json(['status'=> 'validation', 'error'=> $error]);
        }

        $data=$request->all();
        if(!$data['start_date'])
        {
            unset($data['start_date']);
        }

        if(!$data['time'])
        {
            unset($data['time']);
        }
        if(!$data['end_date'])
        {
            unset($data['end_date']);
        }
        if(!$data['recycle_jobs_after'])
        {
            unset($data['recycle_jobs_after']);
        }
        $data['max_display']=$data['max_display'];

        $data['week_limit']=null;
        $data['month_limit'] =null;
        $data['year_limit'] =null;

        if($data['frequency_type']==4)
        {
            $data['week_limit'] = $data['frequency_limit'];
        }else  if($data['frequency_type']==5)
        {
            $data['month_limit'] = $data['frequency_limit'];
        } else  if($data['frequency_type']==6)
        {
            $data['year_limit'] = $data['frequency_limit'];
        }


        $location_breakdown_id=[];
        if(isset($data['location_breakdown_id']))
        {
            foreach ($data['location_breakdown_id'] as $key => $value) {
                array_push($location_breakdown_id,$value);
            }
            unset($data['location_breakdown_id']);
        }
        $location_id='';
        if($data['location_id'])
        {
            $location_id=$data['location_id'];
            unset($data['location_id']);
        }
        $ScheduleTask=ScheduleTask::where('id',$data['id']);
        $bonus=$ScheduleTask->first();

        $data['mon']=(isset($data['mon'])) ? "1":"0";
        $data['thu']=(isset($data['thu'])) ? "1":"0";
        $data['sun']=(isset($data['sun'])) ? "1":"0";
        $data['tue']=(isset($data['tue'])) ? "1":"0";
        $data['fri']=(isset($data['fri'])) ? "1":"0";
        $data['bank']=(isset($data['bank'])) ? "1":"0";
        $data['wed']=(isset($data['wed'])) ? "1":"0";
        $data['sat']=(isset($data['sat'])) ? "1":"0";
        unset($data['_token']);
//        unset($data['frequency_limit']);
//        unset($data['frequency_type']);

        $data['end_date'] = Carbon::parse($data['end_date'])->toDateString();
        $data['start_date'] = Carbon::parse($data['start_date'])->toDateString();
        if (isset($data['recycle_jobs_after'])){
            $data['recycle_jobs_after'] = Carbon::parse($data['recycle_jobs_after'])->toDateString();
        }

        if (isset($data['location_breakdown_id'])){
            $data['location_breakdown_id']=json_encode($data['location_breakdown_id']);
        }


        if (isset($data['user_id'])){
            $data['user_id']=json_encode($data['user_id']);
        }else{
            $data['user_id']=null;
        }
        unset($data['form_type_edit']);
        unset($data['frequency_limit']);
        unset($data['frequency_type']);
        $ScheduleTask->update($data);
         $form_id=Session::get('form_id');
        audit_log($this->cms_array,audit_log_data($data),Config::get('constants.update'),$this->page_name,$this->tab_name,$bonus->id,audit_log_data($bonus->toArray()),$bonus->id,$form_id,$this->db);

        if ($request->form_type_edit !=="m"){
            ScheduleTaskLocation::where('schedule_task_id',$data['id'])->delete();
            foreach ($location_breakdown_id as $key => $value) {
                $temp_data=['company_id'=>$data['company_id'],'schedule_task_id'=>$data['id'],'location_id'=>$location_id,'location_breakdown_id'=>$value];
                ScheduleTaskLocation::create($temp_data);
            }
        }else{
            ScheduleTaskLocation::where('schedule_task_id',$data['id'])->delete();
            $temp_data=['company_id'=>$data['company_id'],'schedule_task_id'=>$data['id'],'location_id'=>$location_id];
            ScheduleTaskLocation::create($temp_data);
        }

        if($bonus->active == 0)
        {
            $flag=0;
        }
        else
        {
            $flag=1;
        }
        return response()->json(['message'=>'Successfully Added !!','flag'=>$flag]);
    }


    public function softDelete (Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.soft_delete'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('form_id'),$this->db);
        ScheduleTask::where('id',$request->id)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function delete(Request $request)
    {
        //audit_log('',Config::get('constants.delete'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('form_id'));
        ScheduleTask::where('id',$request->id)->update(['active'=>0,'deleted'=>0]);
    audit_log($this->cms_array,'',Config::get('constants.delete'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('form_id'),$this->db);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function active(Request $request)
    {
      audit_log($this->cms_array,'',Config::get('constants.restore'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('form_id'),$this->db);
        ScheduleTask::where('id',$request->id)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
    public function activeMultiple(Request $request)
    {
        ScheduleTask::whereIn('id',$request->ids)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }

    public function deleteMultipleSOft(Request $request)
    {
        ScheduleTask::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function deleteMultiple(Request $request)
    {
        ScheduleTask::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }

    public function exportPdf(Request $request)
    {
        $form_id=Session::get('form_id');
        if($request->pdf_array == 1)
        {
            $data= ScheduleTask::where('form_id',$form_id)->with('scheduleTaskLocation.location')->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->pdf_array);
            $data= ScheduleTask::where('form_id',$form_id)->whereIn('id',$array)->with('scheduleTaskLocation.location')->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        $companies='Patroltec';
        $reportname='Schedule';
        $date=\Carbon\Carbon::now()->format('D jS M Y');
        $pdf = PDF::loadView('backend.schedule.pdf',compact('companies','reportname','data','date'));
        return $pdf->download('schedule.pdf');
    }
    public function exportWorld(Request $request)
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        $form_id=Session::get('form_id');
        if($request->word_array == 1)
        {
            $data= ScheduleTask::where('form_id',$form_id)->with('scheduleTaskLocation.location')->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->word_array);
            $data= ScheduleTask::where('form_id',$form_id)->whereIn('id',$array)->with('scheduleTaskLocation.location')->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        $table = $section->addTable();
        foreach ($data as $key => $value) {
            if($key==0)
            {
                $table->addRow();
                if(Auth()->user()->hasPermissionTo('schedule_projectname_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Location Name');
                }else{
                    $table->addCell(1750)->addText('');
                }
                if(Auth()->user()->hasPermissionTo('schedule_startdate_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Start Date');
                }else{
                    $table->addCell(1750)->addText('');
                }
                if(Auth()->user()->hasPermissionTo('schedule_time_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Time');
                }else{
                    $table->addCell(1750)->addText('');
                }
            }
            $table->addRow();
            if(Auth()->user()->hasPermissionTo('schedule_projectname_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText($value->scheduleTaskLocation->location->name);
            }else{
                $table->addCell(1750)->addText('');
            }
            if(Auth()->user()->hasPermissionTo('schedule_startdate_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText($value->start_date);
            }else{
                $table->addCell(1750)->addText('');
            }
            if(Auth()->user()->hasPermissionTo('schedule_time_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText($value->time);
            }else{
                $table->addCell(1750)->addText('');
            }

        }
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('ScheduleTask.docx');
        return response()->download(public_path('ScheduleTask.docx'));
    }
    public function exportExcel(Request $request)
    {
        if($request->excel_array == 1)
        {
            $form_id=Session::get('form_id');
            $data= ScheduleTask::where('form_id',$form_id)->with('scheduleTaskLocation.location')->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $form_id=Session::get('form_id');
            $array=json_decode($request->excel_array);
            $data= ScheduleTask::where('form_id',$form_id)->whereIn('id',$array)->with('scheduleTaskLocation.location')->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        return Excel::download(new ScheduleExport($data), 'ScheduleTask.csv');
    }

    public function job($input)
    {

        $input['user_id'] = array_filter($input['user_id']);
        //dd($input);
        $input['completed'] = 0;
        $users =[];
        $users = $input['user_id'];
        $input['contractor_id'] = Location::find($input['location_id'])->contractor_id;
        $form_sections = FormVersion::where('form_id',$input['form_id'])->where('active',1)->where('deleted',0)->orderBy('id','desc')->get()->pluck('form_section_id');
        $form = Forms::find($input['form_id']);
        $date = $input['date_time'];
        if($input['pre_time_select']==1)
        {
            $input['post_time'] = $date->addMinutes($input['pre_time_mins']);
        }else  if($input['pre_time_select']==2)
        {
            $input['post_time'] = $date->addHours($input['pre_time_mins']);
        } else  if($input['pre_time_select']==3)
        {
            $input['post_time'] = $date->addDays($input['pre_time_mins']);
        } else  if($input['pre_time_select']==4)
        {
            $input['post_time'] = $date->addWeeks($input['pre_time_mins']);
        } else{
            $input['post_time'] = $date;
        }
        if($input['post_time_select']==1)
        {
            $input['pre_time'] = $date->subMinutes($input['post_time_mins']);
        }else  if($input['post_time_select']==2)
        {
            $input['pre_time'] = $date->subHours($input['post_time_mins']);
        } else  if($input['post_time_select']==3)
        {
            $input['pre_time'] = $date->subDays($input['post_time_mins']);
        } else  if($input['post_time_select']==4)
        {
            $input['pre_time'] = $date->subWeeks($input['post_time_mins']);
        } else{
            $input['pre_time'] = $date;
        }
        unset($input['_token']);
        unset($input['date']);
        unset($input['time']);
        $input['title']=$form->name;
        $breakdown = [];
        if(isset( $input['location_breakdown_id'])){

            $breakdown = $input['location_breakdown_id'];
        }

        foreach ($users as $key => $user) {
            $input['user_id'] = $user;
            $input['user_group_id'] = User::find($user)->user_group_id;

            if(isset( $input['location_breakdown_id'])){
                foreach ($breakdown as $value) {
                    $input['location_breakdown_id']=$value;
                    $jobGroup = JobGroup::create($input);
                    $input['job_group_id'] = $jobGroup->id;
                    $x = [];

                    foreach ($form_sections as $form_section)
                    {
                        array_push($x,"1");
                        $input['form_section_id'] = $form_section;
                        Jobs::Create($input);
                    }
                }
            }

            else
            {
                $jobGroup = JobGroup::create($input);
                $input['job_group_id'] = $jobGroup->id;
                $x = [];

                foreach ($form_sections as $form_section)
                {
                    array_push($x,"1");
                    $input['form_section_id'] = $form_section;
                    Jobs::Create($input);
                }
            }
        }
    }



    public function testCron()
    {
        $tasks=ScheduleTask::with('scheduleTaskLocation')->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        $today = Carbon::today();
        foreach ($tasks as $key => $task) {
            $start_date = Carbon::parse($task->start_date);
            $end_date = Carbon::parse($task->end_date);

            if($today->greaterThanOrEqualTo($start_date) && $today->lessThanOrEqualTo($end_date))
            {
                $location = $task->scheduleTaskLocation->toArray();
                unset($location['id']);
                unset($location['created_at']);
                unset($location['updated_at']);
                unset($location['schedule_task_id']);
                unset($location['company_id']);
                if($location['location_breakdown_id']==null)
                {
                    unset($location['location_breakdown_id']);
                }
                $temp = $task->toArray();
                unset($temp['id']);
                unset($temp['created_at']);
                unset($temp['updated_at']);
                unset($temp['schedule_task_location']);

                $data = array_merge($temp,$location);
                // dd($data);


                if($task->repeat>1)
                {
                    $i=0;
                    while($i<$task->repeat)
                    {
                        $case = $today->dayOfWeek;
                        switch ($case) {
                            case 0:
                                if($task->sun == 1)
                                {

                                    $data['date_time']=$today;
                                    $this->job($data);
                                    $checkdate->addMinutes($data['repeat_min_gap']);
                                }
                                break;
                            case 1:
                                if($task->mon == 1)
                                {

                                    $data['date_time']=$today;
                                    $data['user_id'] = ['1',null];
                                    $this->job($data);
                                    $checkdate->addMinutes($data['repeat_min_gap']);
                                }
                                break;
                            case 2:
                                if($task->tue == 1)
                                {


                                    $data['date_time']=$today;
                                    $data['user_id'] = ['1',null];
                                    $this->job($data);

                                }
                                break;
                            case 3:
                                if($task->wed == 1)
                                {
                                    $tempdate=$checkdate;
                                    array_push($dt,$tempdate);
                                    $data['date_time']=$tempdate;
                                    $this->job($data);
                                    $checkdate->addMinutes($data['repeat_min_gap']);
                                }
                                break;
                            case 4:
                                if($task->thu == 1)
                                {
                                    $tempdate=$checkdate;
                                    array_push($dt,$tempdate);
                                    $data['date_time']=$tempdate;
                                    $checkdate->addMinutes($data['repeat_min_gap']);
                                }
                                break;
                            case 5:
                                if($task->fri == 1)
                                {
                                    $tempdate=$checkdate;
                                    array_push($dt,$tempdate);
                                    $data['date_time']=$tempdate;
                                    $this->job($data);
                                    $checkdate->addMinutes($data['repeat_min_gap']);
                                }
                                break;
                            case 6:
                                if($task->sat == 1)
                                {
                                    $tempdate=$checkdate;
                                    array_push($dt,$tempdate);
                                    $data['date_time']=$tempdate;
                                    $this->job($data);
                                    $checkdate->addMinutes($data['repeat_min_gap']);
                                }
                                break;

                        }

                        $i++;

                    }//while
                }
            }
        }
    }

}
