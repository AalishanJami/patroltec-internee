<?php
namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Jobs;
use App\SiteContacts;
use App\JobGroup;
use App\User;
use App\Company;
use App\Location;
use App\LocationBreakdown;
use App\Forms;
use DataTables;
use Mail;
use Carbon\Carbon;
use Validator;
use Auth;
use App\FormVersion;
use App\Response;
use App\ResponseGroups;
use App\Answers;
use App\Questions;
use App\FormType;
use App\PrePopulate;
use App\PopulateResponse;
use App\LinkedJobGroup;
use App\FormSections;
use CloudConvert;
use Config;
use File;
use NcJoes\OfficeConverter\OfficeConverter;
use Symfony\Component\Process\Process;
class JobController extends Controller
{
    private $page_name;
    private $tab_name;
    private $cms_array=[];
    public function __construct() {
        $this->page_name ='Job';
        $this->tab_name ='Job';
        $this->db='locations';
        $cms_data=localization();
        $temp_array=[];
        $temp_array['location_id']['key']='project_name';
        $temp_array['location_id']['value']=checkKey('project_name',$cms_data);
        $temp_array['user_id']['key']='assign_to';
        $temp_array['user_id']['value']=checkKey('assign_to',$cms_data);
        $temp_array['location_breakdown_name']['key']='location_break_down';
        $temp_array['location_breakdown_name']['value']=checkKey('location_break_down',$cms_data);
        $temp_array['date']['key']='date';
        $temp_array['date']['value']=checkKey('date',$cms_data);
        $temp_array['time']['key']='time';
        $temp_array['time']['value']=checkKey('time',$cms_data);
        $temp_array['pre_time']['key']='pre';
        $temp_array['pre_time']['value']=checkKey('pre',$cms_data);
        $temp_array['pre_time_select']['key']='time';
        $temp_array['pre_time_select']['value']=checkKey('time',$cms_data);
        $temp_array['post_time']['key']='post';
        $temp_array['post_time']['value']=checkKey('post',$cms_data);
        $temp_array['post_time_select']['key']='time';
        $temp_array['post_time_select']['value']=checkKey('time',$cms_data);
        $temp_array['notifiable']['key']='user_name';
        $temp_array['notifiable']['value']=checkKey('user_name',$cms_data);
        $temp_array['form_type_id']['key']='form_type';
        $temp_array['form_type_id']['value']=checkKey('form_type',$cms_data);
        $temp_array['form_id']['key']='form_name';
        $temp_array['form_id']['value']=checkKey('form_name',$cms_data);
        $temp_array['populate_check']['key']='populate_check';
        $temp_array['populate_check']['value']=checkKey('populate_check',$cms_data);
        $temp_array['cost']['key']='total_cost';
        $temp_array['cost']['value']=checkKey('total_cost',$cms_data);
        $temp_array['billing_info']['key']='bill_info';
        $temp_array['billing_info']['value']=checkKey('bill_info',$cms_data);
        $temp_array['note_value']['key']='notes';
        $temp_array['note_value']['value']=checkKey('notes',$cms_data);
        $this->cms_array=$temp_array;
        remove_key();
    }
    public function index($id=null)
    {
        $companies=Company::getCompanies();
        $locations = Location::where('active',1)->where('deleted',0)->where('company_id',Auth::user()->default_company)->orderBy('id', 'desc')->get()->pluck('name','id');
       /* $locationbreaks = LocationBreakdown::where('active',1)->where('deleted',0)->where('type','m')->orderBy('id', 'desc')->get()->pluck('name','id');*/

       $locationbreaks = [];
        $users  = User::where('active',1)->where('deleted',0)->where('company_id',Auth::user()->default_company)->orderBy('id', 'desc')->get()->pluck('first_name','id');
        $forms = Forms::where('active',1)->where('deleted',0)->where('company_id',Auth::user()->default_company)->orderBy('id', 'desc')->get()->pluck('name','id');
        $types = FormType::where('active',1)->where('deleted',0)->where('company_id',Auth::user()->default_company)->orderBy('id', 'desc')->has('forms')->get()->pluck('name','id');

        if($id==null)
        {
            $id=0;
        }
        $date = Carbon::now()->format('jS M Y');
        return view('backend.job.index')->with(['locations'=>$locations,'users'=>$users,'forms'=>$forms,'types'=>$types,'id'=>$id,'date'=>$date,'locationbreaks'=>$locationbreaks,'companies'=>$companies]);
    }

    public function createFormmodal(){
        $form = view('backend.job.model.create_form');
        return $form;
    }
    public function getAll($id=null)
    {
        $data=JobGroup::with('users','forms','locations','jobs','locationBreakdowns')->where('company_id',Auth::user()->default_company)->orderBy('date_time','desc')->get();

        $data = $data->where('active',1)->where('deleted',0);

        if($id!=0 && $id!=null)
        { $data = FormType::with('jobGroup.users','jobGroup.forms','jobGroup.locations','jobGroup.jobs','jobGroup.locationBreakdowns')->get()->where('company_id',Auth::user()->default_company)->orderBy('date_time','desc')->find($id);

            $data = $data->JobGroup;
        }

        $now = new Carbon();
        foreach ($data as $key => $value) {
            if($value->completed==2)
            {
                $date = Carbon::parse($value->updated_at);
                $date = $date->addMinutes(5);
                if($now->greaterThan($date))
                {
                    unset($data[$key]);
                }
            }
        }

        //  dd($data);

        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {

            })
            ->addColumn('employee_name', function ($data) {
                $response = "";
                $response = $data->users->first_name;
                return $response;
            }) ->addColumn('location_name', function ($data) {
                $response = "";

                $response = $data->locations->name;
                return $response;
            }) ->addColumn('location_breakdown', function ($data) {
                $response = "";

                $response = $data->locationBreakdowns->name ?? 'main';
                return $response;
            })->addColumn('form_name', function ($data) {
                $response = "";

                $response = $data->forms->name;
                return $response;
            })->addColumn('date_time', function ($data) {
                $response = "";
                if(isset($data->jobs[0]->date_time))
                    //$response = $data->jobs[0]->date_time;
                    $response =Carbon::parse( $data->jobs[0]->date_time)->format('yy-m-d');
                return $response;
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                $view = "";
                $delete = "";

                if($data->completed!=2)
                {

                    $view = '<a type="button" class="btn btn-primary btn-xs my-0 all_action_btn_margin waves-effect waves-light" href="'.url("/job/view").'/'.$data->id.'"><i class="fas fa-eye"></i></a>';
                    if($data->forms->req_delete)
                    {

                        $delete = '<a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light" onclick="deleteJob('.$data->id.',1)"><i class="fas fa-trash"></i></a>';
                    }else
                    {
                        $delete = '<a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light" onclick="deleteJob('.$data->id.',0)"><i class="fas fa-trash"></i></a>';
                    }
                }
                if(Auth()->user()->hasPermissionTo('delete_job') || Auth::user()->all_companies == 1)
                {
                    $response=$response.$delete;
                }
                if(Auth()->user()->hasPermissionTo('view_job') || Auth::user()->all_companies == 1)
                {
                    $response=$response.$view;
                }


                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
    public function view($id)
    {
        $companies=Company::getCompanies();
        $job_group = JobGroup::with('users','forms','locations','locationBreakdowns')->find($id);
        $jobs = Jobs::with('form_Section.questionsS.answer_type','form_Section.questionsS.answer_group.answer','form_Section.form_version.version')->where('job_group_id',$job_group->id)->where('active',1)->where('deleted',0)->orderBy('order','asc')->get();
        // dd($jobs->first());
        $form_sections = $jobs->pluck('form_section_id');
        $form_sections = array_unique($form_sections->toArray());
        //dd($form_sections);
        $ids = $jobs->pluck('id');
        $types = FormType::where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get()->pluck('name','id');
        // dd($jobs);
        /*$populate = PopulateResponse::where('pre_populate_id',$jobs[0]->pre_populate_id)->where('active',1)->where('deleted',0)->get();*/
        /*$populate = ResponseGroups::with('responses')->where('job_group_id',$id)->where('active',1)->where('deleted',0)->get()->last()->responses;*/
        $response_groups = ResponseGroups::with('responses')->where('job_group_id',$id)->where('active',1)->where('deleted',0)->get();
        // dd($response_groups);
        //dd($response_groups[0]->responses->where('answer_id',125));
        //dd($jobs->pluck('id'));
        if($response_groups->isEmpty())
        {
            $empty=true;
        }
        else
        {
            $empty=false;
        }
        $populate = null;
        // dd($response_groups);
        //dd($populate);
        $users  = User::getUser()->get()->pluck('first_name','id');
        /*   if($populate->isEmpty())
           {
             $coll=collect(['answer_id'=>0]);
             $populate->push($coll);
             $populate->push($coll);
           }*/
        // dd($populate);
        $id_static='';
        if(isset($jobs->last()->form_Section->form_version->version->static_form_uploads_id))
        {
            $id_static=$jobs->last()->form_Section->form_version->version->static_form_uploads_id;
        }
        //dd($jobs->last()->form_Section->form_version->version->static_form_uploads_id);
        return view('backend.job.formjs')->with(['jobs'=>$jobs,'companies'=>$companies,'id_static'=>$id_static,'job_group'=>$job_group,'populate'=>$populate,'users'=>$users,'types'=>$types,'response_groups'=>$response_groups,'empty'=>$empty,'ids'=>$ids]);
    }

    public function getVersion(Request $request)
    {
        $jobs = Jobs::with('form_Section.questionsS.answer_type','form_Section.questionsS.answer_group.answer','form_Section.form_version.version')->where('job_group_id',$request->varsion_id)->where('active',1)->where('deleted',0)->orderBy('order','asc')->get();
        $form_id = JobGroup::find($request->varsion_id)->form_id;

        $form_sections = $jobs->pluck('form_section_id');
        $form_sections = array_values($form_sections->toArray());
        $form_section = FormSections::whereIn('id',$form_sections)->with('questionsS.answer_type','questionsS.answer_group.answer')->orderBy('order','asc')->get();
        $response_groups = Response::whereIn('job_id',$jobs->pluck('id'))->whereIn('form_section_id',$form_sections)->where('active',1)->where('deleted',0)->get();

        return response()->json(['form_section'=>$form_section,'jobs'=>$jobs,'response_groups'=>$response_groups]);
    }

    public function viewPdf($id)
    {
        $file = JobGroup::find($id);
        /*    $mpdf = new \Mpdf\Mpdf(['debug' => true]);
              $pagecount= $mpdf->SetSourceFile($file->file_name);
               $mpdf->debug = true;
               $mpdf->showImageErrors = true;
               $tplId = $mpdf->ImportPage($pagecount);
               $mpdf->UseTemplate($tplId);
               $new = explode(".",$file->file_name);
               //dd($new);
               $mpdf->WriteHTML(' ');
           return   $mpdf->Output($new[0].'N.'.$new[1],"I");*/
        return response()->file($file->file_name);
    }

    public function getForm($id)
    {

        $form = Forms::where('Form_type',$id)->where('active',1)->where('deleted',0)->has('version')->orderBy('id', 'desc')->get();
        return response()->json(['form'=>$form]);
    }
    public function getContact($id)
    {

        $contact = SiteContacts::where('location_id',$id)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        return response()->json(['contact'=>$contact]);
    }

    public function getLocation($id)
    {

       $location = LocationBreakdown::where('active',1)->where('deleted',0)->where('location_id',$id)->where('type','m')->orderBy('id', 'desc')->get()->pluck('name','id');
        return response()->json(['location'=>$location]);
    }

    public function getPopulate($id)
    {

        $populate = PrePopulate::where('Form_id',$id)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        $version = FormVersion::where('form_id',$id)->where('active',1)->where('deleted',0)->orderBy('version_id', 'desc')->first()->version_id;
        return response()->json(['populate'=>$populate,'version'=>$version]);
    }


    public function store(Request $request)
    {
        $input = $request->all();
        $error_data = [
            'location_id'=>'required|numeric',
            'location_breakdown_name'=>'required',
            'user_id'=>'required',
            //'notifiable'=>'required',
            'form_type_id'=>'required',
            'form_id'=>'required',
            'date'=>'required',
            'time'=>'required',
        ];
        $validator = Validator::make($request->all(), $error_data);

        if ($validator->fails())
        {
            $errors = $validator->errors();
//            dd($errors);
            $error=array();
            if ($errors->first('location_id')){
                $error['location_id']='Field required';
            }
            if ($errors->first('location_breakdown_name')){
                $error['location_breakdown_name']='Field required';
            }
            if ($errors->first('user_id')){
                $error['user_id']='Field required';
            }
            if ($errors->first('notifiable')){
                $error['notifiable']='Field required';
            }
            if ($errors->first('form_type_id')){
                $error['form_type_id']='Field required';
            }
            if ($errors->first('form_id')){
                $error['form_id']='Field required';
            }
            if ($errors->first('date')){
                $error['date']='Field required';
            }
            if ($errors->first('time')){
                $error['time']='Field required';
            }
            return response()->json(['status'=> 'validation', 'error'=> $error]);
        }
        $input['completed'] = 0;
        $users =[];
        $users =  $input['user_id'];
         if (isset($input['user_id'])){
            $input['user_id'] = array_filter($input['user_id']);
        }
        if($request->file())
        {
            $time = time();
            $file = $request->file('attachments');
            //dd($file);
            $name = $file->getClientOriginalName();
            $pdf=explode(".",$name);
            $attachments = $time.'.'.$file->getClientOriginalExtension();
            $destinationPath = public_path('attachments');
            $file->move($destinationPath, $attachments);
            $input['file_name']= $destinationPath.'/'.$time.'.pdf';
            $converter = new OfficeConverter($destinationPath.'/'.$attachments);
            $converter->convertTo($input['file_name']);
        }

        $input['contractor_id'] = Location::find($input['location_id'])->contractor_id;
        $form_sections = FormVersion::where('form_id',$input['form_id'])->where('active',1)->where('deleted',0)->get();
        $form = Forms::find($input['form_id']);
        $form_sections =  $form_sections->groupBy('version_id')->first();

        if($form_sections==null)
        {
            $notification = array(
                'message' => 'No active Form found with Questions',
                'alert-type' => 'error'
            );
            return response()->json(['error_ms'=>'No active Form found with Questions']);

        }
        if($form_sections->isEmpty())
        {
            $notification = array(
                'message' => 'No active Form found with Questions',
                'alert-type' => 'error'
            );
            return response()->json(['error_ms'=>'No active Form found with Questions']);
        }

        $form_sections = $form_sections->pluck('form_section_id');
        $form_section = array_unique($form_sections->toArray());
        $form_sections = FormSections::whereIn('id',$form_section)->with('questionsS.answer_type','questionsS.answer_group.answer')->orderBy('order','asc')->get();
        //dd($form_sections);
        $mdate = $input['date'];
        $date = Carbon::parse($input['date']);
        $time = explode(':',$input['time']);
        $date->setTime($time[0],$time[1]);
        $input['date_time'] = $date;
        $pre_time= new Carbon($date);
        $post_time = new Carbon($date);
        if($input['post_time_select']==1)
        {
            $input['post_time'] = ($post_time->addMinutes((int)$input['post_time']));
        } if($input['post_time_select']==2)
    {
        $input['post_time'] = ($post_time->addHours((int)$input['post_time']));

    }  if($input['post_time_select']==3)
    {
        $input['post_time'] = ($post_time->addDays((int)$input['post_time']));
    }  if($input['post_time_select']==4)
    {
        $input['post_time'] = ($post_time->addWeeks((int)$input['post_time']));
    }else
    {
        $input['post_time'] = $post_time;
    }


        if($input['pre_time_select']==1)
        {
            $input['pre_time'] = ($pre_time->subMinutes($input['pre_time']));
        } if($input['pre_time_select']==2)
    {
        $input['pre_time'] = ($pre_time->subHours($input['pre_time']));
    }  if($input['pre_time_select']==3)
    {
        $input['pre_time'] = ($pre_time->subDays($input['pre_time']));
    }  if($input['pre_time_select']==4)
    {
        $input['pre_time'] = ($pre_time->subWeeks($input['pre_time']));
    }
    else
    {
        $input['pre_time'] = $pre_time;
    }


        unset($input['_token']);
        unset($input['date']);
        unset($input['time']);
        $input['title']=$form->name;
        $input['total_cost']=$input['cost'];
        $input['cost_info']=$input['billing_info'];
        $input['location_breakdown_id']=$input['location_breakdown_name'];

        $audit_input =$request->all();
        unset($audit_input['_token']);
        unset($audit_input['file_name']);
        if (isset($audit_input['location_breakdown_id'])){
            $audit_input['location_breakdown_id']=json_encode($audit_input['location_breakdown_id']);
        }
        if (isset($audit_input['user_id'])){
            $audit_input['user_id']=json_encode($audit_input['user_id']);
        }
        if (isset($audit_input['notifiable'])){
            $audit_input['notifiable']=json_encode($audit_input['notifiable']);
        }
        $audit_input['date']=Carbon::parse($audit_input['date'])->toDateString();
        if(is_array($users))
        {
            foreach ($users as $key => $user) {
                $input['user_id'] = $user;
                $input['user_group_id'] = User::find($user)->user_group_id;
                $input['company_id'] = Auth::user()->default_company;
                $jobGroup = JobGroup::create($input);
                audit_log($this->cms_array,audit_log_data($audit_input),Config::get('constants.store'),$this->page_name,$this->tab_name,$jobGroup->id,'','',$jobGroup->id,'job_groups');

                $input['job_group_id'] = $jobGroup->id;
                $x = [];
                foreach ($form_sections as $form_section) {
                    array_push($x,"1");
                    $input['form_section_id'] = $form_section->id;
                    $input['order'] = $form_section->order;
                    unset($input['company_id']);
                    $job = Jobs::Create($input);
                    audit_log($this->cms_array,audit_log_data($audit_input),Config::get('constants.store'),$this->page_name,$this->tab_name,$job->id,'','',$job->id,'jobs');
                    if(isset($input['pre_populate_id']) && $input['pre_populate_id']!=null )
                    {
                        unset($input['completed']);
                        $rds = ResponseGroups::where('location_id',$input['location_id'])->where('location_breakdown_id',$input['location_breakdown_id'])->where('pre_populate_id',$input['pre_populate_id'])->where('active',1)->where('deleted',0)->get();
                        // dd($rds);
                        if($rds->isEmpty())
                        {

                            $jobs = Jobs::where('job_group_id',$jobGroup->id)->orderBy('order','asc')->get();

                            $responseGroups = ResponseGroups::create($input);
                            $populates = PopulateResponse::where('pre_populate_id',$input['pre_populate_id'])->where('form_section_id',$form_section->id)->where('active',1)->where('deleted',0)->get();
                            foreach ($populates as $key => $populate) {
                                $populate->response_group_id = $responseGroups->id;
                                $populate->job_id = $jobs[$key]->id;
                                $res =Response::create($populate->toArray());
                                audit_log($this->cms_array,audit_log_data($audit_input),Config::get('constants.store'),$this->page_name,$this->tab_name,$res->id,'','',$res->id,'responses');
                            }
                        }
                        else
                        {
                            foreach ($rds as $key => $rd) {
                                $nrd = $rd->replicate();
                                $nrd->job_group_id = $jobGroup->id;
                                $nrd->save();
                                $rps = Response::where('response_group_id',$rd->id)->get();
                                //dd($rps);
                                foreach ($rps as $key => $rp) {
                                    $nrp = $rp->replicate();
                                    $nrp->response_group_id = $nrd->id;
                                    $nrp->job_id = $job->id;
                                    $nrp->save();
                                    audit_log($this->cms_array,audit_log_data($audit_input),Config::get('constants.update'),$this->page_name,$this->tab_name,$nrp->id,'','',$nrp->id,'responses');
                                }
                            }
                        }
                    }
                }
            }
        }else
        {
            $input['user_id'] = $user;
            $input['user_group_id'] = User::find($input['user_id'])->user_group_id;
            $input['company_id'] = Auth::user()->default_company;
            $jobGroup = JobGroup::create($input);
            audit_log($this->cms_array,audit_log_data($audit_input),Config::get('constants.store'),$this->page_name,$this->tab_name,$jobGroup->id,'','',$jobGroup->id,'job_groups');

            $input['job_group_id'] = $jobGroup->id;
            $x = [];
            foreach ($form_sections as $form_section) {
                array_push($x,"1");
                $input['form_section_id'] = $form_section->id;
                $input['order'] = $form_section->order;
                unset($input['company_id']);
                $job = Jobs::Create($input);
                audit_log($this->cms_array,audit_log_data($audit_input),Config::get('constants.store'),$this->page_name,$this->tab_name,$jobGroup->id,'','',$jobGroup->id,'jobs');
                if(isset($input['pre_populate_id']) && $input['pre_populate_id']!=null )
                {
                    unset($input['completed']);

                    $rds = ResponseGroups::where('location_id',$input['location_id'])->where('location_breakdown_id',$input['location_breakdown_id'])->where('pre_populate_id',$input['pre_populate_id'])->where('active',1)->where('deleted',0)->get();
                    // dd($rds);
                    if($rds->isEmpty())
                    {

                        $jobs = Jobs::where('job_group_id',$jobGroup->id)->orderBy('order','asc')->get();

                        $responseGroups = ResponseGroups::create($input);
                        $populates = PopulateResponse::where('pre_populate_id',$input['pre_populate_id'])->where('form_section_id',$form_section->id)->where('active',1)->where('deleted',0)->get();
                        foreach ($populates as $key => $populate) {
                            $populate->response_group_id = $responseGroups->id;
                            $populate->job_id = $jobs[$key]->id;
                            $res=Response::create($populate->toArray());
                            audit_log($this->cms_array,audit_log_data($audit_input),Config::get('constants.store'),$this->page_name,$this->tab_name,$res->id,'','',$res->id,'responses');

                        }


                    }
                    else
                    {
                        foreach ($rds as $key => $rd) {
                            $nrd = $rd->replicate();
                            $nrd->job_group_id = $jobGroup->id;
                            $nrd->save();
                            $rps = Response::where('response_group_id',$rd->id)->get();
                            //dd($rps);
                            foreach ($rps as $key => $rp) {
                                $nrp = $rp->replicate();
                                $nrp->response_group_id = $nrd->id;
                                $nrp->job_id = $job->id;
                                $nrp->save();
                                audit_log($this->cms_array,audit_log_data($audit_input),Config::get('constants.update'),$this->page_name,$this->tab_name,$nrp->id,'','',$nrp->id,'responses');
                            }
                        }
                    }
                }
            }
        }

        $project_name = Location::find($input['location_id'])->name;
        $locationbreaks = LocationBreakdown::find($input['location_breakdown_id'])->name;

        if(isset($request->notifiable))
        {
            //dd($notifiable);
            $notifiable = array_filter($request->notifiable);
            if($notifiable!=null && !empty($notifiable))
            {


                foreach ($request->notifiable as $key => $value) {
                    $user=SiteContacts::where('id',$value)->first();
                    if(isset($mailData['message']))
                    {
                        $data = array('message'=>$mailData['message'],'title'=>$mailData['title'],'link'=>$mailData['link'],'email'=>$user->email,'name'=>$user->first_name,'id'=>$jobGroup->id,'project_name'=>$project_name,'locationbreaks'=>$locationbreaks,'date'=>$mdate,'cost'=>$input['cost'],'billing_info'=>$input['billing_info'],'notes'=>$input['notes']);
                        //dd($data);
                        Mail::send('mail_jobs',$data, function($message) use ($data) {
                            $message->to($data['email'], 'Customer')->subject
                            ('Patroltec Customer Support');
                            $message->attach($data['link']);
                            $message->from('admin@alandale.patroltec.net','Admin');
                        });
                    }
                    else
                    {
                        $data = array('email'=>$user->email,'name'=>$user->first_name,'id'=>$jobGroup->id,'project_name'=>$project_name,'locationbreaks'=>$locationbreaks,'date'=>$mdate,'cost'=>$input['cost'],'billing_info'=>$input['billing_info'],'notes'=>$input['notes']);
                        // dd($data);
                        Mail::send('mail_jobs',$data, function($message) use ($data) {
                            $message->to($data['email'], 'Customer')->subject('Patroltec Customer Support');
                            $message->from('support@petroltec','Admin');
                        });
                    }
                }
            }
        }

        if(empty($x))
        {
            $notification = array(
                'message' => 'No active Form found with Questions',
                'alert-type' => 'error'
            );
            return response()->json(['error_ms'=>'No active Form found with Questions']);
        }
        else
        {
            $notification = array(
                'message' => 'Job raised Successfully',
                'alert-type' => 'success'
            );
            return response()->json(['message'=>'Job raised Successfully']);
        }
    }

    public function delete(Request $request)
    {
        audit_log('',Config::get('constants.delete'),$this->page_name,$this->tab_name,$request->id,'',$request->id);

        if(isset($request->delete_reason))

        {
            JobGroup::where('id',$request->id)->update(['active'=>0,'delete_reason'=>$request->delete_reason]);
            return redirect()->back();
        }
        else
        { JobGroup::where('id',$request->id)->update(['active'=>0]);
            return response()->json(['message'=>'deleted']);
        }

    }
    public function response(Request $request)
    {
        $input = $request->all();
        //dd($input);
        $responseGroupScore = 0;
        $totalScoreGroup = 0;
        $passCount =0;
        $failCount=0;

        if($input['complete']=="true")
        {
            $complete =true;
        }
        else
        {
            $complete =false;
        }
        unset($input['complete']);
        //dd($input);
        if(isset($input['response_group_id']) && $input['response_group_id']!=null )
        {
            //dd($input);

            ResponseGroups::find($input['response_group_id'])->update($input);

            $responseGroup = ResponseGroups::find($input['response_group_id']);
            Response::where('response_group_id',$input['response_group_id'])->delete();

        }else
        {
            $responseGroup = ResponseGroups::create($input);

        }

        if($complete)
        {
            // dd($complete);
            $jobGroup=JobGroup::find($responseGroup->job_group_id)->first();
            JobGroup::find($responseGroup->job_group_id)->update(['completed'=>2]);
            /* unset($input['_token']);
             unset($input['file']);
            // $input['date'] = Carbon::parse($input['date'])->toDateString();
             audit_log($input,Config::get('constants.update'),$this->page_name,$this->tab_name,$jobGroup->id,$jobGroup->toArray(),$jobGroup->id);*/

        }
        else
        {
            $jobGroup=JobGroup::find($responseGroup->job_group_id)->first();
            JobGroup::find($responseGroup->job_group_id)->update(['completed'=>-1]);
            /*  unset($input['_token']);
              unset($input['file']);
              //$input['date'] = Carbon::parse($input['date'])->toDateString();
              audit_log($input,Config::get('constants.update'),$this->page_name,$this->tab_name,$jobGroup->id,$jobGroup->toArray(),$jobGroup->id);*/
        }
        $job_id = $input['job_id'];
        $user_id = $input['user_id'];
        $location_id = $input['location_id'];
        $job_group_id = $input['job_group_id'];
        if($input['populate_response_id'] != null)
        {
            $populate_response = $input['populate_response_id'];

        }
        $form_id = $input['form_id'];




        if(isset($input['date']))
        {

            $input['contractor_id'] = Location::find($input['location_id'])->contractor_id;
            $input['user_group_id'] = User::find($input['user_id'])->user_group_id;
            $form_sections = FormVersion::where('form_id',$input['form_id'])->where('active',1)->where('deleted',0)->orderBy('id','desc')->get()->pluck('form_section_id');
            $form = Forms::find($input['form_id']);


            $date = Carbon::parse($input['date']);
            $time = explode(':',$input['time']);
            $date->setTime($time[0],$time[1]);
            $input['date_time'] = $date;



            unset($input['_token']);
            unset($input['date']);
            unset($input['time']);
            $input['title']=$form->name;

            $jobGroup = JobGroup::create($input);



            $input['job_group_id'] = $jobGroup->id;

            $x = [];
            foreach ($form_sections as $form_section) {



                array_push($x,"1");
                $input['form_section_id'] = $form_section;
                //dd($input);
                $job = Jobs::Create($input);
                $link = new LinkedJobGroup();
                $link->old_job_id = $job_id;
                $link->old_job_group_id = $job_group_id;
                $link->new_job_group_id=$jobGroup->id;
                $link->new_job_id = $job->id;
                $link->save();


            }
        }
        unset($input['job_id']);
        unset($input['id']);
        unset($input['version_id']);
        unset($input['form_id']);
        unset($input['form_id2']);
        unset($input['locaton_id']);
        unset($input['job_group_id']);
        unset($input['location_id']);
        unset($input['user_id']);
        unset($input['user_id2']);
        unset($input['date_time']);
        unset($input['date']);
        unset($input['time']);
        unset($input['location_breakdown_id']);
        unset($input['contractor_id']);
        unset($input['user_group_id']);
        unset($input['title']);
        unset($input['form_section_id']);
        unset($input['form_type_id']);
        unset($input['form_type_id']);
        unset($input['response_group_id']);
        unset($input['populate_response_id']);
        unset($input['section_question_id']);



        //dd($input);
        $q =[];
        foreach ($input as $key => $value) {
            $ids = explode('_',$key);
            // dd($ids);
            $response = new Response();
            $response->response_group_id=$responseGroup->id;
            $response->job_id=$job_id;
            $response->user_id=Auth::user()->id;
            if($ids[0]!="attachments")
            {
                if(isset($ids[2]))
                {

                    array_push($q,$ids[2]);
                }

            }

            if($ids[0]=="dropdown")
            {

                $answer = Answers::find($value);

                $response->answer_id=$value;
                $response->form_section_id=$ids[1];
                $response->question_id=$ids[2];
                /*   if($populate_response)
                                      {
                                  $populate = PopulateResponse::where('pre_populate_id',$populate_response)->where('form_section_id',$ids[1])->where('question_id',$ids[2])->where('active',1)->where('deleted',0)->get()->last();
                                  $populate->answer_id =$value;
                                  $populate->update();
                                    }*/
                // $response->answer_text = array('field_type'=>$ids[0],'selected_id'=>$value,'text'=>$answer->answer);
                if($complete)
                {
                    $response->score = $answer->score;
                    if($answer->grade==1)
                    {
                        $response->pass=true;
                        $passCount++;
                    }else if($answer->grade==0)
                    {
                        $response->fail=true;
                        $failCount++;

                    }


                    $responseGroupScore+=$response->score;
                }
                $response->save();
                unset($input[$key]);
            }else if($ids[0]=="checkbox")
            {

                if(is_array($value))
                {

                    foreach ($value as  $next)
                    {
                        $response = new Response();
                        $response->response_group_id=$responseGroup->id;
                        $response->job_id=$job_id;
                        $response->user_id=Auth::user()->id;
                        $answer = Answers::find($next);

                        $response->answer_id=$next;
                        $response->form_section_id=$ids[1];
                        $response->question_id=$ids[2];
                        /*if($populate_response)
                        {
                    $populate = PopulateResponse::where('pre_populate_id',$populate_response)->where('form_section_id',$ids[1])->where('question_id',$ids[2])->where('active',1)->where('deleted',0)->get()->last();
                    $populate->answer_id =$next;
                    $populate->update();
                      }*/

                        //	$response->answer_text = array('field_type'=>$ids[0],'selected_id'=>$value,'text'=>$answer->answer);
                        if($complete)
                        {
                            $response->score = $answer->score;
                            if($answer->grade==1)
                            {
                                $response->pass=true;
                                $passCount++;
                            }else if($answer->grade==0)
                            {
                                $response->fail=true;
                                $failCount++;

                            }

                            $responseGroupScore+=$response->score;
                        }
                        $response->save();
                        unset($input[$key]);

                    }

                }

                else
                {
                    $answer = Answers::find($value);

                    $response->answer_id=$value;
                    $response->form_section_id=$ids[1];
                    $response->question_id=$ids[2];
                    /*   if($populate_response)
                                          {
                                      $populate = PopulateResponse::where('pre_populate_id',$populate_response)->where('form_section_id',$ids[1])->where('question_id',$ids[2])->where('active',1)->where('deleted',0)->get()->last();
                                      $populate->answer_id =$value;
                                      $populate->update();
                                        }*/
                    // $response->answer_text = array('field_type'=>$ids[0],'selected_id'=>$value,'text'=>$answer->answer);
                    if($complete)
                    {
                        $response->score = $answer->score;
                        if($answer->grade==1)
                        {
                            $response->pass=true;
                            $passCount++;
                        }else if($answer->grade==0)
                        {
                            $response->fail=true;
                            $failCount++;

                        }
                        $responseGroupScore+=$response->score;

                    }
                    $response->save();
                    unset($input[$key]);

                }
            }

            else if($ids[0]=="multi")
            {
                if(is_array($value))
                {
                    foreach ($value as  $next)
                    {
                        $response = new Response();
                        $response->response_group_id=$responseGroup->id;
                        $response->job_id=$job_id;
                        $response->user_id=Auth::user()->id;
                        $answer = Answers::find($next);

                        $response->answer_id=$next;
                        $response->form_section_id=$ids[1];
                        $response->question_id=$ids[2];
                        /*if($populate_response)
                                   {
                               $populate = PopulateResponse::where('pre_populate_id',$populate_response)->where('form_section_id',$ids[1])->where('question_id',$ids[2])->where('active',1)->where('deleted',0)->get()->last();
                               $populate->answer_id =$next;
                               $populate->update();
                                 }*/
                        // 	$response->answer_text = array('field_type'=>$ids[0],'selected_id'=>$value,'text'=>$answer->answer);
                        if($complete)
                        {
                            $response->score = $answer->score;
                            if($answer->grade==1)
                            {
                                $response->pass=true;
                                $passCount++;
                            }else if($answer->grade==0)
                            {
                                $response->fail=true;
                                $failCount++;

                            }


                            $responseGroupScore+=$response->score;
                        }
                        $response->save();
                        unset($input[$key]);

                    }
                }
                else
                {
                    $answer = Answers::find($value);

                    $response->answer_id=$value;
                    $response->form_section_id=$ids[1];
                    $response->question_id=$ids[2];
                    /*   if($populate_response)
                                          {
                                      $populate = PopulateResponse::where('pre_populate_id',$populate_response)->where('form_section_id',$ids[1])->where('question_id',$ids[2])->where('active',1)->where('deleted',0)->get()->last();
                                      $populate->answer_id =$value;
                                      $populate->update();
                                        }*/
                    // $response->answer_text = array('field_type'=>$ids[0],'selected_id'=>$value,'text'=>$answer->answer);
                    if($complete)
                    {
                        $response->score = $answer->score;
                        if($answer->grade==1)
                        {
                            $response->pass=true;
                            $passCount++;
                        }else if($answer->grade==0)
                        {
                            $response->fail=true;
                            $failCount++;

                        }


                        $responseGroupScore+=$response->score;
                    }
                    $response->save();
                    unset($input[$key]);

                }

            }else if($ids[0]=="radio")
            {
                $answer = Answers::find($value);
                //dd($value);
                $response->answer_id=$value;
                $response->form_section_id=$ids[1];
                $response->question_id=$ids[2];
                /*   if($populate_response)
                                      {
                                  $populate = PopulateResponse::where('pre_populate_id',$populate_response)->where('form_section_id',$ids[1])->where('question_id',$ids[2])->where('active',1)->where('deleted',0)->get()->last();
                                  $populate->answer_id =$value;
                                  $populate->update();
                                    }*/
                //$response->answer_text = array('field_type'=>$ids[0],'selected_id'=>$value,'text'=>$answer->answer);
                if($complete)
                {
                    $response->score = $answer->score;
                    if($answer->grade==1)
                    {
                        $response->pass=true;
                        $passCount++;
                    }else if($answer->grade==0)
                    {
                        $response->fail=true;
                        $failCount++;

                    }


                    $responseGroupScore+=$response->score;

                }
                $response->save();
                unset($input[$key]);

            }else if($ids[0]!="comment" && $ids[0]!="attachments" )
            {

                if(count($ids)>1)
                {

                    $response->form_section_id=$ids[0];
                    $response->question_id=$ids[1];
                    $response->answer_text =$value;
                    /*if($populate_response)
                                       {
                                   $populate = PopulateResponse::where('pre_populate_id',$populate_response)->where('form_section_id',$ids[1])->where('question_id',$ids[2])->where('active',1)->where('deleted',0)->get()->last();
                                   $populate->answer_text =$value;
                                   $populate->update();
                                     }*/
                    $response->save();
                }
                unset($input[$key]);



            }

        }

        // dd($input);

        foreach ($input as $key => $value) {
            $ids = explode('_',$key);
            if($ids[0]=="comment")
            {
                $response = Response::where('form_section_id',$ids[1])->where('question_id',$ids[2]);
                $response->update(['comments'=>$value]);
            }
            else if($ids[0]=="attachments")
            {
                $time = time();
                $file = $request->file($key);
                $in = $file->getClientOriginalName().'.'.$time.'.'.$file->getClientOriginalExtension();
                $destinationPath = public_path('uploads/response');
                $file->move($destinationPath, $in);
                $response = Response::where('form_section_id',$ids[1])->where('question_id',$ids[2])->where('response_group_id',$responseGroup->id);
                //dd($response->get());
                $response->update(['file_name'=>$in]);

            }


        }












        if($complete)
        {
            //dd($q);
            $q = array_unique($q);
            foreach ($q as $key => $value) {
                $questions = Questions::with('answer_group.answer','answer_type')->find($value);
                if($questions->answer_type->name=="checkbox" || $questions->answer_type->name=="multi" )
                {
                    //dd($questions->answer_type->name);
                    $totalScoreGroup+=(int)$questions->answer_group->answer->sum('score');

                }
                if($questions->answer_type->name=="radio" || $questions->answer_type->name=="dropdown" )
                {
                    $totalScoreGroup+=(int)$questions->answer_group->answer->max('score');


                }

            }

            $responseGroup->total_score = $totalScoreGroup;
            $responseGroup->score = $responseGroupScore;
            $responseGroup->pass_count = $passCount;
            $responseGroup->fail_count = $failCount;
            $responseGroup->update();
        }
        Jobs::find($job_id)->update(['completed'=>2]);
        /*  $input['date'] = Carbon::parse($input['date'])->toDateString();
           unset($input['_token']);
           unset($input['file']);
           audit_log($input,Config::get('constants.store'),$this->page_name,$this->tab_name,$jobGroup->id,'',$jobGroup->id);*/
        if($complete)
        {
            return response()->json(['message'=>'Form successfully completed']);
        }
        else
        {
            return response()->json(['message'=>'Form successfully saved']);

        }
    }


    public function replicate(Request $request)
    {
        $job = Jobs::find($request->job_id);
        //dd($job);
        $data=FormSections::where('id',$job->form_section_id)->with('questions')->first()->toArray();
        // dd($data);
        $questions=$data['questions'];
        $order=$data['order'];
        unset($data['id']);
        unset($data['questions']);
        $form_section=FormSections::create($data);
        foreach ($questions as $key => $value) {
            $value['section_id']=$form_section->id;
            $value['active']=2;
            $value['deleted']=2;
            unset($value['id']);
            Questions::create($value);
        }
        $newJob = $job->replicate();
        $newJob->form_section_id = $form_section->id;
        $newJob->save();
        //dd($newJob);
        return redirect()->back();
    }


    public function responseForm(Request $request)
    {
        $input=$request->all();
        $in=null;
        if($request->file())

        {
            $time = time();
            $file = $request->file('ov_attachments');
            $in = $file->getClientOriginalName().'.'.$time.'.'.$file->getClientOriginalExtension();
            $destinationPath = public_path('uploads/response');
            $file->move($destinationPath, $in);
        }

        Forms::find($request->form_id)->update(['ov_file'=>$in,'ov_note'=>$request->ov_note]);

        return response()->json(['message'=>' over all Form successfully saved']);

    }



    public function multipleDelete(Request $request){

        JobGroup::whereIn('id',$request->ids)->update(['active'=>0]);
        return response()->json(['message'=>'Deleted']);

    }

    public function testMail(Request $request){

        $data = array('message'=>'message','title'=>'title','link'=>'link','name'=>'aalisan');

        return view('mail_jobs')->with('data',$data);

    }


}
