<?php
namespace App\Http\Controllers\backend;
use App\AppPermissions;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\AppRoles;
use DataTables;
use Auth;
use App\AppPermissionModule;
use App\AppCompanyPermission;
class AppRolesController extends Controller
{

    public function index() {

        return view('backend.app_roles.index');
    }
    public function getallroll(Request $request)
    {
        $data = AppRoles::where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        $data=sortRoleData($data);
        return $this->alldataGet($data);
    }
    public function alldataGet($data)
    {
        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                $edit = '<a onClick="getapproledata( ' . $data->id . ')" id="' . $data->id . '" class="btn btn-primary btn-xs" href=""  data-toggle="modal" data-target="#modalEditForm"><i class="fas fa-pencil-alt"></i></a>';
                if(Auth()->user()->hasPermissionTo('edit_roles') || Auth::user()->all_companies == 1)
                {
                    $response = $response . $edit;
                }
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    public function edit(Request $request) {
        $role = AppRoles::where('id',$request->id)->first();
        $permission_group_module=AppPermissionModule::with('appPermissionModuleId')->where('active',1)->where('deleted',0)->get();
        return view('backend.app_roles.edit_new', compact('role', 'permission_group_module'));
    }


    public function updateAjax(Request $request)
    {
        $company_id=Auth::user()->default_company;
        $input= $request->all();

        $input['company_id']=$company_id;
        $permission=AppCompanyPermission::where('company_id',$company_id)->where('app_permission_id',$request->app_permission_id)->where('app_role_id',$request->app_role_id)->first();
        if($request->flag == 1) {
            if (empty($permission)){
                AppCompanyPermission::create($input);
            }else{
                AppCompanyPermission::where('id',$permission->id)->update($input);
            }
            $flag='1';
        } else {
            AppCompanyPermission::where('id',$permission->id)->delete();
            $flag='0';
        }
        return response()->json(['message'=>'Successfully Update !!','flag'=>$flag]);
    }
}
