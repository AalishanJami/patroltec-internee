<?php

namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Audits;
use Auth;
use Illuminate\Support\Facades\Schema;
use DB;
use DataTables;
use App\Company;
use Config;
class AuditController extends Controller
{
  public function __construct() {
    remove_key();
  }
  public function index()
  {
    $companies=Company::getCompanies();
  	return view('backend.audit.index')->with(['companies'=>$companies]);
  }
  public function getAll(){
    $data=Audits::where('company_id',Auth::user()->default_company)->orderBy('id', 'desc')->with('audit_fields','user');
  	return Datatables::of($data)
        ->addColumn('name', function($data){
        $response='';
        if (isset($data->user->name)){
          $response = $data->user->name;
        }
        return $response;
        })->addColumn('old_values', function($data){
           	$response='<ul>';
            if($data->action != Config::get('constants.restore'))
            {
              if(count($data->audit_fields)>0)
              {
               	foreach($data->audit_fields as $key_old=>$value_old)
               	{
                  $temp_value=$value_old->old_value;
               	  $response=$response.'<li>'.$value_old->cms_value. ' : '.$temp_value.'</li>';
               	}
              }
            }
           	$response==$response.'</ul>';
            return $response;
        })->addColumn('new_values', function($data){
           	$response='<ul>';
            if($data->action != Config::get('constants.delete'))
            {
              if(count($data->audit_fields)>0)
              {
                foreach($data->audit_fields as $key_old=>$value_old)
                {
                  $temp_value=$value_old->new_value;
                  $response=$response.'<li>'.$value_old->cms_value. ' : '.$temp_value.'</li>';
                }
              }
            }
           	$response=$response.'</ul>';
           	return $response;
        })->addColumn('date', function($data){
            return $data->created_at->format('d-m-y H:i');
        })->addColumn('url', function($data){
            $cms_data=localization();
            $page=checkKey($data->page_name,$cms_data);
            return $page;
        })->addColumn('tab_name', function($data){
            $cms_data=localization();
            $page=checkKey($data->tab_name,$cms_data);
            return $page;
        })->addColumn('event', function($data){
            $cms_data=localization();
            $page=checkKey($data->action,$cms_data);
            return $page;
        })->rawColumns(['old_values','new_values'])->make(true);
  }
}
