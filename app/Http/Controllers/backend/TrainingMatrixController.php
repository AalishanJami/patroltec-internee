<?php
namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Company;
use Carbon\Carbon;
use App\User;
use App\DocType;
use App\UserDocType;
use App\Documents;
use App\Exports\employee\EmployeeExport;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Session;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use DataTables;
use Auth;
class TrainingMatrixController extends Controller
{
    public function getAll(Request $request){
        if($request->position == 'all')
        {
            $data =User::getUser()->get();
            $doc_types_colum =UserDocType::getUserType()->get();
        }
        else
        {
            $data =User::getUser()->where('position_id',$request->position)->get();
            $doc_types_colum =UserDocType::getUserType()->where('position_id',$request->position)->get();
        }

        $data=sortData($data);
        $doc_type=DocType::getDocType()->get();
        $datatable = Datatables::of($data)->with(['doc_type'=>$doc_type,'doc_types_colum'=>$doc_types_colum]);
        foreach($doc_type as $value){
            if(isset($value->name))
            {
                $datatable->addColumn($value->name, function($data) use ($value){
                    $position =UserDocType::getUserType()->where('position_id',$data['position_id'])->first();
                    if($position)
                    {
                        $red_color='';
                        foreach ($data['employee_document'] as $length => $document) {
                                $doc_type_id =UserDocType::getUserType()->where('position_id',$data['position_id'])->where('doc_type_id',$document['doc_type_id'])->first();
                            if(isset($document->doc_type->name) && $doc_type_id && $document->doc_type->name == $value->name)
                            {
                               // if($document->enable == 0)
                               // {
                               //     //green
                               //     return 'green';
                               //     // return '<span style="background-color: green;">✓</span>';
                               // } else {
                               //     $date1 = date( "Y-m-d", strtotime($document->expiry) );
                               //     $date2 = date('Y-m-d');
                               //     $diff = abs(strtotime($date2) - strtotime($date1));
                               //     $years = floor($diff / (365*60*60*24));
                               //     $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                               //     if ($months==0){
                               //         return "orange";
                               //     }else{
                               //         return "r_red";
                               //     }
                               // }

                                $expiry1=date( "Y-m-d", strtotime($document->expiry) );
                                $expiry = date("Y-m-d", strtotime( date( "Y-m-d", strtotime( $document->expiry ) ) . "-1 month" ) );
                                $today = date('Y-m-d');
                                if ($expiry>=$today){
                                    return "green";
                                }else if ($expiry<=$today && $expiry1>=$today){
                                    return "orange";
                                }else{
                                    return "r_red";
                                }
                            }
                            else
                            {
                                if($data['employee_document']->count()-1 == $length)
                                {
                                    return 'red';
                                }
                            }
                        }
                    }
                    else
                    {
                        return '1';
                    }
                });
            }
        }
        $datatable->addColumn('checkbox', function($data){
        });
        $datatable->addColumn('actions', function($data){
            $response="";
            if(permisionCheck('edit_employee')){
                $edit ='<a href="'.url("/employee/detail/".$data->id).'" class="btn btn-primary btn-xs mr-2"><i class="fas fa-pencil-alt"></i></a>';
                $response=$response.$edit;
            }
            return $response;
        });
        return $datatable->rawColumns(['actions'])->make(true);

    }
    public function exportPdf(Request $request)
    {
        if($request->position == 'all')
        {
            if($request->pdf_array == 1)
            {
               $data =User::getUser()->get();
            }
            else
            {
                $array=json_decode($request->pdf_array);
                $data =User::getUser()->whereIn('id',$array)->get();
            }
            $doc_types_colum =UserDocType::getUserType()->get();

        }
        else
        {
            if($request->pdf_array == 1)
            {
                $data =User::getUser()->where('position_id',$request->position)->get();
            }
            else
            {
                $array=json_decode($request->pdf_array);
                $data =User::getUser()->whereIn('id',$array)->where('position_id',$request->position)->get();
            }
            $doc_types_colum =UserDocType::getUserType()->where('position_id',$request->position)->get();
        }
        $doc_types_colum=dynamic_colum($doc_types_colum);
        $data=sortData($data);
        $doc_type=DocType::whereIn('id',$doc_types_colum)->where('active',1)->where('deleted',0)->orderBy('name', 'asc')->get();
        $companies='Patroltec';
        $reportname='Training Matric';
        $date=\Carbon\Carbon::now()->format('D jS M Y');
        $pdf = PDF::loadView('backend.employee.training_pdf',compact('companies','reportname','data','date','doc_type'));
        return $pdf->download('training_matric.pdf');
    }
}

