<?php
namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use FontLib\Table\Type\name;
use Illuminate\Http\Request;
use App\SiteContacts;
use App\ContactsType;
use Spatie\Permission\Models\Role;
use App\Company;
use App\CompanyUser;
use Auth;
use App\Exports\contact\ContactExport;
use App\Exports\contact\ContactExportSoft;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use PDF;
use DataTables;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Hash;
use Session;
use Validator;
use Config;
class ContactController extends Controller
{
    private $page_name;
    private $tab_name;
    private $db;
    private $cms_array=[];
    public function __construct() {
        $this->page_name ='contact';
        $this->tab_name ='project';
        $this->db='site_contacts';
        $cms_data=localization();
        $temp_array=[];
        $temp_array['contacts_type_id']['key']='contact_type';
        $temp_array['contacts_type_id']['value']=checkKey('contact_type',$cms_data);
        $temp_array['title']['key']='title';
        $temp_array['title']['value']=checkKey('title',$cms_data);
        $temp_array['first_name']['key']='first_name';
        $temp_array['first_name']['value']=checkKey('first_name',$cms_data);
        $temp_array['surname']['key']='surname';
        $temp_array['surname']['value']=checkKey('surname',$cms_data);
        $temp_array['email']['key']='email';
        $temp_array['email']['value']=checkKey('email',$cms_data);
        $temp_array['phone_number']['key']='mobile_number';
        $temp_array['phone_number']['value']=checkKey('mobile_number',$cms_data);
        $temp_array['other_number']['key']='other_number';
        $temp_array['other_number']['value']=checkKey('other_number',$cms_data);

        $temp_array['extension']['key']='extension';
        $temp_array['extension']['value']=checkKey('extension',$cms_data);
        $temp_array['name']['key']='building';
        $temp_array['name']['value']=checkKey('building',$cms_data);
        $temp_array['address_street']['key']='street';
        $temp_array['address_street']['value']=checkKey('street',$cms_data);
        $temp_array['town']['key']='town';
        $temp_array['town']['value']=checkKey('town',$cms_data);
        $temp_array['state']['key']='state';
        $temp_array['state']['value']=checkKey('state',$cms_data);
        $temp_array['country']['key']='country';
        $temp_array['country']['value']=checkKey('country',$cms_data);
        $temp_array['post_code']['key']='post_code';
        $temp_array['post_code']['value']=checkKey('post_code',$cms_data);
        $temp_array['location_id']['key']='project';
        $temp_array['location_id']['value']=checkKey('project',$cms_data);
        $this->cms_array=$temp_array;
    }
    public function index()
    {
        $companies=Company::getCompanies();
        $contactsType=ContactsType::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
    	return view('backend.contact.index')->with(['companies'=>$companies,'contactsType'=>$contactsType]);
    }
    public function contactType()
    {
        $contactsType=ContactsType::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        return response()->json(['contactsType'=>$contactsType]);
    }
   	public function contactModalSave(Request $request)
    {
        // dd($request->all());
        $data = [
            'email'=>'required|email',
        ];
        $validator = Validator::make($request->all(), $data);
        if ($validator->fails())
        {
            $error = $validator->messages();
            $html= "";
            foreach ($error->all() as $key => $value) {
                $html=$value;
            }
            return response()->json(['status'=> false, 'error'=> $html]);
        }
        $input = $request->all();
        $email=$input['email'];
        $exist=SiteContacts::where(['location_id'=>Session::get('project_id'),'active'=>1,'deleted'=>0,'email'=>$email])->first();
        if (!empty($exist)){
            return response()->json(['status'=> false, 'error'=> 'Email already exist']);
        }
        $input['location_id'] = Session::get('project_id');
    	$contact=SiteContacts::create($input);
        audit_log($this->cms_array,audit_log_data($input),Config::get('constants.store'),$this->page_name,$this->tab_name,$contact->id,'',$contact->id,Session::get('project_id'),$this->db);
    	return response()->json("Data Added");
    }

    public function getAllContacts(){
    	$data =SiteContacts::where('company_id',Auth::user()->default_company)->where('location_id',Session::get('project_id'))->where('active',1)->where('deleted',0)->with('getContactType')->orderBy('id', 'desc')->get();
    	return Datatables::of($data)->addColumn('checkbox', function($data){})
            ->addColumn('contacttype', function($data){
                $contacttype='';
                if(isset($data->getContactType->name)) {
                    $contacttype=$data->getContactType->name;
                }
                return $contacttype;
            })
            ->addColumn('actions', function($data){
            $response="";
                if(Auth()->user()->hasPermissionTo('edit_contacts') ||  Auth::user()->all_companies == 1 ) {
                    $edit ='<a  id="' . $data->id . '" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modalContactEdit" onclick="contactEdit('.$data->id.')"><i class="fas fa-pencil-alt"></i></a>';
                }else{
                    $edit ='';
                }

                if(Auth()->user()->hasPermissionTo('delete_contacts') ||  Auth::user()->all_companies == 1 ) {
                    $delete ='<a type="button"class="btn btn-danger btn-xs  all_action_btn_margin my-0" onclick="deleteContact( '.$data->id . ')"><i class="fas fa-trash"></i></a>';
                }else{
                    $delete ='';
                }
            $response=$response.$edit;
            $response=$response.$delete;
            return $response;
            })->rawColumns(['actions'])->make(true);
    }

    public function edit(Request $request)
    {
    	$input = $request->all();
		$data = SiteContacts::where('company_id',Auth::user()->default_company)->where('id',$input['id'])->where('active',1)->get()->last();
        $contactsType=ContactsType::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
    	return response()->json(['data'=>$data,'contactsType'=>$contactsType]);
    }

    public function  update(Request $request)
    {
        $data = [
            'email'=>'required|email',
        ];
        $validator = Validator::make($request->all(), $data);
        if ($validator->fails())
        {
            $error = $validator->messages();
            $html= "";
            foreach ($error->all() as $key => $value) {
                $html=$value;
            }
            return response()->json(['status'=> false, 'error'=> $html]);
        }

    	$input = $request->all();
        unset($input['country']);
        $contact=SiteContacts::find($input['id']);
        audit_log($this->cms_array,audit_log_data($input),Config::get('constants.update'),$this->page_name,$this->tab_name,$contact->id,audit_log_data($contact->toArray()),$contact->id,Session::get('project_id'),$this->db);
        $contact->update($input);
       	return response()->json(['message'=>'Contact Updated Successfully']);
    }

    public function delete(Request $request)
    {
        audit_log($this->cms_array,'',Config::get('constants.delete'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('project_id'),$this->db);
        SiteContacts::where('id',$request->id)->update(['active'=>0]);
        return response()->json(['message'=>'delete']);

    }
    public function multipleDelete(Request $request)
    {
    	if($request->flag==1)
    	{
            SiteContacts::whereIn('id',$request->ids)->update(['active'=>0]);
            return response()->json(['message'=>'Deleted']);
    	}else if ($request->flag==2)
    	{
    		SiteContacts::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>1]);
            return response()->json(['message'=>'Restore Deleted']);
    	}
    }

    public function getAllSoftContacts()
    {
		$data =SiteContacts::where('company_id',Auth::user()->default_company)->where('location_id',Session::get('project_id'))->with('getContactType')->where('active',0)->where('deleted',0)->orderBy('id', 'desc')->get();
        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {
            })->addColumn('actions', function ($data) {
           $response = "";
                if(Auth()->user()->hasPermissionTo('restore_delete_contacts') ||  Auth::user()->all_companies == 1 ){
                    $Restore_button = '<a href="javascript:;" class="btn btn-success all_action_btn_margin btn-xs my-0" onclick="restoreContact( ' . $data->id . ')"><i class="fa fa-retweet" aria-hidden="true"></i></button>';
                }else{
                    $Restore_button = '';
                }
                if(Auth()->user()->hasPermissionTo('hard_delete_contacts') ||  Auth::user()->all_companies == 1 ){
                    $hard_delete_button = '<a href="javascript:;" class="btn btn-danger btn-xs my-0" onclick="hardDeleteContact( ' . $data->id . ')"><i class="fa fa-trash" aria-hidden="true"></i></button>';
                }else{
                    $hard_delete_button = '';
                }
                $response = $response . $hard_delete_button;
                $response = $response . $Restore_button;
            return $response;
        })->addColumn('contacttype', function($data){
                if(!empty($data->getContactType)){
                    return $data->getContactType->name;
                }else{
                    return "";
                }
            })
        ->rawColumns(['actions'])
        ->make(true);
    }
    public function restore(Request $request)
    {
        audit_log($this->cms_array,'',Config::get('constants.restore'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('project_id'),$this->db);
        SiteContacts::where('id',$request->id)->update(['active'=>1]);
        return response()->json(['message'=>'restore']);
    }
    public function hardDelete(Request $request)
    {
        audit_log($this->cms_array,'',Config::get('constants.soft_delete'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('project_id'),$this->db);
        SiteContacts::where('id',$request->id)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'hard delete']);
    }
    public function exportExcel(Request $request)
    {
        if($request->export_excel_contact == 1)
        {
            if($request->excel_array == 1)
            {
                $data=SiteContacts::where('location_id',Session::get('project_id'))->where('active',1)->where('deleted',0)->get();
                return Excel::download(new ContactExport($data), 'contacts.csv');
            }
            else
            {
                $array=json_decode($request->excel_array);
                $data=SiteContacts::where('location_id',Session::get('project_id'))->whereIn('id',$array)->where('active',1)->where('deleted',0)->get();
                return Excel::download(new ContactExport($data), 'contacts.csv');
            }
        }
        else
        {
            return Excel::download(new ContactExportSoft, 'contacts.csv');
            //for soft delete
        }

    }

    public function exportPdf(Request $request)
    {
       // dd($request->export_pdf_contact);
        if($request->pdf_array == 1)
        {
           $data= SiteContacts::where('location_id',Session::get('project_id'))->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->pdf_array);
            $data=SiteContacts::where('location_id',Session::get('project_id'))->whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        $companies='Patroltec';
        $reportname='Contact';
        $date=\Carbon\Carbon::now()->format('D jS M Y');
        $pdf = PDF::loadView('backend.contact.pdf',compact('companies','reportname','data','date'));
        return $pdf->download('contact.pdf');
    }

    public function exportWord(Request $request)
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        if($request->word_array == 1)
        {
            $data= SiteContacts::where('location_id',Session::get('project_id'))->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->word_array);
            $data=SiteContacts::where('location_id',Session::get('project_id'))->whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        $table = $section->addTable();
        foreach ($data as $key => $value) {
            if($key==0)
            {
                $table->addRow();
                if(Auth()->user()->hasPermissionTo('contacts_title_word_export') ||  Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Title');
                }
                if(Auth()->user()->hasPermissionTo('contacts_firstname_word_export') ||  Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('First Name');
                }
                if(Auth()->user()->hasPermissionTo('contacts_surname_word_export') ||  Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Surname');
                }
                if(Auth()->user()->hasPermissionTo('contacts_email_word_export') ||  Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Email');
                }
                if(Auth()->user()->hasPermissionTo('contacts_phonenumber_word_export') ||  Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Phone Number');
                }
                if(Auth()->user()->hasPermissionTo('contacts_postcode_word_export') ||  Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Post Code');
                }
            }
            $table->addRow();
            if(Auth()->user()->hasPermissionTo('contacts_title_word_export') ||  Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText($value->title);
            }
            if(Auth()->user()->hasPermissionTo('contacts_firstname_word_export') ||  Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText($value->first_name);
            }
            if(Auth()->user()->hasPermissionTo('contacts_surname_word_export') ||  Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText($value->surname);
            }
            if(Auth()->user()->hasPermissionTo('contacts_email_word_export') ||  Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText($value->email);
            }
            if(Auth()->user()->hasPermissionTo('contacts_phonenumber_word_export') ||  Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText($value->phone_number);
            }
            if(Auth()->user()->hasPermissionTo('contacts_postcode_word_export') ||  Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText($value->post_code);
            }
        }

        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('contact.docx');
        return response()->download(public_path('contact.docx'));
    }


}
