<?php
namespace App\Http\Controllers\backend;
use App\Exports\support\SupportExport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Mail;
use Auth;
use DataTables;
use App\Company;
use App\Support;
use PDF;
use Config;
use Carbon\Carbon;
class SupportController extends Controller
{
    private $page_name;
    private $tab_name;
    private $db;
    private $cms_array=[];
    public function __construct() {
        $this->page_name ='support';
        $this->tab_name ='support';
        remove_key();
        $this->db='supports';
        $cms_data=localization();
        $temp_array=[];
        $temp_array['title']['key']='title';
        $temp_array['title']['value']=checkKey('title',$cms_data);
        $temp_array['message']['key']='description';
        $temp_array['message']['value']=checkKey('description',$cms_data);
        $this->cms_array=$temp_array;
    }
    public function index()
    {
        $companies=Company::getCompanies();
    	return view('backend.support.index')->with(['companies'=>$companies]);
    }
    public function getAll()
    {
        $data=Support::where('company_id',Auth::user()->default_company)->with('user')->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                if(Auth()->user()->hasPermissionTo('edit_emailSupport') || Auth::user()->all_companies == 1 ){
                    $edit = '<a onClick="getSupportdata( ' . $data->id . ')" id="' . $data->id . '" class="btn btn-primary btn-xs" href=""  data-toggle="modal" data-target="#modalEditForm"><i class="fas fa-pencil-alt"></i></a>';
                }else{
                    $edit = '';
                }
                if(Auth()->user()->hasPermissionTo('delete_emailSupport') || Auth::user()->all_companies == 1 ){
                    $delete = '<a type="button"class="btn btn-danger all_action_btn_margin btn-xs my-0" onclick="deleteSupportdata( ' . $data->id . ')"><i class="fas fa-trash"></i></a>';
                }else{
                    $delete ='';
                }
                $response = $response . $edit;
                $response = $response . $delete;
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);


    }
    public function getAllSoft()
    {
        $data=Support::where('company_id',Auth::user()->default_company)->with('user')->where('active',0)->where('deleted',0)->orderBy('id', 'desc')->get();
        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                if(Auth()->user()->hasPermissionTo('edit_emailSupport') || Auth::user()->all_companies == 1 ){
                    $restore = '<a onClick="restoreSupportData( ' . $data->id . ')" id="' . $data->id . '" class="btn btn-primary btn-xs" href=""  data-toggle="modal" data-target="#modalEditForm"><i class="fa fa-retweet"></i></a>';
                }else{
                    $restore ='';
                }
                if(Auth()->user()->hasPermissionTo('hard_delete_emailSupport') || Auth::user()->all_companies == 1 ){
                    $delete = '<a type="button"class="btn btn-danger all_action_btn_margin btn-xs my-0" onclick="harddeleteSupportdata( ' . $data->id . ')"><i class="fas fa-trash"></i></a>';
                }else{
                    $delete ='';
                }
                $response = $response . $restore;
                $response = $response . $delete;
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    public function send(Request $request)
    {
        $input=$request->all();
        $data=array();
        $data['title']=$request->title;
        $data['text_message']=$request->message;
        Mail::send('backend.support.send_mail', $data, function($message) use ($data) {
            $message->to('admin@xweb4u.com', 'Admin')->subject('Patroltec Customer Support')->from('admin@xweb4u.com','Admin');
        });

        $input['user_id']=Auth::user()->id;
        $supportdata=Support::create($input);
        unset($input['_token']);
        audit_log($this->cms_array,$input,Config::get('constants.store'),$this->page_name,$this->tab_name,$supportdata->id,'',$supportdata->id,'',$this->db);
        toastr()->success('Email send Successfully !');
        return redirect('/support');
    }
    public function edit(Request $request)
    {
        $data=Support::where('id',$request->id)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->first();
         return response()->json($data);
    }

    public function delete(Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.delete'),$this->page_name,$this->tab_name,$request->id,'',$request->id,'',$this->db);
        Support::where('id',$request->id)->update(['active'=>0]);
        return response()->json(['message'=>'delete']);
    }

    public function deleteMultipleSOft(Request $request)
    {
        Support::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function deleteMultiple(Request $request)
    {
        Support::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }

    public function restore(Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.restore'),$this->page_name,$this->tab_name,$request->id,'',$request->id,'',$this->db);
        Support::where('id',$request->id)->update(['active'=>1]);
        return response()->json(['message'=>'delete']);
    }
    public function restoreMutilple(Request $request)
    {
        Support::whereIn('id',$request->ids)->update(['active'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function harddelete(Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.soft_delete'),$this->page_name,$this->tab_name,$request->id,'',$request->id,'',$this->db);
        Support::where('id',$request->id)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'delete']);
    }

    public function exportPdf(Request $request)
    {
        if($request->pdf_array == 1)
        {
            $data=Support::where('company_id',Auth::user()->default_company)->with('user')->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->pdf_array);
            $data=Support::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->with('user')->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        $date=Carbon::now()->format('D jS M Y');
        $companies='Patroltec';
        $reportname='Support';
        $pdf = PDF::loadView('backend.support.pdf',compact('data','date','reportname','companies'));
        return $pdf->download('support.pdf');
    }

    public function exportWord(Request $request)
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();

        if($request->word_array == 1)
        {
            $data=Support::where('company_id',Auth::user()->default_company)->with('user')->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->word_array);
            $data=Support::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->with('user')->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        $text = $section->addText('Word Export data');
        $table = $section->addTable();
        foreach ($data as $key => $value) {
            if($key==0) {
                $table->addRow();
                if(Auth()->user()->hasPermissionTo('emailSupport_ticketid_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Ticket ID');
                }else{
                    $table->addCell(1750)->addText('');
                }
                if(Auth()->user()->hasPermissionTo('emailSupport_username_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('User Name');
                }else{
                    $table->addCell(1750)->addText('');
                }
                if(Auth()->user()->hasPermissionTo('emailSupport_title_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Title');
                }else{
                    $table->addCell(1750)->addText('');
                }
                if(Auth()->user()->hasPermissionTo('emailSupport_message_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Message');
                }else{
                    $table->addCell(1750)->addText('');
                }
                if(Auth()->user()->hasPermissionTo('emailSupport_senddate_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Send Date');
                }else{
                    $table->addCell(1750)->addText('');
                }
            }
            $table->addRow();
            if(Auth()->user()->hasPermissionTo('emailSupport_ticketid_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText(strip_tags($value->id));
            }else{
                $table->addCell(1750)->addText('');
            }
            if(Auth()->user()->hasPermissionTo('emailSupport_username_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText(strip_tags($value->user_id));
            }else{
                $table->addCell(1750)->addText('');
            }
            if(Auth()->user()->hasPermissionTo('emailSupport_title_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText(strip_tags($value->title));
            }else{
                $table->addCell(1750)->addText('');
            }
            if(Auth()->user()->hasPermissionTo('emailSupport_message_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText(strip_tags($value->message));
            }else{
                $table->addCell(1750)->addText('');
            }
            if(Auth()->user()->hasPermissionTo('emailSupport_senddate_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText(strip_tags($value->created_at));
            }else{
                $table->addCell(1750)->addText('');
            }

        }
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('support.docx');
        return response()->download(public_path('email.docx'));
    }

    public function exportExcel(Request $request)
    {
        if($request->excel_array == 1)
        {
            $data=Support::where('company_id',Auth::user()->default_company)->with('user')->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->excel_array);
            $data=Support::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->with('user')->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        return Excel::download(new SupportExport($data), 'support.csv');
    }
}
