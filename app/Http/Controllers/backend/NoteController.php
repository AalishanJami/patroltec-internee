<?php

namespace App\Http\Controllers\backend;
use App\Exports\note\NoteExport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Company;
use App\Notes;
use Auth;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use PDF;
use DataTables;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Hash;
use Session;
use Validator;
use Config;
class NoteController extends Controller
{
    private $page_name;
    private $tab_name;
    private $db;
    private $cms_array=[];
    public function __construct() {
        $this->page_name ='notes';
        $this->tab_name ='notes';
        $this->db='notes';
        $cms_data=localization();
        $temp_array=[];
        $temp_array['title']['key']='title';
        $temp_array['title']['value']=checkKey('title',$cms_data);
        $temp_array['note']['key']='notes';
        $temp_array['note']['value']=checkKey('notes',$cms_data);
        $this->cms_array=$temp_array;
    }
    public function store(Request $request)
    {
        $input = $request->all();
        if($input['flag']==1)
        {
            $input['link_id'] = 1;
            $input['link_name'] = "client";
            $this->tab_name ='client';

        }
        else if($input['flag']==2)
        {
            $input['link_id'] = 2;
            $input['link_name'] = "document";
            $this->tab_name ='document_library';
        }
         else if($input['flag']==3)
        {
            $input['link_id'] = 3;
            $input['link_name'] = "employee";
            $this->tab_name ='employees';
        }

        $input['company_id'] = Auth::user()->default_company;
        $note=Notes::create($input);
        unset($input['link_id']);
        unset($input['link_name']);
        unset($input['flag']);
        audit_log($this->cms_array,audit_log_data($input),Config::get('constants.store'),$this->page_name,$this->tab_name,$note->id,'',$note->id,'',$this->db);
        return response()->json(['message'=>'Data Added Successfully ..!!','flag'=>1]);
    }

    public function getAll(Request $request)
    {
       $data =Notes::where('company_id',Auth::user()->default_company)->where('active',1)->where('link_id',$request->flag)->where('deleted',0)->orderBy('id', 'desc')->get();
       return Datatables::of($data)->addColumn('checkbox', function($data){})
            ->addColumn('note', function($data){
                return strip_tags($data->note);
            })
            ->addColumn('actions',function($data){
                $response="";
                $edit ='<a  id="' . $data->id . '" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modalNoteEdit" onclick="noteEdit('.$data->id.')"><i class="fas fa-pencil-alt"></i></a>';
                $delete ='<a class="btn btn-danger all_action_btn_margin btn-xs my-0" onclick="deleteNote( '.$data->id . ')"><i class="fas fa-trash"></i></a>';

                if($data->link_id == 3)
                {
                    if(Auth()->user()->hasPermissionTo('edit_employeeNotes') || Auth::user()->all_companies == 1)
                    {
                        $response=$response.$edit;
                    }
                    if(Auth()->user()->hasPermissionTo('delete_employeeNotes') || Auth::user()->all_companies == 1)
                    {
                        $response=$response.$delete;
                    }
                }
                elseif($data->link_id == 2)
                {
                    if(Auth()->user()->hasPermissionTo('edit_documentNotes') || Auth::user()->all_companies == 1)
                    {
                        $response=$response.$edit;
                    }
                    if(Auth()->user()->hasPermissionTo('delete_documentNotes') || Auth::user()->all_companies == 1)
                    {
                        $response=$response.$delete;
                    }
                }
                elseif($data->link_id == 1)
                {
                    if(Auth()->user()->hasPermissionTo('edit_clientNotes') || Auth::user()->all_companies == 1)
                    {
                        $response=$response.$edit;
                    }
                    if(Auth()->user()->hasPermissionTo('delete_clientNotes') || Auth::user()->all_companies == 1)
                    {
                        $response=$response.$delete;
                    }
                }
                return $response;
            })->rawColumns(['actions','note'])->make(true);
    }

    public function getAllSoft(Request $request)
    {
        $data =Notes::where('company_id',Auth::user()->default_company)->where('active',0)->where('link_id',$request['flag'])->where('deleted',0)->orderBy('id', 'desc')->get();
        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {
            })->addColumn('actions', function ($data) {
               $response = "";
                    $restore_button = '<a href="javascript:;" class="btn btn-success all_action_btn_margin btn-xs my-0" onclick="restoreNote( ' . $data->id . ')"><i class="fa fa-retweet" aria-hidden="true"></i></button>';
                    $hard_delete_button = '<a href="javascript:;" class="btn  btn-danger btn-xs my-0" onclick="hardDeleteNote( ' . $data->id . ')"><i class="fa fa-trash" aria-hidden="true"></i></button>';
                    if($data->link_id == 3)
                    {
                        if(Auth()->user()->hasPermissionTo('hard_delete_employeeNotes') || Auth::user()->all_companies == 1)
                        {
                            $response = $response . $hard_delete_button;
                        }
                        if(Auth()->user()->hasPermissionTo('restore_delete_employeeNotes') || Auth::user()->all_companies == 1)
                        {
                            $response = $response . $restore_button;
                        }
                    }
                    elseif($data->link_id == 2)
                    {
                        if(Auth()->user()->hasPermissionTo('hard_delete_employeeNotes') || Auth::user()->all_companies == 1)
                        {
                            $response = $response . $hard_delete_button;
                        }
                        if(Auth()->user()->hasPermissionTo('restore_delete_employeeNotes') || Auth::user()->all_companies == 1)
                        {
                            $response = $response . $restore_button;
                        }
                    }
                    elseif($data->link_id == 1)
                    {
                        if(Auth()->user()->hasPermissionTo('hard_delete_employeeNotes') || Auth::user()->all_companies == 1)
                        {
                            $response = $response . $hard_delete_button;
                        }
                        if(Auth()->user()->hasPermissionTo('restore_delete_employeeNotes') || Auth::user()->all_companies == 1)
                        {
                            $response = $response . $restore_button;
                        }
                    }
                return $response;
            })
            ->rawColumns(['actions','note'])
            ->make(true);
    }

    public function edit(Request $request)
    {
        $input = $request->all();
        $data = Notes::where('id',$input['id'])->where('active',1)->get()->last();
        return response()->json($data);
    }
    public function flagCheck($flag)
    {
        if($flag==1)
        {
            $this->tab_name ='client';
        }
        else if($flag==2)
        {
            $this->tab_name ='document_library';
        }
        else if($flag==3)
        {
            $this->tab_name ='employees';
        }
    }
    public function  update(Request $request)
    {
        $input = $request->all();
        $notes=Notes::find($input['id']);
        $this->flagCheck($notes->link_id);
        audit_log($this->cms_array,audit_log_data($input),Config::get('constants.update'),$this->page_name,$this->tab_name,$notes->id,audit_log_data($notes->toArray()),$notes->id,'',$this->db);
        $notes->update($input);
        // Notes::where('id',$input['id'])->update($input);
        return response()->json(['message'=>'Data Updated Successfully ..!!','flag'=>1]);
    }
    public function delete(Request $request)
    {
        $notes=Notes::find($request->id);
        $this->flagCheck($notes->link_id);
        audit_log($this->cms_array,'',Config::get('constants.delete'),$this->page_name,$this->tab_name,'','',$request->id,'',$this->db);
        $notes->update(['active'=>0]);
        return response()->json(['message'=>'delete','flag'=>1]);
    }
    public function active(Request $request)
    {
        $notes=Notes::find($request->id);
        $this->flagCheck($notes->link_id);
        audit_log($this->cms_array,'',Config::get('constants.restore'),$this->page_name,$this->tab_name,'','',$request->id,'',$this->db);
        $notes->update(['active'=>1]);
        return response()->json(['message'=>'restore']);
    }
    public function softDelete(Request $request)
    {
        $notes=Notes::find($request->id);
        $this->flagCheck($notes->link_id);
        audit_log($this->cms_array,'',Config::get('constants.soft_delete'),$this->page_name,$this->tab_name,'','',$request->id,'',$this->db);
        $notes->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'hard delete','flag'=>1]);
    }
    public function deleteMultipleSOft(Request $request)
    {
        Notes::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function deleteMultiple(Request $request)
    {
        Notes::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function activeMultiple(Request $request)
    {
        Notes::whereIn('id',$request->ids)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }

    public function exportPdf(Request $request)
    {
        if($request->pdf_array == 1)
        {
           $data =Notes::where('company_id',Auth::user()->default_company)->where(['active'=>1,'deleted'=>0,'link_id'=>$request->link_id,'link_name'=>$request->link_name])->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->pdf_array);
            $data= Notes::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->where(['active'=>1,'deleted'=>0,'link_id'=>$request->link_id,'link_name'=>$request->link_name])->orderBy('id', 'desc')->get();
        }
        $flag=$request->link_id;
        $companies='Patroltec';
        $reportname='Notes';
        $date=\Carbon\Carbon::now()->format('D jS M Y');
        $pdf = PDF::loadView('backend.notes.pdf',compact('companies','reportname','data','date','flag'));
        return $pdf->download('notes.pdf');
    }

    public function exportWorld(Request $request){
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        if($request->word_array == 1)
        {
            $data =Notes::where('company_id',Auth::user()->default_company)->where(['active'=>1,'deleted'=>0,'link_id'=>$request->link_id,'link_name'=>$request->link_name])->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->word_array);
            $data= Notes::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->where(['active'=>1,'deleted'=>0,'link_id'=>$request->link_id,'link_name'=>$request->link_name])->orderBy('id', 'desc')->get();
        }

        $table = $section->addTable();
        foreach ($data as $key => $value) {
            if($key==0)
            {
                $row = $table->addRow();
                if($request->link_id == 3)
                {
                    if(Auth()->user()->hasPermissionTo('employeeNotes_subject_word_export') || Auth::user()->all_companies == 1)
                    {
                        $row->addCell()->addText('Title');
                    }
                    if(Auth()->user()->hasPermissionTo('employeeNotes_notes_word_export') || Auth::user()->all_companies == 1)
                    {
                        $row->addCell()->addText('Note');
                    }
                }
                elseif($request->link_id == 2)
                {
                    if(Auth()->user()->hasPermissionTo('documentNotes_subject_word_export') || Auth::user()->all_companies == 1)
                    {
                        $row->addCell()->addText('Title');
                    }
                    if(Auth()->user()->hasPermissionTo('documentNotes_notes_word_export') || Auth::user()->all_companies == 1)
                    {
                        $row->addCell()->addText('Note');
                    }
                }
                elseif($request->link_id == 1)
                {
                    if(Auth()->user()->hasPermissionTo('clientNotes_subject_word_export') || Auth::user()->all_companies == 1)
                    {
                        $row->addCell()->addText('Title');
                    }
                    if(Auth()->user()->hasPermissionTo('clientNotes_notes_word_export') || Auth::user()->all_companies == 1)
                    {
                        $row->addCell()->addText('Note');
                    }
                }
            }
            $table->addRow();
            if($request->link_id == 3)
            {
                if(Auth()->user()->hasPermissionTo('employeeNotes_subject_word_export') || Auth::user()->all_companies == 1)
                {
                    $row->addCell()->addText($value->title);
                }
                if(Auth()->user()->hasPermissionTo('employeeNotes_notes_word_export') || Auth::user()->all_companies == 1)
                {
                    $row->addCell()->addText($value->note);
                }
            }
            elseif($request->link_id == 2)
                {
                    if(Auth()->user()->hasPermissionTo('documentNotes_subject_word_export') || Auth::user()->all_companies == 1)
                    {
                        $row->addCell()->addText($value->title);
                    }
                    if(Auth()->user()->hasPermissionTo('documentNotes_notes_word_export') || Auth::user()->all_companies == 1)
                    {
                        $row->addCell()->addText($value->note);
                    }
                }
            elseif($request->link_id == 1)
                {
                    if(Auth()->user()->hasPermissionTo('clientNotes_subject_word_export') || Auth::user()->all_companies == 1)
                    {
                        $row->addCell()->addText($value->title);
                    }
                    if(Auth()->user()->hasPermissionTo('clientNotes_notes_word_export') || Auth::user()->all_companies == 1)
                    {
                        $row->addCell()->addText($value->note);
                    }
                }
        }
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('notes.docx');
        return response()->download(public_path('notes.docx'));
    }

    public function exportExcel(Request $request){
        if($request->excel_array == 1)
        {
            $data =Notes::where('company_id',Auth::user()->default_company)->where(['active'=>1,'deleted'=>0,'link_id'=>$request->link_id,'link_name'=>$request->link_name])->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->excel_array);
            $data= Notes::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->where(['active'=>1,'deleted'=>0,'link_id'=>$request->link_id,'link_name'=>$request->link_name])->orderBy('id', 'desc')->get();
        }
        $flag=$request->link_id;
        return Excel::download(new NoteExport($data,$flag), 'notes.csv');
    }

}

