<?php
namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\User;
use App\Company;
use Auth;
use DB;
use DataTables;
use Illuminate\Support\Facades\Hash;
use App\Exports\CompanyExport;
use App\Exports\CompanyExportSoft;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
class CompanyController extends Controller
{
    public function index()
    {
        $companies=Company::all();
        return view('backend.company.index')->with(['companies'=>$companies]);
    }
    public function store(Request $request)
    {
        $company=Company::where('name',$request->name)->orderBy('id', 'desc')->first();
        if (empty($company)){
            Company::create(['name'=>$request->name,'field_notes'=>$request->field_notes]);
            return response()->json(['message'=>'New company Created Successfully!']);
        }
    }
    public function edit(Request $request)
    {
        return response()->json(Company::find($request->id));
    }

    public function multipleDelete(Request $request){
        // dd($request->all());
        if($request->flag==1) {
            Company::whereIn('id',$request->ids)->update(['active'=>0]);
            return response()->json(['message'=>'Deleted']);
        }else if ($request->flag==2) {
            Company::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>1]);
            return response()->json(['message'=>'Deleted']);
        }
    }

    public function activeMultiple(Request $request)
    {
        Company::whereIn('id',$request->ids)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>2]);
    }
    public function delete(Request $request)
    {
        Company::where('id',$request->id)->update(['active'=>0]);
        return response()->json(['message'=>'delete']);

    }
    public function hardDelete(Request $request)
    {
        Company::where('id',$request->id)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'hard delet']);
    }

    public function update(Request $request)
    {
        Company::where('id',$request->id)->first()->update(['name'=>$request->name,'field_notes'=>$request->field_notes]);
        return response()->json(['message'=>"Record Updated Successfully!"]);
    }

    public function getallcompany(Request $request)
    {

        $data = Company::where('active',1)->get();
        if($request->ajax()) {
            return Datatables::of($data)
                ->addColumn('checkbox', function($data){
                })
                ->addColumn('actions', function($data){
                    $response="";

                    $view ='<a  id="view'.$data->id.'" class="btn btn-info btn-xs" href="'.url("cms/app").'"><i class="fa fa-eye"></i></a>';
                    $edit ='<a  id="' . $data->id . '" class="btn all_action_btn_margin btn-primary btn-xs" onclick="get_company_data('.$data->id.')" href="javascript:;"><i class="fas fa-pencil-alt"></i></a>';
                    $delete ='<button type="button"class="btn btn-danger btn-xs my-0" onclick="deletecompanydata( '.$data->id . ')"><i class="fas fa-trash"></i></button>';
                    $response=$response.$view;
                    if(Auth()->user()->hasPermissionTo('edit_company') || Auth::user()->all_companies == 1 )
                    {
                        $response=$response.$edit;
                    }
                    if(Auth()->user()->hasPermissionTo('delete_company') || Auth::user()->all_companies == 1 )
                    {
                        $response=$response.$delete;
                    }

                    return $response;
                })
                ->rawColumns(['actions'])
                ->make(true);

        }
    }

    public function getAllSoftCompany(Request $request)
    {
        $data = Company::where('active',0)->where('deleted',0)->get();
        if($request->ajax()) {
            return Datatables::of($data)
                ->addColumn('checkbox', function($data){
                })
                ->addColumn('actions', function($data){
                    $response = "";
                    $Restore_button = '<button type="button"class="btn btn-success btn-xs my-0" onclick="restore_company( ' . $data->id . ')"><i class="fa fa-retweet" aria-hidden="true"></i></button>';
                    $hard_delete_button = '<button type="button"class="btn btn-danger btn-xs my-0" onclick="hard_delete_company( ' . $data->id . ')"><i class="fa fa-trash" aria-hidden="true"></i></button>';
                    if(Auth()->user()->hasPermissionTo('restore_delete_company') || Auth::user()->all_companies == 1 )
                    {
                        $response = $response . $Restore_button;

                    }
                    if(Auth()->user()->hasPermissionTo('hard_delete_company') || Auth::user()->all_companies == 1 )
                    {
                        $response = $response . $hard_delete_button;

                    }

                    return $response;
                })
                ->rawColumns(['actions'])
                ->make(true);

        }
    }

    public function restore(Request $request)
    {
        Company::where('id',$request->id)->update(['active'=>1]);
        return response()->json(['message'=>'restore']);
    }

    public function export_excel_company(Request $request)
    {
        if($request->export_excel_company == 1)
        {
            return Excel::download(new CompanyExport, 'companies.csv');
        }
        else
        {
            return Excel::download(new CompanyExportSoft, 'companies.csv');
        }
    }

    public function exportPdfCompany(Request $request){
        if($request->pdf_array == 1)
        {
            $data= Company::where('active', '=',1)->get();
        }
        else
        {
            $array=json_decode($request->pdf_array);
            $data= Company::where('active', '=',1)->where('deleted',0)->whereIn('id',$array)->orderBy('id', 'desc')->get();
        }
        $date=Carbon::now()->format('D jS M Y');
        $companies='Patroltec';
        $reportname='Company';
        $pdf = PDF::loadView('backend.company.pdf',compact('companies','date','reportname','data'));
        return $pdf->download('company.pdf');
    }
    public function exportWordCompany(Request $request)
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        if($request->word_array == 1)
        {
            $companies= Company::where('active', '=',1)->get();
        }
        else
        {
            $array=json_decode($request->word_array);
            $companies= Company::where('active', '=',1)->where('deleted',0)->whereIn('id',$array)->orderBy('id', 'desc')->get();
        }
//
//        if(Auth::user()->all_companies == 1)
//        {
//            if($request->export_word_user == 1)
//            {
//                $companies= Company::where('active', '=',1)->get();
//            }
//            else
//            {
//                $companies= Company::where('active', '=',0)->get();
//            }
//            $text = $section->addText('Id   Name',array('name'=>'Arial','size' => 20,'bold' => true));
//        }
//        else
//        {
//            if($request->export_word_user == 1)
//            {
//                if(Auth()->user()->hasPermissionTo('company_name'))
//                {
//                    $companies= Company::where('active', '=',1)->get();
//                    $text = $section->addText('Id   Name',array('name'=>'Arial','size' => 20,'bold' => true));
//                }
//                else
//                {
//                    $companies=[];
//                    $text = $section->addText('empty',array('name'=>'Arial','size' => 20,'bold' => true));
//                }
//
//            }
//            else
//            {
//                if(Auth()->user()->hasPermissionTo('company_name'))
//                {
//                    $companies= Company::where('active', '=',0)->get();
//                    $text = $section->addText('Id   Name',array('name'=>'Arial','size' => 20,'bold' => true));
//                }
//                else
//                {
//                    $companies=[];
//                    $text = $section->addText('empty',array('name'=>'Arial','size' => 20,'bold' => true));
//                }
//            }
//
//        }
//


        $companies=$companies->unique();
        foreach ($companies as $key => $value) {
            $text = $section->addText($value->id .'         '. $value->name.'         '. $value->field_notes);
        }
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        ob_clean();
        $objWriter->save('companies.docx');
        return response()->download(public_path('companies.docx'));
    }
}
