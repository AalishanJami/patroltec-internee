<?php
namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Company;
use App\User;
use App\Location;
use App\Exports\manager\ManagerExport;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use PDF;
use Session;
use Auth;
use Carbon\Carbon;
use DataTables;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Hash;
use Validator;
use Config;

class ManagerController extends Controller
{
    private $page_name;
    private $tab_name;
    private $db;
    private $cms_array=[];
    public function __construct() {
        $data=localization();
        $this->page_name ='manager';
        $this->tab_name ='main_detail';
        $this->db='users';
        $cms_data=localization();
        $temp_array=[];
        $temp_array['first_name']['key']='first_name';
        $temp_array['first_name']['value']=checkKey('first_name',$cms_data);
        $temp_array['surname']['key']='surname';
        $temp_array['surname']['value']=checkKey('surname',$cms_data);
        $temp_array['position']['key']='position';
        $temp_array['position']['value']=checkKey('position',$cms_data);
        $this->cms_array=$temp_array;
    }
    public function getAllDropDown()
    {
        $data=User::where('company_id',Auth::user()->default_company)->where('position',2)->where('active',1)->where('deleted',0)->orderBy('id','desc')->get();
        $contract_managers=User::where('company_id',Auth::user()->default_company)->where('position',3)->where('active',1)->where('deleted',0)->orderBy('id','desc')->get();
        $project = Location::find(Session::get('project_id'));
        return response()->json(['project'=>$project,'data'=>$data,'contract_managers'=>$contract_managers]);
    }
    public function getAll()
    {
        $data=User::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id','desc')->get();
        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {
            })
            ->addColumn('submit', function ($data) {
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                if(Auth()->user()->hasPermissionTo('delete_managerName') || Auth::user()->all_companies == 1 )
                {
                    $delete = '<a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light delete-btn' . $data->id . '" onclick="deletemanagerdata( ' . $data->id . ')"><i class="fas fa-trash"></i></a>';
                    $response = $response . $delete;
                }
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);

    }
    public function getAllSoft()
    {
        $data=User::where('company_id',Auth::user()->default_company)->where('active',0)->where('deleted',0)->orderBy('id','desc')->get();
            return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {
            })
            ->addColumn('submit', function ($data) {
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                if(Auth()->user()->hasPermissionTo('restore_delete_managerName') || Auth::user()->all_companies == 1 )
                {
                    $restore_button = '<button type="button"class="btn btn-success btn-xs my-0" onclick="restoremanagerdata('.$data->id.')"><i class="fa fa-retweet" aria-hidden="true"></i></button>';
                    $response = $response . $restore_button;
                }
                if(Auth()->user()->hasPermissionTo('hard_delete_managerName') || Auth::user()->all_companies == 1 )
                {
                    $hard_delete_button = '<button type="button"class="btn btn-danger btn-xs my-0" onclick="deletesoftmanagerdata('.$data->id.')"><i class="fa fa-trash" aria-hidden="true"></i></button>';
                    $response = $response . $hard_delete_button;
                }
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
    public function store()
    {
        $email ='dummyTest'.substr(md5(rand(0,5)),0,5).'@patroltec.net';
        $data = user::create(['email'=>$email,'name'=>'','password' => Hash::make("12345678")]);
        $id = $data->id;
        $email = $data->id.'@patroltec.net';
        $user_data = User::find($data->id);
    audit_log($this->cms_array,'',Config::get('constants.store'),$this->page_name,$this->tab_name,$id,'',$id,Session::get('project_id'),$this->db);
        $user_data->update(['email'=>$email]);
        return response()->json(['message'=>'Added successfully','data'=>$data]);
    }
    public function update(Request $request)
    {
        // dd($request->all());
        if (isset($request->name)){
            $data = [
                'company_id'=>'required|numeric',
                'first_name'=>'required',
            ];
        }else{
            $data = [
                'company_id'=>'required|numeric',
            ];
        }
         $validator = Validator::make($request->all(), $data);
         if ($validator->fails())
         {
             $error = $validator->messages();
             $html='<ul>';
             foreach ($error->all() as $key => $value) {
                 $html=$html.'<li>'.$value.'</li>';
             }
             $html=$html.'</ul>';
             return response()->json(['status'=> false, 'error'=> $html]);
         }
        $data=$request->all();
        if (isset($request->first_name)){
            $data['first_name']=$request->first_name;
        }else{
            $data['first_name']='';
        }
        $User=User::where('id',$data['id']);
        $User_first=$User->first();
        audit_log($this->cms_array,audit_log_data($data),Config::get('constants.update'),$this->page_name,$this->tab_name,$data['id'],audit_log_data($User_first->toArray()),$data['id'],Session::get('project_id'),$this->db);
        $User->update($data);
        if($User_first->active == 0)
        {
            $flag=0;
        }
        else
        {
            $flag=1;
        }

        return response()->json(['message'=>'Successfully Added !!','flag'=>$flag]);

    }
    public function softDelete (Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.soft_delete'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('project_id'),$this->db);
        User::where('id',$request->id)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function delete(Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.delete'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('project_id'),$this->db);
        User::where('id',$request->id)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function active(Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.restore'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('project_id'),$this->db);
        User::where('id',$request->id)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
    public function activeMultiple(Request $request)
    {
        User::whereIn('id',$request->ids)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
     public function deleteMultipleSOft(Request $request)
    {
        User::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function deleteMultiple(Request $request)
    {
        User::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function exportPdf(Request $request)
    {
        if($request->pdf_array == 1)
        {
            $data =User::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->pdf_array);
            $data= User::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        $date=Carbon::now()->format('D jS M Y');
        $companies='Patroltec';
        $reportname='Manager';
        $pdf = PDF::loadView('backend.user.manager.pdf',compact('data','date','reportname','companies'));
        return $pdf->download('manager.pdf');
    }
    public function exportWorld(Request $request)
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        if($request->word_array == 1)
        {
            $data= User::where('company_id',Auth::user()->default_company)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->word_array);
            $data= User::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        $table = $section->addTable();
        foreach ($data as $key => $value) {
            if($key==0)
            {
                $table->addRow();
                if(Auth()->user()->hasPermissionTo('managerName_name_word_export') || Auth::user()->all_companies == 1){
                    $table->addCell(1750)->addText('Name');
                }
                if(Auth()->user()->hasPermissionTo('managerName_position_word_export') || Auth::user()->all_companies == 1){
                    $table->addCell(1750)->addText('Position');
                }
            }
            $table->addRow();
            if(Auth()->user()->hasPermissionTo('managerName_name_word_export') || Auth::user()->all_companies == 1){
                $table->addCell(1750)->addText($value->first_name);
            }
            if(Auth()->user()->hasPermissionTo('managerName_position_word_export') || Auth::user()->all_companies == 1){
                $table->addCell(1750)->addText('Manager');
            }
        }
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('manager.docx');
        return response()->download(public_path('manager.docx'));
    }
    public function exportExcel(Request $request){
        if($request->excel_array == 1) {
            $data= User::where('company_id',Auth::user()->default_company)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        } else {
            $array=json_decode($request->excel_array);
            $data= User::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        return Excel::download(new ManagerExport($data), 'manager.csv');
    }
}
