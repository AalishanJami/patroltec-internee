<?php
namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use Validator;
use App\Company;
use Config;
use App\User;
class ContactInformationController extends Controller
{
    private $page_name;
    private $tab_name;
    private $db;
    private $cms_array=[];
    public function __construct() {
        $this->page_name ='contact_information';
        $this->tab_name ='employees';
        $this->db='users';
        $cms_data=localization();
        $temp_array=[];
        $temp_array['email']['key']='email';
        $temp_array['email']['value']=checkKey('email',$cms_data);
        $temp_array['phone']['key']='phone';
        $temp_array['phone']['value']=checkKey('phone',$cms_data);
        $temp_array['mobile']['key']='mobile_number';
        $temp_array['mobile']['value']=checkKey('mobile_number',$cms_data);
        $temp_array['address']['key']='address';
        $temp_array['address']['value']=checkKey('address',$cms_data);
        $temp_array['house_name']['key']='house_name';
        $temp_array['house_name']['value']=checkKey('house_name',$cms_data);
        $temp_array['street']['key']='street';
        $temp_array['street']['value']=checkKey('street',$cms_data);
        $temp_array['town']['key']='town';
        $temp_array['town']['value']=checkKey('town',$cms_data);
        $temp_array['county']['key']='country';
        $temp_array['county']['value']=checkKey('country',$cms_data);
        $temp_array['postcode']['key']='post_code';
        $temp_array['postcode']['value']=checkKey('post_code',$cms_data);
        $this->cms_array=$temp_array;
    }
    public function index()
    {
    	$companies=Company::getCompanies();
    	$user=User::where('id',Session::get('employee_id'))->first();
    	return view('backend.contact_infomation.index')->with(['companies'=>$companies,'user'=>$user]);
    }
    public function update(Request $request)
    {
    	$data = [
    	    'email' => 'required|email|unique:users,email,'.Session::get('employee_id'),
        ];
        $validator = Validator::make($request->all(), $data);
        if ($validator->fails())
        {
            return back()->withErrors($validator)->withInput();
        }
        $input=$request->all();
        unset($input['_token']);
        $user=User::where('id',Session::get('employee_id'));
        $user=$user->first();
        audit_log($this->cms_array,audit_log_data($input),Config::get('constants.update'),$this->page_name,$this->tab_name,Session::get('employee_id'),audit_log_data($user->toArray()),Session::get('employee_id'),Session::get('employee_id'),$this->db);
        $user->update($input);
    	toastr()->success('Contact Information update!');
        return redirect('/contact_infomation');
    }

}
