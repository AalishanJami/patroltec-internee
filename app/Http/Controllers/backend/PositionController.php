<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Notes;
use App\Position;
use App\User;
use App\SexualOrientations;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Session;
use DataTables;
use PDF;
use Config;
use Carbon\Carbon;
use App\Exports\employee\PositionExport;
use Auth;
class PositionController extends Controller
{
    private $page_name;
    private $tab_name;
    private $db;
    private $cms_array=[];
    public function __construct() {
        $this->page_name ='position';
        $this->tab_name ='position';
        $this->db='positions';
        $cms_data=localization();
        $temp_array=[];
        $temp_array['name']['key']='name';
        $temp_array['name']['value']=checkKey('name',$cms_data);
        $this->cms_array=$temp_array;
    }
    public function store()
    {
        $data=Position::create(['name'=>'','default_company'=>Auth::user()->default_company]);
    audit_log($this->cms_array,'',Config::get('constants.store'),$this->page_name,$this->tab_name,$data->id,'',$data->id,'',$this->db);
        return response()->json(['data'=>$data]);
    }
    public function update(Request $request)
    {
        $data = [
            'company_id'=>'required|numeric',
            'name'=>'required',
        ];
        $validator = Validator::make($request->all(), $data);
        if ($validator->fails())
        {
            $error = $validator->messages();
            $html='<ul>';
            foreach ($error->all() as $key => $value) {
                $html=$html.'<li>'.$value.'</li>';
            }
            $html=$html.'</ul>';
            return response()->json(['status'=> false, 'error'=> $html]);
        }
        $data=$request->all();
        $religion=Position::where('id',$data['id']);
        $religion_first=$religion->first();
        unset($religion_first->position_permision);
        audit_log($this->cms_array,audit_log_data($data),Config::get('constants.update'),$this->page_name,$this->tab_name,$religion_first->id,audit_log_data($religion_first->toArray()),$religion_first->id,'',$this->db);
        $religion->update($data);
        if($religion_first->active == 0) {
            $flag=0;
        } else {
            $flag=1;
        }
        return response()->json(['message'=>'Successfully Added !!','flag'=>$flag]);
    }
    public function getAll(){
        $data =Position::getPosition()->get();
        $user=User::where('id',Session::get('employee_id'))->first();
        return Datatables::of($data)->with(['user'=>$user])
            ->addColumn('checkbox', function ($data) {
            })
            ->addColumn('submit', function ($data) {
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                if(permisionCheck('delete_employeePosition')  ){
                    $delete = '<a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light deletepositiondata'.$data->id.' delete-btn'.$data->id.'" onclick="deletepositiondata( ' . $data->id . ')"><i class="fas fa-trash"></i></a>';
                }else{
                    $delete = '';
                }
                $response = $response . $delete;
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
    public function getAllSoft(){
        $data =Position::getPosition(1)->get();
        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {
            })
            ->addColumn('submit', function ($data) {
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                if(permisionCheck('restore_delete_employeePosition')  ){
                    $restore='<a type="button" class="btn btn-success btn-xs my-0 waves-effect waves-light" onclick="restorepositiondata('.$data->id.')"><i class="fa fa-retweet" aria-hidden="true"></i></a>';
                }else{
                    $restore='';
                }
                if(permisionCheck('hard_delete_employeePosition')  ){
                    $delete = '<a type="button" class="btn btn-danger btn-xs my-0 all_action_btn_margin waves-effect waves-light" onclick="deletesoftpositiondata( ' . $data->id . ')"><i class="fas fa-trash"></i></a>';
                }else{
                    $delete='';
                }
                $response = $response . $restore . $delete;
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
    public function delete(Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.delete'),$this->page_name,$this->tab_name,'','',$request->id,'',$this->db);
        Position::where('id',$request->id)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function active(Request $request)
    {
     audit_log($this->cms_array,'',Config::get('constants.restore'),$this->page_name,$this->tab_name,'','',$request->id,'',$this->db);
        Position::where('id',$request->id)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
    public function activeMultiple(Request $request)
    {
        Position::whereIn('id',$request->ids)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
    public function softDelete(Request $request){
    audit_log($this->cms_array,'',Config::get('constants.soft_delete'),$this->page_name,$this->tab_name,'','',$request->id,'',$this->db);
        Position::where('id',$request->id)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function deleteMultipleSOft(Request $request)
    {
        Position::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }

    public function deleteMultiple(Request $request){
        Position::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function exportPdf(Request $request)
    {
        if($request->pdf_array == 1) {
           $data =Position::getPosition()->get();
        } else {
            $array=json_decode($request->pdf_array);
            $data= Position::getPosition()->whereIn('id',$array)->get();
        }
        $date=Carbon::now()->format('D jS M Y');
        $companies='Patroltec';
        $reportname='Position';
        $pdf = PDF::loadView('backend.employee.export.position_pdf',compact('data','date','reportname','companies'));
        return $pdf->download('position.pdf');
    }
    public function exportWord(Request $request){
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        if($request->word_array == 1)
        {
            $data= Position::getPosition()->get();
        }
        else
        {
            $array=json_decode($request->word_array);
            $data= Position::getPosition()->whereIn('id',$array)->get();
        }
        $table = $section->addTable();
        $row = $table->addRow();
        if(permisionCheck('employeePosition_name_word_export')  ){
            $row->addCell()->addText('Name');
        }
        foreach ($data as $key => $value) {
            $row = $table->addRow();
            if(permisionCheck('employeePosition_name_word_export')  ){
                $row->addCell()->addText($value->name);
            }
        }
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('position_word.docx');
        return response()->download(public_path('position_word.docx'));
    }
    public function exportExcel(Request $request){
        if($request->excel_array == 1)
        {
            $data= Position::getPosition()->get();
        }
        else
        {
            $array=json_decode($request->excel_array);
            $data= Position::getPosition()->whereIn('id',$array)->get();
        }
        return Excel::download(new PositionExport($data), 'position.csv');
    }

}
