<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\DocType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Session;
use DataTables;
use PDF;
use App\Exports\doc_type\DocTypeExport;
use Auth;
use Carbon\Carbon;
class DocTypeController extends Controller
{
    public function store()
    {
        $data=DocType::create();
        return response()->json(['data'=>$data]);
    }

    public function update(Request $request)
    {
        $data = [
            // 'company_id'=>'required|numeric',
            'name'=>'required',
        ];
        $validator = Validator::make($request->all(), $data);
        if ($validator->fails())
        {
            $error = $validator->messages();
            $html='<ul>';
            foreach ($error->all() as $key => $value) {
                $html=$html.'<li>'.$value.'</li>';
            }
            $html=$html.'</ul>';
            return response()->json(['status'=> false, 'error'=> $html]);
        }
        $data=$request->all();
        $religion=DocType::where('id',$data['id']);
        $religion_first=$religion->first();
        $religion->update($data);
        if($religion_first->active == 0) {
            $flag=0;
        } else {
            $flag=1;
        }
        return response()->json(['message'=>'Successfully Added !!','flag'=>$flag]);
    }
    public function getAll(){
        $data =DocType::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {
            })
            ->addColumn('submit', function ($data) {
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                $delete = '<a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light delete-btn'.$data->id.'" onclick="deletedoc_typedata( ' . $data->id . ')"><i class="fas fa-trash"></i></a>';
                $response = $response . $delete;
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
    public function getAllSoft(){
        $data =DocType::where('company_id',Auth::user()->default_company)->where('active',0)->where('deleted',0)->orderBy('id', 'desc')->get();
        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {
            })
            ->addColumn('submit', function ($data) {
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                $restore='<a type="button" class="btn btn-success btn-xs my-0 waves-effect waves-light" onclick="restoredoc_typedata('.$data->id.')"><i class="fa fa-retweet" aria-hidden="true"></i></a>';
                $delete = '<a type="button" class="btn btn-danger btn-xs my-0 all_action_btn_margin waves-effect waves-light" onclick="deletesoftdoc_typedata( ' . $data->id . ')"><i class="fas fa-trash"></i></a>';
                $response = $response . $restore . $delete;
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
    public function delete(Request $request)
    {
        DocType::where('id',$request->id)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function active(Request $request)
    {
        DocType::where('id',$request->id)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
    public function activeMultiple(Request $request)
    {
        DocType::whereIn('id',$request->ids)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
    public function softDelete(Request $request){
        DocType::where('id',$request->id)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function deleteMultipleSOft(Request $request)
    {
        DocType::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }

    public function deleteMultiple(Request $request){
        DocType::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function exportPdf(Request $request)
    {
        if($request->pdf_array == 1) {
           $data =DocType::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        } else {
            $array=json_decode($request->pdf_array);
            $data= DocType::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        $date=Carbon::now()->format('D jS M Y');
        $companies='Patroltec';
        $reportname='Doc Type';
        $pdf = PDF::loadView('backend.doc_type.pdf',compact('data','date','reportname','companies'));
        return $pdf->download('Doc Type.pdf');
    }
    public function exportWord(Request $request){
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        if($request->word_array == 1)
        {
            $data= DocType::where('company_id',Auth::user()->default_company)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->word_array);
            $data= DocType::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        $table = $section->addTable();
        $row = $table->addRow();
        $row->addCell()->addText('Name');
        foreach ($data as $key => $value) {
            $row = $table->addRow();
            $row->addCell()->addText($value->name);
        }
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('doc_Type.docx');
        return response()->download(public_path('doc_Type.docx'));
    }
    public function exportExcel(Request $request)
    {
        if($request->excel_array == 1)
        {
            $data= DocType::where('company_id',Auth::user()->default_company)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->excel_array);
            $data= DocType::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        return Excel::download(new DocTypeExport($data), 'doc_Type.csv');
    }

}
