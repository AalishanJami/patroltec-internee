<?php
namespace App\Http\Controllers\backend;
use App\Clients;
use App\Company;
use App\Http\Controllers\Controller;
use App\SexualOrientations;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\User;
use App\DocType;
use App\CompanyUser;
use Config;
use App\Position;
use App\Exports\employee\EmployeeExport;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Session;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use DataTables;
use Auth;
class EmployeeController extends Controller
{
    private $page_name;
    private $tab_name;
    private $db;
    private $cms_array=[];
    public function __construct() {
        Session::forget('project_id');
        Session::forget('form_id');
        Session::forget('client_id');
        Session::forget('folder_id');
        $this->page_name ='employees';
        $this->tab_name ='main_detail';
        $this->db='users';
        $cms_data=localization();
        $temp_array=[];
        $temp_array['first_name']['key']='first_name';
        $temp_array['first_name']['value']=checkKey('first_name',$cms_data);
        $temp_array['surname']['key']='surname';
        $temp_array['surname']['value']=checkKey('surname',$cms_data);
        $temp_array['payroll_number']['key']='payroll_number';
        $temp_array['payroll_number']['value']=checkKey('payroll_number',$cms_data);
        $temp_array['client_id']['key']='client';
        $temp_array['client_id']['value']=checkKey('client',$cms_data);
        $temp_array['manager_id']['key']='manager';
        $temp_array['manager_id']['value']=checkKey('manager',$cms_data);
        $temp_array['sex']['key']='sex';
        $temp_array['sex']['value']=checkKey('sex',$cms_data);
        $temp_array['position']['key']='employee_type';
        $temp_array['position']['value']=checkKey('employee_type',$cms_data);
        $temp_array['position_id']['key']='position';
        $temp_array['position_id']['value']=checkKey('position',$cms_data);
        $temp_array['dob']['key']='dob';
        $temp_array['dob']['value']=checkKey('dob',$cms_data);
        $temp_array['visa_expiry_date']['key']='ved';
        $temp_array['visa_expiry_date']['value']=checkKey('ved',$cms_data);
        $temp_array['national_Insurance_number']['key']='national_Insurance_number';
        $temp_array['national_Insurance_number']['value']=checkKey('national_Insurance_number',$cms_data);
        $temp_array['ethinic_origin_id']['key']='ethnic';
        $temp_array['ethinic_origin_id']['value']=checkKey('ethnic',$cms_data);
        $temp_array['payroll_number']['key']='payroll_number';
        $temp_array['payroll_number']['value']=checkKey('payroll_number',$cms_data);
        $temp_array['cscs_Card']['key']='cscs_card';
        $temp_array['cscs_Card']['value']=checkKey('cscs_card',$cms_data);
        $temp_array['cscs_Card_Expiry_Date']['key']='cscs_card_expiry_date';
        $temp_array['cscs_Card_Expiry_Date']['value']=checkKey('cscs_card_expiry_date',$cms_data);
        $temp_array['sexual_orientation_id']['key']='sexual_orientation';
        $temp_array['sexual_orientation_id']['value']=checkKey('sexual_orientation',$cms_data);
        $temp_array['warning']['key']='warnings';
        $temp_array['warning']['value']=checkKey('warnings',$cms_data);
        $temp_array['notes']['key']='notes';
        $temp_array['notes']['value']=checkKey('notes',$cms_data);
        $this->cms_array=$temp_array;
    }
    public function index()
    {
        $companies=Company::getCompanies();
        $doc_type=DocType::getDocType()->get();
        $positions=Position::getPosition()->get();
    	return view('backend.employee.index')->with(['companies'=>$companies,'doc_type'=>$doc_type,'positions'=>$positions]);
    }
    public function getAll(Request $request){
        $data=User::getUser()->get();
        $data=sortData($data);
        return Datatables::of($data)
        ->addColumn('checkbox', function($data){})
        ->addColumn('client_id', function($data){
            $client_id=(isset($data->client->first_name) ? $data->client->first_name : '');
            return $client_id;
        })
        ->addColumn('manager_id', function($data){
            $manager_id='';
            if($data->manager_id == '2')
            {

                $manager_id='Manager';

            }
            elseif($data->manager_id == '3')
            {
                $manager_id='Admin';
            }
            // $manager_id=(isset($data->locationUserManager->name) ? $data->locationUserManager->name : '');
            return $manager_id;
        })
        ->addColumn('actions', function($data){
                $response="";
                if(permisionCheck('edit_employee')){
                    $edit ='<a href="'.url("/employee/detail/".$data->id).'" class="btn btn-primary btn-xs mr-2"><i class="fas fa-pencil-alt"></i></a>';
                }else{
                    $edit ='';
                }
                if(permisionCheck('delete_employee')){
                    $delete ='<a class="btn btn-danger btn-xs my-0" onclick="deleteemployeedata( '.$data->id . ')"><i class="fas fa-trash"></i></a>';
                }else{
                    $delete ='';
                }
                $response=$response.$edit;
                $response=$response.$delete;
                return $response;
        })->rawColumns(['actions'])->make(true);
    }
    public function getAllSoft()
    {
        $data=User::getUser(1)->get();
        $data=sortData($data);
        return Datatables::of($data)
        ->addColumn('checkbox', function($data){})
        ->addColumn('client_id', function($data){
            $client_id=(isset($data->client->first_name) ? $data->client->first_name : '');
            return $client_id;
        })
        ->addColumn('manager_id', function($data){
            $manager_id='';
            if($data->manager_id == 2)
            {

                $manager_id='Manager';

            }
            elseif($data->manager_id == 3)
            {
                $manager_id='Admin';
            }
            // $manager_id=(isset($data->locationUserManager->name) ? $data->locationUserManager->name : '');
            return $manager_id;
        })
        ->addColumn('actions', function ($data) {
            $response = "";
            $btn = '<a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light" onclick="deletesoftemployeedata('.$data->id.')"><i class="fas fa-trash"></i></a><a type="button" class="btn btn-success btn-xs my-0 waves-effect waves-light" onclick="restoreemployeedata('.$data->id.')"><i class="fa fa-retweet" aria-hidden="true"></i></a>';
            $response = $response . $btn;
            return $response;
        })
        ->rawColumns(['actions'])
        ->make(true);
    }
    public function detail($id)
    {
        $companies=Company::getCompanies();
        Session::put('employee_id',$id);
        $employee = User::getUser()->where('users.id',$id)->first();
        if(!$employee)
        {
            return redirect('employee');
        }
        $positions=Position::getPosition()->get();
        $managers = User::getUser()->orWhere('position','=','2')->orWhere('position','=','3')->get();
        $clients =Clients::getClients()->get()->pluck('first_name','id');
    	return view('backend.employee.details')->with(['companies'=>$companies,'employee'=>$employee,'clients'=>$clients,'managers'=>$managers,'positions'=>$positions]);
    }

    public function createDetail(){
        $data = user::create(['email'=>'dummyTest@patroltec.net','default_company'=>Auth::user()->default_company,'name'=>'Edit me','password' => Hash::make("12345678")]);
        $id = $data->id;
        $email = $data->id.'@patroltec.net';
        CompanyUser::create(['user_id'=>$id,'company_id'=>Auth::user()->default_company]);
        User::find($id)->update(['email'=>$email]);
    audit_log($this->cms_array,'',Config::get('constants.store'),$this->page_name,$this->tab_name,$id,'',$id,'',$this->db);
        return $this->detail($id);
    }
    public function updateDetail(Request $request){
        $data = [
            'first_name'=>'required',
        ];
        $validator = Validator::make($request->all(), $data);
        if ($validator->fails()) {
            return back()->withErrors($validator)->with('style','border-bottom:1px solid #e36db1;')->withInput();
        }
        $input = $request->all();
        unset($input['_token']);
        unset($input['id']);
        if (isset($input['dob'])){
            $input['dob']=Carbon::parse($request->dob)->toDateString();
        }else{
            $input['dob']=date('Y-m-d');
        }
        if (isset($input['cscs_Card_Expiry_Date'])){
            $input['cscs_Card_Expiry_Date']=Carbon::parse($request->cscs_Card_Expiry_Date)->toDateString();
        }else{
            $input['cscs_Card_Expiry_Date']=date('Y-m-d');
        }
        if (isset($input['visa_expiry_date'])){
            $input['visa_expiry_date']=Carbon::parse($request->visa_expiry_date)->toDateString();
        }else{
            $input['visa_expiry_date']=date('Y-m-d');
        }
        $employee = User::where('id',$request->id);
        audit_log($this->cms_array,audit_log_data($input),Config::get('constants.update'),$this->page_name,$this->tab_name,$request->id,audit_log_data($employee->first()->toArray()),$request->id,'',$this->db);
        $employee->update($input);
        return redirect('employee/detail/'.$request->id);
    }

    public function notes()
    {
        $flag=3;
        $companies=Company::getCompanies();
        return view('backend.employee.notes')->with(['companies'=>$companies,'flag'=>$flag]);
    }

    public function softDelete (Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.soft_delete'),$this->page_name,$this->tab_name,'','',$request->id,'',$this->db);
        User::where('id',$request->id)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function delete(Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.delete'),$this->page_name,$this->tab_name,'','',$request->id,'',$this->db);
        User::where('id',$request->id)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function active(Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.restore'),$this->page_name,$this->tab_name,'','',$request->id,'',$this->db);
        User::where('id',$request->id)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
    public function activeMultiple(Request $request)
    {
        User::whereIn('id',$request->ids)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
     public function deleteMultipleSOft(Request $request)
    {
        User::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }

    public function deleteMultiple(Request $request){
        User::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }

    public function exportPdf(Request $request)
    {
        if($request->pdf_array == 1)
        {
            $data=User::getUser()->get();
        }
        else
        {
            $array=json_decode($request->pdf_array);
            $data=User::getUser()->whereIn('id',$array)->get();
        }
        $companies='Patroltec';
        $reportname='Employee';
        $date=\Carbon\Carbon::now()->format('D jS M Y');
        $pdf = PDF::loadView('backend.employee.pdf',compact('companies','reportname','data','date'));
        return $pdf->download('employee.pdf');
    }

    public function exportWorld(Request $request)
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        if($request->word_array == 1)
        {
            $data=User::getUser()->get();
        }
        else
        {
            $array=json_decode($request->word_array);
            $data=User::getUser()->whereIn('id',$array)->get();
        }
        $table = $section->addTable();
        foreach ($data as $key => $value) {
            if($key==0)
            {
                $table->addRow();
                if(permisionCheck('employee_firstname_word_export')){
                    $table->addCell(1750)->addText('Name');
                }
                if(permisionCheck('employee_payrolnumber_word_export')){
                    $table->addCell(1750)->addText('Payroll Number');
                }
                if(permisionCheck('employee_client_word_export')){
                    $table->addCell(1750)->addText('Client');
                }
                if(permisionCheck('employee_managerrole_word_export')){
                    $table->addCell(1750)->addText('Manager');
                }
            }
            $table->addRow();
            if(permisionCheck('employee_firstname_word_export')){
                $table->addCell(1750)->addText($value->first_name);
            }
            if(permisionCheck('employee_payrolnumber_word_export')){
                $table->addCell(1750)->addText($value->payroll_number);
            }
            if(permisionCheck('employee_client_word_export')){
                $table->addCell(1750)->addText(isset($value->client->surname) ? $value->client->surname : '');
            }
            if(permisionCheck('employee_managerrole_word_export')){
                $table->addCell(1750)->addText( isset($value->locationUserManager->name) ? $value->locationUserManager->name : '');
            }
        }
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('employee.docx');
        return response()->download(public_path('employee.docx'));
    }
    public function exportExcel(Request $request)
    {
        if($request->excel_array == 1)
        {
            $data=User::getUser()->get();
        }
        else
        {
            $array=json_decode($request->excel_array);
            $data=User::getUser()->whereIn('id',$array)->get();
        }
        return Excel::download(new EmployeeExport($data), 'employee.csv');
    }

}
