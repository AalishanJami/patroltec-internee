<?php
namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Location;
use App\LocationGroupPermision;
use App\LocationPermision;
use App\SiteGroups;
use App\Company;
use Config;
use Session;
use Auth;
class LocationController extends Controller
{
    private $page_name;
    private $tab_name;
    private $db;
    private $cms_array=[];
    public function __construct() {
        $this->page_name ='locations';
        $this->tab_name ='employees';
        $this->db='location_group_permisions';
        $cms_data=localization();
        $temp_array=[];
        $temp_array['user_id']['key']='user';
        $temp_array['user_id']['value']=checkKey('user',$cms_data);
        $temp_array['permissions']['key']='permissions';
        $temp_array['permissions']['value']=checkKey('permissions',$cms_data);
        $this->cms_array=$temp_array;
    }
    public function index()
    {
    	$companies=Company::getCompanies();
    	$groups=SiteGroups::getSiteGroups()->get();
    	$groups=sortData($groups);
        $groups_array=[];
        foreach ($groups as $key => $value) {
            if($value->enable == 1)
            {
                $groups_array[$key]=$value['id'];
            }
        }
    	$locations =Location::getLocation()->whereIn('site_group_id',$groups_array)->get();
    	$locations=sortData($locations);
        // dd($locations->toArray());
    	return view('backend.location.index')->with(['groups'=>$groups,'locations'=>$locations,'companies'=>$companies]);
    }
    public function groupUpdate(Request $request)
    {
        $old_value=dataPermision('LocationGroupPermision','','site_groups');
        $new_value=dataPermision('site_groups',$request->group);
        if($request->location_group_id)
        {
            LocationGroupPermision::where('employee_id',Session::get('employee_id'))->whereIn('site_groups_id',$request->location_group_id)->delete();
        }
    	if($request->group)
    	{
	    	foreach ($request->group as $key => $value) {
	    		LocationGroupPermision::create(['site_groups_id'=>$value,'employee_id'=>Session::get('employee_id'),'company_id'=>Auth::user()->default_company]);
	    	}
    	}
        audit_log($this->cms_array,audit_log_data($new_value),Config::get('constants.update'),$this->page_name,$this->tab_name,Session::get('employee_id'),audit_log_data($old_value),Session::get('employee_id'),Session::get('employee_id'),$this->db);
    	toastr()->success('Location Group Permision Successfully Updated!');
        return redirect('/location');
    }
    public function locationUpdate(Request $request)
    {
        $old_value=dataPermision('LocationPermision','','location');
        $new_value=dataPermision('locations',$request->location);
        if($request->location_id)
        {
            LocationPermision::where('employee_id',Session::get('employee_id'))->whereIn('location_id',$request->location_id)->delete();
        }
    	if($request->location)
    	{
	    	foreach ($request->location as $key => $value) {
	    		LocationPermision::create(['location_id'=>$value,'employee_id'=>Session::get('employee_id'),'company_id'=>Auth::user()->default_company]);
	    	}
    	}
        audit_log($this->cms_array,audit_log_data($new_value),Config::get('constants.update'),$this->page_name,$this->tab_name,Session::get('employee_id'),audit_log_data($old_value),Session::get('employee_id'),Session::get('employee_id'),'location_permisions');
    	toastr()->success('Location Permision Successfully Updated!');
        return redirect('/location');
    }
    public function document()
    {
    	return view('backend.user_document.model.index');
    }


   
}
