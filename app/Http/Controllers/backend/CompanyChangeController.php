<?php
namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Auth;
use Session;
class CompanyChangeController extends Controller
{
    public function change(Request $request)
    {
    	$flag=0;
    	if(Session::get('project_id') || Session::get('employee_id') || Session::get('form_id') || Session::get('folder_id') || Session::get('client_id')) 
    	{
    		$flag=1;

    	}
        User::where('id',Auth::user()->id)->update(['default_company'=>$request->default_company]);
        return response()->json(['message'=>'Company change successfully','flag'=>$flag]);
    }
}
