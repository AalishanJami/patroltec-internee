<?php

namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use DataTables;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\CustomRole;
use App\Company;
use Session;
use App\PermissionGroup;
use Carbon\Carbon;
use App\PermissionModuleName;
class RoleController extends Controller {
        public function index() {
        // $permissionsArrayListing=Permission::where('sub_module_name','listing')->where('module_name_id',6)->orderBy('order', 'asc')->get()->groupby('permission_name');
        // dd($permissionsArrayListing->toArray());
        $roles = Role::where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        $companies=Company::getCompanies();
        $permissions = Permission::get()->groupby('group');
        return view('backend.roles.index')->with(['roles'=>$roles,'permissions'=>$permissions,'companies'=>$companies]);
    }
    public function getallroll(Request $request)
    {
        $data = Role::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        $data=sortRoleData($data);
        return $this->alldataGet($data);
    }
    public function alldataGet($data)
    {
        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {
            }) ->addColumn('module_name', function ($data) {
                  $response = "";
                foreach($data->permissions->groupBy('module_name') as $key=>$value)
                {
                    $response=$response .$key ." ";

                }
                 return $response;
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                $edit = '<a onClick="getroledata( ' . $data->id . ')" id="' . $data->id . '" class="btn btn-primary btn-xs" href=""  data-toggle="modal" data-target="#modalEditForm"><i class="fas fa-pencil-alt"></i></a>';
                $delete = '<a class="btn btn-danger btn-xs my-0 all_action_btn_margin" onclick="deleteroledata( ' . $data->id . ')"><i class="fas fa-trash"></i></a>';

                if(Auth()->user()->hasPermissionTo('edit_roles') || Auth::user()->all_companies == 1)
                {
                    $response = $response . $edit;
                }
                if(Auth()->user()->hasPermissionTo('edit_roles') || Auth::user()->all_companies == 1)
                {
                    $response = $response . $delete;
                }

                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);

    }
    public function getAllSoftRoll(Request $request)
    {
        $data = Role::where('company_id',Auth::user()->default_company)->where('active',0)->where('deleted',0)->get();
        $data=sortRoleData($data);
            if ($request->ajax()) {

                return Datatables::of($data)
                    ->addColumn('checkbox', function ($data) {
                    }) ->addColumn('module_name', function ($data) {
                          $response = "";
                        foreach($data->permissions->groupBy('module_name') as $key=>$value)
                        {
                            $response=$response .$key ." ";

                        }
                         return $response;
                    })
                    ->addColumn('actions', function ($data) {
                        $response = "";

                        $Restore_button = '<button type="button"class="btn btn-success btn-xs my-0" onclick="restore_role( ' . $data->id . ')"><i class="fa fa-retweet" aria-hidden="true"></i></button>';


                        $hard_delete_button = '<button type="button"class="btn btn-danger btn-xs my-0" onclick="hard_delete_role( ' . $data->id . ')"><i class="fa fa-trash" aria-hidden="true"></i></button>';

                        if(Auth()->user()->hasPermissionTo('restore_delete_roles') || Auth::user()->all_companies == 1)
                        {
                            $response = $response . $Restore_button;
                        }
                        if(Auth()->user()->hasPermissionTo('hard_delete_roles') || Auth::user()->all_companies == 1)
                        {
                            $response = $response . $hard_delete_button;
                        }


                        return $response;
                    })
                    ->rawColumns(['actions'])
                    ->make(true);

             }
    }
    public function company($id)
    {
        $data = Role::where('active',1)->where('company_id',$id)->get();
        $data=sortRoleData($data);
        return $this->alldataGet($data);
    }
    public function store(Request $request) {
        $this->validate($request, [
            'name'=>'required|unique:roles|max:10',
            // 'permissions' =>'required',
            ]
        );

        $name = $request['name'];
        $role = new CustomRole();
        $role->name = $name;
        $role->guard_name ='web';
        $role->company_id=$request['company_id'];
        // $permissions = $request['permissions'];
        $role->save();
        // foreach ($permissions as $permission) {
        //     $p = Permission::where('id', '=', $permission)->firstOrFail();
        //     $role = Role::where('name', '=', $name)->first();
        //     $role->givePermissionTo($p);
        // }
        toastr()->success('Role '. $role->name.' added!');
        return redirect('/roles');
    }

    public function show($id) {
        return redirect('roles');
    }
     public function restore(Request $request)
    {
        Role::where('id',$request->id)->update(['active'=>1]);
        return response()->json(['message'=>'restore']);

    }
    public function multipleDelete(Request $request)
    {
        Role::whereIn('id',$request->ids)->update(['active'=>0]);
        return response()->json(['message'=>'Deleted']);
    }
    public function hardDelete(Request $request)
    {
        Role::where('id',$request->id)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'hard delet']);
    }
    public function getAjax(Request $request)
    {
        if(isset($request->role_id))
        {
            $role = CustomRole::findOrFail($request->role_id);
            $role=$role->customPermissions($request->id)->get()->toArray();
        }
        $permissionsArray=Permission::where('sub_module_name','operation')->where('module_name_id',$request->id)->orderBy('type','asc')->get();
        $permissionsArrayListing=Permission::where('sub_module_name','listing')->where('module_name_id',$request->id)->orderBy('order', 'asc')->get()->groupby('permission_name');
        $permissionsArrayQuestion=Permission::where('sub_module_name','Question')->where('module_name_id',$request->id)->orderBy('type','asc')->get();
        $permissionsArrayForm=Permission::where('sub_module_name','Form')->where('module_name_id',$request->id)->orderBy('type','asc')->get();
        return view('backend.roles.listing', compact('permissionsArray','permissionsArrayListing','role','permissionsArrayForm','permissionsArrayQuestion'));
    }
    public function edit(Request $request) {
        $role = Role::findOrFail($request->id);
        $companies=Company::getCompanies();
        $groups=PermissionGroup::with('permissionGroupModule.permissionModule')->orderBy('order','asc')->get();
        return view('backend.roles.edit_new', compact('role', 'groups','companies'));
        // $permissions = Permission::get()->groupby('group');
        // return view('backend.roles.edit', compact('role', 'permissions','companies'));
    }
    public function updateAjax(Request $request)
    {
        $role = Role::findOrFail($request->role_id);
        $permission = Permission::where('id',$request->permision_id)->first();
        if($request->flag == 1)
        {
            $role->givePermissionTo($permission);
            $flag='1';
        }
        else
        {
           $role->revokePermissionTo($permission);
           $flag='0';
        }
        return response()->json(['message'=>'Successfully Update !!','flag'=>$flag]);
    }
    public function update(Request $request, $id) {
        // ini_set('max_execution_time', 180);
        $role = Role::findOrFail($id);
        $this->validate($request, [
            'name'=>'required',
            'permissions' =>'required',
        ]);

        $input = $request->except(['permissions']);
        $permissions = $request['permissions'];
        $role->fill($input)->save();
        // $p_all = Permission::all();
        // foreach ($p_all as $p) {
        //     $role->revokePermissionTo($p);
        // }
        // foreach ($permissions as $permission) {
        //     $p = Permission::where('id', '=', $permission)->firstOrFail();
        //     $role->givePermissionTo($p);
        // }
        toastr()->success('Role '. $role->name.' updated!');
        return redirect('/roles')
            ->with('message',
             'Role '. $role->name.' updated!');
    }
    public function delete(Request $request)
    {
        Role::where('id',$request->id)->update(['active'=>0]);
        return response()->json(['message'=>'delete']);

    }
}
