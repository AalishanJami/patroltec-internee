<?php
namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Company;
use App\Location;
use App\Exports\scoring\ScoringExport;
use App\Exports\scoring\ScoringSoftExport;
use App\Scoring;
use PDF;
use Auth;
use Session;
use Config;
use Maatwebsite\Excel\Facades\Excel;
use Validator;
use DataTables;
class ScoreController extends Controller
{
    private $page_name;
    private $tab_name;
    private $db;
    private $cms_array=[];
    public function __construct() {
        $this->page_name ='score';
        $this->tab_name ='document_library';
        $this->db='scorings';
        $cms_data=localization();
        $temp_array=[];
        $temp_array['location_id']['key']='location';
        $temp_array['location_id']['value']=checkKey('location',$cms_data);
        $temp_array['score']['key']='score';
        $temp_array['score']['value']=checkKey('score',$cms_data);
        $temp_array['max_visits']['key']='max_visit';
        $temp_array['max_visits']['value']=checkKey('max_visit',$cms_data);
        $temp_array['dashboard_name']['key']='dashboard';
        $temp_array['dashboard_name']['value']=checkKey('dashboard',$cms_data);
        $temp_array['notes']['key']='notes';
        $temp_array['notes']['value']=checkKey('notes',$cms_data);
        $this->cms_array=$temp_array;
    }
    public function index()
    {
        $companies=Company::getCompanies();
        return view('backend.scoring.index')->with(['companies'=>$companies]);
    }

    public function getAll()
    {
        $locations=Location::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->with('score_location')->orderBy('id', 'desc')->get();
        $form_id=Session::get('form_id');
        $flag_check=Scoring::where('form_id',$form_id)->where('active',1)->where('deleted',0)->where('all_location',1)->first();
        $data=Scoring::where('form_id',$form_id)->with('location')->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        return Datatables::of($data)->with(['flag_check'=>$flag_check,'all_location'=>$locations,'form_id'=>$form_id])
            ->addColumn('checkbox', function ($data) {})
            ->addColumn('location_id', function($data) use ($locations,$form_id,$flag_check){
                $location_edit='';
                $html='';
                $html=$html.'<input type="hidden" id="old_location' . $data->id . '" value="'.$data->location_id.'">';
                $html=$html.'<select '.$location_edit.' onchange="editSelectedRow(scoring_tr'.$data->id.')" class="location" searchable="Search here.."  id="location_id'. $data->id .'"  style="background-color: inherit;border: 0px">';
                $html=$html.'<option value=" ">Select Location</option>';
                if(!$flag_check)
                {
                    $html=$html.'<option value="0">All Location</option>';
                }
                if($data->all_location == 1)
                {
                    $html=$html.'<option value="0" selected >All Location</option>';
                }

                $locations=scoring($locations,$form_id,$data->location_id);
                foreach ($locations as $key => $value) {

                    $html=$html.'<option value="'.$value['id'].'" ';
                    $checked='';
                    if($data->location_id == $value['id'] )
                    {
                        $checked='selected';
                    }
                    $html=$html.$checked.'>'.$value['name'].'</option>';
                }
                $html=$html.'</select>';
                return $html;
            })
            ->addColumn('submit', function ($data) {})
            ->addColumn('flag', function ($data) { return "1";})
            ->addColumn('actions', function ($data) {
                $response = "";
                if(Auth()->user()->hasPermissionTo('delete_scoring') || Auth::user()->all_companies == 1 )
                {
                    $delete = '<a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light delete-btn' . $data->id . '" onclick="deletescoringdata( ' . $data->id . ')"><i class="fas fa-trash"></i></a>';
                    $response = $response . $delete;
                }
                return $response;
            })
            ->rawColumns(['actions','location_id'])
            ->make(true);
    }
    public function getAllSoft()
    {

        $locations=Location::where('active',1)->where('deleted',0)->with('score_location')->orderBy('id', 'desc')->get();
        $form_id=Session::get('form_id');
        $flag_check=Scoring::where('form_id',$form_id)->where('active',1)->where('deleted',0)->where('all_location',1)->first();
        $data=Scoring::where('form_id',$form_id)->where('active',0)->with('location')->where('deleted',0)->orderBy('id', 'desc')->get();
       return Datatables::of($data)->with(['flag_check'=>$flag_check,'all_location'=>$locations,'form_id'=>$form_id])
            ->addColumn('checkbox', function ($data) {})
            ->addColumn('location_id', function($data) use ($locations,$form_id,$flag_check){
                $location_edit='';
                $html='';
                $html=$html.'<input type="hidden" id="old_location' . $data->id . '" value="'.$data->location_id.'">';
                $html=$html.'<select '.$location_edit.' class="location" searchable="Search here.."  id="location_id'. $data->id .'"  style="background-color: inherit;border: 0px">';
                if(!$flag_check)
                {
                    $html=$html.'<option value="0">All Location</option>';
                }
                if($data->all_location == 1)
                {
                    $html=$html.'<option value="0" selected >All Location</option>';
                }

                $locations=scoring($locations,$form_id,$data->location_id);
                foreach ($locations as $key => $value) {

                    $html=$html.'<option value="'.$value['id'].'" ';
                    $checked='';
                    if($data->location_id == $value['id'] )
                    {
                        $checked='selected';
                    }
                    $html=$html.$checked.'>'.$value['name'].'</option>';
                }
                $html=$html.'</select>';
                return $html;
            })
            ->addColumn('submit', function ($data) {})
            ->addColumn('flag', function ($data) { return "2";})
            ->addColumn('actions', function ($data) {
                $response = "";
                if(Auth()->user()->hasPermissionTo('hard_delete_scoring') || Auth::user()->all_companies == 1 )
                {
                    $delete = '<a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light" onclick="deletesoftscoringdata(' . $data->id . ')"><i class="fas fa-trash"></i></a>';
                    $response = $response . $delete;
                }
                if(Auth()->user()->hasPermissionTo('restore_delete_scoring') || Auth::user()->all_companies == 1 )
                {
                    $restore = '<a type="button" class="btn btn-success all_action_btn_margin btn-xs my-0 waves-effect waves-light" onclick="restorescoringdata(' . $data->id . ')"><i class="fa fa-retweet" aria-hidden="true"></i></a>';
                    $response = $response . $restore;
                }
                return $response;
            })
            ->rawColumns(['actions','location_id'])
            ->make(true);
    }
    public function store()
    {
        $form_id=Session::get('form_id');
        $data=Scoring::create(['form_id'=>$form_id]);
        $locations=Location::where('active',1)->where('deleted',0)->with('score_location')->orderBy('id', 'desc')->get();
        $locations=scoring($locations,$form_id);
        $flag=Scoring::where('form_id',$form_id)->where('active',1)->where('deleted',0)->where('all_location',1)->first();
        audit_log($this->cms_array,'',Config::get('constants.store'),$this->page_name,$this->tab_name,$data->id,'',$data->id,$form_id,$this->db);
        return response()->json(['data'=>$data,'locations'=>$locations,'flag'=>$flag]);
    }
    public function update(Request $request)
    {
        if ($request->old_location !== $request->location_id){
            $data = [
                'company_id'=>'required|numeric',
                'score'=>'required|numeric',
                'max_visits'=>'required|numeric',
                'location_id'=>'required|numeric',
            ];
        }else{
            $data = [
                'company_id'=>'required|numeric',
                'score'=>'required|numeric',
                'max_visits'=>'required|numeric',
                'location_id'=>'required|numeric',
            ];
        }

        $validator = Validator::make($request->all(), $data);
        if ($validator->fails())
        {
            $error = $validator->messages();
            $html='<ul>';
            foreach ($error->all() as $key => $value) {
                $html=$html.'<li>'.$value.'</li>';
            }
            $html=$html.'</ul>';
            return response()->json(['status'=> false, 'error'=> $html]);
        }
        $data=$request->all();
        unset($data['old_location']);
        if($data['location_id'] == 0)
        {
            unset($data['location_id']);
            $data['all_location']=1;
        }
        $scoring=Scoring::where('id',$data['id']);
        $bonus=$scoring->first();
        $scoring->update($data);
        if($bonus->active == 0)
        {
            $flag=0;
        }
        else
        {
            $flag=1;
        }
        $form_id=Session::get('form_id');
        $input=$request->all();
        audit_log($this->cms_array,audit_log_data($input),Config::get('constants.update'),$this->page_name,$this->tab_name,$bonus->id,audit_log_data($bonus->toArray()),$bonus->id,$form_id,$this->db);
        return response()->json(['message'=>'Successfully Added !!','flag'=>$flag]);

    }
    public function softDelete (Request $request)
    {
        Scoring::where('id',$request->id)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function delete(Request $request)
    {
        Scoring::where('id',$request->id)->update(['active'=>0,'deleted'=>0]);
    audit_log($this->cms_array,'',Config::get('constants.delete'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('project_id'),$this->db);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function active(Request $request)
    {
        Scoring::where('id',$request->id)->update(['active'=>1,'deleted'=>0]);
    audit_log($this->cms_array,'',Config::get('constants.restore'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('project_id'),$this->db);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
    public function activeMultiple(Request $request)
    {
        Scoring::whereIn('id',$request->ids)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
     public function deleteMultipleSOft(Request $request)
    {
        Scoring::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function deleteMultiple(Request $request)
    {
        Scoring::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function exportPdf(Request $request)
    {
        $form_id=Session::get('form_id');
        if($request->pdf_array == 1)
        {
           $data= Scoring::with('location')->where('form_id',$form_id)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->pdf_array);
            $data= Scoring::with('location')->where('form_id',$form_id)->whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        $companies='Patroltec';
        $reportname='Scoring';
        $date=\Carbon\Carbon::now()->format('D jS M Y');
        $pdf = PDF::loadView('backend.scoring.pdf',compact('companies','reportname','data','date'));
        return $pdf->download('scoring.pdf');
    }

    public function exportWorld(Request $request)
    {
        $form_id=Session::get('form_id');
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        if($request->word_array == 1)
        {
            $data= Scoring::with('location')->where('form_id',$form_id)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->word_array);
            $data= Scoring::with('location')->where('form_id',$form_id)->whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        $table = $section->addTable();
        foreach ($data as $key => $value) {
            if($key==0)
            {
                $table->addRow();
                if(Auth()->user()->hasPermissionTo('scoring_location_word_export') || Auth::user()->all_companies == 1 )
                {
                    $table->addCell(1750)->addText('Location');
                }
                if(Auth()->user()->hasPermissionTo('scoring_score_word_export') || Auth::user()->all_companies == 1 )
                {
                    $table->addCell(1750)->addText('Score');
                }
                if(Auth()->user()->hasPermissionTo('scoring_max_visit_word_export') || Auth::user()->all_companies == 1 )
                {
                    $table->addCell(1750)->addText('Max Visits');
                }
                if(Auth()->user()->hasPermissionTo('scoring_dashbroad_word_export') || Auth::user()->all_companies == 1 )
                {
                    $table->addCell(1750)->addText('Dashboard Name');
                }
                if(Auth()->user()->hasPermissionTo('scoring_notes_word_export') || Auth::user()->all_companies == 1 )
                {
                    $table->addCell(1750)->addText('Notes');
                }

            }
            $table->addRow();
            if(Auth()->user()->hasPermissionTo('scoring_location_word_export') || Auth::user()->all_companies == 1 )
            {
                $location='All Location';
                if(isset($value->location->name))
                {
                    $location=$value->location->name;
                }
                $table->addCell(1750)->addText($location);
            }
            if(Auth()->user()->hasPermissionTo('scoring_score_word_export') || Auth::user()->all_companies == 1 )
            {
                $table->addCell(1750)->addText($value->score);
            }
            if(Auth()->user()->hasPermissionTo('scoring_max_visit_word_export') || Auth::user()->all_companies == 1 )
            {
                $table->addCell(1750)->addText($value->max_visits);
            }
            if(Auth()->user()->hasPermissionTo('scoring_dashbroad_word_export') || Auth::user()->all_companies == 1 )
            {
                $table->addCell(1750)->addText($value->dashboard_name);
            }
            if(Auth()->user()->hasPermissionTo('scoring_notes_word_export') || Auth::user()->all_companies == 1 )
            {
                $table->addCell(1750)->addText($value->notes);
            }

        }
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('scoring.docx');
        return response()->download(public_path('scoring.docx'));
    }
    public function exportExcel(Request $request)
    {
        $form_id=Session::get('form_id');
        if($request->excel_array == 1)
        {
            $data= Scoring::with('location')->where('form_id',$form_id)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->excel_array);
            $data= Scoring::with('location')->where('form_id',$form_id)->whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        return Excel::download(new ScoringExport($data), 'scoring.csv');
    }
    public function detail()
    {
    	return view('backend.project.details');
    }

}
