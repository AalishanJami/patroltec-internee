<?php
namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Company;
use App\SiaLicence;
use DataTables;
use Validator;
use App\Exports\sia_license\SiaLicenseExport;
use PDF;
use Session;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;
use Auth;
class LicenceController extends Controller
{
    public function index()
    {
    	$companies=Company::getCompanies();
    	return view('backend.sia_licence.index')->with(['companies'=>$companies]);
    }
    public function getAll()
    {
        $data=SiaLicence::where('user_id',Session::get('employee_id'))->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        return Datatables::of($data)
        		->addColumn('checkbox', function ($data) {})
        		->addColumn('end', function ($data) {
        			return Carbon::parse($data->end)->format('jS M Y');
        		})
                ->addColumn('actions', function ($data) {
                    $response = "";
                    if(Auth()->user()->hasPermissionTo('edit_licence') || Auth::user()->all_companies == 1 ){
                        $edit = '<a onClick="getsia_licencedata( ' . $data->id . ')" id="' . $data->id . '" class="btn btn-primary btn-xs" ><i class="fas fa-pencil-alt"></i></a>';
                    }else{
                        $edit = '';
                    }
                    if(Auth()->user()->hasPermissionTo('delete_licence') || Auth::user()->all_companies == 1 ){
                        $delete = '<a href="javascript:;" type="button"class="btn btn-danger btn-xs my-0" onclick="deletesia_licencedata( ' . $data->id . ')"><i class="fas fa-trash"></i></a>';
                    }
                    $response = $response . $edit;
                    $response = $response . $delete;
                    return $response;
                })
                ->rawColumns(['actions'])
                ->make(true);
    }
    public function edit(Request $request)
    {
        $sia_licence=SiaLicence::where('id',$request->id)->where('active',1)->where('deleted',0)->first();
        return view('backend.sia_licence.edit')->with(['sia_licence'=>$sia_licence]);
    }
    public function getAllSoft()
    {
        $data=SiaLicence::where('user_id',Session::get('employee_id'))->where('active',0)->where('deleted',0)->orderBy('id', 'desc')->get();
        return Datatables::of($data)
        		->addColumn('checkbox', function ($data) {})
        		->addColumn('end', function ($data) {
        			return Carbon::parse($data->end)->format('jS M Y');
        		})
                ->addColumn('actions', function ($data) {
                    $response = "";
                    if(Auth()->user()->hasPermissionTo('hard_delete_licence') || Auth::user()->all_companies == 1 ){
                        $restore = '<a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light" onclick="deletesoftsia_licencedata('.$data->id.')"><i class="fas fa-trash"></i></a>';
                    }else{
                        $restore = '';
                    }

                    if(Auth()->user()->hasPermissionTo('restore_delete_licence') || Auth::user()->all_companies == 1 ){
                        $delete = '<a type="button" class="btn btn-success btn-xs my-0 waves-effect waves-light" onclick="restoresia_licencedata('.$data->id.')"><i class="fa fa-retweet" aria-hidden="true"></i></a>';
                    }
                    $response = $response . $restore;
                    $response = $response . $delete;
                    return $response;
                })
                ->rawColumns(['actions'])
                ->make(true);
    }
    public function store(Request $request)
    {
        $data = [
            'company_id'=>'required|numeric',
            'first_name'=>'required',
            'licence'=>'max:3',
        ];
        $validator = Validator::make($request->all(), $data);
        if ($validator->fails())
        {
            $error = $validator->messages();
            $html='<ul>';
            foreach ($error->all() as $key => $value) {
                $html=$html.'<li>'.$value.'</li>';
            }
            $html=$html.'</ul>';
            return response()->json(['status'=> false, 'error'=> $html]);
        }

        $input=[
        	'name'=>$request->name,
        	'contact'=>$request->contact,
        	'user_id'=>Session::get('employee_id'),
        	'type'=>$request->status,
        	'sector'=>$request->licence_sector,
        	'licence_loc'=>$request->licence,
        	'end'=>Carbon::parse($request->expiry),
        	'role'=>$request->role,
        	'notes'=>$request->notes,
        	'sia_number'=>$request->licence_number,
        	'first_name'=>$request->first_name,
        	'surname'=>$request->surname,
        	'sia_reference'=>$request->reference_number,
        	'tracking_number'=>$request->tracking_number,
        	'company_id'=>$request->company_id,

        ];
       	SiaLicence::create($input);
        return response()->json(['message'=>'Save Data Successfully..!!']);
    }
    public function update(Request $request)
    {
        $data = [
                'company_id'=>'required|numeric',
                'first_name'=>'required',
                'licence'=>'max:3',
                ];
        $validator = Validator::make($request->all(), $data);
       	$validator = Validator::make($request->all(), $data);
        if ($validator->fails())
        {
            $error = $validator->messages();
            $html='<ul>';
            foreach ($error->all() as $key => $value) {
                $html=$html.'<li>'.$value.'</li>';
            }
            $html=$html.'</ul>';
            return response()->json(['status'=> false, 'error'=> $html]);
        }
        $input=[
        	'name'=>$request->name,
        	'contact'=>$request->contact,
        	'user_id'=>Session::get('employee_id'),
        	'type'=>$request->status,
        	'sector'=>$request->licence_sector,
        	'licence_loc'=>$request->licence,
        	'end'=>Carbon::parse($request->expiry),
        	'role'=>$request->role,
        	'notes'=>$request->notes,
        	'sia_number'=>$request->licence_number,
        	'first_name'=>$request->first_name,
        	'surname'=>$request->surname,
        	'sia_reference'=>$request->reference_number,
        	'tracking_number'=>$request->tracking_number,
        	'company_id'=>$request->company_id,

        ];
        SiaLicence::where('id',$request->id)->update($input);
        return response()->json(['message'=>'Successfully update !!']);

    }
    public function softDelete (Request $request)
    {
        SiaLicence::where('id',$request->id)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function delete(Request $request)
    {
        SiaLicence::where('id',$request->id)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function active(Request $request)
    {
        SiaLicence::where('id',$request->id)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
    public function activeMultiple(Request $request)
    {
        SiaLicence::whereIn('id',$request->ids)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
     public function deleteMultipleSOft(Request $request)
    {
        SiaLicence::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function deleteMultiple(Request $request)
    {
        SiaLicence::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function exportPdf(Request $request)
    {
        if($request->pdf_array == 1)
        {
           $data= SiaLicence::where('user_id',Session::get('employee_id'))->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->pdf_array);
            $data= SiaLicence::where('user_id',Session::get('employee_id'))->whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        $pdf = PDF::loadView('backend.sia_licence.pdf',compact('data'));
        return $pdf->download('SiaLicence.pdf');
    }

    public function exportWorld(Request $request)
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        if($request->word_array == 1)
        {
            $data= SiaLicence::where('user_id',Session::get('employee_id'))->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->word_array);
            $data= SiaLicence::where('user_id',Session::get('employee_id'))->whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        $table = $section->addTable();
        foreach ($data as $key => $value) {
            if($key==0)
            {
                $table->addRow();
                if(Auth()->user()->hasPermissionTo('licence_firstname_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Name');
                }
                if(Auth()->user()->hasPermissionTo('licence_licence_ldn_ref_no_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Service');
                }
                if(Auth()->user()->hasPermissionTo('licence_status_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Status');
                }
                if(Auth()->user()->hasPermissionTo('licence_licence_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Licence');
                }
                if(Auth()->user()->hasPermissionTo('licence_end_date_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Expiry');
                }
                if(Auth()->user()->hasPermissionTo('licence_licence_no_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Licence Number');
                }
            }
            $table->addRow();
            if(Auth()->user()->hasPermissionTo('licence_firstname_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText($value->name);
            }
            if(Auth()->user()->hasPermissionTo('licence_licence_ldn_ref_no_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText($value->licence_loc);
            }
            if(Auth()->user()->hasPermissionTo('licence_status_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText($value->type);
            }
            if(Auth()->user()->hasPermissionTo('licence_licence_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText($value->sia_reference);
            }
            if(Auth()->user()->hasPermissionTo('licence_end_date_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText(Carbon::parse($value->end)->format('jS M Y'));
            }
            if(Auth()->user()->hasPermissionTo('licence_licence_no_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText($value->sia_number);
            }
        }
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('SiaLicence.docx');
        return response()->download(public_path('SiaLicence.docx'));
    }
    public function exportExcel(Request $request)
    {
    	 if($request->excel_array == 1)
        {
            $data= SiaLicence::where('user_id',Session::get('employee_id'))->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
            return Excel::download(new SiaLicenseExport($data), 'SiaLicence.csv');
        }
        else
        {
            $array=json_decode($request->excel_array);
            $data= SiaLicence::where('user_id',Session::get('employee_id'))->whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
            return Excel::download(new SiaLicenseExport($data), 'SiaLicence.csv');
        }
    }

}

