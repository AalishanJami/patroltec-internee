<?php

namespace App\Http\Controllers\backend;
use App\EthinicOrigins;
use App\Http\Controllers\Controller;
use App\SexualOrientations;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Session;
use DataTables;
use PDF;
use Config;
use App\Exports\employee\EthnicExport;
use Auth;
use Carbon\Carbon;
class EthnicOriginController extends Controller
{
    private $page_name;
    private $tab_name;
    private $db;
    private $cms_array=[];
    public function __construct() {
        $this->page_name ='ethnic';
        $this->tab_name ='main_detail';
        $this->db='ethinic_origins';
        $cms_data=localization();
        $temp_array=[];
        $temp_array['name']['key']='name';
        $temp_array['name']['value']=checkKey('name',$cms_data);
        $this->cms_array=$temp_array;
    }
    public function store()
    {
        $data=EthinicOrigins::create(['name'=>'']);
    audit_log($this->cms_array,'',Config::get('constants.store'),$this->page_name,$this->tab_name,$data->id,'',$data->id,Session::get('employee_id'),$this->db);
        return response()->json(['data'=>$data]);
    }

    public function update(Request $request)
    {
        $data = [
            'company_id'=>'required|numeric',
            'name'=>'required',
        ];
        $validator = Validator::make($request->all(), $data);
        if ($validator->fails())
        {
            $error = $validator->messages();
            $html='<ul>';
            foreach ($error->all() as $key => $value) {
                $html=$html.'<li>'.$value.'</li>';
            }
            $html=$html.'</ul>';
            return response()->json(['status'=> false, 'error'=> $html]);
        }
        $data=$request->all();
        $ethinic=EthinicOrigins::where('id',$data['id']);
        $ethinic_first=$ethinic->first();
        audit_log($this->cms_array,audit_log_data($data),Config::get('constants.update'),$this->page_name,$this->tab_name,$ethinic_first->id,audit_log_data($ethinic_first->toArray()),$ethinic_first->id,Session::get('employee_id'),$this->db);
        $ethinic->update($data);
        if($ethinic_first->active == 0)
        {
            $flag=0;
        }
        else
        {
            $flag=1;
        }

        return response()->json(['message'=>'Successfully Added !!','flag'=>$flag]);
    }

    public function getAll()
    {
        $data =EthinicOrigins::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {
            })
            ->addColumn('submit', function ($data) {
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                if(Auth()->user()->hasPermissionTo('delete_employeeEthnic') || Auth::user()->all_companies == 1 ){
                    $delete = '<a type="button" class="btn deleteethinicdata'.$data->id .' btn-danger btn-xs my-0 waves-effect waves-light" onclick="deleteethinicdata( ' . $data->id . ')"><i class="fas fa-trash"></i></a>';
                }else{
                    $delete = '';
                }
                $response = $response . $delete;
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    public function getAllSoft()
    {
        $data =EthinicOrigins::where('company_id',Auth::user()->default_company)->where('active',0)->where('deleted',0)->orderBy('id', 'desc')->get();
        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {
            })
            ->addColumn('submit', function ($data) {
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                if(Auth()->user()->hasPermissionTo('restore_delete_employeeEthnic') || Auth::user()->all_companies == 1 ){
                    $restore='<a type="button" class="btn btn-success btn-xs my-0 waves-effect waves-light" onclick="restoreethinicdata('.$data->id.')"><i class="fa fa-retweet" aria-hidden="true"></i></a>';
                }else{
                    $restore='';
                }
                if(Auth()->user()->hasPermissionTo('hard_delete_employeeEthnic') || Auth::user()->all_companies == 1 ){
                    $delete = '<a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light" onclick="deletesoftethinicdata( ' . $data->id . ')"><i class="fas fa-trash"></i></a>';
                }else{
                    $delete='';
                }
                $response = $response . $restore . $delete;
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
    public function delete(Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.delete'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('employee_id'),$this->db);
        EthinicOrigins::where('id',$request->id)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function active(Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.restore'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('employee_id'),$this->db);
        EthinicOrigins::where('id',$request->id)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
    public function activeMultiple(Request $request)
    {
        EthinicOrigins::whereIn('id',$request->ids)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
    public function softDelete(Request $request){
    audit_log($this->cms_array,'',Config::get('constants.soft_delete'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('employee_id'),$this->db);
        EthinicOrigins::where('id',$request->id)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function deleteMultipleSOft(Request $request)
    {
        EthinicOrigins::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }

    public function deleteMultiple(Request $request){
        EthinicOrigins::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }

    public function exportPdf(Request $request){
        if($request->pdf_array == 1)
        {
           $data =EthinicOrigins::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->pdf_array);
            $data= EthinicOrigins::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        $date=Carbon::now()->format('D jS M Y');
        $companies='Patroltec';
        $reportname='Ethinic';
        $pdf = PDF::loadView('backend.employee.export.ethinic_pdf',compact('data','date','reportname','companies'));
        return $pdf->download('ethnic.pdf');
    }

    public function exportWord(Request $request){
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        if($request->word_array == 1)
        {
            $data= EthinicOrigins::where('company_id',Auth::user()->default_company)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->word_array);
            $data= EthinicOrigins::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        $table = $section->addTable();
        $row = $table->addRow();
        if(Auth()->user()->hasPermissionTo('employeeEthnic_name_word_export') || Auth::user()->all_companies == 1 ){
            $row->addCell()->addText('Name');
        }
        foreach ($data as $key => $value) {
            $row = $table->addRow();
            if(Auth()->user()->hasPermissionTo('employeeEthnic_name_word_export') || Auth::user()->all_companies == 1 ){
                $row->addCell()->addText($value->name);
            }
        }
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('ethnic.docx');
        return response()->download(public_path('ethnic.docx'));
    }

    public function exportExcel(Request $request){
        if($request->excel_array == 1)
        {
            $data= EthinicOrigins::where('company_id',Auth::user()->default_company)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();;
        } else {
            $array=json_decode($request->excel_array);
            $data= EthinicOrigins::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        return Excel::download(new EthnicExport($data), 'ethnic.csv');
    }
}
