<?php

namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\CompanyLogo;
use Auth;
class CompanyLogoController extends Controller
{
    public function update(Request $request)
    {
        $time = time();
        $image = $request->file('image');
        $file = $time.'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('logo');
        $image->move($destinationPath, $file);
        $company_logo=CompanyLogo::where('company_id',Auth::user()->default_company);
        if($company_logo->first())
        {
            $company_logo->update(['logo'=>$file]);
        }
        else
        {
            CompanyLogo::create(['company_id'=>Auth::user()->default_company,'logo'=>$file]);
        }
        toastr()->success('Logo SuccessFully updated!');
        return redirect()->back();

    }
}
