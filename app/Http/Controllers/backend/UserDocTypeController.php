<?php

namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use App\UserDocType;
use App\DocType;
use App\Position;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Session;
use DataTables;
use PDF;
use Carbon\Carbon;
use App\Exports\user_doc_type\UserDocTypeExport;
use Auth;
class UserDocTypeController extends Controller
{
    public function getAll()
    {
        $doc_type=DocType::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->get();
        $positions=Position::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->get();
        $data=UserDocType::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->get();
        return Datatables::of($data)->with(['doc_type'=>$doc_type,'positions'=>$positions])
            ->addColumn('checkbox', function ($data) {
            })
            ->addColumn('submit', function ($data) {
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                $delete = '<a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light delete-btn'.$data->id.'" onclick="deleteuser_doc_typedata( ' . $data->id . ')"><i class="fas fa-trash"></i></a>';
                $response = $response . $delete;
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
    public function getAllSoft()
    {
        $doc_type=DocType::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->get();
        $positions=Position::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->get();
        $data=UserDocType::where('company_id',Auth::user()->default_company)->where('active',0)->where('deleted',0)->get();
        return Datatables::of($data)->with(['doc_type'=>$doc_type,'positions'=>$positions])
            ->addColumn('checkbox', function ($data) {
            })
            ->addColumn('submit', function ($data) {
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                $restore_button = '<button type="button"class="btn btn-success btn-xs my-0" onclick="restoreuser_doc_typedata('.$data->id.')"><i class="fa fa-retweet" aria-hidden="true"></i></button>';
                $hard_delete_button = '<button type="button"class="btn btn-danger btn-xs my-0" onclick="deletesoftuser_doc_typedata('.$data->id.')"><i class="fa fa-trash" aria-hidden="true"></i></button>';

                $response = $response . $restore_button;
                $response = $response . $hard_delete_button;
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
    public function store()
    {
        $data=UserDocType::create();
        $positions=Position::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->get();
        $doc_type=DocType::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->get();
        return response()->json(['data'=>$data,'doc_type'=>$doc_type,'positions'=>$positions]);
    }

    public function getAllbyCustAndDivi(Request $request)
    {
        $data=UserDocType::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->where('division_id',$request->division_id)->where('customer_id',$request->customer_id)->orderBy('id','desc')->get();
        return response()->json(['data'=>$data,'flag'=>0]);
    }

    public function update(Request $request)
    {
        $data = [
            'doc_type_id' => 'required',
            'position_id' => 'required',
        ];
        $validator = Validator::make($request->all(), $data);
        if ($validator->fails())
        {
//            $error = $validator->messages();
            $errors = $validator->errors();
            $html='<ul>';
            if ($errors->first('position_id')){
                $html='<li>Position field required</li>';
            }
            if ($errors->first('doc_type_id')){
                $html='<li>Doc type field required</li>';
            }
            $html=$html.'</ul>';
            return response()->json(['status'=> false, 'error'=> $html]);
        }
        $doctyp = UserDocType::where('doc_type_id', $request->doc_type_id)->where('position_id', $request->position_id)->where('active',1)->where('deleted',0)->first();
        if (!empty($doctyp)){
            $html='<ul><li>Doc type already been taken</li></ul>';
            return response()->json(['status'=> false, 'error'=> $html]);
        }
        $data=$request->all();
        $user_doc_type=UserDocType::where('id',$data['id']);
        $user_doc_type_first=$user_doc_type->first();
        $user_doc_type->update($data);
        if($user_doc_type_first->active == 0)
        {
            $flag=0;
        }
        else
        {
            $flag=1;
        }
        return response()->json(['message'=>'Successfully Added !!','flag'=>$flag]);

    }
    public function softDelete (Request $request)
    {
        UserDocType::where('id',$request->id)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function delete(Request $request)
    {
        UserDocType::where('id',$request->id)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function active(Request $request)
    {
        UserDocType::where('id',$request->id)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
    public function activeMultiple(Request $request)
    {
        UserDocType::whereIn('id',$request->ids)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
     public function deleteMultipleSOft(Request $request)
    {
        UserDocType::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function deleteMultiple(Request $request)
    {
        UserDocType::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function exportPdf(Request $request)
    {
        if($request->pdf_array == 1)
        {
            $data =UserDocType::where('company_id',Auth::user()->default_company)->with('position','doc_type')->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->pdf_array);
            $data= UserDocType::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->with('position','doc_type')->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        $date=Carbon::now()->format('D jS M Y');
        $companies='Patroltec';
        $reportname='User Doc Type';
        $pdf = PDF::loadView('backend.user_doc_type.pdf',compact('data','date','reportname','companies'));
        return $pdf->download('User Doc Type.pdf');
    }
    public function exportWord(Request $request)
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        if($request->word_array == 1) {
            $data= UserDocType::where('company_id',Auth::user()->default_company)->with('position','doc_type')->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        } else {
            $array=json_decode($request->word_array);
            $data= UserDocType::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->with('position','doc_type')->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }

        $table = $section->addTable();
        foreach ($data as $key => $value) {
            if($key==0)
            {
                $table->addRow();
                $table->addCell(1750)->addText('Doc Type Name');
                $table->addCell(1750)->addText('Position Name');
            }
            $table->addRow();
            $table->addCell(1750)->addText($value->doc_type->name);
            $table->addCell(1750)->addText($value->position->name);
        }
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('UserDocType.docx');
        return response()->download(public_path('UserDocType.docx'));
    }
    public function exportExcel(Request $request){
        if($request->excel_array == 1) {
            $data= UserDocType::where('company_id',Auth::user()->default_company)->with('position','doc_type')->where('active', 1)->where('deleted', 0)->orderBy('id', 'desc')->get();
        } else {
            $array=json_decode($request->excel_array);
            $data= UserDocType::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->with('position','doc_type')->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        return Excel::download(new UserDocTypeExport($data), 'UserDocType.csv');
    }
}
