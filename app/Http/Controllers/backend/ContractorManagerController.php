<?php
namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Company;
use App\User;
use App\Exports\contractormanager\ContractorManagerExport;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use PDF;
use Auth;
use DataTables;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Hash;
use Validator;

class ContractorManagerController extends Controller
{

    public function getAll()
    {
        $data=User::where('active',1)->where('deleted',0)->orderBy('id','desc')->get();
        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {
            })
            ->addColumn('submit', function ($data) {
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                if(Auth()->user()->hasPermissionTo('delete_contractmanagerName') || Auth::user()->all_companies == 1 )
                {
                    $delete = '<a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light" onclick="deletecontratormanagerdata( ' . $data->id . ')"><i class="fas fa-trash"></i></a>';
                    $response = $response . $delete;
                }
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);

    }
    public function getAllSoft()
    {
        $data=User::where('active',0)->where('deleted',0)->orderBy('id','desc')->get();
            return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {
            })
            ->addColumn('submit', function ($data) {
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                if(Auth()->user()->hasPermissionTo('restore_delete_contractmanagerName') || Auth::user()->all_companies == 1 )
                {
                    $restore_button = '<button type="button"class="btn btn-success btn-xs my-0" onclick="restorecontratormanagerdata('.$data->id.')"><i class="fa fa-retweet" aria-hidden="true"></i></button>';
                    $response = $response . $restore_button;
                }
                if(Auth()->user()->hasPermissionTo('hard_delete_contractmanagerName') || Auth::user()->all_companies == 1 )
                {
                    $hard_delete_button = '<button type="button"class="btn btn-danger btn-xs my-0" onclick="deletesoftcontratormanagerdata('.$data->id.')"><i class="fa fa-trash" aria-hidden="true"></i></button>';
                    $response = $response . $hard_delete_button;
                }
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
    public function store()
    {
        $email ='dummyTest'.substr(md5(rand(0,5)),0,5).'@patroltec.net';
        $data = user::create(['email'=>$email,'name'=>'','password' => Hash::make("12345678")]);
        $id = $data->id;
        $email = $data->id.'@patroltec.net';
        $user_data = User::find($data->id)->update(['email'=>$email]);
        $userData = User::find($data->id);
        return response()->json(['message'=>'Added successfully','data'=>$data]);
    }
    public function update(Request $request)
    {
        if (isset($request->name)){
            $data = [
                'company_id'=>'required|numeric',
                'name'=>'required',
            ];
        }else{
            $data = [
                'company_id'=>'required|numeric',
            ];
        }
         $validator = Validator::make($request->all(), $data);
         if ($validator->fails())
         {
             $error = $validator->messages();
             $html='<ul>';
             foreach ($error->all() as $key => $value) {
                 $html=$html.'<li>'.$value.'</li>';
             }
             $html=$html.'</ul>';
             return response()->json(['status'=> false, 'error'=> $html]);
         }
        $data=$request->all();
        if (isset($request->name)){
            $data['name']=$request->name;
        }else{
            $data['name']='';
        }
        $User=User::where('id',$data['id']);
        $User_first=$User->first();
        $User->update($data);

        if($User_first->active == 0)
        {
            $flag=0;
        }
        else
        {
            $flag=1;
        }

        return response()->json(['message'=>'Successfully Added !!','flag'=>$flag]);

    }
    public function softDelete (Request $request)
    {
        User::where('id',$request->id)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function delete(Request $request)
    {
        User::where('id',$request->id)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function active(Request $request)
    {
        User::where('id',$request->id)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
    public function activeMultiple(Request $request)
    {
        User::whereIn('id',$request->ids)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
     public function deleteMultipleSOft(Request $request)
    {
        User::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function deleteMultiple(Request $request)
    {
        User::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function exportPdf(Request $request)
    {
        if($request->pdf_array == 1)
        {
            $data =User::where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->pdf_array);
            $data= User::whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        $pdf = PDF::loadView('backend.user.contractormanager.pdf',compact('data'));
        return $pdf->download('contractormanager.pdf');
    }
    public function exportWorld(Request $request)
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        if($request->word_array == 1)
        {
            $data= User::where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->word_array);
            $data= User::whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        $table = $section->addTable();
        foreach ($data as $key => $value) {
            if($key==0)
            {
                $table->addRow();
                if(Auth()->user()->hasPermissionTo('contractmanagerName_name_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Name');
                }
                if(Auth()->user()->hasPermissionTo('contractmanagerName_position_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Position');
                }
            }
            $table->addRow();
            if(Auth()->user()->hasPermissionTo('contractmanagerName_name_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText($value->name);
            }
            if(Auth()->user()->hasPermissionTo('contractmanagerName_position_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText($value->position);
            }
        }
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('contractormanager.docx');
        return response()->download(public_path('contractormanager.docx'));
    }
    public function exportExcel(Request $request)
    {
        if($request->excel_array == 1)
        {
            $data= User::where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->excel_array);
            $data= User::whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        return Excel::download(new ContractorManagerExport($data), 'contractormanager.csv');
    }
}
