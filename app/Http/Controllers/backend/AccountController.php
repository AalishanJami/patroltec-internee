<?php
namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use Validator;
use App\CustomRole;
use App\CompanyUser;
use App\Company;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use App\User;
use Auth;
use Config;
class AccountController extends Controller
{
    private $page_name;
    private $tab_name;
    private $db;
    private $cms_array=[];
    public function __construct() {
        $this->page_name ='account';
        $this->tab_name ='employees';
        $this->db='users';
        $cms_data=localization();
        $temp_array=[];
        $temp_array['name']['key']='name';
        $temp_array['name']['value']=checkKey('name',$cms_data);
        $temp_array['company']['key']='company';
        $temp_array['company']['value']=checkKey('company',$cms_data);
        $temp_array['roles']['key']='roles';
        $temp_array['roles']['value']=checkKey('roles',$cms_data);
        $this->cms_array=$temp_array;
    }
    public function index()
    {
        $companies=Company::getCompanies();
        $roles = Role::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        $roles=sortRoleData($roles);
        $user=User::getUser()->where('users.id',Session::get('employee_id'))->first();
        $companyUser=CompanyUser::where('user_id',$user->id)->get();
        $company_id=[];
        foreach ($companyUser as $key => $value) {
            array_push($company_id,$value->company_id);
        }
        return view('backend.account.index')->with(['companies'=>$companies,'user'=>$user,'roles'=>$roles,'company_id'=>$company_id]);
    }
    public function update(Request $request)
    {
        $data = [
            'name' => 'required',
        ];
        $validator = Validator::make($request->all(), $data);
        if ($validator->fails())
        {
            return back()->withErrors($validator)->withInput();
        }
        $input=$request->all();
        unset($input['user_id']);
        unset($input['companies']);
        $data = User::where('id',Session::get('employee_id'));
        if(isset($input['password']))
        {
            $data->update(['password'=>Hash::make($input['password'])]);
        }
        $user_data=$data->first();
        $user_input=$input;
        unset($user_input['_token']);
        unset($user_input['password']);
        unset($user_input['roles']);
        if(isset($request['roles']))
        {
            if($request['roles'][0] == 0)
            {
                $name='Super Admin';
            }
            else
            {
                $name=CustomRole::where('id',$request['roles'][0])->first();
                $name=$name->name;
            }
            $user_input['roles']=$name;
        }
        $old_companies_name=[];
        $old_companies=CompanyUser::with('company')->where('user_id',$request->user_id);
        foreach ($old_companies->get() as $key => $value) {
            $old_companies_name['company-'.$key]=$value->company->name;
        }
        $old_companies->delete();
        foreach ($request->companies as $key => $value) {
            $temp=['user_id'=>$request->user_id,'company_id'=>$value];
            CompanyUser::create($temp);
            $company=Company::where('id',$value)->first();
            $user_input['company-'.$key]=$company->name;
        }
        $user_data=$user_data->toArray();
        $user_data=array_merge($user_data,$old_companies_name);
        audit_log($this->cms_array,audit_log_data($user_input),Config::get('constants.update'),$this->page_name,$this->tab_name,Session::get('employee_id'),audit_log_data($user_data),Session::get('employee_id'),Session::get('employee_id'),$this->db);
        $data->update(['name'=>$input['name']]);
        $user=$data->first();
        $roles = $request['roles'];
        // dd($request->all(),$user->roles()->get(),Auth::user()->default_company);
        if(isset($roles))
        {
            foreach ($user->roles()->get() as $key => $value) {
                if(Auth::user()->default_company != $value->company_id)
                {
                    array_push($roles,$value->id);
                    // dd($value,$value->id,$roles);
                }
            }
            foreach ($roles as $key => $value) {
                if($value == '0')
                {
                    User::where('id',Session::get('employee_id'))->update(['all_companies'=>1]);
                    $user->roles()->detach();
                    toastr()->success('Acount Information update!');
                    return redirect('/account');
                }
            }
            User::where('id',Session::get('employee_id'))->update(['all_companies'=>0]);
            if (isset($roles)) {
                $user->roles()->sync($roles);
            }
            else {
                $user->roles()->detach();
            }
        }
        toastr()->success('Acount Information update!');
        return redirect('/account');
    }
}
