<?php
namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\LocationAddress;
use Spatie\Permission\Models\Role;
use App\Company;
use App\CompanyUser;
use App\AddressType;
use Auth;
use App\Exports\LocationAddressType\LocationAddressExport;
use App\Exports\LocationAddressType\LocationAddressSoft;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use PDF;
use DataTables;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Hash;
use Session;
use Validator;
use Config;

class AddressController extends Controller
{
    private $page_name;
    private $tab_name;
    private $db;
    private $cms_array=[];
    public function __construct() {
        $this->page_name ='address';
        $this->tab_name ='project';
        $this->db='location_addresses';
        $cms_data=localization();
        $temp_array=[];
        $temp_array['address_type_id']['key']='address_type';
        $temp_array['address_type_id']['value']=checkKey('address_type',$cms_data);
        $temp_array['telephone']['key']='number';
        $temp_array['telephone']['value']=checkKey('number',$cms_data);
        $temp_array['email']['key']='email';
        $temp_array['email']['value']=checkKey('email',$cms_data);
        $temp_array['name']['key']='building';
        $temp_array['name']['value']=checkKey('building',$cms_data);
        $temp_array['address_street']['key']='street';
        $temp_array['address_street']['value']=checkKey('street',$cms_data);
        $temp_array['town']['key']='town';
        $temp_array['town']['value']=checkKey('town',$cms_data);
        $temp_array['state']['key']='state';
        $temp_array['state']['value']=checkKey('state',$cms_data);
        $temp_array['country']['key']='country';
        $temp_array['country']['value']=checkKey('country',$cms_data);
        $temp_array['post_code']['key']='post_code';
        $temp_array['post_code']['value']=checkKey('post_code',$cms_data);
        $this->cms_array=$temp_array;
    }
    public function index()
    {
        $companies=Company::getCompanies();
        $addressType=AddressType::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        return view('backend.address.index')->with(['companies'=>$companies,'addressType'=>$addressType]);
    }

    public function addressType()
    {
        $addressType=AddressType::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        return response()->json(['addressType'=>$addressType]);
    }
    public function addressTypeModalSave(Request $request)
    {
        if (isset($request->email)){
            $data = [
                'email'=>'required|email',
            ];
        }else{
            $data = [];
        }

        $validator = Validator::make($request->all(), $data);
        if ($validator->fails())
        {
            $error = $validator->messages();
            $html= "";
            foreach ($error->all() as $key => $value) {
                $html=$value;
            }
            return response()->json(['status'=> false, 'error'=> $html]);
        }

        $input = $request->all();
        $input['location_id'] = Session::get('project_id');
        $address=LocationAddress::create($input);
        audit_log($this->cms_array,audit_log_data($input),Config::get('constants.store'),$this->page_name,$this->tab_name,$address->id,'',$address->id,Session::get('project_id'),$this->db);
        return response()->json(['message'=> 'Data Added']);
    }
    public function getAllLocations(Request $request)
    {
        $data =LocationAddress::where('company_id',Auth::user()->default_company)->where('location_id',Session::get('project_id'))->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->with('getAddressType')->get();
        return Datatables::of($data)->addColumn('checkbox', function($data){})->addColumn('addresstype', function($data){
            return $data->getAddressType->name;
        })->addColumn('actions', function($data){
            $response="";
            if(Auth()->user()->hasPermissionTo('edit_address') || Auth::user()->all_companies == 1 ){
                $edit ='<a  id="' . $data->id . '" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modalAddressEdit" onclick="locationEdit('.$data->id.')"><i class="fas fa-pencil-alt"></i></a>';
            }else{
                $edit ='';
            }
            if( Auth()->user()->hasPermissionTo('delete_address') || Auth::user()->all_companies == 1 ){
                $delete ='<a type="button"class="btn btn-danger btn-xs all_action_btn_margin" onclick="deleteLocation( '.$data->id . ')"><i class="fas fa-trash"></i></a>';
            }else{
                $delete ='';
            }
            $response=$response.$edit;
            $response=$response.$delete;
            return $response;
        })->rawColumns(['actions'])->make(true);

    }
    public function edit(Request $request)
    {
        $input = $request->all();
        $data = LocationAddress::where('id',$input['id'])->where('active',1)->get()->last();
        return response()->json($data);
    }
    public function  update(Request $request)
    {
        if (isset($request->email)){
            $data = [
                'email'=>'required|email',
            ];
        }
        $validator = Validator::make($request->all(), $data);
        if ($validator->fails())
        {
            $error = $validator->messages();
            $html= "";
            foreach ($error->all() as $key => $value) {
                $html=$value;
            }
            return response()->json(['status'=> false, 'error'=> $html]);
        }
        $input = $request->all();
        $location=LocationAddress::find($input['id']);
        audit_log($this->cms_array,audit_log_data($input),Config::get('constants.update'),$this->page_name,$this->tab_name,$location->id,audit_log_data($location->toArray()),$location->id,Session::get('project_id'),$this->db);
        $location->update($input);
        return response()->json(['message'=>'Updated']);
    }
    public function delete(Request $request)
    {
        audit_log($this->cms_array,'',Config::get('constants.delete'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('project_id'),$this->db);
        LocationAddress::where('id',$request->id)->update(['active'=>0]);
        return response()->json(['message'=>'delete']);

    }
    public function multipleDelete(Request $request)
    {
        if($request->flag==1)
        {
            LocationAddress::whereIn('id',$request->ids)->update(['active'=>0]);
            return response()->json(['message'=>'Deleted']);
        }else if ($request->flag==2)
        {
            LocationAddress::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>1]);
            return response()->json(['message'=>'Restore Deleted']);
        }
    }
    public function getAllSoftLocations()
    {
        $data =LocationAddress::where('company_id',Auth::user()->default_company)->where('location_id',Session::get('project_id'))->where('active',0)->where('deleted',0)->orderBy('id', 'desc')->get();
        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {
            })->addColumn('actions', function ($data) {
                $response = "";
                if(Auth()->user()->hasPermissionTo('restore_delete_address') || Auth::user()->all_companies == 1 ){
                    $Restore_button = '<a href="javascript:;" class="btn btn-success btn-xs all_action_btn_margin my-0" onclick="restoreLocation( ' . $data->id . ')"><i class="fa fa-retweet" aria-hidden="true"></i></button>';
                }else{
                    $Restore_button ='';
                }
                if( Auth()->user()->hasPermissionTo('hard_delete_address') || Auth::user()->all_companies == 1 ){
                    $hard_delete_button = '<a href="javascript:;" class="btn btn-danger btn-xs " onclick="hardDeleteLocation( ' . $data->id . ')"><i class="fa fa-trash" aria-hidden="true"></i></button>';
                }else{
                    $hard_delete_button ='';
                }
                $response = $response . $hard_delete_button;
                $response = $response . $Restore_button;
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);

    }
    public function restore(Request $request)
    {
        audit_log($this->cms_array,'',Config::get('constants.restore'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('project_id'),$this->db);
        LocationAddress::where('id',$request->id)->update(['active'=>1]);
        return response()->json(['message'=>'restore']);
    }
    public function multipleActive(Request $request)
    {
        LocationAddress::whereIn('id',$request->ids)->update(['active'=>1]);
        return response()->json(['message'=>'Deleted']);
    }
    public function hardDelete(Request $request)
    {
       audit_log($this->cms_array,'',Config::get('constants.soft_delete'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('project_id'),$this->db);
        LocationAddress::where('id',$request->id)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'hard delet']);
    }
    public function exportExcel(Request $request)
    {

        if($request->excel_array == 1)
        {
            $data=LocationAddress::where('location_id',Session::get('project_id'))->where('active',1)->where('deleted',0)->get();
            return Excel::download(new LocationAddressExport($data), 'location.csv');
        }
        else
        {
            $array=json_decode($request->excel_array);
            $data=LocationAddress::where('location_id',Session::get('project_id'))->whereIn('id',$array)->where('active',1)->where('deleted',0)->get();
            return Excel::download(new LocationAddressExport($data), 'location.csv');
        }

    }
    public function exportPdf(Request $request)
    {
        if($request->pdf_array == 1)
        {
            $data=LocationAddress::where('location_id',Session::get('project_id'))->where('active',1)->where('deleted',0)->get();
        }
        else
        {
            $array=json_decode($request->pdf_array);
            $data=LocationAddress::where('location_id',Session::get('project_id'))->whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        $companies='Patroltec';
        $reportname='Address';
        $date=\Carbon\Carbon::now()->format('D jS M Y');
        $pdf = PDF::loadView('backend.address.pdf',compact('companies','reportname','data','date'));
        return $pdf->download('address.pdf');
    }

    public function exportWord(Request $request)
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        if($request->word_array == 1)
        {
            $data= LocationAddress::where('location_id',Session::get('project_id'))->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->word_array);
            $data=LocationAddress::where('location_id',Session::get('project_id'))->whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        $table = $section->addTable();
        foreach ($data as $key => $value) {
            if($key==0)
            {
                $table->addRow();
                if(Auth()->user()->hasPermissionTo('address_building_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Name');
                }
                if(Auth()->user()->hasPermissionTo('address_addressType_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Street');
                }
                if(Auth()->user()->hasPermissionTo('address_phone_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('TelePhone');
                }
                if(Auth()->user()->hasPermissionTo('address_email_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Email');
                }
                if(Auth()->user()->hasPermissionTo('address_postcode_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Post Code');
                }

            }
            $table->addRow();
            if(Auth()->user()->hasPermissionTo('address_building_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText($value->name);
            }
            if(Auth()->user()->hasPermissionTo('address_addressType_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText($value->address_street);
            }
            if(Auth()->user()->hasPermissionTo('address_phone_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText($value->telephone);
            }
            if(Auth()->user()->hasPermissionTo('address_email_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText($value->email);
            }
            if(Auth()->user()->hasPermissionTo('address_postcode_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText($value->post_code);
            }
        }
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('address.docx');
        return response()->download(public_path('address.docx'));
    }


}





