<?php

namespace App\Http\Controllers\backend;
use App\Exports\upload\UploadExport;
use App\Http\Controllers\Controller;
use App\Upload;
use App\Imports\BulkImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\PrePopulate;
use App\Jobs;
use App\FormType;
use App\JobGroup;
use App\VersionLog;
use Carbon\Carbon;
use App\FormVersion;
use App\FormSections;
use App\Company;
use App\Questions;
use App\Answers;
use App\AnswerTypes;
use App\AnswerGroups;
use App\Response;
use App\ResponseGroups;
use App\PopulateResponse;
use App\Location;
use App\User;
use App\Forms;
use DB;
use PDF;
use DataTables;
use Auth;
use Validator;
use Config;
use Session;
use stdClass;
class UploadController extends Controller
{
    private $page_name;
    private $tab_name;
    private $db;
    private $cms_array=[];
    public function __construct() {
        $this->page_name ='upload';
        $this->tab_name ='document_library';
        $this->db='uploads';
        $cms_data=localization();
        $temp_array=[];
        $temp_array['title']['key']='title';
        $temp_array['title']['value']=checkKey('title',$cms_data);
        $temp_array['location_id']['key']='location';
        $temp_array['location_id']['value']=checkKey('location',$cms_data);
        $temp_array['user_id']['key']='user_name';
        $temp_array['user_id']['value']=checkKey('user_name',$cms_data);
        $temp_array['date']['key']='date';
        $temp_array['date']['value']=checkKey('date',$cms_data);
        $temp_array['time']['key']='time';
        $temp_array['time']['value']=checkKey('status',$cms_data);
        $temp_array['file']['key']='file';
        $temp_array['file']['value']=checkKey('file',$cms_data);
        $temp_array['location_breakdown_id']['key']='location_break_down';
        $temp_array['location_breakdown_id']['value']=checkKey('location_break_down',$cms_data);
        $temp_array['notes']['key']='notes';
        $temp_array['notes']['value']=checkKey('notes',$cms_data);
        $this->cms_array=$temp_array;
    }
    public function store(Request $request)
    {

        $input = $request->all();
        $form_id=Session::get('form_id');
        $input['completed'] = 0;
        if($input['flag']==1)
        {
            $upload =  new Upload();
            $path = $request->file('file')->store('uploads/clients');
            $upload->link_id = 1;
            $upload->link_name = "client";
            $upload->name = $input['title'];
            $upload->notes = $input['notes'];
            $upload->file_name = $path;
            $upload->original_file_name = $request->file('file')->getClientOriginalName();
            $upload->company_id = Auth::user()->default_company;
            $status=null;
            $upload->save();
            if (isset($input['location_breakdown_id'])){
                $input['location_breakdown_id']=json_encode($input['location_breakdown_id']);
            }
            if (isset($input['user_id'])){
                $input['user_id']=json_encode($input['user_id']);
            }
            unset($input['flag']);
        audit_log($this->cms_array,'',Config::get('constants.store'),$this->page_name,$this->tab_name,$upload->id,'',$upload->id,$form_id,$this->db);
        }
        else
        {
            if(!$request->file())
            {
                return response()->json(['status'=> 'validation', 'error'=>'file required']);
            }
            $data = [
                'file'=>'mimes:xls,csv,xlsx|required',
            ];
            $validator = Validator::make($request->all(), $data);
            if ($validator->fails())
            {
                $error = $validator->messages();
                $html='=';
                foreach ($error->all() as $key => $value) {
                    $html=$value;
                }
                $html=$html.'';
                return response()->json(['status'=> 'validation', 'error'=>$html]);
            }
            $upload =  new Upload();
            $path = $request->file('file')->store('uploads/clients');
            $upload->link_id = 2;
            $status=null;
            $upload->link_name = "document";
            $upload->name = $input['title'];
            $upload->notes = $input['notes'];
            $upload->status = $status;
            $upload->file_name = $path;
            $upload->original_file_name = $request->file('file')->getClientOriginalName();
            $upload->company_id = Auth::user()->default_company;

            $collection = Excel::toArray(new BulkImport,request()->file('file'));
            // dd($collection);
            $form_sections = FormVersion::where('form_id',$input['form_id'])->where('active',1)->where('deleted',0)->orderBy('id','desc')->get();
            $form = Forms::find($input['form_id']);
            $form_sections = $form_sections->groupBy('version_id')->first()->pluck('form_section_id');
            $form_section = array_unique($form_sections->toArray());

            $form_sections = FormSections::whereIn('id',$form_section)->with('questions.answer_type','questions.answer_group.answer')->orderBy('order','asc')->get();
            $cq = Questions::whereIn('section_id',$form_section)->get();
            //dd($cq);
            $sections = FormSections::whereIn('id',$form_section)->with('questions')->orderBy('order','asc')->get();

            //$newC = array_filter($this->array_remove_null($collection));
            //dd($collection);

            $newC = array_map('array_filter', $collection);
            $newC = array_filter( $newC );
            $points = collect();
            //dd($newC);
            foreach ($newC as $key => $imports) {
                foreach ($imports as $key2 => $import) {
                    foreach ($import as $key3 => $col) {
                        foreach ($sections as $key4 => $section) {
                            foreach ($section->questions as $key5 => $question) {
                                if(isset($question->import_name))
                                {
                                    if($col!=null && strcasecmp($col,$question->import_name)==0)
                                    {
                                        //dd($col,$question->import_name,$collection[$key][$key2][$key3]);
                                        // $point = new stdClass();

                                        $point= $key.'+'.$key2.'+'.$key3.'+'.$question->section_id.'+'.$question->id;
                                        $points->add($point);

                                    }
                                }

                            }

                        }
                    }


                }
                $test =[];
                if($points->isEmpty())
                {
                    $upload->status = 'No Jobs Extracted';
                    $upload->save();
                    return response()->json(['error_ms'=>'No fields found in excel sheet']);

                }
                $t=[];
                foreach ($points as $key => $point) {
                    $pts=explode("+",$point);
                    //dd($pts);
                    for ($i=$pts[1]; $i <=array_key_last($newC[$pts[0]]) ; $i++) {
                        $vr = $cq->find($pts[4]);
                        if(isset($newC[$pts[0]][$i][$pts[2]]))
                        {
                            if(isset($vr->import_name))
                            {



                                if($vr->import_name != $newC[$pts[0]][$i][$pts[2]])
                                {


                                    if(isset($t[$vr->import_name]))
                                    {
                                        array_push($t[$vr->import_name],$newC[$pts[0]][$i][$pts[2]].'+'.$pts[4].'+'.$pts[3]);
                                    }
                                    else{
                                        $t[$vr->import_name]=[];
                                        array_push($t[$vr->import_name],$newC[$pts[0]][$i][$pts[2]].'+'.$pts[4].'+'.$pts[3]);
                                        // array_push($t,$t[$vr->import_name]);


                                    }


                                }
                            }
                        }

                    }
                    array_push($test,$t);

                }
                //dd($t);
                $idate = $input['date'];
                $itime = $input['time'];
                $breakdown = [];

                if(isset( $input['location_breakdown_id'])){

                    $breakdown = $input['location_breakdown_id'];
                }

                if (is_array(array_first($t)))
                {


                    for ($i=0; $i < count(array_first($t)) ; $i++) {


                        if(isset($input['pre_populate_id']))
                        {
                            unset($input['pre_populate_id']);
                        }
                        $input['contractor_id'] = Location::find($input['location_id'])->contractor_id;
                        $input['user_group_id'] = User::find($input['user_id'])->user_group_id;
                        $form_sections = FormVersion::where('form_id',$input['form_id'])->where('active',1)->where('deleted',0)->orderBy('id','desc')->get();
                        $form = Forms::find($input['form_id']);
                        $form_sections = $form_sections->groupBy('version_id')->first()->pluck('form_section_id');
                        $form_section = array_unique($form_sections->toArray());
                        $form_sections = FormSections::whereIn('id',$form_section)->with('questionsS.answer_type','questionsS.answer_group.answer')->orderBy('order','asc')->get();


                        //dd($input['date']);
                        $date = Carbon::parse($idate);
                        $time = explode(':',$itime);
                        $date->setTime($time[0],$time[1]);
                        $input['date_time'] = $date;

                        if($form->lock_job_after==1)
                        {
                            $input['post_time'] = $date->addMinutes($form->lock_from);
                        }else  if($form->lock_job_after==2)
                        {
                            $input['post_time'] = $date->addHours($form->lock_from);
                        } else  if($form->lock_job_after==3)
                        {
                            $input['post_time'] = $date->addDays($form->lock_from);
                        } else  if($form->lock_job_after==4)
                        {
                            $input['post_time'] = $date->addWeeks($form->lock_from);
                        } else{
                            $input['post_time'] = $date;
                        }


                        if($form->lock_job_before==1)
                        {
                            $input['pre_time'] = $date->subMinutes($form->lock_to);
                        }else  if($form->lock_job_before==2)
                        {
                            $input['pre_time'] = $date->subHours($form->lock_to);
                        } else  if($form->lock_job_before==3)
                        {
                            $input['pre_time'] = $date->subDays($form->lock_to);
                        } else  if($form->lock_job_before==4)
                        {
                            $input['pre_time'] = $date->subWeeks($form->lock_to);
                        } else{
                            $input['pre_time'] = $date;
                        }

                        unset($input['_token']);
                        unset($input['date']);
                        unset($input['time']);
                        $input['title']=$form->name;

                        //dd($input);
                        $input['form_type_id'] = $form->type;
                        if(isset( $input['location_breakdown_id'])){
                            foreach ($breakdown as $value) {

                                $input['location_breakdown_id']=$value;
                                $input['company_id']= Auth::user()->default_company;
                                $jobGroup = JobGroup::create($input);
                                $input['job_group_id'] = $jobGroup->id;


                                $x = [];
                                foreach ($form_sections as $form_section) {

                                    $responseGroup = ResponseGroups::create($input);


                                    array_push($x,"1");
                                    $input['form_section_id'] = $form_section->id;
                                    $input['order'] = $form_section->order;
                                    unset($input['company_id']) ;
                                    $job = Jobs::Create($input);
                                    foreach ($t as $key => $value) {
                                        $pts=explode("+",$value[$i]);
                                        //dd($pts);
                                        if($form_section->id == $pts[2] )
                                        {
                                            $response = new Response();
                                            $response->response_group_id =$responseGroup->id;
                                            $response->form_section_id=$pts[2];
                                            $response->question_id=$pts[1];
                                            $response->answer_text =$pts[0];
                                            $response->job_id =$job->id;
                                            $response->save();
                                        }
                                    }




                                }


                            }

                        }
                        else
                        {
                            $input['company_id']= Auth::user()->default_company;
                            $jobGroup = JobGroup::create($input);
                            $input['job_group_id'] = $jobGroup->id;


                            $x = [];
                            foreach ($form_sections as $form_section) {
                                $responseGroup = ResponseGroups::create($input);


                                array_push($x,"1");
                                $input['form_section_id'] = $form_section->id;
                                $input['order'] = $form_section->order;
                                unset($input['company_id']) ;
                                $job = Jobs::Create($input);
                                foreach ($t as $key => $value) {
                                    $pts=explode("+",$value[$i]);
                                    //dd($pts);
                                    if($form_section->id == $pts[2] )
                                    {
                                        $response = new Response();
                                        $response->response_group_id =$responseGroup->id;
                                        $response->form_section_id=$pts[2];
                                        $response->question_id=$pts[1];
                                        $response->answer_text =$pts[0];
                                        $response->job_id =$job->id;
                                        $response->save();
                                    }
                                }




                            }

                        }




                    }
                }


            }

            if (isset($input['location_breakdown_id'])){
                $input['location_breakdown_id']=json_encode($input['location_breakdown_id']);
            }
            if (isset($input['user_id'])){
                $input['user_id']=json_encode($input['user_id']);
            }
        audit_log($this->cms_array,'',Config::get('constants.store'),$this->page_name,$this->tab_name,$upload->id,'',$upload->id,$form_id,$this->db);
        }

        $upload->status = 'Jobs Extracted';
        $upload->save();
        return response()->json(['message'=>'Updated successfully']);
    }

    public function updateUploadClient(Request $request){

        $input = $request->all();
        $upload_old = Upload::find($request->id);
        $upload =  Upload::find($request->id);
        $upload->name = $input['title'];
        $upload->notes = $input['notes_edit'];
        if (isset($request->file)){
            $path = $request->file('file')->store('uploads/clients');
            $upload->file_name = $path;
            $upload->original_file_name = $request->file('file')->getClientOriginalName();
        }
        if (isset($input['location_breakdown_id'])){
            $input['location_breakdown_id']=json_encode($input['location_breakdown_id']);
        }
        if (isset($input['user_id'])){
            $input['user_id']=json_encode($input['user_id']);
        }
        $upload->save();
        unset($input['notes_edit']);
        $form_id=Session::get('form_id');
        audit_log($this->cms_array,audit_log_data($input),Config::get('constants.update'),$this->page_name,$this->tab_name,$bonus->id,audit_log_data($upload_old->toArray()),$upload->id,$form_id,$this->db);
        return response()->json(['message'=>'Updated successfully']);
    }

    public function getAll(Request $request)
    {
        // dd($request->flag);

        $data =Upload::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->where('link_id',$request->flag)->orderBy('id','Desc')->get();

        return Datatables::of($data)->addColumn('checkbox', function($data){})->addColumn('actions',function($data){
            $response="";
            if ($data->link_name = "document"){
                if(Auth()->user()->hasPermissionTo('delete_documentUpload') || Auth::user()->all_companies == 1 ){
                    $delete ='<a href="javascript:;" class="btn btn-danger btn-xs my-0" onclick="deleteUpload( '.$data->id . ')"><i class="fas fa-trash"></i></a>';
                }else{
                    $delete ='';
                }
                if(Auth()->user()->hasPermissionTo('download_documentUpload') || Auth::user()->all_companies == 1 ){
                    $download ='<a href="'.url("/upload/download/orginal/".$data->id).'" class="btn btn-success all_action_btn_margin btn-xs mr-2"><i class="fas fa-download"></i></a>';
                }else{
                    $download ='';
                }
            }else{
                if(Auth()->user()->hasPermissionTo('delete_clientUpload') || Auth::user()->all_companies == 1 ){
                    $delete ='<a href="javascript:;" class="btn btn-danger btn-xs my-0" onclick="deleteUpload( '.$data->id . ')"><i class="fas fa-trash"></i></a>';
                }else{
                    $delete ='';
                }
                if(Auth()->user()->hasPermissionTo('download_clientUpload') || Auth::user()->all_companies == 1 ){
                    $download ='<a href="'.url("/upload/download/orginal/".$data->id).'" class="btn btn-success all_action_btn_margin btn-xs mr-2"><i class="fas fa-download"></i></a>';
                }else{
                    $download ='';
                }
            }

            if ($data->link_id=='1'){
                if(Auth()->user()->hasPermissionTo('edit_clientUpload') || Auth::user()->all_companies == 1 ){
                    $edit ='<a href="javascript:;" onclick="uploadEdit('.$data->id .')" class="btn btn-primary btn-xs mr-2"><i class="fas fa-pencil-alt"></i></a>';
                }else{
                    $edit ='';
                }
            }else{
                $edit ='';
            }
            //$edit ='<a  id="' . $data->id . '" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modalUploadEdit" onclick="uploadEdit('.$data->id.')"><i class="fas fa-pencil-alt"></i></a>';
            //$response=$response.$edit;
            $response=$response.$edit;
            $response=$response.$delete;
            $response=$response.$download;
            return $response;
        })->rawColumns(['actions'])->make(true);
    }
    public function getAllSoft(Request $request)
    {
        $data =Upload::where('company_id',Auth::user()->default_company)->where('active',0)->where('deleted',0)->orderBy('id', 'desc')->get();
        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {
            })->addColumn('actions', function ($data) {
                $response = "";
                if ($data->link_name = "document"){
                    if(Auth()->user()->hasPermissionTo('restore_delete_documentUpload') || Auth::user()->all_companies == 1 ){
                        $Restore_button = '<a href="javascript:;" class="btn btn-success all_action_btn_margin btn-xs my-0" onclick="restoreUpload( ' . $data->id . ')"><i class="fa fa-retweet" aria-hidden="true"></i></a>';
                    }else{
                        $Restore_button ='';
                    }
                    if(Auth()->user()->hasPermissionTo('hard_delete_documentUpload') || Auth::user()->all_companies == 1 ){
                        $hard_delete_button = '<a href="javascript:;" class="btn btn-danger btn-xs my-0" onclick="hardDeleteUpload( ' . $data->id . ')"><i class="fa fa-trash" aria-hidden="true"></i></a>';
                    }else{
                        $hard_delete_button ='';
                    }
                }else{
                    if(Auth()->user()->hasPermissionTo('restore_delete_clientUpload') || Auth::user()->all_companies == 1 ){
                        $Restore_button = '<a href="javascript:;" class="btn btn-success all_action_btn_margin btn-xs my-0" onclick="restoreUpload( ' . $data->id . ')"><i class="fa fa-retweet" aria-hidden="true"></i></a>';
                    }else{
                        $Restore_button ='';
                    }
                    if(Auth()->user()->hasPermissionTo('hard_delete_clientUpload') || Auth::user()->all_companies == 1 ){
                        $hard_delete_button = '<a href="javascript:;" class="btn btn-danger btn-xs my-0" onclick="hardDeleteUpload( ' . $data->id . ')"><i class="fa fa-trash" aria-hidden="true"></i></a>';
                    }else{
                        $hard_delete_button ='';
                    }
                }

                $response = $response . $hard_delete_button;
                $response = $response . $Restore_button;
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
    public function orginalDownload($id)
    {
        $file = Upload::find($id);

        $destinationPath = storage_path('app/'.$file->file_name);

        return response()->download($destinationPath,$file->original_file_name);
    }
    public function Edit(Request $request)
    {
        $input = $request->all();
        $data = Upload::where('id',$input['id'])->where('active',1)->get()->last();
        return response()->json($data);
    }
    public function update(Request $request)
    {
        $input = $request->all();
        $upload = Upload::find($input['id']);
        if (isset($input['location_breakdown_id'])){
            $input['location_breakdown_id']=json_encode($input['location_breakdown_id']);
        }
        if (isset($input['user_id'])){
            $input['user_id']=json_encode($input['user_id']);
        }
        $form_id=Session::get('form_id');
        audit_log($this->cms_array,audit_log_data($input),Config::get('constants.update'),$this->page_name,$this->tab_name,$upload->id,audit_log_data($upload->toArray()),$upload->id,$form_id,$this->db);
        $path = $request->file('file')->store('uploads/clients');
        $upload->name = $input['title'];
        $upload->file_name = $path;
        $upload->original_file_name = $request->file('file')->getClientOriginalName();
        $upload->update();
        return redirect()->back();
    }
    public function restore(Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.restore'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('form_id'),$this->db);
        Upload::where('id',$request->id)->update(['active'=>1]);
        return response()->json(['message'=>'restore']);
    }
    public function multipleActive(Request $request)
    {
        Upload::whereIn('id',$request->ids)->update(['active'=>1]);
        return response()->json(['message'=>'Activated']);
    }
    public function hardDelete(Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.soft_delete'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('form_id'),$this->db);
        Upload::where('id',$request->id)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'hard delet']);
    }
    public function delete(Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.delete'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('form_id'),$this->db);
        Upload::where('id',$request->id)->update(['active'=>0]);
        return response()->json(['message'=>'delete']);
    }


    public function multipleDelete(Request $request)
    {
        if($request->flag==1) {
            Upload::whereIn('id',$request->ids)->update(['active'=>0]);
            return response()->json(['message'=>'Deleted']);
        }else if ($request->flag==2) {
            Upload::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>1]);
            return response()->json(['message'=>'Deleted']);
        }
    }
    public function exportPdf(Request $request)
    {
        if($request->pdf_array == 1) {
            $data =Upload::where('company_id',Auth::user()->default_company)->where(['active'=>1,'deleted'=>0,'link_id'=>$request->link_id,'link_name'=>$request->link_name])->orderBy('id', 'desc')->get();
        } else {
            $array=json_decode($request->pdf_array);
            $data= Upload::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->where(['active'=>1,'deleted'=>0,'link_id'=>$request->link_id,'link_name'=>$request->link_name])->orderBy('id', 'desc')->get();
        }
        $flag=$request->link_id;
        $companies='Patroltec';
        $reportname='Upload';
        $date=\Carbon\Carbon::now()->format('D jS M Y');
        $pdf = PDF::loadView('backend.bulk_upload.pdf',compact('companies','reportname','data','date','flag'));
        return $pdf->download('upload.pdf');
    }
    public function exportWorld(Request $request){
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        if($request->word_array == 1)
        {
            $data =Upload::where('company_id',Auth::user()->default_company)->where(['active'=>1,'deleted'=>0,'link_id'=>$request->link_id,'link_name'=>$request->link_name])->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->word_array);
            $data= Upload::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->where(['active'=>1,'deleted'=>0,'link_id'=>$request->link_id,'link_name'=>$request->link_name])->orderBy('id', 'desc')->get();
        }

        $table = $section->addTable();
        foreach ($data as $key => $value) {
            if($key==0)
            {
                $row = $table->addRow();
                if($request->link_id == 2)
                {
                    if(Auth()->user()->hasPermissionTo('documentUpload_title_word_export') || Auth::user()->all_companies == 1)
                    {
                        $row->addCell()->addText('Title');
                    }
                    if(Auth()->user()->hasPermissionTo('documentUpload_notes_word_export') || Auth::user()->all_companies == 1)
                    {
                        $row->addCell()->addText('Note');
                    }
                }
                elseif($request->link_id == 1)
                {
                    if(Auth()->user()->hasPermissionTo('clientUpload_title_word_export') || Auth::user()->all_companies == 1)
                    {
                        $row->addCell()->addText('Title');
                    }
                    if(Auth()->user()->hasPermissionTo('clientUpload_notes_word_export') || Auth::user()->all_companies == 1)
                    {
                        $row->addCell()->addText('Note');
                    }
                }
            }
            $row = $table->addRow();
            if($request->link_id == 2)
            {
                if(Auth()->user()->hasPermissionTo('documentUpload_title_word_export') || Auth::user()->all_companies == 1)
                {
                    $row->addCell()->addText(strip_tags($value->name));
                }
                if(Auth()->user()->hasPermissionTo('documentUpload_notes_word_export') || Auth::user()->all_companies == 1)
                {
                    $row->addCell()->addText(strip_tags($value->notes));
                }
            }
            elseif($request->link_id == 1)
            {
                if(Auth()->user()->hasPermissionTo('clientUpload_title_word_export') || Auth::user()->all_companies == 1)
                {
                    $row->addCell()->addText(strip_tags($value->name));
                }
                if(Auth()->user()->hasPermissionTo('clientUpload_notes_word_export') || Auth::user()->all_companies == 1)
                {
                    $row->addCell()->addText(strip_tags($value->notes));
                }
            }
        }
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('uploads.docx');
        return response()->download(public_path('uploads.docx'));
    }
    public function exportExcel(Request $request){
        if($request->excel_array == 1)
        {
            $data =Upload::where('company_id',Auth::user()->default_company)->where(['active'=>1,'deleted'=>0,'link_id'=>$request->link_id,'link_name'=>$request->link_name])->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->excel_array);
            $data= Upload::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->where(['active'=>1,'deleted'=>0,'link_id'=>$request->link_id,'link_name'=>$request->link_name])->orderBy('id', 'desc')->get();
        }
        $flag=$request->link_id;
        return Excel::download(new UploadExport($data,$flag), 'upload.csv');
    }

    function array_remove_null($item)
    {
        if (!is_array($item)) {
            return $item;
        }
        return collect($item)
            ->reject(function ($item) {
                return is_null($item);
            })
            ->flatMap(function ($item, $key) {
                return is_numeric($key)
                    ? [$this->array_remove_null($item)]
                    : [$key => $this->array_remove_null($item)];
            })
            ->toArray();
    }
}
