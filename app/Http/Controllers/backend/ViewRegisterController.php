<?php
namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\FormSections;
use App\Company;
use App\Questions;
use App\Answers;
use App\AnswerTypes;
use App\AnswerGroups;
use App\Forms;
use Session;
use PDF;
use App\ViewRegisterPermision;
use Validator;
use Auth;
use App\VersionLog;
use Carbon\Carbon;
use App\FormVersion;
use App\Jobs;
use App\JobGroup;
use App\ResponseGroups;
use App\Response;
use DataTables;

class ViewRegisterController extends Controller
{
    public function index()
    {
        $companies=Company::getCompanies();
        $data=FormVersion::where('form_id',Session::get('form_id'))->where('active',1)->get();
        $array_temp=[];
        foreach ($data as $key => $value) {
            array_push($array_temp,$value->form_section_id);
        }
        $questions=Questions::whereIn('section_id',$array_temp)->get();
        // dd($questions);
        // $questions = FormSections::where('form_sections.id',$data->form_section_id)->where('form_sections.is_complete',1)->where('form_sections.active',1)->where('form_sections.deleted',0)->where('form_id',Session::get('form_id'))
        // ->join('questions', 'questions.section_id', '=', 'form_sections.id')
        // ->where('questions.active',1)
        // ->whereIn('questions.comment', array(NULL,''))
        // ->where('questions.deleted',0)
        // ->select('questions.*')->get();
        $questions=sortData($questions);
        return view('backend.view_register.index')->with('questions',$questions)->with('companies',$companies);
    }
    public function getData()
    {
        // $data=FormVersion::where('form_id',Session::get('form_id'))->where('active',1)->first();
        // $questions = FormSections::where('form_sections.id',$data->form_section_id)
        // ->where('form_sections.is_complete',1)->where('form_sections.active',1)->where('form_sections.deleted',0)->where('form_id',Session::get('form_id'))
        // ->join('questions', 'questions.section_id', '=', 'form_sections.id')
        // ->where('questions.active',1)
        // ->whereIn('questions.comment', array(NULL,''))
        // ->where('questions.deleted',0)
        // ->select('questions.*')->get();
        $data=FormVersion::where('form_id',Session::get('form_id'))->where('active',1)->get();
        $array_temp=[];
        foreach ($data as $key => $value) {
            array_push($array_temp,$value->form_section_id);
        }
        $questions=Questions::whereIn('section_id',$array_temp)->get();
        $questions=sortData($questions);
        $responseGroups = ResponseGroups::with('responses','locations','locationBreakdowns','users')->where('form_id',Session::get('form_id'))->get();
        $dummy = $responseGroups->pluck('id');
        $data = $responseGroups;
        $datatable = Datatables::of($data);
        $datatable->addColumn('completed_by', function($data){
            $response = $data->users['name'];
            if($response!=null)
                return $response;
        });
        $datatable->addColumn('location', function($data){
            $response = $data->locations['name'];
            if($response!=null)
                return $response;
        });
        $datatable->addColumn('date_completed', function($data){
            $response = Carbon::parse($data->created_at)->format('jS M Y');
            if($response!=null)
                return $response;
        });
        foreach ($questions as $key => $value) {
            $datatable->addColumn($value->question, function($data) use ($value){
                $text=$data->responses->where('question_id',$value->id)->last()['answer_text'];
                if($text!=null)
                {
                    return $text;
                }
                else
                {
                    $id=$data->responses->where('question_id',$value->id)->last()['answer_id'];
                    $temp=Answers::find($id)['answer'];
                    if(!$temp)
                    {
                        $temp='';
                    }
                    return $temp;
                }
            });
        }
        return $datatable->rawColumns([''])->make(true);
        // return $datatable->make(true);
    }
    public function updatePermision(Request $request)
    {
        $form_id=Session::get('form_id');
        ViewRegisterPermision::where('form_id',$form_id)->delete();
        if(isset($request->ids))
        {
            foreach ($request->ids as $key => $value) {
                ViewRegisterPermision::create(['question_id'=>$value,'form_id'=>$form_id]);
            }
        }
        return response()->json(['message'=>'Permision Successfully Updated!']);
    }
}
