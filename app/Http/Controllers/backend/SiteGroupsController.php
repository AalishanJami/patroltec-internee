<?php
namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Company;
use App\SiteGroups;
use App\Customers;
use App\Divisions;
use Auth;
use Config;
use App\Location;
use Session;
use App\Exports\site_group\SiteGroupExport;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use Carbon\Carbon;
use DataTables;
use PDF;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Hash;
use Validator;

class SiteGroupsController extends Controller
{
    private $page_name;
    private $tab_name;
    public function __construct() {
        $this->page_name ='group';
        $this->tab_name ='main_detail';
        $this->db='site_groups';
        $cms_data=localization();
        $temp_array=[];
        $temp_array['name']['key']='name';
        $temp_array['name']['value']=checkKey('name',$cms_data);
        $temp_array['division_id']['key']='devision';
        $temp_array['division_id']['value']=checkKey('devision',$cms_data);
        $temp_array['customer_id']['key']='customer';
        $temp_array['customer_id']['value']=checkKey('customer',$cms_data);
        $this->cms_array=$temp_array;
    }
    public function getAll(Request $request)
    {
    	$divisions=Divisions::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->get();
    	$customers=Customers::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->get();
        $data=SiteGroups::where('company_id',Auth::user()->default_company)->where('division_id',$request->division_id)->where('customer_id',$request->customer_id)->where('active',1)->where('deleted',0)->get();
        $project = Location::find(Session::get('project_id'));
        $data=sortData($data);
        return Datatables::of($data)->with(['divisions'=>$divisions,'customers'=>$customers,'project'=>$project])
            ->addColumn('checkbox', function ($data) {
            })
            ->addColumn('submit', function ($data) {
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                if(Auth()->user()->hasPermissionTo('delete_group') || Auth::user()->all_companies == 1 ){
                    $delete = '<a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light delete-btn' . $data->id . '" onclick="deletesitegroupdata( ' . $data->id . ')"><i class="fas fa-trash"></i></a>';
                }else{
                    $delete = '';
                }

                $response = $response . $delete;
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
    public function getAllSoft(Request $request)
    {
        $divisions=Divisions::where('company_id',Auth::user()->default_company)->where('active',0)->where('deleted',0)->get();
        $customers=Customers::where('company_id',Auth::user()->default_company)->where('active',0)->where('deleted',0)->get();
        $data=SiteGroups::where('company_id',Auth::user()->default_company)->where('division_id',$request->division_id)->where('customer_id',$request->customer_id)->where('active',0)->where('deleted',0)->get();
        $data=sortData($data);
        return Datatables::of($data)->with(['divisions'=>$divisions,'customers'=>$customers])
            ->addColumn('checkbox', function ($data) {
            })
            ->addColumn('submit', function ($data) {
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                if(Auth()->user()->hasPermissionTo('restore_delete_group') || Auth::user()->all_companies == 1 ){
                    $restore_button = '<button type="button"class="btn btn-success btn-xs my-0" onclick="restoresitegroupdata('.$data->id.')"><i class="fa fa-retweet" aria-hidden="true"></i></button>';
                }else{
                    $restore_button = '';
                }
                if(Auth()->user()->hasPermissionTo('hard_delete_group') || Auth::user()->all_companies == 1 ){
                    $hard_delete_button = '<button type="button"class="btn btn-danger btn-xs my-0" onclick="deletesoftsitegroupdata('.$data->id.')"><i class="fa fa-trash" aria-hidden="true"></i></button>';
                }else{
                    $hard_delete_button ='';
                }

                $response = $response . $restore_button;
                $response = $response . $hard_delete_button;
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
    public function store()
    {
        $data=SiteGroups::create(['name'=>'']);
    audit_log($this->cms_array,'',Config::get('constants.store'),$this->page_name,$this->tab_name,$data->id,'',$data->id,Session::get('project_id'),$this->db);
        $customers=Customers::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->get();
        $divisions=Divisions::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->get();
        return response()->json(['data'=>$data,'divisions'=>$divisions,'customers'=>$customers]);
    }
    public function getAllbyCustAndDivi(Request $request)
    {
        $data=SiteGroups::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->where('division_id',$request->division_id)->where('customer_id',$request->customer_id)->orderBy('id','desc')->get();
        $project = Location::find(Session::get('project_id'));
        return response()->json(['data'=>$data,'project'=>$project,'flag'=>0]);
    }

    public function update(Request $request)
    {
        $data=$request->all();
        if ($request->name){
            $data['name']=$request->name;
        }else{
            $data['name']='';
        }
        $SiteGroups=SiteGroups::where('id',$data['id']);
        $SiteGroups_first=$SiteGroups->first();
        audit_log($this->cms_array,audit_log_data($data),Config::get('constants.update'),$this->page_name,$this->tab_name,$SiteGroups_first->id,audit_log_data($SiteGroups_first->toArray()),$SiteGroups_first->id,Session::get('project_id'),$this->db);
        $SiteGroups->update($data);
        if($SiteGroups_first->active == 0)
        {
            $flag=0;
        }
        else
        {
            $flag=1;
        }
        return response()->json(['message'=>'Successfully Added !!','flag'=>$flag]);

    }
    public function softDelete (Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.soft_delete'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('project_id'),$this->db);
        SiteGroups::where('id',$request->id)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function delete(Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.delete'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('project_id'),$this->db);
        SiteGroups::where('id',$request->id)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function active(Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.restore'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('project_id'),$this->db);
        SiteGroups::where('id',$request->id)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
    public function activeMultiple(Request $request)
    {
        SiteGroups::whereIn('id',$request->ids)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
     public function deleteMultipleSOft(Request $request)
    {
        SiteGroups::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function deleteMultiple(Request $request)
    {
        SiteGroups::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function exportPdf(Request $request)
    {
        if($request->pdf_array == 1)
        {
            $data =SiteGroups::where('company_id',Auth::user()->default_company)->with('division','customer')->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
            $data=sortData($data);
        } else {
            $array=json_decode($request->pdf_array);
            $data= SiteGroups::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->with('division','customer')->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
            $data=sortData($data);
        }
        $date=Carbon::now()->format('D jS M Y');
        $companies='Patroltec';
        $reportname='Site Groups';
        $pdf = PDF::loadView('backend.site_groups.pdf',compact('data','date','reportname','companies'));
        return $pdf->download('sitegroup.pdf');
    }
    public function exportWorld(Request $request)
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        if($request->word_array == 1) {
            $data= SiteGroups::where('company_id',Auth::user()->default_company)->with('division','customer')->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
            $data=sortData($data);
        } else {
            $array=json_decode($request->word_array);
            $data= SiteGroups::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->with('division','customer')->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
            $data=sortData($data);
        }

        $table = $section->addTable();
        foreach ($data as $key => $value) {
            if($key==0)
            {
                $table->addRow();
                if(Auth()->user()->hasPermissionTo('group_name_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Name');
                }
                if(Auth()->user()->hasPermissionTo('group_customer_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Customer');
                }
                if(Auth()->user()->hasPermissionTo('group_division_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Division');
                }
            }
            $table->addRow();
            if(Auth()->user()->hasPermissionTo('group_name_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText($value->name);
            }
            if(Auth()->user()->hasPermissionTo('group_customer_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText(isset($value->customer->name)?$value->customer->name : '');
            }
            if(Auth()->user()->hasPermissionTo('group_division_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText(isset($value->division->name)?$value->division->name : '');
            }
        }
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('sitegroup.docx');
        return response()->download(public_path('sitegroup.docx'));
    }

    public function exportExcel(Request $request){
        if($request->excel_array == 1) {
            $data= SiteGroups::where('company_id',Auth::user()->default_company)->with('division','customer')->where('active', 1)->where('deleted', 0)->orderBy('id', 'desc')->get();
            $data=sortData($data);
        } else {
            $array=json_decode($request->excel_array);
            $data= SiteGroups::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->with('division','customer')->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
            $data=sortData($data);
        }
        return Excel::download(new SiteGroupExport($data), 'sitegroup.csv');
    }
}

