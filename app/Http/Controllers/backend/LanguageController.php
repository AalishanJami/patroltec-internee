<?php
namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use App\Language;
use App\CompanyLabel;
use Config;
class LanguageController extends Controller
{
    private $page_name;
    private $tab_name;
    public function __construct() {
        $this->page_name ='web_cms';
        $this->tab_name ='web_cms';
    }
    public function index()
    {
        return view('Backend.Language.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function changeText(Request $request)
    {
        $keyGet=CompanyLabel::with('labels')->where('id',$request->data_id)->first();
        $cms_array=[];
        $cms_array[$keyGet->labels->key]['key']=$keyGet->labels->key;
        $cms_array[$keyGet->labels->key]['value']=$keyGet->value;
        $input=$request->all();
        $input[$keyGet->labels->key]=$input['data_value'];
        unset($input['_token']);
        unset($input['data_value']);
        unset($input['data_id']);
        $label=CompanyLabel::find($request->data_id);
        $old_label[$keyGet->labels->key]=$label->value;
        audit_log($cms_array,$input,Config::get('constants.update'),$this->page_name,$this->tab_name,$label->id,$old_label,$label->id);
        $label->update(['value'=>$request->data_value]);
        return response()->json(['message' =>$request->data_value]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        if ($request->ajax())
        {
            Language::create($request->all());
            return response()->json([
                'intended' => $this->redirectPath(),
            ]);


        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Language  $language
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $data = Language::get();
        if ($request-> ajax()) {

            return Datatables::of($data)->addColumn('checkbox', function ($data) {

                })-> addColumn('actions', function ($data) {
                    $response = "";
                    $roles_manage = '<button class = "btn btn-success btn-sm" > <i class = "fas fa-user-shield"> </i></button > ';
                    $edit = '<a onClick = "getuserdata( '.$data->id . ')" id = "' .$data->id . '"
                            class = "btn btn-primary btn-sm" href = "" data - toggle = "modal" data - target = "#modalEditForm" ><i class = "fas fa-pencil-alt"> </i></a > ';
                    $delete = '<button type="button"class="btn btn-danger btn-sm my-0" onclick="deleteuserdata( '.$data ->id.
                    ')"> <i class = "fas fa-trash"></i> </button>';


                    if (\Illuminate\ Support\ Facades\ Auth::user() -> hasPermissionTo('edit users')) {
                        $response = $response.$edit;
                    }

                    if (\Illuminate\ Support\ Facades\ Auth::user() -> hasPermissionTo('manage roles')) {
                        $response = $response.$roles_manage;
                    }
                    if (\Illuminate\ Support\ Facades\ Auth::user() -> hasPermissionTo('delete users')) {
                        $response = $response.$delete;
                    }

                    return $response;
                })->rawColumns(['actions']) -> make(true);

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Language  $language
     * @return \Illuminate\Http\Response
     */
    public function edit(Language $language)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Language  $language
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Language $language)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Language  $language
     * @return \Illuminate\Http\Response
     */
    public function destroy(Language $language)
    {
        //
    }
}
