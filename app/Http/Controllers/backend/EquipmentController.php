<?php
namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Equipment;
use PDF;
use App\Exports\equipment\equipment\EquipmentExportTab2;
use App\Exports\equipment\equipment\EquipmentSoftExport2;
use Maatwebsite\Excel\Facades\Excel;
use DataTables;
use Validator;
use Config;
use Auth;
use Session;
class EquipmentController extends Controller
{
    private $page_name;
    private $tab_name;
    private $db;
    private $cms_array=[];
    public function __construct() {
        $this->page_name ='equipment';
        $this->tab_name ='project';
        $this->db='equipment';
        $cms_data=localization();
        $temp_array=[];
        $temp_array['name']['key']='name';
        $temp_array['name']['value']=checkKey('name',$cms_data);
        $temp_array['location_id']['key']='project';
        $temp_array['location_id']['value']=checkKey('project',$cms_data);
        $this->cms_array=$temp_array;
    }
    public function getAll()
    {
    	$data=Equipment::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->get();
        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {
            })
            ->addColumn('submit', function ($data) {
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                if(Auth()->user()->hasPermissionTo('delete_equipment') || Auth::user()->all_companies == 1 )
                {
                    $delete = '<a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light delete-btn'.$data->id.'" onclick="deleteequipment2data( ' . $data->id . ')"><i class="fas fa-trash"></i></a>';
                    $response = $response . $delete;
                }
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);

    }
    public function getAllSoft()
    {
    	$data=Equipment::where('company_id',Auth::user()->default_company)->where('active',0)->where('deleted',0)->get();
    	return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {
            })
            ->addColumn('submit', function ($data) {
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                if(Auth()->user()->hasPermissionTo('hard_delete_equipment') || Auth::user()->all_companies == 1 )
                {
                    $delete = '<a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light" onclick="deletesoftequipment2data( ' . $data->id . ')"><i class="fas fa-trash"></i></a>';
                    $response = $response . $delete;
                }
                if(Auth()->user()->hasPermissionTo('hard_delete_equipment') || Auth::user()->all_companies == 1 )
                {
                    $restore = '<a type="button" class="btn btn-success btn-xs my-0 waves-effect waves-light" onclick="restoreequipment2data( ' . $data->id . ')"><i class="fa fa-retweet"></i></a>';
                    $response = $response . $restore;
                }
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
    public function store()
    {
    	$data=Equipment::create(['company_id'=>Auth::user()->default_company]);
    audit_log($this->cms_array,'',Config::get('constants.store'),$this->page_name,$this->tab_name,$data->id,'',$data->id,Session::get('project_id'),$this->db);
    	return response()->json(['data'=>$data]);

    }
    public function update(Request $request)
    {
        $data = [
                'company_id'=>'required|numeric',
                ];
        $validator = Validator::make($request->all(), $data);
        if ($validator->fails())
        {
            $error = $validator->messages();
            $html='<ul>';
            foreach ($error->all() as $key => $value) {
                $html=$html.'<li>'.$value.'</li>';
            }
            $html=$html.'</ul>';
            return response()->json(['status'=> false, 'error'=> $html]);
        }
    	$data=$request->all();
        $equipment=Equipment::find($data['id']);
        audit_log($this->cms_array,audit_log_data($data),Config::get('constants.update'),$this->page_name,$this->tab_name,$equipment->id,audit_log_data($equipment->toArray()),$equipment->id,Session::get('project_id'),$this->db);
        $equipment->update($data);
    	return response()->json(['message'=>'Successfully Added !!']);

    }
    public function deleteMultipleSOft(Request $request)
    {
    	Equipment::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function restore(Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.restore'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('project_id'),$this->db);
        Equipment::where('id',$request->id)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Restore','flag'=>0]);
    }
    public function restoreMultiple(Request $request)
    {
        Equipment::whereIn('id',$request->ids)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Restore','flag'=>0]);
    }
    public function deleteMultiple(Request $request)
    {
    	Equipment::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function delete(Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.delete'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('project_id'),$this->db);
    	Equipment::where('id',$request->id)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function softDelete(Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.soft_delete'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('project_id'),$this->db);
    	Equipment::where('id',$request->id)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function activeMultiple(Request $request)
    {
    	Equipment::whereIn('id',$request->ids)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active']);
    }
    public function exportPdf(Request $request)
    {
        if($request->pdf_array == 1)
        {
            $data= Equipment::where('company_id',Auth::user()->default_company)->where('active', '=',1)->where('deleted', '=',0)->get();
        }
        else
        {
            $array=json_decode($request->pdf_array);
            $data= Equipment::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->get();
        }
        $companies='Patroltec';
        $reportname='Equipment';
        $date=\Carbon\Carbon::now()->format('D jS M Y');
        $pdf = PDF::loadView('backend.plant_and_equipment.equipment_tab_pdf',compact('companies','reportname','data','date'));
        return $pdf->download('equipment.pdf');
    }

    public function exportWorld(Request $request)
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
      	if($request->export_world_equipment2 == 1)
        {
            if($request->word_array == 1)
            {
                $data= Equipment::where('company_id',Auth::user()->default_company)->where('active', '=',1)->where('deleted', '=',0)->get();
            }
            else
            {
                $array=json_decode($request->word_array);
                $data= Equipment::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->get();
            }
        }
        else
        {
             $data= Equipment::where('company_id',Auth::user()->default_company)->where('active', '=',0)->where('deleted', '=',0)->get();
        }

        if(Auth()->user()->hasPermissionTo('equipment_name_word_export') || Auth::user()->all_companies == 1 ){
            $text = $section->addText('Name');
        }
        foreach ($data as $key => $value) {
            if(Auth()->user()->hasPermissionTo('equipment_name_word_export') || Auth::user()->all_companies == 1 ){
                $text = $section->addText($value->name);
            }
        }
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('equipment.docx');
        return response()->download(public_path('equipment.docx'));
    }
    public function exportExcel(Request $request)
    {
    	if($request->export_excel_equipment2 == 1)
        {
            if($request->excel_array == 1)
            {
                $data= Equipment::where('company_id',Auth::user()->default_company)->where('active', '=',1)->where('deleted', '=',0)->get();
                return Excel::download(new EquipmentExportTab2($data), 'equipment.csv');
            }
            else
            {
                $array=json_decode($request->excel_array);
                $data= Equipment::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->get();
                return Excel::download(new EquipmentExportTab2($data), 'equipment.csv');
            }
            //for active user
    		// return Excel::download(new EquipmentExportTab2, 'equipment.csv');
        }
        else
        {
        	return Excel::download(new EquipmentSoftExport2, 'equipment.csv');
            //for soft delete

        }
    }
    public function list()
    {
    	return view('backend.plant_and_equipment.index_main');
    }

}
