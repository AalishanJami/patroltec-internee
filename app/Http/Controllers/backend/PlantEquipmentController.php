<?php
namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\LocationBreakdown;
use App\Company;
use App\Equipment;
use PDF;
use Session;
use Validator;
use App\Exports\equipment\EquipmentExport;
use App\Exports\equipment\EquipmentSoftExport;
use Maatwebsite\Excel\Facades\Excel;
use DataTables;
use Auth;
use Config;
use Carbon\Carbon;
class PlantEquipmentController extends Controller
{
    private $page_name;
    private $tab_name;
    private $db;
    private $cms_array=[];
    public function __construct() {
        $this->page_name ='plant_and_equipment';
        $this->tab_name ='project';
        $this->db='location_breakdowns';
        $cms_data=localization();
        $temp_array=[];
        $temp_array['name']['key']='name';
        $temp_array['name']['value']=checkKey('name',$cms_data);
        $temp_array['equipment_id']['key']='equipment';
        $temp_array['equipment_id']['value']=checkKey('equipment',$cms_data);
        $temp_array['serial_number']['key']='serial_number';
        $temp_array['serial_number']['value']=checkKey('serial_number',$cms_data);
        $temp_array['make']['key']='make';
        $temp_array['make']['value']=checkKey('make',$cms_data);
        $temp_array['model']['key']='model';
        $temp_array['model']['value']=checkKey('model',$cms_data);
        $temp_array['last_service_date']['key']='last_service_date';
        $temp_array['last_service_date']['value']=checkKey('last_service_date',$cms_data);
        $temp_array['next_service_date']['key']='next_service_date';
        $temp_array['next_service_date']['value']=checkKey('next_service_date',$cms_data);
        $temp_array['condition']['key']='condition';
        $temp_array['condition']['value']=checkKey('condition',$cms_data);
        $temp_array['type']['key']='type';
        $temp_array['type']['value']=checkKey('type',$cms_data);
        $temp_array['location_id']['key']='project';
        $temp_array['location_id']['value']=checkKey('project',$cms_data);
        $this->cms_array=$temp_array;
    }
    public function index()
    {
        $companies=Company::getCompanies();
    	$plant_and_equipments=LocationBreakdown::where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
    	return view('backend.plant_and_equipment.index')->with(['plant_and_equipments'=>$plant_and_equipments,'companies'=>$companies]);
    }

    public function edit(Request $request)
    {
        $equipments=Equipment::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        $data=LocationBreakdown::where('id',$request->id)->where('active',1)->first();
        return response()->json(['data'=>$data,'last_service_date'=>Carbon::parse($data->last_service_date),'next_service_date'=>Carbon::parse($data->next_service_date),'location_breakdowns'=>$equipments]);
    }

    public function getAll()
    {
        $equipments=Equipment::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
    	$data=LocationBreakdown::where('company_id',Auth::user()->default_company)->where('type','e')->where('location_id',Session::get('project_id'))->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        return Datatables::of($data)->with(['flag'=>1,'equipments'=>$equipments])
          ->addColumn('checkbox', function ($data) {
            })
            ->addColumn('qr', function ($data) {})
            ->addColumn('last_service_date', function ($data) {return Carbon::parse($data->last_service_date)->format('M d Y');})
            ->addColumn('next_service_date', function ($data) {return Carbon::parse($data->next_service_date)->format('M d Y');})
            ->addColumn('submit', function ($data) {
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                if(Auth()->user()->hasPermissionTo('delete_equipmentGroup') || Auth::user()->all_companies == 1 ){
                    $delete = '<a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light delete-btn' . $data->id . '" onclick="deleteplant_and_equipmentdata( ' . $data->id . ')"><i class="fas fa-trash"></i></a>';
                }else{
                    $delete = '';
                }
                $response = $response . $delete;
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    public function getAllSoft()
    {
        $equipments=Equipment::where('company_id',Auth::user()->default_company)->where('active',0)->where('deleted',0)->orderBy('id', 'desc')->get();
    	$data=LocationBreakdown::where('company_id',Auth::user()->default_company)->where('type','e')->where('active',0)->where('deleted',0)->orderBy('id', 'desc')->get();
        return Datatables::of($data)->with(['flag'=>0,'equipments'=>$equipments])
            ->addColumn('checkbox', function ($data) {
            })->addColumn('qr', function ($data) {
            })
            ->addColumn('submit', function ($data) {
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                if(Auth()->user()->hasPermissionTo('hard_delete_equipmentGroup') || Auth::user()->all_companies == 1 ){
                    $delete = '<a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light" onclick="deletesoftplant_and_equipmentdata( ' . $data->id . ')"><i class="fas fa-trash"></i></a>';
                }else{
                    $delete = '';
                }
                if(Auth()->user()->hasPermissionTo('restore_delete_equipmentGroup') || Auth::user()->all_companies == 1 ){
                    $restore = '<a type="button" class="btn btn-success btn-xs my-0 waves-effect waves-light" onclick="activeplant_and_equipmentdata( ' . $data->id . ')"><i class="fas fa-retweet"></i></a>';
                }else{
                    $restore = '';
                }
                $response = $response . $restore;
                $response = $response . $delete;
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    public function store()
    {
    	$data=LocationBreakdown::create(['name'=>'','type'=>'e']);
    audit_log($this->cms_array,'',Config::get('constants.store'),$this->page_name,$this->tab_name,$data->id,'',$data->id,Session::get('project_id'),$this->db);
        $equipments=Equipment::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
    	return response()->json(['data'=>$data,'equipments'=>$equipments]);

    }
    public function update(Request $request)
    {
        $data = [
                'name'=>'required',
                'company_id'=>'required|numeric',
            ];
        $location_id=Session::get('project_id');
        $validator = Validator::make($request->all(), $data);
        if ($validator->fails())
        {
            $error = $validator->messages();
            $html='<ul>';
            foreach ($error->all() as $key => $value) {
                $html=$html.'<li>'.$value.'</li>';
            }
            $html=$html.'</ul>';
            return response()->json(['status'=> false, 'error'=> $html]);
        }

    	$data=$request->all();
        $data['location_id']=$location_id;
        $data['type']='e';
        if(isset($data['last_service_date']))
        {
            $data['last_service_date'] = date('Y/m/d', strtotime($data['last_service_date']));
        }
        if(isset($data['next_service_date']))
        {
            $data['next_service_date'] = date('Y/m/d', strtotime($data['next_service_date']));
        }
    	if($data['name'])
    	{
	    	$name='qr/'.$data['name'].'.png';
	    	$newPath = public_path($name);
			$oldPath = public_path('qr/qr.png');  // publc/images/2.jpg
			if (\File::copy($oldPath , $newPath)) {
			   \QrCode::size(80)->format('png')->generate($data['name'], public_path($name));
			}
    	}
        $location=LocationBreakdown::find($data['id']);
        audit_log($this->cms_array,audit_log_data($data),Config::get('constants.update'),$this->page_name,$this->tab_name,$location->id,audit_log_data($location->toArray()),$location->id,Session::get('project_id'),$this->db);
        $location->update($data);
        $qr_image='';
        if($data['name'])
        {
            $qr_image='<img src="'.url('qr').'\\'.$data['name'].'.png"  width="50" height="50">';
        }
    	return response()->json(['message'=>'Successfully Added !!','qr'=>$qr_image]);

    }
    public function deleteMultipleSOft(Request $request)
    {
    	LocationBreakdown::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function deleteMultiple(Request $request)
    {
    	LocationBreakdown::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function delete(Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.delete'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('project_id'),$this->db);
    	LocationBreakdown::where('id',$request->id)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function active(Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.restore'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('project_id'),$this->db);
        LocationBreakdown::where('id',$request->id)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }

    public function softDelete(Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.soft_delete'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('project_id'),$this->db);
    	LocationBreakdown::where('id',$request->id)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function activeMultiple(Request $request)
    {
    	LocationBreakdown::whereIn('id',$request->ids)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active']);
    }
    public function exportPdf(Request $request)
    {
        if($request->pdf_array == 1)
        {
            $data= LocationBreakdown::where('company_id',Auth::user()->default_company)->where('type','e')->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->pdf_array);
            $data= LocationBreakdown::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->where('type','e')->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        $companies='Patroltec';
        $reportname='Plant and Equipment';
        $date=\Carbon\Carbon::now()->format('D jS M Y');
        $pdf = PDF::loadView('backend.plant_and_equipment.pdf',compact('companies','reportname','data','date'));
        return $pdf->download('plant_and_equipment.pdf');
    }

    public function exportWorld(Request $request)
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        if($request->word_array == 1)
        {
            $data= LocationBreakdown::where('company_id',Auth::user()->default_company)->where('type','e')->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->word_array);
            $data= LocationBreakdown::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->where('type','e')->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }

        $table = $section->addTable();
        foreach ($data as $key => $value) {
            if($key==0)
            {
                $table->addRow();
                if(Auth()->user()->hasPermissionTo('equipmentGroup_qr_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('QR');
                }
                if(Auth()->user()->hasPermissionTo('equipmentGroup_name_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Name');
                }
                $table->addCell(1750)->addText('Equipment');
                $table->addCell(1750)->addText('Serial Number');
                $table->addCell(1750)->addText('Make');
                $table->addCell(1750)->addText('Model');
                $table->addCell(1750)->addText('Last Service Date');
                $table->addCell(1750)->addText('Next Service Date');
                $table->addCell(1750)->addText('Condition');
            }
            $table->addRow();
            if(Auth()->user()->hasPermissionTo('equipmentGroup_qr_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addImage(public_path('qr/'.$value->name.'.png'));
            }
            if(Auth()->user()->hasPermissionTo('equipmentGroup_name_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText($value->name);
            }
            if (!empty($value->equipment->name)){
                $table->addCell(1750)->addText(strip_tags($value->equipment->name));
            }else{
                $table->addCell(1750)->addText('');
            }
            $table->addCell(1750)->addText(strip_tags($value->serial_number));
            $table->addCell(1750)->addText(strip_tags($value->make));
            $table->addCell(1750)->addText(strip_tags($value->model));
            if(isset($value->last_service_date))
            {
                $table->addCell(1750)->addText(date('jS M Y',strtotime($value->last_service_date)));
            }
            else
            {
                $table->addCell(1750)->addText('');
            }
            if(isset($value->next_service_date))
            {

                $table->addCell(1750)->addText(date('jS M Y',strtotime($value->next_service_date)));
            }
            else
            {

                $table->addCell(1750)->addText('');
            }
            $table->addCell(1750)->addText(strip_tags($value->condition));
        }
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('equipment.docx');
        return response()->download(public_path('equipment.docx'));
    }
    public function exportExcel(Request $request)
    {
        if($request->excel_array == 1)
        {
            $data= LocationBreakdown::where('company_id',Auth::user()->default_company)->where('type','e')->with('equipment')->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();

            return Excel::download(new EquipmentExport($data), 'equipment.csv');
        }
        else
        {
            $array=json_decode($request->excel_array);
            $data= LocationBreakdown::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->with('equipment')->where('type','e')->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
            return Excel::download(new EquipmentExport($data), 'equipment.csv');
        }
    }

}
