<?php

namespace App\Http\Controllers\backend;
use App\Equipment;
use App\Http\Controllers\Controller;
use App\Location;
use App\LocationBreakdown;
use App\Company;
use Illuminate\Http\Request;
use DataTables;
use App\Exports\report\QRExport;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use App\Exports\report\PlantAndEquipmentExport;
use Auth;
use Carbon\Carbon;
class ReportController extends Controller
{
    public function __construct() {
        remove_key();
    }
    //QR report start
    public function qrlist(){
        $companies=Company::getCompanies();
        return view('backend.reports.qr_list')->with(['companies'=>$companies]);
    }

    public function getAllQRList(){
        $data=LocationBreakdown::where('company_id',Auth::user()->default_company)->where('type','t')->with('location')->orderBy('id', 'desc')->get();
        return $this->qrListDataTable($data);
    }

    public function qrListDataTable($data)
    {
        return Datatables::of($data)->with(['flag'=>1])
        ->addColumn('project', function ($data) {
            if (!empty($data->location->name)){
                return $data->location->name;
            }else{
                return "";
            }
        })->addColumn('qr', function ($data) {})
        ->rawColumns(['actions'])
        ->make(true);
    }

    public function customSearchQR(Request $request)
    {
        if(isset($request->data['location_id'])){
            $location_ids = [];
            $locations =  Location::where('name','LIKE','%'.$request->data['location_id'].'%')->get();

            foreach ($locations as  $location){
                array_push($location_ids, $location->id );
            }
        }
        $dataArray=LocationBreakdown::where('company_id',Auth::user()->default_company)->where('type','t');
        if($request->data)
        {
            foreach ($request->data as $key => $value) {
                if($key == 'location_id'){
                    $dataArray = $dataArray->whereIn($key , $location_ids);
                }
                else{
                    $dataArray = $dataArray->where($key, 'LIKE', '%' . $value . '%');
                }
            }
        }
        $dataArray=$dataArray->with('location','equipment')->orderBy('id', 'desc')->get();
        // $data=LocationBreakdown::where('company_id',Auth::user()->default_company)->where('type','t')->with('location','equipment')->orderBy('id', 'desc')->get();
        return $this->qrListDataTable($dataArray);
    }

    public function exportPdfQR(Request $request)
    {
        if($request->pdf_array == 1)
        {
            $data= LocationBreakdown::where('company_id',Auth::user()->default_company)->where('type','t')->with('location')->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->pdf_array);
            $data= LocationBreakdown::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->where('type','t')->with('location')->orderBy('id', 'desc')->get();
        }
        $date=Carbon::now()->format('D jS M Y');
        $companies='Patroltec';
        $reportname='QR Report';
        $pdf = PDF::loadView('backend.reports.export.qr.pdf',compact('data','date','reportname','companies'));
        return $pdf->download('qr_report.pdf');
    }

    public function exportWordQR(Request $request)
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        if($request->word_array == 1)
        {
            $data= LocationBreakdown::where('company_id',Auth::user()->default_company)->where('type','t')->with('location')->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->word_array);
            $data= LocationBreakdown::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->where('type','t')->with('location')->orderBy('id', 'desc')->get();
        }
        $text = $section->addText('Word Export data');
        $table = $section->addTable();
        foreach ($data as $key => $value) {
            if($key==0) {
                $table->addRow();
                if(Auth()->user()->hasPermissionTo('qrReport_project_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Project');
                }else{
                    $table->addCell(1750)->addText('');
                }
                if(Auth()->user()->hasPermissionTo('qrReport_name_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Name');
                }else{
                    $table->addCell(1750)->addText('');
                }
                if(Auth()->user()->hasPermissionTo('qrReport_directions_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Direction');
                }else{
                    $table->addCell(1750)->addText('');
                }
                if(Auth()->user()->hasPermissionTo('qrReport_information_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Information');
                }else{
                    $table->addCell(1750)->addText('');
                }

            }
            $table->addRow();
            if(Auth()->user()->hasPermissionTo('qrReport_project_word_export') || Auth::user()->all_companies == 1 ){
                if (!empty($value->location->name)){
                    $table->addCell(1750)->addText(strip_tags($value->location->name));
                }else{
                    $table->addCell(1750)->addText('');
                }
            }else{
                $table->addCell(1750)->addText('');
            }
            if(Auth()->user()->hasPermissionTo('qrReport_name_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText(strip_tags($value->name));
            }else{
                $table->addCell(1750)->addText('');
            }
            if(Auth()->user()->hasPermissionTo('qrReport_directions_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText(strip_tags($value->directions));
            }else{
                $table->addCell(1750)->addText('');
            }
            if(Auth()->user()->hasPermissionTo('qrReport_information_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText(strip_tags($value->information));
            }else{
                $table->addCell(1750)->addText('');
            }

        }
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('qr_report.docx');
        return response()->download(public_path('qr_report.docx'));
    }

    public function exportExcelQR(Request $request)
    {
        if($request->excel_array == 1)
        {
            $data= LocationBreakdown::where('company_id',Auth::user()->default_company)->where('type','t')->with('location')->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->excel_array);
            $data= LocationBreakdown::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->where('type','t')->with('location')->orderBy('id', 'desc')->get();
        }
        return Excel::download(new QRExport($data), 'qr_report.csv');
    }
    //QR report end
    public function customSearch(Request $request)
    {
//        dd($request->all());
        if(isset($request->data['location_id'])){
            $location_ids = [];
           $locations =  Location::where('name','LIKE','%'.$request->data['location_id'].'%')->get();
           foreach ($locations as  $location){
               array_push($location_ids, $location->id );
           }
        }
        $dataArray=LocationBreakdown::where('company_id',Auth::user()->default_company)->where('type','e');
        if($request->data)
        {
            foreach ($request->data as $key => $value) {
                if($key == 'location_id'){
                    $dataArray = $dataArray->whereIn($key , $location_ids);
                }else if($key == 'next_service_date'){
                   $date = Carbon::parse($value)->toDateString();
                    $dataArray = $dataArray->whereDate($key, '=', $date)->orWhereDate('last_service_date', '=', $date);;
                }else{
                    $dataArray = $dataArray->where($key, 'LIKE', '%' . $value . '%');
                }
            }
        }
        $dataArray=$dataArray->with('location','equipment')->orderBy('id', 'desc')->get();
        return $this->plantAndEquipmentListDataTable($dataArray);
    }
    //plantAndEquipment start
    public function plantAndEquipmentList()
    {
        $companies=Company::getCompanies();
        return view('backend.reports.plant_and_equipment_list')->with(['companies'=>$companies]);
    }

    public function getAllplantAndEquipmentList()
    {
        $data=LocationBreakdown::where('company_id',Auth::user()->default_company)->where('type','e')->with('location','equipment')->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->orderBy('id', 'desc')->get();
//        dd($data);
        return $this->plantAndEquipmentListDataTable($data);
    }

    public function plantAndEquipmentListDataTable($data)
    {
        $equipments=Equipment::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        return Datatables::of($data)->with(['flag'=>1,'equipments'=>$equipments])
        ->addColumn('qr', function ($data) {})
        ->addColumn('last_service_date', function ($data) {
            return Carbon::parse($data->last_service_date)->format('jS M Y');
        })
        ->addColumn('next_service_date', function ($data) {
             return Carbon::parse($data->next_service_date)->format('jS M Y');
        })
        ->addColumn('project', function ($data) {
            if (!empty($data->location->name)){
                return $data->location->name;
            }else{
                return "";
            }
        })->addColumn('equipment', function ($data) {
            if (!empty($data->equipment->name)){
                return $data->equipment->name;
            }else{
                return "";
            }
        })
        ->rawColumns(['project'])
        ->make(true);
    }

    public function exportPdfplantAndEquipment(Request $request)
    {
        if($request->pdf_array == 1)
        {
            $data=LocationBreakdown::where('company_id',Auth::user()->default_company)->where('type','e')->with('location','equipment')->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->pdf_array);
            $data= LocationBreakdown::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->where('type','e')->with('location','equipment')->orderBy('id', 'desc')->get();
        }
        $date=Carbon::now()->format('D jS M Y');
        $companies='Patroltec';
        $reportname='Plant & Equipment Report';
        $pdf = PDF::loadView('backend.reports.export.plantandequipment.pdf',compact('data','date','reportname','companies'));
        return $pdf->download('Plant & Equipment.pdf');
    }

    public function exportWordplantAndEquipment(Request $request)
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();

        if($request->word_array == 1)
        {
            $data=LocationBreakdown::where('company_id',Auth::user()->default_company)->where('type','e')->with('location','equipment')->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->word_array);
            $data= LocationBreakdown::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->where('type','e')->with('location','equipment')->orderBy('id', 'desc')->get();
        }
        $text = $section->addText('Word Export data');
        $table = $section->addTable();
        foreach ($data as $key => $value) {
            if($key==0) {
                $table->addRow();
                if(Auth()->user()->hasPermissionTo('plantandequipmentReport_project_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Project');
                }else{
                    $table->addCell(1750)->addText('');
                }

                if(Auth()->user()->hasPermissionTo('plantandequipmentReport_equipment_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Equipment');
                }else{
                    $table->addCell(1750)->addText('');
                }
                if(Auth()->user()->hasPermissionTo('plantandequipmentReport_serialnumber_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Serial Number');
                }else{
                    $table->addCell(1750)->addText('');
                }
                if(Auth()->user()->hasPermissionTo('plantandequipmentReport_make_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Make');
                }else{
                    $table->addCell(1750)->addText('');
                }
                if(Auth()->user()->hasPermissionTo('plantandequipmentReport_model_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Model');
                }else{
                    $table->addCell(1750)->addText('');
                }
                if(Auth()->user()->hasPermissionTo('plantandequipmentReport_lastservicedate_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Last Service Date');
                }else{
                    $table->addCell(1750)->addText('');
                }
                if(Auth()->user()->hasPermissionTo('plantandequipmentReport_nextservicedate_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Next Service Date');
                }else{
                    $table->addCell(1750)->addText('');
                }
                if(Auth()->user()->hasPermissionTo('plantandequipmentReport_condition_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Condition');
                }else{
                    $table->addCell(1750)->addText('');
                }

            }
            $table->addRow();
            if(Auth()->user()->hasPermissionTo('plantandequipmentReport_name_word_export') || Auth::user()->all_companies == 1 ){
                if (!empty($value->location->name)){
                    $table->addCell(1750)->addText(strip_tags($value->location->name));
                }else{
                    $table->addCell(1750)->addText('');
                }
            }else{
                $table->addCell(1750)->addText('');
            }
            if(Auth()->user()->hasPermissionTo('plantandequipmentReport_equipment_word_export') || Auth::user()->all_companies == 1 ){
                if (!empty($value->equipment->name)){
                    $table->addCell(1750)->addText(strip_tags($value->equipment->name));
                }else{
                    $table->addCell(1750)->addText('');
                }
            }else{
                $table->addCell(1750)->addText('');
            }
            if(Auth()->user()->hasPermissionTo('plantandequipmentReport_serialnumber_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText(strip_tags($value->serial_number));
            }else{
                $table->addCell(1750)->addText('');
            }
            if(Auth()->user()->hasPermissionTo('plantandequipmentReport_make_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText(strip_tags($value->make));
            }else{
                $table->addCell(1750)->addText('');
            }
            if(Auth()->user()->hasPermissionTo('plantandequipmentReport_model_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText(strip_tags($value->model));
            }else{
                $table->addCell(1750)->addText('');
            }
            if(Auth()->user()->hasPermissionTo('plantandequipmentReport_lastservicedate_word_export') || Auth::user()->all_companies == 1 ){
                if(isset($value->last_service_date))
                    $table->addCell(1750)->addText(date('jS M Y',strtotime($value->last_service_date)));

                else
                    $table->addCell(1750)->addText('');

            }else{
                $table->addCell(1750)->addText('');
            }
            if(Auth()->user()->hasPermissionTo('plantandequipmentReport_nextservicedate_word_export') || Auth::user()->all_companies == 1 ){
                if(isset($value->next_service_date))
                $table->addCell(1750)->addText(date('jS M Y',strtotime($value->next_service_date)));

                else
                    $table->addCell(1750)->addText('');

            }else{
                $table->addCell(1750)->addText('');
            }

            if(Auth()->user()->hasPermissionTo('plantandequipmentReport_condition_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText(strip_tags($value->condition));
            }else{
                $table->addCell(1750)->addText('');
            }

        }
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('plantandequipment.docx');
        return response()->download(public_path('plantandequipment.docx'));
    }

    public function exportExcelplantAndEquipment(Request $request)
    {
        if($request->excel_array == 1)
        {
            $data=LocationBreakdown::where('company_id',Auth::user()->default_company)->where('type','e')->with('location','equipment')->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->excel_array);
            $data= LocationBreakdown::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->where('type','e')->with('location','equipment')->orderBy('id', 'desc')->get();
        }
        return Excel::download(new PlantAndEquipmentExport($data), 'plantandequipment.csv');
    }
    //plantAndEquipment end
}
