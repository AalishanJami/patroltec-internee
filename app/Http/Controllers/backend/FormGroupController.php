<?php
namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Company;
use Auth;
use App\Location;
use App\Exports\scoring\ScoringExport;
use App\Exports\scoring\ScoringSoftExport;
use App\FormGroups;
use App\AnswerGroups;
use App\User;
use PDF;
use DataTables;
use Maatwebsite\Excel\Facades\Excel;
use Validator;
use Session;
use Carbon\Carbon;
use Config;
class FormGroupController extends Controller
{
    private $page_name;
    private $tab_name;
    private $db;
    private $cms_array=[];
    public function __construct() {
        Session::forget('employee_id');
        Session::forget('project_id');
        Session::forget('client_id');
        $this->page_name ='folder';
        $this->tab_name ='folder';
        $this->db='form_groups';
        $cms_data=localization();
        $temp_array=[];
        $temp_array['name']['key']='name';
        $temp_array['name']['value']=checkKey('name',$cms_data);
        $this->cms_array=$temp_array;
    }
    public function index()
    {
        Session::put('form_id', null);
        Session::put('folder_id', null);
        $date = Carbon::now()->format('jS M Y');
        $time = Carbon::now()->format('h:i');
        $companies=Company::getCompanies();
        $locations = Location::all()->where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->pluck('name','id');
        $users = User::all()->where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->pluck('first_name','id');
        $answer_groups = AnswerGroups::where('company_id',Auth::user()->default_company)->where('sig_select',1)->where('active',1)->where('deleted',0)->pluck('name','id');
        return view('backend.document.index')->with(['companies' => $companies,'locations' => $locations,'users' => $users,'answer_groups' => $answer_groups,'date' => $date,'time' => $time]);
    }
    public function reOrder(Request $request)
    {
        $data = $request->array;
        foreach($data as $key=> $item){
            FormGroups::where('id', $item['id'])->update([
                'order' => ++$key,
            ]);
        }
        return response()->json(['success'=>true]);
    }

    public function paste(Request $request)
    {
        $data=FormGroups::where('id',$request->cut_id)->update(['form_group_id'=>(int)$request->paste_id]);
        return response()->json(['message'=>'Successfully Paste']);
    }
    public function getAll($id)
    {
        $data=FormGroups::where('company_id',Auth::user()->default_company)->with('form_group_delete')->where('active',1)->where('form_group_id', '=', $id)->where('deleted',0)->orderBy('order', 'asc')->get();
        $data=sortData($data);
        $data_list = FormGroups::where('id',$id)->with('form_group.form_group')->first();
        $parent_list=($id != 0 ? $this->loop($data_list) : '');
        return Datatables::of($data)
            ->addColumn('actions', function($data) use ($id,$parent_list){
                $response="";
                $cut_btn='';
                $cut_btn=$cut_btn.'<div class="btn-group dropleft">';
                    $cut_btn=$cut_btn.'<button type="button" class="btn btn-primary dropdown-toggle px-3"';
                    $cut_btn=$cut_btn.'data-toggle="dropdown" aria-haspopup="true"';
                    $cut_btn=$cut_btn.'aria-expanded="false">';
                    $cut_btn=$cut_btn.'</button>';
                    $cut_btn=$cut_btn.'<div class="dropdown-menu dropdown_menu_width">';
                        $cut_btn=$cut_btn.'<a class="dropdown-item copy_paste" data-key="cut" data-value="'.$data->id.'" >Cut</a>';
                        $cut_btn=$cut_btn.'<a class="dropdown-item enable_paste copy_paste disable_href" data-key="paste"  data-value="'.$data->id.'" >Paste</a>';
                        if($id != 0)
                        {
                            $cut_btn=$cut_btn.'<a class="dropdown-item enable_paste" data-key="'.$data->id.'"  data-value="0" >Root</a>';
                            foreach ($parent_list as $parent_key => $parent_name) {
                                if($id !=$parent_key)
                                {
                                    $cut_btn=$cut_btn.'<a class="dropdown-item enable_paste_parent" data-key="'.$parent_key.'"  data-value="'.$data->id.'" >'.$parent_name.'</a>';
                                }
                            }
                        }
                    $cut_btn=$cut_btn.'</div>';
                $cut_btn=$cut_btn.'</div>';
                $localization=localization();
                if(Auth()->user()->hasPermissionTo('viewform_group_documentfolder') || Auth::user()->all_companies == 1 ){
                    $view ='<a type="button" href="/form_group/folder/' . $data->id . '" class="btn btn-outline-light btn-sm my-0 mr-2"  style="background-color: #fff"><i class="far fa-eye"></i></a>';
                }else{
                    $view ='';
                }
                $delete='';
                if(!$data->form_group_delete){
                    if(Auth()->user()->hasPermissionTo('delete_documentfolder') || Auth::user()->all_companies == 1 ){
                        $delete ='<a class="form_groupdatadelete'.$data->id.' btn btn-outline-light btn-sm my-0" onclick="viewform_groupdatadelete(' . $data->id . ')" style="background-color: red !important;border:2px solid red !important;margin-left: 2% !important;"><i class="fa fa-trash"></i></a>';
                    }else{
                        $delete ='';
                    }
                }
                $submit='';
                if(Auth()->user()->hasPermissionTo('edit_documentfolder') || Auth::user()->all_companies == 1 ){
                    $submit='<a type="button" class="btn-sm my-0 btn btn-primary btn-xs all_action_btn_margin waves-effect waves-light saveform_groupdata form_groupdata_show_button_' . $data->id . '" style="display: none;" id="' . $data->id . '"><i class="fas fa-check"></i></a>';
                }
                $response=$response.$cut_btn;
                $response=$response.$view;
                $response=$response.$delete;
                $response=$response.$submit;
                return $response;
            })->rawColumns(['actions','name'])->make(true);
    }
    public function getAllSoft()
    {
        $data=FormGroups::where('company_id',Auth::user()->default_company)->where('active',0)->where('deleted',0)->orderby('id','desc')->get();
        $data=sortData($data);
        $locations=Location::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->get();
        return response()->json(['data'=>$data,'flag'=>0,'locations'=>$locations]);
    }
    public function store()
    {
        $form_id = Session::get('folder_id');
        $data_list = FormGroups::where('id',$form_id)->with('form_group.form_group')->first();
        if($form_id != null){
            $data=FormGroups::create([
                'form_group_id' => $form_id
            ]);
        }
        else {
            $data=FormGroups::create([
                'form_group_id' => 0
            ]);
        }
    audit_log($this->cms_array,'',Config::get('constants.store'),$this->page_name,$this->tab_name,$data->id,'',$data->id,$data->id,'',$this->db);
        $parent_list=($form_id != 0 ? $this->storeJavascript($data_list,$form_id,$data) : '');
        return response()->json(['data'=>$data,'form_id'=>$form_id,'parent_list'=>$parent_list]);
    }
    public function storeJavascript($data_list,$form_id,$data)
    {
        $parent_list=$this->loop($data_list);
        $cut_btn='';
        $cut_btn=$cut_btn.'<a class="dropdown-item enable_paste" data-key="'.$data->id.'"  data-value="0" >Root</a>';
        foreach ($parent_list as $parent_key => $parent_name) {
            if($form_id !=$parent_key)
            {
                $cut_btn=$cut_btn.'<a class="dropdown-item enable_paste_parent" data-key="'.$parent_key.'"  data-value="'.$data->id.'" >'.$parent_name.'</a>';
            }
        }
        return $cut_btn;
    }
    public function update(Request $request)
    {
        // dd($request->all());
        $form_id = Session::get('folder_id');
        $data = [
                'company_id'=>'required|numeric',
                ];
        $validator = Validator::make($request->all(), $data);
        if ($validator->fails())
        {
            $error = $validator->messages();
            $html='<ul>';
            foreach ($error->all() as $key => $value) {
                $html=$html.'<li>'.$value.'</li>';
            }
            $html=$html.'</ul>';
            return response()->json(['status'=> false, 'error'=> $html]);
        }
        $data=$request->all();
        $scoring=FormGroups::where('id',$data['id']);
        $bonus=$scoring->first();
        $scoring->update($data);
        if($bonus->active == 0)
        {
            $flag=0;
        }
        else
        {
            $flag=1;
        }
        $input=$request->all();
        audit_log($this->cms_array,audit_log_data($input),Config::get('constants.update'),$this->page_name,$this->tab_name,$bonus->id,audit_log_data($bonus->toArray()),$bonus->id,$bonus->id,'',$this->db);
        return response()->json(['message'=>'Successfully Added !!','flag'=>$flag,'form_id'=>$form_id]);

    }
    public function softDelete (Request $request)
    {
        FormGroups::where('id',$request->id)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function delete(Request $request)
    {
        $form_id = Session::get('folder_id');
    audit_log($this->cms_array,'',Config::get('constants.delete'),$this->page_name,$this->tab_name,'','',$request->id,$request->id,$this->db);
        FormGroups::where('id',$request->id)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','form_id'=>$form_id]);
    }
    public function active(Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.restore'),$this->page_name,$this->tab_name,'','',$request->id,$request->id,'',$this->db);
        FormGroups::where('id',$request->id)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
    public function activeMultiple(Request $request)
    {
        FormGroups::whereIn('id',$request->ids)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
     public function deleteMultipleSOft(Request $request)
    {
        FormGroups::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function deleteMultiple(Request $request)
    {
        FormGroups::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function exportPdf(Request $request)
    {
        if($request->export_pdf_scoring == 1)
        {
            $data= FormGroups::where('company_id',Auth::user()->default_company)->where('active', '=',1)->where('deleted', '=',0)->get();
        }
        else
        {
            $data= FormGroups::where('company_id',Auth::user()->default_company)->where('active', '=',0)->where('deleted', '=',0)->get();
        }
        $pdf = PDF::loadView('backend.scoring.pdf',compact('data'));
        return $pdf->download('scoring.pdf');
    }

    public function exportWorld(Request $request)
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        if($request->export_world_scoring == 1)
        {

            $data= FormGroups::where('company_id',Auth::user()->default_company)->where('active', '=',1)->where('deleted', '=',0)->get();
        }
        else
        {
             $data= FormGroups::where('company_id',Auth::user()->default_company)->where('active', '=',0)->where('deleted', '=',0)->get();
        }
        $text = $section->addText('Score Max Visits Dashboard Name Notes');
        foreach ($data as $key => $value) {
            $text = $section->addText($value->score .'         '. $value->max_visits .'         '. $value->dashboard_name.'         '. $value->notes);
        }
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('formgroups.docx');
        return response()->download(public_path('scoring.docx'));
    }
    public function exportExcel(Request $request)
    {
        if($request->export_excel_scoring == 1)
        {
            //for active user
            return Excel::download(new ScoringExport, 'scoring.csv');
        }
        else
        {
            return Excel::download(new ScoringSoftExport, 'scoring.csv');
            //for soft delete

        }
    }
    public function detail()
    {
    	return view('backend.project.details');
    }

    public function folderInside($id)
    {
        $check=[];
        Session::put('folder_id', $id);
        $companies=Company::getCompanies();
        $data = FormGroups::where('id',$id)->pluck('name');
        $data_list = FormGroups::where('id',$id)->with('form_group.form_group')->first();
        $breadcrum=$this->loop($data_list);
        $breadcrum =array_reverse($breadcrum);
        $data_list =FormGroups::where('id',$id)->with('form_group')->get()->last();
        array_push($check,$data_list->form_group_id);
        while($data_list->form_group_id!=0)
        {
            $data_list =FormGroups::where('id',$data_list->form_group_id)->get()->last();
            array_push($check,$data_list->form_group_id);
        }
        $check=array_reverse($check);
        $date = Carbon::now()->format('jS M Y');
        $time = Carbon::now()->format('h:i');
        $companies=Company::getCompanies();
        $locations = Location::all()->where('active',1)->where('deleted',0)->pluck('name','id');
        $users = User::all()->where('active',1)->where('deleted',0)->pluck('name','id');
        $answer_groups = AnswerGroups::where('sig_select',1)->where('active',1)->where('deleted',0)->pluck('name','id');
        return view('backend.document.folder_inside')->with(['id'=>$id, 'folder_name' => $data[0], 'companies'=>$companies,'breadcrum'=>$breadcrum,'check'=>$check,'companies' => $companies,'locations' => $locations,'users' => $users,'answer_groups' => $answer_groups,'date' => $date,'time' => $time]);

    }
    public function loop($data)
    {
        $breadcrum=[];
        $name=strip_tags($data->name,'</span>');
        // array_push($breadcrum, $name);
        $breadcrum[$data->id]=$name;
        if($data->form_group)
        {
            $breadcrum=$this->inside($data->form_group,$breadcrum);
        }
         return $breadcrum;
    }
    public function inside($inside,$breadcrum)
    {
        $name=strip_tags($inside->name,'</span>');
        $breadcrum[$inside->id]=$name;
        if($inside->form_group)
        {
            $breadcrum=$this->inside($inside->form_group,$breadcrum);
        }
        return $breadcrum;
    }

}
