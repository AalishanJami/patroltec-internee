<?php
namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Spatie\Permission\Models\Role;
use App\Company;
use App\CompanyUser;
use Auth;
use App\Exports\UsersExport;
use App\Exports\UsersExportSoft;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use PDF;
use DataTables;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Hash;
class UserController extends Controller
{
    public function index()
    {
    	$companies=Company::getCompanies();
        $roles=Role::get();
    	return view('backend.user.index')->with(['companies'=>$companies,'roles'=>$roles]);
    }
    public function list()
    {
        $companies=Company::getCompanies();
        $roles=Role::get();
        return view('backend.user.list')->with(['companies'=>$companies,'roles'=>$roles]);
    }
    public function role(Request $request)
    {
        $user = User::findOrFail($request->id); //Get user with specified id
        $roles = Role::get();
        return view('backend.user.edit', compact('roles', 'user'));
    }
    public function copyUser(Request $request)
    {
        $user=User::where('id',$request->id)->first();
        $companies_id=CompanyUser::where('user_id',$request->id)->get();
        $all_companies=Company::getCompanies();
        $html=$this->editHtmlUser($all_companies,$companies_id,2,$user);  
        return response()->json(['user'=>$user,'html'=>$html]);
    }
    public function companyList()
    {
        $companies=Company::getCompanies();
        $data=localization();
        return view('backend.user.companylist', compact('companies','data'));
    }
    public function roleUpdate(Request $request)
    { 

        $user = User::where('id',$request->user_id)->first();
        $roles = $request['roles'];
        foreach ($roles as $key => $value) {
            if($value == '0')
            { 
                User::where('id',$request->user_id)->update(['all_companies'=>1]);
                return redirect('/users')->with('message','User successfully edited.');
            }
        }
        if (isset($roles)) {        
            $user->roles()->sync($roles);  //If one or more role is selected associate user to roles          
        }        
        else {
            $user->roles()->detach(); //If no role is selected remove exisiting role associated to a user
        }
        return redirect('/users')->with('message','User successfully edited.');
    }
    public function store(Request $request)
    {
    	if ($request->ajax())
        {
            if (User::where('email', '=', $request['email'])->exists())
            {
                return response()->json([

                    'user_exist' => true,
                    'auth' => auth()->check(),

                ]);
            }
            else{
                $user =  User::create([
                    'name' => $request['name'],
                    'email' => $request['email'],
                    'active'=>1,
                    'password' => Hash::make($request['password']),

                ]);
                foreach ($request->selected_companies as $key => $value) {
                    // dd($value);
                    if($value == 0)
                    {
                       $user->all_companies=1;
                       $user->save();
                    }
                    else
                    {

                        CompanyUser::create(['user_id'=>$user->id,'company_id'=>$value]);
                    }
                   
                }
                return response()->json([
                    'auth' => auth()->check(),
                    'token' => csrf_token(),
                    'user' => $user,
                    'user_exist' => false,
                ]);
            }


        }
    }
    public function edit(Request $request)
    {
        $companies_id=CompanyUser::where('user_id',$request->id)->get();
        $all_companies=Company::getCompanies();
        $data=User::find($request->id);
		$data['companies']=$companies_id;
        $data['html']=$this->editHtmlUser($all_companies,$companies_id,1,$data);                  
        return response()->json($data);
    }
    public function editHtmlUser($all_companies,$companies_id,$flag,$user)
    {
         $data=localization();
        $html='';
        $html=$html.'<div class="md-form mb-5">';
         $html=$html.'<div  disabled="true">';
        if($flag == 1)
        {
             if(Auth()->user()->hasPermissionTo('user_company_edit') || Auth::user()->all_companies == 1 )
            {
                /*$html=$html.'<select id="edit_companies_selected" class="mdb-select colorful-select dropdown-primary md-form" multiple searchable="Search here.." required>';*/
                $html = $html.'<ul class="list-group-flush ">';
            }
            else
            {
                /*$html=$html.'<select id="edit_companies_selected" disabled class="mdb-select colorful-select dropdown-primary md-form" multiple searchable="Search here.." required>';
                */
                $html = $html.'<ul class="list-group-flush disabled">';            }
        }
        else
        {
            /*$html=$html.'<select id="companies_selected" class="mdb-select md-form" multiple searchable="Search here.." >';*/
             $html = $html.'<ul class="list-group-flush ">';

        }
        if($user->all_companies == 1)
        {

        /* $html=$html.'<option value="0" selected > All Companies</option>';*/
         $html=$html.' 
                        <li class="list-group-item"><div class="form-check">
    <input type="checkbox" class="form-check-input company edit_companies_selected" id="all_companies" value="0" checked>
    <label class="form-check-label" for="all_companies">All Companies</label></div></li>';
        }
        else
        {
            /*$html=$html.'<option value="0" > All Companies</option>';*/
            $html=$html.' 
                        <li class="list-group-item"><div class="form-check">
    <input type="checkbox" class="form-check-input company edit_companies_selected" id="all_companies" value="0">
    <label class="form-check-label" for="all_companies">All Companies</label></div></li>';
        }
        foreach ($all_companies as $key => $value) {
           $flag=0;
            foreach ($companies_id as $index => $value_index) {
               if($value_index->company_id == $value->id && $user->all_companies == 1)
               {
                    $flag=1;
                    break;
               }
            }
            
            if($flag == 1)
            {

                /*$html=$html.'<option  value="'.$value->id.'" selected>';*/
                $html=$html.'<li class="list-group-item"><div class="form-check"><input type="checkbox" class="form-check-input company edit_companies_selected" id="company'.$value->id.'" value="'.$value->id.'" checked>
    <label class="form-check-label" for="company'.$value->id.'">'.$value->name.'</label></div></li>';
            }
            else
            {
                 /*$html=$html.'<option value="'.$value->id.'">';*/

                           $html=$html.'<li class="list-group-item"><div class="form-check"><input type="checkbox" class="form-check-input company edit_companies_selected" id="company'.$value->id.'" value="'.$value->id.'" >
    <label class="form-check-label" for="company'.$value->id.'">'.$value->name.'</label></div></li>';
            }
            /*$html=$html.$value->name;
            $html=$html.'</option>';*/
        }
       /* $html=$html.'</select>';
        $html=$html.'<label class="mdb-main-label">';
        $html=$html.'<span class="assign_class label'.getKeyid("select_company",$data).'" data-id="'.getKeyid("select_company",$data).'" data-value="'.checkKey("select_company",$data).'" >'.checkKey("select_company",$data).'</span></label>';*/
        $html=$html.'</ul>';
        $html=$html.'</div>';
        $html=$html.'</div>';
        return $html;
    }
    public function updatePassword(Request $request)
	{

	    $user=User::find($request->id);
		$user->password=Hash::make($request->password);
		$user->save();

		return 1 ;
	}
    public function update(Request $request)
	{
        $company_ids = json_decode($request->companies);
		$user=User::find($request->id);
		$user->name=$request->name;
		$user->email=$request->email;
		$user->save();
        CompanyUser::where('user_id',$request->id)->delete();
        foreach($company_ids as $company_id)
        {

            if($company_id == 0)
            {
               $user->all_companies=1;
               $user->save();
            }
            else
            {

                CompanyUser::create(['user_id'=>$request->id,'company_id'=>$company_id]);
            }

           

        }
		return response()->json();
	}
    public function getalluser(Request $request)
    {
        if(Auth::user()->all_companies == 1)
        {
            $data =User::where('active',1)->get();
        }
        else
        {
            $temp = collect([]);
            $data=CompanyUser::where('user_id',Auth::user()->id)->get();
            foreach ($data as $key => $value) {
                 $temp_data=CompanyUser::where('company_id',$value->company_id)->with('user')->get();
                 foreach ($temp_data as $index) {
                    foreach ($index->user as $index_1) {
                        if($index_1->active == 1)
                        {
                            $temp->push($index_1);
                        }

                    }
                 }
            }
            $data=$temp->unique();
        }
        if ($request->ajax()) {
            return Datatables::of($data)
                ->addColumn('name', function ($data) {
                    $name='';
                    if(Auth()->user()->hasPermissionTo('user_name') || Auth::user()->all_companies == 1)
                    {
                        $name = $data->name;
                    }
                    return $name;
                })
                ->addColumn('email', function ($data) {
                    $email='';
                    if(Auth()->user()->hasPermissionTo('user_email') || Auth::user()->all_companies == 1)
                    {
                        $email = $data->email;
                    }
                    return $email;
                })
                ->addColumn('checkbox', function ($data) {
                })->addColumn('actions', function ($data) {
                    $response = "";

                    $roles_manage = '<button onClick="getuserrole(' . $data->id . ')" class="btn btn-success btn-xs" data-toggle="modal" data-target="#RoleModal1"><i class="fas fa-user-shield"></i></button>';


                    $edit = '<a onClick="getuserdata( ' . $data->id . ')" id="' . $data->id . '" class="btn btn-primary btn-xs" href=""  data-toggle="modal" data-target="#modalEditForm"><i class="fas fa-pencil-alt"></i></a>';


                    $delete = '<button type="button"class="btn btn-danger btn-xs my-0" onclick="deleteuserdata( ' . $data->id . ')"><i class="fas fa-trash"></i></button>';
                    $copy = '<button id="'.$data->id.'" type="button" class="copy_user btn btn-warning btn-xs my-0"><i class="fas fa-copy" style="color: white" ></i></button>';
                    if(Auth()->user()->hasPermissionTo('copy_users') || Auth::user()->all_companies == 1)
                    {
                        $response=$response.$copy;
                    }

                    if(Auth()->user()->hasPermissionTo('edit_users') || Auth::user()->all_companies == 1)
                    {
                        $response = $response . $edit;
                    }
                    // dd($data->all_companies);
                    if(Auth()->user()->hasPermissionTo('manage_roles') || Auth::user()->all_companies == 1 && $data->all_companies != 1)
                    {
                        //for super admin
                        $response = $response . $roles_manage;
                        
                    }
                    if(Auth()->user()->hasPermissionTo('delete_users')|| Auth::user()->all_companies == 1)
                    {
                        $response = $response . $delete;
                    }
                    

                   
                    return $response;
                })
                ->rawColumns(['actions'])
                ->make(true);

        }
    }
    public function multipleDelete(Request $request)
    {
        User::whereIn('id',$request->ids)->update(['active'=>0]);
        return response()->json(['message'=>'Deleted']);
    }
    public function getAllSoftUser()
    {
       if(Auth::user()->all_companies == 1)
        {
            $data =User::where('active',0)->where('deleted',0)->get();
        }
        else
        {
            $temp = collect([]);
            $data=CompanyUser::where('user_id',Auth::user()->id)->get();
            foreach ($data as $key => $value) {
                 $temp_data=CompanyUser::where('company_id',$value->company_id)->with('user')->get();
                 foreach ($temp_data as $index) {
                    foreach ($index->user as $index_1) {
                        if($index_1->active == 0 && $index_1->deleted== 0 )
                        {
                            $temp->push($index_1);
                        }

                    }
                 }
            }
                    
            
            $data=$temp->unique();
        }
        return Datatables::of($data)
            ->addColumn('name', function ($data) {
                $name='';
                if(Auth()->user()->hasPermissionTo('user_name') || Auth::user()->all_companies == 1)
                {
                    $name = $data->name;
                }
                return $name;
            })
            ->addColumn('email', function ($data) {
                $email='';
                if(Auth()->user()->hasPermissionTo('user_email') || Auth::user()->all_companies == 1)
                {
                    $email = $data->email;
                }
                return $email;
            })
            ->addColumn('checkbox', function ($data) {
            })->addColumn('actions', function ($data) {
               $response = "";
                    $Restore_button = '<button type="button"class="btn btn-success btn-xs my-0" onclick="restore_user( ' . $data->id . ')"><i class="fa fa-retweet" aria-hidden="true"></i></button>';


                    $hard_delete_button = '<button type="button"class="btn btn-danger btn-xs my-0" onclick="hard_delete_user( ' . $data->id . ')"><i class="fa fa-trash" aria-hidden="true"></i></button>';
                    if(Auth()->user()->hasPermissionTo('restore_delete_users') || Auth::user()->all_companies == 1)
                    {
                        $response = $response . $Restore_button;
                    }
                    if(Auth()->user()->hasPermissionTo('hard_delete_users') || Auth::user()->all_companies == 1)
                    {
                        $response = $response . $hard_delete_button;
                    }

               
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);

        
    }
    public function restore(Request $request)
    {
        User::where('id',$request->id)->update(['active'=>1]);
        return response()->json(['message'=>'restore']);
        
    }
    public function hardDelete(Request $request)
    {
         User::where('id',$request->id)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'hard delet']);
    }
    public function delete(Request $request)
    {
        User::where('id',$request->id)->update(['active'=>0]);
        return response()->json(['message'=>'delete']);
        
    }
     public function export_excel_user(Request $request)
    {
    
        if($request->export_excel_user == 1)
        {
            //for active user
            return Excel::download(new UsersExport, 'users.csv');
        }
        else
        {
            return Excel::download(new UsersExportSoft, 'users.csv');
            //for soft delete

        }

    }
    public function exportPdfUser(Request $request)
    {
        if(Auth::user()->all_companies == 1)
        {
            if($request->export_pdf_user == 1)
            {

                $users= User::where('active', '=',1)->get();
            }
            else
            {
                 $users= User::where('active', '=',0)->get();
            }
        }
        else
        {
            $users = collect([]);
            $data=CompanyUser::where('user_id',Auth::user()->id)->get();
                foreach ($data as $key => $value) {
                    if($request->export_pdf_user == 1)
                    {
                        if(Auth()->user()->hasPermissionTo('user_name_pdf_export') && Auth()->user()->hasPermissionTo('user_email_pdf_export'))
                        {
                            $temp_data=CompanyUser::where('company_id',$value->company_id)->with(array('user'=>function($query){
                            $query->select('id','name','email')->where('active', '=',1);
                            }))->get();
                        }
                        elseif(Auth()->user()->hasPermissionTo('user_name_pdf_export'))
                        {
                           $temp_data=CompanyUser::where('company_id',$value->company_id)->with(array('user'=>function($query){
                            $query->select('id','name')->where('active', '=',1);
                            }))->get();
                           
                        }elseif(Auth()->user()->hasPermissionTo('user_email_pdf_export'))
                        {
                            $temp_data=CompanyUser::where('company_id',$value->company_id)->with(array('user'=>function($query){
                            $query->select('id','email')->where('active', '=',1);
                            }))->get();
                            
                        }
                        else
                        {
                            $temp_data=[];
                           
                        }

                       

                    }
                    else
                    {
                        if(Auth()->user()->hasPermissionTo('user_name_pdf_export') && Auth()->user()->hasPermissionTo('user_email_pdf_export'))
                        {
                            $temp_data=CompanyUser::where('company_id',$value->company_id)->with(array('user'=>function($query){
                            $query->select('id','name','email')->where('active', '=',0);
                            }))->get();
                           
                        }
                        elseif(Auth()->user()->hasPermissionTo('user_name_pdf_export'))
                        {
                           $temp_data=CompanyUser::where('company_id',$value->company_id)->with(array('user'=>function($query){
                            $query->select('id','name')->where('active', '=',0);
                            }))->get();
                            
                        }elseif(Auth()->user()->hasPermissionTo('user_email_pdf_export'))
                        {
                            $temp_data=CompanyUser::where('company_id',$value->company_id)->with(array('user'=>function($query){
                            $query->select('id','email')->where('active', '=',0);
                            }))->get();
                            
                        }
                        else
                        {
                             $temp_data=[];
                        
                        }
                      
                    }
                    foreach ($temp_data as $index) {
                        foreach ($index->user as $index_1) {  
                                $users->push($index_1);
                        }
                     }
                }
                
        }
        $pdf = PDF::loadView('backend.user.pdf',compact('users'));
        return $pdf->download('invoice.pdf');
    }

    public function exportWordUser(Request $request)
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        if(Auth::user()->all_companies == 1)
        {
            if($request->export_word_user == 1)
            {

                $users= User::where('active', '=',1)->get();
            }
            else
            {
                 $users= User::where('active', '=',0)->get();
            }
            $text = $section->addText('Id   Name   Email',array('name'=>'Arial','size' => 20,'bold' => true));
        }
        else
        {
            $users = collect([]);
            $data=CompanyUser::where('user_id',Auth::user()->id)->get();
                foreach ($data as $key => $value) {
                    if($request->export_word_user == 1)
                    {
                        if(Auth()->user()->hasPermissionTo('user_name_word_export') && Auth()->user()->hasPermissionTo('user_email_word_export'))
                        {
                            $temp_data=CompanyUser::where('company_id',$value->company_id)->with(array('user'=>function($query){
                            $query->select('id','name','email')->where('active', '=',1);
                            }))->get();
                            $text = $section->addText('Id   Name   Email',array('name'=>'Arial','size' => 20,'bold' => true));
                        }
                        elseif(Auth()->user()->hasPermissionTo('user_name_word_export'))
                        {
                           $temp_data=CompanyUser::where('company_id',$value->company_id)->with(array('user'=>function($query){
                            $query->select('id','name')->where('active', '=',1);
                            }))->get();
                            $text = $section->addText('Id   Name',array('name'=>'Arial','size' => 20,'bold' => true));
                        }elseif(Auth()->user()->hasPermissionTo('user_email_word_export'))
                        {
                            $temp_data=CompanyUser::where('company_id',$value->company_id)->with(array('user'=>function($query){
                            $query->select('id','email')->where('active', '=',1);
                            }))->get();
                            $text = $section->addText('Id   Email',array('name'=>'Arial','size' => 20,'bold' => true));
                        }
                        else
                        {
                             $temp_data=[];
                            $text = $section->addText('empty',array('name'=>'Arial','size' => 20,'bold' => true));
                        }

                       

                    }
                    else
                    {
                        if(Auth()->user()->hasPermissionTo('user_name_word_export') && Auth()->user()->hasPermissionTo('user_email_word_export'))
                        {
                            $temp_data=CompanyUser::where('company_id',$value->company_id)->with(array('user'=>function($query){
                            $query->select('id','name','email')->where('active', '=',0);
                            }))->get();
                            $text = $section->addText('Id   Name   Email',array('name'=>'Arial','size' => 20,'bold' => true));
                        }
                        elseif(Auth()->user()->hasPermissionTo('user_name_word_export'))
                        {
                           $temp_data=CompanyUser::where('company_id',$value->company_id)->with(array('user'=>function($query){
                            $query->select('id','name')->where('active', '=',0);
                            }))->get();
                            $text = $section->addText('Id   Name',array('name'=>'Arial','size' => 20,'bold' => true));
                        }elseif(Auth()->user()->hasPermissionTo('user_email_word_export'))
                        {
                            $temp_data=CompanyUser::where('company_id',$value->company_id)->with(array('user'=>function($query){
                            $query->select('id','email')->where('active', '=',0);
                            }))->get();
                            $text = $section->addText('Id   Email',array('name'=>'Arial','size' => 20,'bold' => true));
                        }
                        else
                        {
                             $temp_data=[];
                            $text = $section->addText('empty',array('name'=>'Arial','size' => 20,'bold' => true));
                        }
                        //     $temp_data=CompanyUser::where('company_id',$value->company_id)->with(array('user'=>function($query){
                        //     $query->select('id','name','email')->where('active', '=',0);
                        //     }))->get();
                        //     $text = $section->addText('Id   Name   Email',array('name'=>'Arial','size' => 20,'bold' => true));
                    }

                    
                     foreach ($temp_data as $index) {
                        foreach ($index->user as $index_1) {  
                                $users->push($index_1);
                        }
                     }
                }
                
        }
        $users=$users->unique();
        foreach ($users as $key => $value) {
            $text = $section->addText($value->id .'         '. $value->name .'         '. $value->email);
        }  
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');

        $objWriter->save('users.docx');
        return response()->download(public_path('users.docx'));
    }
}
