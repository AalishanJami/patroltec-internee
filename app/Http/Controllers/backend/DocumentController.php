<?php
namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\FormSections;
use App\Company;
use App\Questions;
use App\Answers;
use App\AnswerTypes;
use App\AnswerGroups;
use App\Response;
use App\ResponseGroups;
use App\Forms;
use Session;
use PDF;
use Validator;
use Auth;
use App\VersionLog;
use Carbon\Carbon;
use App\FormVersion;
use App\StaticFormUpload;
use App\Location;
use App\Jobs;
use Config;
use App\FormType;
use App\JobGroup;
use App\User;
use App\PrePopulate;
use App\PopulateResponse;
use App\FormGroups;
class DocumentController extends Controller
{
    private $page_name;
    private $tab_name;
    private $raisejob_tab_name;
    public function __construct() {
        $this->page_name ='dynamic_forms';
        $this->tab_name ='document_library';
        $this->raisejob_tab_name ='jobs';
    }

    public function index()
    {
        $companies=Company::getCompanies();
        return view('backend.document.index')->with(['companies' => $companies]);
    }
    public function document_cms()
    {
        $cms_data=localization();
        $temp_array['name']['key']='name';
        $temp_array['name']['value']=checkKey('name',$cms_data);
        $temp_array['ref']['key']='ref';
        $temp_array['ref']['value']=checkKey('ref',$cms_data);
        $temp_array['location_type']['key']='location_type';
        $temp_array['location_type']['value']=checkKey('location_type',$cms_data);
        $temp_array['form_type']['key']='form_type';
        $temp_array['form_type']['value']=checkKey('form_type',$cms_data);
        $temp_array['pre_populate']['key']='pre_populate';
        $temp_array['pre_populate']['value']=checkKey('pre_populate',$cms_data);
        $temp_array['import_jobs']['key']='import_jobs';
        $temp_array['import_jobs']['value']=checkKey('import_jobs',$cms_data);
        $temp_array['req_delete']['key']='reason_for_delete';
        $temp_array['req_delete']['value']=checkKey('reason_for_delete',$cms_data);
        $temp_array['req_sign_off']['key']='require_sign_off';
        $temp_array['req_sign_off']['value']=checkKey('require_sign_off',$cms_data);
        $temp_array['app_form']['key']='show_form_onapp';
        $temp_array['app_form']['value']=checkKey('show_form_onapp',$cms_data);
        $temp_array['req_tag']['key']='is_tag_job';
        $temp_array['req_tag']['value']=checkKey('is_tag_job',$cms_data);
        $temp_array['view']['key']='view';
        $temp_array['view']['value']=checkKey('view',$cms_data);
        $temp_array['files']['key']='files';
        $temp_array['files']['value']=checkKey('files',$cms_data);
        $temp_array['lock_from']['key']='lock_job_from';
        $temp_array['lock_from']['value']=checkKey('lock_job_from',$cms_data);
        $temp_array['lock_job_after']['key']='lock_job_after';
        $temp_array['lock_job_after']['value']=checkKey('lock_job_after',$cms_data);
        $temp_array['lock_to']['key']='lock_job_before';
        $temp_array['lock_to']['value']=checkKey('lock_job_before',$cms_data);
        $temp_array['lock_job_before']['key']='lock_job_before';
        $temp_array['lock_job_before']['value']=checkKey('lock_job_before',$cms_data);
        $temp_array['valid_from']['key']='valid_from';
        $temp_array['valid_from']['value']=checkKey('valid_from',$cms_data);
        $temp_array['valid_to']['key']='valid_to';
        $temp_array['valid_to']['value']=checkKey('valid_to',$cms_data);
        $temp_array['notes']['key']='notes';
        $temp_array['notes']['value']=checkKey('notes',$cms_data);
        return $temp_array;

    }
    public function createDetail(Request $request)
    {
        $valid_to = Carbon::now()->addYear()->format('yy-m-d');
        $form_id = Session::get('folder_id');
        if($form_id != null){
            $data=Forms::create([
                'form_group_id' => $form_id,
                'valid_to'=>$valid_to,
                'company_id'=> Auth::user()->default_company,
            ]);
        }
        else {
            $data=Forms::create([
                'form_group_id' => 0,
                'valid_to'=>$valid_to,
                'company_id'=> Auth::user()->default_company,
            ]);
        }
        $input= $request->all();
        audit_log('',$input,Config::get('constants.store'),$this->page_name,'main_detail',$data->id,'',$data->id);
        return $this->detail($data->id);
    }
    public function detail($id)
    {

        $companies=Company::getCompanies();
        $forms = Forms::where('id',$id)->where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->first();
        if(!$forms)
        {
            return redirect('form_group');
        }
        Session::put('form_id',$id);
        Session::put('is_import',$forms->import_jobs);
        $pre_populate = PrePopulate::where('form_id',$id)->get();
        if(!$pre_populate->isEmpty())
        {
            $forms->pre_populate=1;
        }
        $sections = FormSections::where('shared',1)->where('active',1)->where('deleted',0)->get()->pluck('name','id');
        $forms_types=FormType::where('active',1)->where('deleted',0)->where('company_id',Auth::user()->default_company)->orderBy('id', 'desc')->get()->pluck('name','id')->toArray();
        $form_groups=FormGroups::where('active',1)->where('deleted',0)->where('company_id',Auth::user()->default_company)->orderBy('id', 'desc')->get()->pluck('name','id')->toArray();
        return view('backend.document.details')->with(['companies'=>$companies,'forms'=>$forms,'form_groups'=>$form_groups,'sections'=>$sections,'forms_types'=>$forms_types]);
    }

    public function updateDetail(Request $request)
    {
        $input = $request->all();
//        dd($input);
        if(isset($input['view']))
        {
            if($input['view']==2)
            {
                $input['doc_download'] = 1;
                $input['pdf_download'] = null;
            }else if($input['view']==3)
            {
                $input['pdf_download'] = 1;
                $input['doc_download'] = null;
            }
            else  if($input['view']==4)
            {
                $input['doc_download'] = 1;
                $input['pdf_download'] = 1;
            }else  if($input['view']==1)
            {
                $input['doc_download'] = null;
                $input['pdf_download'] = null;
            }
        }
        if(!isset($input['import_jobs']))
        {
            $input['import_jobs']=0;
        }

        if(!isset($input['req_sign_off']))
        {
            $input['req_sign_off']=0;
        }

        if(!isset($input['req_tag']))
        {
            $input['req_tag']=0;
        }

        if(!isset($input['app_form']))
        {
            $input['app_form']=0;
        }

        if(!isset($input['req_delete']))
        {
            $input['req_delete']=0;
        }

        if(isset($input['notes']))
        {
            $input['notes']=$request->notes;
        }
        $input['valid_from']=str_replace(',','',$input['valid_from']);
        $input['valid_to']=str_replace(',','',$input['valid_to']);
        $date = Carbon::parse($input['valid_from'])->toDateString();
        $input['valid_from'] = $date;
        $date = Carbon::parse($input['valid_to'])->toDateString();
        $input['valid_to'] = $date;
//        dd($input);
        $formdata=Forms::where('id',$input['id'])->first();
        if(isset($input['form_section_id'])){
            $input['form_section_id']=$input['form_section_id'];
        }else{
            $input['form_section_id']=null;
        }
        $forms=Forms::find($input['id'])->update($input);
        if(isset($input['form_section_id']))
        {
            // dd($input)
            foreach ($input['form_section_id'] as $key => $value) {
                $form_version = new FormVersion();
                $form_version->form_id = $input['id'];
                $form_version->form_section_id = $value;
                $form_version->save();
            }
        }
        if(isset($input['pre_populate']))
        {
            PrePopulate::create(['name'=>$input['name'],'form_id'=>$input['id']]);
        }
        unset($input['form_section_id']);
        unset($input['_token']);
        unset($formdata->form_section_id);
        $input['valid_from']=Carbon::parse($input['valid_from'])->toDateString();
        $input['valid_to']=Carbon::parse($input['valid_to'])->toDateString();
        audit_log($this->document_cms(),audit_log_data($input),Config::get('constants.update'),$this->page_name,'main_detail',$formdata->id,audit_log_data($formdata->toArray()),$formdata->id,'','forms');
        return redirect()->route('document.detail',[$input['id']]);
    }

    public function notes()
    {
        $flag=2;
        $companies=Company::getCompanies();
        return view('backend.document.notes')->with(['flag'=>$flag,'companies'=>$companies]);
    }
    public function upload()
    {
        $flag=2;
        $companies=Company::getCompanies();
        $date = Carbon::now()->format('jS M Y');
        $time = Carbon::now()->format('h:i');
        $locations = Location::all()->where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->pluck('name','id');
        $users = User::all()->where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->pluck('first_name','id');
        $answer_groups = AnswerGroups::where('sig_select',1)->where('active',1)->where('deleted',0)->pluck('name','id');
        $pre_populate = PrePopulate::where('form_id',Session::get('form_id'))->where('active',1)->orderBy('id', 'desc')->pluck('name','id');
        return view('backend.bulk_upload.index')->with('flag',$flag)->with('date',$date)->with('time',$time)->with('locations',$locations)->with('users',$users)->with('companies',$companies)->with('answer_groups',$answer_groups)->with('pre_populate',$pre_populate);
    }

    public function location()
    {
        return view('backend.document.location');
    }
    public function userDocument()
    {
        return view('backend.document.user_document');
    }
    public function answer()
    {
        $answer_groups = AnswerGroups::where('active',1)->where('deleted',0)->orderBy('id','desc')->get();
        return response()->json(['answer_groups'=>$answer_groups]);
    }
    public function forms()
    {
        // $forms=FormSections::where('active',1)->where('deleted',0)->where('form_id',Session::get('form_id'))->orderBy('order','asc')->get();
        // dd($forms->toArray());

        $companies=Company::getCompanies();
        //dd($companies);
        $answer_types = AnswerTypes::where('active',1)->where('deleted',0)->orderBy('id','desc')->get();
        $answer_groups = AnswerGroups::where('active',1)->where('deleted',0)->orderBy('id','desc')->get();
        if (Session::has('form_id')){
            $name = Forms::find(Session::get('form_id'))->name;
        }else{
            return redirect('form_group');
        }
        $pre_populate = PrePopulate::where('form_id',Session::get('form_id'))->orderBy('id', 'desc')->where('active', 1)->pluck('name','id')->toArray();
        $sig_select = AnswerGroups::where('sig_select',1)->where('active',1)->where('deleted',0)->pluck('name','id');

        return view('backend.document.forms')->with(['companies' => $companies,'name' => $name, 'answer_types' => $answer_types,'answer_groups'=>$answer_groups,'pre_populate'=>$pre_populate,'sig_select'=>$sig_select]);
    }
    public function folder()
    {
        return view('backend.document.folder');
    }
    public function folderInside($id)
    {
        return view('backend.document.folder_inside')->with(['id'=>$id]);

    }
    public function edit(Request $request)
    {
        $data = FormSections::where('id',$request->id)->where('active',1)->where('deleted',0)->first();
        return response()->json(['data'=>$data]);

    }
    public function reOrder(Request $request)
    {
        $data = $request->array;
        $data = array_map("unserialize", array_unique(array_map("serialize", $data)));
        foreach($data as $key=> $item){
            $index=++$key;
            FormSections::where('id','=',$item['id'])->update(array('order'=>(int)$index));
        }
        return response()->json(['success'=>true]);
    }
    public function getAll()
    {
        $form_version = FormVersion::where('form_id',Session::get('form_id'))->where('draft',1)->where('deleted',0)->orderBy('id','asc')->get()->pluck('form_section_id');
        $form_version = array_values($form_version->toArray());
        /*  $form_section = FormSections::where('is_complete',0)->where('active',1)->where('form_id',Session::get('form_id'))->where('deleted',0)->orderBy('order', 'asc')->with('questions.answer_type','questions.answer_group.answer')->get();*/
        $form_section = FormSections::whereIn('id',$form_version)->orderBy('order', 'asc')->with('questions.answer_type','questions.answer_group.answer')->orderBy('order','asc')->get();
        // dd($form_section);
        /*
                $form_array = Forms::find(Session::get('form_id'))->form_section_id;
                $selected = FormSections::where('id',$form_array)->get();
                $form_section  = $form_section->merge($selected);*/
        return response()->json(['form_section'=>$form_section]);
    }
    public function getAllActive(){
        $form_version = FormVersion::where('form_id',Session::get('form_id'))->where('active',1)->where('deleted',0)->orderBy('id','asc')->get()->pluck('form_section_id');

        $form_version = array_values($form_version->toArray());

        $form_section = FormSections::whereIn('id',$form_version)->orderBy('order', 'asc')->with('questions.answer_type','questions.answer_group.answer')->orderBy('order','asc')->get();
        return response()->json(['form_section'=>$form_section]);
    }

    public function copyActive()
    {
        $form_version = FormVersion::with('form_Section.questions.answer_group')->where('form_id',Session::get('form_id'))->where('active',1)->where('deleted',0)->orderBy('id','desc')->get();
        // dd($form_version);
        //$form_version = array_values($form_version->toArray());
        if($form_version!=null)
        {
            foreach ($form_version as $key => $value) {
                $form_section=$value->form_Section;
                $form_section->is_complete=0;
                $form_section=$form_section->toArray();
                unset($form_section['id']);
                $new_form_section=FormSections::create($form_section);
                foreach ($form_section['questions'] as $key_question => $index) {
                    $question=$index;
                    unset($question['id']);
                    $question['section_id']=$new_form_section->id;
                    Questions::create($question);
                }
                $new = new FormVersion();
                $new->form_section_id = $new_form_section->id;
                $new->form_id = Session::get('form_id');
                $new->save();
            }
        }
        return redirect()->back();
    }
    public function getAllQuestion(){
        $form_section = FormSections::where('shared',1)->where('active',1)->where('deleted',0)->orderBy('order', 'asc')->with('questions.answer_type','questions.answer_group.answer')->get();
        return response()->json(['form_section'=>$form_section]);
    }

    public function completeForm(Request $request)
    {

        $data=FormSections::whereIn('id',$request->form_section_ids)->with('questions.answer_type','questions.answer_group.answer')->where('active',1)->where('deleted',0);
        $static_form=StaticFormUpload::where('form_id',$request->id)->where('is_complete',1)->first();
        if($static_form)
        {
            $static_form=$static_form->id;
        }
        $data->update(['is_complete'=>1]);
        $version =  VersionLog::create(['company_id'=>$request->company_id,'form_id'=>$request->id,'date_time'=> Carbon::now(),'user_id'=>Auth::user()->id,'static_form_uploads_id'=>$static_form]);
        $form_version = FormVersion::whereIn('form_section_id',$request->form_section_ids)->where('form_id',Session::get('form_id'))->where('draft',1)->where('deleted',0);
        $form_version->update(['active'=>1,'draft'=>0,'version_id'=>$version->id]);
        $form_version = FormVersion::whereNotIn('form_section_id',$request->form_section_ids)->where('form_id',Session::get('form_id'));
        $form_version->update(['active'=>0,'draft'=>0]);
        $pre_populate = PrePopulate::where('form_id',Session::get('form_id'))->whereNull('version_id');
        $pre_populate->update(['version_id'=>$version->id]);
        return response()->json(['message'=>'Form Section Successfully Complete']);
    }

    public function cms_section()
    {
        $cms_data=localization();
        $temp_array=[];
        $temp_array['name']['key']='name';
        $temp_array['name']['value']=checkKey('name',$cms_data);
        $temp_array['is_repeatable']['key']='is_repeatable';
        $temp_array['is_repeatable']['value']=checkKey('is_repeatable',$cms_data);
        $temp_array['header']['key']='header';
        $temp_array['header']['value']=checkKey('header',$cms_data);
        $temp_array['header_height']['key']='header_height';
        $temp_array['header_height']['value']=checkKey('header_height',$cms_data);
        $temp_array['show_header']['key']='show_header';
        $temp_array['show_header']['value']=checkKey('show_header',$cms_data);
        $temp_array['footer']['key']='footer';
        $temp_array['footer']['value']=checkKey('footer',$cms_data);
        $temp_array['footer_height']['key']='footer_height';
        $temp_array['footer_height']['value']=checkKey('footer_height',$cms_data);
        $temp_array['show_footer']['key']='show_footer';
        $temp_array['show_footer']['value']=checkKey('show_footer',$cms_data);
        $temp_array['width']['key']='width';
        $temp_array['width']['value']=checkKey('width',$cms_data);
        $temp_array['form_section_column']['key']='column';
        $temp_array['form_section_column']['value']=checkKey('column',$cms_data);
        $temp_array['form_sections_height']['key']='height';
        $temp_array['form_sections_height']['value']=checkKey('height',$cms_data);
        $temp_array['signature']['key']='signature';
        $temp_array['signature']['value']=checkKey('signature',$cms_data);
        return $temp_array;
    }


    public function updateForm(Request $request)
    {
        $rules = [
            'header_height'=>'gte:50',
            'footer_height'=>'gte:50',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails())
        {
            $error = $validator->messages();
            $html='<ul>';
            foreach ($error->all() as $key => $value) {
                $html=$html.'<li>'.$value.'</li>';
            }
            $html=$html.'</ul>';
            return response()->json(['status'=> false, 'error'=> $html]);
        }
        $data=$request->all();
        if (isset($data['show_header'])){
            $data['show_header']='1';
        }else{
            $data['show_header']='0';
        }
        if (isset($data['show_footer'])){
            $data['show_footer']='1';
        }else{
            $data['show_footer']='0';
        }

        if (isset($data['show_footer'])){
            $data['form_sections_height'] = $request->form_sections_height;
        }else{
            $data['form_sections_height'] =null;
        }

        $data['form_section_column'] =null;
        if ($data['width']=='12'){
            $data['form_section_column'] = $request->form_section_column;
        }
        // $data['show_on_dashboard'] =$data['dashboard'];
        // unset($data['dashboard']);
        $form_section_data=FormSections::where('id',$data['id'])->first();
        if(!isset($data['is_repeatable']))
        {
            $data['is_repeatable'] = 0;
        }
        if(!isset($data['is_take_pic']))
        {
            $data['is_take_pic'] = 0;
        }
        if(!isset($data['is_select_doc']))
        {
            $data['is_select_doc'] = 0;
        }
        if(!isset($data['is_select_pic']))
        {
            $data['is_select_pic'] = 0;
        }
         if(!isset($data['is_notes']))
        {
            $data['is_notes'] = 0;
        }
        $form_section=FormSections::where('id',$data['id']);
        $form_section_data=$form_section->first();
        audit_log($this->cms_section(),audit_log_data($data),Config::get('constants.update'),$this->page_name,$this->tab_name,$form_section_data->id,audit_log_data($form_section_data->toArray()),Session::get('form_id'),'form_sections');
        $form_section->update($data);
        return response()->json(['message'=>'Data Successfully update','flag'=>1]);
    }
    public function store(Request $request)
    {
        // $forms=FormSections::where('form_id',Session::get('form_id'))->get()->toArray();
        // dd($forms);

        $order=FormSections::max('order');
        if(!$order)
        {
            $order=1;
        }else
        {
            $order=intval($order)+1;
        }
        $data=$request->all();
        $validate = [
            'company_id'=>'required|numeric',
            'header_height'=>'gte:50',
            'footer_height'=>'gte:50',
        ];
        $validator = Validator::make($request->all(), $validate);
        if ($validator->fails())
        {
            $error = $validator->messages();
            $html='<ul>';
            foreach ($error->all() as $key => $value) {
                $html=$html.'<li>'.$value.'</li>';
            }
            $html=$html.'</ul>';
            return response()->json(['status'=> false, 'error'=> $html]);
        }
        if (isset($request->form_sections_height)){
            $form_sections_height = $request->form_sections_height;
        }else{
            $form_sections_height =null;
        }
        $form_id=Session::get('form_id');
        $is_repeatable=0;
        if(isset($request->is_repeatable))
        {
            $is_repeatable=1;
        }

         if(!isset($data['is_take_pic']))
        {
            $data['is_take_pic'] = 0;
        }
        if(!isset($data['is_select_doc']))
        {
            $data['is_select_doc'] = 0;
        }
        if(!isset($data['is_select_pic']))
        {
            $data['is_select_pic'] = 0;
        }
         if(!isset($data['is_notes']))
        {
            $data['is_notes'] = 0;
        }

        if (isset($data['show_header'])){
            $data['show_header']='1';
        }else{
            $data['show_header']='0';
        }
        if (isset($data['show_footer'])){
            $data['show_footer']='1';
        }else{
            $data['show_footer']='0';
        }
        $data['form_section_column'] =null;
        if ($data['width']=='12'){
            $data['form_section_column'] = $request->form_section_column;
        }
        // dd($request->all());
        $form_section=FormSections::create([
            'name' => $request->name,
            'header' => $request->header,
            'footer' => $request->footer,
            'width' => $request->width,
            'order'=>$order,
            'form_id'=>$form_id,
            'show_on_dashboard' => $request->dashboard,
            'company_id' => $request->company_id,
            'show_header' => $data['show_header'],
            'header_height' => $request->header_height,
            'show_footer' => $data['show_footer'],
            'footer_height' => $request->footer_height,
            'form_section_column' => $data['form_section_column'],
            'form_sections_height' => $form_sections_height,
            'is_repeatable'=>$is_repeatable,
            'signature'=>$request->signature,
            'is_take_pic' => $data['is_take_pic'],
            'is_select_pic' => $data['is_select_pic'],
            'is_select_doc' => $data['is_select_doc'],
            'is_notes' => $data['is_notes'],
        ]);
        $form_version = new FormVersion();
        $form_version->form_section_id = $form_section->id;
        $form_version->form_id = Session::get('form_id');
        $form_version->save();
        $input= $request->all();
        audit_log($this->cms_section(),audit_log_data($input),Config::get('constants.store'),$this->page_name,$this->tab_name,$form_section->id,'',Session::get('form_id'),'form_sections');
        return response()->json(['message'=>'Save Data Successfully']);
    }
    public function deleteForm(Request $request)
    {
        audit_log($this->cms_section(),'',Config::get('constants.delete'),$this->page_name,$this->tab_name,$request->id,'',Session::get('form_id'),'form_sections','form_sections');

        FormSections::where('id',$request->id)->update(['active'=>0,'deleted'=>0]);
        FormVersion::where('form_id',Session::get('form_id'))->where('form_section_id',$request->id)->update(['active'=>0,'draft'=>0]);
        return response()->json(['message'=>'Successfully Delete']);
    }
    public function share(Request $request)
    {
        FormSections::where('id', $request->id)->update([
            'shared_section_id' => $request->id,
            'shared' => 1
        ]);

        return response()->json(['message'=>'Data Successfully Shared','flag'=>1]);
    }
    public function cloneForm(Request $request)
    {
        $data=FormSections::where('id',$request->id)->with('questions')->first()->toArray();
        $questions=$data['questions'];
        $order=$data['order'];
        unset($data['id']);
        unset($data['questions']);
        // $data['order']=intval($order)+1;
        // dd($data);
        $form_section=FormSections::create($data);
        foreach ($questions as $key => $value) {
            $value['section_id']=$form_section->id;
            unset($value['id']);
            Questions::create($value);
        }
        $form_version = new FormVersion();
        $form_version->form_section_id = $form_section->id;
        $form_version->form_id = Session::get('form_id');
        $form_version->save();
        return response()->json(['message'=>'Data Successfully Clone']);
    }

    public function cms_raisejob()
    {
        $cms_data=localization();
        $temp_array=[];
        $temp_array['form_id']['key']='form';
        $temp_array['form_id']['value']=checkKey('form',$cms_data);
        $temp_array['location_id']['key']='project_name';
        $temp_array['location_id']['value']=checkKey('project_name',$cms_data);
        $temp_array['user_id']['key']='assign_to';
        $temp_array['user_id']['value']=checkKey('assign_to',$cms_data);
        $temp_array['pre_populate_id']['key']='pre_populate';
        $temp_array['pre_populate_id']['value']=checkKey('pre_populate',$cms_data);
        $temp_array['date']['key']='date';
        $temp_array['date']['value']=checkKey('date',$cms_data);
        $temp_array['time']['key']='time';
        $temp_array['time']['value']=checkKey('time',$cms_data);
        return $temp_array;
    }

    public function raiseJob(Request $request)
    {
        $input = $request->all();
//        dd($input);
        $input['user_id'] = array_filter($input['user_id']);
        $input['completed'] = 0;
        $users =[];
        $users = $input['user_id'];
        $populate_id =null;
        if (isset($input['pre_populate_id'])) {
            $populate_id =$input['pre_populate_id'];
        }

        unset($input['pre_populate_id']);
        // array_push($users,$input['user_id']);
        //dd($users);
        //dd($input);
        $data = [
            'date'=>'required',
            'time'=>'required',
        ];
        $validator = Validator::make($request->all(), $data);
        if ($validator->fails())
        {
            $notification = array(
                'message' => 'date and time required',
                'alert-type' => 'error'
            );
            return redirect()->back()->with($notification);
        }
        $input['contractor_id'] = Location::find($input['location_id'])->contractor_id;

        //dd($form_sections);
        $form_sections = FormVersion::where('form_id',$input['form_id'])->where('active',1)->where('deleted',0)->orderBy('id','desc')->get();
        $form = Forms::find($input['form_id']);

        $form_sections = $form_sections->groupBy('version_id')->first()->pluck('form_section_id');
        $form_section = array_unique($form_sections->toArray());
        $form_sections = FormSections::whereIn('id',$form_section)->with('questions.answer_type','questions.answer_group.answer')->orderBy('order','asc')->get();

            $date = Carbon::parse($input['date']);
        $time = explode(':',$input['time']);
        $date->setTime($time[0],$time[1]);
        $input['date_time'] = $date;
        $pre_time= new Carbon($date);
        $post_time = new Carbon($date);
        if((int)$form->lock_job_after==1)
        {
            $input['post_time'] = ($post_time->addMinutes((int)$form->lock_to));
        } if((int)$form->lock_job_after==2)
    {
        $input['post_time'] = ($post_time->addHours((int)$form->lock_to));

    }  if((int)$form->lock_job_after==3)
    {
        $input['post_time'] = ($post_time->addDays((int)$form->lock_to));
    }  if((int)$form->lock_job_after==4)
    {
        $input['post_time'] = ($post_time->addWeeks((int)$form->lock_to));
    }else
    {
        $input['post_time'] = $post_time;
    }


        if((int)$form->lock_job_before==1)
        {
            $input['pre_time'] = ($pre_time->subMinutes((int)$form->lock_from));
        } if((int)$form->lock_job_before==2)
    {
        $input['pre_time'] = ($pre_time->subHours((int)$form->lock_from));
    }  if((int)$form->lock_job_before==3)
    {
        $input['pre_time'] = ($pre_time->subDays((int)$form->lock_from));
    }  if((int)$form->lock_job_before==4)
    {
        $input['pre_time'] = ($pre_time->subWeeks((int)$form->lock_from));
    }
    else
    {
        $input['pre_time'] = $pre_time;
    }
        unset($input['_token']);
        unset($input['date']);
        unset($input['time']);
        $input['title']=$form->name;
        $breakdown = [];

        if(isset( $input['location_breakdown_id'])){

            $breakdown = $input['location_breakdown_id'];
        }
        //dd($breakdown);
        $input['form_type_id'] = $form->type;

        $audit_input =$request->all();
        unset($audit_input['_token']);
        if (isset($audit_input['location_breakdown_id'])){
            $audit_input['location_breakdown_id']=json_encode($audit_input['location_breakdown_id']);
        }
        if (isset($audit_input['user_id'])){
            $audit_input['user_id']=json_encode($audit_input['user_id']);
        }
        $audit_input['date']=Carbon::parse($audit_input['date'])->toDateString();

        foreach ($users as $key => $user) {
            $input['user_id'] = $user;
            $input['user_group_id'] = User::find($user)->user_group_id;
            $input['company_id'] = Auth::user()->default_company;
            if(isset( $input['location_breakdown_id'])){
                foreach ($breakdown as $value) {
                    $input['location_breakdown_id']=$value;
                    // dd($input);
                    $jobGroup = JobGroup::create($input);
                    $input['job_group_id'] = $jobGroup->id;
                    $x = [];
                    $input['tag'] = $form->req_tag;
                    foreach ($form_sections as $form_section) {
                        array_push($x,"1");
                        $input['form_section_id'] = $form_section->id;
                        $input['order'] = $form_section->order;
                        unset($input['company_id']);
                        $job = Jobs::Create($input);
                        // dd($populate_id);
                        if($populate_id!='null' || $populate_id!=null)
                        {
                            unset($input['completed']);

                            $rds = ResponseGroups::where('location_id',$input['location_id'])->where('location_breakdown_id',$value)->where('pre_populate_id',$populate_id)->where('active',1)->where('deleted',0)->get();
                            //dd($rds);
                            if($rds->isEmpty())
                            {

                                $jobs = Jobs::where('job_group_id',$jobGroup->id)->orderBy('order','asc')->get();

                                $responseGroups = ResponseGroups::create($input);
                                $populates = PopulateResponse::where('pre_populate_id',$populate_id)->where('form_section_id',$form_section->id)->where('active',1)->where('deleted',0)->get();

                                /*$populates = PopulateResponse::where('pre_populate_id',$populate_id)->where('active',1)->where('deleted',0)->get();*/
                                //dd($populates);
                                foreach ($populates as $key => $populate) {
                                    //dd($populates);
                                    $populate->response_group_id = $responseGroups->id;
                                    $populate->job_id = $job->id;
                                    Response::create($populate->toArray());

                                }


                            }
                            else
                            {
                                foreach ($rds as $key => $rd) {
                                    $nrd = $rd->replicate();
                                    $nrd->job_group_id = $jobGroup->id;
                                    $nrd->save();
                                    $rps = Response::where('response_group_id',$rd->id)->get();
                                    //dd($rps);
                                    foreach ($rps as $key => $rp) {
                                        $nrp = $rp->replicate();
                                        $nrp->response_group_id = $nrd->id;
                                        $nrp->job_id = $job->id;
                                        $nrp->save();

                                    }


                                }



                            }
                        }
                        audit_log($this->cms_raisejob(),audit_log_data($audit_input),Config::get('constants.store'),$this->page_name,$this->raisejob_tab_name,$job->id,'','',$job->id,'jobs');
                    }
                    audit_log($this->cms_raisejob(),audit_log_data($audit_input),Config::get('constants.store'),$this->page_name,$this->raisejob_tab_name,$jobGroup->id,'','',$jobGroup->id,'job_groups');
                }
            }
            else
            {
                $jobGroup = JobGroup::create($input);
                $input['job_group_id'] = $jobGroup->id;
                $x = [];
                $input['tag'] = $form->req_tag;
                foreach ($form_sections as $form_section) {
                    array_push($x,"1");
                    $input['form_section_id'] = $form_section->id;
                    $input['order'] = $form_section->order;
                    unset($input['company_id']);
                    $job =Jobs::Create($input);

                    if($populate_id=='null' || $populate_id==null)
                    {
                        unset($input['completed']);
                        $rds = ResponseGroups::where('location_id',$input['location_id'])->where('pre_populate_id',$populate_id)->where('active',1)->where('deleted',0)->get();
                        //dd($rds);
                        if($rds->isEmpty())
                        {
                            $responseGroups = ResponseGroups::create($input);
                            $jobs = Jobs::where('job_group_id',$jobGroup->id)->orderBy('order','asc')->get();
                            $populates = PopulateResponse::where('pre_populate_id',$populate_id)->where('form_section_id',$form_section->id)->where('active',1)->where('deleted',0)->get();
                            // dd($populates,'if',$input['pre_populate_id']);
                            foreach ($populates as $key => $populate) {
                                $populate->response_group_id = $responseGroups->id;
                                $populate->job_id = $job->id;
                                $res=Response::create($populate->toArray());
                                audit_log($this->cms_raisejob(),audit_log_data($audit_input),Config::get('constants.store'),$this->page_name,$this->raisejob_tab_name,$res->id,'','',$res->id,'responses');
                            }
                        }
                        else
                        {
                            // dd($rds,'else');
                            foreach ($rds as $key => $rd) {
                                $nrd = $rd->replicate();
                                $nrd->job_group_id = $jobGroup->id;
                                $nrd->save();
                                $rps = Response::where('response_group_id',$rd->id)->get();
                                //dd($rd,'else');
                                foreach ($rps as $key => $rp) {
                                    $nrp = $rp->replicate();
                                    $nrp->response_group_id = $nrd->id;
                                    $nrp->job_id = $job->id;
                                    $nrp->save();
                                }
                                audit_log($this->cms_raisejob(),audit_log_data($audit_input),Config::get('constants.update'),$this->page_name,$this->raisejob_tab_name,$rd->id,audit_log_data($rd->toArray()),'',$rd->id,'responses');
                            }
                        }
                    }
                    audit_log($this->cms_raisejob(),audit_log_data($audit_input),Config::get('constants.store'),$this->page_name,$this->raisejob_tab_name,$job->id,'','',$job->id,'jobs');
                }
                audit_log($this->cms_raisejob(),audit_log_data($audit_input),Config::get('constants.store'),$this->page_name,$this->raisejob_tab_name,$jobGroup->id,'','',$jobGroup->id,'job_groups');
            }
        }
        if(empty($x))
        {
            $notification = array(
                'message' => 'No active Form found with Questions',
                'alert-type' => 'error'
            );
            return redirect()->back()->with($notification);
        }
        else
        {
            $notification = array(
                'message' => 'Job raised Successfully',
                'alert-type' => 'success'
            );

            return redirect()->back()->with($notification);

        }


    }
    public function getAllPopulate()
    {
        $data = PrePopulate::where('form_id',Session::get('form_id'))->where('active',1)->orderBy('id', 'desc')->get();
        $pre_populate = PrePopulate::where('form_id',Session::get('form_id'))->where('active',1)->orderBy('id', 'desc')->pluck('name','id')->toArray();
        return response()->json(['data'=>$data,'flag'=>1,'pre_populate'=>$pre_populate]);

    }

    public function storePopulate()
    {
        $data=PrePopulate::create(['name'=>'']);
        return response()->json(['data'=>$data]);
    }

    public function updatePopulate(Request $request)
    {
        $input = $request->all();
        PrePopulate::find($input['id'])->update(['name'=>$input['name'],'form_id'=>Session::get('form_id')]);
        $pre_populate = PrePopulate::where('form_id',Session::get('form_id'))->where('active',1)->orderBy('id', 'desc')->pluck('name','id')->toArray();
        $data = PrePopulate::where('form_id',Session::get('form_id'))->where('active',1)->orderBy('id', 'desc')->get();

        return response()->json(['message'=>'Successfully added','pre_populate'=>$pre_populate,'data'=>$data,'flag'=>1]);

    }

    public function deletePopulate(Request $request)
    {
        PrePopulate::where('id',$request->id)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Deleted','flag'=>1]);
    }

}
