<?php
namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Questions;
use App\AnswerTypes;
use App\AnswerGroups;
use Config;
use Session;
class QuestionController extends Controller
{
    private $page_name;
    private $tab_name;
    private $db;
    private $cms_array=[];
    public function __construct() {
        $this->page_name ='questions';
        $this->tab_name ='document_library';
        $this->db='questions';
        $cms_data=localization();
        $temp_array=[];
        $temp_array['question']['key']='questions';
        $temp_array['question']['value']=checkKey('questions',$cms_data);
        $temp_array['question_mandatory']['key']='mandatory_message';
        $temp_array['question_mandatory']['value']=checkKey('mandatory_message',$cms_data);
        $temp_array['import_name']['key']='import_name';
        $temp_array['import_name']['value']=checkKey('import_name',$cms_data);
        $temp_array['order']['key']='order';
        $temp_array['order']['value']=checkKey('order',$cms_data);
        $temp_array['answer_type_id']['key']='answer_type';
        $temp_array['answer_type_id']['value']=checkKey('answer_type',$cms_data);
        $temp_array['comment']['key']='comment';
        $temp_array['comment']['value']=checkKey('comment',$cms_data);
        $temp_array['column_size']['key']='column_size';
        $temp_array['column_size']['value']=checkKey('column_size',$cms_data);
        $temp_array['horizontal']['key']='horizontal';
        $temp_array['horizontal']['value']=checkKey('horizontal',$cms_data);
        $temp_array['vertical']['key']='vertical';
        $temp_array['vertical']['value']=checkKey('vertical',$cms_data);
        $temp_array['answer_groups_id']['key']='answer';
        $temp_array['answer_groups_id']['value']=checkKey('answer',$cms_data);
        $this->cms_array=$temp_array;
    }
    public function edit($id)
    {
        $data=Questions::where('id',$id)->with('answer_type')->first();
        $sig_select = AnswerGroups::where('sig_select',1)->where('active',1)->where('deleted',0)->get();
        $answer_types=AnswerTypes::where('active',1)->where('deleted',0)->get();
        $answer_groups=AnswerGroups::where('active',1)->where('deleted',0)->get();
        return response()->json(['data'=>$data,'answer_types'=>$answer_types,'answer_groups'=>$answer_groups,'sig_select'=>$sig_select]);
    }
    public function update(Request $request)
    {
        // dd($request->all());
        $question=$this->removePTag($request->question_name);
        $question_mandatory=$this->removePTag($request->question_mandatory);
        $comment=$this->removePTag($request->comment);
        if($request->signature_required_app_select_menu!=null)
        {
            $data=[
                'company_id' => $request->company_id,
                'question' =>   $question,
                'layout' => $request->layout,
                'order' => $request->question_number,
                'answer_type_id' => $request->answer_number,
                'answer_groups_id' => $request->sig_select,
                'mandatory_message' => $question_mandatory,
                'icon_size' => $request->icon_size,
                'column_size' => $request->column_size,
                'comment' => $comment,
                'import_name' => $request->import_name,
                'signature_required_app_user' => $request->signature_required_app_user,
                'signature_required_app_text_field' => $request->signature_required_app_text_field,
                'signature_required_app_select_menu' => $request->signature_required_app_select_menu
            ];
         }
         else
         {
            $data=[
                'company_id' => $request->company_id,
                'question' => $question,
                'answer_type_id' => $request->answer_number,
                'layout' => $request->layout,
                'order' => $request->question_number,
                'answer_groups_id' => $request->answer_group,
                'mandatory_message' => $question_mandatory,
                'icon_size' => $request->icon_size,
                'column_size' => $request->column_size,
                'comment' => $comment,
                'import_name' => $request->import_name,
                 'signature_required_app_user' => $request->signature_required_app_user,
                'signature_required_app_text_field' => $request->signature_required_app_text_field,
                'signature_required_app_select_menu' => $request->signature_required_app_select_menu

            ];
        }
        $form_question_data=Questions::where('id', $request->id)->first();
        $input= $request->all();
        unset($input['signature_required_app_text_field']);
        unset($input['sig_select']);
        unset($input['signature_required_app_select_menu']);
        $input['order']=$input['question_number'];
        unset($input['question_number']);
        $input['question']=$input['question_name'];
        unset($input['question_name']);
        $input['answer_type_id']=$input['answer_number'];
        unset($input['answer_number']);
        $input['answer_groups_id']=$input['answer_group'];
        unset($input['answer_group']);
        unset($input['section_id']);
        // dd($input);
        audit_log($this->cms_array,audit_log_data($input),Config::get('constants.update'),$this->page_name,$this->tab_name,$form_question_data->id,audit_log_data($form_question_data->toArray()),Session::get('form_id'),$this->db);
        $data=Questions::where('id', $request->id)->update($data);
        return response()->json(['message'=>'Successfully Updated !!']);

    }

    public function delete(Request $request)
    {
        $data=[
            'active' => 0,
        ];
        Questions::where('id', $request->id)->update($data);
    audit_log($this->cms_array,'',Config::get('constants.delete'),$this->page_name,$this->tab_name,$request->id,'',Session::get('form_id'),$request->id,$this->db);

        return response()->json(['message'=>'Successfully delete !!']);
    }
    public function removePTag($value)
    {
        $value=str_ireplace('<p>','',$value);
        $value=str_ireplace('</p>','',$value);
        return $value;
    }
    public function store(Request $request)
    {
         //dd($request->all());
        $question=$this->removePTag($request->question_name);
        $comment=$this->removePTag($request->comment);
        $mandatory_message=$this->removePTag($request->mandatory_message);

        if($request->signature_required_app_select_menu!=null)
        {
            $data=Questions::create([
                'company_id' => $request->company_id,
                'section_id' => $request->section_id,
                'question' =>   $question,
                'layout' => $request->layout,
                'order' => $request->question_number,
                'answer_type_id' => $request->answer_number,
                'answer_groups_id' => $request->sig_select,
                'icon_size' => $request->icon_size,
                'column_size' => $request->column_size,
                'comment' => $comment,
                'import_name' => $request->import_name,
                'display_pic' => $request->display_picture,
                'mandatory_message' => $mandatory_message,
                'signature_required_app_user' => $request->signature_required_app_user,
                'signature_required_app_text_field' => $request->signature_required_app_text_field,
                'signature_required_app_select_menu' => $request->signature_required_app_select_menu
            ]);
         }
         else
         {
            $data=Questions::create([
                'company_id' => $request->company_id,
                'section_id' => $request->section_id,
                'question' => $question,
                'answer_type_id' => $request->answer_number,
                'layout' => $request->layout,
                'order' => $request->question_number,
                'answer_groups_id' => $request->answer_group,
                'icon_size' => $request->icon_size,
                'column_size' => $request->column_size,
                'comment' => $comment,
                'display_pic' => $request->display_picture,
                'import_name' => $request->import_name,
                'mandatory_message' => $mandatory_message,
                 'signature_required_app_user' => $request->signature_required_app_user,
                'signature_required_app_text_field' => $request->signature_required_app_text_field,
                'signature_required_app_select_menu' => $request->signature_required_app_select_menu
            ]);
        }
        $input= $request->all();
        unset($input['signature_required_app_text_field']);
        unset($input['sig_select']);
        unset($input['signature_required_app_select_menu']);
        $input['order']=$input['question_number'];
        unset($input['question_number']);
        $input['question']=$input['question_name'];
        unset($input['question_name']);
        $input['answer_type_id']=$input['answer_number'];
        unset($input['answer_number']);
        $input['answer_groups_id']=$input['answer_group'];
        unset($input['answer_group']);
        unset($input['section_id']);
        // dd($input);
        audit_log($this->cms_array,audit_log_data($input),Config::get('constants.store'),$this->page_name,$this->tab_name,$data->id,'',Session::get('form_id'),$this->db);
        return response()->json(['message'=>'Successfully Added !!']);

    }


}
