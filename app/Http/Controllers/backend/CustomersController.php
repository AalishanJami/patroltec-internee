<?php
namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Company;
use App\SiteGroups;
use App\Customers;
use App\Divisions;
use App\Location;
use Session;
use Auth;
use Config;
use App\Exports\customer\CustomerExport;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use PDF;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Hash;
use Validator;
use Carbon\Carbon;
use DataTables;
class CustomersController extends Controller
{
    private $page_name;
    private $tab_name;
    private $db;
    private $cms_array=[];
    public function __construct() {
        $this->page_name ='customer';
        $this->tab_name ='main_detail';
        $this->db='customers';
        $cms_data=localization();
        $temp_array=[];
        $temp_array['name']['key']='name';
        $temp_array['name']['value']=checkKey('name',$cms_data);
        $temp_array['division_id']['key']='devision';
        $temp_array['division_id']['value']=checkKey('devision',$cms_data);
        $this->cms_array=$temp_array;
    }
    public function getAll()
    {
    	$divisions=Divisions::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->get();
        $data=Customers::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->get();
        $project = Location::find(Session::get('project_id'));
        return Datatables::of($data)->with(['divisions'=>$divisions,'project'=>$project])
            ->addColumn('checkbox', function ($data) {
            })
            ->addColumn('submit', function ($data) {
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                $delete = '<a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light delete-btn' . $data->id . '" onclick="deletecustomerdata( ' . $data->id . ')"><i class="fas fa-trash"></i></a>';
                $response = $response . $delete;
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    public function getAllbySite(Request $request)
    {
        $data=SiteGroups::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->where('division_id',$request->division_id)->where('customer_id',$request->customer_id)->orderBy('id','desc')->get();
        return response()->json(['data'=>$data,'flag'=>0]);
    }
    public function getAllbyDivision(Request $request)
    {
        $divisions=Divisions::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id','desc')->get();
        $data=Customers::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->where('division_id',$request->division_id)->orderBy('id','desc')->get();
        $project = Location::find(Session::get('project_id'));
        return response()->json(['data'=>$data,'project'=>$project,'flag'=>0]);
    }

    public function getAllSoft()
    {
        $divisions=Divisions::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->get();
        $data=Customers::where('company_id',Auth::user()->default_company)->where('active',0)->where('deleted',0)->get();
        return Datatables::of($data)->with(['divisions'=>$divisions])
            ->addColumn('checkbox', function ($data) {
            })
            ->addColumn('submit', function ($data) {
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                $restore_button = '<button type="button"class="btn btn-success btn-xs my-0" onclick="restorecustomerdata('.$data->id.')"><i class="fa fa-retweet" aria-hidden="true"></i></button>';
                $hard_delete_button = '<button type="button"class="btn btn-danger btn-xs my-0" onclick="deletesoftcustomerdata('.$data->id.')"><i class="fa fa-trash" aria-hidden="true"></i></button>';
                $response = $response . $restore_button;
                $response = $response . $hard_delete_button;
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
    public function store()
    {
        $data=Customers::create(['name'=>'']);
    audit_log($this->cms_array,'',Config::get('constants.store'),$this->page_name,$this->tab_name,$data->id,'',$data->id,Session::get('project_id'),$this->db);
        $divisions=Divisions::where('active',1)->where('deleted',0)->get();
        return response()->json(['data'=>$data,'divisions'=>$divisions]);
    }
    public function update(Request $request)
    {
        if (isset($request->name)){
            $data = [
                'company_id'=>'required|numeric',
                'name'=>'required',
            ];
        }else{
            $data = [
                'name'=>'required',
            ];
        }
        $validator = Validator::make($request->all(), $data);
        if ($validator->fails())
        {
            $error = $validator->messages();
            $html='<ul>';
            foreach ($error->all() as $key => $value) {
                $html=$html.'<li>'.$value.'</li>';
            }
            $html=$html.'</ul>';
            return response()->json(['status'=> false, 'error'=> $html]);
        }
        $data=$request->all();
        if (isset($request->name)){
            $data['name']=$request->name;
        }else{
            $data['name']='';
        }
        $Customers=Customers::where('id',$data['id']);
        $Customers_first=$Customers->first();
        audit_log($this->cms_array,audit_log_data($data),Config::get('constants.update'),$this->page_name,$this->tab_name,$Customers_first->id,audit_log_data($Customers_first->toArray()),$Customers_first->id,Session::get('project_id'),$this->db);
        $Customers->update($data);
        if($Customers_first->active == 0)
        {
            $flag=0;
        }
        else
        {
            $flag=1;
        }

        return response()->json(['message'=>'Successfully Added !!','flag'=>$flag]);

    }
    public function softDelete (Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.soft_delete'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('project_id'),$this->db);
        Customers::where('id',$request->id)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function delete(Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.delete'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('project_id'),$this->db);
        Customers::where('id',$request->id)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function active(Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.restore'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('project_id'),$this->db);
        Customers::where('id',$request->id)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
    public function activeMultiple(Request $request)
    {
        Customers::whereIn('id',$request->ids)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
     public function deleteMultipleSOft(Request $request)
    {
        Customers::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function deleteMultiple(Request $request)
    {
        Customers::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function exportPdf(Request $request)
    {
        if($request->pdf_array == 1)
        {
            $data =Customers::where('company_id',Auth::user()->default_company)->with('division')->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->pdf_array);
            $data= Customers::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->with('division')->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        $date=Carbon::now()->format('D jS M Y');
        $companies='Patroltec';
        $reportname='Customer';
        $pdf = PDF::loadView('backend.customers.pdf',compact('data','date','reportname','companies'));
        return $pdf->download('customer.pdf');
    }
    public function exportWorld(Request $request)
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        if($request->word_array == 1)
        {
            $data= Customers::where('company_id',Auth::user()->default_company)->with('division')->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->word_array);
            $data= Customers::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->with('division')->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        $table = $section->addTable();
        foreach ($data as $key => $value) {
            if($key==0)
            {
                $table->addRow();
                if(Auth()->user()->hasPermissionTo('customer_name_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Name');
                }
                if(Auth()->user()->hasPermissionTo('customer_division_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Division');
                }
            }
            $table->addRow();
            if(Auth()->user()->hasPermissionTo('customer_name_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText($value->name);
            }
            if(Auth()->user()->hasPermissionTo('customer_division_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText($value->division->name);
            }
        }
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('customer.docx');
        return response()->download(public_path('customer.docx'));
    }
    public function exportExcel(Request $request)
    {
        if($request->excel_array == 1)
        {
            $data= Customers::where('company_id',Auth::user()->default_company)->with('division')->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->excel_array);
            $data= Customers::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->with('division')->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        return Excel::download(new CustomerExport($data), 'customer.csv');
    }
}
