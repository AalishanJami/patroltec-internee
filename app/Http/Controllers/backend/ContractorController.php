<?php
namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Company;
use App\Exports\contractor\ContractorExport;
use App\Contractor;
use PDF;
use DataTables;
use Maatwebsite\Excel\Facades\Excel;
use Validator;
use Carbon\Carbon;
use Auth;
use Config;
use Session;
class ContractorController extends Controller
{
    private $page_name;
    private $tab_name;
    private $db;
    private $cms_array=[];
    public function __construct() {
        $this->page_name ='contractor';
        $this->tab_name ='main_detail';
        $this->db='contractors';
        $cms_data=localization();
        $temp_array=[];
        $temp_array['name']['key']='name';
        $temp_array['name']['value']=checkKey('name',$cms_data);
        $temp_array['start']['key']='start_date';
        $temp_array['start']['value']=checkKey('start_date',$cms_data);
        $temp_array['end']['key']='expiry_date';
        $temp_array['end']['value']=checkKey('expiry_date',$cms_data);
        $temp_array['banned']['key']='banned';
        $temp_array['banned']['value']=checkKey('banned',$cms_data);
        $temp_array['preferred']['key']='allowed';
        $temp_array['preferred']['value']=checkKey('allowed',$cms_data);
        $this->cms_array=$temp_array;
    }
    public function getAll()
    {
        $data=Contractor::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {
            })
            ->addColumn('submit', function ($data) {
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                $delete = '<a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light delete-btn' . $data->id . '" onclick="deletecontractordata( ' . $data->id . ')"><i class="fas fa-trash"></i></a>';
                $response = $response . $delete;
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);

    }
    public function getAllSoft()
    {
        $data=Contractor::where('company_id',Auth::user()->default_company)->where('active',0)->where('deleted',0)->orderBy('id', 'desc')->get();
        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {
            })
            ->addColumn('submit', function ($data) {
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                $restore_button = '<button type="button"class="btn btn-success btn-xs my-0" onclick="restorecontractordata('.$data->id.')"><i class="fa fa-retweet" aria-hidden="true"></i></button>';
                $hard_delete_button = '<button type="button"class="btn btn-danger btn-xs my-0" onclick="deletesoftcontractordata('.$data->id.')"><i class="fa fa-trash" aria-hidden="true"></i></button>';
                $response = $response . $restore_button;
                $response = $response . $hard_delete_button;
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
    public function store()
    {
        $data=Contractor::create();
    audit_log($this->cms_array,'',Config::get('constants.store'),$this->page_name,$this->tab_name,$data->id,'',$data->id,Session::get('project_id'),$this->db);
        return response()->json(['data'=>$data]);
    }
    public function update(Request $request)
    {
        $data = [
                'company_id'=>'required|numeric',
                // 'start'=>'date',
                // 'end'=>'date',
                ];
        $validator = Validator::make($request->all(), $data);
        if ($validator->fails())
        {
            $error = $validator->messages();
            $html='<ul>';
            foreach ($error->all() as $key => $value) {
                $html=$html.'<li>'.$value.'</li>';
            }
            $html=$html.'</ul>';
            return response()->json(['status'=> false, 'error'=> $html]);
        }
        $data=$request->all();
        if(!$data['start'])
        {
            unset($data['start']);

            // $data['start']= date("Y-m-d", strtotime($data['start']));
        }
        if(!$data['end'])
        {  unset($data['end']);
            // $data['end']= date("Y-m-d", strtotime($data['end']));
        }
        $contractor=Contractor::where('id',$data['id']);
        $contractor_first=$contractor->first();
        audit_log($this->cms_array,audit_log_data($data),Config::get('constants.update'),$this->page_name,$this->tab_name,$contractor_first->id,audit_log_data($contractor_first->toArray()),$contractor_first->id,Session::get('project_id'),$this->db);
        $contractor->update($data);
        if($contractor_first->active == 0)
        {
            $flag=0;
        }
        else
        {
            $flag=1;
        }

        return response()->json(['message'=>'Successfully Added !!','flag'=>$flag]);

    }
    public function softDelete (Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.soft_delete'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('project_id'),$this->db);
        Contractor::where('id',$request->id)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }

    public function delete(Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.delete'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('project_id'),$this->db);
        Contractor::where('id',$request->id)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function active(Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.restore'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('project_id'),$this->db);
        Contractor::where('id',$request->id)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
    public function activeMultiple(Request $request)
    {
        Contractor::whereIn('id',$request->ids)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
     public function deleteMultipleSOft(Request $request)
    {
        Contractor::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function deleteMultiple(Request $request)
    {
        Contractor::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function exportPdf(Request $request)
    {
        if($request->pdf_array == 1)
        {
            $data =Contractor::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->pdf_array);
            $data= Contractor::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        $date=Carbon::now()->format('D jS M Y');
        $companies='Patroltec';
        $reportname='Contractor';
        $pdf = PDF::loadView('backend.contractor.pdf',compact('data','date','reportname','companies'));
        return $pdf->download('contractor.pdf');
    }

    public function exportWorld(Request $request)
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        if($request->word_array == 1)
        {
            $data= Contractor::where('company_id',Auth::user()->default_company)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->word_array);
            $data= Contractor::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        $table = $section->addTable();
        foreach ($data as $key => $value) {
            if($key==0)
            {
                $table->addRow();
                if(Auth()->user()->hasPermissionTo('contractor_name_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Name');
                }
                if(Auth()->user()->hasPermissionTo('contractor_start_date_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Start');
                }
                if(Auth()->user()->hasPermissionTo('contractor_end_date_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('End');
                }
                if(Auth()->user()->hasPermissionTo('contractor_allowed_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Allowed');
                }
                if(Auth()->user()->hasPermissionTo('contractor_banned_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Banned');
                }
            }
            $table->addRow();
            if(Auth()->user()->hasPermissionTo('contractor_name_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText($value->name);
            }
            if(Auth()->user()->hasPermissionTo('contractor_start_date_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText(Carbon::parse($value->start)->format('jS M Y'));
            }
            if(Auth()->user()->hasPermissionTo('contractor_end_date_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText(Carbon::parse($value->end)->format('jS M Y'));
            }
            if(Auth()->user()->hasPermissionTo('contractor_allowed_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText($value->preferred);
            }
            if(Auth()->user()->hasPermissionTo('contractor_banned_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText($value->banned);
            }

        }
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('contractor.docx');
        return response()->download(public_path('contractor.docx'));
    }
    public function exportExcel(Request $request)
    {
        if($request->excel_array == 1)
        {
            $data= Contractor::where('company_id',Auth::user()->default_company)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->excel_array);
            $data= Contractor::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        return Excel::download(new ContractorExport($data), 'contractor.csv');
    }

}
