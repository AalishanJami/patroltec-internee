<?php
namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Company;
use App\Support;
use DataTables;
use App\Exports\emails\EmailExport;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use Auth;
use Carbon\Carbon;
class EmailController extends Controller
{
    public function index()
    {
        $companies=Company::getCompanies();
        return view('backend.emails.index')->with(['companies'=>$companies]);
    }
    public function getAll()
    {
        $data=Support::where('company_id',Auth::user()->default_company)->with('user')->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        return Datatables::of($data)->make(true);
    }
    public function exportPdf(Request $request)
    {
        if($request->pdf_array == 1)
        {
            $data=Support::where('company_id',Auth::user()->default_company)->with('user')->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->pdf_array);
            $data=Support::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->with('user')->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        $date=Carbon::now()->format('D jS M Y');
        $companies='Patroltec';
        $reportname='Email';
        $pdf = PDF::loadView('backend.emails.pdf',compact('data','date','reportname','companies'));
        return $pdf->download('email.pdf');
    }

    public function exportWord(Request $request)
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();

        if($request->word_array == 1)
        {
            $data=Support::where('company_id',Auth::user()->default_company)->with('user')->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->word_array);
            $data=Support::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->with('user')->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        $text = $section->addText('Word Export data');
        $table = $section->addTable();
        foreach ($data as $key => $value) {
            if($key==0) {
                $table->addRow();
                if(Auth()->user()->hasPermissionTo('emailReport_ticketid_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Ticket ID');
                }else{
                    $table->addCell(1750)->addText('');
                }
                if(Auth()->user()->hasPermissionTo('emailReport_username_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('User Name');
                }else{
                    $table->addCell(1750)->addText('');
                }
                if(Auth()->user()->hasPermissionTo('emailReport_title_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Title');
                }else{
                    $table->addCell(1750)->addText('');
                }
                if(Auth()->user()->hasPermissionTo('emailReport_message_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Message');
                }else{
                    $table->addCell(1750)->addText('');
                }
                if(Auth()->user()->hasPermissionTo('emailReport_senddate_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Send Date');
                }else{
                    $table->addCell(1750)->addText('');
                }
            }
            $table->addRow();
            if(Auth()->user()->hasPermissionTo('emailReport_ticketid_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText(strip_tags($value->id));
            }else{
                $table->addCell(1750)->addText('');
            }
            if(Auth()->user()->hasPermissionTo('emailReport_username_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText(strip_tags($value->user_id));
            }else{
                $table->addCell(1750)->addText('');
            }
            if(Auth()->user()->hasPermissionTo('emailReport_title_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText(strip_tags($value->title));
            }else{
                $table->addCell(1750)->addText('');
            }
            if(Auth()->user()->hasPermissionTo('emailReport_message_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText(strip_tags($value->message));
            }else{
                $table->addCell(1750)->addText('');
            }
            if(Auth()->user()->hasPermissionTo('emailReport_senddate_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText(strip_tags($value->created_at));
            }else{
                $table->addCell(1750)->addText('');
            }

        }
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('email.docx');
        return response()->download(public_path('email.docx'));
    }

    public function exportExcel(Request $request)
    {

        if($request->excel_array == 1)
        {
            $data=Support::where('company_id',Auth::user()->default_company)->with('user')->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->excel_array);
            $data=Support::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->with('user')->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        return Excel::download(new EmailExport($data), 'email.csv');
    }
}
