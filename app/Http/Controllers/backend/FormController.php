<?php
namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Company;
use App\Location;
use App\Exports\form\FormExport;
use App\Exports\form\FormSoftExport;
use App\Forms;
use App\FormGroups;
use PDF;
use Maatwebsite\Excel\Facades\Excel;
use Validator;
use DataTables;
use Session;
use App\PrePopulate;
use App\PopulateResponse;
use Auth;
use Config;
class FormController extends Controller
{
    private $page_name;
    private $tab_name;
    public function __construct() {
        $this->page_name ='document_library';
        $this->tab_name ='main_detail';
    }
    public function index()
    {
        Session::put('form_id', null);
        Session::put('folder_id', null);
        $companies=Company::all();
        return view('backend.document.index')->with(['companies'=>$companies]);
    }
    public function document_cms()
    {
        $cms_data=localization();
        $temp_array['name']['key']='name';
        $temp_array['name']['value']=checkKey('name',$cms_data);
        $temp_array['ref']['key']='ref';
        $temp_array['ref']['value']=checkKey('ref',$cms_data);
        $temp_array['location_type']['key']='location_type';
        $temp_array['location_type']['value']=checkKey('location_type',$cms_data);
        $temp_array['form_type']['key']='form_type';
        $temp_array['form_type']['value']=checkKey('form_type',$cms_data);
        $temp_array['pre_populate']['key']='pre_populate';
        $temp_array['pre_populate']['value']=checkKey('pre_populate',$cms_data);
        $temp_array['import_jobs']['key']='import_jobs';
        $temp_array['import_jobs']['value']=checkKey('import_jobs',$cms_data);
        $temp_array['req_delete']['key']='reason_for_delete';
        $temp_array['req_delete']['value']=checkKey('reason_for_delete',$cms_data);
        $temp_array['req_sign_off']['key']='require_sign_off';
        $temp_array['req_sign_off']['value']=checkKey('require_sign_off',$cms_data);
        $temp_array['app_form']['key']='show_form_onapp';
        $temp_array['app_form']['value']=checkKey('show_form_onapp',$cms_data);
        $temp_array['req_tag']['key']='is_tag_job';
        $temp_array['req_tag']['value']=checkKey('is_tag_job',$cms_data);
        $temp_array['view']['key']='view';
        $temp_array['view']['value']=checkKey('view',$cms_data);
        $temp_array['files']['key']='files';
        $temp_array['files']['value']=checkKey('files',$cms_data);
        $temp_array['lock_from']['key']='lock_job_from';
        $temp_array['lock_from']['value']=checkKey('lock_job_from',$cms_data);
        $temp_array['lock_job_after']['key']='lock_job_after';
        $temp_array['lock_job_after']['value']=checkKey('lock_job_after',$cms_data);
        $temp_array['lock_to']['key']='lock_job_before';
        $temp_array['lock_to']['value']=checkKey('lock_job_before',$cms_data);
        $temp_array['lock_job_before']['key']='lock_job_before';
        $temp_array['lock_job_before']['value']=checkKey('lock_job_before',$cms_data);
        $temp_array['valid_from']['key']='valid_from';
        $temp_array['valid_from']['value']=checkKey('valid_from',$cms_data);
        $temp_array['valid_to']['key']='valid_to';
        $temp_array['valid_to']['value']=checkKey('valid_to',$cms_data);
        $temp_array['notes']['key']='notes';
        $temp_array['notes']['value']=checkKey('notes',$cms_data);
        return $temp_array;

    }
    public function getAll()
    {
        $form_id = Session::get('folder_id');
        if($form_id != null){
            $data=Forms::where('company_id',Auth::user()->default_company)->where('active',1)->where('form_group_id', '=', $form_id)->with('form','form_section','version','jobGroups')->orderBy('id', 'desc')->where('deleted',0)->get();
            $data=sortData($data);
        }
        else {
            $data=Forms::where('company_id',Auth::user()->default_company)->where('active',1)->where('form_group_id', '=', 0)->with('form','form_section','version','jobGroups')->orderBy('id', 'desc')->where('deleted',0)->get();
            $data=sortData($data);
        }


        return Datatables::of($data)
            ->addColumn('checkbox', function($data){})
            ->addColumn('actions', function($data){
                $response="";
                if(Auth()->user()->hasPermissionTo('edit_documentlibrary') || Auth::user()->all_companies == 1 ){
                    $edit ='<a href="/document/detail/'.$data->id.'" class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i></a>';
                }else{
                    $edit ='';
                }

                if(!$data->version->isEmpty()){
                    if(Auth()->user()->hasPermissionTo('versionlog_documentlibrary') || Auth::user()->all_companies == 1 ){
                        $view='<a href="/version/log/viewLatest/'.$data->id.'"  class="btn btn-success btn-sm"><i class="fas fa-eye"></i></a>';
                    }else{
                        $view='';
                    }
                    if(Auth()->user()->hasPermissionTo('selectedForm_documentlibrary') || Auth::user()->all_companies == 1 ){
                        $popup='<a  data-toggle="modal" data-target="#modelviewpopup" class="btn btn-default btn-sm" onclick="selectedForm('.$data->id.')" ><i class="fas fa-angle-double-up"></i></a><span class="counter counter-floating counter-sm">'.$data->jobGroups->where('company_id',Auth::user()->default_company)->where('active', '=',1)->where('deleted', '=',0)->count().'</span>';
                    }else{
                        $popup='';
                    }
                    if(Auth()->user()->hasPermissionTo('viewregister_documentlibrary') || Auth::user()->all_companies == 1 ){
                        $register='<a href="/view_register" class="btn btn-warning  btn-sm"><i class="fas fa-copy"></i></a>';
                    }else{
                        $register='';
                    }
                }else{
                    if(Auth()->user()->hasPermissionTo('versionlog_documentlibrary') || Auth::user()->all_companies == 1 ){
                        $view='<a href="javascript:;"  class="btn btn-sm" style="background-color: #b5b5b5 !important;"><i class="fas fa-eye"></i></a>';
                    }else{
                        $view='';
                    }
                    if(Auth()->user()->hasPermissionTo('selectedForm_documentlibrary') || Auth::user()->all_companies == 1 ){
                        $popup='<a href="javascript:;" style="background-color: #b5b5b5 !important;" class="btn btn-sm"><i class="fas fa-angle-double-up"></i></a>';
                    }else{
                        $popup='';
                    }
                    if(Auth()->user()->hasPermissionTo('viewregister_documentlibrary') || Auth::user()->all_companies == 1 ){
                        $register='<a href="javascript:;" style="background-color: #b5b5b5 !important;" class="btn btn-sm"><i class="fas fa-copy"></i></a>';
                    }else{
                        $register='';
                    }
                }

                if(Auth()->user()->hasPermissionTo('delete_documentlibrary') || Auth::user()->all_companies == 1 ){
                    $delete ='<a type="button"class="btn btn-danger btn-sm my-0" onclick="deleteDocumentdata('.$data->id.')"><i class="fas fa-trash"></i></a>';
                }else{
                    $delete ='';
                }
                $response=$response.$edit;
                $response=$response.$view;
                $response=$response.$popup;
                $response=$response.$register;
                $response=$response.$delete;
                return $response;
            })->rawColumns(['actions'])->make(true);
    }

    public function get($id)
    {
        $data=Forms::where('company_id',Auth::user()->default_company)->where('form_group_id', '=', $id)->where('active',1)->where('deleted',0)->get();
        $data=sortData($data);

        return Datatables::of($data)
            ->addColumn('checkbox', function($data){})
            ->addColumn('actions', function($data){
                $response="";
                if(Auth()->user()->hasPermissionTo('edit_documentlibrary') || Auth::user()->all_companies == 1 ){
                    $edit ='<a href="/document/detail/'.$data->id.'" class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i></a>';
                }else{
                    $edit ='';
                }

                if(count($data->form_section)>0){
                    if(Auth()->user()->hasPermissionTo('versionlog_documentlibrary') || Auth::user()->all_companies == 1 ){
                        $view='<a href="/version/log/viewLatest/'.$data->id.'"  class="btn btn-success btn-sm"><i class="fas fa-eye"></i></a>';
                    }else{
                        $view='';
                    }
                    if(Auth()->user()->hasPermissionTo('selectedForm_documentlibrary') || Auth::user()->all_companies == 1 ){
                        $popup='<a  data-toggle="modal" data-target="#modelviewpopup" class="btn btn-default btn-sm" onclick="selectedForm('.$data->id.')" ><i class="fas fa-angle-double-up"></i></a>';
                    }else{
                        $popup='';
                    }
                    if(Auth()->user()->hasPermissionTo('viewregister_documentlibrary') || Auth::user()->all_companies == 1 ){
                        $register='<a href="/view_register" class="btn btn-warning  btn-sm"><i class="fas fa-copy"></i></a>';
                    }else{
                        $register='';
                    }
                }else{
                    if(Auth()->user()->hasPermissionTo('versionlog_documentlibrary') || Auth::user()->all_companies == 1 ){
                        $view='<a href="javascript:;"  class="btn btn-sm" style="background-color: #b5b5b5 !important;"><i class="fas fa-eye"></i></a>';
                    }else{
                        $view='';
                    }
                    if(Auth()->user()->hasPermissionTo('selectedForm_documentlibrary') || Auth::user()->all_companies == 1 ){
                        $popup='<a href="javascript:;" style="background-color: #b5b5b5 !important;" class="btn btn-sm"><i class="fas fa-angle-double-up"></i></a>';
                    }else{
                        $popup='';
                    }
                    if(Auth()->user()->hasPermissionTo('viewregister_documentlibrary') || Auth::user()->all_companies == 1 ){
                        $register='<a href="javascript:;" style="background-color: #b5b5b5 !important;" class="btn btn-sm"><i class="fas fa-copy"></i></a>';
                    }else{
                        $register='';
                    }
                }

                if(Auth()->user()->hasPermissionTo('delete_documentlibrary') || Auth::user()->all_companies == 1 ){
                    $delete ='<a type="button"class="btn btn-danger btn-sm my-0" onclick="deleteDocumentdata('.$data->id.')"><i class="fas fa-trash"></i></a>';
                }else{
                    $delete ='';
                }
                $response=$response.$edit;
                $response=$response.$view;
                $response=$response.$popup;
                $response=$response.$register;
                $response=$response.$delete;
                return $response;
            })->rawColumns(['actions'])->make(true);

    }

    public function getAllSoft()
    {
//        $data=Forms::where('active',0)->where('deleted',0)->orderBy('id', 'desc')->get();
//        $data=sortData($data);
//        $data=$data->toArray();
//        $data = array_values($data);
//        $locations=Location::where('active',1)->where('deleted',0)->get();
//        return response()->json(['data'=>$data,'flag'=>0,'locations'=>$locations]);

        $form_id = Session::get('folder_id');
        if($form_id != null){
            $data=Forms::where('company_id',Auth::user()->default_company)->where('active',0)->where('form_group_id', '=', $form_id)->with('form','form_section')->orderBy('id', 'desc')->where('deleted',0)->get();
            $data=sortData($data);
        }
        else {
            $data=Forms::where('company_id',Auth::user()->default_company)->where('active',0)->where('form_group_id', '=', 0)->with('form','form_section')->orderBy('id', 'desc')->where('deleted',0)->get();
            $data=sortData($data);
        }
        return Datatables::of($data)
            ->addColumn('checkbox', function($data){})
            ->addColumn('actions', function($data){
                $response="";
                if(Auth()->user()->hasPermissionTo('active_documentlibrary') || Auth::user()->all_companies == 1 ){
                    $resotre ='<a href="javascript:;" class="btn btn-success btn-sm" onclick="restoreformdata('.$data->id.')"><i class="fa fa-retweet"></i></a>';
                }else{
                    $resotre ='';
                }

                if(count($data->form_section)>0){
                    if(Auth()->user()->hasPermissionTo('versionlog_documentlibrary') || Auth::user()->all_companies == 1 ){
                        $view='<a href="/version/log/viewLatest/'.$data->id.'"  class="btn btn-success btn-sm"><i class="fas fa-eye"></i></a>';
                    }else{
                        $view='';
                    }
                    if(Auth()->user()->hasPermissionTo('selectedForm_documentlibrary') || Auth::user()->all_companies == 1 ){
                        $popup='<a  data-toggle="modal" data-target="#modelviewpopup" class="btn btn-default btn-sm" onclick="selectedForm('.$data->id.')" ><i class="fas fa-angle-double-up"></i></a>';
                    }else{
                        $popup='';
                    }
                    if(Auth()->user()->hasPermissionTo('viewregister_documentlibrary') || Auth::user()->all_companies == 1 ){
                        $register='<a href="/view_register" class="btn btn-warning  btn-sm"><i class="fas fa-copy"></i></a>';
                    }else{
                        $register='';
                    }
                }else{
                    if(Auth()->user()->hasPermissionTo('versionlog_documentlibrary') || Auth::user()->all_companies == 1 ){
                        $view='<a href="javascript:;"  class="btn btn-sm" style="background-color: #b5b5b5 !important;"><i class="fas fa-eye"></i></a>';
                    }else{
                        $view='';
                    }
                    if(Auth()->user()->hasPermissionTo('selectedForm_documentlibrary') || Auth::user()->all_companies == 1 ){
                        $popup='<a href="javascript:;" style="background-color: #b5b5b5 !important;" class="btn btn-sm"><i class="fas fa-angle-double-up"></i></a>';
                    }else{
                        $popup='';
                    }
                    if(Auth()->user()->hasPermissionTo('viewregister_documentlibrary') || Auth::user()->all_companies == 1 ){
                        $register='<a href="javascript:;" style="background-color: #b5b5b5 !important;" class="btn btn-sm"><i class="fas fa-copy"></i></a>';
                    }else{
                        $register='';
                    }
                }

                if(Auth()->user()->hasPermissionTo('hard_delete_documentlibrary') || Auth::user()->all_companies == 1 ){
                    $delete ='<a type="button"class="btn btn-danger btn-sm my-0" onclick="deletesoftformdata('.$data->id.')"><i class="fas fa-trash"></i></a>';
                }else{
                    $delete ='';
                }

                $response=$response.$resotre;
                $response=$response.$view;
                $response=$response.$popup;
                $response=$response.$register;
                $response=$response.$delete;
                return $response;
            })->rawColumns(['actions'])->make(true);
    }
    public function store()
    {
        $form_id = Session::get('form_id');
        if($form_id != null){
            $data=Forms::create([
                'form_group_id' => $form_id
            ]);
            return response()->json(['data'=>$data]);
        }
        else {
            $data=Forms::create([
                'form_group_id' => 0
            ]);
            return response()->json(['data'=>$data]);
        }

    }

    public function update(Request $request)
    {
        $data = [
            'company_id'=>'required|numeric',
        ];
        $validator = Validator::make($request->all(), $data);
        if ($validator->fails())
        {
            $error = $validator->messages();
            $html='<ul>';
            foreach ($error->all() as $key => $value) {
                $html=$html.'<li>'.$value.'</li>';
            }
            $html=$html.'</ul>';
            return response()->json(['status'=> false, 'error'=> $html]);
        }
        $data=$request->all();
        $scoring=Forms::where('id',$data['id']);
        $bonus=$scoring->first();
        $scoring->update($data);
        if($bonus->active == 0)
        {
            $flag=0;
        }
        else
        {
            $flag=1;
        }
        return response()->json(['message'=>'Successfully Added !!','flag'=>$flag]);

    }
    public function softDelete (Request $request)
    {
        Forms::where('id',$request->id)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function delete(Request $request)
    {
        Forms::where('id',$request->id)->update(['active'=>0,'deleted'=>0]);
        audit_log($this->document_cms(),'',Config::get('constants.delete'),$this->page_name,$this->tab_name,'','',$request->id,'','forms');
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function active(Request $request)
    {
        Forms::where('id',$request->id)->update(['active'=>1,'deleted'=>0]);
        audit_log($this->document_cms(),'',Config::get('constants.restore'),$this->page_name,$this->tab_name,'','',$request->id,'','forms');
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
    public function activeMultiple(Request $request)
    {
        Forms::whereIn('id',$request->ids)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
    public function deleteMultipleSOft(Request $request)
    {
        Forms::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function deleteMultiple(Request $request)
    {
        Forms::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function exportPdf(Request $request)
    {
        $form_id = Session::get('folder_id');
        if($request->pdf_array == 1)
        {
            if ($request->folder==2){
                $data= Forms::where('company_id',Auth::user()->default_company)->where('active', '=',1)->where('form_group_id', '=', $form_id)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
            }else{
                $data= Forms::where('company_id',Auth::user()->default_company)->where('active', '=',1)->where('form_group_id', '=', 0)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
            }
        }
        else
        {
            if ($request->folder==2){
                $array=json_decode($request->pdf_array);
                $data=Forms::whereIn('id',$array)->where('form_group_id', '=', $form_id)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
            }else{
                $array=json_decode($request->pdf_array);
                $data=Forms::whereIn('id',$array)->where('form_group_id', '=', 0)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
            }
            $data=sortData($data);
        }
        $companies='Patroltec';
        $reportname='Document Library';
        $date=\Carbon\Carbon::now()->format('D jS M Y');
        $pdf = PDF::loadView('backend.document.pdf',compact('companies','reportname','data','date'));
        return $pdf->download('Form.pdf');
    }

    public function exportWorld(Request $request)
    {
        $form_id = Session::get('folder_id');
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        if($request->word_array == 1)
        {
            if ($request->folder==2){
                $data= Forms::where('company_id',Auth::user()->default_company)->where('active', '=',1)->where('form_group_id', '=', $form_id)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
            }else{
                $data= Forms::where('company_id',Auth::user()->default_company)->where('active', '=',1)->where('form_group_id', '=', 0)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
            }
        }
        else
        {
            if ($request->folder==2){
                $array=json_decode($request->word_array);
                $data=Forms::whereIn('id',$array)->where('form_group_id', '=', $form_id)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
            }else{
                $array=json_decode($request->word_array);
                $data=Forms::whereIn('id',$array)->where('form_group_id', '=', 0)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
            }
            $data=sortData($data);
        }


        $text = $section->addText('Form data');
        $table=$section->addTable();
        foreach ($data as $key => $value) {
            if($key==0)
            {
                $table->addRow();
                if(Auth()->user()->hasPermissionTo('documentlibrary_name_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Name');
                }else{
                    $table->addCell(1750)->addText('');
                }
                if(Auth()->user()->hasPermissionTo('documentlibrary_ref_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Reference');
                }else{
                    $table->addCell(1750)->addText('');
                }
            }
            $table->addRow();
            if(Auth()->user()->hasPermissionTo('documentlibrary_name_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText($value->name);
            }else{
                $table->addCell(1750)->addText('');
            }
            if(Auth()->user()->hasPermissionTo('documentlibrary_ref_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText($value->ref);
            }else{
                $table->addCell(1750)->addText('');
            }
        }
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('forms.docx');
        return response()->download(public_path('forms.docx'));
    }
    public function exportExcel(Request $request)
    {
        $form_id = Session::get('folder_id');
        if($request->excel_array == 1)
        {
            if ($request->folder==2){
                $data= Forms::where('company_id',Auth::user()->default_company)->where('active', '=',1)->where('form_group_id', '=', $form_id)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
            }else{
                $data= Forms::where('company_id',Auth::user()->default_company)->where('active', '=',1)->where('form_group_id', '=', 0)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
            }
        }
        else
        {
            if ($request->folder==2){
                $array=json_decode($request->excel_array);
                $data=Forms::whereIn('id',$array)->where('form_group_id', '=', $form_id)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
            }else{
                $array=json_decode($request->excel_array);
                $data=Forms::whereIn('id',$array)->where('form_group_id', '=', 0)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
            }
            $data=sortData($data);
        }
        return Excel::download(new FormExport($data), 'forms.csv');
    }

    public function folderInside($id)
    {

        Session::put('form_id', $id);
        $companies=Company::all();
        $data = Forms::where('company_id',Auth::user()->default_company)->where('id',$id)->pluck('name');
        return view('backend.document.folder_inside')->with(['id'=>$id, 'folder_name' => $data[0], 'companies'=>$companies]);
    }
    public function addPopulate(Request $request)
    {
        $input = $request->all();
        PrePopulate::create(['name'=>$input['name'],'form_id'=>Session::get('form_id')]);
        $pre_populate = PrePopulate::where('form_id',Session::get('form_id'))->orderBy('id', 'desc')->where('active', 1)->pluck('name','id')->toArray();
        return response()->json(['message'=>'Successfully added','pre_populate'=>$pre_populate]);
    }
    public function populate(Request $request)
    {
        $input = $request->all();
        unset($input['_token']);
        unset($input['formData']['id']);
        unset($input['formData']['section_question_id']);
        PopulateResponse::where('pre_populate_id',$input['formData']['pre_populate_id'])->update(['active'=>0,'deleted'=>1]);
        foreach ($input['formData'] as $key => $value) {
            $response = new PopulateResponse();
            $response->pre_populate_id = $input['formData']['pre_populate_id'];
            if($key!="pre_populate_id")
            {
                $ids = explode('.',$key);
                if($ids[0]=="dropdown")
                {
                    $response->answer_id=$value;
                    $response->form_section_id=$ids[1];
                    $response->question_id=$ids[2];
                    $response->save();
                }else if($ids[0]=="checkbox")
                {
                    if(is_array($value))
                    {
                        foreach ($value as  $next)
                        {
                            $response = new PopulateResponse();
                            $response->pre_populate_id = $input['formData']['pre_populate_id'];
                            $response->answer_id=$next;
                            $response->form_section_id=$ids[1];
                            $response->question_id=$ids[2];
                            $response->save();
                        }
                    }

                    else
                    {
                        $response->answer_id=$value;
                        $response->form_section_id=$ids[1];
                        $response->question_id=$ids[2];
                        $response->save();
                    }
                }

                else if($ids[0]=="multi")
                {
                    if(is_array($value))
                    {
                        foreach ($value as  $next)
                        {
                            $response = new PopulateResponse();
                            $response->pre_populate_id = $input['formData']['pre_populate_id'];
                            $response->answer_id=$next;
                            $response->form_section_id=$ids[1];
                            $response->question_id=$ids[2];
                            $response->save();
                        }
                    }
                    else
                    {
                        $response->answer_id=$value;
                        $response->form_section_id=$ids[1];
                        $response->question_id=$ids[2];
                        $response->save();
                    }

                }else if($ids[0]=="answer_id")
                {
                    $response->answer_id=$value;
                    $response->form_section_id=$ids[1];
                    $response->question_id=$ids[2];
                    $response->save();
                }else
                {
                    $response->answer_text=$value;
                    $response->form_section_id=$ids[0];
                    $response->question_id=$ids[1];
                    $response->save();
                }
            }
        }
        return response()->json(['message'=>'Successfully Populated Data']);
    }
}
