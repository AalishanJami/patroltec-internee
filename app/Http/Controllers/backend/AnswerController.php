<?php
namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Answers;
use App\AnswerTypes;
use PDF;
use Auth;
use Maatwebsite\Excel\Facades\Excel;
use Validator;
use DataTables;
use Config;
use Session;
use App\Exports\answer\AnswerExport;

class AnswerController extends Controller
{
    private $page_name;
    private $tab_name;
    private $db;
    private $cms_array=[];
    public function __construct() {
        $this->page_name ='answer';
        $this->tab_name ='document_library';
        $this->db='answers';
        $cms_data=localization();
        $temp_array=[];
        $temp_array['answer']['key']='name';
        $temp_array['answer']['value']=checkKey('name',$cms_data);
        $temp_array['grade']['key']='grade';
        $temp_array['grade']['value']=checkKey('grade',$cms_data);
        $temp_array['score']['key']='score';
        $temp_array['score']['value']=checkKey('score',$cms_data);
        $temp_array['answer_group_id']['key']='answer_group';
        $temp_array['answer_group_id']['value']=checkKey('answer_group',$cms_data);
        $this->cms_array=$temp_array;
    }
    public function getAll($id)
    {
        $data=Answers::where('answer_group_id', $id)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        return Datatables::of($data)
        ->addColumn('checkbox', function ($data) {
        })
        ->addColumn('image', function ($data) {})
        ->addColumn('upload', function ($data) {})
        ->addColumn('actions', function ($data) {
            $response = "";
            $html=' <div style=" display: flex;flex-direction: row; flex-wrap: nowrap; ">';
            $html= $html.'<span class="save showAnswer'. $data->id .'" style="margin-top:6px; margin-right:6px;" id="'. $data->id .'" hidden><a type="button"';
            $html= $html. 'class="btn btn-success btn-rounded btn-sm my-0"><i class="fa fa-check"></i></a>';
            $html= $html. '</span>';
            if(Auth()->user()->hasPermissionTo('delete_document_answer') || Auth::user()->all_companies == 1 )
            {
                $html= $html. '<span class="table-remove_answer" style="margin-top:6px" id="'. $data->id .'"><a type="button"';
                $html= $html. 'class="btn remove_answer delete_answer'.$data->id.' btn-danger btn-rounded btn-sm my-0"><i class="fas fa-trash"></i></a>';
                $html= $html. '</span>';
            }
            $html= $html. '</div>';
            $response = $html;
            return $response;
        })
        ->rawColumns(['actions'])
        ->make(true);
    }

    public function get($id)
    {
        $data=Answers::where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        return response()->json(['data'=>$data,'flag'=>1]);

    }
    public function getAllSoft($id)
    {
        $data=Answers::where('answer_group_id', $id)->where('active',0)->where('deleted',0)->orderBy('id', 'desc')->get();
        return Datatables::of($data)
        ->addColumn('checkbox', function ($data) {
        })
        ->addColumn('image', function ($data) {
        })
        ->addColumn('actions', function ($data) {
            $response = "";
            $restore = '<span><a type="button" class="btn btn-success btn-xs my-0 waves-effect waves-light" onclick="restoreanswerdata('.$data->id.')"><i class="fa fa-retweet" aria-hidden="true"></i></a> </span>';
                $response = $response . $restore;
            $delete= '<span><a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light" onclick="deletesoftanswerdata('.$data->id.')"><i class="fas fa-trash"></i></a></span>';
                $response = $response . $delete;
            return $response;
        })
        ->rawColumns(['actions'])
        ->make(true);
    }
    public function store(Request $request)
    {
        $data=Answers::create([
            'company_id' => $request->company_id,'answer' => ' ']);
        $answer_types=AnswerTypes::where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
    audit_log($this->cms_array,'',Config::get('constants.store'),$this->page_name,$this->tab_name,$data->id,'',$data->id,Session::get('form_id'),$this->db);
        return response()->json(['data'=>$data,'answer_types'=>$answer_types]);

    }

    public function uploadIcon(Request $request)
    {
        $input = $request->all();
        $time = time();
        $file = $request->file('file');
        if(isset($file))
        {
            $input['file'] = $time.'.'.$file->getClientOriginalExtension();
            $destinationPath = public_path('uploads/icons');
            $file->move($destinationPath, $input['file']);
             // $path = $request->file('file')->store('uploads/icons');
            $answer = Answers::find($input['answer_id']);
            $answer->icon =  $input['file'];
            $answer->update();
            $get_answer_group=Answers::where('id',$input['answer_id'])->first();
            return response()->json(['message'=>'Successfully Added !!','data'=>$get_answer_group]);
        }
        else
        {
            return response()->json(['flag'=>1]);
        }
    }

    public function update(Request $request)
    {
        $data = [
            'company_id'=>'required|numeric',
            'answer'=>'required',
            'grade'=>'numeric',
            'score'=>'required|numeric',
            ];
        $validator = Validator::make($request->all(), $data);
        if ($validator->fails())
        {
            $error = $validator->messages();
            $html='<ul>';
            foreach ($error->all() as $key => $value) {
                if ($value=='The answer field is required.'){
                    $value='The name field is required.';
                }
                $html=$html.'<li>'.$value.'</li>';
            }
            $html=$html.'</ul>';
            return response()->json(['status'=> false, 'error'=> $html]);
        }

        $data=$request->all();
        $answer_group=Answers::where('id',$data['id']);
        $answer_group_first=$answer_group->first();
        if(isset($data['answer']) && $data['answer']==null)
        {
            $data['answer']=' ';
        }
        $answer_group->update($data);
        if($answer_group_first->active == 0)
        {
            $flag=0;
        }
        else
        {
            $flag=1;
        }
        $get_answer_group=Answers::where('id',$data['id'])->first();
        audit_log($this->cms_array,audit_log_data($data),Config::get('constants.update'),$this->page_name,$this->tab_name,$answer_group_first->id,audit_log_data($answer_group_first->toArray()),$answer_group_first->id,Session::get('form_id'),$this->db);
        return response()->json(['message'=>'Successfully Added !!','flag'=>$flag,'data'=>$get_answer_group]);

    }
    public function softDelete (Request $request)
    {
        Answers::where('id',$request->id)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function delete(Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.delete'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('form_id'),$this->db);
        Answers::where('id',$request->id)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function active(Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.restore'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('form_id'),$this->db);
        Answers::where('id',$request->id)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
    public function activeMultiple(Request $request)
    {
        Answers::whereIn('id',$request->ids)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
     public function deleteMultipleSOft(Request $request)
    {
        Answers::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function deleteMultiple(Request $request)
    {
        Answers::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function exportExcel(Request $request)
    {
        if($request->excel_array == 1)
        {
            $data= Answers::where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->excel_array);
            $data=Answers::whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        return Excel::download(new AnswerExport($data), 'answer.csv');
    }
    public function exportPdf(Request $request)
    {
        if($request->pdf_array == 1)
        {
            $data= Answers::where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->pdf_array);
            $data=Answers::whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        $companies='Patroltec';
        $reportname='Answer';
        $date=\Carbon\Carbon::now()->format('D jS M Y');
        $pdf = PDF::loadView('backend.document.answer.pdf',compact('companies','reportname','data','date'));
        return $pdf->download('answer.pdf');
    }
    public function exportWorld(Request $request)
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        if($request->word_array == 1)
        {
            $data= Answers::where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->word_array);
            $data=Answers::whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        $table = $section->addTable();
        foreach ($data as $key => $value) {
            if($key==0)
            {
                $table->addRow();
                if(Auth()->user()->hasPermissionTo('document_answer_image_word_export') || Auth::user()->all_companies == 1 )
                {
                    $table->addCell(1750)->addText('Image');
                }
                if(Auth()->user()->hasPermissionTo('document_answer_name_word_export') || Auth::user()->all_companies == 1 )
                {
                    $table->addCell(1750)->addText('Name');
                }
                if(Auth()->user()->hasPermissionTo('document_answer_grade_word_export') || Auth::user()->all_companies == 1 )
                {
                    $table->addCell(1750)->addText('Grade');
                }
                if(Auth()->user()->hasPermissionTo('document_answer_score_word_export') || Auth::user()->all_companies == 1 )
                {
                    $table->addCell(1750)->addText('Score');
                }

            }
            $table->addRow();
            if(Auth()->user()->hasPermissionTo('document_answer_image_word_export') || Auth::user()->all_companies == 1 )
            {

                $table->addCell(1750)->addImage(public_path('uploads/icons/'.$value->icon),array('width'=>50, 'height'=>50, 'align'=>'left'));
            }
            if(Auth()->user()->hasPermissionTo('document_answer_name_word_export') || Auth::user()->all_companies == 1 )
            {
                $table->addCell(1750)->addText($value->answer);
            }
            if(Auth()->user()->hasPermissionTo('document_answer_grade_word_export') || Auth::user()->all_companies == 1 )
            {
                $table->addCell(1750)->addText($value->grade);
            }
            if(Auth()->user()->hasPermissionTo('document_answer_score_word_export') || Auth::user()->all_companies == 1 )
            {
                $table->addCell(1750)->addText($value->score);
            }

        }
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('answer.docx');
        return response()->download(public_path('answer.docx'));
    }


}
