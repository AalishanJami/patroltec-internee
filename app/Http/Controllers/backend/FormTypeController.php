<?php
namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Company;
use App\FormType;
use Auth;
use App\Exports\form_type\FormTypeExport;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use DataTables;
use PDF;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Hash;
use Validator;
use Session;
use Config;

class FormTypeController extends Controller
{
    private $page_name;
    private $tab_name;
    private $db;
    private $cms_array=[];
    public function __construct() {
        $this->page_name ='form_type';
        $this->tab_name ='document_library';
        $this->db='form_types';
        $cms_data=localization();
        $temp_array=[];
        $temp_array['name']['key']='name';
        $temp_array['name']['value']=checkKey('name',$cms_data);
        $temp_array['action_score']['key']='action_score';
        $temp_array['action_score']['value']=checkKey('action_score',$cms_data);
        $temp_array['_show_dashboard']['key']='show_dashboard';
        $temp_array['_show_dashboard']['value']=checkKey('show_dashboard',$cms_data);
        $temp_array['count_day']['key']='count_day';
        $temp_array['count_day']['value']=checkKey('count_day',$cms_data);
        $temp_array['count_dashboard']['key']='count_dashboard';
        $temp_array['count_dashboard']['value']=checkKey('count_dashboard',$cms_data);
        $temp_array['count_response_positive']['key']='count_response_positive';
        $temp_array['count_response_positive']['value']=checkKey('count_response_positive',$cms_data);
        $temp_array['count_response_negative']['key']='count_response_negative';
        $temp_array['count_response_negative']['value']=checkKey('count_response_negative',$cms_data);
        $temp_array['outanding_count']['key']='outstanding_count';
        $temp_array['outanding_count']['value']=checkKey('outstanding_count',$cms_data);
        $temp_array['late_completion_count']['key']='late_completion_count';
        $temp_array['late_completion_count']['value']=checkKey('late_completion_count',$cms_data);
        $temp_array['detailed_field_stats']['key']='detailed_field_stats';
        $temp_array['detailed_field_stats']['value']=checkKey('detailed_field_stats',$cms_data);
        $this->cms_array=$temp_array;
    }
    public function getAll()
    {
        $data=FormType::where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {})
            ->addColumn('action_score', function ($data) { return $data->action_score;})
            ->addColumn('submit', function ($data) {})
            ->addColumn('actions', function ($data) {
                $response = "";
                if(Auth()->user()->hasPermissionTo('delete_form_type') || Auth::user()->all_companies == 1 )
                {
                    $delete = '<a href="javascript:;" class="btn btn-danger form_type_btn btn-xs all_action_btn_margin" onclick="deleteform_typedata( ' . $data->id . ')"><i class="fas fa-trash"></i></a>';
                    $edit ='<a href="javascript:;" id="' . $data->id . '" class="btn btn-xs form_type_btn btn-primary " onclick="formTypeEdit('.$data->id.')"><i class="fas fa-pencil-alt"></i></a>';
                    $response = $response . $edit;
                    $response = $response . $delete;
                }
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    public function refresh()
    {
        $data=FormType::where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        return response()->json(['data'=>$data]);
    }
    public function getAllSoft()
    {
        $data=FormType::where('active',0)->where('deleted',0)->orderBy('id', 'desc')->get();
        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {
            })
            ->addColumn('submit', function ($data) {
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                if(Auth()->user()->hasPermissionTo('restore_delete_form_type') || Auth::user()->all_companies == 1 )
                {
                    $restore = '<a href="javascript:;" class="btn btn-success form_type_btn my-0 waves-effect waves-light" onclick="restoreform_typedata('.$data->id.')"><i class="fa fa-retweet" aria-hidden="true"></i></a>';
                    $response = $response . $restore;
                }
                if(Auth()->user()->hasPermissionTo('hard_delete_form_type') || Auth::user()->all_companies == 1 )
                {
                    $delete = '<a href="javascript:;" class="btn btn-danger form_type_btn all_action_btn_margin  my-0 waves-effect waves-light" onclick="deletesoftform_typedata('.$data->id.')"><i class="fas fa-trash"></i></a>';
                    $response = $delete. $response;
                }
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
    public function store(Request $request)
    {
        // array_key_exists("Toyota",$a)
        $form_id=Session::get('form_id');
        $input = $request->all();
        if(array_key_exists('_show_dashboard',$input) && $input['_show_dashboard'] == null )
        {
            $input['_show_dashboard']=1;
        }
        else
        {
            $input['_show_dashboard']=0;   
        }
        if(array_key_exists('count_dashboard',$input) && $input['count_dashboard'] == null )
        {
            $input['count_dashboard']=1;
        }
        else
        {
            $input['count_dashboard']=0;
        }
        if(array_key_exists('count_day',$input) && $input['count_day'] == null )
        {
            $input['count_day']=1;
        }
        else
        {
            $input['count_day']=0;
        }
        if(array_key_exists('count_response_positive',$input) && $input['count_response_positive'] == null )
        {
            $input['count_response_positive']=1;
        }
        else
        {
            $input['count_response_positive']=0;
        }
        if(array_key_exists('count_response_negative',$input) && $input['count_response_negative'] == null )
        {
            $input['count_response_negative']=1;
        }
        else
        {
            $input['count_response_negative']=0;
        }
        if(array_key_exists('outanding_count',$input) && $input['outanding_count'] == null )
        {
            $input['outanding_count']=1;
        }
        else
        {
            $input['outanding_count']=0;
        }
        if(array_key_exists('late_completion_count',$input) && $input['late_completion_count'] == null )
        {
            $input['late_completion_count']=1;
        }
        else
        {
            $input['late_completion_count']=0;
        }
        if(array_key_exists('detailed_field_stats',$input) && $input['detailed_field_stats'] == null )
        {
            $input['detailed_field_stats']=1; 
        }
        else
        {
            $input['detailed_field_stats']=0;
        }
        if(isset($input['form_type_id']))
        {
            if($input['form_type_id']!=null)
            {
                $data = FormType::find($input['form_type_id']);
                audit_log($this->cms_array,audit_log_data($input),Config::get('constants.update'),$this->page_name,$this->tab_name,$data->id,audit_log_data($data->toArray()),$data->id,$form_id,$this->db);
                $data->update($input);
                return response()->json(['message'=>'Successfully Updated !!']);
            }
            else
            {
                $data=FormType::create($input);
                audit_log($this->cms_array,audit_log_data($input),Config::get('constants.store'),$this->page_name,$this->tab_name,$data->id,$data->id,$form_id,$this->db);
                return response()->json(['message'=>'Successfully Saved !!']);
            }
        }else
        {
            $data=FormType::create($input);
            audit_log($this->cms_array,audit_log_data($input),Config::get('constants.store'),$this->page_name,$this->tab_name,$data->id,$data->id,$form_id,$this->db);
          return response()->json(['message'=>'Successfully Saved !!']);
        }
    }

    public function update(Request $request)
    {
        $data=$request->all();
        FormType::where('id',$data['id'])->update($data);;
        return response()->json(['message'=>'Successfully Update !!']);

    }
    public function softDelete (Request $request)
    { 
        audit_log($this->cms_array,'',Config::get('constants.soft_delete'),$this->page_name,$this->tab_name,'','',$request->id,'',$this->db);
        FormType::where('id',$request->id)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function delete(Request $request)
    {
        audit_log($this->cms_array,'',Config::get('constants.delete'),$this->page_name,$this->tab_name,'','',$request->id,'',$this->db);
        FormType::where('id',$request->id)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function active(Request $request)
    {
        audit_log($this->cms_array,'',Config::get('constants.restore'),$this->page_name,$this->tab_name,'','',$request->id,'',$this->db);
        FormType::where('id',$request->id)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
    public function activeMultiple(Request $request)
    {
        FormType::whereIn('id',$request->ids)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
     public function deleteMultipleSOft(Request $request)
    {
        FormType::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function deleteMultiple(Request $request)
    {
        FormType::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function exportPdf(Request $request)
    {
        if($request->pdf_array == 1)
        {
             $data=FormType::where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->pdf_array);
            $data=FormType::whereIn('id',$array)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        $companies='Patroltec';
        $reportname='Form Type';
        $date=\Carbon\Carbon::now()->format('D jS M Y');
        $pdf = PDF::loadView('backend.document.form_type.pdf',compact('companies','reportname','data','date'));
        return $pdf->download('Form Type.pdf');

    }

    public function exportWorld(Request $request)
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        if($request->word_array == 1)
        {
            $data=FormType::where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->word_array);
            $data=FormType::whereIn('id',$array)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }

        $table = $section->addTable();
        foreach ($data as $key => $value) {
            if($key==0)
            {
                $table->addRow();
                if(Auth()->user()->hasPermissionTo('form_type_name_word_export') || Auth::user()->all_companies == 1 )
                {
                    $table->addCell(1750)->addText('Name');
                }
                if(Auth()->user()->hasPermissionTo('form_type_show_dashboard_word_export') || Auth::user()->all_companies == 1 )
                {
                    $table->addCell(1750)->addText('Show Dashboard');
                }
                if(Auth()->user()->hasPermissionTo('form_type_count_dashboard_word_export') || Auth::user()->all_companies == 1 )
                {
                    $table->addCell(1750)->addText('Count Dashboard');
                }
                if(Auth()->user()->hasPermissionTo('form_type_count_day_word_export') || Auth::user()->all_companies == 1 )
                {
                    $table->addCell(1750)->addText('Count Day');
                }
                if(Auth()->user()->hasPermissionTo('form_type_count_response_positive_word_export') || Auth::user()->all_companies == 1 )
                {
                    $table->addCell(1750)->addText('Count Response Positive');
                }
                if(Auth()->user()->hasPermissionTo('form_type_count_response_negative_word_export') || Auth::user()->all_companies == 1 )
                {
                    $table->addCell(1750)->addText('Count Response Negative');
                }
                if(Auth()->user()->hasPermissionTo('form_type_outanding_count_word_export') || Auth::user()->all_companies == 1 )
                {
                    $table->addCell(1750)->addText('Outanding Count');
                }
                if(Auth()->user()->hasPermissionTo('form_type_late_completion_count_word_export') || Auth::user()->all_companies == 1 )
                {
                    $table->addCell(1750)->addText('Late Completion Count');
                }
                if(Auth()->user()->hasPermissionTo('form_type_detailed_field_stats_word_export') || Auth::user()->all_companies == 1 )
                {
                    $table->addCell(1750)->addText('Detailed Field Stats');
                }
            }
            $table->addRow();
            if(Auth()->user()->hasPermissionTo('form_type_name_word_export') || Auth::user()->all_companies == 1 )
            {
                 $table->addCell(1750)->addText($value->name);
            }
            if(Auth()->user()->hasPermissionTo('form_type_show_dashboard_word_export') || Auth::user()->all_companies == 1 )
            {
                $table->addCell(1750)->addText($value->_show_dashboard);
            }
            if(Auth()->user()->hasPermissionTo('form_type_count_dashboard_word_export') || Auth::user()->all_companies == 1 )
            {
                $table->addCell(1750)->addText($value->count_dashboard);
            }
            if(Auth()->user()->hasPermissionTo('form_type_count_day_word_export') || Auth::user()->all_companies == 1 )
            {
                $table->addCell(1750)->addText($value->count_day);
            }
            if(Auth()->user()->hasPermissionTo('form_type_count_response_positive_word_export') || Auth::user()->all_companies == 1 )
            {
                $table->addCell(1750)->addText($value->count_response_positive);
            }
            if(Auth()->user()->hasPermissionTo('form_type_count_response_negative_word_export') || Auth::user()->all_companies == 1 )
            {
                $table->addCell(1750)->addText($value->count_response_negative);
            }
            if(Auth()->user()->hasPermissionTo('form_type_outanding_count_word_export') || Auth::user()->all_companies == 1 )
            {
                $table->addCell(1750)->addText($value->outanding_count);
            }
            if(Auth()->user()->hasPermissionTo('form_type_late_completion_count_word_export') || Auth::user()->all_companies == 1 )
            {
                $table->addCell(1750)->addText($value->late_completion_count);
            }
            if(Auth()->user()->hasPermissionTo('form_type_detailed_field_stats_word_export') || Auth::user()->all_companies == 1 )
            {
                $table->addCell(1750)->addText($value->detailed_field_stats);
            }
        }
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('Form_Type.docx');
        return response()->download(public_path('Form_Type.docx'));
    }
    public function exportExcel(Request $request)
    {
        if($request->excel_array == 1)
        {
            $data=FormType::where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
            return Excel::download(new FormTypeExport($data), 'Form Type.xlsx');
        }
        else
        {
            $array=json_decode($request->excel_array);
            $data=FormType::whereIn('id',$array)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
            return Excel::download(new FormTypeExport($data), 'Form Type.xlsx');

        }
    }

     public function edit(Request $request)
    {
        $input = $request->all();
        $data = FormType::find($input['id']);
        return response()->json($data);
    }

}
