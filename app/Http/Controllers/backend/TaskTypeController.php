<?php
namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\TaskType;
use Validator;
use DataTables;
use PDF;
use App\Exports\tasktype\TaskTypeExport;
use Maatwebsite\Excel\Facades\Excel;
use Auth;
use Session;
use Config;
class TaskTypeController extends Controller
{
    private $page_name;
    private $tab_name;
    public function __construct() {
        $this->page_name ='Schedule Task Type';
        $this->tab_name ='Document Library';
    }
    public function getAll()
    {
        $data=TaskType::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->get();
        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {})
            ->addColumn('flag', function ($data) {
                return '1';
            })
            ->addColumn('submit', function ($data) {
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                $delete = '';
                if(Auth()->user()->hasPermissionTo('delete_documentTaskType') || Auth::user()->all_companies == 1 ){
                    $delete = '<a href="javascript:;" type="button"class="btn btn-danger btn-xs my-0 delete-btn'.$data->id.'" onclick="deletetasktypedata( ' . $data->id . ')"><i class="fas fa-trash"></i></a>';
                }
                $response = $response . $delete;
                return $response;
            })
            ->rawColumns(['actions','checkbox'])
            ->make(true);
    }

    public function getAllSoft()
    {
        $data=TaskType::where('company_id',Auth::user()->default_company)->where('active',0)->where('deleted',0)->get();
        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {})
            ->addColumn('flag', function ($data) {
                return '0';
            })
            ->addColumn('submit', function ($data) {
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                if(Auth()->user()->hasPermissionTo('hard_delete_documentTaskType') || Auth::user()->all_companies == 1 ){
                    $delete = '<a href="javascript:;" type="button"class="btn btn-danger btn-xs my-0" onclick="deletesofttasktypedata( ' . $data->id . ')"><i class="fas fa-trash"></i></a>';
                }else{
                    $delete = '';
                }
                if(Auth()->user()->hasPermissionTo('hard_delete_documentTaskType') || Auth::user()->all_companies == 1 ){
                    $restore = '<a href="javascript:;" type="button"class="btn btn-success all_action_btn_margin btn-xs my-0" onclick="restoretasktypedata( ' . $data->id . ')"><i class="fa fa-retweet"></i></a>';
                }else{
                    $restore = '';
                }
                $response = $response. $delete. $restore;
                return $response;
            })
            ->rawColumns(['actions','checkbox'])
            ->make(true);
    }

    public function store()
    {
        $data=TaskType::create(['name'=>'','company_id'=>Auth::user()->default_company]);
        $form_id=Session::get('form_id');
        audit_log('',Config::get('constants.store'),$this->page_name,$this->tab_name,$data->id,'',$data->id,$form_id);

        return response()->json(['data'=>$data]);
    }
    public function update(Request $request)
    {
        // // dd($request->all());
        // $data = [
        //         'company_id'=>'required|numeric',
        //         ];
        // $validator = Validator::make($request->all(), $data);
        // if ($validator->fails())
        // {
        //     $error = $validator->messages();
        //     $html='<ul>';
        //     foreach ($error->all() as $key => $value) {
        //         $html=$html.'<li>'.$value.'</li>';
        //     }
        //     $html=$html.'</ul>';
        //     return response()->json(['status'=> false, 'error'=> $html]);
        // }
        $data=$request->all();
        $TaskType=TaskType::where('id',$data['id']);
        $TaskType_first=$TaskType->first();
        $TaskType->update($data);
        if($TaskType_first->active == 0)
        {
            $flag=0;
        }
        else
        {
            $flag=1;
        }
        $input= $request->all();
        $form_id=Session::get('form_id');
        audit_log($input,Config::get('constants.update'),$this->page_name,$this->tab_name,$TaskType_first->id,$TaskType_first->toArray(),$TaskType_first->id,$form_id);
        return response()->json(['message'=>'Successfully Added !!','flag'=>$flag]);

    }
    public function softDelete (Request $request)
    {
        TaskType::where('id',$request->id)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function delete(Request $request)
    {
        $form_id=Session::get('form_id');
        TaskType::where('id',$request->id)->update(['active'=>0,'deleted'=>0]);
        audit_log('',Config::get('constants.delete'),$this->page_name,$this->tab_name,'','',$request->id,$form_id);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function active(Request $request)
    {
        TaskType::where('id',$request->id)->update(['active'=>1,'deleted'=>0]);
        $form_id=Session::get('form_id');
        audit_log('',Config::get('constants.restore'),$this->page_name,$this->tab_name,'','',$request->id,$form_id);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
    public function activeMultiple(Request $request)
    {
        TaskType::whereIn('id',$request->ids)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>1]);
    }
     public function deleteMultipleSOft(Request $request)
    {
        TaskType::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function deleteMultiple(Request $request)
    {
        TaskType::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function exportPdf(Request $request)
    {
        if($request->pdf_array == 1)
        {
            $data= TaskType::where('company_id',Auth::user()->default_company)->where('active', '=',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->pdf_array);
            $data= TaskType::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->where('active', '=',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        $companies='Patroltec';
        $reportname='Scoring';
        $date=\Carbon\Carbon::now()->format('D jS M Y');
        $pdf = PDF::loadView('backend.schedule.model.pdf',compact('companies','reportname','data','date'));
        return $pdf->download('TaskType.pdf');
    }

    public function exportWorld(Request $request)
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        if($request->word_array == 1)
        {
            $data= TaskType::where('company_id',Auth::user()->default_company)->where('active', '=',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->word_array);
            $data= TaskType::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->where('active', '=',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        $text = $section->addText('Word Export data');
        $table = $section->addTable();
        foreach ($data as $key => $value) {
            if($key==0) {
                $table->addRow();
                if(Auth()->user()->hasPermissionTo('documentTaskType_name_word_export') || Auth::user()->all_companies == 1 ){
                    $table->addCell(1750)->addText('Name');
                }else{
                    $table->addCell(1750)->addText('');
                }
            }
            $table->addRow();
            if(Auth()->user()->hasPermissionTo('documentTaskType_name_word_export') || Auth::user()->all_companies == 1 ){
                $table->addCell(1750)->addText(strip_tags($value->name));
            }else{
                $table->addCell(1750)->addText('');
            }

        }
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('TaskType_first.docx');
        return response()->download(public_path('TaskType_first.docx'));
    }
    public function exportExcel(Request $request)
    {
        if($request->excel_array == 1)
        {
            $data= TaskType::where('company_id',Auth::user()->default_company)->where('active', '=',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->excel_array);
            $data= TaskType::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->where('active', '=',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        return Excel::download(new TaskTypeExport($data), 'taskType.csv');
    }

}
