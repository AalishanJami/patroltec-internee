<?php

namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use App\SexualOrientations;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Session;
use DataTables;
use PDF;
use Config;
use App\Exports\employee\SexualExport;
use Auth;
use Carbon\Carbon;
class SexualOrientationController extends Controller
{
    private $page_name;
    private $tab_name;
    private $db;
    private $cms_array=[];
    public function __construct() {
        $this->page_name ='sexual_orientation';
        $this->tab_name ='main_detail';
        $this->db='sexual_orientations';
        $cms_data=localization();
        $temp_array=[];
        $temp_array['name']['key']='name';
        $temp_array['name']['value']=checkKey('name',$cms_data);
        $this->cms_array=$temp_array;
    }
    public function store()
    {
        $data=SexualOrientations::create(['name'=>'']);
    audit_log($this->cms_array,'',Config::get('constants.store'),$this->page_name,$this->tab_name,$data->id,'',$data->id,Session::get('employee_id'),$this->db);
        return response()->json(['data'=>$data]);
    }

    public function update(Request $request)
    {
        $data = [
            'company_id'=>'required|numeric',
        ];
        $validator = Validator::make($request->all(), $data);
        if ($validator->fails())
        {
            $error = $validator->messages();
            $html='<ul>';
            foreach ($error->all() as $key => $value) {
                $html=$html.'<li>'.$value.'</li>';
            }
            $html=$html.'</ul>';
            return response()->json(['status'=> false, 'error'=> $html]);
        }
        $data=$request->all();
        $Clients=SexualOrientations::where('id',$data['id']);
        $Clients_first=$Clients->first();
        audit_log($this->cms_array,audit_log_data($data),Config::get('constants.update'),$this->page_name,$this->tab_name,$Clients_first->id,audit_log_data($Clients_first->toArray()),$Clients_first->id,Session::get('employee_id'),$this->db);
        $Clients->update($data);
        if($Clients_first->active == 0)
        {
            $flag=0;
        }
        else
        {
            $flag=1;
        }

        return response()->json(['message'=>'Successfully Added !!','flag'=>$flag]);
    }

    public function getAll()
    {
        $data =SexualOrientations::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {
            })
            ->addColumn('submit', function ($data) {
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                if(Auth()->user()->hasPermissionTo('delete_employeeSexualoren') || Auth::user()->all_companies == 1 ){
                    $delete = '<a type="button" class="btn deletesexualorientationdata'.$data->id.' btn-danger btn-xs my-0 waves-effect waves-light" onclick="deletesexualorientationdata( ' . $data->id . ')"><i class="fas fa-trash"></i></a>';
                }else{
                    $delete = '';
                }
                $response = $response . $delete;
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    public function getAllSoft()
    {
        $data =SexualOrientations::where('company_id',Auth::user()->default_company)->where('active',0)->where('deleted',0)->orderBy('id', 'desc')->get();
        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {
            })
            ->addColumn('submit', function ($data) {
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                if(Auth()->user()->hasPermissionTo('restore_delete_employeeSexualoren') || Auth::user()->all_companies == 1 ){
                    $restore='<a type="button" class="btn btn-success btn-xs my-0 waves-effect waves-light" onclick="restoresexualorientationdata('.$data->id.')"><i class="fa fa-retweet" aria-hidden="true"></i></a>';
                }else{
                    $restore='';
                }
                if(Auth()->user()->hasPermissionTo('hard_delete_employeeSexualoren') || Auth::user()->all_companies == 1 ){
                    $delete = '<a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light" onclick="deletesoftsexualorientationdata( ' . $data->id . ')"><i class="fas fa-trash"></i></a>';
                }else{
                    $delete='';
                }
                $response = $response . $restore . $delete;
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    public function delete(Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.delete'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('employee_id'),$this->db);
        SexualOrientations::where('id',$request->id)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function active(Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.restore'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('employee_id'),$this->db);
        SexualOrientations::where('id',$request->id)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
    public function activeMultiple(Request $request)
    {
        SexualOrientations::whereIn('id',$request->ids)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
    public function softDelete(Request $request){
    audit_log($this->cms_array,'',Config::get('constants.soft_delete'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('employee_id'),$this->db);
        SexualOrientations::where('id',$request->id)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function deleteMultipleSOft(Request $request)
    {
        SexualOrientations::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }

    public function deleteMultiple(Request $request){
        SexualOrientations::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }

    public function exportPdf(Request $request){
        if($request->pdf_array == 1)
        {
           $data =SexualOrientations::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->pdf_array);
            $data= SexualOrientations::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        $date=Carbon::now()->format('D jS M Y');
        $companies='Patroltec';
        $reportname='Sexual Orientation';
        $pdf = PDF::loadView('backend.employee.export.sexual_pdf',compact('data','date','reportname','companies'));
        return $pdf->download('sexual.pdf');
    }

    public function exportWord(Request $request){
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        if($request->word_array == 1)
        {
            $data= SexualOrientations::where('company_id',Auth::user()->default_company)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->word_array);
            $data= SexualOrientations::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        $table = $section->addTable();
        $row = $table->addRow();
        if(Auth()->user()->hasPermissionTo('employeeSexualoren_name_word_export') || Auth::user()->all_companies == 1 ){
            $row->addCell()->addText('Name');
        }
        foreach ($data as $key => $value) {
            $row = $table->addRow();
            if(Auth()->user()->hasPermissionTo('employeeSexualoren_name_word_export') || Auth::user()->all_companies == 1 ){
                $row->addCell()->addText($value->name);
            }
        }
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('sexual.docx');
        return response()->download(public_path('sexual.docx'));
    }

    public function exportExcel(Request $request){
        if($request->excel_array == 1)
        {
            $data= SexualOrientations::where('company_id',Auth::user()->default_company)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->excel_array);
            $data= SexualOrientations::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        return Excel::download(new SexualExport($data), 'sexual.csv');
    }
}
