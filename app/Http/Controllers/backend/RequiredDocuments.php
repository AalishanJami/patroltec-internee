<?php
namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Company;
class RequiredDocuments extends Controller
{
	public function __construct() {   
        remove_key();
    }
    public function index()
    {
    	$companies=Company::getCompanies();
        return view('backend.required_documents.index')->with(['companies'=>$companies]);
    }
   
}
