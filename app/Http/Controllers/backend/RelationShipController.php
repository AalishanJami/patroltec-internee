<?php
namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\RelationShip;
use App\Company;
use DataTables;
use PDF;
use Auth;
use Session;
use Config;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\relationship\RelationshipExport;
class RelationShipController extends Controller
{
    private $page_name;
    private $tab_name;
    private $db;
    private $cms_array=[];
    public function __construct() {
        $this->page_name ='relationship';
        $this->tab_name ='employees';
        $this->db='relation_ships';
        $cms_data=localization();
        $temp_array=[];
        $temp_array['name']['key']='name';
        $temp_array['name']['value']=checkKey('name',$cms_data);
        $this->cms_array=$temp_array;
    }
    public function index()
    {
    	return view('backend.relationship.index');
    }
    public function relationship()
    {
    	$companies=Company::getCompanies();
    	return view('backend.relationship.relationship')->with(['companies'=>$companies]);
    }
    public function store()
    {
    	$data=RelationShip::create();
    audit_log($this->cms_array,'',Config::get('constants.store'),$this->page_name,$this->tab_name,$data->id,'',$data->id,Session::get('employee_id'),$this->db);
        return response()->json(['data'=>$data]);
    }
    public function getAll()
    {
    	$data=RelationShip::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {
            })
            ->addColumn('submit', function ($data) {
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                if(Auth()->user()->hasPermissionTo('delete_employee_relationship') || Auth::user()->all_companies == 1 )
                {
                    $delete = '<a type="button" class="deleterelationshipdata'.$data->id.' btn btn-danger btn-xs my-0 waves-effect waves-light" onclick="deleterelationshipdata( ' . $data->id . ')"><i class="fas fa-trash"></i></a>';
                    $response = $response . $delete;
                }
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
    public function getAllSoft()
    {
    	$data=RelationShip::where('company_id',Auth::user()->default_company)->where('active',0)->where('deleted',0)->orderBy('id', 'desc')->get();
        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {
            })
            ->addColumn('submit', function ($data) {
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                if(Auth()->user()->hasPermissionTo('hard_delete_employee_relationship') || Auth::user()->all_companies == 1 )
                {
                    $delete = '<a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light" onclick="deletesoftrelationshipdata('.$data->id.')"><i class="fas fa-trash"></i></a>';
                    $response = $response . $delete;
                }
                if(Auth()->user()->hasPermissionTo('restore_delete_employee_relationship') || Auth::user()->all_companies == 1 )
                {
                    $restore = '<a type="button" class="btn btn-success btn-xs my-0 waves-effect waves-light" onclick="restorerelationshipdata('.$data->id.')"><i class="fa fa-retweet" aria-hidden="true"></i></a>';
                    $response = $response . $restore;
                }
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
    public function update(Request $request)
    {
        $input=$request->all();
        $data=RelationShip::where('id',$input['id']);
        $first=$data->first();
        audit_log($this->cms_array,audit_log_data($input),Config::get('constants.update'),$this->page_name,$this->tab_name,$first->id,audit_log_data($first->toArray()),$first->id,Session::get('employee_id'),$this->db);
        $data->update($input);
        if($first->active == 0)
        {
            $flag=0;
        }
        else
        {
            $flag=1;
        }
        return response()->json(['message'=>'Successfully Updated !!','flag'=>$flag]);
    }
    public function delete(Request $request){
    audit_log($this->cms_array,'',Config::get('constants.delete'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('employee_id'),$this->db);
        RelationShip::where('id',$request->id)->update(['active'=>0]);
        return response()->json(['message'=>'delete','flag'=>1]);
   	}
   	public function activeMultiple(Request $request){
        RelationShip::whereIn('id',$request->ids)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'active','flag'=>0]);
   	}
   	public function deleteMultiple(Request $request){
        RelationShip::whereIn('id',$request->ids)->update(['active'=>0]);
        return response()->json(['message'=>'Deleted','flag'=>1]);
   }
   	public function deleteMultipleSoft(Request $request){
        RelationShip::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Hard Deleted','flag'=>0]);   
   }
    public function restore(Request $request){
    audit_log($this->cms_array,'',Config::get('constants.restore'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('employee_id'),$this->db);
        RelationShip::where('id',$request->id)->update(['active'=>1]);
        return response()->json(['message'=>'restore','flag'=>0]);
    }
    public function softDelete(Request $request){
    audit_log($this->cms_array,'',Config::get('constants.soft_delete'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('employee_id'),$this->db);
        RelationShip::where('id',$request->id)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Soft delete','flag'=>0]);
    }
    public function exportExcel(Request $request)
    {
	 	if($request->excel_array == 1)
        {
            $data= RelationShip::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->excel_array);
            $data=RelationShip::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        return Excel::download(new RelationshipExport($data), 'relationship.csv');
    }
    public function exportPdf(Request $request)
    {
        if($request->pdf_array == 1)
        {
            $data= RelationShip::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->pdf_array);
            $data=RelationShip::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        $companies='Patroltec';
        $reportname='Relationship';
        $date=\Carbon\Carbon::now()->format('D jS M Y');
        $pdf = PDF::loadView('backend.relationship.pdf',compact('companies','reportname','data','date'));
        return $pdf->download('relationship.pdf');
    }
    public function exportWorld(Request $request)
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        if($request->word_array == 1)
        {
            $data= RelationShip::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->word_array);
            $data=RelationShip::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        $table = $section->addTable();
        foreach ($data as $key => $value) {
            if($key==0)
            {
                $table->addRow();
                if(Auth()->user()->hasPermissionTo('employee_relationship_name_word_export') || Auth::user()->all_companies == 1 )
                {
                    $table->addCell(1750)->addText('Name');
                }
            }
            $table->addRow();
            if(Auth()->user()->hasPermissionTo('employee_relationship_name_word_export') || Auth::user()->all_companies == 1 )
                {
                    $table->addCell(1750)->addText($value->name);
                }
           
        }
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('relationship.docx');
        return response()->download(public_path('relationship.docx'));
    }
    
   
}
