<?php

namespace App\Http\Controllers\backend;
use App\Religions;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Session;
use DataTables;
use App\Exports\employee\ReligionExport;
use PDF;
use Auth;
use Config;
use Carbon\Carbon;
class ReligionController extends Controller
{
    private $page_name;
    private $tab_name;
    private $db;
    private $cms_array=[];
    public function __construct() {
        $this->page_name ='religion';
        $this->tab_name ='main_detail';
        $this->db='religions';
        $cms_data=localization();
        $temp_array=[];
        $temp_array['name']['key']='name';
        $temp_array['name']['value']=checkKey('name',$cms_data);
        $this->cms_array=$temp_array;
    }
    public function store()
    {
        $data=Religions::create(['name'=>'']);
    audit_log($this->cms_array,'',Config::get('constants.store'),$this->page_name,$this->tab_name,$data->id,'',$data->id,Session::get('employee_id'),$this->db);
        return response()->json(['data'=>$data]);
    }
    public function update(Request $request)
    {
        $data = [
            'company_id'=>'required|numeric',
            'name'=>'required',
        ];
        $validator = Validator::make($request->all(), $data);
        if ($validator->fails())
        {
            $error = $validator->messages();
            $html='<ul>';
            foreach ($error->all() as $key => $value) {
                $html=$html.'<li>'.$value.'</li>';
            }
            $html=$html.'</ul>';
            return response()->json(['status'=> false, 'error'=> $html]);
        }
        $data=$request->all();
        $religion=Religions::where('id',$data['id']);
        $religion_first=$religion->first();
        audit_log($this->cms_array,audit_log_data($data),Config::get('constants.update'),$this->page_name,$this->tab_name,$religion_first->id,audit_log_data($religion_first->toArray()),$religion_first->id,Session::get('employee_id'),$this->db);
        $religion->update($data);
        if($religion_first->active == 0)
        {
            $flag=0;
        }
        else
        {
            $flag=1;
        }
        return response()->json(['message'=>'Successfully Added !!','flag'=>$flag]);
    }

    public function getAll()
    {
        $data =Religions::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {
            })
            ->addColumn('submit', function ($data) {
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                if(Auth()->user()->hasPermissionTo('delete_employeeReligion') || Auth::user()->all_companies == 1 ){
                    $delete = '<a type="button" class="btn deletereligiondata'.$data->id.' btn-danger btn-xs my-0 waves-effect waves-light" onclick="deletereligiondata( ' . $data->id . ')"><i class="fas fa-trash"></i></a>';
                }else{
                    $delete='';
                }
                $response = $response . $delete;
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    public function getAllSoft()
    {
        $data =Religions::where('company_id',Auth::user()->default_company)->where('active',0)->where('deleted',0)->orderBy('id', 'desc')->get();
        return Datatables::of($data)
            ->addColumn('checkbox', function ($data) {
            })
            ->addColumn('submit', function ($data) {
            })
            ->addColumn('actions', function ($data) {
                $response = "";
                if(Auth()->user()->hasPermissionTo('hard_delete_employeeReligion') || Auth::user()->all_companies == 1 ){
                    $delete = '<a type="button" class="btn btn-danger btn-xs my-0 waves-effect waves-light" onclick="deletesoftreligiondata( ' . $data->id . ')"><i class="fas fa-trash"></i></a>';
                }else{
                    $delete='';
                }
                if(Auth()->user()->hasPermissionTo('restore_delete_employeeReligion') || Auth::user()->all_companies == 1 ){
                    $restore='<a type="button" class="btn btn-success btn-xs my-0 waves-effect waves-light" onclick="restorereligiondata('.$data->id.')"><i class="fa fa-retweet" aria-hidden="true"></i></a>';
                }else{
                    $restore='';
                }
                $response = $response.$delete;
                $response = $response.$restore;
                return $response;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
    public function delete(Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.delete'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('employee_id'),$this->db);
        Religions::where('id',$request->id)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function active(Request $request)
    {
    audit_log($this->cms_array,'',Config::get('constants.restore'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('employee_id'),$this->db);
        Religions::where('id',$request->id)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
    public function activeMultiple(Request $request)
    {
        Religions::whereIn('id',$request->ids)->update(['active'=>1,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Active','flag'=>0]);
    }
    public function softDelete(Request $request){
    audit_log($this->cms_array,'',Config::get('constants.soft_delete'),$this->page_name,$this->tab_name,'','',$request->id,Session::get('employee_id'),$this->db);
        Religions::where('id',$request->id)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }
    public function deleteMultipleSOft(Request $request)
    {
        Religions::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>1]);
        return response()->json(['message'=>'Successfully Delete','flag'=>0]);
    }

    public function deleteMultiple(Request $request){
        Religions::whereIn('id',$request->ids)->update(['active'=>0,'deleted'=>0]);
        return response()->json(['message'=>'Successfully Delete','flag'=>1]);
    }
    public function exportPdf(Request $request){
        if($request->pdf_array == 1)
        {
           $data =Religions::where('company_id',Auth::user()->default_company)->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->pdf_array);
            $data= Religions::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        $date=Carbon::now()->format('D jS M Y');
        $companies='Patroltec';
        $reportname='Ethinic';
        $pdf = PDF::loadView('backend.employee.export.religion_pdf',compact('data','date','reportname','companies'));
        return $pdf->download('religion.pdf');
    }

    public function exportWord(Request $request){
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        if($request->word_array == 1)
        {
            $data= Religions::where('company_id',Auth::user()->default_company)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->word_array);
            $data= Religions::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        $table = $section->addTable();
        $row = $table->addRow();
        if(Auth()->user()->hasPermissionTo('employeeReligion_name_word_export') || Auth::user()->all_companies == 1 ){
            $row->addCell()->addText('Name');
        }
        foreach ($data as $key => $value) {
            $row = $table->addRow();
            if(Auth()->user()->hasPermissionTo('employeeReligion_name_word_export') || Auth::user()->all_companies == 1 ) {
                $row->addCell()->addText($value->name);
            }
        }
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('religion.docx');
        return response()->download(public_path('religion.docx'));
    }

    public function exportExcel(Request $request){
        if($request->excel_array == 1)
        {
            $data= Religions::where('company_id',Auth::user()->default_company)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        else
        {
            $array=json_decode($request->excel_array);
            $data= Religions::where('company_id',Auth::user()->default_company)->whereIn('id',$array)->where('active', '=',1)->where('deleted', '=',0)->orderBy('id', 'desc')->get();
        }
        return Excel::download(new ReligionExport($data), 'religion.csv');
    }
}
