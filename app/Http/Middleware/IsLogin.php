<?php

namespace App\Http\Middleware;
use Closure;
use Auth;
use Session;
class IsLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth()->check()){
            if(Auth()->user()->active == 1)
            {
                return $next($request);
            }
            else
            {
                Auth::logout();
                return redirect('/login')->With(['error'=>'Your Account Is deleted']);
            }   
        }
        return redirect('/login');
    }
}
