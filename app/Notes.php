<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Notes extends Model
{
    protected $fillable = [
        'link_id',
        'link_name',
        'title',
        'note',
        'company_id',
        'manager_id',
        'date_added'
    ];

}
