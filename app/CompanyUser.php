<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class CompanyUser extends Model 
{
     protected $fillable = [
        'user_id','company_id'
    ];
    public function companyUser() {
        return $this->belongsTo('App\User','company_id','id');
    }
    public function company() {
        return $this->belongsTo('App\Company','company_id','id');
    }
    public function user() {
        return $this->hasMany('App\User','id','user_id');
    }
}
