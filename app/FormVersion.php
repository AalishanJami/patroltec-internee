<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormVersion extends Model
{
     protected $fillable = [
        'form_id',
         'version_id',
         'form_section_id'
    ];
    public function version(){
        return $this->belongsTo('App\VersionLog','version_id','id');
    }
    public function form_Section(){
        return $this->belongsTo('App\FormSections','form_section_id','id')->orderBy('order', 'asc');
    }
}
