<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class Support extends Model
{
    protected $fillable = [
		'company_id',
		'message',
		'title' ,
		'user_id',
    ];
    public function user(){
        return $this->belongsTo('App\User','user_id','id');
    }
    public function getCreatedAtAttribute($value)
	{
		return Carbon::parse($value)->format('jS M Y');
	}
}
