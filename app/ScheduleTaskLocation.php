<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScheduleTaskLocation extends Model
{
    protected $fillable = [
		'company_id',
		'location_breakdown_id',
		'location_id',
		'schedule_task_id',
		'active','deleted'
    ];
    public function company()
    {
        return $this->belongsTo('App\Company','company_id','id');
    }
     public function location()
    {
        return $this->belongsTo('App\Location','location_id','id');
    }
     public function location_breakdowns()
    {
        return $this->belongsTo('App\LocationBreakdown','location_breakdown_id','id');
    }
}
