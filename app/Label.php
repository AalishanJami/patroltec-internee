<?php

namespace App;
use Auth;
use Illuminate\Database\Eloquent\Model;

class Label extends Model 
{
    protected $fillable = [
        'key', 'lang', 'language_id',
    ];
    public function companyLabel() {
		return $this->hasMany('App\CompanyLabel','label_id','id');
	}
	public function companyLabelGetOne() {
		return $this->hasOne('App\CompanyLabel','label_id','id')->where('company_id',Auth::user()->default_company);
	}
  
}
