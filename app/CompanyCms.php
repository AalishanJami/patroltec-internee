<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyCms extends Model
{
    protected $table = 'company_cms';

     public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }
}
