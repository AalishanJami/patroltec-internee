<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocationPermision extends Model
{
    protected $fillable = [
        'location_id','employee_id','company_id'
    ];
    public function location(){
        return $this->hasOne('App\Location','id','location_id');
    }
}
