<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class ViewRegisterPermision extends Model
{
	protected $fillable = [
        'user_id','question_id','form_id'
    ];
    public function question(){
        return $this->hasOne('App\Questions','id','question_id');
    }
   
}
