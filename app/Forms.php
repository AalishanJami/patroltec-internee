<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\FilePermision;
use Session;
use Auth;
class Forms extends Model
{
    protected $appends = ['enable'];
    protected $fillable = [
        'company_id',
        'ref',
        'name',
        'form_group_id',
        'valid_to' ,
        'form_type',
        'form_action',
        'performance_reporting',
        'dashboard_group_id',
        'actions_required',
        'app_form',
        'doc_download',
        'pdf_download',
        'valid_from',
        'form_section_id',
        'import_jobs',
        'location_type',
        'lock_job_after',
        'lock_job_before',
        'default_site',
        'lock_from',
        'lock_to',
        'notes',
        'req_delete',
        'req_sign_off',
        'ov_file',
        'ov_note',
        'req_tag'
    ];
    static public function getFrom($flag=null)
    {
        $form_groups=Forms::with('form','form_group')->where('company_id',Auth::user()->default_company)->where('deleted',0)->orderBy('id', 'desc');
        if($flag== 1)
        {
           return $form_groups->where('active',0);
        }
        else
        {
            return $form_groups->where('active',1);
        }
    }
    public function form() {
        return $this->belongsTo('App\VersionLog','form_id','id');
    }

    public function form_section() {
        return $this->hasMany('App\FormSections','form_id','id');
    }
   public function version() {
        return $this->hasMany('App\VersionLog','form_id','id');
    }
    public function form_group() {
        return $this->belongsTo('App\FormGroups','form_group_id','id');
    }
    public function jobGroups() {
        return $this->hasMany('App\JobGroup','form_id','id')->where('completed','!=',2);
    }
    public function file_permision($flag=null,$id=null){
        $file_permision=FilePermision::where('form_id',$this->id);
        if($flag == 1)
        {
            return $file_permision->where('employee_id',$id)->first();
        }
        else
        {
          return $file_permision->where('employee_id',Auth::user()->id)->first();
        }
    
    }
    public function getEnableAttribute()
    {
        $id=Session::get('employee_id');
        if(isset(Auth::user()->all_companies)){
            if($this->file_permision(Auth::user()->all_companies,$id))
            {
                return 1;
            }
            else
            {
                return 0;
            }
        } else {
            return 0;
        }
    }

    protected $casts = ['form_section_id' => 'array'];
}
