<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questions extends Model
{
	protected $fillable = [
		'company_id',
		'form_id',
		'order',
		'name',
		'question',
		'section_id',
		'header',
		'footer',
		'width',
		'border_style',
		'show_on_dashboard',
        'icon_size',
        'column_size',
		'active',
		'deleted',
		'layout',
		'answer_groups_id',
        'comment',
		'answer_type_id',
        'signature_required_app_user',
        'signature_required_app_text_field',
        'signature_required_app_select_menu',
        'import_name',
        'display_pic',
        'mandatory_message'

    ];
    public function getEnableAttribute()
    {
        if($this->question_permision)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
    public function question_permision(){
        return $this->belongsTo('App\ViewRegisterPermision','id','question_id');
    }
    public function form_sections() {
        return $this->belongsTo('App\FormSections','section_id');
    }

    public function populate() {
        return $this->hasMany('App\PopulateResponse','question_id','id')->where('active',1)->where('deleted',0);
    }
    public function answer_type(){
    	return $this->hasOne('App\AnswerTypes','id', 'answer_type_id');
    }
   
    public function answer_group(){
    	return $this->hasOne('App\AnswerGroups','id', 'answer_groups_id');
    }


}
