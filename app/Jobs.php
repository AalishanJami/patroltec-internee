<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jobs extends Model
{
     protected $fillable = [
        'name','active','job_no','title','form_name','schedule_date','date_time','pre_time','post_time','t_type','service_id','details','tag','tag_job','sig','completed','dashboard','hide','form_section_id','job_group_id','pre_populate_id','signature_required_app_user','signature_required_app_text_field','signature_required_app_select_menu','order'
    ];

    protected $attributes = [
       'completed' => 0,
    ];

// protected $casts = ['user_id' => 'array'];
    public function form_section(){
    	  return $this->hasOne('App\FormSections','id','form_section_id')->with('questions.answer_group.answer');
          


		}

		public function jobGroups(){
        return $this->hasOne('App\JobGroup','id','job_group_id');
    }

   


}
