<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Scoring extends Model
{
    protected $fillable = [
        'company_id', 'location_id','form_id' ,'score','max_visits','dashboard_name','notes','active','deleted','all_location'
    ];
    public function location(){
        return $this->belongsTo('App\Location','location_id','id');
    }
    public function getScoreAttribute($value = null)
	{
		if(!$value)
		{
			return '';
		}
		else
		{
			return $value;
		}
	}
	public function getMaxVisitsAttribute($value = null)
	{
		if(!$value)
		{
			return '';
		}
		else
		{
			return $value;
		}
	}
	public function getNotesAttribute($value = null)
	{
		if(!$value)
		{
			return '';
		}
		else
		{
			return $value;
		}
	}
}
