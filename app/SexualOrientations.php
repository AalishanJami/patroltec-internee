<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SexualOrientations extends Model
{
    protected $fillable=['name'];
}
