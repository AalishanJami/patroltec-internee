<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\TaskType;
use App\LocationBreakdown;
use App\Company;
use App\Location;
use App\ScheduleTask;
use Auth;
use App\ScheduleTaskLocation;
use DataTables;
use Validator;
use App\Forms;
use App\Exports\schedule\ScheduleExport;
use App\Exports\schedule\ScheduleSoftExport;
use PDF;
use Session;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;
use App\Jobs;
use App\JobGroup;
use App\User;
use App\FormVersion;

class ScheduleTaskCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'schedule:task';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
           $tasks=ScheduleTask::with('scheduleTaskLocation')->where('active',1)->where('deleted',0)->orderBy('id', 'desc')->get();
        $today = Carbon::today();
        foreach ($tasks as $key => $task) {
            $start_date = Carbon::parse($task->start_date);
            $end_date = Carbon::parse($task->end_date);

            if($today->greaterThanOrEqualTo($start_date) && $today->lessThanOrEqualTo($end_date))
            {
                if($task->scheduleTaskLocation)
                    $location = $task->scheduleTaskLocation->toArray();
                else
                    $location = null;

                unset($location['id']);
                unset($location['created_at']);
                unset($location['updated_at']);
                unset($location['schedule_task_id']);
                unset($location['company_id']);
                if($location['location_breakdown_id']==null)
                {
                    unset($location['location_breakdown_id']);
                }
                $temp = $task->toArray();
                unset($temp['id']);
                unset($temp['created_at']);
                unset($temp['updated_at']);
                unset($temp['schedule_task_location']);

                if($location !=null)
                $data = array_merge($temp,$location);
               // dd($data);
                    

                if($task->repeat>=1)
                {
                    $i=0;
                    while($i<$task->repeat)
                    {
                        $case = $today->dayOfWeek;  
                        switch ($case) {
                                        case 0:
                                            if($task->sun == 1)
                                                {
                                           
                                                  $data['date_time']=$today;
                                                  $data['user_id'] = ['1',null];
                                                app('App\Http\Controllers\backend\ScheduleController')->job($data);
                                               
                                                }
                                            break;
                                        case 1:
                                            if($task->mon == 1)
                                                {
                                              
                                                  $data['date_time']=$today;
                                                  $data['user_id'] = ['1',null];
                                                    app('App\Http\Controllers\backend\ScheduleController')->job($data);
                                               
                                                }
                                            break;
                                        case 2:
                                            if($task->tue == 1)
                                                {
                                          
                                               
                                                  $data['date_time']=$today;
                                                  $data['user_id'] = ['1',null];
                                                  app('App\Http\Controllers\backend\ScheduleController')->job($data);
                                                
                                                }
                                            break;
                                        case 3:
                                            if($task->wed == 1)
                                                {
                                              $data['date_time']=$today;
                                                  $data['user_id'] = ['1',null];
                                                    app('App\Http\Controllers\backend\ScheduleController')->job($data);
                                                }
                                            break;
                                        case 4:
                                            if($task->thu == 1)
                                                {
                                              $data['date_time']=$today;
                                                  $data['user_id'] = ['1',null];
                                                    app('App\Http\Controllers\backend\ScheduleController')->job($data);
                                                }
                                            break;
                                        case 5:
                                            if($task->fri == 1)
                                                {
                                               $data['date_time']=$today;
                                                  $data['user_id'] = ['1',null];
                                                    app('App\Http\Controllers\backend\ScheduleController')->job($data);
                                                }
                                            break;
                                        case 6:
                                            if($task->sat == 1)
                                                {
                                            $data['date_time']=$today;
                                                  $data['user_id'] = ['1',null];
                                                    app('App\Http\Controllers\backend\ScheduleController')->job($data);
                                                }
                                            break;
                              
                                }

                            $i++;

                    }//while

                }
            }
            
        }
          


    }
}
