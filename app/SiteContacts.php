<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteContacts extends Model 
{
    protected $fillable = [
        'location_id',
        'company_id',
        'contacts_type_id',
  		'title',
        'name',
        'first_name',
        'surname',
        'phone_number',
        'extension',
        'mobile_number',
        'other_number',
        'address_street',
        'email',
        'town',
        'state',
        'post_code',

    ];
    public function getNameAttribute($value = null)
    {
        if(!$value)
        {
            return '';
        }
        else
        {
            return $value;
        }
    }

    public function getContactType(){
        return $this->hasOne('App\ContactsType','id','contacts_type_id');
    }

}
