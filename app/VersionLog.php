<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class VersionLog extends Model
{
    protected $fillable = [
        'company_id','form_id','location_id','date_time','paht_pdf','user_id','static_form_uploads_id'
    ];
    public function company() {
        return $this->belongsTo('App\Company','company_id','id');
    }
    public function user() {
        return $this->belongsTo('App\User','user_id','id');
    }

    public function form() {
        return $this->belongsTo('App\Forms','form_id','id');
    }

    public function staticForm() {
        return $this->belongsTo('App\StaticFormUpload','static_form_uploads_id','id');
    }
    public function getDateTimeAttribute($value)
    {

        return Carbon::parse($value)->format('jS M Y g:i a');

    }
}
