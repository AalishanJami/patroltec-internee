<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BonusScore extends Model
{
	protected $fillable = [
        'company_id', 'location_id', 'month','year','bonus_score','notes','active','deleted'
    ];

    public function companyBonusScor(){
        return $this->hasOne('App\Company','id','company_id');
    }
    public function getMonthAttribute($value = null)
	{
		if(!$value)
		{
			return '';
		}
		else
		{
			return $value;
		}
	}
	public function getYearAttribute($value = null)
	{
		if(!$value)
		{
			return '';
		}
		else
		{
			return $value;
		}
	}
	public function getBonusScoreAttribute($value = null)
	{
		if(!$value)
		{
			return '';
		}
		else
		{
			return $value;
		}
	}
	public function getNotesAttribute($value = null)
	{
		if(!$value)
		{
			return '';
		}
		else
		{
			return $value;
		}
	}
}
