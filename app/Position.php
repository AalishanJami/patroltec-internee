<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Auth;
use App\PositionPermision;
use Session;
class Position extends Model
{
	protected $appends = ['enable'];
    protected $fillable=['name','employee_id'];
    public function position_permision($flag=null,$id=null){
        $position_permision=PositionPermision::where('position_id',$this->id);
        if($flag)
        {
            return $position_permision->where('user_id',$id)->first();
        }
        else
        {
     	  return $position_permision->where('user_id',Auth::user()->id)->first();
        }
    }
    public function getEnableAttribute()
    {
        if($this->position_permision(Auth::user()->all_companies,Session::get('employee_id')))
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
    static public function getPosition($flag=null)
    {
        $position=Position::where('company_id',Auth::user()->default_company)->where('deleted',0)->orderBy('name', 'asc');
        if($flag== 1)
        {
           return $position->where('active',0);
        }
        else
        {
            return $position->where('active',1);
        }
    }
}
