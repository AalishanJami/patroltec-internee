<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AddressType extends Model
{
    protected $fillable = [
        'company_id', 'name','active','deleted'
    ];
    public function getAddressType(){
        return $this->belongsTo('App\LocationAddress','id','address_type_id');
    }
    public function getNameAttribute($value = null)
	{
		if(!$value)
		{
			return '';
		}
		else
		{
			return trim($value);
		}
	}
}
