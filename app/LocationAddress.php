<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class LocationAddress extends Model 
{
    protected $fillable = [
        'company_id',
        'location_id',
        'address_type_id',
        'name',
        'address_street',
        'address_village',
        'town',
        'state',
        'post_code',
        'country',
        'telephone',
        'email',

    ];


    public function getAddressType(){
        return $this->hasOne('App\AddressType','id','address_type_id');
    }


     public function getPostCodeAttribute($value = null)
    {
        if(!$value)
        {
            return '';
        }
        else
        {
            return $value;
        }
    }
     public function getTelephoneAttribute($value = null)
    {
        if(!$value)
        {
            return '';
        }
        else
        {
            return $value;
        }
    }
    public function getEmailAttribute($value = null)
    {
        if(!$value)
        {
            return '';
        }
        else
        {
            return $value;
        }
    }
    public function getNameAttribute($value = null)
    {
        if(!$value)
        {
            return '';
        }
        else
        {
            return $value;
        }
    }
    public function getAddressStreetAttribute($value = null)
    {
        if(!$value)
        {
            return '';
        }
        else
        {
            return $value;
        }
    }
    public function getVillageStreetAttribute($value = null)
    {
        if(!$value)
        {
            return '';
        }
        else
        {
            return $value;
        }
    }
}
